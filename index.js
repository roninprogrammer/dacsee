// import applyDecoratedDescriptor from '@babel/runtime/helpers/es6/applyDecoratedDescriptor';
// import initializerDefineProperty from '@babel/runtime/helpers/es6/initializerDefineProperty';

// Object.assign(babelHelpers, {applyDecoratedDescriptor, initializerDefineProperty});

// require('./src');

import { AppRegistry } from 'react-native'
import ApplicationCore from './application'
import config from './app.env'
import $ from './application/global'
import BackgroundLocationService from './application/global/service/backgroundLocation'


global['$'] = $
global['$'].config = config

console.disableYellowBox = true

AppRegistry.registerComponent('dacsee', () => ApplicationCore)

BackgroundLocationService.registerHeadlessTask()