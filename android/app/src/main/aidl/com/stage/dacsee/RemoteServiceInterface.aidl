// RemoteServiceInterface.aidl
package com.stage.dacsee;

// Declare any non-default types here with import statements

interface RemoteServiceInterface {
    String getServiceName();
}
