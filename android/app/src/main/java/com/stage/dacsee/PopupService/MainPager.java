package com.stage.dacsee.PopupService;
import android.animation.ArgbEvaluator;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.WindowManager;

import com.stage.dacsee.R;

import java.util.ArrayList;
import java.util.List;

public class MainPager extends AppCompatActivity {

    ViewPager viewPager;
    Adapter adapter;
    List<BookingModel> models = new ArrayList<>();
    Integer[] colors = null;
    ArgbEvaluator argbEvaluator = new ArgbEvaluator();

    final BroadcastReceiver addReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle extras = intent.getExtras();
            BookingModel bookingModel = new BookingModel(extras);

            models.add(bookingModel);
            adapter.notifyDataSetChanged();

            stopPlaying();
            startPlaying();

            viewPager.setCurrentItem(models.size() - 1);

            handlerFinish.removeCallbacks(runnableFinish);
            handlerFinish.postDelayed(runnableFinish, 60000);
        }
    };

    final BroadcastReceiver removeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle extras = intent.getExtras();
            String booking_id = extras.getString("BOOKING_ID");

            int index = findModel(booking_id);

            if (index > -1) {
                models.remove(index);
                adapter.notifyDataSetChanged();

                int currItem = index < models.size() ? index : models.size() - 1;

                if (currItem > -1) {
                    viewPager.setCurrentItem(currItem);
                } else {
                    // Models is empty, remove MainPager
                }
            } else {
                // Booking not found
            }

            if (models.size() == 0) {
                stopPlaying();
                finish();
            }
        }
    };

    final BroadcastReceiver actionReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getStringExtra("action");
            String booking_id = intent.getStringExtra("booking_id");

            if (action.equals("accept")) {
                Intent userAction = new Intent("android.intent.action.POPUP");
                userAction.putExtra("action", "accept");
                userAction.putExtra("booking_id", booking_id);
                getApplicationContext().sendBroadcast(userAction);

                stopPlaying();
                finish();

            } else if (action.equals("reject")) {
                int index = findModel(booking_id);

                if (index > -1) {
                    models.remove(index);
                    adapter.notifyDataSetChanged();

                    Intent userAction = new Intent("android.intent.action.POPUP");
                    userAction.putExtra("action", "reject");
                    userAction.putExtra("booking_id", booking_id);
                    getApplicationContext().sendBroadcast(userAction);

                    int currItem = index < models.size() ? index : models.size() - 1;

                    if (currItem > -1) {
                        viewPager.setCurrentItem(currItem);
                    } else {
                        // Models is empty, remove MainPager
                    }
                } else {
                    // Booking not found
                }

                if (models.size() == 0) {
                    stopPlaying();
                    finish();
                }
            }
        }
    };

    private MediaPlayer bookingMP;
    private AudioManager mAudioManager;

    Handler handlerFinish = new Handler();
    Runnable runnableFinish = new Runnable() {
        public void run() {
            stopPlaying();
            finish();
        }
    };

    public int findModel(String booking_id) {
        int index = 0;

        for (BookingModel model : models) {
            if (model.getBookingId().equals(booking_id)) {
                return index;
            } else {
                index++;
            }
        }

        return -1;
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        IntentFilter intentAdd = new IntentFilter("addBookingView");
        IntentFilter intentRemove = new IntentFilter("removeBookingView");
        IntentFilter intentUpdate = new IntentFilter("updateBookingView");

        registerReceiver(addReceiver, intentAdd);
        registerReceiver(removeReceiver, intentRemove);
        registerReceiver(actionReceiver, intentUpdate);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
                | WindowManager.LayoutParams.FLAG_ALLOW_LOCK_WHILE_SCREEN_ON
        );

        setContentView(R.layout.activity_pager);

        mAudioManager = (AudioManager) getSystemService(AUDIO_SERVICE);

        int media_max_volume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);

        mAudioManager.setStreamVolume(
                AudioManager.STREAM_MUSIC, // Stream type
                media_max_volume - 7, // Index
                0 // Flags
        );

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            models.add(new BookingModel(extras));

            stopPlaying();
            startPlaying();

            handlerFinish.removeCallbacks(runnableFinish);
            handlerFinish.postDelayed(runnableFinish, 60000);
        }

        adapter = new Adapter(models, this);

        viewPager = findViewById(R.id.viewPager);
        viewPager.setOffscreenPageLimit(3);
        viewPager.setAdapter(adapter);
        viewPager.setPadding(50, 0, 50, 0);
        viewPager.setBackgroundColor(getResources().getColor(R.color.greyBg));

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int i) {

            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();

        SharedPreferences sharedPref = this.getSharedPreferences("MainPager", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean("running", false);
        editor.commit();

        // stopPlaying();

        try {
            getApplicationContext().unregisterReceiver(addReceiver);
            getApplicationContext().unregisterReceiver(removeReceiver);
            getApplicationContext().unregisterReceiver(actionReceiver);

        } catch(IllegalArgumentException e) {
            e.printStackTrace();
        }

        handlerFinish.removeCallbacks(runnableFinish);
    }

    protected void startPlaying() {
        if (bookingMP == null) {
            bookingMP = MediaPlayer.create(getApplicationContext(), R.raw.new_booking);
            bookingMP.setLooping(false);
        }

        bookingMP.start();

    }

    protected void stopPlaying() {
        if (bookingMP != null){
            if (bookingMP.isPlaying()) bookingMP.stop();

            bookingMP.reset();
            bookingMP.release();
            bookingMP = null;
        }
    }
}
