package com.stage.dacsee.KeepAliveService;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.location.Location;
import android.os.IBinder;
import android.util.Log;

import com.stage.dacsee.utils.HttpConnectionUtil;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;


public class DriverWorkService extends Service {
  private final static String TAG = "DriverWorkService";
  private DriverWorkConn conn;

  private Boolean threadAutorun = true;

  private String authToken = "";
  private String vehicleID = "";
  private String putUrl = "";
  private SmartLocation smartLocation = null;
  private Location location = null;

  @Override
  public void onCreate() {
    super.onCreate();
    smartLocation = SmartLocation.with(this);
    smartLocation.location().start(new OnLocationUpdatedListener() {
      @Override
      public void onLocationUpdated(Location _location) {
        location = _location;
      }
    });

    IntentFilter intentFilter = new IntentFilter();
    intentFilter.addAction("kill_self");
    intentFilter.addAction("set_value");
    registerReceiver(broadcastReceiver, intentFilter);
    timer.schedule(task, 0, 5000);// 设置检测的时间周期(毫秒数)

    this.conn = new DriverWorkConn();
  }

  private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
      if ("kill_self".equals(intent.getAction())) {
        threadAutorun = false;
        killMyselfPid(); // 杀死自己的进程
      } else if ("set_value".equals(intent.getAction())) {
        authToken = intent.getStringExtra("authToken");
        vehicleID = intent.getStringExtra("vehicleID");
        putUrl = intent.getStringExtra("putUrl");
      }
    }
  };

  class DriverWorkConn implements ServiceConnection {
    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
      if (threadAutorun) {
        DriverWorkService.this.startService(new Intent(DriverWorkService.this, GuardianService.class));
        DriverWorkService.this.bindService(new Intent(DriverWorkService.this, GuardianService.class), conn, Context.BIND_IMPORTANT);
      }
    }
  }


  private Timer timer = new Timer();
  private TimerTask task = new TimerTask() {
    @Override
    public void run() {
      autoUpload();
//      checkIsAlive();
    }
  };

  private void autoUpload() {
    try {
      if (authToken.length() == 0 || vehicleID.length() == 0) return;

      Map<String, Object> headers = new HashMap<>();
      headers.put("Authorization", authToken);

      Map<String, Object> body = new HashMap<>();
      body.put("latitude", location.getLatitude());
      body.put("longitude", location.getLongitude());
      body.put("vehicle_id", vehicleID);

      Log.e("[DACSEE]", body.toString());

      String log = HttpConnectionUtil.getHttp().putRequset(putUrl, headers, body);
      Log.e("[DACSEE]", log);
    } catch (Exception e) {
      Log.e("[DACSEE]", e.toString());
    }
  }

//  private void checkIsAlive() {
//    String format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.CHINA).format(new Date());
//    Log.e(TAG, "守护服务启动时间: " + format);
//
//    boolean stageEnv = getPackageName().indexOf(".stage") != -1;
//    String packgeName = stageEnv ? "com.stage.dacsee" : "com.stage.dacsee";
//    String mainActivity = stageEnv ? "MainActivity" : "MainActivity";
//
//    boolean WrokActivity = UtilsPackages.isClsRunning(DriverWorkService.this, packgeName, mainActivity);
//    if (!WrokActivity && threadAutorun) {
//      Class mainClass = null;
//      try {
//        mainClass = Class.forName(mainActivity);
//      } catch (Exception e) {
//        Log.e(TAG, "获取应用失败: " + format);
//      }
//
//      if (mainClass != null) {
//        Log.e(TAG, "守护服务激活进程: " + format);
//        Intent intent = new Intent();
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        intent.setClass(DriverWorkService.this, mainClass);
//        startActivity(intent);
//      }
//    }
//  }

  @Override
  public int onStartCommand(Intent intent, int flags, int startId) {
    return START_STICKY;
  }

  @Override
  public IBinder onBind(Intent arg0) {
    return null;
  }

  private void killMyselfPid() {
    int pid = android.os.Process.myPid();
    String command = "kill -9 " + pid;
    Log.e(TAG, "killMyselfPid: " + command);
    stopService(new Intent(DriverWorkService.this, DriverWorkService.class));
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    SmartLocation.with(this).location().stop();
    unregisterReceiver(broadcastReceiver);
    if (task != null) { task.cancel(); }
    if (timer != null) { timer.cancel(); }
    if (threadAutorun) {
      DriverWorkService.this.startService(new Intent(DriverWorkService.this, GuardianService.class));
      DriverWorkService.this.bindService(new Intent(DriverWorkService.this, GuardianService.class), conn, Context.BIND_IMPORTANT);
    }
  }
}