package com.stage.dacsee.PopupService;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.stage.dacsee.R;

import java.util.List;

public class Adapter extends PagerAdapter {

    private List<BookingModel> models;
    private LayoutInflater layoutInflater;
    private Context context;


    public Adapter(List models, Context context) {
        this.models = models;
        this.context = context;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public int getCount() {
        return  models.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.new_dialog, container, false);

        BookingModel bookingModel = this.models.get(position);
        String booking_id = bookingModel.getBookingId();
        String booking_code = bookingModel.getBookingCode();
        String groupName = bookingModel.getGroupName();
        String fare = bookingModel.getFare();
        String bookingTime = bookingModel.getBookingTime();
        String type = bookingModel.getType();
        String payment_method = bookingModel.getPaymentMethod();
        String fromName = bookingModel.getFromName();
        String fromAdd = bookingModel.getFromAdd();
        String destinationName = bookingModel.getDestinationName();
        String destinationAdd = bookingModel.getDestinationAdd();
        String distance = bookingModel.getDistance();
        String hasPromo = bookingModel.getHasPromo();
        String hasSurge = bookingModel.getHasSurge();
        String hasCampaign = bookingModel.getHasCampaign();
        String notes = bookingModel.getNotes();
        String rejectBtn = bookingModel.getRejectBtn();
        String acceptBtn = bookingModel.getAcceptBtn();
        String incentive = bookingModel.getIncentive();

        TextView tBOOKING_CODE = (TextView) view.findViewById(R.id.BOOKING_CODE);
        tBOOKING_CODE.setText(booking_code);

        TextView tGROUP_NAME = (TextView) view.findViewById(R.id.GROUP_NAME);
        tGROUP_NAME.setText(groupName);

        TextView tFARE = (TextView) view.findViewById(R.id.FARE);
        tFARE.setText(fare);

        RelativeLayout advanceVisible = (RelativeLayout) view.findViewById(R.id.ADVANCE_BOOKING);
        advanceVisible.setVisibility(type.equals("advance") ? View.VISIBLE : View.GONE);

        TextView promoVisible = (TextView) view.findViewById(R.id.PROMO_LABEL);
        promoVisible.setVisibility(hasPromo.equals("true") ? View.VISIBLE : View.GONE);

        TextView surgeVisible = (TextView) view.findViewById(R.id.FARE_LABEL);
        surgeVisible.setVisibility(hasSurge.equals("true") ? View.VISIBLE : View.GONE);

        // TextView campaignVisible = (TextView) view.findViewById(R.id.CAMPAIGN_LABEL);
        // campaignVisible.setVisibility(hasCampaign.equals("true") ? View.VISIBLE : View.GONE);

//        LinearLayout labelVisible = (LinearLayout) view.findViewById(R.id.BOOKING_LABEL);
//        labelVisible.setVisibility(hasPromo.equals("false") && hasSurge.equals("false") ? View.GONE : View.VISIBLE);
        // labelVisible.setVisibility(hasPromo.equals("false") && hasSurge.equals("false") && hasCampaign.equals("false") ? View.GONE : View.VISIBLE);

        TextView tNOTE = (TextView) view.findViewById(R.id.NOTE);
        tNOTE.setText(notes);

        LinearLayout noteVisible = (LinearLayout) view.findViewById(R.id.PASSENGER_NOTE);
        noteVisible.setVisibility(TextUtils.isEmpty(notes) ? View.GONE : View.VISIBLE);

        TextView tBOOKING_TIME = (TextView) view.findViewById(R.id.BOOKING_TIME);
        tBOOKING_TIME.setText(bookingTime);

        TextView tPAYMENT = (TextView) view.findViewById(R.id.PAYMENT);
        tPAYMENT.setText(payment_method);

        TextView tFROM = (TextView) view.findViewById(R.id.FROM);
        tFROM.setText(fromName);

        TextView tFROM_ADD = (TextView) view.findViewById(R.id.FROM_ADD);
        tFROM_ADD.setText(fromAdd);

        TextView tDESTINATION = (TextView) view.findViewById(R.id.DESTINATION);
        tDESTINATION.setText(destinationName);

        TextView tDESTINATION_ADD = (TextView) view.findViewById(R.id.DESTINATION_ADD);
        tDESTINATION_ADD.setText(destinationAdd);

        TextView tDISTANCE = (TextView) view.findViewById(R.id.DISTANCE);
        tDISTANCE.setText(distance);

        TextView tINCENTIVE = (TextView) view.findViewById(R.id.INCENTIVE);
        tINCENTIVE.setText(incentive);
        tINCENTIVE.setVisibility(TextUtils.isEmpty(incentive) ? View.GONE : View.VISIBLE);

        Button tREJECT_BTN = (Button) view.findViewById(R.id.rejectBtn);
        tREJECT_BTN.setText(rejectBtn);

        Button tACCEPT_BTN = (Button) view.findViewById(R.id.acceptBtn);
        tACCEPT_BTN.setText(acceptBtn);

        tACCEPT_BTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentUpdate = new Intent("updateBookingView");
                intentUpdate.putExtra("action", "accept");
                intentUpdate.putExtra("booking_id", booking_id);

                context.sendBroadcast(intentUpdate);
            }
        });

        tREJECT_BTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentUpdate = new Intent("updateBookingView");
                intentUpdate.putExtra("action", "reject");
                intentUpdate.putExtra("booking_id", booking_id);

                context.sendBroadcast(intentUpdate);
            }
        });

        container.addView(view, 0);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((RelativeLayout) object);
    }
}
