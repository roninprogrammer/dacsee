package com.stage.dacsee.PopupService;

import android.os.Bundle;

public class BookingModel {

    private String booking_id = "";
    private String booking_code = "";
    private String groupName = "";
    private String fare = "";
    private String bookingTime = "";
    private String type = "";
    private String payment_method = "";
    private String fromName = "";
    private String fromAdd = "";
    private String destinationName = "";
    private String destinationAdd = "";
    private String distance = "";
    private String hasPromo = "";
    private String hasSurge = "";
    private String hasCampaign = "";
    private String notes = "";
    private String rejectBtn = "";
    private String acceptBtn = "";
    private String incentive = "";


    public BookingModel(Bundle extras) {
        this.booking_id = extras.getString("BOOKING_ID");
        this.booking_code = extras.getString("BOOKING_CODE");
        this.groupName = extras.getString("GROUP_NAME");
        this.fare = extras.getString("FARE");
        this.bookingTime = extras.getString("BOOKING_TIME");
        this.type = extras.getString("TYPE");
        this.payment_method = extras.getString("PAYMENT");
        this.fromName = extras.getString("FROM");
        this.fromAdd = extras.getString("FROM_ADD");
        this.destinationName = extras.getString("DESTINATION");
        this.destinationAdd = extras.getString("DESTINATION_ADD");
        this.distance = extras.getString("DISTANCE");
        this.hasPromo = extras.getString("HAS_PROMO");
        this.hasSurge = extras.getString("HAS_SURGE");
        this.hasCampaign = extras.getString("HAS_CAMPAIGN");
        this.notes = extras.getString("NOTES");
        this.rejectBtn = extras.getString("REJECT_BTN");
        this.acceptBtn = extras.getString("ACCEPT_BTN");
        this.incentive = extras.getString("INCENTIVE");
    }

    public String getBookingId() {
        return booking_id;
    }

    public void setBookingId(String booking_id) {
        this.booking_id = booking_id;
    }

    public String getBookingCode() {
        return booking_code;
    }

    public void setBookingCode(String booking_code) {
        this.booking_code = booking_code;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getFare() {
        return fare;
    }

    public void setFare(String fare) {
        this.fare = fare;
    }

    public String getBookingTime() {
        return bookingTime;
    }

    public void setBookingTime(String bookingTime) {
        this.bookingTime = bookingTime;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPaymentMethod() {
        return payment_method;
    }

    public void setPaymentMethod(String payment_method) {
        this.payment_method = payment_method;
    }

    public String getFromName() {
        return fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    public String getFromAdd() {
        return fromAdd;
    }

    public void setFromAdd(String fromAdd) {
        this.fromAdd = fromAdd;
    }

    public String getDestinationName() {
        return destinationName;
    }

    public void setDestinationName(String destinationName) {
        this.destinationName = destinationName;
    }

    public String getDestinationAdd() {
        return destinationAdd;
    }

    public void setDestinationAdd(String destinationAdd) {
        this.destinationAdd = destinationAdd;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getHasPromo() {
        return hasPromo;
    }

    public void setHasPromo(String hasPromo) {
        this.hasPromo = hasPromo;
    }

    public String getHasSurge() {
        return hasSurge;
    }

    public void setHasSurge(String hasSurge) {
        this.hasSurge = hasSurge;
    }

    public String getHasCampaign() {
        return hasCampaign;
    }

    public void setHasCampaign(String hasCampaign) {
        this.hasCampaign = hasCampaign;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getRejectBtn() {
        return rejectBtn;
    }

    public void setRejectBtn(String rejectBtn) {
        this.rejectBtn = rejectBtn;
    }

    public String getAcceptBtn() {
        return acceptBtn;
    }

    public void setAcceptBtn(String acceptBtn) {
        this.acceptBtn = acceptBtn;
    }

    public String getIncentive() {
        return incentive;
    }

    public void setIncentive(String incentive) {
        this.incentive = incentive;
    }
}
