package com.stage.dacsee;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.airbnb.android.react.lottie.LottiePackage;
import com.airbnb.android.react.maps.MapsPackage;
import com.stage.dacsee.BuildConfig;
import com.stage.dacsee.nativeBridge.LocationService.LocationServicePackage;
import com.stage.dacsee.nativeBridge.Payment.iPay88.IPay88Package;
import com.stage.dacsee.nativeBridge.ToastModule.CustomToastPackage;
import com.stage.dacsee.nativeBridge.Utils.UtilsPackages;
import com.flurry.android.FlurryAgent;
import com.stage.dacsee.PopupService.PopupServicePackage;
import com.facebook.react.ReactApplication;
// import com.reactnativecommunity.netinfo.NetInfoPackage;
import com.transistorsoft.rnbackgroundgeolocation.RNBackgroundGeolocation;
import com.dieam.reactnativepushnotification.ReactNativePushNotificationPackage;
import com.levelasquez.androidopensettings.AndroidOpenSettingsPackage;
import org.pweitz.reactnative.locationswitch.LocationSwitchPackage;
import com.smixx.fabric.FabricPackage;
//import com.transistorsoft.rnbackgroundfetch.RNBackgroundFetchPackage;
import com.facebook.soloader.SoLoader;
//import com.microsoft.appcenter.reactnative.crashes.AppCenterReactNativeCrashesPackage;
//import com.microsoft.appcenter.reactnative.analytics.AppCenterReactNativeAnalyticsPackage;
import com.horcrux.svg.SvgPackage;
import com.rnim.rn.audio.ReactNativeAudioPackage;
import com.transistorsoft.rnbackgroundfetch.RNBackgroundFetchPackage;
import com.zmxv.RNSound.RNSoundPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.facebook.react.shell.MainReactPackage;
//import com.facebook.soloader.SoLoader;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.imagepicker.ImagePickerPackage;
import com.learnium.RNDeviceInfo.RNDeviceInfo;
import com.mehcode.reactnative.splashscreen.SplashScreenPackage;
import com.microsoft.codepush.react.CodePush;
import com.oblador.vectoricons.VectorIconsPackage;
import com.rnfs.RNFSPackage;
import org.reactnative.camera.RNCameraPackage;
import java.util.ArrayList;
import java.util.List;

import static android.util.Log.VERBOSE;


public class MainApplication extends Application implements ReactApplication {

  public static Context context;

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {

    @Override
    protected String getJSBundleFile() {
      return CodePush.getJSBundleFile();
    }
    
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {

      List<ReactPackage> packages = new ArrayList<>();
      packages.add(new MainReactPackage());
      // packages.add(new NetInfoPackage());
      packages.add(new AndroidOpenSettingsPackage());
      packages.add(new LocationSwitchPackage());
      packages.add(new FabricPackage());
      packages.add(new RNBackgroundGeolocation());
      packages.add(new RNBackgroundFetchPackage());
      packages.add(new SvgPackage());
      packages.add(new ReactNativeAudioPackage());
      packages.add(new RNSoundPackage());
      packages.add(new LottiePackage());
      packages.add(new ImagePickerPackage());
      packages.add(new SplashScreenPackage());
      packages.add(new RNDeviceInfo());
      packages.add(new ReactNativePushNotificationPackage());
      packages.add(new VectorIconsPackage());
      packages.add(new CodePush("", getApplicationContext(), com.facebook.react.BuildConfig.DEBUG));
      packages.add(new RNFSPackage());
      packages.add(new UtilsPackages());
      packages.add(new LocationServicePackage());
      packages.add(new IPay88Package());
      packages.add(new CustomToastPackage());
      packages.add(new PopupServicePackage());
      packages.add(new RNCameraPackage());

      int googleAvailable = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(context);
      if (googleAvailable == ConnectionResult.SUCCESS || googleAvailable == ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED) {
        packages.add(new MapsPackage());
      }
      return packages;
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();

    SharedPreferences preferences = this.getSharedPreferences("defaultPerferences", Context.MODE_PRIVATE);
    if (preferences == null) {
      SharedPreferences.Editor editor = preferences.edit();
      editor.putBoolean("auotrun", false);
      editor.commit();
    }

    String packageName = getPackageName();
    Boolean stageEnv = packageName.indexOf(".stage") != -1;

    new FlurryAgent.Builder()
            .withLogEnabled(true)
            .withCaptureUncaughtExceptions(true)
            .withContinueSessionMillis(10000)
            .withLogLevel(VERBOSE)
            .build(this, stageEnv ? "6FQFRPTPJYKM4DBMSW3W" : "F9KPJ93XZQTYR4V7XWKV");

    SoLoader.init(this, /* native exopackage */ false);
    context = this.getApplicationContext();

//    UtilsPackages.isServiceWorked(this, "DriverWorkService");
//    Intent localService = new Intent(this, DriverWorkService.class);
//    startService(localService);
  }

  public static void sendEvent(ReactContext appContext, String eventName, WritableMap map) {
    appContext.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit(eventName, map);
  }
}
