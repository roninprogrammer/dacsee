package com.stage.dacsee.PopupService;

import com.stage.dacsee.R;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.stage.dacsee.R;

public class SampleSlide extends Fragment {

    private static final String ARG_LAYOUT_RES_ID = "layoutResId";
    private int layoutResId;
    private String booking_id = "";
    private String groupName = "";
    private String fare = "";
    private String bookingTime = "";
    private String type = "";
    private String payment_method = "";
    private String fromName = "";
    private String fromAdd = "";
    private String destinationName = "";
    private String destinationAdd = "";
    private String distance = "";
    private String hasPromo = "";
    private String hasSurge = "";
    private String notes = "";
    private String rejectBtn = "";
    private String acceptBtn = "";

    public static SampleSlide newInstance(int layoutResId, Bundle args) {
        SampleSlide sampleSlide = new SampleSlide();

        args.putInt(ARG_LAYOUT_RES_ID, layoutResId);
        sampleSlide.setArguments(args);

        return sampleSlide;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            layoutResId = getArguments().getInt(ARG_LAYOUT_RES_ID);
            fromName = getArguments().getString("FROM");

        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(layoutResId, container, false);

        TextView tFROM = (TextView) view.findViewById(R.id.FROM);
        tFROM.setText(fromName);

        return view;
    }
}