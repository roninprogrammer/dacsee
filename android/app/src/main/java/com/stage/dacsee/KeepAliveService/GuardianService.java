package com.stage.dacsee.KeepAliveService;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import androidx.annotation.Nullable;
import android.util.Log;

import com.stage.dacsee.RemoteServiceInterface;

public class GuardianService extends Service {

  private final static String TAG = "GuardianService";

  private GuardianConn conn;
  private GuardianBinder binder;

  private Boolean threadAutorun = true;

  private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
      if ("kill_self".equals(intent.getAction())) {
        threadAutorun = false;
        killMyselfPid(); // 杀死自己的进程
      }
    }
  };

  @Nullable
  @Override
  public IBinder onBind(Intent intent) {
    return binder;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    Log.e(TAG, "onCreate: 启动远程监控服务! ");
    conn = new GuardianConn();
    binder = new GuardianBinder();
  }

  @Override
  public int onStartCommand(Intent intent, int flags, int startId) {
    this.bindService(new Intent(this, DriverWorkService.class), conn, Context.BIND_IMPORTANT);
    return START_STICKY;
  }

  class GuardianBinder extends RemoteServiceInterface.Stub {
    @Override
    public String getServiceName() throws RemoteException {
      return GuardianService.class.getSimpleName();
    }
  }

  class GuardianConn implements ServiceConnection {

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {

    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
      if (threadAutorun) {
        GuardianService.this.startService(new Intent(GuardianService.this, DriverWorkService.class));
        GuardianService.this.bindService(new Intent(GuardianService.this, DriverWorkService.class), conn, Context.BIND_IMPORTANT);
      }
    }

  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    if (threadAutorun) {
      GuardianService.this.startService(new Intent(GuardianService.this, DriverWorkService.class));
      GuardianService.this.bindService(new Intent(GuardianService.this, DriverWorkService.class), conn, Context.BIND_IMPORTANT);
    }
  }

  private void killMyselfPid() {
    int pid = android.os.Process.myPid();
    String command = "kill -9 " + pid;
    Log.e(TAG, "killMyselfPid: " + command);
    stopService(new Intent(GuardianService.this, GuardianService.class));
  }
}
