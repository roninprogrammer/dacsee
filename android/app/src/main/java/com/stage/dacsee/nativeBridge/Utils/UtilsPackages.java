package com.stage.dacsee.nativeBridge.Utils;

import android.app.ActivityManager;
import android.content.Context;
import android.text.TextUtils;

import com.facebook.react.ReactPackage;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.uimanager.ViewManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class UtilsPackages implements ReactPackage {

  @Override
  public List<ViewManager> createViewManagers(ReactApplicationContext reactContext) {
    return Collections.emptyList();
  }

  @Override
  public List<NativeModule> createNativeModules(ReactApplicationContext reactContext) {
    List<NativeModule> packages = new ArrayList<>();
    packages.add(new DeviceModule(reactContext));
    return packages;
  }

  public static boolean isServiceWorked(Context context, String serviceName) {
    ActivityManager myManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
    ArrayList<ActivityManager.RunningServiceInfo> runningService = (ArrayList<ActivityManager.RunningServiceInfo>) myManager.getRunningServices(Integer.MAX_VALUE);
    for (int i = 0; i < runningService.size(); i++) {
      if (runningService.get(i).service.getClassName().toString().equals(serviceName)) {
        return true;
      }
    }
    return false;
  }

  /**
   * 判断一个Activity是否正在运行
   *
   * @param pkg     pkg为应用包名
   * @param cls     cls为类名eg
   * @param context
   * @return
   */
  public static boolean isClsRunning(Context context, String pkg, String cls) {
    ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
    List<ActivityManager.RunningTaskInfo> tasks = am.getRunningTasks(1);
    ActivityManager.RunningTaskInfo task = tasks.get(0);
    if (task != null) {
      return TextUtils.equals(task.topActivity.getPackageName(), pkg) &&
              TextUtils.equals(task.topActivity.getClassName(), cls);
    }
    return false;
  }
}
