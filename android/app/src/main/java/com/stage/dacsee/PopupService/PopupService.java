package com.stage.dacsee.PopupService;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.Promise;
import com.facebook.react.modules.core.DeviceEventManagerModule;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.BroadcastReceiver;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.app.Activity;
import android.app.AlertDialog;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import android.content.BroadcastReceiver;
import java.lang.ref.WeakReference;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
public class PopupService extends ReactContextBaseJavaModule {

  private static final String DURATION_SHORT_KEY = "SHORT";
  private static final String DURATION_LONG_KEY = "LONG";
  final BroadcastReceiver mReceiver;
//  String booking_id = "";

  private static final String LOG_TAG = "PopupService";

  private static final String E_ACTIVITY_DOES_NOT_EXIST = "E_ACTIVITY_DOES_NOT_EXIST";
  private static final String SUCCESS = "SUCCESS";

  public Boolean storesHydrated = false;
  public Boolean reactUILoaded = false;

  public PopupService(ReactApplicationContext reactContext) {

    super(reactContext);
    IntentFilter intentFilter = new IntentFilter(
            "android.intent.action.POPUP");

    mReceiver = new BroadcastReceiver() {

      @Override
      public void onReceive(Context context, Intent intent) {
        String action = intent.getStringExtra("action");
        String booking_id = intent.getStringExtra("booking_id");

        // ADD RCT DEVICE EVENT EMITTER
        WritableMap params = Arguments.createMap();
        params.putString("booking_id", booking_id);
        params.putString("action", action);

        getReactApplicationContext()
                .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                .emit("NewBooking", params);
      }
    };

    getReactApplicationContext().registerReceiver(mReceiver, intentFilter);
  }

  @Override
  public String getName() {
    return "PopupService";
  }

  @ReactMethod
  public void unregisterReceiver() {
    if (getCurrentActivity() == null) return;
    try
    {
      getCurrentActivity().unregisterReceiver(mReceiver);
    } catch (java.lang.IllegalArgumentException e) {
      // Have been unregistered
    }
  }

  @ReactMethod
  public void showPopup() {
    if(getCurrentActivity() != null) {
      Context context = getReactApplicationContext();

      Intent intent = new Intent(getCurrentActivity(), DefaultIntro.class);

      context.startActivity(intent);
    }
  }

  @ReactMethod
  public void addBookingView(
          String booking_id,
          String booking_code,
          String groupName,
          String fare,
          String bookingTime,
          String type,
          String payment_method,
          String fromName,
          String fromAdd,
          String destinationName,
          String destinationAdd,
          String distance,
          String hasPromo,
          String hasSurge,
          String hasCampaign,
          String notes,
          String rejectBtn,
          String acceptBtn,
          String incentive,
          Promise promise
  ) {
    if(getCurrentActivity() != null) {
      Context context = getReactApplicationContext();

      Intent pagerIntent = new Intent(getCurrentActivity(), MainPager.class);

      SharedPreferences sharedPref = getCurrentActivity().getSharedPreferences("MainPager", Context.MODE_PRIVATE);
      Boolean running = sharedPref.getBoolean("running", false);

      if (running) {
        Intent intent = new Intent("addBookingView");
        intent.putExtra("BOOKING_ID", booking_id);
        intent.putExtra("BOOKING_CODE", booking_code);
        intent.putExtra("GROUP_NAME", groupName);
        intent.putExtra("FARE", fare);
        intent.putExtra("BOOKING_TIME", bookingTime);
        intent.putExtra("TYPE", type);
        intent.putExtra("PAYMENT", payment_method);
        intent.putExtra("FROM", fromName);
        intent.putExtra("FROM_ADD", fromAdd);
        intent.putExtra("DESTINATION", destinationName);
        intent.putExtra("DESTINATION_ADD", destinationAdd);
        intent.putExtra("DISTANCE", distance);
        intent.putExtra("HAS_PROMO", hasPromo);
        intent.putExtra("HAS_SURGE", hasSurge);
        intent.putExtra("HAS_CAMPAIGN", hasCampaign);
        intent.putExtra("NOTES", notes);
        intent.putExtra("REJECT_BTN", rejectBtn);
        intent.putExtra("ACCEPT_BTN", acceptBtn);
        intent.putExtra("INCENTIVE", incentive);

        getReactApplicationContext().sendBroadcast(intent);

      } else {
        pagerIntent.putExtra("BOOKING_ID", booking_id);
        pagerIntent.putExtra("BOOKING_CODE", booking_code);
        pagerIntent.putExtra("GROUP_NAME", groupName);
        pagerIntent.putExtra("FARE", fare);
        pagerIntent.putExtra("BOOKING_TIME", bookingTime);
        pagerIntent.putExtra("TYPE", type);
        pagerIntent.putExtra("PAYMENT", payment_method);
        pagerIntent.putExtra("FROM", fromName);
        pagerIntent.putExtra("FROM_ADD", fromAdd);
        pagerIntent.putExtra("DESTINATION", destinationName);
        pagerIntent.putExtra("DESTINATION_ADD", destinationAdd);
        pagerIntent.putExtra("DISTANCE", distance);
        pagerIntent.putExtra("HAS_PROMO", hasPromo);
        pagerIntent.putExtra("HAS_SURGE", hasSurge);
        pagerIntent.putExtra("HAS_CAMPAIGN", hasCampaign);
        pagerIntent.putExtra("NOTES", notes);
        pagerIntent.putExtra("REJECT_BTN", rejectBtn);
        pagerIntent.putExtra("ACCEPT_BTN", acceptBtn);
        pagerIntent.putExtra("INCENTIVE", incentive);

        pagerIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(pagerIntent);

        getCurrentActivity().overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
      }

      SharedPreferences.Editor editor = sharedPref.edit();
      editor.putBoolean("running", true);
      editor.commit();

      promise.resolve(SUCCESS);
    } else {
      promise.reject(E_ACTIVITY_DOES_NOT_EXIST, "No Activity");
    }
  }

  @ReactMethod
  public void removeBookingView(String booking_id, String msg) {
    Intent intent = new Intent("removeBookingView");
    intent.putExtra("BOOKING_ID", booking_id);

    getReactApplicationContext().sendBroadcast(intent);

    Toast toast = Toast.makeText(getReactApplicationContext(), msg, Toast.LENGTH_LONG);
    toast.show();
  }

  @ReactMethod
  public void notifyUser(
          String booking_id,
          String booking_code,
          String groupName,
          String fare,
          String bookingTime,
          String type,
          String payment_method,
          String fromName,
          String fromAdd,
          String destinationName,
          String destinationAdd,
          String distance,
          String hasPromo,
          String hasSurge,
          String hasCampaign,
          String notes,
          String rejectBtn,
          String acceptBtn,
          String incentive,
          Promise promise
  ) {
//    this.booking_id = booking_id;
    if(getCurrentActivity() != null) {
      Intent intent = new Intent(getCurrentActivity(), DialogActivity.class);
      intent.putExtra("BOOKING_ID", booking_id);
      intent.putExtra("BOOKING_CODE", booking_code);
      intent.putExtra("GROUP_NAME", groupName);
      intent.putExtra("FARE", fare);
      intent.putExtra("BOOKING_TIME", bookingTime);
      intent.putExtra("TYPE", type);
      intent.putExtra("PAYMENT", payment_method);
      intent.putExtra("FROM", fromName);
      intent.putExtra("FROM_ADD", fromAdd);
      intent.putExtra("DESTINATION", destinationName);
      intent.putExtra("DESTINATION_ADD", destinationAdd);
      intent.putExtra("DISTANCE", distance);
      intent.putExtra("HAS_PROMO", hasPromo);
      intent.putExtra("HAS_SURGE", hasSurge);
      intent.putExtra("HAS_CAMPAIGN", hasCampaign);
      intent.putExtra("NOTES", notes);
      intent.putExtra("REJECT_BTN", rejectBtn);
      intent.putExtra("ACCEPT_BTN", acceptBtn);
      intent.putExtra("INCENTIVE", incentive);
      intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
      getReactApplicationContext().startActivity(intent);
      getCurrentActivity().overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
      promise.resolve(SUCCESS);
    } else {
      promise.reject(E_ACTIVITY_DOES_NOT_EXIST, "No Activity");
    }
  }

  @ReactMethod
  public void dismissActivity(String msg) {
    Intent userAction = new Intent("android.intent.action.POPUP_DISMISS");
    Context context = getReactApplicationContext();
    context.sendBroadcast(userAction);

    Toast toast = Toast.makeText(getReactApplicationContext(), msg, Toast.LENGTH_LONG);
    toast.show();
  }

  @ReactMethod
  public void launchApp(Promise promise) {
    Intent intent = new Intent("android.intent.category.LAUNCHER");
    intent.setClassName("com.stage.dacsee", "com.stage.dacsee.MainActivity");
    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    getReactApplicationContext().startActivity(intent);
    promise.resolve(SUCCESS);
  }

  @ReactMethod
  public void reactStoresHydrated() {
    storesHydrated = true;
    synchronized (this){
      notify();
    }
  }

  @ReactMethod
  public void reactUILoaded() {
    reactUILoaded = true;
  }

  private static class AppHydrated extends AsyncTask<String, Void, Map<String, String>> {

    private WeakReference<PopupService> activityReference;

    AppHydrated(PopupService context) {
      activityReference = new WeakReference<>(context);
    }

    @Override
    protected Map<String, String> doInBackground(String... params) {
      PopupService activity = activityReference.get();
      Boolean ready = false;
      Date executionDate = new Date();
      while (!ready) {
        Date now = new Date();
        Long lastExecutionSecondsAgo = (now.getTime()-executionDate.getTime())/1000;
        if (lastExecutionSecondsAgo > 5) {
          executionDate = new Date();
          if (activity.storesHydrated && activity.reactUILoaded) {
            ready = true;
          }
        }
      }

      Map<String, String> result = new HashMap<>();
      result.put("booking_id", params[0]);
      result.put("booking_code", params[1]);
      result.put("groupName", params[2]);
      result.put("fare", params[3]);
      result.put("bookingTime", params[4]);
      result.put("type", params[5]);
      result.put("payment_method", params[6]);
      result.put("fromName", params[7]);
      result.put("fromAdd", params[8]);
      result.put("destinationName", params[9]);
      result.put("destinationAdd", params[10]);
      result.put("distance", params[11]);
      result.put("hasPromo", params[12]);
      result.put("hasSurge", params[13]);
      result.put("hasCampaign", params[14]);
      result.put("notes", params[15]);
      result.put("rejectBtn", params[16]);
      result.put("acceptBtn", params[17]);
      result.put("incentive", params[18]);

      return result;
    }

    @Override
    protected void onPostExecute(Map<String, String> result) {
      PopupService activity = activityReference.get();
      if (activity == null) return;

    }
  }
}