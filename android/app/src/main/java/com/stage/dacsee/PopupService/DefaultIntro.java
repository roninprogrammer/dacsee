package com.stage.dacsee.PopupService;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.stage.dacsee.PopupService.SampleSlide;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntroFragment;
import com.github.paolorotolo.appintro.model.SliderPage;
import com.stage.dacsee.R;

/**
 * Created by andrew on 11/17/16.
 */

public class DefaultIntro extends AppIntro {
    BroadcastReceiver mReceiver;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        IntentFilter intentFilter = new IntentFilter("AddBooking");

        mReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.i("DefaultIntro", "AddBooking Listener");

                Bundle params = new Bundle();
                params.putString("FROM", "PICKUP LISTENER");
                addSlide(SampleSlide.newInstance(R.layout.activity_dialog, params));
            }
        };

        getApplicationContext().registerReceiver(mReceiver, intentFilter);


        Log.i("DefaultIntro", "NEW WINDOW");
        // Sample Slide
        Bundle args = new Bundle();
        args.putString("FROM", "PICKUP 1");
        addSlide(SampleSlide.newInstance(R.layout.activity_dialog, args));


        // Optional
        showSkipButton(false);
        setProgressButtonEnabled(false);
    }

    public void addSlide(@Nullable Bundle booking) {
        booking.putString("FROM", "PICKUP 5");
        addSlide(SampleSlide.newInstance(R.layout.activity_dialog, booking));
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        Bundle args = new Bundle();
        args.putString("FROM", "PICKUP 1");
        addSlide(SampleSlide.newInstance(R.layout.activity_dialog, args));
    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
        finish();
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        finish();
    }

    @Override
    public void onSlideChanged(@Nullable Fragment oldFragment, @Nullable Fragment newFragment) {
        super.onSlideChanged(oldFragment, newFragment);
    }
}
