package com.stage.dacsee.PopupService;

import com.stage.dacsee.R;

import android.app.AlertDialog;
import android.app.KeyguardManager;
import android.app.KeyguardManager.KeyguardDismissCallback;
import android.view.Window;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.BroadcastReceiver;
import android.os.Handler;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.text.TextUtils;
import android.media.MediaPlayer;
import android.media.AudioManager;

import java.util.Random;

import androidx.appcompat.app.AppCompatActivity;

import android.widget.TextView;

public class DialogActivity extends AppCompatActivity {
    String booking_id = "";
    String booking_code = "";
    String groupName = "";
    String fare = "";
    String bookingTime = "";
    String type = "";
    String payment_method = "";
    String fromName = "";
    String fromAdd = "";
    String destinationName = "";
    String destinationAdd = "";
    String distance = "";
    String hasPromo = "";
    String hasSurge = "";
    String hasCampaign = "";
    String notes = "";
    String rejectBtn = "";
    String acceptBtn = "";

    final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            finish();
        }
    };

    static boolean alreadyListening = false;

    Handler handlerFinish = new Handler();

    Runnable runnableFinish = new Runnable() {
        public void run() {
            finish();
        }
    };

    private MediaPlayer bookingMP;

    private AudioManager mAudioManager;
    private Random mRandom = new Random();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);

        IntentFilter intentFilter = new IntentFilter(
                "android.intent.action.POPUP_DISMISS");

        registerReceiver(mReceiver, intentFilter);

        // Solution #1
//        PowerManager powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
//        WakeLock wakeLock = powerManager.newWakeLock((PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP), "TAG");
//        wakeLock.acquire();

        // Solution #2
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
            | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
            | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
            | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
            | WindowManager.LayoutParams.FLAG_ALLOW_LOCK_WHILE_SCREEN_ON
        );

        setContentView(R.layout.activity_dialog);

        stopPlaying();
        mAudioManager = (AudioManager) getSystemService(AUDIO_SERVICE);

        int media_max_volume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);

        int volume = Math.round(media_max_volume);

        mAudioManager.setStreamVolume(
                AudioManager.STREAM_MUSIC, // Stream type
                media_max_volume - 7, // Index
                0 // Flags
        );

        bookingMP = MediaPlayer.create(getApplicationContext(), R.raw.new_booking);
        bookingMP.setLooping(false);
        bookingMP.start();

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            booking_code = extras.getString("BOOKING_CODE");
            TextView tBOOKING_CODE = (TextView) findViewById(R.id.BOOKING_CODE);
            tBOOKING_CODE.setText(booking_code);

            groupName = extras.getString("GROUP_NAME");
            TextView tGROUP_NAME = (TextView) findViewById(R.id.GROUP_NAME);
            tGROUP_NAME.setText(groupName);

            fare = extras.getString("FARE");
            TextView tFARE = (TextView) findViewById(R.id.FARE);
            tFARE.setText(fare);

            type = extras.getString("TYPE");
            RelativeLayout advanceVisible = (RelativeLayout) findViewById(R.id.ADVANCE_BOOKING);
            advanceVisible.setVisibility(type.equals("advance") ? View.VISIBLE : View.GONE);

            hasPromo = extras.getString("HAS_PROMO");
            TextView promoVisible = (TextView) findViewById(R.id.PROMO_LABEL);
            promoVisible.setVisibility(hasPromo.equals("true") ? View.VISIBLE : View.GONE);

            hasSurge = extras.getString("HAS_SURGE");
            TextView surgeVisible = (TextView) findViewById(R.id.FARE_LABEL);
            surgeVisible.setVisibility(hasSurge.equals("true") ? View.VISIBLE : View.GONE);

            hasCampaign = extras.getString("HAS_CAMPAIGN");
            TextView campaignVisible = (TextView) findViewById(R.id.CAMPAIGN_LABEL);
            campaignVisible.setVisibility(hasCampaign.equals("true") ? View.VISIBLE : View.GONE);

            LinearLayout labelVisible = (LinearLayout) findViewById(R.id.BOOKING_LABEL);
            labelVisible.setVisibility(hasPromo.equals("false") && hasSurge.equals("false") && hasCampaign.equals("false") ? View.GONE : View.VISIBLE);

            notes = extras.getString("NOTES");
            TextView tNOTE = (TextView) findViewById(R.id.NOTE);
            tNOTE.setText(notes);

            LinearLayout noteVisible = (LinearLayout) findViewById(R.id.PASSENGER_NOTE);
            noteVisible.setVisibility(TextUtils.isEmpty(notes) ? View.GONE : View.VISIBLE);

            bookingTime = extras.getString("BOOKING_TIME");
            TextView tBOOKING_TIME = (TextView) findViewById(R.id.BOOKING_TIME);
            tBOOKING_TIME.setText(bookingTime);

            payment_method = extras.getString("PAYMENT");
            TextView tPAYMENT = (TextView) findViewById(R.id.PAYMENT);
            tPAYMENT.setText(payment_method);

            fromName = extras.getString("FROM");
            TextView tFROM = (TextView) findViewById(R.id.FROM);
            tFROM.setText(fromName);

            fromAdd = extras.getString("FROM_ADD");
            TextView tFROM_ADD = (TextView) findViewById(R.id.FROM_ADD);
            tFROM_ADD.setText(fromAdd);

            destinationName = extras.getString("DESTINATION");
            TextView tDESTINATION = (TextView) findViewById(R.id.DESTINATION);
            tDESTINATION.setText(destinationName);

            destinationAdd = extras.getString("DESTINATION_ADD");
            TextView tDESTINATION_ADD = (TextView) findViewById(R.id.DESTINATION_ADD);
            tDESTINATION_ADD.setText(destinationAdd);

            distance = extras.getString("DISTANCE");
            TextView tDISTANCE = (TextView) findViewById(R.id.DISTANCE);
            tDISTANCE.setText(distance);

            rejectBtn = extras.getString("REJECT_BTN");
            Button tREJECT_BTN = (Button) findViewById(R.id.rejectBtn);
            tREJECT_BTN.setText(rejectBtn);

            acceptBtn = extras.getString("ACCEPT_BTN");
            Button tACCEPT_BTN = (Button) findViewById(R.id.acceptBtn);
            tACCEPT_BTN.setText(acceptBtn);
        }

        Button acceptBtn = (Button) findViewById(R.id.acceptBtn);

        acceptBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent userAction = new Intent("android.intent.action.POPUP").putExtra("action", "accept");
                getApplicationContext().sendBroadcast(userAction);

                stopPlaying();

                finish(); // Close Dialog
            }
        });

        Button rejectBtn = (Button) findViewById(R.id.rejectBtn);

        rejectBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent userAction = new Intent("android.intent.action.POPUP").putExtra("action", "reject");
                getApplicationContext().sendBroadcast(userAction);

                stopPlaying();

                finish(); // Close Dialog
            }
        });

        handlerFinish.postDelayed(runnableFinish, 60000);
    }

    protected void stopPlaying() {
        if (bookingMP != null && bookingMP.isPlaying()){
            bookingMP.stop();
            bookingMP.release();
            bookingMP = null;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

//        if (this.isFinishing()) {
//            stopPlaying();
//        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (this.isFinishing()) {
            stopPlaying();
        }

        // Prompt Unlock Phone
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
            | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
            | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
            | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
            | WindowManager.LayoutParams.FLAG_ALLOW_LOCK_WHILE_SCREEN_ON
        );

        stopPlaying();
        mAudioManager = (AudioManager) getSystemService(AUDIO_SERVICE);

        int media_max_volume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);

        int volume = Math.round(media_max_volume);

        mAudioManager.setStreamVolume(
                AudioManager.STREAM_MUSIC, // Stream type
                media_max_volume - 5, // Index
                0 // Flags
        );

        bookingMP = MediaPlayer.create(getApplicationContext(), R.raw.new_booking);
        bookingMP.setLooping(false);
        bookingMP.start();

        Bundle extras = intent.getExtras();
        if(extras != null) {
            booking_code = extras.getString("BOOKING_CODE");
            TextView tBOOKING_CODE = (TextView) findViewById(R.id.BOOKING_CODE);
            tBOOKING_CODE.setText(booking_code);

            groupName = extras.getString("GROUP_NAME");
            TextView tGROUP_NAME = (TextView) findViewById(R.id.GROUP_NAME);
            tGROUP_NAME.setText(groupName);

            fare = extras.getString("FARE");
            TextView tFARE = (TextView) findViewById(R.id.FARE);
            tFARE.setText(fare);

            type = extras.getString("TYPE");
            RelativeLayout advanceVisible = (RelativeLayout) findViewById(R.id.ADVANCE_BOOKING);
            advanceVisible.setVisibility(type.equals("advance") ? View.VISIBLE : View.GONE);

            hasPromo = extras.getString("HAS_PROMO");
            TextView promoVisible = (TextView) findViewById(R.id.PROMO_LABEL);
            promoVisible.setVisibility(hasPromo.equals("true") ? View.VISIBLE : View.GONE);

            hasSurge = extras.getString("HAS_SURGE");
            TextView surgeVisible = (TextView) findViewById(R.id.FARE_LABEL);
            surgeVisible.setVisibility(hasSurge.equals("true") ? View.VISIBLE : View.GONE);

            hasCampaign = extras.getString("HAS_CAMPAIGN");
            TextView campaignVisible = (TextView) findViewById(R.id.CAMPAIGN_LABEL);
            campaignVisible.setVisibility(hasCampaign.equals("true") ? View.VISIBLE : View.GONE);

            LinearLayout labelVisible = (LinearLayout) findViewById(R.id.BOOKING_LABEL);
            labelVisible.setVisibility(hasPromo.equals("false") && hasSurge.equals("false") && hasCampaign.equals("false") ? View.GONE : View.VISIBLE);

            notes = extras.getString("NOTES");
            TextView tNOTE = (TextView) findViewById(R.id.NOTE);
            tNOTE.setText(notes);

            LinearLayout noteVisible = (LinearLayout) findViewById(R.id.PASSENGER_NOTE);
            noteVisible.setVisibility(TextUtils.isEmpty(notes) ? View.GONE : View.VISIBLE);

            bookingTime = extras.getString("BOOKING_TIME");
            TextView tBOOKING_TIME = (TextView) findViewById(R.id.BOOKING_TIME);
            tBOOKING_TIME.setText(bookingTime);

            payment_method = extras.getString("PAYMENT");
            TextView tPAYMENT = (TextView) findViewById(R.id.PAYMENT);
            tPAYMENT.setText(payment_method);

            fromName = extras.getString("FROM");
            TextView tFROM = (TextView) findViewById(R.id.FROM);
            tFROM.setText(fromName);

            fromAdd = extras.getString("FROM_ADD");
            TextView tFROM_ADD = (TextView) findViewById(R.id.FROM_ADD);
            tFROM_ADD.setText(fromAdd);

            destinationName = extras.getString("DESTINATION");
            TextView tDESTINATION = (TextView) findViewById(R.id.DESTINATION);
            tDESTINATION.setText(destinationName);

            destinationAdd = extras.getString("DESTINATION_ADD");
            TextView tDESTINATION_ADD = (TextView) findViewById(R.id.DESTINATION_ADD);
            tDESTINATION_ADD.setText(destinationAdd);

            distance = extras.getString("DISTANCE");
            TextView tDISTANCE = (TextView) findViewById(R.id.DISTANCE);
            tDISTANCE.setText(distance);

            rejectBtn = extras.getString("REJECT_BTN");
            Button tREJECT_BTN = (Button) findViewById(R.id.rejectBtn);
            tREJECT_BTN.setText(rejectBtn);

            acceptBtn = extras.getString("ACCEPT_BTN");
            Button tACCEPT_BTN = (Button) findViewById(R.id.acceptBtn);
            tACCEPT_BTN.setText(acceptBtn);
        }

        handlerFinish.removeCallbacks(runnableFinish);
        handlerFinish.postDelayed(runnableFinish, 60000);
    }
}