package com.stage.dacsee.nativeBridge.LocationService;

import android.content.Intent;

import com.stage.dacsee.KeepAliveService.DriverWorkService;
import com.stage.dacsee.KeepAliveService.GuardianService;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

public class LocationService extends ReactContextBaseJavaModule {

  private boolean active = false;


  public LocationService(ReactApplicationContext reactContext) {
    super(reactContext);
  }

  @Override
  public String getName() {
    return "LocationService";
  }

  @ReactMethod
  public void startTracking(final Callback successCallback) {
    Intent intent = new Intent(getReactApplicationContext(), DriverWorkService.class);
    intent.setAction("android.intent.action.RESPOND_VIA_MESSAGE");
    getReactApplicationContext().startService(intent);

    Thread task = new Thread(new Runnable() {
      @Override
      public void run() {
        try {
          Thread.sleep(500);
        } catch (Exception e) { } finally {
          successCallback.invoke();
        }
      }
    });
    task.start();
  }

  @ReactMethod
  public void setTrackParams(final String authToken, final String vehicleID, final String putUrl, final Callback successCallback) {
    Thread task = new Thread(new Runnable() {
      @Override
      public void run() {
        Intent intent = new Intent();
        intent.setAction("set_value");
        intent.putExtra("authToken", authToken);
        intent.putExtra("vehicleID", vehicleID);
        intent.putExtra("putUrl", putUrl);
        getReactApplicationContext().sendOrderedBroadcast(intent, null);
        successCallback.invoke();
      }
    });
    task.start();
  }


  @ReactMethod
  public void stopTracking(final Callback successCallback) {
    Intent intent = new Intent();
    intent.setAction("kill_self");
    getReactApplicationContext().sendOrderedBroadcast(intent, null);
    successCallback.invoke();

    Intent driverIntent = new Intent(getReactApplicationContext(), DriverWorkService.class);
    driverIntent.setAction("android.intent.action.RESPOND_VIA_MESSAGE");
    getReactApplicationContext().stopService(driverIntent);

    Intent guardianIntent = new Intent(getReactApplicationContext(), GuardianService.class);
    guardianIntent.setAction("android.intent.action.RESPOND_VIA_MESSAGE");
    getReactApplicationContext().stopService(guardianIntent);
  }

}
