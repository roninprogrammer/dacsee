import { AppRegistry } from 'react-native'
import ApplicationCore from './application'
import config from './app.env'
import $ from './application/global'
import BackgroundLocationService from './application/global/service/backgroundLocation'

global['$'] = $
global['$'].config = config

console.ignoredYellowBox = [ 'Warning: isMounted(...)' ]

AppRegistry.registerComponent('dacsee', () => ApplicationCore)

BackgroundLocationService.registerHeadlessTask()