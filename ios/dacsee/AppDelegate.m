/**
 * Copyright (c) 2015-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

#import "AppDelegate.h"
//#import <AppCenterReactNativeCrashes/AppCenterReactNativeCrashes.h>
//#import <AppCenterReactNativeAnalytics/AppCenterReactNativeAnalytics.h>
#import <CodePush/CodePush.h>

#import <React/RCTBundleURLProvider.h>
#import <React/RCTRootView.h>
// #import <React/RCTPushNotificationManager.h>  Deprecated. Use @react-native-community/push-notification-ios instead.
#import <RNCPushNotificationIOS.h>
#import <React/RCTLinkingManager.h>

//#import <MAMapKit/MAMapKit.h>
//#import <AMapFoundationKit/AMapFoundationKit.h>

//#import <Bugly/Bugly.h>
#import <RCTSplashScreen/RCTSplashScreen.h>

#import "RNUMConfigure.h"
//#import <UMShare/UMSocialManager.h>
#import <GoogleMaps/GoogleMaps.h>
#import <Flurry-iOS-SDK/Flurry.h>
// crashlytics related imports
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <asl.h>
#import "RCTLog.h"
// #############################################
@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
  // #######################################################
//  [AppCenterReactNativeCrashes registerWithAutomaticProcessing];  // Initialize AppCenter crashes
//  [AppCenterReactNativeAnalytics registerWithInitiallyEnabled:true];  // Initialize AppCenter analytics
//  [AMapServices sharedServices].apiKey = @"b06eec333302b4faf9ae7397e273bc12";
  NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
  NSString *appVersion = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
//  NSString *appBuild = [infoDictionary objectForKey:@"CFBundleVersion"];
  NSString *appBundleId = [infoDictionary objectForKey:@"CFBundleIdentifier"];
  
  NSError *error;
  NSString *bundleVersion = @"";
  // NSDictionary *currentPackageMetadata = [CodePushPackage getCurrentPackage: &error];
  // if (error || !currentPackageMetadata) {
  //   bundleVersion = @"BASE";
  // } else {
  //   bundleVersion = [[currentPackageMetadata objectForKey: @"label"] stringValue];
  // }
  
  BOOL stageEnv = ([appBundleId rangeOfString: @".stage"].location != NSNotFound);
  NSString *appIdent = [NSString stringWithFormat: @"%@_%@_%@", appVersion, bundleVersion, stageEnv ? @"STAGE" : @"STORE"];
  
  
  FlurrySessionBuilder* builder = [[[[[FlurrySessionBuilder new] withLogLevel:FlurryLogLevelAll] withCrashReporting:YES] withSessionContinueSeconds:10] withAppVersion: appIdent];
  [Flurry startSession: stageEnv ? @"T3B337VPKD5HNR56TBRM" : @"BKN5TK3R73SD4C49SBXB" withSessionBuilder:builder];
  
//  [UMConfigure setLogEnabled:YES];
//  [RNUMConfigure initWithAppkey:@"5a7aa3f2a40fa355a700002a" channel:@"App Store"];
  [GMSServices provideAPIKey: @"AIzaSyALLnpXjwuJyfuq884msD20gIGDdYxKdX0"];
  // #######################################################
  
  NSURL *jsCodeLocation;

  #ifdef DEBUG
    jsCodeLocation = [[RCTBundleURLProvider sharedSettings] jsBundleURLForBundleRoot:@"index" fallbackResource:nil];
  #else
    jsCodeLocation = [CodePush bundleURL];
  #endif


  // RCTBridge *bridge = [[RCTBridge alloc] initWithDelegate:self launchOptions:launchOptions];
  // RCTRootView *rootView = [[RCTRootView alloc] initWithBridge:bridge
  //                                                  moduleName:@"dacsee"
  //                                           initialProperties:nil];

  // rootView.backgroundColor = [[UIColor alloc] initWithRed:1.0f green:1.0f blue:1.0f alpha:1];

  RCTRootView *rootView =  [[RCTRootView alloc] initWithBundleURL:jsCodeLocation
                                                      moduleName:@"dacsee"
                                                initialProperties:nil
                                                      launchOptions:launchOptions];

  rootView.backgroundColor = [[UIColor alloc] initWithRed:1.0f green:1.0f blue:1.0f alpha:1];

  
  [RCTSplashScreen show:rootView];
  
  self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
  UIViewController *rootViewController = [UIViewController new];
  rootViewController.view = rootView;
  self.window.rootViewController = rootViewController;
  [self.window makeKeyAndVisible];
  // ########### crashlytics related ###########
  [Fabric with:@[[Crashlytics class]]];
  RCTSetLogThreshold(RCTLogLevelInfo);
  RCTSetLogFunction(CrashlyticsReactLogFunction);
  // #############################################
  return YES;
}

- (void)registerPushNotificationBeep {
  
}

// - (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
//   //6.3的新的API调用，是为了兼容国外平台(例如:新版facebookSDK,VK等)的调用[如果用6.2的api调用会没有回调],对国内平台没有影响
// //  [[UMSocialManager defaultManager] handleOpenURL:url sourceApplication:sourceApplication annotation:annotation];
//   return [RCTLinkingManager application:application openURL:url sourceApplication:sourceApplication annotation:annotation];
// }

//- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey, id> *)options
//{
//  //6.3的新的API调用，是为了兼容国外平台(例如:新版facebookSDK,VK等)的调用[如果用6.2的api调用会没有回调],对国内平台没有影响
//  BOOL result = [[UMSocialManager defaultManager]  handleOpenURL:url options:options];
//  if (!result) {
//    return [RCTLinkingManager application:app openURL:url
//                        sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
//                              annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
//    
//  }
//  return result;
//}

- (BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void (^)(NSArray * _Nullable))restorationHandler
{
  return [RCTLinkingManager application:application continueUserActivity:userActivity restorationHandler:restorationHandler];
}

// - (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
//   [RCTPushNotificationManager didRegisterUserNotificationSettings:notificationSettings];
// }

// - (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
//   [RCTPushNotificationManager didRegisterForRemoteNotificationsWithDeviceToken:deviceToken];
// }

// - (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
//   [RCTPushNotificationManager didReceiveRemoteNotification:userInfo fetchCompletionHandler:completionHandler];
// }

// - (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
//   [RCTPushNotificationManager didFailToRegisterForRemoteNotificationsWithError:error];
// }

// - (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
//   [RCTPushNotificationManager didReceiveLocalNotification:notification];
// }

// #### Push notification related ####
// Required to register for notifications
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
  [RNCPushNotificationIOS didRegisterUserNotificationSettings:notificationSettings];
}
// Required for the register event.
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
  [RNCPushNotificationIOS didRegisterForRemoteNotificationsWithDeviceToken:deviceToken];
}
// Required for the notification event. You must call the completion handler after handling the remote notification.
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
  [RNCPushNotificationIOS didReceiveRemoteNotification:userInfo fetchCompletionHandler:completionHandler];
}
// Required for the registrationError event.
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
  [RNCPushNotificationIOS didFailToRegisterForRemoteNotificationsWithError:error];
}
// Required for the localNotification event.
- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
  [RNCPushNotificationIOS didReceiveLocalNotification:notification];
}



// ########### crashlytics related ###########
RCTLogFunction CrashlyticsReactLogFunction = ^(
                                              RCTLogLevel level,
                                              __unused RCTLogSource source,
                                              NSString *fileName,
                                              NSNumber *lineNumber,
                                              NSString *message
                                              )
{
  NSString *log = RCTFormatLog([NSDate date], level, fileName, lineNumber, message);
  
//#ifdef DEBUG
//  fprintf(stderr, "%s\n", log.UTF8String);
//  fflush(stderr);
//#else
//  CLS_LOG(@"REACT LOG: %s", log.UTF8String);
//#endif
  CLS_LOG(@"REACT LOG: %s", log.UTF8String);
  
  int aslLevel;
  switch(level) {
    case RCTLogLevelTrace:
      aslLevel = ASL_LEVEL_DEBUG;
      break;
    case RCTLogLevelInfo:
      aslLevel = ASL_LEVEL_NOTICE;
      break;
    case RCTLogLevelWarning:
      aslLevel = ASL_LEVEL_WARNING;
      break;
    case RCTLogLevelError:
      aslLevel = ASL_LEVEL_ERR;
      break;
    case RCTLogLevelFatal:
      aslLevel = ASL_LEVEL_CRIT;
      break;
  }
  asl_log(NULL, NULL, aslLevel, "%s", message.UTF8String);
  
  
};
  // #############################################
@end
