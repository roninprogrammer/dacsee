import { Platform } from 'react-native'
import device from 'react-native-device-info'

const PACKAGE_NAME = device.getBundleId()

const config = {
  region: PACKAGE_NAME.indexOf('.cn.') >= 0 ? 'cn' : 'global',
  staging: PACKAGE_NAME.indexOf('.stage.') !== -1,
  env: {
    global: {
      senderID: {
        staging: '1015649092920',
        production: '1015649092920'
      },
      google_key: {
        staging: 'AIzaSyC68_1yUASOLY7OVPVb9r3II-Q8SG7Qbso',
        production: 'AIzaSyDxPYr1yrx1GF5-UqDdDdyWL3hvMVmiW8s'
      },
      deployment_key: Platform.select({
        ios: {
          staging: '5PcepJNSyucJUJld0v-Asdvs8MNDH1MeqVDVz',
          production: 'xnBqrpY4wmN1izwRxCb1dlX5dn6_Sy2xqVDVM'
        },
        android: {
          staging: '50X6Bx8Z_NoVXPtj3D8FC-2sG80-BkF6K4PNz',
          production: '5Tk5dUDPQF9qA1AN_EzpcSvKmw3oB14AF4w4M'
        }
      }),
      groupInviteLink: {
        staging: 'http://clientstaging.incredible-qr.com/clients/dacsee/dacsee-microsite/#/community-landing',
        production: 'https://dacsee.com/#/community-landing'
      },
      storage: {
        staging: 'https://storage.googleapis.com/dacsee-dev-service-circle/',
        production: 'https://storage.googleapis.com/dacsee-service-circle/'
      },
      server: {
        staging: {
          User: 'https://svc-stage-user.dacsee.io/api/',
          Campaign: 'https://svc-stage-booking.dacsee.io/api/',
          Circle: 'https://svc-stage-circle.dacsee.io/api/',
          Booking: 'https://svc-stage-booking.dacsee.io/api/',
          Driver: 'https://svc-stage-dv.dacsee.io/api/',
          Location: 'https://svc-stage-location.dacsee.io/api/',
          Push: 'https://svc-stage-push.dacsee.io/api/',
          Scheduler: 'https://svc-stage-scheduler.dacsee.io/api/',
          Rating: 'https://svc-stage-rating.dacsee.io/api/',
          Lookup: 'https://svc-stage-lookup.dacsee.io/api/',
          Lookup_CN: 'https://svc-stage-lookup-cn.dacsee.io/api/',
          Wallet: 'https://svc-stage-wallet.dacsee.io/api/',
          payment: 'https://svc-stage-payment.dacsee.io/api/',
          Chat: 'http://svc-stage-chat.dacsee.io:8080',
          Joy: 'https://svc-stage-joy.dacsee.io/api/',
          Inbox: 'https://svc-stage-inbox.dacsee.io/api/',
          Log: 'https://svc-stage-log.dacsee.io/api/'
        },
        production: {
          User: 'https://svc-prod-user.dacsee.io/api/',
          Campaign: 'https://svc-prod-booking.dacsee.io/api/',
          Circle: 'https://svc-prod-circle.dacsee.io/api/',
          Booking: 'https://svc-prod-booking.dacsee.io/api/',
          Driver: 'https://svc-prod-dv.dacsee.io/api/',
          Location: 'https://svc-prod-location.dacsee.io/api/',
          Push: 'https://svc-prod-push.dacsee.io/api/',
          Scheduler: 'https://svc-prod-scheduler.dacsee.io/api/',
          Rating: 'https://svc-prod-rating.dacsee.io/api/',
          Lookup: 'https://svc-prod-lookup.dacsee.io/api/',
          Lookup_CN: 'https://svc-prod-lookup-cn.dacsee.io/api/',
          Wallet: 'https://svc-prod-wallet.dacsee.io/api/',
          payment: 'https://svc-prod-payment.dacsee.io/api/',
          Chat: 'http://svc-stage-chat.dacsee.io:8080',
          Joy: 'https://svc-prod-joy.dacsee.io/api/',
          Inbox: 'https://svc-prod-inbox.dacsee.io/api/',
          Log: 'https://svc-prod-log.dacsee.io/api/'
        }
      }
    }
  }
}

const format = function () {
  const selectEnv = config.env[config.region]
  return {
    region: config.region,
    staging: config.staging,
    env: {
      senderID: config.staging ? selectEnv.senderID.staging : selectEnv.senderID.production,
      deployment_key: config.staging ? selectEnv.deployment_key.staging : selectEnv.deployment_key.production,
      server: config.staging ? selectEnv.server.staging : selectEnv.server.production,
      groupInviteLink: config.staging ? selectEnv.groupInviteLink.staging : selectEnv.groupInviteLink.production,
      google_key: config.staging ? selectEnv.google_key.staging : selectEnv.google_key.production,
      storage: config.staging ? selectEnv.storage.staging : selectEnv.storage.production
    }
  }
}

export default format()
