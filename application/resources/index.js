export default {
  image: {
    logo_landscape: require('./images/logo-landscape.png'),
    logo_portrait: require('./images/logo-portrait.png'),
    logo_landscape_border: require('./images/logo-landscape-border.png'),
    logo_portrait_border: require('./images/logo-portrait-border.png'),
    logo: require('./images/logo.png'),
    login_logo: require('./images/login_logo.png'),
    // // pin: require('./images/pin.png'),
    security: require('./images/security.png'),
    taxi: require('./images/taxi.png'),
    upgrade: require('./images/upgrade.png'),
    floating_driver: require('./images/driver.png'),
    floating_passenger: require('./images/passenger.png'),
    joblist_car: require('./images/joblist_car.png'),
    joblist_payment: require('./images/joblist_payment.png'),
    joblist_empty: require('./images/joblist_empty.png'),
    booking_detail_bell: require('./images/booking_detail_bell.png'),
    booking_detail_linking: require('./images/booking_detail_linking.png'),
    booking_detail_info: require('./images/booking_detail_info.png'),
    booking_detail_car: require('./images/booking_detail_car.png'),
    booking_detail_payment: require('./images/booking_detail_payment.png'),
    booking_detail_phone: require('./images/booking_detail_phone.png'),
    booking_detail_message: require('./images/booking_detail_message.png'),
    map_from_pin: require('./images/rn_amap_startpoint.png'),
    map_destination_pin: require('./images/rn_amap_endpoint.png'),
    map_car_pin: require('./images/rn_car.png'),
    distance:require ('./images/distance.png'),
    slice_adv_car:require('./images/Slice_Adv_Car.png'),
    car_dafult:require('./images/car_dafult.png'),
    transfer_in:require('./images/arrow_in.png'),
    transfer_out:require('./images/arrow_out.png'),
    // car_budget: require('./images/car_budget.png'),
    // car_premium: require('./images/car_premium.png'),
    // car_rookie: require('./images/car_rookie.png'),
    // car_taxi: require('./images/car_taxi.png'),
    car_xl :require('./images/XL.png'),
    book_page: require('./images/book_page.png'),
    car_vehicle: require ('./images/car_vehicle.png'),
    wallet_bg: require ('./images/walletBg.png'),
    from_addr: require('./images/from.png'),
    to_addr: require('./images/location.png'),
    rate_bg: require('./images/solution.png'),
    sos: require('./images/sos.png'),
    cost: require('./images/cost.png'),
    tag: require('./images/tag.png'),
    //myteamlisting 图片
    share: require('./images/share.png'),
    warning: require('./images/warning.png'),
    close: require('./images/close.png'),
    referrer: require('./images/referrer.png'),
    user: require('./images/user.png'),
    sms: require('./images/sms.png'),
    phone: require('./images/phone.png'),
    chat: require('./images/chat.png'),
    email: require('./images/email.png'),
    ufo: require('./images/ufo.png'),
    whatsapp: require('./images/whatsapp.png'),
    sponsored_location: require('./images/sponsored-icon.png'),
    info_icon: require('./images/info-icon.png'),
    attention_icon: require('./images/attention-icon.png'),
    attention_icon2: require('./images/attention-2.png'),
    select_icon: require('./images/select-icon.png'),
    right_icon: require('./images/right-icon.png'),
    google_icon: require('./images/google.png'),
    joy_placeholder: require('./images/img-placeholder.png'),
    cash_back: require('./images/cashback.png'),
    radio_button_checked: require('./images/radio_button_checked.png'),
    radio_button_unchecked: require('./images/radio_button_unchecked.png'),
    popup_logo: require('./images/popup_logo.jpg'),
    inbox: require('./images/inbox.png'),
    img_placeholder: require('./images/img-placeholder.png'),
    inbox_placeholder: require('./images/placeholder.png'),
    upload: require('./images/upload.png'),
    tick_icon: require('./images/tick.png'),
    verification_complete: require('./images/verification_sent.png'),
    cancel_icon: require('./images/cancel.png'),
    income_icon: require('./images/income-icon.png'),
    rating_icon: require('./images/rating.png'),
    miss_icon: require('./images/miss.png'),
    emptystate_no_special_driver: require('./images/emptystate-no-special-driver.png'),
    compact_detail: require('./images/compact-detail.png'),
    comfort_detail: require('./images/comfort-detail.png'),
    exe_detail: require('./images/exe-detail.png'),
    xl_detail: require('./images/xl-detail.png'),
    official_group_icon: require('./images/official-group-icon.png'),
    private_group_icon: require('./images/private-group-icon.png'),
    public_group_icon: require('./images/public-group-icon.png'),
    collapse_icon: require('./images/colapse-icon.png'),
    expand_icon: require('./images/expand-icon.png'),
    navigate_icon: require('./images/navigation.png'),
    driver_note_icon: require('./images/note.png'),
    booking_time_icon: require('./images/time.png'),
    payment_type_icon: require('./images/wallet.png'),
    back_icon: require('./images/back-icon.png'),
    system_cause_error: require('./images/system_cause_error.png'),
    user_cause_error: require('./images/user_cause_error.png'),
    placeholder_car_front: require('./images/placeholder-car-front.jpg'),
    placeholder_car_back: require('./images/placeholder-car-back.jpg'),
    backspace: require('./images/backspace.png'),
    edit: require('./images/edit.png')
  },

  animation: {
    round_cap_loading: require('./animation/round_cap_material_loading.json'),
    LineProgressLoading: require('./animation/LineProgressLoading.json'),

    simpleLoader: require('./animation/simple_loader.json'),
    loader: require('./animation/loader.json'),
    loader_infinity_black: require('./animation/loader_infinity_black.json'),
    loader_infinity_white: require('./animation/loader_infinity_white.json'),
    loader_world: require('./animation/world_locations.json'),
    loader_fall_ball: require('./animation/loader_animation.json'),

    loader_multi_line: require('./animation/loader_multi_line.json'),
    loader_radar: require('./animation/loader_radar.json'),
    loader_star: require('./animation/loader_star.json'),
    speech_start: require('./animation/voice.json')
  }
}
