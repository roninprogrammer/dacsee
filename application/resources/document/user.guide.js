
const markdown = `
# Privacy Policy

DACSEE Co Ltd, (dacsee.com) value and respect your privacy.
We have created the Privacy and Security Policy to keep you informed how we use the information you have provided and how we protect your personal information.

## Collecting Personal and Technical information

We collect personal information such as your names, addresses, e-mail addresses, phone numbers, or other information, when you onboard or register on our platform (Mobile app & Website). If you wish, you may also visit our website anonymously.
Certain information in our website are collected by using technologies such as cookies and web server logs, to enhance your experience when visiting our website.

## Sharing the Information

We share the information when it is necessary to:

- Create and manage your online account;
- Process your order and payment;
- Personalize your site experience;
- Provide better customer service;
- Forward you the promotional materials;
- Have communications and administration of your participation in contest, special events and/or any other offers;
- Comply with any legal requirements or requests, where applicable.

## We may also share the information with:

- Our affiliates for the purposes described in this Privacy Policy or as otherwise specified in the notice provided at the time of collection.
- Service providers who perform services on our behalf in accordance with our instructions as necessary to perform services on our behalf or comply with the legal requirements and to evaluate use of the website.
- Other third parties with your consent.

## Security

We maintain appropriate technical and management policies to safeguard your personal information and implemented security measures to protect the confidentiality, security and integrity of data stored in our system.

## Terms

By using the dacsee.com (website or mobile app) , you are agreeing to this Privacy and Security Policy. We are not responsible for any breach of privacy statement or other contents on websites operate independently from dacsee.com.

## Payment by Passenger

The Passenger or Customer may choose to pay for the Service by cash and where available, by credit or debit card (“Card”). In the event that the Passenger or Customer chooses to pay for the Service by Card, all payments due to you. DMD technology (The local operator) will hold the amount on behalf of the driver. Payment for the Service will be channeled to you in every withdrawal request. The cut-off date for withdrawal is every Monday and the paid out will be on a weekly basis which is the following Wednesday.

## Update of Our Privacy Policy

We may change or update our Privacy Policy from time to time without prior notice by posting a new version of the policy on our website. Please review frequently the privacy policy page on our website.
`


export {
  markdown
}