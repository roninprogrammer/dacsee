import { NativeModules } from 'react-native'

export default class LocationService {
  static startTracking () {
    return new Promise(resolve => {
      NativeModules.LocationService.startTracking(resolve)
    })
  }

  static setParams (authToken, vehicleId, putUrl) {
    return new Promise(resolve => {
      NativeModules.LocationService.setTrackParams(authToken, vehicleId, putUrl, resolve)
    })
  }

  static stopTracking () {
    return new Promise(resolve => {
      NativeModules.LocationService.stopTracking(resolve)
    })
  }
}
