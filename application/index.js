import React, { Component } from 'react'
import { View, Linking, AppState, StyleSheet, Text, Platform, TextInput, AsyncStorage } from 'react-native'
import DeviceEventEmitter from 'RCTDeviceEventEmitter'
import { observer, Provider, inject } from 'mobx-react'
import { create } from 'mobx-persist'

import Navigator from './navigator'
import { Domain } from 'dacsee-store'
import { UpdateModalScreen, IndicatorModalScreen, MessageModalScreen, ReminderModal } from './views/modal'
import BackgroundLocationService from './global/service/backgroundLocation'
import RN_PushNotification from 'react-native-push-notification'
import { PushNotificationService, CrashlyticsService, AnswersSrv } from './global/service'
import { DriverFoundModal } from './views/modal'
import OfflineBar from './global/service/network';


const hydrate = create({ storage: AsyncStorage })



PushNotificationService.configure()



CrashlyticsService.init()

//在正式环境中清空console.log()
if (!__DEV__) {
  global.console = {
    info: () => {
    },
    log: () => {
    },
    warn: () => {
    },
    error: () => {
    },
  }
}
@inject('app', 'account', 'circle', 'chat', 'vehicles', 'jobs', 'bank')
@observer

class LaunchEventHook extends Component {

  constructor(props) {
    super(props)

    this.state = {
      bookingProcessId: null
    }

  }

  async componentDidMount() {
    const { app, account, circle, chat, vehicles, jobs, bank } = this.props
    const promises = [
      hydrate('language', app),
      hydrate('user', account),
      hydrate('authToken', account),
      hydrate('friends', circle.friendData),
      hydrate('chatList', chat),
      hydrate('msgList', chat),
      hydrate('vehiclesData', vehicles),
      hydrate('working', jobs),
      hydrate('bankList', bank)
    ];

    await Promise.all(promises);

    app.run()

    await $.store.jobs.syncServerWorkingStatus()

    PushNotificationService.setCallbacks(
      $.store.app.onPushRegister,
      $.store.app.onPushNotification
    )

    RN_PushNotification.popInitialNotification(notification => {
      if (notification) PushNotificationService.onNotification(notification);
    })

    AppState.addEventListener('change', this.handleAppStateChange)

    BackgroundLocationService.init()

    this.listener = DeviceEventEmitter.addListener('NewBooking', (params) => {
      if (this.bookingProcessId !== params.booking_id) {
        this.bookingProcessId = params.booking_id // To avoid trigger twice from native dialog (new incoming job card)
        if ($.store.app.booking_sound_new) $.store.app.booking_sound_new.stop()

        $.store.jobs.bookingHandle({ action: params.action }, params.booking_id)
      }

    })

    
  }



  componentWillUnmount() {
    Linking.removeEventListener('url', this.props.app.handleOpenURL)
    AppState.removeEventListener('change', this.handleAppStateChange)

    BackgroundLocationService.removeListeners();

    if (this.listener === undefined || this.listener === null || !('remove' in this.listener)) return
    this.listener.remove();

    $.store.jobs.workingChange(false)
  }

  getActiveRouteName(navigationState) {
    if (!navigationState) return null

    const route = navigationState.routes[navigationState.index]
    
    if (route.routes) return this.getActiveRouteName(route)

    return route.routeName;
  }

  render() {
    return (
      <Navigator 
        onNavigationStateChange={(prevState, currentState) => {
          const currentScreen = this.getActiveRouteName(currentState);
          const prevScreen = this.getActiveRouteName(prevState);
    
          if (prevScreen !== currentScreen) {
            AnswersSrv.logContentView(currentScreen)
          }
        }}
        ref={navigatorRef => $.navigator = navigatorRef} 
      />
    )
  }

  handleAppStateChange = async (state) => {
    if (state === 'active') {
      $.store.app.checkAppSetting()
      $.store.jobs.syncServerWorkingStatus()
    }
  }
}
class ApplicationCore extends Component {

  constructor(props) {
    super(props)
  }
  

  componentWillUnmount() {
    // BackgroundGeoLocation.removeEventListener()
  }

  render() {
    if (Platform.OS === 'android') {
      TextInput.defaultProps = { ...(TextInput.defaultProps || {}), allowFontScaling: false };
      const oldRender = Text.render;
      Text.render = function render(...args) {
        const origin = oldRender.call(this, ...args);
        return React.cloneElement(origin, {
          style: [styles.defaultFontFamily, origin.props.style],
          allowFontScaling: false
        });
      };
    }
    if (Platform.OS === 'ios') {
      TextInput.defaultProps = { ...(TextInput.defaultProps || {}), allowFontScaling: false };
      const oldRender = Text.render;
      Text.render = function render(...args) {
        const origin = oldRender.call(this, ...args);
        return React.cloneElement(origin, {
          style: [styles.defaultFontFamily, origin.props.style],
          allowFontScaling: false
        });
      };
    }

    return (
      <Provider {...Domain}>
        <View style={{ flex: 1 }}>
          <OfflineBar />
          <LaunchEventHook />
          <IndicatorModalScreen />
          <MessageModalScreen />
          <UpdateModalScreen />
          <ReminderModal />   
          <DriverFoundModal />  
        </View>
      </Provider>
    )
  }
}

const styles = StyleSheet.create({
  defaultFontFamily: {
    fontFamily: 'Metropolis',

  },
});

export default ApplicationCore
