import React, { Component } from 'react'
import { View } from 'react-native'

import { createSwitchNavigator, createStackNavigator, createDrawerNavigator } from 'react-navigation'

import { MODAL_DEFAULT_OPTIONS, DRAWER_DEFAULT_OPTIONS, STACKNAVIGATOR_DEFAULT_OPTIONS } from './define'
import { StackScreen, AuthScreen } from './route.app'
import Modal from './route.modal'
import NavigatorComponent from '../views'

const AuthNavigator = createStackNavigator({
  ...AuthScreen
}, STACKNAVIGATOR_DEFAULT_OPTIONS)


const AppNavigator = createDrawerNavigator({
  Drawer: {
    screen: createStackNavigator({
      ...StackScreen
    }, STACKNAVIGATOR_DEFAULT_OPTIONS)
  }
}, DRAWER_DEFAULT_OPTIONS)

const ModalContainerNavigator = createStackNavigator({
  BaseScreen: { screen: AppNavigator },
  ...Modal.ModalScreen
}, MODAL_DEFAULT_OPTIONS)

export default createSwitchNavigator({
  NavigatorComponent: NavigatorComponent,
  App: ModalContainerNavigator,
  Auth: AuthNavigator
}, { initialRouteName: 'NavigatorComponent' })
