/* global store */
import React, { Component } from 'react'
import { ScrollView, StyleSheet, View, Text, Image, TouchableOpacity, Platform } from 'react-native'
import { SafeAreaView, NavigationActions, DrawerActions } from 'react-navigation'
import Material from 'react-native-vector-icons/MaterialIcons'
import { inject, observer } from 'mobx-react'
import { Icons, DCColor } from 'dacsee-utils'

const ICONS_COLOR = '#888'

const MENUS_OPTIONS = [{
  key: '1',
  name: 'inbox_news',
  icon: <Material name={'mail'} size={24} color={ICONS_COLOR} style={{ left: 1.5 }} />,
  onPress: ({ navigation }) => {
    navigation.navigate('InboxListingScreen')
    global.$.navigator.dispatch(DrawerActions.closeDrawer())
  }
}, {
  key: '2',
  name: 'drawer_profile',
  icon: <Material name={'account-circle'} size={24} color={ICONS_COLOR} style={{ left: 1.5 }} />,
  onPress: ({ navigation }) => {
    navigation.navigate('SettingAccount')
    global.$.navigator.dispatch(DrawerActions.closeDrawer())
  }
}, {
  key: '3',
  name: 'mytrip',
  icon: <Material name={'data-usage'} size={24} color={ICONS_COLOR} style={{ left: .5, top: 1 }} />,
  onPress: ({ navigation }) => {
    navigation.navigate('TripList')
    global.$.navigator.dispatch(DrawerActions.closeDrawer())
  }
}, {
  key: '4',
  name: 'my_group',
  icon: <Material name={'people'} size={24} color={ICONS_COLOR} style={{ left: 1.5 }} />,
  onPress: ({ navigation }) => {
    navigation.navigate('MyCircle')
    global.$.navigator.dispatch(DrawerActions.closeDrawer())
  }
}, {
  key: '5',
  name: 'downline',
  icon: <Material name={'people'} size={24} color={ICONS_COLOR} style={{ left: 1.5 }} />,
  onPress: ({ navigation }) => {
    navigation.navigate('DownLineTotalLevel')
    global.$.navigator.dispatch(DrawerActions.closeDrawer())
  }
}, {
  key: '6',
  name: 'wallet',
  icon: <Material name={'account-balance-wallet'} size={24} color={ICONS_COLOR} style={{ left: .5 }} />,
  onPress: ({ navigation }) => {
    navigation.navigate('WalletBalance')
    global.$.navigator.dispatch(DrawerActions.closeDrawer())
  }
}, {
  key: '7',
  name: 'upgradeDriver',
  icon: <Material name={'beenhere'} size={24} color={ICONS_COLOR} style={{ left: 1.5 }} />,
  onPress: ({ navigation }) => {
    $.store.driverVerification.getVerificationRequest('driverRegistration')
    global.$.navigator.dispatch(DrawerActions.closeDrawer())
  }
}, 
{
  key: '8',
  name: 'drivertools',
  icon: <Material name={'beenhere'} size={24} color={ICONS_COLOR} style={{ left: 1.5 }} />
}, 
{
  key: '9',
  name: 'psv_submission',
  icon: <Material name={'beenhere'} size={24} color={ICONS_COLOR} style={{ left: 2.0 }} />,
  onPress: ({navigation}) => {  
    $.store.driverVerification.getVerificationRequest('psvRegistration')
    global.$.navigator.dispatch(DrawerActions.closeDrawer())
  }
},
{
  key: '10',
  name: 'myvehicle',
  icon: <Material name={'directions-car'} size={24} color={ICONS_COLOR} style={{ left: 2.0 }} />,
  onPress: ({ navigation }) => {
    navigation.navigate('VehicleAdd', { type: 'update' })
    global.$.navigator.dispatch(DrawerActions.closeDrawer())
  }
},
{
  key: '11',
  name: 'myjob',
  icon: <Material name={'directions-car'} size={24} color={ICONS_COLOR} style={{ left: 2.0 }} />,
  onPress: ({ navigation }) => {
    navigation.navigate('JobsList')
    global.$.navigator.dispatch(DrawerActions.closeDrawer())
  }
},
{
  key: '12',
  name: 'help',
  icon: <Material name={'help'} size={24} color={ICONS_COLOR} style={{ left: 1.5 }} />,
  onPress: ({ navigation }) => {
    navigation.navigate('HelpScreen', { type: 'update' })
    global.$.navigator.dispatch(DrawerActions.closeDrawer())
  }
}, {
  key: '13',
  name: 'settings',
  icon: <Material name={'settings'} size={24} color={ICONS_COLOR} style={{ left: 1.5 }} />,
  onPress: ({ navigation }) => {
    navigation.navigate('SettingMenu')
    global.$.navigator.dispatch(DrawerActions.closeDrawer())
  }
},
// {
//   key: '14',
//   name: 'update_driver_document',
//   icon: <Material name={'beenhere'} size={24} color={ICONS_COLOR} style={{ left: 1.5 }} />,
//   onPress: ({ navigation }) => {
//     // $.store.driverVerification.getVerificationRequest()
//     navigation.navigate('DriverUpdateDocument')
//     global.$.navigator.dispatch(DrawerActions.closeDrawer())
//   }
// }, 
// {
]
@inject('app', 'account')
@observer
export default class DrawerContentComponent extends Component {

  constructor(props) {
    super(props)
    this.state = {
      verifiedSet: [],
      driverTools: false,
      requestInfo: '',
    }
    this.requestType = props.account.user.driverInfo.requestType;
  }

  async componentDidMount() {
    const { driverInfo } = this.props.account.user
    const { requestType } = driverInfo

    if (requestType === 'driverRegistration') {   //non-verified
      const filterData = MENUS_OPTIONS.filter(({name}) =>    
      name !== 'psv_submission' &&
      name !== 'myvehicle' &&
      name !== 'drivertools' &&
      name !== 'myjob' 
      )
      this.setState({  requestInfo: requestType, verifiedSet : filterData})
      this.requestType = requestType
      return filterData
    }
    else if (requestType === 'psvRegistration') {        //psvregistration
      const filterData = MENUS_OPTIONS.filter(({name}) => 
      name !== 'upgradeDriver' )
      this.setState({ requestInfo: requestType, verifiedSet : filterData})
      this.requestType = requestType
      return filterData
    }
    else {
      const filterData = MENUS_OPTIONS.filter(({name}) =>  //non
      name !== 'upgradeDriver' &&
      name !== 'psv_submission' )
      this.setState({ requestInfo: requestType, verifiedSet : filterData})
      this.requestType = requestType
      return filterData
    }
  }

  async componentDidUpdate(prevProps) {
    if (this.requestType !== prevProps.account.user.driverInfo.requestType) {
      await this.componentDidMount();
    }
  }

  render() {
    const { account = {}, app } = this.props
    const { strings } = app
    const { user, isElite = false, isSuspend = false } = account
    const { verifiedSet } = this.state
    const { fullName, avatars = [], verified = false } = user
    
    return (
      <View style={{ flex: 1 }}>
        <SafeAreaView style={styles.container} forceInset={{ top: 'always', horizontal: 'never' }}>
          {/* Profile */}
          <View style={{ marginBottom: 25 }}>
            {/* TODO: LOAD USER DATA */}
            <TouchableOpacity
              style={{ flexDirection: 'row', justifyContent: 'flex-end', paddingRight: 20, paddingTop: 8 }}
              onPress={() => {
                global.$.navigator.dispatch(NavigationActions.navigate({
                  routeName: 'SettingQrCode', params: {
                    title: strings.my_qr_code,
                    editorName: 'String',
                    option: { userId: user.userId, id: user._id }
                  }
                }))
                global.$.navigator.dispatch(DrawerActions.closeDrawer())
              }}
            >
              {Icons.Generator.Awesome('qrcode', 25, '#bbb')}
            </TouchableOpacity>
            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
              <TouchableOpacity
                activeOpacity={.7}
                onPress={() => {
                  // global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'ProfileChangeAvatar', params: { type: 'user' } }))

                  global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'SettingAccount' }))
                  global.$.navigator.dispatch(DrawerActions.closeDrawer())
                }}
                style={[
                  { backgroundColor: '#eee', overflow: 'hidden', borderColor: '#e8e8e8', borderWidth: 1 },
                  { borderRadius: 50, width: 100, height: 100, justifyContent: 'center', alignItems: 'center' }
                ]}
              >
                <Image style={{ width: 100, height: 100 }} source={{ uri: $.method.utils.getHeaderPicUrl(avatars) }} />
                {/* { Icons.Generator.Material('account-circle', 100, '#fad723') } */}
              </TouchableOpacity>
            </View>
            <Text style={[
              { marginTop: 10, textAlign: 'center' },
              { color: '#333', fontWeight: Platform.select({ ios: '600', android: '400' }), fontSize: 18 }
            ]}>{fullName}</Text>
            <View style={{ flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
              {
                isElite && !isSuspend && (
                  <View style={{ justifyContent: 'center', alignItems: 'center', marginBottom: 5 }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: DCColor.BGColor('primary-1'), borderRadius: 10, paddingHorizontal: 6, paddingVertical: 2 }}>
                      <Text style={{ fontSize: 13, color: DCColor.BGColor('primary-2') }}>Elite Driver</Text>
                    </View>
                  </View>
                )
              }
              {
                verified && !isSuspend && (
                  <View style={{ justifyContent: 'center', alignItems: 'center', marginBottom: 5  }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: DCColor.BGColor('primary-1'), borderRadius: 10, paddingHorizontal: 6, paddingVertical: 2 }}>
                      <Text style={{ fontSize: 13, color: DCColor.BGColor('primary-2') }}>Verified Driver</Text>
                    </View>
                  </View>
                )
              }
              {
                isSuspend && (
                  <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: 'red', borderRadius: 10, paddingHorizontal: 6, paddingVertical: 2 }}>
                      <Text style={{ fontSize: 13, color: 'white' }}>Suspended Driver</Text>
                    </View>
                  </View>
                )
              }
            </View>
          </View>

          <View style={{ height: 1, backgroundColor: '#f1f1f1', marginHorizontal: 27, marginBottom: 12, borderRadius: 2 }} />
          {/* Menu */}
          <ScrollView style={{ paddingHorizontal: 25 }} containerStyle={{ flex: 1 }}>
            { 
              verifiedSet.map(pipe => (
                <TouchableOpacity
                  onPress={pipe.onPress ? pipe.onPress.bind(this, this.props) : () => { }}
                  key={pipe.key}
                  style={pipe.name === 'psv_submission' || pipe.name === 'myvehicle' || pipe.name === 'myjob' ? { height: 44, flexDirection: 'row', justifyContent: 'space-between', marginLeft: 40 }: { height: 44, flexDirection: 'row', justifyContent: 'space-between'}}
                >
                  <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                    <View style={{ marginRight: 10 }}>{pipe.icon}</View>
                    <Text style={pipe.name === 'psv_submission' ? { fontSize: 14, fontWeight: Platform.select({ ios: '600', android: '400' }), color: 'red' }: { fontSize: 14, fontWeight: Platform.select({ ios: '600', android: '400' }), color: '#333' }}>
                      {app.strings[pipe.name]}
                    </Text>
                  </View>

                  {pipe.name == 'inbox_news' && $.store.inbox.unreadInbox.length > 0 ?
                    <View style={{ flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center', backgroundColor: DCColor.BGColor('red'), paddingHorizontal: 10, borderRadius: 10, height: 22, alignSelf: 'center' }}>
                      <Text style={{ color: 'white' }}>{$.store.inbox.unreadInbox.length}</Text>
                    </View>
                    :
                    <View></View>
                  }
                  
                </TouchableOpacity>
              ))
            }
          </ScrollView>
        </SafeAreaView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: { flex: 1 }
})
