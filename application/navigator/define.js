
import React, { Component } from 'react'
import { TouchableOpacity, Platform } from 'react-native'
import { View } from 'react-navigation'
import Material from 'react-native-vector-icons/MaterialIcons'
import { DCColor } from 'dacsee-utils';

const _HEADER_BACK_BUTTON = (navigation) => (
  <TouchableOpacity 
    activeOpacity={0.7} 
    style={{ top: 1, width: 54, paddingLeft: 8, justifyContent: 'center', alignItems: 'flex-start' }} 
    onPress={() => navigation.goBack()}
  >
    <Material name={'keyboard-arrow-left'} size={30} color={DCColor.BGColor('primary-2')} />
  </TouchableOpacity>
)

const STACKNAVIGATOR_DEFAULT_OPTIONS = {
  navigationOptions: ({ navigation }) => {
    let options = {
      drawerLockMode: 'locked-closed',
      headerStyle: {
        backgroundColor: DCColor.BGColor('primary-1'),
        shadowOffset: { width: 0, height: 1 }, shadowColor: '#000', shadowOpacity: .05,
        borderBottomWidth: 0,
        borderBottomColor: 'transparent',
        elevation: .5
      },
      headerTintColor: DCColor.BGColor('primary-2'),
      headerTitleStyle: Platform.select({
        ios: {
          fontSize: 17,
          fontWeight: '600'
        },
        android: {
          fontSize: 17,
          fontWeight: '600',
          textAlign : 'center',
          flexGrow : 1,
          marginLeft: -40
        }
      }),
      headerBackTitle: null
    }
    if (!('index' in navigation.state)) options = Object.assign(options, {
      headerLeft: _HEADER_BACK_BUTTON(navigation)
    })
    return options
  }
}

import DrawerContent from './component.drawer'

const DRAWER_DEFAULT_OPTIONS = {
  drawerOpenRoute: 'DrawerOpen',
  drawerCloseRoute: 'DrawerClose',
  drawerToggleRoute: 'DrawerToggle',
  initialRouteName: 'Drawer',
  contentComponent: DrawerContent,
  contentOptions: {
    activeTintColor: '#e91e63',
  }
}

const MODAL_DEFAULT_OPTIONS = { 
  mode: 'modal', headerMode: 'none' 
}

const TAB_DEFAULT_OPTIONS = {
  tabBarComponent: CustomTabBar,
  tabBarOptions: {
    showLabel: false
  }
}

class CustomTabBar extends Component {
  render() {
    return (
      <View>
        <View style={{ backgroundColor: 'white' }}>

        </View>
      </View>
    )
  }
}

export {
  STACKNAVIGATOR_DEFAULT_OPTIONS,
  DRAWER_DEFAULT_OPTIONS,
  MODAL_DEFAULT_OPTIONS,
  TAB_DEFAULT_OPTIONS
}