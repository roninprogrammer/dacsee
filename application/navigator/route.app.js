import {
  LoginScreen,
  PickerCountryScreen,
  LoginSelectAccountScreen,
  SocialRegisterScreen
} from '../views/login'

import {
  MainScreen,
  CircleSelectScreen,
  OfficialDriverDetailScreen,
  SpecialDriverDetailScreen,
  CommunityDetailScreen,
  PassengerCompleteScreen,
  BookingRateScreen,
  PassengerFareScreen
} from '../views/main'
import { PickerAddressModal } from '../views/modal'
import {
  MyCircleScreen,
  CircleGroupScreen,
  CircleGroupDetailScreen,
  CircleGroupRoomScreen,
  CircleGroupInviteScreen,
  PickerLoctionComponent,
  CircleGroupAddScreen,
  CircleFriendsAddScreen,
  FriendsRequestScreen,
  FriendsDetailScreen,
  FriendsSearchScreen,
  CommonScanQRCodeScreen,
  ReferrerScanQRCodeScreen,
  FriendsMultipleScreen
} from '../views/circle'
import {
  JobsListScreen,
  JobsOnlineScreen,
  JobsListDetailScreen,
  JobsFareScreen,
  JobsRateScreen
} from '../views/jobs'
import { VehicleScreen } from '../views/vehicle'
import {
  TripListScreen,
  TripListDetailScreen,
  TripAdvanceDetailScreen
} from '../views/trip'

import {
  WalletBalanceScreen,
  WalletBalanceListScreen,
  WalletTransactionListScreen,
  WalletDetailScreen,
  WalletTransferScreen,
  WalletTransferSelectionScreen,
  WalletTransferSummaryScreen,
  // BankListScreen,
  WalletTopUpScreen,
  WalletTopUpDetailScreen,
  WalletWithdrawScreen,
  WalletVerification
} from '../views/wallet'

import {
  SettingMenuScreen,
  SettingAboutScreen,
  SettingAccountScreen,
  SettingMessageNotificationScreen,
  SettingLanguageRegionScreen,
  SettingLanguageChooseScreen,
  SettingFeedbackScreen,
  SettingHelpCenterScreen,
  SettingWetViewScreen,
  SettingPrivateScreen,
  ProfileChangeAvatarScreen,
  SettingQrCodeScreen,
  SettingPinUpdate,
  SettingPinReset,
  // bank
  BankDetailsScreen,
  BankListScreen,
  AddBankAccountScreen,
  //driverInfo
  DriverPersonalInfo,
  VehicleInfo,
  PSVDocumentation,
  EmergencyContact,
  EHailingPermit,
} from '../views/setting'

import {
  DriverVerificationForm,
  DriverImgViewer,
  DriverVerificationSummary,
  DriverUpdateDocument,
  PSVsubmission
} from '../views/driverVerification'

import {
  DownLineListScreen,
  DownLineDetailOne,
  DownLineDetailTwo,
  DownLineDetailThree,
  DownLineTotalScreen,
  DownLineTotalLevelScreen,
} from '../views/downLine'

import {
  FormEditorScreen
} from '../components/form-builder'

import {
  IncomeListScreen
} from '../views/income'

import { ChatScreen, MessageScreen } from '../views/chat'

import {
  SponsorDetailScreen,
  SponsorLocationScreen
} from '../views/main/sponsorLocation'


import {
  JoyUploadReceipt,
  JoyDetailScreen,
  JoyImgViewer
} from '../views/joy'

import {
  InboxListingScreen,
  InboxDetailScreen
} from '../views/inbox'

import {
  HelpScreen
} from '../views/help'

const AuthScreen = {
  Login: LoginScreen,
  SettingWetView: SettingWetViewScreen,
  PickerCountry: PickerCountryScreen,
  LoginSelectAccount: LoginSelectAccountScreen,
  SocialRegister: SocialRegisterScreen,
  ReferrerScanQRCode: ReferrerScanQRCodeScreen
}

const StackScreen = {

  Main: MainScreen,
  CircleSelect: CircleSelectScreen,
  PassengerComplete: PassengerCompleteScreen,
  PassengerRate:BookingRateScreen,
  PassengerFare:PassengerFareScreen,
  MyCircle : MyCircleScreen,
  CircleFriendAdd: CircleFriendsAddScreen,
  FriendsSearchBase: FriendsSearchScreen,
  FriendsRequest: FriendsRequestScreen,
  FriendsDetail: FriendsDetailScreen,
  MultipleFriends: FriendsMultipleScreen,

  PickerAddressModal: PickerAddressModal,

  CircleGroupAdd: CircleGroupAddScreen,
  PickerLoction: PickerLoctionComponent,
  CircleGroupDetail: CircleGroupDetailScreen,
  CircleGroupInvite: CircleGroupInviteScreen,
  CircleGroupRoom: CircleGroupRoomScreen,
  CommonScanQRCode: CommonScanQRCodeScreen,

  JobsRate: JobsRateScreen,
  JobsList: JobsListScreen,
  JobsListDetail: JobsListDetailScreen,
  JobsOnline: JobsOnlineScreen,
  VehicleAdd: VehicleScreen,
  JobsFare: JobsFareScreen,

  TripList: TripListScreen,
  TripListDetail: TripListDetailScreen,
  TripAdvanceDetail: TripAdvanceDetailScreen,
  WalletBalance: WalletBalanceScreen,
  // WalletBalanceList: WalletBalanceListScreen,
  WalletTransaction: WalletTransactionListScreen,
  WalletDetail: WalletDetailScreen,
  WalletTopUp:WalletTopUpScreen,
  WalletTopUpDetail:WalletTopUpDetailScreen,
  WalletTransfer: WalletTransferScreen,
  WalletTransferSelection: WalletTransferSelectionScreen,
  WalletTransferSummary: WalletTransferSummaryScreen,
  BankList: BankListScreen,
  WalletWithdrawScreen,
  WalletVerification,

  // IncomeList: IncomeListScreen,
  InboxListingScreen,
  InboxDetailScreen,

  SettingMenu: SettingMenuScreen,
  SettingAbout: SettingAboutScreen,
  SettingAccount: SettingAccountScreen,
  SettingMessageNotification: SettingMessageNotificationScreen,
  SettingLanguageRegion: SettingLanguageRegionScreen,
  SettingLanguageChoose: SettingLanguageChooseScreen,
  SettingFeedback: SettingFeedbackScreen,
  SettingHelpCenter: SettingHelpCenterScreen,
  SettingPrivate: SettingPrivateScreen,
  SettingWetView: SettingWetViewScreen,
  SettingQrCode: SettingQrCodeScreen,
  SettingPinUpdate,
  SettingPinReset,

  //driver personal info
  DriverPersonalInfo,
  VehicleInfo,
  PSVDocumentation,
  EmergencyContact,
  EHailingPermit,

  DownLineTotalLevel: DownLineTotalLevelScreen,
  DownLineTotal: DownLineTotalScreen,
  DownLineList: DownLineListScreen,
  DownLineDetailOne,
  DownLineDetailTwo,
  DownLineDetailThree,
  ProfileChangeAvatar: ProfileChangeAvatarScreen,

  // UpgradeDriver: UpgradeDriverScreen,

  FormEditor: FormEditorScreen,
  PublicPickerCountry: PickerCountryScreen,
  ChatWindow: ChatScreen,
  ChatMessage: MessageScreen,
  BankDetails: BankDetailsScreen,
  AddBankAccount: AddBankAccountScreen,

  // Sponsor Location 
  SponsorDetailScreen,
  SponsorLocationScreen,

  // Driver Verification
  DriverVerificationForm,
  DriverImgViewer,
  DriverVerificationSummary,
  DriverUpdateDocument,
  PSVsubmission,

  // My Joy
  JoyUploadReceipt,
  JoyDetailScreen,
  JoyImgViewer,

  // Help
  HelpScreen,

  OfficialDriverDetailScreen,
  SpecialDriverDetailScreen,
  CommunityDetailScreen
}

export {
  AuthScreen,
  StackScreen
}
