import { Platform, Dimensions } from 'react-native'
import method from '../../method'
const { width, height } = Dimensions.get('window')
const bar_height=Platform.OS === 'android' ? 54 : method.device.ios.x ? 88 : 64
const tab_height = Platform.select({ ios: method.device.ios.x ? 56 + 22 : 56, android: 56 })
const iphoneX_height = method.device.ios.x ? 22:0
const screen = {
  topbar_height: bar_height,
  width: width,
  height: height,
  safe_height: height - bar_height,
  content_height: height - bar_height - tab_height,
  cell_height: 40,
  btn_height_lg: 60,
  btn_height_md: 50,
  content_horizontal: 20,
  text_margin:5,
  bottom_margin: iphoneX_height + 10 ,
}
export default screen