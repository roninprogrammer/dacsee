const color = {
  topbar_color:'#FFC411',
  water_bule:'#5bc0de',
  deep_blue:'#347ab7',
  simple_red:'#da524e',
  simple_green:'#56d587',
  black: 'black',
  white: 'white',
  blue:'blue',
  red:'red',
  deep_gray: '#373737',
  heavy_gray: '#626262',
  middle_gray: '#8E8E8E',
  simple_gray: '#BABABA',
  light_gray: '#D4D4D4'
  //TODO 
}
export default color