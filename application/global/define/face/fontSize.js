import { PixelRatio, Dimensions } from 'react-native'

const { width, height } = Dimensions.get('window')
const dpr = PixelRatio.get()
const language = 'en'

const TextSize = (size) => {
  if (dpr === 2) {
    // iphone 5s和老安卓机
    if (width < 360) {
      return size * 0.95
    }
    // iphone 5
    if (height < 667) {
      return size
      // iphone 6-6s
    } else if (height >= 667 && height <= 735) {
      return size * 1.05
    }
    // older phablets
    return size * 1.15
  }
  if (dpr === 3) {
    // catch Android font scaling on small machines
    // where pixel ratio / font scale ratio => 3:3
    if (width <= 360) {
      return size
    }
    // Catch other weird android width sizings
    if (dpr < 667) {
      return size * 1.1
      // catch in-between size Androids and scale font up
      // a tad but not too much
    }
    if (height >= 667 && height <= 735) {
      return size * 1.15
    }
    // catch larger devices
    // ie iphone 6s plus / 7 plus / mi note 等等
    return size * 1.20
  }
  if (dpr === 3.5) {
    // catch Android font scaling on small machines
    // where pixel ratio / font scale ratio => 3:3
    if (width <= 360) {
      return size
      // Catch other smaller android height sizings
    }
    if (height < 667) {
      return size * 1.10
      // catch in-between size Androids and scale font up
      // a tad but not too much
    }
    if (height >= 667 && height <= 735) {
      return size * 1.20
    }
    // catch larger phablet devices
    return size * 1.25
  }
  // if older device ie pixelRatio !== 2 || 3 || 3.5
  return size
}

const font_cn = {
  tiny: TextSize(12),
  small: TextSize(13),
  norm: TextSize(14),
  middle: TextSize(16),
  large: TextSize(18),
  huge: TextSize(20)
}
const font_us = {
  tiny: TextSize(13),
  small: TextSize(14),
  norm: TextSize(15),
  middle: TextSize(18),
  large: TextSize(20),
  huge: TextSize(22)
}

const fontSize = language === 'ZH_CN' ? font_cn : font_us
export default fontSize