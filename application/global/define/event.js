
const event = {
  app: {
    launch: 1, // 主应用启动
    recovery_data: 2, // 恢复持久化数据
    component_render: 3,
    run: 4,
    background: -1,
    terminal: -2
  }
}

export default event