import func from './func'
import event from './event'
import error from './error'
import { screen, fontSize, color } from './face'

export default {
  func,
  event,
  error,
  screen, 
  fontSize, 
  color
}