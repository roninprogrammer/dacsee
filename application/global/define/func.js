import { ListView } from 'react-native'
// import Toast from 'react-native-root-toast'
import { toastShort, toastLong } from "../method/ToastUtil";


export default {
  /**
   * 空的CALL结构
   */
  object: () => ({}),
  null: () => null,
  undefined: () => undefined,

  /**
   * 延迟方法
   * @param ms 延迟时间，毫秒
   * @returns {Promise<any> | Promise}
   */
  delay: (ms) => new Promise(resolve => setTimeout(resolve, ms)),

  dataContrast: (rowChangedFunc, sectionChangedFunc = () => {}) => new ListView.DataSource({ rowHasChanged: rowChangedFunc, sectionHeaderHasChanged: sectionChangedFunc }),

  showMessage:(payload) => {
    // const title = typeof(payload) === 'object' ? payload.title : payload
    // const options = typeof(payload) === 'object' ? payload.options : {}
    // Toast.show(title, Object.assign({}, {
    //   duration: Toast.durations.LONG,
    //   position: Toast.positions.CENTER,
    //   shadow: true,
    //   animation: true,
    //   hideOnPress: true,
    // }, options))
    toastShort(payload)
  },
  showMessageLong:(payload) => {
    toastLong(payload)
  }
}