import { NativeModules, Platform, ToastAndroid } from 'react-native'

const RCTToastAndroid = Platform.OS === 'android' ? ToastAndroid : NativeModules.LRDRCTSimpleToast

const Toast = {
  // Toast duration constants
  SHORT: RCTToastAndroid.SHORT,
  LONG: RCTToastAndroid.LONG,

  // Toast gravity constants
  TOP: RCTToastAndroid.TOP,
  BOTTOM: RCTToastAndroid.BOTTOM,
  CENTER: RCTToastAndroid.CENTER,

  show(message, duration) {
    RCTToastAndroid.show(message, duration === undefined ? this.SHORT : duration)
  },

  showWithGravity(message, duration, gravity) {
    RCTToastAndroid.showWithGravity(message, duration === undefined ? this.SHORT : duration, gravity)
  }
}

export default Toast