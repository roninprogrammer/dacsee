import Toasts from './Toast'

/**
 * 冒一个时间比较短的Toast
 * @param content
 */
export const toastShort = content => {
  Toasts.showWithGravity(content ? content.toString() : '网络错误', Toasts.SHORT, Toasts.CENTER)
}

/**
 * 冒一个时间比较久的Toast
 * @param content
 */
export const toastLong = content => {
  Toasts.showWithGravity(content.toString(), Toasts.LONG, Toasts.CENTER)
}