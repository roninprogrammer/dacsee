import { PermissionsAndroid, Platform } from 'react-native'
import session from './session'
import region from './region'
import device from './device'
import utils from './utils'


const DEFINE_SAFA_COORDS = {
  accuracy: 0,
  altitude: 0,
  altitudeAccuracy: 0,
  heading: -1,
  latitude: 0,
  longitude: 0,
  speed: -1
}
const LOCATION_OPTIONS =  Platform.select({
  ios: { timeout: 2000 },
  android: { enableHighAccuracy: false, timeout: 5000, maximumAge: 10000 }
})

const location = {
  /**
   * 检查位置权限
   * @returns {Promise<Promise<any> | Promise>}
   */
  checkPermissions: async () => {
    return new Promise((resolve) => {
      navigator.geolocation.setRNConfiguration({ skipPermissionRequests: true })
      navigator.geolocation.getCurrentPosition(
        () => resolve({ permissions: true }),
        (error) => {
          resolve({ permissions: false, error })
        },
        LOCATION_OPTIONS
      )
      navigator.geolocation.setRNConfiguration({ skipPermissionRequests: false })
    })
  },
  /**
   * 请求GPS访问权限
   * @returns {boolean}
   */
  requestPermission: Platform.select({
    ios: () => {
      navigator.geolocation.requestAuthorization()
      return true
    },
    android: async () => {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
          { title: 'Location Service', message: '' }
        )
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          return true
        } else {
          return false
        }
      } catch (e) {
        console.log(e)
      }
    }
  }),
  /**
   * 获取当前位置
   * @returns {Promise<Promise<any> | Promise>}
   */
  once: async () => {
    return new Promise((resolve) => navigator.geolocation.getCurrentPosition(
      (position) => resolve({success: true, coords: position.coords}),
      () => resolve({success: false, coords: DEFINE_SAFA_COORDS}),
      LOCATION_OPTIONS
    ))
  },
  /**
   * 设置监听事件
   * @param onListener
   */
  onWatch: (onListener, onError = () => {}) => {
    let watchID = null
    if (onListener) {
      const options = { timeout: 5000, enableHighAccuracy: true, distanceFilter: 50 }
      watchID = navigator.geolocation.watchPosition(onListener, onError, options)
    }
    return watchID
  },
  /**
   * 停止监听
   * @param ListenerId
   */
  stopWatch: (watchID) => {
    navigator.geolocation.clearWatch(watchID)
  }
}

export default {
  location,
  region,
  device,
  utils,
  session
}