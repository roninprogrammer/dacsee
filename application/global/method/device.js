import DeviceInfo from 'react-native-device-info'
import { Platform } from 'react-native'
const system = {
  ios: {
    std: (DeviceInfo.getModel().startsWith('iPhone') && !DeviceInfo.getModel().endsWith('Plus')),
    plus: (DeviceInfo.getModel().startsWith('iPhone') && DeviceInfo.getModel().endsWith('Plus')),
    x: DeviceInfo.getModel() === 'iPhone X',
    mini: DeviceInfo.getModel().startsWith('iPad Mini'),
    air: DeviceInfo.getModel().startsWith('iPad Air'),
    pro: DeviceInfo.getModel().startsWith('iPad Pro')
  },
  android: {

  }
}
const device = {
  SystemVersion: DeviceInfo.getSystemVersion(),
  Version: DeviceInfo.getVersion(),
  Build: DeviceInfo.getBuildNumber(),
  UUID: DeviceInfo.getUniqueID(),
  Platform: {
    Name: Platform.OS,
    Android: Platform.OS === 'android',
    iOS: Platform.OS === 'ios'
  },
  Brand: DeviceInfo.getBrand(),
  Name: DeviceInfo.getModel()
}

export default {
  ...device,
  ... system
}
