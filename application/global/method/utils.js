import DeviceInfo from 'react-native-device-info'
import Sound from 'react-native-sound'
import Resources from 'dacsee-resources';
import moment from 'moment'
import Toasts from './Toast'
import {toJS} from 'mobx'
// 正则验证
const rules = {
  isMail: (val) => {
    const reg = /^[A-Za-z\d]+([-_.][A-Za-z\d]+)*@([A-Za-z\d]+[-.])+[A-Za-z\d]{2,5}$/
    return reg.test(val)
  },
  isNumber: (val) => {
    const reg = /^[0-9]*$/
    return reg.test(val)
  }
}

const LoadSound = (name, path = Sound.MAIN_BUNDLE) => new Promise((resolve, reject) => {
  const sound = new Sound(name, path, (error) => {
    if (error) return reject()
    resolve(sound)
  })
})

const getHeaderPicUrl = (avatars, useLarge = false) => {
  let url = 'https://storage.googleapis.com/dacsee-service-user/_shared/default-profile.jpg'
  if (!avatars) avatars = []
  if (avatars.length !== 0) {
    let lastest = avatars[avatars.length - 1]
    url = useLarge ? lastest.url : (lastest.urlSmall ? lastest.urlSmall : lastest.url)
  }
  return url
}

const getPicUrl = (avatars, useLarge = false) => {
  let url = Resources.image.img_placeholder
  if (!avatars) avatars = []
  if (avatars.length !== 0) {
    let lastest = avatars[avatars.length - 1]
    url = useLarge ? lastest.url : (lastest.urlSmall ? lastest.urlSmall : lastest.url)
  }
  return url
}

const getPicSource = (avatars, useLarge = false, placeholder) => {
  let source = !!placeholder ? placeholder : Resources.image.img_placeholder

  if (avatars && avatars.length !== 0) {
    let lastest = avatars[avatars.length - 1]
    let url = useLarge ? lastest.url : (lastest.urlSmall ? lastest.urlSmall : lastest.url)

    source = {uri: url}
  }

  return source
}

const distance = (n1, e1, n2, e2) => {
  let jd = 102834.74258026089
  let wd = 111712.69150641055
  let b = Math.abs((e1 - e2) * jd)
  let a = Math.abs((n1 - n2) * wd)
  return Math.sqrt((a * a + b * b))
}

const distance_v2 = (a, b) => {
  if (!a.lat || !a.lng || !b.lat || !b.lng) return 'n/a'
  const R = 6371 // Radius of the earth in km
  const deg2rad = deg => deg * (Math.PI / 180)
  const dLat = deg2rad(b.lat - a.lat)
  const dLon = deg2rad(b.lng - a.lng)
  const x = (Math.sin(dLat / 2) * Math.sin(dLat / 2)) +
    (
      Math.cos(deg2rad(a.lat)) * Math.cos(deg2rad(b.lat)) *
      Math.sin(dLon / 2) * Math.sin(dLon / 2)
    )
  const c = 2 * Math.atan2(Math.sqrt(x), Math.sqrt(1 - x))
  const d = R * c
  return d
}

const direction = (n1, e1, n2, e2) => {
  let e3 = 0
  let n3 = 0
  e3 = e1 + 0.005
  n3 = n1
  let a = 0
  let b = 0
  let c = 0
  a = distance(e1, n1, e3, n3)
  b = distance(e3, n3, e2, n2)
  c = distance(e1, n1, e2, n2)
  let cosB = 0
  if ((a * c) != 0) {
    cosB = (a * a + c * c - b * b) / (2 * a * c)
  }
  let B = Math.acos(cosB) * 180 / Math.PI

  if (n2 < n1) {
    B = 180 + (180 - B)
  }
  return B
}

const carDirection = (n1, e1, n2, e2) => {
  const DEFINE_ANGLE = 32
  const _direction = direction(n1, e1, n2, e2)
  return parseInt(_direction / DEFINE_ANGLE) + ((_direction % DEFINE_ANGLE) >= (DEFINE_ANGLE / 2) ? 1 : 0) + 1
}

// TODO 按数据格式重新处理
const getCurrentTime = (time = 0, islist = false) => {
  const nowTime = new Date() // 当前日前对象
  const now = new Date().getTime() // 当前毫秒数
  const myyear = nowTime.getFullYear() // 当前年份
  const myday = nowTime.getDay() // 当前星期
  const fullday = 24 * 3600 * 1000 // 一天的毫秒数
  const delay = moment().diff(moment(time).toArray().slice(0, 3), 'days') // 时间差
  const old = new Date(parseInt(time)) // 目标日期对象
  const oldyear = old.getFullYear() // 目标年份
  const oldm = old.getMonth() + 1 // 目标月份
  const oldd = old.getDate() + 1 // 目标日期
  const oldday = old.getDay() // 目标星期
  const oldh = old.getHours() // 目标时
  const oldmin = old.getMinutes() < 10 ? `0${old.getMinutes()}`:old.getMinutes() // 目标分
  // 时间在一天之内只返回时分
  if (delay === 0) {
    return `${oldh}:${oldmin}`
  }
  // 时间在两天之内的
  if (delay === 1) {
    if (islist) {
      return `${$.store.app.strings.yesterday}`
    }
    return `${$.store.app.strings.yesterday} ${oldh}:${oldmin}`
  }

  // 当前星期内
  if (delay > 1 && myday > oldday && delay < 7) {
    let xingqi
    switch (oldday) {
      case 0:xingqi = `${$.store.app.strings.sun}`; break
      case 1:xingqi = `${$.store.app.strings.mon}`; break
      case 2:xingqi = `${$.store.app.strings.tue}`; break
      case 3:xingqi = `${$.store.app.strings.wed}`; break
      case 4:xingqi = `${$.store.app.strings.thu}`; break
      case 5:xingqi = `${$.store.app.strings.fri}`; break
      case 6:xingqi = `${$.store.app.strings.sat}`; break
    }
    if (islist) {
      return `${xingqi}`
    }
    return `${xingqi} ${oldh}:${oldmin}`
  }

  if (delay > 1 && myday === oldday && oldyear === myyear) {
    if (islist) {
      return `${oldm}/${oldd}`
    }
    return `${oldm}/${oldd} ${oldh}:${oldmin}`
  }

  if (delay > 1 && myday === oldday && oldyear < myyear) {
    if (islist) {
      return `${oldyear}/${oldm}/${oldd}`
    }
    return `${oldyear}/${oldm}/${oldd} ${oldh}:${oldmin}`
  }

  if (delay > 1 && myday < oldday && oldyear === myyear) {
    if (islist) {
      return `${oldm}/${oldd}`
    }
    return `${oldm}/${oldd} ${oldh}:${oldmin}`
  }
  if (delay > 1 && myday > oldday && oldyear === myyear && delay > 7) {
    if (islist) {
      return `${oldm}/${oldd}`
    }
    return `${oldm}/${oldd} ${oldh}:${oldmin}`
  }

  if (delay > 1 && myday > oldday && delay >= 7 && oldyear < myyear) {
    if (islist) {
      return `${oldyear}/${oldm}/${oldd}`
    }
    return `${oldyear}/${oldm}/${oldd} ${oldh}:${oldmin}`
  }
  if (delay > 1 && myday < oldday && oldyear < myyear) {
    if (islist) {
      return `${oldyear}/${oldm}/${oldd}`
    }
    return `${oldyear}/${oldm}/${oldd} ${oldh}:${oldmin}`
  }
}
const mathDistanceZoom = (distance) => {
  const km = distance / 1000
  let zoom = 16
  if (km >= 160) { zoom = 8 } else if (km >= 80) { zoom = 9 } else if (km >= 40) { zoom = 10 } else if (km >= 20) { zoom = 11 } else if (km >= 10) { zoom = 12 } else if (km >= 5) { zoom = 13 } else if (km >= 2.5) { zoom = 14 } else { zoom = 15 }

  return zoom
}

const isArray = (list) => {
  list = toJS(list)
  return Array.isArray(list) && list.length
}

const getRegion = (points) => {
  // points should be an array of { latitude: X, longitude: Y }
  let minX, maxX, minY, maxY

  // init first point
  ((point) => {
    minX = point.latitude
    maxX = point.latitude
    minY = point.longitude
    maxY = point.longitude
  })(points[0])

  // calculate rect
  points.map((point) => {
    minX = Math.min(minX, point.latitude)
    maxX = Math.max(maxX, point.latitude)
    minY = Math.min(minY, point.longitude)
    maxY = Math.max(maxY, point.longitude)
  })

  const midX = (minX + maxX) / 2
  const midY = (minY + maxY) / 2
  const deltaX = (maxX - minX)
  const deltaY = (maxY - minY)

  return {
    latitude: midX - 0.01,
    longitude: midY,
    latitudeDelta: deltaX * 4,
    longitudeDelta: deltaY * 4
  }
}

const toastShort = content => {
  Toasts.showWithGravity(content ? content.toString() : '网络错误', Toasts.SHORT, Toasts.CENTER)
}

const toastLong = content => {
  Toasts.showWithGravity(content.toString(), Toasts.LONG, Toasts.CENTER)
}

export default {
  LoadSound,
  getHeaderPicUrl,
  getPicUrl,
  getPicSource,
  rules,
  mathDistanceZoom,
  carDirection,
  getCurrentTime,
  distance,
  direction,
  distance_v2,
  isArray,
  getRegion,
  toastShort,
  toastLong
}
