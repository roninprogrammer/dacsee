const region = {
  select: (obj) => {
    if (!('default' in obj)) for(let k in obj) { obj.default = obj[k]; break }
    if ($.region === 'CHN') return 'chn' in obj ? obj.chn : obj.default
    if ($.region === 'MAS') return 'mas' in obj ? obj.mas : obj.default
    if ($.region === 'JPN') return 'jpn' in obj ? obj.jpn : obj.default
    if ($.region === 'KOR') return 'kor' in obj ? obj.kor : obj.default
    if ($.region === 'HKG') return 'hkg' in obj ? obj.hkg : obj.default
    if ($.region === 'TPE') return 'tpe' in obj ? obj.tpe : obj.default
    return obj.default
  },
  getCoords: (country) => {
    switch(country) {
      case 'MY':
        return { latitude: 3.138161, longitude: 101.691451 }
      default:
        return { latitude: 0, longitude: 0 }
    }
  }
}

export default region