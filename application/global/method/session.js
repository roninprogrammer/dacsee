/* global store */
import axios from 'axios'
import config from '../../../app.env'
const Server = config.env.server

const SESSION_TIMEOUT = 10000

const instance = axios.create({ timeout: SESSION_TIMEOUT })

// REQUEST HANDLE
instance.interceptors.request.use((config) => {
  const authToken = $.store.account.authToken
  config.headers['longitude'] = $.store.app.coords.longitude
  config.headers['latitude'] = $.store.app.coords.latitude

  if (authToken) {
    config.headers.Authorization = authToken
  }

  return config
}, err => {
  return Promise.reject(err)
})

instance.interceptors.response.use((response) => {
  const _response = response || { data: null }

  return _response.data || {}
}, (err) => {
  try {
    if (err.config) {
      const { url = '', method = '' } = err.config
      const { data = {}, status = 0 } = err.response

      if (data.code === "INACTIVE_USER") {
        $.store.account.logout()
        $.define.func.showMessage($.store.app.strings.suspended_user)
        return Promise.reject(err)
      }

      console.log(`[SESSION][${method.toUpperCase()}][${url}][${status}][${data}]`, err.response)
    } else {
      console.log(err)
    }
  } catch (e) {
    console.log(err)
  }
  return Promise.reject(err)
})

const sessionMethodBuild = (baseUrl) => {
  return {
    Post: async (path: string, body = {}) => {
      return await instance.post(`${baseUrl}${path}`, body)
    },

    Get: async (path: string, body = {}, params = {}) => {
      return await instance.get(`${baseUrl}${path}`, Object.assign({}, body, { params }))
    },

    Put: async (path: string, body = {}) => {
      return await instance.put(`${baseUrl}${path}`, body)
    },

    Delete: async (path: string, body = {}) => {
      return await instance.delete(`${baseUrl}${path}`, body)
    },

    Upload: async (path: string, body = {}) => {
      return await instance.post(`${baseUrl}${path}`, body, { timeout: 120000 })
    }
  }
}

export default {

  User: sessionMethodBuild(Server.User),

  Campaign: sessionMethodBuild(Server.Campaign),

  Circle: sessionMethodBuild(Server.Circle),

  Booking: sessionMethodBuild(Server.Booking),

  Driver: sessionMethodBuild(Server.Driver),

  Location: sessionMethodBuild(Server.Location),

  Push: sessionMethodBuild(Server.Push),

  Scheduler: sessionMethodBuild(Server.Scheduler),

  Rating: sessionMethodBuild(Server.Rating),

  GoogleMap: sessionMethodBuild('https://maps.googleapis.com/maps/api/'),

  Lookup: sessionMethodBuild(Server.Lookup),

  Lookup_CN: sessionMethodBuild(Server.Lookup_CN),

  Wallet: sessionMethodBuild(Server.Wallet),
  
  Joy: sessionMethodBuild(Server.Joy),
  
  Bank: sessionMethodBuild(Server.Bank),
  
  Log: sessionMethodBuild(Server.Log),

  Inbox: sessionMethodBuild(Server.Inbox),

}
