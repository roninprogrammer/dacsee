
import define from './define'
import bridge from './bridge'
import method from './method'
import Store, { Domain } from './stores'

export default {
  store: Store, 
  define,
  method,
  bridge
}


export const StoreDomain = Domain