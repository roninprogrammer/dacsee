// @flow

import { observable, action, autorun, toJS } from 'mobx'

import BaseStore from './base.store'
import moment from 'moment'
import Session from '../method/session'
import { persist, create } from 'mobx-persist'
import {NavigationActions} from 'react-navigation'

export default {
  Name: 'vehicles',
  Store: class VehicleStore extends BaseStore {
    constructor (store) {
      super(store)
    }

    @observable loading: Boolean = false
    
    @persist('list') @observable vehiclesData: Array = []
    
    @observable manufacturerData: Array = []
    
    @observable carModelData: Array = []

    @observable carStorege: Array = []
    
    @action.bound async getVehiclesData () {
      try {
        let manufacturerData = await Session.Lookup.Get('v1/lookup/vehicleManufacturers?resultType=nameOnly')
        let vehiclesData = await Session.User.Get('v1/vehicles')
        
        this.vehiclesData = vehiclesData
        this.manufacturerData = manufacturerData
        
        return vehiclesData || []
      } catch (e) {
        console.log(e)
        return []
      }
    }

    @action.bound getCheckVehicle (index: Int) {
      let _clone = this.vehiclesData.slice()
      let vehicle = _clone[0].availableCategories.slice()
      const nextSelect = !vehicle[index].checked
      vehicle[index].checked = nextSelect
      this.vehiclesData = _clone
    }

    @action.bound async fetchCarModel (text = '') {
      try {
        const carModelData = await $.method.session.Lookup.Get(`v1/lookup/vehicleModels?manufacturerName=${text}&resultType=nameOnly`)
        this.carModelData = carModelData
      } catch (e) {
        console.log(e)
      }
    }

    @action.bound async updateVehicleCategory(_id){
      const selected_vehicle_id = []
      let _vehicle = this.vehiclesData[0].availableCategories
      _vehicle = _vehicle.filter(item => item.checked)

      if (_vehicle.length > 0){
        const vehiclesIDS = toJS(_vehicle).map(pipe => pipe._id).join(',')
        selected_vehicle_id.push(vehiclesIDS.split(","))
        const data = {category_ids: selected_vehicle_id[0]}
        const result = await $.method.session.User.Put(`v1/vehicles/${_id}`, data)
        
        return !!result
      } else{
        return '0 selected';
      }
    }

    @action.bound async addOrUpdateVehicle (params) {
      const {
        carNumber,
        manufacturer,
        carModel,
        manufactureYear,
        color,
        front_photo_data,
        back_photo_data,
        back_photo,
        front_photo,
        type,
        update_id
      } = params
      
      if (!back_photo || !front_photo) return $.define.func.showMessage($.store.app.strings.error_photos_empty)
      if (carNumber && manufacturer && carModel && manufactureYear && color && back_photo && front_photo) {
        try {
          this.loading = true
          let postData = {}
          // 审核通过
          if (this.vehiclesData.verified) {
            if (back_photo_data.length > 0) postData.backPhoto = `data:image/jpeg;base64,${back_photo_data}`
            if (front_photo_data.length > 0) postData.frontPhoto = `data:image/jpeg;base64,${front_photo_data}`
          } else {
            // 未通过
            if (type === 'update') {
              if (back_photo_data.length > 0) postData.backPhoto = `data:image/jpeg;base64,${back_photo_data}`
              if (front_photo_data.length > 0) postData.frontPhoto = `data:image/jpeg;base64,${front_photo_data}`
              postData.registrationNo = carNumber
              postData.manufacturer = manufacturer
              postData.model = carModel
              postData.manufactureYear = manufactureYear
              postData.color = color
              postData.country = 'MY'
            } else {
              postData = {
                registrationNo: carNumber,
                manufacturer: manufacturer,
                model: carModel,
                manufactureYear: manufactureYear,
                color,
                frontPhoto: `data:image/jpeg;base64,${front_photo_data}`,
                backPhoto: `data:image/jpeg;base64,${back_photo_data}`,
                country: 'MY'
              }
            }
          }

          let vehicleData = type === 'update'
            ? await $.method.session.User.Put(`v1/vehicles/${update_id}`, {...postData})
            : await $.method.session.User.Post('v1/vehicles', {...postData})
          this.vehiclesData[0] = vehicleData
          
          global.$.navigator.dispatch(NavigationActions.back())
        
        } catch (e) {
          this.loading = false
          let msg = $.store.app.strings.error_try_again
          if (e && e.response && e.response.data && e.response.data.message) {
            if (e.response.data.message === 'Cannot edit verified vehicle') {
              msg = $.store.app.strings.vehicle_updated_msg
            } else {
              msg = e.response.data.message
            }
            $.define.func.showMessage(msg)
          }
        }
      } else {
        $.define.func.showMessage($.store.app.strings.error_params_empty)
      }
    }
  },
  Autorun: (store) => {
    const autorunVehicle = async (authToken) => {
      if (authToken && authToken.length > 0) store.vehicles.getVehiclesData()
    }

    autorun(() => autorunVehicle(store.account.authToken))
  }
}
