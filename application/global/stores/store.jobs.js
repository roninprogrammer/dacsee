import { Platform, Alert, NativeModules } from 'react-native'
import { observable, action, autorun, computed } from 'mobx'
import { persist } from 'mobx-persist'
import Session from '../method/session'
import Device from '../method/device'
import AliveService from '../../native/location-service'
import BaseStore from './base.store'
import { NavigationActions } from 'react-navigation'
import moment from 'moment'

import config from '../../../app.env'
import BackgroundLocationService from '../service/backgroundLocation'
import PushNotificationService from '../service/pushNotification'
import LocationSwitch from 'react-native-location-switch';

export default {
  Name: 'jobs',
  Store: class JobStore extends BaseStore {
    constructor (store) {
      super(store)
    }

    @observable loading: Boolean = false

    @observable jobList: Array = []

    @observable working: Boolean = false

    @observable bookingPath: Object = {}

    @observable await_jobs: Array = []

    @observable active_job: Object = {}

    @observable advanceBooking: Array = []

    @observable busy: Boolean = false

    @observable showOnlineVerify: Boolean = false

    @observable blockReasons: Array = []

    @observable driverDashboard: Object = {}

    @observable locationRequested: Boolean = false

    @observable cancelModalVisible: Boolean = false

    @observable recentCancellationPct: String = ''

    @observable timer: null

    @observable getCurrLocation: Boolean = true 

    @observable gotJobs: Int = 0

    @observable gotNewJob: Boolean = true

    @observable etaUpdateModal: Boolean = false

    @persist @observable navigationOption: String = 'waze'

    @observable fareInfoModalVisible: Boolean = false

    @persist('object') @observable lastLocationUpdate = {on:new Date()}
    @persist('object') @observable providerInfo = {
      gps: false, 
      enabled: false, 
      network: false
    }

    serverWorkingStatus: Object = {}

    @action.bound 
    async syncServerWorkingStatus() {
      try {
        this.serverWorkingStatus = await $.method.session.Location.Get('v1/status')
        const serverOnlineStatus = (this.serverWorkingStatus.onlineData ? this.serverWorkingStatus.onlineData.onlineStatus : false)
        if (serverOnlineStatus !== this.working) this.workingChange(serverOnlineStatus)
      } catch (e) {
        console.log(e)
      }
    }

    @action.bound 
    async getJobList (dateStr = moment().toISOString()) {
      const dateFrom = this.processDate(dateStr).dateFrom
      const dateTo = this.processDate(dateStr).dateTo
      const fields = [
        '_id', 'assign_type', 'group_info', 'booking_at', 'country', 'destination',
        'fare', 'from', 'additional_fees', 'notes', 'status', 'payment_method', 'passenger_info', 'type', 'fareObj'
      ]
      try {
        this.loading = true
        const resp = await Session.Booking.Get(`v1/bookings?role=driver&date_from=${dateFrom}&date_to=${dateTo}&fields=${fields.join(',')}`)
        const jobs = resp.filter(pipe => (
          pipe.status === 'Completed_by_Passenger' ||
          pipe.status === 'Completed' ||
          pipe.status === 'No_Show' ||
          pipe.status === 'Cancelled_by_Driver' ||
          pipe.status === 'Cancelled_by_System' ||
          pipe.status === 'Cancelled_by_Passenger' //&& pipe.driver_id
        ))
        this.jobList = jobs
        this.loading = false
      } catch (e) {
        this.loading = false
      }
    }
    
    @action.bound 
    async getAdvanceJobs () {
      try {
        const data = await Session.Booking.Get('v1/bookings?role=driver&sort=-booking_at&status=Confirmed')
        this.advanceBooking = data || []
      } catch (e) {
        console.log(e)
      }
    }
    
    @action.bound 
    async getActiveJobs () {
      try {
        let jobs = await Session.Booking.Get('v1/bookings?role=driver&&sort=-booking_at&status=Pending_Acceptance')
        var i = this.gotJobs
        var j = jobs.length
        switch(true) {
          case i < j:
            this.gotNewJob = true;
            this.gotJobs = jobs.length;
            break;
          case i > j:
            // this.gotNewJob = false;
            this.gotJobs = jobs.length;
            break;
          case i == j && this.busy === true:
            this.gotNewJob = false;
            this.gotJobs = jobs.length;
            break;
          case i > j && this.busy === false:
            this.gotJobs = jobs.length;
            break;
        }
        this.await_jobs = jobs || []
      } catch (e) {
        console.log(e)
      }
    }

    @action.bound 
    async getBookingDetail () {
      const fields = [
        '_id', 'assign_type', 'group_info', 'booking_at', 'country', 'destination',
        'fare', 'from', 'additional_fees', 'notes', 'status', 'payment_method', 'passenger_info', 'type', 'fareObj', 'code'
      ]
      const {_id} = this.active_job

      try {
        let _job = await Session.Booking.Get(`v1/bookings/${_id}?fields=${fields.join(',')}`)

        this.active_job = _job

        if (_job.status === 'Cancelled_by_Passenger') {
          this.busy = false
          $.define.func.showMessage($.store.app.strings.Cancelled_by_Passenger)
          global.$.navigator.dispatch(NavigationActions.back())

        } else if (['On_The_Way', 'Arrived', 'On_Board', 'Finalize_Total_Fare'].indexOf(_job.status) > -1 && !this.busy) {
          this.busy = true
          if (!this.working) this.working = true
        }

      } catch (e) {
        console.log('getBookingDetail:', e)
      }
    }

    @action.bound 
    async recoverActiveBooking (_id: String) {
      const fields = [
        '_id', 'assign_type', 'booking_at', 'group_info', 'country', 'destination',
        'fare', 'from', 'additional_fees', 'notes', 'status', 'payment_method', 'passenger_info', 'type', 'fareObj'
      ]
      try {
        let _job = await Session.Booking.Get(`v1/bookings/${_id}?fields=${fields.join(',')}`)
        this.active_job = _job
        this.busy = true
        this.working = true
      } catch (e) {
        console.log('recoverActiveBooking:', e)
      }
    }

    @action
    async bookingRating (rating) {
      const isIos = Platform.OS === 'ios';
      
      !isIos && $.store.app.showLoading()
      try {
        if(rating == 0 || rating == 1 || rating == 2 || rating == 3){
          rating+=1
        }
        const data = await Session.Rating.Post('v1', {booking_id: this.active_job._id, rating})
      } catch (e) {
        console.log(e)
      } finally {
        !isIos && $.store.app.hideLoading()
      }
    }

    @action.bound 
    async uploadLocation (location, vehicles) {
      // Used to update the vehicle location to server
      try {
        const on = $.store.jobs.lastLocationUpdate.on

        if (moment().diff(on) < 5000) {
          throw('Coordinate has been updated in past 5 seconds')
        }

        await Session.Location.Put('v1', { ...location, vehicle_id: vehicles })

        $.store.jobs.lastLocationUpdate = { 
          coords: {
            latitude: location.latitude,
            longitude: location.longitude
          }, 
          on: new Date() 
        }
      } catch (e) {
        if (typeof e === 'string') {
          // console.log(e)
          
        } else if (this.working && e.response.data.code === 'INACTIVE_DRIVER') {
          const status =  e.response.data.data && e.response.data.data.status

          switch(status) {
            case 'invalidPsvNo':
              Alert.alert("Notice", "Your account is currently inactive, as you have not submitted your PSV documents - please submit them to proceed. For more details, please contact our customer service from the Help Page.") 
              break
            case 'invalidEvpNo':
              Alert.alert("Notice", "Your account is currently inactive, as you are not allocated an EVP Slot. For more details, please contact our customer service from the Help Page.")
              break
            default:
              Alert.alert("Notice", "Your account is inactive. Please contact our customer service.")
              break
          }
          
          this.workingChange(false)

        } else if (this.working && e.response.data.code === 'SUSPENDED_DRIVER') {
          Alert.alert("Notice", "Your account has been suspended. For more details, please contact our customer service from the Help Page.")
          this.workingChange(false)
        }
      }
    }

    @action.bound
    async cancelHandle (_id) {
      try {
        const { assign_type } = this.active_job

        if (assign_type === 'reserved_group') {
          const data = await Session.Rating.Get('v1/statistics?resultType=driverDashboard')
          let { recentCancellationPct = '' } = data

          this.recentCancellationPct = recentCancellationPct
          this.cancelModalVisible = true
        } else {
          Alert.alert($.store.app.strings.cancel_trip, $.store.app.strings.waiting_cancel_punish, [
            { text: $.store.app.strings.cancel },
            {
              text: $.store.app.strings.confirm,
              onPress: async () => {
                this.bookingHandle({ action: 'cancel' }, _id)
                global.$.navigator.dispatch(NavigationActions.back())
              }
            }
          ])
        }
        
      } catch (e) {
        Alert.alert($.store.app.strings.cancel_trip, $.store.app.strings.waiting_cancel_punish, [
          { text: $.store.app.strings.cancel },
          {
            text: $.store.app.strings.confirm,
            onPress: async () => {
              this.bookingHandle({ action: 'cancel' }, _id)
              global.$.navigator.dispatch(NavigationActions.back())
            }
          }
        ])
      }
    }

    @action.bound
    async bookingHandle (params, bookingId) {
      const fields = [
        '_id', 'assign_type', 'group_info', 'booking_at', 'country', 'destination',
        'fare', 'additional_fees', 'from', 'notes', 'status', 'payment_method', 'passenger_info', 'type', 'fareObj'
      ]
      const { action } = params
      const isIos = Platform.OS === 'ios';

      try {
        !isIos && $.store.app.showLoading()
        let data = await Session.Booking.Put(`v1/${bookingId}`, params)
        if (!data.isSuccess) throw data

        this.active_job = data.doc
        this.loading = false

        if (action === 'accept') {
          await this.getActiveJobs()

          if (Platform.OS === 'android') {
            $.store.app.acceptSoundPlay()
          } else {
            $.store.app.newBookingSoundStop()
          }

          if (this.active_job.status !== 'Confirmed') {
            this.busy = true
          } 

          if (this.active_job.type === 'advance') {
            this.getAdvanceJobs()
            $.store.app.control.tab.navigate('MainTab')
            $.store.app.control.header.indexTrip = 1

            global.$.navigator.dispatch(NavigationActions.navigate({
              routeName: 'Main'
            }))
          } else {
            global.$.navigator.dispatch(NavigationActions.navigate({
              routeName: 'JobsListDetail', params: { type: 'active_booking' }
            }))
          }

        } else if (['reject', 'cancel', 'no_show'].indexOf(action) > -1) {
          $.store.app.newBookingSoundStop()
          
          if (this.active_job.type === 'advance') this.getAdvanceJobs()

          await $.store.jobs.getDriverDashboard()
          await $.store.wallet.getDriverCreditWallet()

          this.busy = false

          PushNotificationService.localPushCancel({ id: '1' })
          PushNotificationService.localPushCancel({ id: '2' })
          PushNotificationService.localPushCancel({ id: '3' })

        } else if (action === 'on_the_way') {
          this.busy = true
          if (this.active_job.type === 'advance') this.getAdvanceJobs()

        } else if (action === 'arrived') {
          PushNotificationService.localPushCancel({ id: '1' })
          PushNotificationService.localPushCancel({ id: '2' })
          PushNotificationService.localPushCancel({ id: '3' })

        } else if (action === 'completed') {
          await $.store.jobs.getDriverDashboard()
          await $.store.wallet.getDriverCreditWallet()
          this.busy = false
          this.workingChange(true)
          $.store.passenger.promo_info = {}
          this.getActiveJobs()
        
        }
        !isIos && $.store.app.hideLoading()
      } catch (e) {
        this.loading = false
        !isIos && $.store.app.hideLoading()

        if (e.response.data) {
          const { code = '', message = ''} = e.response.data

          $.store.app.logBooking(bookingId, action, code, message)
        }

        if (e.response && e.response.data.code === 'INSUFFICIENT_BALANCE') {
          $.define.func.showMessage($.store.app.strings.insufficient_balance)

          try {
            let jobs = await Session.Booking.Get(`v1/bookings?role=driver&limit=15&sort=-booking_at&fields=${fields.join(',')}`)
            jobs = jobs.filter(({ status }) =>
              status === 'Pending_Acceptance' ||
              status === 'On_The_Way' ||
              status === 'Confirmed' ||
              status === 'Arrived' ||
              status === 'On_Board'
            )
            this.await_jobs = jobs

          } catch (error) {
            console.log(error)
          }
        } else {
          $.define.func.showMessage($.store.app.strings.booking_unavailable)
          global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'Main' }))
        }
      } finally {
        !isIos && $.store.app.hideLoading()
      }
    }

    scheduleEtaUpdate () {
      const { _id, status, driverETA } = this.active_job
  
      if (status.toUpperCase() === 'ON_THE_WAY') {
        const seconds = driverETA && driverETA.duration ? driverETA.duration : 120

        const format = seconds >= 3600 ? 'h[hours] m[mins]': 'm[mins]'
        const duration = moment.utc(seconds * 1000).format(format)
        
        const title = `Your current ETA is ${duration}.`
        const message = `Do you have a new ETA?`

        const FIRST_PROMPT = 15 // seconds
        const SECOND_PROMPT = 30 // seconds
  
        const first_push = {
          id: '1',
          date: moment().add(FIRST_PROMPT, 's').toDate(),
          userInfo: {
            id: '1', // iOS
            target: 'etaUpdate',
            bookingId: _id
          },
          title,
          message
        }
  
        PushNotificationService.localPushSchedule(first_push)
  
        const second_push = {
          id: '2',
          date: moment().add(SECOND_PROMPT, 's').toDate(),
          userInfo: {
            id: '2', // iOS
            target: 'etaUpdate',
            bookingId: _id
          },
          title,
          message
        }
  
        PushNotificationService.localPushSchedule(second_push)
  
        const last_push = {
          id: '3',
          date: moment().add(seconds, 's').toDate(),
          title: 'Have you reached the pickup address?',
          userInfo: {
            id: '3', // iOS
            target: 'etaUpdate',
            bookingId: _id
          },
          message
        }
  
        PushNotificationService.localPushSchedule(last_push)
      }
    }

    rescheduleEtaUpdate (input) {
      const { _id, status } = this.active_job
  
      if (status.toUpperCase() === 'ON_THE_WAY') {
        const message = `Do you have a new ETA?`
  
        PushNotificationService.localPushCancel({ id: '2' })
        PushNotificationService.localPushCancel({ id: '3' })
  
        const last_push = {
          id: '3',
          date: moment().add(input, 'm').toDate(),
          userInfo: {
            id: '3', // iOS
            target: 'etaUpdate',
            bookingId: _id
          },
          title: 'Have you reached the pickup address?',
          message
        }
  
        PushNotificationService.localPushSchedule(last_push)
      }
    }
    
    processDate (date) {
      const dateStr = moment(date).startOf('day')
      const dateFrom = dateStr.toISOString()
      const dateTo = moment(dateStr).add(1, 'days').toISOString()
      return { dateFrom: dateFrom, dateTo: dateTo }
    }

    @action.bound 
    async closeOnlineVerifyModal (redirect) {
      this.showOnlineVerify = false;
      if (redirect) {
        $.store.driverVerification.getVerificationRequest()
      } else return
    }

    @action.bound 
    async workingChange (status) {
      try {
        this.working = status
        $.store.jobs.getCurrLocation = true

        await $.store.account.getProfile()
        const isDriver = $.store.account.isDriver

        if (!isDriver && status) {
          // this.showOnlineVerify = true
          this.working = false

          await Session.Location.Delete('v1/offline')
          
        } else {
          if (this.working) {
            $.store.app.control.header.indexTrip = 1
            await Session.Location.Put('v1/online', { vehicle_id: $.store.vehicles.vehiclesData[0]._id })

            if (Platform.OS === 'android') NativeModules.UtilDevice.flagKeepScreenOn(true)

            this.timer = setInterval(() => {
              this.getDriverDashboard()
            }, 600000)

          } else {
            await Session.Location.Delete('v1/offline')

            if (Platform.OS === 'android') NativeModules.UtilDevice.flagKeepScreenOn(false)

            clearInterval(this.timer)
            this.getDriverDashboard()
          }
        }
      } catch (error) {
        console.log(error)
      }
    }

    checkProviderStatus () {
      try {
        if (!this.locationRequested) {
          this.locationRequested = true
          LocationSwitch.isLocationEnabled(
            () => {
              this.locationRequested = false
              // console.log('Location services is enabled')
            },
            () => { 
              LocationSwitch.enableLocationService(1000, true,
                () => {
                  this.locationRequested = false
                  // console.log('Location services is enabled')
                },
                () => { 
                  this.locationRequested = false
                  // console.log('Location services is disabled')
                },
              );
            },
          );
        }
      } catch (e) {
        console.log('checkProviderStatus error ', e)
      }

      // const { gps = false, enabled = false, network = false } = this.providerInfo
    }

    @action
    async getBlockReasons(blockType){
      try{
        this.blockReasons = await Session.Lookup.Get('v1/lookup/feedbacks?type='+blockType)
      } catch (e) {
        console.log(e)
      }
    }

    @action.bound
    async getDriverDashboard(){
      try{
        //$.store.app.showLoading()
        const data = await Session.Rating.Get('v1/statistics?resultType=driverDashboard')
        this.driverDashboard = data
        const res = $.store.wallet.getDriverCreditWallet()
        if(res){
          return data
        }
      } catch (e) {
        // Show error and logout user out
        $.store.app.hideLoading()
        console.log(e)
      }
    }

    @computed 
    get actualFrom () {
      let from = {}
      if (this.active_job.hasOwnProperty('status')) {
        if (this.active_job.status === 'Pending_Acceptance') {
          from.coords = {
            latitude: this.active_job.from.coords.lat ? this.active_job.from.coords.lat : this.active_job.from.coords.latitude,
            longitude: this.active_job.from.coords.lng ? this.active_job.from.coords.lng : this.active_job.from.coords.longitude
          }
        } else {
          from.coords = {
            latitude: $.store.app.coords.lat ? $.store.app.coords.lat : $.store.app.coords.latitude,
            longitude: $.store.app.coords.lng ? $.store.app.coords.lng : $.store.app.coords.longitude
          }
        }
      }
      return from
    }

    @computed 
    get destination () {
      let destination = {}
      if (this.active_job.hasOwnProperty('status')) {
        if (this.active_job.status === 'Pending_Acceptance') {
          destination.coords = {
            latitude: this.active_job.destination.coords.lat ? this.active_job.destination.coords.lat : this.active_job.destination.coords.latitude,
            longitude: this.active_job.destination.coords.lng ? this.active_job.destination.coords.lng : this.active_job.destination.coords.longitude
          }
        } else if (this.active_job.status === 'On_The_Way') {
          destination.coords = {
            latitude: this.active_job.from.coords.lat ? this.active_job.from.coords.lat : this.active_job.from.coords.latitude,
            longitude: this.active_job.from.coords.lng ? this.active_job.from.coords.lng : this.active_job.from.coords.longitude
          }
        } else {
          destination.coords = {
            latitude: this.active_job.destination.coords.lat ? this.active_job.destination.coords.lat : this.active_job.destination.coords.latitude,
            longitude: this.active_job.destination.coords.lng ? this.active_job.destination.coords.lng : this.active_job.destination.coords.longitude
          }
        }
      }
      return destination
    }
  },
  Autorun: (store) => {
    let jobsTimer = null
    let advanceJobsTimer = null
    let locationTimer = null
    let bookingTimer = null

    const autorunJobs = (working, busy) => {
      jobsTimer && clearInterval(jobsTimer)
      jobsTimer = undefined
      advanceJobsTimer && clearInterval(advanceJobsTimer)
      advanceJobsTimer = undefined
      locationTimer && clearInterval(locationTimer)
      locationTimer = undefined
      bookingTimer && clearInterval(bookingTimer)
      bookingTimer = undefined

      if (store.vehicles.vehiclesData.length === 0) {
        return null
      }

      if (working) {
        BackgroundLocationService.start()
      } else {
        BackgroundLocationService.stop()
      }

      if (working && !busy) {
        store.jobs.getActiveJobs()
        jobsTimer = setInterval(() => store.jobs.getActiveJobs(), 2000)

        store.jobs.getAdvanceJobs()
        advanceJobsTimer = setInterval(() => store.jobs.getAdvanceJobs(), 2000)

      } else if (working && busy) {
        store.jobs.getBookingDetail()
        bookingTimer = setInterval(() => store.jobs.getBookingDetail(), 2000)

      } else {
        store.jobs.await_jobs = []
        
      }
    }
    autorun(() => autorunJobs(store.jobs.working, store.jobs.busy))

    // const autorunFunctionPickAddress = async (activeJob) => {
    //   if (activeJob.hasOwnProperty('status')) {
    //     store.jobs.bookingPath = await store.passenger.directions(
    //       store.jobs.actualFrom.coords,
    //       store.jobs.destination.coords,
    //       {type: 'driver'}
    //     )
    //   }
    // }
    // autorun(() => autorunFunctionPickAddress(store.jobs.active_job), { delay: 1000 })
  }
}
