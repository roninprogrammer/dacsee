// @flow

import { observable, action, autorun, computed } from 'mobx'
import { Alert } from 'react-native'
import { NavigationActions } from 'react-navigation'
import BaseStore from './base.store'
import Session from '../method/session'
export default {
  Name: 'driver',
  Store: class driverStore extends BaseStore {
    constructor(store) {
      super(store)
    }

    // loading
    @observable loading: Boolean = false
    @observable formSubmitted: Boolean = false

    @action.bound async createRequestFile(file, formType) {
      try {
        $.store.app.showLoading()
        let resp = await $.method.session.Driver.Post(`v1/requests/own/file2?formType=${formType}`, file)
        $.store.app.hideLoading()
        return resp
      } catch (e) {
        $.define.func.showMessage(e.response.data.message)
        $.store.app.hideLoading()
        return false
      }
    }

    @action.bound async getRequestFile(file, formType) {
      try {
        $.store.app.showLoading()
        let resp = await $.method.session.Driver.Get(`v1/requests/own/file/${file}?formType=${formType}`)
        $.store.app.hideLoading()
        return resp
      } catch (e) {
        $.define.func.showMessage(e.response.data.message)
        $.store.app.hideLoading()
      }
    }

    @action.bound async updateRequest(data,formType) {
      try {
        $.store.app.showLoading()
        data.formType = formType
        await $.method.session.Driver.Put(`v1/requests/own`, data)
        this.dvFormLive = toJS(data)
        $.store.app.hideLoading()
        return true
      } catch (e) {
        $.define.func.showMessage(e.response.data.message)
        $.store.app.hideLoading()
        return false
      }
    }

  },

  Autorun: (store) => {}
}
