// @flow

import { observable, action, computed } from 'mobx'

import BaseStore from './base.store'
import { StackActions, NavigationActions } from 'react-navigation'

export default {
  Name: 'wallet',
  Store: class walletStore extends BaseStore {

    constructor(store) {
      super(store)
    }

    @observable walletListLoading: Boolean = false

    @observable transactionListLoading: Boolean = false

    @observable searching: Boolean = false

    @observable transferring: Boolean = false

    @observable walletList: Array = []

    @observable walletIndex: Number = 0

    @observable walletDetailList: Array = []

    @observable activityState: Boolean = true

    @observable driverCreditWallet: Object = {}

    @computed get walletSelected() {
      return this.walletList[this.walletIndex]
    }

    // Store Action
    changeAmount(amount) {
      const nextList = [...this.walletList]
      nextList[this.walletIndex].availableAmount -= amount
      this.walletList = nextList
    }

    // Wallet List Action
    @action.bound async getWalletList() {
      this.walletListLoading = true
      try {
        const resp = await $.method.session.Wallet.Get('v1/wallets')
        this.walletList = resp
        this.walletListLoading = false
      } catch (e) {
        console.log(e)
        this.walletListLoading = false
      }
    }

    @action
    updateWalletIndex(idx) {
      this.walletIndex = idx
    }

    // Wallet Detail Action
    @action.bound
    async getWalletDetail(type = '') {
      this.transactionListLoading = true
      try {
        const resp = await $.method.session.Wallet.Get(`v1/walletTransactions?walletType=${type}`)
        this.walletDetailList = resp
        this.transactionListLoading = false
      } catch (e) {
        console.log(e)
        this.transactionListLoading = false
      }
    }

    // Transfer Action
    @action.bound
    async searchUser(body = {}, amount = 0, remark = '') {
      var url = ''
      for (let item in body) {
        url = url + (url == '' ? '' : '&') + item + '=' + body[item]
      }
      try {
        this.searching = true
        const resp = await $.method.session.User.Get('v1/search?' + url)
        const transferInfo = {
          userList: resp,
          amount: amount,
          remark: remark
        }
        if (resp.length == 0) {
          this.searching = false
        } else {
          resp.length == 1 ?
            global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'WalletTransferSummary', params: { transferInfo: transferInfo } })) :
            global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'WalletTransferSelection', params: { transferInfo: transferInfo } }))
          this.searching = false
        }
      } catch (e) {
        this.searching = false
        $.define.func.showMessage($.store.app.strings.unable_connect_server_pls_retry_later)
      }
    }

    @action.bound
    async transfer(params = {}) {
      try {
        this.transferring = true
        await $.method.session.Wallet.Post('v1/transferTransactions', params)
        this.changeAmount(params.amount)
        this.transferring = false
        global.$.navigator.dispatch(StackActions.reset({
          index: 2,
          actions: [NavigationActions.navigate({ routeName: 'Main' }),
          NavigationActions.navigate({ routeName: 'WalletBalance' }),
          NavigationActions.navigate({ routeName: 'WalletDetail' })]
        }))
      } catch (e) {
        console.log(e)
        this.transferring = false
      }

    }

    @action.bound
    async getDriverCreditWallet() {
      try {
        const res = await $.method.session.Wallet.Get('v1/wallets/CR-MY')
        this.driverCreditWallet = res
        return res;
      } catch (e) {
        console.log(e)
      }
    }

    @action.bound
    async clickActivityState(state) {
      this.activityState = state === true ? false : true
    }

  },
  Autorun: (store) => {
  }
}