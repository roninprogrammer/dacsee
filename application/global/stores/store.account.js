// @flow

import { Platform } from 'react-native'
import { observable, action, autorun, reaction } from 'mobx'
import { persist } from 'mobx-persist'
import { NavigationActions } from 'react-navigation'
import { System } from 'dacsee-utils'
import BaseStore from './base.store'
import Session from '../method/session'
import { CrashlyticsService, PushNotificationService } from '../../global/service'

class LoginData {
  @observable phoneNo: String
  @observable phoneCountryCode: String = '+60'
  @observable phoneVerificationCode: String = ''
  @observable referralUserId: String = ''
  @observable fullName: String = ''
  @observable stage: Int = 0
  @observable counttime: Int = 0
  @observable mulitiple_account: Array = []
  @observable emailVerificationCode: String = ''
  @observable sendCodeLoading: Boolean = false
  @observable TNCVisible: Boolean = false
}

export default {
  Name: 'account',
  Store: class AccountStore extends BaseStore {
    constructor(store) {
      super(store)
      this.loginData = new LoginData()
    }
    
    @observable timer = null
    @persist('object') @observable user: Object = {}
    @persist @observable isDriver: Boolean = false
    @persist @observable isElite: Boolean = false
    @persist @observable isSuspend: Boolean = false
    @observable referralGroupId: String = ''
    @persist @observable authToken: String = ''

    // Store Action
    setValue(params) {
      this.user = Object.assign({}, this.user, params)
    }

    // Login & Registration Action
    async loginSuccess(data: Object) {
      this.user = data.user
      this.authToken = data.authToken
      this.loginData = new LoginData()

      if (this.referralGroupId !== '') {
        await $.store.circle.isMember(this.referralGroupId)
        
        let isMember = $.store.circle.groupData.memberShipType.hasOwnProperty('_id')
        let membershipType = isMember ? $.store.circle.groupData.memberShipType.membershipType : ''
        
        await $.store.circle.getGroupInfo(this.referralGroupId)
        
        global.$.navigator.dispatch(NavigationActions.navigate({
          routeName: 'CircleGroupDetail',
          params: Object.assign({}, $.store.circle.groupData.groupInfo, { isMember, membershipType })
        }))
      } else {
        global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'App' }))
      }

      $.store.circle.getFriends()
      $.store.circle.getGroups()
      $.store.circle.getDefaultGroup()
    }
    
    @action.bound
    async loginWithPhone() {
      const { phoneNo, phoneCountryCode } = this.loginData

      $.store.app.showLoading()

      try {
        sendCodeLoading = true
        const data = await $.method.session.User.Post('v1/sendVerificationCode/phone', { phoneNo, phoneCountryCode })

        $.store.app.hideLoading()
        sendCodeLoading = false
        this.loginData.stage = 1
        if (data) {
          this.timer && clearInterval(this.timer)
          this.startCountDown(data.resendWaitSec)
        }
      } catch (e) {
        $.store.app.hideLoading()
        this.loginData.sendCodeLoading = false
        if (e.response && e.response.data.code == 'VERIFICATION_CODE_RESEND_WAIT') {
          this.timer && clearInterval(this.timer)
          this.startCountDown(parseInt(e.response.data.data))
        }
      }
    }

    startCountDown(val) {
      this.loginData.counttime = val
      this.timer = setInterval(() => {
        if (this.loginData.counttime === 0) {
          this.timer && clearInterval(this.timer)
        } else {
          this.loginData.counttime--
        }
      }, 1000)
    }

    @action.bound
    clearTimeInterval() {
      this.timer && clearInterval(this.timer)
      this.loginData.counttime = 0
    }

    @action.bound
    async judgePhoneCode(_id: String) {
      const { phoneNo, emailVerificationCode, phoneCountryCode, phoneVerificationCode } = this.loginData
      const { showMessage } = $.define.func
      const strings = $.store.app.strings
      let body = { phoneNo, phoneCountryCode, phoneVerificationCode }
      if (_id) body = Object.assign({}, body, { _id })
      $.store.app.showLoading()
      try {
        const data = await $.method.session.User.Post('v1/auth/phone', body)
        $.store.app.hideLoading()
        if (data && data.user && data.authToken) this.loginSuccess(data)
      } catch (e) {
        $.store.app.hideLoading()
        if (e.response) {
          console.log(e)
          switch (e.response.data.code) {
            case 'INVALID_USER':
              this.loginData.stage = 2
              break
            case 'MULTIPLE_USER_ACCOUNT':
              this.loginData.mulitiple_account = e.response.data.data
              global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'LoginSelectAccount', params: { type: 'PHONE_LOGIN' } }))
              break
            case 'EXPIRED_VERIFICATION_CODE':
              showMessage(e.response.data.message)
              break
            case 'INACTIVE_USER':
              break
            default:
              showMessage(strings.verify_err)
              break
          }
        }
      }
    }

    @action.bound
    async registerPhoneUser() {
      const { fullName, phoneNo, phoneCountryCode, phoneVerificationCode, referralUserId } = this.loginData
      let body = { fullName, phoneNo, phoneCountryCode, phoneVerificationCode, referralUserId }
      try {
        const data = await $.method.session.User.Post('v1/register', body)
        if (data && data.user && data.authToken) this.loginSuccess(data)
      } catch (e) {
        // $.store.app.hideLoading()
        if (e.response.data && e.response.data.code === 'INVALID_REFERRAL') {
          $.define.func.showMessage($.store.app.strings.invalid_referral)
        } else if (e.response.data && e.response.data.code === 'MISSING_INPUT') {
          $.define.func.showMessage($.store.app.strings.error_params_empty)
        }
      }
    }

    @action async getProfile() {
      try {
        this.user = await $.method.session.User.Get('v1/profile?')
        this.isDriver = ['non', 'pendingVerification'].indexOf(this.user.driverInfo.status) < 0
        this.isElite = this.user.achievements.indexOf('eliteDriver') > -1
        this.isSuspend = this.user.driverInfo.suspension ? true : false
        this.pinStatus = this.user.pinStatus

      } catch (e) {
        // Show error and logout user out
        console.log(e)
      }
    }
    
    @action.bound
    async sendVerificationCode() {
      const { phoneNo, phoneCountryCode } = this.loginData
      try {
        this.loginData.sendCodeLoading = true
        const data = await $.method.session.User.Post('v1/sendVerificationCode/phone', { phoneNo, phoneCountryCode })
        this.loginData.sendCodeLoading = false
        if (data) {
          this.timer && clearInterval(this.timer)
          this.startCountDown(data.resendWaitSec)
        }
      } catch (e) {
        this.loginData.sendCodeLoading = false
        if (e.response && e.response.data.code === 'VERIFICATION_CODE_RESEND_WAIT') {
          this.timer && clearInterval(this.timer)
          this.startCountDown(parseInt(e.response.data.data))
        }
      }
    }
    
    @action.bound async changeAvatar(base64) {
      try {
        const data = await $.method.session.User.Upload('v1/profile/avatar2', {
          data: `data:image/jpeg;base64,${base64}`
        })
        if (data) this.user = data
        $.define.func.showMessageLong($.store.app.strings.already_upload_avatar)
      } catch (e) {
        $.define.func.showMessageLong($.store.app.strings.unable_change_avatar)
      }
    }
    
    @action.bound async changeFullName(name = '') {
      try {
        const resp = await $.method.session.User.Put('v1/profile', { fullName: name })
        this.setValue({ fullName: resp.fullName })
        $.define.func.showMessage($.store.app.strings.update_succ)
      } catch (e) {
        console.log(e)
        $.define.func.showMessage($.store.app.strings.unable_connect_server_pls_retry_later)
      }
    }
    
    @action.bound async changeReferralUserId(referralUserId = '') {
      try {
        const resp = await $.method.session.User.Put('v1/profile', { referralUserId: referralUserId })
        if (!resp.referral || resp.referral === null || resp.referral === '') {
          $.define.func.showMessage($.store.app.strings.update_referral_failed)
        }
        else {
          this.setValue({ referral: resp.referral })
          $.define.func.showMessage($.store.app.strings.update_succ)

        }
      } catch (e) {
        console.log(e)
        $.define.func.showMessage($.store.app.strings.unable_connect_server_pls_retry_later)
      }
    }
    
    @action.bound async changeEmail(email = '') {
      try {
        const resp = await $.method.session.User.Put('v1/profile', { email })
        this.setValue({ email: resp.email })
        $.define.func.showMessage($.store.app.strings.update_succ)
      } catch (e) {
        console.log(e)
        $.define.func.showMessage($.store.app.strings.unable_connect_server_pls_retry_later)
      }
    }

    @action async bindDevice(token) {
      const data = {
        token,
        device: {
          available: true,
          platform: Platform.select({ ios: 'iOS', android: 'android' }),
          version: System.Device.Version,
          uuid: System.UUID,
          model: System.Device.Name,
          manufacturer: System.Device.Brand,
          isVirtual: false,
          serial: 'UNKNOW'
        },
        status: 'active',
        type: System.Platform.Name,
        uuid: System.UUID,
        user_id: this.user._id
      }

      try {
        const resp = await Session.Push.Post('v1/register', data)
      } catch (e) {
        console.log('Bind Device ', e)
      }
    }
    
    @action.bound
    async privacySetting(status = false, type, action) {
      let postData = {}
      postData[type] = {}
      postData[type][action] = status
      try {
        const resp = await $.method.session.User.Put('v1/profile', { pref: postData })
        this.user = resp
      } catch (e) {
        $.define.func.showMessage(this.strings.unable_connect_server_pls_retry_later)
        console.log(e)
      }
    }
    
    @action.bound async logout() {
      try {
        $.store.app.showLoading()
        const resp = await $.method.session.Push.Put('v1/unsubscribe', { uuid: System.UUID })
      } catch (e) {
        console.log(e)
        $.store.app.hideLoading()
      } finally {
        this.user = {}
        this.authToken = ''
        $.store.app.control.tab.navigate('MainTab')
        $.store.app.control.header.indexTrip = 0
        $.store.vehicles.vehiclesData = []
        $.store.circle.friendData.friends = []
        $.store.chat.chatList = {}
        $.store.chat.msgList = []
        $.store.jobs.working = false
        $.store.app.hideLoading()
        global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'Auth' }))
      }
    }

    @action.bound setReferrerValue(id) {
      this.loginData.referralUserId = id
    }

    @action.bound setReferrerGroupValue(id) {
      this.referralGroupId = id
    }

    // Autorun get user push notification token
    @action async bindDevice(token) {
      const data = {
        token,
        device: {
          available: true,
          platform: Platform.select({ ios: 'iOS', android: 'android' }),
          version: System.Device.Version,
          uuid: System.UUID,
          model: System.Device.Name,
          manufacturer: System.Device.Brand,
          isVirtual: false,
          serial: 'UNKNOW'
        },
        status: 'active',
        type: System.Platform.Name,
        uuid: System.UUID,
        user_id: this.user._id
      }
      try {
        await Session.Push.Post('v1/register', data)
      } catch (e) {
        console.log('Bind Device ', e)
      }
    }

    // Crashlytic Action
    accountChangeReaction = reaction(
      () => ({ fullName: this.user.fullName, userId: this.user.userId }),
      (user) => {
        CrashlyticsService.setUserName(user.fullName)
        CrashlyticsService.setUserIdentifier(user.userId)
      },
      { fireImmediately: false }
    )
  },
  Autorun: async (store) => {
    const autorunDispose_GetProfile = async (authToken) => {
      if (!authToken || authToken.length === 0) return null
      await store.account.getProfile()
      dispose && dispose()
    }
    const dispose = autorun(() => autorunDispose_GetProfile(store.account.authToken))

    const autorunDispose_UpdateDeviceToken = async (authToken, deviceToken) => {
      deviceToken = deviceToken || PushNotificationService.DEVICE_TOKEN

      if (!authToken || authToken.length === 0) return null
      if (!deviceToken || deviceToken.length == 0) return null

      await store.account.bindDevice(deviceToken)
    }
    autorun(() => autorunDispose_UpdateDeviceToken(store.account.authToken, store.app.deviceToken))
  }
}
