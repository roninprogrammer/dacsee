import App from './store.app'
import Passenger from './store.passenger'
import Chat from './store.chat'
import Wallet from './store.wallet'
import Circle from './store.circle'
import Trip from './store.trip'
import Jobs from './store.jobs'
import Vehicles from './store.vehicle'
import Account from './store.account'
import DownLine from './store.downLine'
import Joy from './store.joy'
import Bank from './store.bank'
import Inbox from './store.inbox'
import Driver from './store.driver'
import DriverVerification from './store.driverVerification'

const Store = new (class Store {
  _stores : Array = [
    App,
    Passenger,
    Chat,
    Wallet,
    Circle,
    Trip,
    Jobs,
    Vehicles,
    Account,
    DownLine, 
    Joy,
    Bank,
    Inbox,
    Driver,
    DriverVerification,
  ]

  constructor () {
    this._stores.forEach(pipe => { this[pipe.Name] = new pipe.Store(this) })
    this._stores.forEach(pipe => pipe.Autorun(this))
  }
})()

export default Store
export const Domain = { ...Store }
