// @flow

import { observable, action, autorun, computed } from 'mobx'
import { Alert } from 'react-native'
import { NavigationActions } from 'react-navigation'
import BaseStore from './base.store'
import Session from '../method/session'
export default {
  Name: 'inbox',
  Store: class inboxStore extends BaseStore {
    constructor(store) {
      super(store)
    }

    // loading
    @observable loading = false
    @observable inboxList = []
    @observable inboxDetail = {}
    @observable inboxModalDetail = []
    @observable unreadInbox = []
    @observable checkInbox = false
    @observable viewModal = false

  @action.bound async getInboxList() {
    try {
      this.loading = true
      const data = await $.method.session.Inbox.Get('v1/messages')
      if(data && data.length !== 0){
        if(data.length == this.inboxList.length){
          if(data === this.inboxList){
            this.loading = false
            return
          }
          else{
            this.inboxList = Array.isArray(data)
            ? data.map(this.formatMessage)
            : [];
            //$.store.inbox.getInboxModalDetail()
            this.loading = false
          }
        }
        else{
          this.inboxList = Array.isArray(data)
          ? data.map(this.formatMessage)
          : [];
          //$.store.inbox.getInboxModalDetail()
          $.store.inbox.getInboxUnreadList()
          this.loading = false
        }
      }
      else{ 
        this.inboxList = Array.isArray(data)
            ? data.map(this.formatMessage)
            : [];
          this.loading = false
      }
      
    } catch (e) {
      console.log(e)
      //this.loading = false
    }
  }

  @action.bound async getInboxDetail(id){
    try {
      this.loading = true
      const inListing = this.inboxList.find(m => m._id === id);
      if (inListing) {
        this.inboxDetail = inListing;
      }
      const data = await $.method.session.Inbox.Get(`v1/messages/${id}`)
      this.loading = false
      this.inboxDetail = data && data._id ? this.formatMessage(data) : {};
      return this.inboxDetail
    } catch (e) {
      console.log(e)
      this.loading = false
    }

  }

  @action.bound async deleteInboxMessage(id){
    try{
      const result = await $.method.session.Inbox.Delete(`v1/messages/${id}`)
      $.store.inbox.getInboxList()
    } catch(e){
      console.log(e)
    }
  }

  @action.bound async submitMessageRead(id){
    try{
      const data = await $.method.session.Inbox.Put(`v1/messages/${id}/markAsRead`)
      $.store.inbox.getInboxList()
      $.store.inbox.getInboxModalDetail()
      $.store.inbox.getInboxUnreadList()
    } catch(e){
      console.log(e)
    }
  }

  @action.bound async getInboxUnreadList(){
    //this.loading = true
    //this.unreadInbox = await $.method.session.Inbox.Get('v1/messages?state=new')
    const data = await $.method.session.Inbox.Get('v1/messages?state=new')
    if(data && data.lenght !== 0){
      this.unreadInbox = data
    }
    return data
    //this.loading = false
  }

  @action.bound async pressNeverShow(){

    const data = this.inboxModalDetail.map(i=> i._id)
    let body = {hidePopup: {_ids: data}}
    const result = await $.method.session.Inbox.Post('v1/messages/updateBatch', body)
  }

  @action.bound async getInboxModalDetail(){
    try {
      //this.loading = true
      const data = await $.method.session.Inbox.Get('v1/messages?popup=true')
      if(data && data.length !== 0){
        this.inboxModalDetail = data
        //this.loading = false
        return data
      }
    } catch (e) {
      console.log(e)
      //this.loading = false
    }

  }

  formatMessage(message) {
    if (!message ||!message._id || !message.title) return null;
    const placeholderImg = '../../resources/images/placeholder.png';
    const placeholderDesc = 'New Announcement';
    const hasImg = Array.isArray(message.avatars) && message.avatars.length;
    const hasPopupImg = Array.isArray(message.popupAvatars) && message.popupAvatars.length;
    const hasButtons = Array.isArray(message.buttons) && message.buttons.length;
    const popupImageLength = message.popupAvatars.length;
    const avatarLength = message.avatars.length;
    const hasBarcode = true
    if(!message.barcode){this.hasBarcode = false}
    return {
      _id: message._id,
      title: message.title,
      description: message.description || placeholderDesc,
      img: hasImg
        ? { uri: message.avatars[avatarLength - 1].url}
        : require(placeholderImg),
      imgSmall: hasImg
        ? { uri: message.avatars[avatarLength - 1].urlSmall}
        : require(placeholderImg),
      popupAvatars: message.popupAvatars || [],
      popupImg: hasPopupImg
        ? { uri: message.popupAvatars[popupImageLength - 1].url}
        : require(placeholderImg),
      popup: message.popup,
      startOn: message.startOn,
      readOn: message.readOn,
      state: message.state,
      barcode: hasBarcode
        ? message.barcode
        : null,
      buttons: hasButtons
        ? message.buttons
        : null,
      hasImg: hasImg
    }
   }
  },
  Autorun: (store) => {}
}