// @flow

import { observable, computed, action, autorun } from 'mobx'
import { persist } from 'mobx-persist'
import BaseStore from './base.store'
import { NavigationActions } from 'react-navigation'

export default {
  Name: 'bank',
  Store: class bankStore extends BaseStore {
    constructor(store) {
      super(store)
    }

    //loading
    @observable loading: Boolean = false
    @persist('list') @observable bankList: Array = []
    @observable selectedBank: String = ''
    @observable bankDetail: Object = {}
    @observable allBankList: Array = []
    @observable updatebankList: Array = []
    @observable resetTimer: null
    @observable counttime: Number = 0
    @observable errormessage: String = ''
    @observable errorcode: String = ''

    @action.bound async getBankList(showLoading = true) {
      try {
        if (showLoading) $.store.app.showLoading()
        data = await $.method.session.User.Get('v1/banks?')
        this.bankList = data
        this.errormessage = ''
        this.errorcode = ''
        if (showLoading) setTimeout(() => $.store.app.hideLoading(), 500)
      } catch (e) {
        console.log('fail to get banklist')
        if (showLoading) setTimeout(() => $.store.app.hideLoading(), 500)
      }
    }

    @action.bound async getBankDetail(id) {
      try {
        $.store.app.showLoading()
        const data = await $.method.session.User.Get(`v1/banks/${id}`)
        this.bankDetail = data
        setTimeout(() => $.store.app.hideLoading(), 500)
        return this.bankDetail
      } catch (e) {
        let msg = $.store.app.strings.error_try_again
        if (e.response && e.response.data.message) {
          msg = e.response.data.message
        }
        $.define.func.showMessage(msg)
        setTimeout(() => $.store.app.hideLoading(), 500)
        return null
      }
    }

    @action.bound async updateBankInfo(postData) {
      try {
        $.store.app.showLoading()
        const { _id, country, name, accountNo, accountHolderIdNo, accountHolderName } = postData
        const resp = await $.method.session.User.Put(`v1/banks/${_id}`, {
          country,
          name,
          accountNo,
          accountHolderIdNo,
          accountHolderName
        })
        const bankIdx = this.bankList.findIndex(b => b._id === _id)
        if (bankIdx >= 0) this.bankList[bankIdx] = resp
        setTimeout(() => $.store.app.hideLoading(), 500)
        return true
      } catch (e) {
        let msg = $.store.app.strings.error_try_again
        if (e.response && e.response.data.message) {
          msg = e.response.data.message
        }
        $.define.func.showMessage(msg)
        setTimeout(() => $.store.app.hideLoading(), 500)
        return false
      }
    }

    @action.bound async getLookUpBankList(country) {
      try {
        $.store.app.showLoading()
        data = await $.method.session.Lookup.Get(
          `v1/lookup/banks?country=${country}&resultType=nameOnly`
        )
        this.allBankList = data
        setTimeout(() => $.store.app.hideLoading(), 500)
      } catch (e) {
        console.log('getBank:', e)
        setTimeout(() => $.store.app.hideLoading(), 500)
      }
    }

    @action.bound async addBank(form) {
      try {
        $.store.app.showLoading()
        const { country, name, accountNo, accountHolderIdNo, accountHolderName } = form
        const resp = await $.method.session.User.Post('v1/banks', {
          country,
          name,
          accountNo,
          accountHolderIdNo,
          accountHolderName
        })
        if (resp && resp._id) {
          const exist = this.bankList.findIndex(b => b._id === resp._id)
          if (exist < 0) this.bankList.push(resp)
        }
        $.define.func.showMessage($.store.app.strings.add_bank_msg_1)
        setTimeout(() => $.store.app.hideLoading(), 500)
        return true
      } catch (e) {
        let msg = $.store.app.strings.error_try_again
        if (e.response && e.response.data.message) {
          msg = e.response.data.message
        }
        $.define.func.showMessage(msg)
        setTimeout(() => $.store.app.hideLoading(), 500)
        return false
      }
    }

    @action.bound async createWithdrawal(form) {
      try {
        $.store.app.showLoading()
        await $.method.session.Wallet.Post('v1/withdrawalTransactions', form)
        await $.store.wallet.getWalletList()
        $.define.func.showMessage($.store.app.strings.withdrawal_successfully)
        global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'WalletBalance' }))
        this.errormessage = ''
        setTimeout(() => $.store.app.hideLoading(), 500)
      } catch (e) {
         this.errormessage = $.store.app.strings.error_try_again
         if (e.response && e.response.data.message) {
          this.errormessage = e.response.data.message
          this.errorcode = e.response.data.code
        }
        setTimeout(() => $.store.app.hideLoading(), 500)
      }
    }

    @action.bound async setNewPin(pin) {
      try {
        $.store.app.showLoading()
        await $.method.session.User.Put('v1/pin', {
          newPin: pin
        })
        $.define.func.showMessage($.store.app.strings.pin_success)
        $.store.account.getProfile()
        setTimeout(() => $.store.app.hideLoading(), 500)
        return true
      } catch (e) {
        let msg = $.store.app.strings.error_try_again
        if (e.response && e.response.data.message) {
          msg = e.response.data.message
        }
        $.define.func.showMessage(msg)
        setTimeout(() => $.store.app.hideLoading(), 500)
        return false
      }
    }

    @action.bound async updatePin(data) {
      try {
        $.store.app.showLoading()
        await $.method.session.User.Put('v1/pin', data)
        $.define.func.showMessage($.store.app.strings.pin_success)
        this.errormessage = ''
        this.errorcode = ''
        $.store.account.getProfile()
        setTimeout(() => $.store.app.hideLoading(), 500)
        return true
      } catch (e) {
        let msg = $.store.app.strings.error_try_again
        if (e.response && e.response.data.message) {
          msg = e.response.data.message
        }
        $.define.func.showMessage(msg)
        setTimeout(() => $.store.app.hideLoading(), 500)
        return false
      }
    }

    @action.bound async resetPin(data) {
      try {
        $.store.app.showLoading()
        await $.method.session.User.Put('v1/pin/reset', data)
        $.define.func.showMessage($.store.app.strings.pin_success)
        this.errormessage = ''
        this.errorcode = ''
        $.store.account.getProfile()
        setTimeout(() => $.store.app.hideLoading(), 500)
        return true
      } catch (e) {
        let msg = $.store.app.strings.error_try_again
        if (e.response && e.response.data.message) {
          msg = e.response.data.message
        }
        $.define.func.showMessage(msg)
        setTimeout(() => $.store.app.hideLoading(), 500)
        return false
      }
    }

    @action.bound async getResetCode() {
      try {
        $.store.app.showLoading()
        const data = await $.method.session.User.Post('v1/pin/sendResetCode')
        setTimeout(() => $.store.app.hideLoading(), 500)
        if (data) {
          this.resetTimer && clearInterval(this.resetTimer)
          this.startCountDown(30)
          $.define.func.showMessage($.store.app.strings.reset_code_info)
          return true
        }
      } catch (e) {
        setTimeout(() => $.store.app.hideLoading(), 500)
        if (e.response && e.response.data.code == 'VERIFICATION_CODE_RESEND_WAIT') {
          this.resetTimer && clearInterval(this.resetTimer)
          this.startCountDown(parseInt(e.response.data.data))
          $.define.func.showMessage(`${$.store.app.strings.please_wait_for} ${e.response.data.data} ${$.store.app.strings.seconds_later}`)
        }
      }
    }

    startCountDown(val) {
      this.counttime = val
      this.resetTimer = setInterval(() => {
        if (this.counttime === 0) {
          this.resetTimer && clearInterval(this.resetTimer)
        } else {
          this.counttime--
        }
      }, 1000)
    }

    @action.bound
    clearTimeInterval() {
      this.resetTimer && clearInterval(this.resetTimer)
      this.counttime = 0
    }

    // @action.bound updateSelectedBank(index) {
    //     const name = $.store.bank.allBankList[index]
    //     this.name = name

    //     // const selectedBank = $.store.bank.allBankList[index]
    //     // console.log(selectedBank)
    //     // this.selectedBank = selectedBank
    // }
  },

  Autorun: store => {
    const recoverBankList = async token => {
      if (!token) return
      await store.bank.getBankList(false)
    }
    autorun(() => recoverBankList(store.account.authToken))
  }
}
