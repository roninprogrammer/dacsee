// @flow
import { Alert, ListView } from 'react-native'
import { observable, action, computed, autorun, toJS } from 'mobx'
import BaseStore from './base.store'
import _ from 'lodash'
import { NavigationActions, StackActions } from 'react-navigation'
import { persist, create } from 'mobx-persist'
import Session from '../method/session'
import { AnswersSrv } from '../service'

class FriendData {
  @observable selectAll = false
  @observable select_friends = []
  @observable check_friends = []
  @observable requestors = []
  @observable search_friends = []
  @observable searchAccouts = []
  @observable allFriend = false
  @observable check_all_friend = []
  @observable friend_selected = []
  @persist('list') @observable friends = []
  @observable friend_lists = []
  @observable friend_count = 0
}
class GroupData {
  @observable newGroup: { name: String, type: String, tagline:String, option: Object, description: String, location: Object } = {}
  @observable countries = []
  @observable cities = []
  @observable check_group = []
  @observable select_group:Object = {}
  @observable groups = {}
  @observable groups_official = []
  @observable groups_my = []
  @observable groups_featured = []
  @observable groupInfo = {}
  @observable newMembers = {}
  @observable members = {}
  @observable searchResults = {}
  @observable invitations = []
  @observable searchGroups = []
  @observable memberShipType = {}
  @observable campaigns = []
  @observable userCampaigns = []
  @observable groupsFares = []
  @observable featuredCampaigns = []
}
const page = 0
const limit = 20

export default {
  Name: 'circle',
  Store: class CircleStore extends BaseStore {
    @observable online = false
    @observable driver = false
    @observable pressSend = true
    @observable checkedData = false
    @observable filter = true
    @observable circle_points: Array = []
    @observable circleLoading: Boolean = false
    @observable defaultGroup = {}
    @observable advanceOptions = false
    @observable isAdvanceCampaign = false
    constructor (store) {
      super(store)
      this.friendData = new FriendData()
      this.groupData = new GroupData()
      for (let i = 0; i < this.friendData.friends.length; i++) {
        this.friendData.friends[i]._id = Math.round(Math.random() * 10000000)
      }
    }
    
    @action.bound
    async searchAccount (type, value, code = 'CN') {
      let searchStr = ''
      if (type === 'phone') {
        searchStr = `&phoneCountryCode=${code}&phoneNo=${value}`
      } else if (type === 'email') {
        searchStr = `&email=${value}`
      } else {
        searchStr = `&fullNameOrUserId=${value}`
      }
      try {
        // const { country = 'CN' } = this.props.account TODO
        const _accounts = await $.method.session.User.Get(`v1/search?country=CN${searchStr}`)
        this.friendData.searchAccouts = _accounts
        if (_accounts.length === 0) {
          $.define.func.showMessage($.store.app.strings.no_user)
          return global.$.navigator.dispatch(NavigationActions.back())
        }
      } catch (e) {
        $.define.func.showMessage($.store.app.strings.error_try_again)
      }
    }

    @action.bound
    async searchFriendAccount (type, value) {
      let searchStr = ''
      if (type === 'id') {
        searchStr = `&_id=${value}`
      }
      try {
        const _accounts = await $.method.session.User.Get(`v1/search?country=CN${searchStr}`)
        if (_accounts.length === 0) {
          return [];
        }
        else{
          console.log(_accounts)
          return _accounts;
        }
      } catch (e) {
        $.define.func.showMessage($.store.app.strings.error_try_again)
      }
    }

    @action 
    setCirclePoint (value = []) {
      this.circle_points = Array.isArray(value) ? value : []
    }

    setLocations(list) {
      try {
        const locations = list.filter(pipe => {
          return pipe.availability && pipe.latitude !== null && pipe.longitude !== null && !isNaN(Number(pipe.latitude)) && !isNaN(Number(pipe.longitude))
        }).map(pipe => {
          return { 
            _id: pipe._id, 
            type: 'rookie', 
            coords: { 
              latitude: parseFloat(pipe.latitude), 
              longitude: parseFloat(pipe.longitude) 
            } ,
            marker: pipe.marker
          }
        })

        this.setCirclePoint(locations)
      } catch (err) {
        console.log('[ERROR] setLocations', err)
      }
    }
    
    @action
    async searchFriend (value, online = false, driver = false) {
      let data = {}
      try {
        if(online === true && driver === false){
          data = await $.method.session.Circle.Get(`v1/circleSearch?fullName=${value}&onlineStatus=user-online`)
        }
        else if(online === false && driver === true){
          data = await $.method.session.Circle.Get(`v1/circleSearch?fullName=${value}&driversOnly=true`)
        }
        else if(online === true && driver === true){
          data = await $.method.session.Circle.Get(`v1/circleSearch?fullName=${value}&driversOnly=true&onlineStatus=user-online`)
        }
        else{
          data = await $.method.session.Circle.Get(`v1/circleSearch?fullName=${value}`)
        }
      } catch (e) {
        $.define.func.showMessage($.store.app.strings.error_try_again)
      }
      this.friendData.search_friends = data.friends || []
    }
    
    @action
    async searchGroup (value) {
      try {
        const _groups = await $.method.session.Circle.Get(`v1/groupSearch?name=${value}`)
        if (_groups) this.groupData.searchGroups = _groups
      } catch (e) {
        $.define.func.showMessage($.store.app.strings.error_try_again)
      }
    }
    
    @action.bound
    async sendRequest (invite_id, back = false) {
      try {
        const response = await $.method.session.Circle.Post('v1/requests', { addFriend_id: invite_id })
        $.define.func.showMessage($.store.app.strings.already_send_wait_confirm)
        back && global.$.navigator.dispatch(NavigationActions.back())
      } catch (e) {
        if (e.response && e.response.data && e.response.data.code === 'CIRCLE_REQUEST_EXIST') {
          $.define.func.showMessage($.store.app.strings.already_send_wait_confirm)
        } else if (e.response && e.response.data && e.response.data.code === 'ALREADY_IN_CIRCLE') {
          $.define.func.showMessage($.store.app.strings.already_friend)
        } else {
          $.define.func.showMessage($.store.app.strings.error_try_again)
        }
      }
    }

    @action.bound
    async joinGroup (group_id: String, joinRequireApproval:Boolean, search_value:String) {
      try {
        const response = await $.method.session.Circle.Post(`v1/joinGroup/${group_id}`)
        if (joinRequireApproval) {
          $.define.func.showMessage($.store.app.strings.join_sented)
        } else {
          $.define.func.showMessage($.store.app.strings.join_success)
          search_value && $.store.circle.searchGroup(search_value)
          $.store.circle.getGroups()
        }
      } catch (e) {
        if (e.response && e.response.data.code === 'PENDING_REQUEST_EXIST') {
          $.define.func.showMessage($.store.app.strings.join_sented)
        } else {
          $.define.func.showMessage($.store.app.strings.error_try_again)
        }
      }
    }
    
    @action
    async getFriends (type = 'all', verify = false) {
      try {
        if (type === 'all' && verify === false) {
          this.circleLoading = true
          const _friends = await $.method.session.Circle.Get('v1/circle')
          if (_friends.length > 0) {
            $.store.circle.filter = true
            this.friendData.friends = _friends
            this.friendData.friend_lists = _friends
            this.friendData.friend_count = _friends.length

          }
          else {
            $.store.circle.filter = false
            this.friendData.friends = _friends
            this.friendData.friend_lists = _friends
          }
        }
        else if (type === 'online' && verify === false) {
          if($.store.circle.filter === true){
            this.circleLoading = true
            const _friends = await $.method.session.Circle.Get('v1/circle?onlineStatus=user-online')
            if (_friends.length > 0) {
              this.friendData.friend_lists = _friends
            }
            else {
              this.friendData.friend_lists = _friends
            }
          }
        }
        else if (type === 'all' && verify === true) {
          if($.store.circle.filter === true){
            this.circleLoading = true
            const _friends = await $.method.session.Circle.Get('v1/circle?driversOnly=true')
            if (_friends.length > 0) {
              this.friendData.friend_lists = _friends
            }
            else {
              this.friendData.friend_lists = _friends
            }
          }
        }
        else if (type === 'online' && verify === true) {
          if($.store.circle.filter === true){
            this.circleLoading = true
            const _friends = await $.method.session.Circle.Get('v1/circle?onlineStatus=user-online&driversOnly=true')
            if (_friends.length > 0) {
              this.friendData.friend_lists = _friends
            }
            else {
              this.friendData.friend_lists = _friends
            }
          }
        }
      } catch (e) {
        $.define.func.showMessage($.store.app.strings.error_try_again)
      } finally {
        this.circleLoading = false
      }
    }
    
    @action
    async getMembers (group_id: String, limit = 50) {
      const nextData = Object.assign({}, this.groupData.members)
      const currentMembers = this.groupData.members[group_id] || []
      const currentLength = currentMembers.length
      try {
        const _members = await $.method.session.Circle.Get(`v1/groupMembers/${group_id}?skip=${currentLength}&limit=${limit}`)
        //const _members = await $.method.session.Circle.Get(`v1/groupMembers/${group_id}?limit=${limit}`)
        // if(currentLength == 0){
        // nextData[group_id] = [...currentMembers, ...(_members || [])]
        // this.groupData.members = nextData
        // // }
        // // else if(currentLength > 0 && _members.length !== currentLength){
          nextData[group_id] = Object.values(currentMembers.concat(_members).reduce((r,o) => {r[o.user_id] = o;
            return r;
          },{}));
          this.groupData.members = nextData

        // }
        return _members;
      } catch (e) {
        $.define.func.showMessage($.store.app.strings.timeout_try_again)
      }
    }
    
    @action
    async getGroupInfo (group_id: String) {
      let _groupInfo = {}
      try {
        _groupInfo = await $.method.session.Circle.Get(`v1/groups/${group_id}`)
      } catch (e) {
        $.define.func.showMessage($.store.app.strings.error_try_again)
      }
      this.groupData.groupInfo = _groupInfo
    }
    
    @action
    async getNewMembers (group_id: String, limit = 50) {
      const nextData = Object.assign({}, this.groupData.newMembers)
      const currentNewMembers = this.groupData.newMembers[group_id] || []
      const currentLength = currentNewMembers.length
      try {
        const _newMembers = await $.method.session.Circle.Get(`v1/groupRequests/${group_id}?skip=${currentLength}&limit=${limit}`)
        nextData[group_id] = [...currentNewMembers, ...(_newMembers || [])]
        this.groupData.newMembers = nextData
        return _newMembers;
      } catch (e) {
        $.define.func.showMessage($.store.app.strings.error_try_again)
        return null;
      }
    }
    
    @action
    async searchGroupMembersAndRequests(
      group_id: String,
      text: String,
      skip: Number = 0,
      limit: Number = 25,
      includeUsers: Boolean = true,
      includeRequests: Boolean = false
    ) {
      if (!text || text.length < 3) {
        return {
          requests: [],
          members: []
        }
      }

      try {
        const result = await $.method.session.Circle.Get(
          `v1/searchGroupUsers/${group_id}?text=${text}&skip=${skip}&limit=${limit}&includeUsers=${includeUsers}&includeRequests=${includeRequests}`
        )
        return result;
      } catch (e) {
        $.define.func.showMessage($.store.app.strings.error_try_again)
        return null
      }

    }
    
    @action
    async getInvitations () {
      let _invitations = []
      try {
        _invitations = await $.method.session.Circle.Get('v1/groupsInvitations')
      } catch (e) {
        // $.define.func.showMessage($.store.app.strings.error_try_again)
      }
      this.groupData.invitations = _invitations
    }
    
    @computed
    get getInviteUsers () {
      const { groupInfo } = this.groupData
      const { members } = this.groupData
      const currentList = members[groupInfo._id]
      let _inviteUsers = this.friendData.friends.map(pipe => Object.assign({}, pipe, {
        ismember: currentList.find(sub => sub.user_id === pipe.friend_id) !== undefined
      }))
      return _inviteUsers
    }

    @computed
    get getAllGroups () {
      const { groups_official = [], groups_my = [], groups_featured = [] } = this.groupData
      const groups = groups_official.concat(groups_my, groups_featured)
      return groups
    }
    
    @action
    async getFriendRequests () {
      let _requestors = []
      try {
        _requestors = await $.method.session.Circle.Get('v1/requests?skip=0&limit=20')
      } catch (e) {
        // $.define.func.showMessage($.store.app.strings.error_try_again )
      }
      this.friendData.requestors = _requestors
    }
    
    @action.bound
    async friendRequest (action: String, requestor_id: String, goback: Boolean = false) {
      const { strings } = $.store.app
      try {
        const data = await $.method.session.Circle.Put(`v1/requests/${requestor_id}`, { action: action })
      } catch (e) {
        $.define.func.showMessage(strings.timeout_try_again)
      } finally {
        try {
          const _requestors = await $.method.session.Circle.Get('v1/requests?skip=0&limit=20')
          const _friends = await $.method.session.Circle.Get('v1/circle')
          if (_requestors) this.friendData.requestors = _requestors
          if (_friends) this.friendData.friends = _friends
        } catch (e) {
          // $.define.func.showMessage( strings.timeout_try_again )
        }
        if (goback) global.$.navigator.dispatch(NavigationActions.back())
      }
    }
    
    @action
    async groupRequest (action: String, requestor_id: String) {
      try {
        const data = await $.method.session.Circle.Put(`v1/requests/${requestor_id}`, { action: action })
        try {
          const _invitations = await $.method.session.Circle.Get('v1/groupsInvitations')
          const _groups = await $.method.session.Circle.Get('v1/groups')
          if (_invitations) this.groupData.invitations = _invitations
          if (_groups) {
            this.groupData.groups_featured = _groups.featured
            this.groupData.groups_official = _groups.reserved
            this.groupData.groups_my = _groups.own
            this.groupData.groups = _groups
          }
        } catch (e) {
          $.define.func.showMessage($.store.app.strings.timeout_try_again)
        }
      } catch (e) {
        console.log('groupRequest:', e)
      }
    }

    @action.bound
    async joinGroupRequest (action: String, request_id: String, group_id: String) {
      try {
        const result = await $.method.session.Circle.Put(`v1/requests/${request_id}`, { action: action })
        if (result && true === result.isSuccess) this.removeJoinGroupRequestLocally(group_id, request_id);
        return result;
      } catch (e) {
        if (e && e.data && e.data.code === 'DATA_NOT_FOUND') {
          this.removeJoinGroupRequestLocally(group_id, request_id, type);
        }
        console.log('groupRequest:', e)
      }
    }

    @action 
    removeJoinGroupRequestLocally(group_id, request_id) {
      const requestIdx = (this.groupData.newMembers[group_id]).findIndex((i) => i._id === request_id)
      if (requestIdx >= 0) this.groupData.newMembers[group_id].splice(requestIdx, 1);
    }
    
    @action
    async updateMemberInfo (info: Object, member_id: String, group_id: String) {
      const nextData = Object.assign({}, this.groupData.members)
      try {
        const data = await $.method.session.Circle.Put(`v1/groupMembership/${member_id}`, info)
        if (data && data.isSuccess) {
          const _members = await $.method.session.Circle.Get(`v1/groupMembers/${group_id}`)
          if (_members) {
            nextData[group_id] = _members || []
            this.groupData.members = nextData
          }
          const _groups = await $.method.session.Circle.Get('v1/groups')
          if (_groups) {
            this.groupData.groups = _groups
            this.groupData.groups_featured = _groups.featured
            this.groupData.groups_official = _groups.reserved
            this.groupData.groups_my = _groups.own
          }
          return true
        }
      } catch (e) {
        $.define.func.showMessage($.store.app.strings.timeout_try_again)
        return false
      }
    }

    @action
    async uploadGroupAvatar (group_id: String, base64: String) {
      const { strings } = $.store.app
      try {
        const data = await $.method.session.Circle.Post(`v1/groupImage/${group_id}/avatars`, {
          data: `data:image/jpeg;base64,${base64}`
        })
        if (data.isSuccess) this.groupData.groupInfo = data.group
        try {
          const _groups = await $.method.session.Circle.Get('v1/groups')
          if (_groups) {
            this.groupData.groups = _groups
            this.groupData.groups_featured = _groups.featured
            this.groupData.groups_official = _groups.reserved
            this.groupData.groups_my = _groups.own
          }
        } catch (e) {
          console.log('getInvitations:', e)
        }
      } catch (e) {
        $.define.func.showMessage(strings.timeout_try_again)
      }
    }

    @action
    async leaveGroup (group_id: String) {
      const { strings } = $.store.app
      try {
        const data = await $.method.session.Circle.Put(`v1/leaveGroup/${group_id}`)
        try {
          // const _invitations = await $.method.session.Circle.Get('v1/groupsInvitations')
          const _groups = await $.method.session.Circle.Get('v1/groups')
          // if(_invitations)this.groupData.invitations = _invitations
          if (_groups) {
            this.groupData.groups = _groups
            this.groupData.groups_featured = _groups.featured
            this.groupData.groups_official = _groups.reserved
            this.groupData.groups_my = _groups.own
          }
        } catch (e) {
          console.log('getInvitations:', e)
        }
        global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'Main' }))
      } catch (e) {
        $.define.func.showMessage(strings.timeout_try_again)
      }
    }
    
    @action
    async sendInvite (group_id: String, user_id: String) {
      try {
        let data = await $.method.session.Circle.Post(`v1/groupInvite/${group_id}`, { user_id: user_id })
        if (data.isSuccess) {
          $.define.func.showMessage($.store.app.strings.invite_success)
        }
      } catch (e) {
        if (e.response.data) {
          if (e.response.data.code === 'PENDING_INVITATION_EXIST') {
            $.define.func.showMessage($.store.app.strings.invite_success)
          }else {
            $.define.func.showMessage($.store.app.strings.invite_failed)
          }
        } else {
          $.define.func.showMessage($.store.app.strings.unable_connect_server_pls_retry_later)
        }
      }
    }
    
    @action.bound
    async getGroups () {
      try {
        this.circleLoading = true
        const _groups = await $.method.session.Circle.Get('v1/groups')
        if (_groups) {
          this.groupData.groups_featured = _groups.featured
          this.groupData.groups_official = _groups.reserved
          this.groupData.groups_my = _groups.own
          this.groupData.groups = _groups
        }
      } catch (e) {
        $.define.func.showMessage($.store.app.strings.timeout_try_again)
      } finally {
        this.circleLoading = false
      }
    }
    
    async getFareData () {
      let groups = this.groupData.groups_official

      const fareResponse = await $.store.passenger.getFareV2(groups, false)

      if (!fareResponse || !fareResponse.length) return

      const defaultGroup = $.store.circle.defaultGroup

      groups = groups.map(pipe => {
        const group = fareResponse.find(sub => sub.group_id === pipe._id)

        pipe.selected = !!defaultGroup ? defaultGroup._id === pipe._id : false
        pipe.amount = !!group ? group.amount : 0

        return pipe
      })

      this.groupData.groups_official = groups
    }

    async getGroupsFares () {
      try {
        const model = $.store.passenger.model

        const parameters = [
          {key: "from_lat", value: model.from.coords.lat},
          {key: "from_lng", value: model.from.coords.lng},
          {key: "destination_lat", value: model.destination.coords.lat},
          {key: "destination_lng", value: model.destination.coords.lng},
          {key: "distance", value: model.distance},
          {key: "booking_at", value: model.booking_at},
          {key: "promo_code", value: !!model.promo_code ? encodeURIComponent(model.promo_code) : ''}
        ]

        const groupFares = await Session.Circle.Get(`v1/userCampaignsFares?${parameters.map(o => `${o.key}=${o.value}`).join('&')}`)

        this.groupData.groupsFares = groupFares || []
        
      } catch (error) {
        console.log('getjoygroup :', error);
      }
    }

    async getGroupsFaresV2 () {
      try {
        const model = $.store.passenger.model

        const parameters = [
          {key: "from_lat", value: model.from.coords.lat},
          {key: "from_lng", value: model.from.coords.lng},
          {key: "destination_lat", value: model.destination.coords.lat},
          {key: "destination_lng", value: model.destination.coords.lng},
          {key: "type", value: model.type ? model.type : 'now'},
          {key: "distance", value: model.distance},
          {key: "booking_at", value: model.booking_at},
          {key: "promo_code", value: !!model.promo_code ? encodeURIComponent(model.promo_code) : ''}
        ]

        const campaigns = await Session.Circle.Get(`v2/campaigns?${parameters.map(o => `${o.key}=${o.value}`).join('&')}`)
        
        const campaignData = campaigns.map((campaign) => {
          campaign.campaign_info = Object.assign({}, campaign.campaign_info, campaign.campaign_info.ui)

          return campaign
        }).sort(function(a, b){
          return a.campaign_info.listingPriority - b.campaign_info.listingPriority
        })

        this.groupData.campaigns = campaignData.filter(campaign => campaign.campaign_info.groupOnline === true)
        this.groupData.featuredCampaigns = campaignData.filter(campaign => campaign.campaign_info.featured === true)
        this.groupData.groupsFares = campaignData.filter(campaign => campaign.campaign_info.myJoy === true)
        
      } catch (error) {
        console.log('getjoygroup :', error);
      }
    }

    async getCampaigns() {
      try {
        const model = $.store.passenger.model

        const parameters = [
          {key: "from_lat", value: model.from.coords.lat},
          {key: "from_lng", value: model.from.coords.lng},
          {key: "destination_lat", value: model.destination.coords.lat},
          {key: "destination_lng", value: model.destination.coords.lng},
          {key: "type", value: model.type ? model.type : 'now'},
          {key: "booking_at", value: model.booking_at},
          {key: "promo_code", value: !!model.promo_code ? encodeURIComponent(model.promo_code) : ''},
          {key: "distance", value: model.distance},
        ]

        const campaigns = await Session.Circle.Get(`v1/campaigns?${parameters.map(o => `${o.key}=${o.value}`).join('&')}`)

        const campaignData = campaigns.map((campaign) => {
          campaign.campaign_info = Object.assign({}, campaign.campaign_info, campaign.campaign_info.ui)

          return campaign
        }).sort(function(a, b){
          return a.campaign_info.listingPriority - b.campaign_info.listingPriority
        })

        this.groupData.campaigns = campaignData.filter(campaign => campaign.campaign_info.groupOnline === true)
        this.groupData.featuredCampaigns = campaigns.filter(group => group.campaign_info.featured === true)

      } catch (error) {
        console.log('getCampaigns :', error);
      }
    }

    async getUserCampaigns() {
      try {
        const campaigns = await Session.Circle.Get(`v1/userCampaigns`)

        this.groupData.userCampaigns = campaigns.map((campaign) => {
          return {
            _id: campaign._id,
            name: campaign.name,
            tagline: campaign.description,
            avatars: campaign.avatars,
            type: campaign.group_info.type,
            tnc: campaign.tnc
          }
        })

      } catch (error) {
        console.log('getUserCampaigns :', error);
      }
    }

    @action.bound
    async getCountries () {
      let _countries = []
      try {
        _countries = await $.method.session.Lookup.Get('v1/lookup/countryDivisions')
      } catch (e) {
        console.log('getCountries:', e)
      }
      this.groupData.countries = _countries
    }
    
    @action.bound
    async getCities (parent_id: String, keyword: String = '') {
      let _cities = []
      try {
        _cities = await $.method.session.Lookup.Get(`v1/lookup/countryDivisions?state=state&parent_id=${parent_id}&keyword=${keyword}`)
      } catch (e) {
        console.log('getCities:', e)
      }
      this.groupData.cities = _cities
    }
    
    @action.bound
    async createNewGroup (base64) {
      $.store.app.showLoading()
      try {
        let data = await $.method.session.Circle.Post('v1/groups', this.groupData.newGroup)
        if (data.isSuccess) {
          if (base64) await this.uploadGroupAvatar(data.group._id, base64)
          const _groups = await $.method.session.Circle.Get('v1/groups')
          if (_groups) {
            this.groupData.groups = _groups
            this.groupData.groups_featured = _groups.featured
            this.groupData.groups_official = _groups.reserved
            this.groupData.groups_my = _groups.own
            this.groupData.groups = _groups
          }
        }
        $.define.func.showMessage($.store.app.strings.create_group_success)
        global.$.navigator.dispatch(NavigationActions.back())
        this.groupData.newGroup = {}
      } catch (e) {
        if (e.response.data && e.response.code === 'GROUP_NAME_EXIST') {
          $.define.func.showMessage(e.response.message)
        } else {
          $.define.func.showMessage($.store.app.strings.create_group_failed)
        }
      } finally {
        $.store.app.hideLoading()
      }
    }

    @action
    async updateGroupDetail (group_id: String) {
      $.store.app.showLoading()
      try {
        console.log(this.groupData.newGroup)
        let data = await $.method.session.Circle.Put(`v1/groups/${group_id}`, this.groupData.newGroup)
        if (data.isSuccess) {
          $.define.func.showMessage($.store.app.strings.update_succ)
          const _groupInfo = await $.method.session.Circle.Get(`v1/groups/${group_id}`)
          if (_groupInfo) this.groupData.groupInfo = _groupInfo
          const _groups = await $.method.session.Circle.Get('v1/groups')
          if (_groups) {
            this.groupData.groups_featured = _groups.featured
            this.groupData.groups_official = _groups.reserved
            this.groupData.groups_my = _groups.own
            this.groupData.groups = _groups
          }
          this.groupData.newGroup = {}
          global.$.navigator.dispatch(NavigationActions.back())
        }
      } catch (e) {
        $.define.func.showMessage($.store.app.strings.error_try_again)
      } finally {
        $.store.app.hideLoading()
      }
    }

    @action.bound
    setValue (params) {
      this.friendData = Object.assign({}, this.friendData, params)
    }

    @action.bound
    toggerSelectAll () {
      let _clone = this.friendData.check_friends.slice()
      if (this.friendData.selectAll) {
        let _friend = _clone.map(pipe => Object.assign({}, pipe, {
          checked: false
        }))
        this.friendData.selectAll = false
        this.friendData.check_friends = _friend
      } else {
        let _friend = _clone.map(pipe => Object.assign({}, pipe, {
          checked: true
        }))
        this.friendData.selectAll = true
        this.friendData.check_friends = _friend
      }
    }

    @action.bound
    selectAllFriendToggle () {
      let _clone = this.friendData.check_all_friend.slice()
      console.log(_clone)
      if (this.friendData.allFriend) {
        let _friend = _clone.map(pipe => Object.assign({}, pipe, {
          checked: false
        }))
        this.friendData.allFriend = false
        this.friendData.check_all_friend = _friend
        
      } else {
        let _friend = _clone.map(pipe => Object.assign({}, pipe, {
          checked: true
        }))
        this.friendData.allFriend = true
        this.friendData.check_all_friend = _friend
      }
      let checkData = this.friendData.check_all_friend.filter(pipe => pipe.checked)
      if(checkData.length > 0){
        this.checkedData = true
      }
      else{
        this.checkedData = false
      }
    }

    @action.bound
    onPressCheck (itemData: Object, index: Int) {
      let nextSelectAll = this.friendData.selectAll
      let _clone = this.friendData.check_friends.slice()
      const nextSelect = !_clone[index].checked
      _clone[index].checked = nextSelect
      if (nextSelect) {
        nextSelectAll = _clone.filter(pipe => !pipe.checked).length === 0 && (!nextSelectAll)
      } else {
        nextSelectAll = false
      }
      this.friendData.selectAll = nextSelectAll
      this.friendData.check_friends = _clone
    }

    @action.bound
    onPressCheckFriend (itemData: Object, index: Int) {
      let nextSelectAll = this.friendData.allFriend
      let _clone = this.friendData.check_all_friend.slice()
      const nextSelect = !_clone[index].checked
      _clone[index].checked = nextSelect
      if (nextSelect) {
        nextSelectAll = _clone.filter(pipe => !pipe.checked).length === 0 && (!nextSelectAll)
      } else {
        nextSelectAll = false
      }
      this.friendData.allFriend = nextSelectAll
      this.friendData.check_all_friend = _clone
      let checkData = this.friendData.check_all_friend.filter(pipe => pipe.checked)
      if(checkData.length > 0){
        this.checkedData = true
      }
      else{
        this.checkedData = false
      }
      
    }

    @action.bound
    async sendSuggestFriend (id) {
      let selected_friends_id = []
      let _friends = this.friendData.check_all_friend
      _friends = _friends.filter(item => item.checked)
      this.friendData.friend_selected = _friends
      const friendIds = toJS(_friends).map(pipe => pipe.friend_id).join(',')
      selected_friends_id.push(friendIds.split(","))
      // console.log(selected_friends_id[0])
      const data = {id: id, sendTo: { ids: selected_friends_id[0]}}
      const result = await Session.Circle.Post(`v1/sendFriendSuggest`, data)
      if(result && result.isSuccess){
        return true
      }
      else{
        return false
      }

    }

    @action.bound
    selectGroup (itemData: Object) {
      const { check_group = [], select_group = {} } = this.groupData

      let _groups = check_group.map(pipe => Object.assign({}, pipe, {
        checked: pipe._id === itemData._id && !pipe.checked
      }))
      this.groupData.check_group = _groups

      if (!!$.store.passenger.selectedCircle && $.store.passenger.selectedCircle === 'group') {
        if (!!select_group && select_group._id === itemData._id) {
          this.groupData.select_group.checked = !select_group.checked
        }
      }
    }
    
    @action.bound
    selectGroupNew (itemData: Object) {
      let _groups = this.groupData.searchGroups.map(pipe => Object.assign({}, pipe, {
        checked: pipe._id === itemData._id && !pipe.checked
      }))
      this.groupData.searchGroups = _groups
    }

    async getDefaultGroup() {
      let _group = await $.method.session.Circle.Get(`v1/defaultBookingGroup?country=MY`)

      this.defaultGroup = _group
    }

    @action
    async setDefaultGroup() {
      if (this.defaultGroup && !this.defaultGroup.hasOwnProperty('type')) await this.getDefaultGroup()
      
      let _group = this.defaultGroup

      this.groupData.select_group = _group
      this.friendData.select_friends = []
      this.friendData.selectAll = false

      this.groupData.groups_official = this.groupData.groups_official.map((pipe, key) => Object.assign({}, pipe, {
        selected: _group._id === pipe._id
      }))

      this.groupData.campaigns = this.groupData.campaigns.map((pipe, key) => Object.assign({}, pipe, {
        selected: false
      }))

      let vehicleId = _group.type === 'reserved' ? _group.options.vehicle_category_id : _group._id

      let url = _group.type === 'reserved'
        ? `v1/official_group_drivers?vehicleCategory_id=${_group.options.vehicle_category_id}`
        : `v1/community_group_drivers?group_id=${_group._id}`

      try {
        const groups = await Session.Location.Get(url)

        this.setLocations(groups[vehicleId])

        if ($.store.passenger.type) $.store.passenger.getFare()
      } catch (e) {
        console.log(e)
      }
    }

    @action.bound 
    async confirmFriendSelect () {
      let _friends = this.friendData.check_friends
      
      _friends = _friends.filter(item => item.checked)
      
      this.friendData.select_friends = _friends
      if (this.groupData.select_group) {
        this.groupData.select_group = {}
      }

      $.store.passenger.assign_type = 'selected_circle'
      $.store.passenger.campaign_id = ""
      $.store.passenger.campaign_info = {}

      const friendIds = toJS(_friends).map(pipe => pipe.friend_id).join(',')
      try {
        const friends = await Session.Location.Get(`v1/friends?reqUser_id=${friendIds}`)
        
        this.setLocations(friends)
      } catch (e) {
        console.log(e)
      }

      $.store.passenger.getFare()
      global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'Main'}))
    }

    @action.bound 
    async confirmGroupSelect (isSearch, searchGroups) {
      let _group = isSearch ? searchGroups : this.groupData.check_group
      
      _group = _group.filter(item => item.checked).map(pipe => Object.assign({}, _.omit(pipe, 'checked')))
      
      if (_group.length > 0) {
        this.groupData.select_group = _group[0]
        if (this.friendData.select_friends) {
          this.friendData.select_friends = []
          this.friendData.selectAll = false
        }

        let vehicleId = _group[0].type === 'reserved' ? _group[0].options.vehicle_category_id : _group[0]._id
        let url = _group[0].type === 'reserved'
          ? `v1/official_group_drivers?vehicleCategory_id=${_group[0].options.vehicle_category_id}`
          : `v1/community_group_drivers?group_id=${_group[0]._id}`

        try {
          const groups = await Session.Location.Get(url)

          this.setLocations(groups[vehicleId])
        } catch (e) {
          console.log(e)
        }
      }

      global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'Main'}))
      if ($.store.passenger.type) $.store.passenger.getFare()
    }

    @action.bound 
    async setSelectedGroup(group, advanceOption = false) {
      group._id = !!group.group_id ? group.group_id : group._id
      group.name = !!group.campaign_id ? group.campaign_info.name : group.group_name ? group.group_name : group.name
      group.avatars = !!group.campaign_id ? group.campaign_info.avatars : !!group.avatars ? group.avatars : group.group_info.avatars  

      AnswersSrv.logCustom(`Selected - ${group.name}`)

      $.store.passenger.assign_type = group.type === 'reserved' ? 'reserved_group' : 'community_group'
      $.store.passenger.campaign_id = !!group.campaign_id ? group.campaign_id : ''
      $.store.passenger.campaign_info = !!group.campaign_info ? group.campaign_info : {}

      console.log('group', group)
      this.groupData.select_group = group
      this.friendData.select_friends = []
      this.friendData.selectAll = false

      let vehicleId = group.type === 'reserved' ? group.options.vehicle_category_id : group._id
      let url = group.type === 'reserved'
        ? `v1/official_group_drivers?vehicleCategory_id=${group.options.vehicle_category_id}`
        : `v1/community_group_drivers?group_id=${group._id}`

      try {
        const groups = await Session.Location.Get(url)

        this.setLocations(groups[vehicleId])

        $.store.passenger.getFare()
        global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'Main'}))
        this.advanceOptions = advanceOption // visibility of timepicker
        this.isAdvanceCampaign = advanceOption //validation of advance booking flag
      } catch (e) {
        console.log(e)
      }
    }

    @action.bound
    async delFriend (_id) {
      const { strings } = $.store.app
      try {
        await $.method.session.Circle.Delete(`v1/circle/${_id}`)
        Alert.alert(strings.finish, strings.already_del_friend, [{
          text: strings.confirm,
          onPress: () => {
            this.friendData.friends = this.friendData.friends.filter(item => item._id !== _id) // 直观表现，可以删除
            $.store.chat.removeChatMsg(`friend_${_id}`)
            global.$.navigator.dispatch(StackActions.reset({
              index: 0,
              actions: [NavigationActions.navigate({ routeName: 'Main' })]
            }))
          }
        }])
      } catch (e) {
        console.log(e)
        $.define.func.showMessage(strings.timeout_try_again)
      }
    }

  @action.bound async checkPermission (group_id) {
      const { strings } = $.store.app
      try {
        const resp = await $.method.session.Circle.Get(`v1/userGroupPermissions/${group_id}`)
        return resp.isMember
      } catch (e) {
        console.log(e)
        $.define.func.showMessage(strings.timeout_try_again)
        return -1
      }
    }

  @action.bound
  async changeMemberShip (_id, params = {}, group_id) {
    const { strings } = $.store.app
    try {
      const resp = await $.method.session.Circle.Put(`v1/groupMembership/${_id}`, params)
      if (resp && resp.isSuccess && resp.doc) {
        // update member options locally if his membership exists
        const currentMembers = this.groupData.members[group_id] || []
        const memberIdx = currentMembers.findIndex(m => m.membership_id === resp.doc._id)
        if (memberIdx >= 0) {
          if (params.status === 'inactive') {
            this.groupData.members[group_id].splice(memberIdx, 1)
          } else {
            this.groupData.members[group_id][memberIdx].membershipOptions = resp.doc.membershipOptions
          }
        }
      }
      console.log(resp)
      return true
    } catch (e) {
      console.log(e)
      $.define.func.showMessage(strings.timeout_try_again)
      return false
    }
  }

    @action.bound
    async isMember (_id) {
      try {
        const resp = await $.method.session.Circle.Get(`v1/userGroupStatus/${_id}`)
        this.groupData.memberShipType = resp
      } catch (e) {
        console.log(e)
        this.groupData.memberShipType = {}
      }
    }
  },
  Autorun: (store) => {}
}
