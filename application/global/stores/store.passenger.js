// @flow
import { Alert, Platform } from 'react-native'
import { observable, action, autorun, computed, toJS } from 'mobx'
import BaseStore from './base.store'
import Session from '../method/session'
import { toastShort, toastLong } from '../method/ToastUtil'
import { NavigationActions, StackActions } from 'react-navigation'

import moment from 'moment'

export default {
  Name: 'passenger',
  Store: class PassengerStore extends BaseStore {
    constructor (store) {
      super(store)
    }

    @observable mapRefs: Object = {}

    @observable event_disable = true 
    
    @observable status: Number = STATUS_DEFINE.PICK_ADDRESS

    @observable id: String = ''

    @observable booking_code: String = ''
    
    @observable booking_status: String = ''

    @observable destination: AddressModel = null

    @observable from: AddressModel = null

    @observable destination_directions = {}

    @observable driver_directions = {}

    @observable booking_at: String = ''

    @observable payment_method: String = 'Cash'

    @observable fare: Number = -1

    @observable fareLoading: Boolean = false

    @observable routeSummary = ''

    @observable additional_fees: Number = 0

    @observable assign_type: String = ''

    @observable vehicle_category_id: String = ''

    @observable include_parameter: Any = ''

    @observable type: String = 'now'

    @observable favorite: Array = []

    @observable driver_info: Object = {}

    @observable vehicle_info: Object = {}

    @observable places: Array = []

    @observable notes: String = ''

    @observable driver_id: String = ''

    @observable joy_location_id: String = ''

    @observable joy_location_info: Object = {}

    @observable driverLocation: Object = {}

    @observable driverETA: Number = 0

    @observable timerStartedAt = null

    @observable timerSeconds: Number = 0

    @observable moveMapFlag: Boolean = false

    @observable sponsorVisible: Boolean = false

    @observable shownSponsor: Boolean = false

    @observable bookingTimer = null

    @observable blockReasons: Array = []

    @observable promo_code: String = ''

    @observable promo_info: Object = {}

    @observable promoModalVisible: Boolean = false

    @observable fare_info: Object = {}

    @observable promoInfoModalVisible: Boolean = false

    @observable advanceFareModalVisible: Boolean = false

    @observable advanceFareInfo: Object = {}

    @observable distance: Number = 0

    @observable fareChecksum: String = ''

    @observable fareObj: Object = {}

    @observable chatButton: Boolean = true

    @observable loaded: Boolean = false

    @observable campaign_id: String = ''

    @observable campaign_info: Object = {}

    @observable campaignErrorModalVisible: Boolean = false

    @observable campaignInfo: Object = {}

    @observable day: Number = 0

    @observable noDriverModal: Boolean = false

    @observable group_info: Object = {}
    
    @observable groupName: String = ''
    
    @observable driverFoundModal: Boolean = false

    @observable countDriver: Boolean = false

    @action 
    changeStatus (status) {
      this.status = status
    }

    @action
    setEventDisable (disable) {
      this.event_disable = disable
    }

    @action
    async bookingRating (rating) {
      try {
        await Session.Rating.Post('v1', { booking_id: this.id, rating})
      } catch (e) {
        console.log(e)
      }
    }

    @action
    async getNearbyDrivers () {
      try {
        let { longitude, latitude } = this.from.coords

        const drivers = await $.method.session.Location.Get(`v1/all_drivers?latitude=${latitude}&longitude=${longitude}`)

        $.store.circle.setLocations(drivers)
      } catch (e) {
        // $.define.func.showMessage($.store.app.strings.timeout_try_again)
      }
    }

    @action
    async setAddressCoords (coords) {
      this.from = new AddressModel({}, coords)

      this.getNearbyDrivers()
    }

    @action
    async getAddressFromCoords (coords) {
      const { longitude, latitude } = coords
      try {
        this.from = null
        const currents = await Session.Lookup.Get(
          `v1/lookup/geocodes?latlng=${latitude},${longitude}`
        )

        let location = currents.results[0]

        location.name = 'Pin Location'
        location.coords = coords

        this.from = new AddressModel(location, coords)
      } catch (e) {
        this.from = new AddressModel({}, coords)
      } finally {
        // this.event_disable = false
      }

      this.getNearbyDrivers()
    }

    @action
    async getAddressFromPlaces(address) {
      this.event_disable = true

      response = await Session.Lookup.Get(`v1/lookup/places/${address.placeId}`)
      const ress = {
        address: response.address,
        coords: {
          latitude: response.coords.lat,
          longitude: response.coords.lng
        },
        name: response.name,
        placeId: response._id
      }

      this.from = new AddressModel(ress)

      if (this.destination) {
        global.$.navigator.dispatch(
          NavigationActions.navigate({routeName: 'CircleSelect'})
        )

        this.destination_directions = await this.directions(
          this.from.coords,
          this.destination.coords
        )
        this.getFare()
        this.mapRefs.handle.moveTo($.method.utils.getRegion([this.from.coords, this.destination.coords]))
      } else {
        this.getNearbyDrivers()
        this.mapRefs.handle.moveTo(this.from.coords)
      }

      setTimeout(() => { this.event_disable = false }, 1000)
    }

    @action
    async setFromAddress(coords) {
      const placeId = this.from && this.from.placeId ? this.from.placeId : ''

      if (placeId) {
        await this.getAddressFromPlaces(this.from)
      } else {
        await this.getAddressFromCoords(coords)
      }
    }

    @action
    async setDestinationAddress(address) {
      this.event_disable = true
      const response = await Session.Lookup.Get(`v1/lookup/places/${address.placeId}`)
      const ress = {
        address: response.address,
        coords: {
          latitude: response.coords.lat,
          longitude: response.coords.lng
        },
        name: response.name,
        placeId: response._id
      }

      this.joy_location_id = ''
      this.destination = new AddressModel(ress)

      global.$.navigator.dispatch(
        NavigationActions.navigate({routeName: 'CircleSelect'})
      )

      this.status = STATUS_DEFINE.PASSENGER_CONFIRM
      this.destination_directions = await this.directions(
        this.from.coords,
        this.destination.coords
      )

      $.store.circle.setDefaultGroup()

      this.mapRefs.handle.moveTo($.method.utils.getRegion([this.from.coords, this.destination.coords]))
    }

    @action
    async getBlockReasons(blockType){
      try{
        this.blockReasons = await Session.Lookup.Get('v1/lookup/feedbacks?type='+blockType)
      } catch (e) {
        console.log(e)
      }
    }

    async onNearBy (keywords, coords) {
      const { latitude, longitude } = coords
      const response = await Session.GoogleMap.Get(
        `place/nearbysearch/json?language=en&location=${latitude},${longitude}&radius=${1000 *
        500}&key=${$.config.env.google_key}`
      )
      const places = (response.results || [])
        .filter(
          pipe =>
            pipe.geometry &&
            pipe.geometry.location &&
            pipe.geometry.location.lng &&
            pipe.geometry.location.lat
        )
        .map(pipe => {
          return {
            placeId: pipe.place_id,
            name: pipe.name,
            address: pipe.vicinity,
            coords: {
              longitude: parseFloat(pipe.geometry.location.lng),
              latitude: parseFloat(pipe.geometry.location.lat)
            }
          }
        })
      if (this.favorite.length > 0) {
        for (var i = 0; i < this.favorite.length; i++) {
          for (var j = 0; j < places.length; j++) {
            if (this.favorite[i].placeId === places[j].placeId) {
              places[j] = Object.assign(places[j], { star: true })
            }
          }
        }
      }
      this.places = places || []
    }

    @action 
    async setJoyLocation (joy_location_details) {
      try {
        const { name, address, coords, _id: joy_location_id} = joy_location_details

        this.joy_location_id = joy_location_id

        this.event_disable = true

        const location_coords = {
          latitude: coords.lat ? coords.lat : coords.latitude,
          longitude: coords.lng ? coords.lng : coords.longitude
        }

        const ress = {
          joy_location_id: joy_location_id,
          address: address,
          coords: location_coords,
          name: name,
          placeId: ''
        }

        this.destination = new AddressModel(ress, location_coords)

        global.$.navigator.dispatch(
          NavigationActions.navigate({routeName: 'CircleSelect'})
        )

        this.status = STATUS_DEFINE.PASSENGER_CONFIRM
        this.destination_directions = await this.directions(
          this.from.coords,
          this.destination.coords
        )

        $.store.circle.setDefaultGroup()
      } catch (e) {
        console.log(e)
      }
    }

    @action
    async updateJoyLocation (joy_location_details) {
      try {
        const { name, address, coords, _id: joy_location_id} = joy_location_details
        const bookingId = $.store.passenger.id

        this.joy_location_id = joy_location_id

        $.store.app.showLoading()
        const response = await Session.Booking.Put(`v1/${bookingId}`, { action: 'update_joy', joy_location_id: joy_location_id})
        if (response.isSuccess) {
          $.store.app.hideLoading()
          this.recoverBooking()
        }
        $.store.app.hideLoading()
      } catch (e) {
        $.store.app.hideLoading()
        console.log(e)
      }
    }

    @action
    async enterPromo() {
      try {
        if (!!this.campaign_id && !this.campaign_info.allowPromo) {
          Alert.alert('', $.store.app.strings.campaign_promo_error)
        } else {
          $.store.passenger.promo_code = ''
          $.store.passenger.promo_info = {}
          $.store.passenger.promoModalVisible = true
        }
      } catch (e) {
        console.log(e)
      }
    }

    @action
    async applyPromo(promo_code) {
      try {
        this.promo_code = promo_code.toUpperCase()
        this.promoModalVisible = false

        this.getFare()
      } catch (e) {
        console.log(e)
      }
    }

    @action
    async showPromo () {
      try {
        if (!this.promo_info) return

        $.store.passenger.promoInfoModalVisible = true
        $.store.passenger.promo_info = Object.assign(this.promo_info, {
          cancelLabel: $.store.app.strings.okay.toUpperCase(),
          onCancel: () => {
            $.store.passenger.promoInfoModalVisible = false
          },
          confirmLabel: $.store.app.strings.clear_code.toUpperCase(),
          onConfirm: () => {
            $.store.passenger.promoInfoModalVisible = false
            this.promo_code = ''
            this.getFare()
          } 
        })
      } catch (e) {
        console.log(e)
      }
    }

    @action
    async showPromoSummary () {
      try {
        if (!this.promo_info) return

        $.store.passenger.promoInfoModalVisible = true
        $.store.passenger.promo_info = Object.assign(this.promo_info, {
          confirmLabel: $.store.app.strings.okay.toUpperCase(),
          onConfirm: () => {
            $.store.passenger.promoInfoModalVisible = false
          } 
        })
      } catch (e) {
        console.log(e)
      }
    }

    // TODO: Fix Data Struct
    async onAutoComplete(keywords, coords) {
      const { latitude, longitude } = coords
      const response = await Session.Lookup.Get(
        `v1/lookup/places?latitude=${latitude}&longitude=${longitude}&keyword=${keywords}`
      )

      const placeDetail = response.map((prediction) => {
        return {
          placeId: prediction._id,
          name: prediction.name,
          address: prediction.address,
          hasSubplace: prediction.hasSubplace,
          level: prediction.level
          // coords: { latitude: 0, longitude: 0 },
          // distance: 0
        }
      })

      if (this.favorite.length > 0) {
        for (var i = 0; i < this.favorite.length; i++) {
          for (var j = 0; j < placeDetail.length; j++) {
            if (this.favorite[i].placeId === placeDetail[j].placeId) {
              placeDetail[j] = Object.assign(placeDetail[j], { star: true })
            }
          }
        }
      }
      this.places = placeDetail || []
    }

    async directions (from, destination) {
      const response = await Session.Lookup.Get(
        `v1/lookup/directions?origin=${from.latitude},${from.longitude}&destination=${destination.latitude},${destination.longitude}`
      )

      const direction = {}

      try {
        const bestWay = response.routes[0]

        direction.bounds = bestWay.bounds
        direction.distance = bestWay.legs[0].distance
        direction.duration = bestWay.legs[0].duration
        direction.routes = bestWay.legs[0].steps
          .map(pipe => AddressDispose.decode(pipe.polyline.points))
          .reduce((prev, next) => prev.concat(next))
          .map(pipe => ({ latitude: pipe[0], longitude: pipe[1] }))
      } catch (e) {
        console.log(e)
      }

      return direction
    }

    @computed
    get model () {
      const key =
        typeof this.include_parameter === 'string'
          ? 'group_id'
          : 'selected_circle_ids'

      const model = {
        from: this.from.model,
        destination: this.destination.model,
        notes: this.notes,
        fare: this.fare,
        type: this.type,
        booking_at: this.booking_at,
        payment_method: this.payment_method,
        [key]:
          typeof this.include_parameter === 'string'
            ? this.include_parameter
            : this.include_parameter.slice(),
        assign_type: this.assign_type,
        routeSummary: this.routeSummary,
        promo_code: this.promo_code,
        distance: this.distance,
        fareChecksum: this.fareChecksum
      }

      if (this.joy_location_id) {
        model.joy_location_id = this.joy_location_id
      }

      if (typeof this.include_parameter !== 'string') {
        model.vehicle_category_id = this.vehicle_category_id
      }

      if (this.campaign_id) {
        model.campaign_id = this.campaign_id
        delete model.group_id
      }

      return model
    }

    @action 
    changeMoveFlag (flag) {
      this.moveMapFlag = flag
    }

    @action.bound
    reset () {
      this.status = STATUS_DEFINE.PICK_ADDRESS
      this.from = null
      this.destination = null
      this.event_disable = false
      this.driver_directions = {}
      this.destination_directions = {}
      this.driverLocation = {}
      this.driverETA = 0
      this.fare = 0
      this.fareObj = {}
      this.additional_fees = 0
      this.type = 'now'
      this.assign_type = ''
      this.shownSponsor = false
      this.promo_code = ''
      this.promo_info = {}
      this.fare_info = {}
      this.distance = 0
      this.fareChecksum = ''
      this.joy_location_id = ''
      this.joy_location_info = {}
      this.id = ''
      this.advanceFareModalVisible = false
      this.advanceFareInfo = {}
      this.campaign_id = ''
      this.campaign_info = {}
      this.campaignErrorModalVisible = false
      this.campaignInfo = {}
      this.group_info = {}
      this.groupName = ''
      this.countDriver = false
      this.driverFoundModal = false
      this.noDriverModal = false
      $.store.circle.getGroups()
      $.store.circle.friendData.select_friends = []
      $.store.circle.friendData.selectAll = false
      $.store.circle.groupData.select_group = {}
      $.store.circle.circle_points = []
      $.store.circle.isAdvanceCampaign = false

      this.setAddressCoords($.store.app.coords)
      this.moveMapFlag = true
    }

    async getFare () {
      if (this.fareLoading) return
      this.fareLoading = true
      this.fare = 0

      try {
        if (this.assign_type === 'selected_circle') {
          const vehicleCategories = await Session.Lookup.Get('v1/lookup/vehicleCategories?country=MY&type=circle')
          const vehicle = vehicleCategories.find(
            pipe => pipe.name === 'My Circle'
          )
          this.vehicle_category_id = vehicle._id
          this.include_parameter = $.store.circle.friendData.select_friends.map(pipe => pipe.friend_id)

        } else {
          this.include_parameter = $.store.circle.groupData.select_group._id
        }
      } catch (e) {
        console.log(e)
        this.status = STATUS_DEFINE.PASSENGER_CONFIRM
        $.define.func.showMessage($.store.app.strings.select_friends_or_community) // timeout_try_again
        this.fareLoading = false
        return
      }

      try {
        if (this.type === 'now') this.booking_at = moment().toISOString()
        
        const parameters = [
          { key: 'type', value: this.type },
          { key: 'from_lat', value: this.from.coords.latitude },
          { key: 'from_lng', value: this.from.coords.longitude },
          { key: 'destination_lat', value: this.destination.coords.latitude },
          { key: 'destination_lng', value: this.destination.coords.longitude },
          { key: 'assign_type', value: this.assign_type },
          { key: 'booking_at', value: this.booking_at },
          { key: 'fareChecksum', value: this.fareChecksum }
        ]

        if (this.assign_type === 'selected_circle') {
          parameters.push({
            key: 'vehicle_category_id',
            value: this.vehicle_category_id
          })
        } else {
          parameters.push({ 
            key: 'group_id', 
            value: this.include_parameter 
          })
        }

        if (this.promo_code) {
          parameters.push({
            key: 'promo_code',
            value: encodeURIComponent(this.promo_code)
          })
        }

        if (this.campaign_id) {
          parameters.push({
            key: 'campaign_ids',
            value: this.campaign_id
          })
        }

        const fareResponse = await Session.Booking.Get(
          `v1/fares?${parameters.map(o => `${o.key}=${o.value}`).join('&')}`
        )

        const { distance = 0, fareChecksum = '', fare } = fareResponse
        const { amount, promo, minAdvanceBookingFare = 0 } = fare[0]

        const day = moment(this.booking_at).format('dddd')
        switch (day) {
          case 'Monday':
            this.day = 1
            break
          case 'Tuesday':
            this.day = 2
            break
          case 'Wednesday':
            this.day = 3
            break
          case 'Thursday':
            this.day = 4
            break
          case 'Friday':
            this.day = 5
            break
          case 'Saturday':
            this.day = 6
            break
          case 'Sunday':
            this.day = 7
            break
          default:
            break
        }

        this.distance = distance
        this.fareChecksum = fareChecksum
        this.fare = parseFloat(amount)

        if (this.promo_code && !!promo) {
          // Valid Promo Code
          if (this.promo_info && this.promo_info.hasOwnProperty('_id') && this.promo_info._id === promo._id) {
            this.routeSummary = fareResponse.routeSummary || ''
            this.fareLoading = false
            return
          }
          
          $.store.passenger.promoInfoModalVisible = true
          $.store.passenger.promo_info = Object.assign(promo, {
            cancelLabel: $.store.app.strings.not_now.toUpperCase(),
            onCancel: () => {
              $.store.passenger.promoInfoModalVisible = false
              this.promo_code = ''
              this.getFare()
            },
            confirmLabel: $.store.app.strings.use_code.toUpperCase(),
            onConfirm: () => {
              $.store.passenger.promoInfoModalVisible = false
            } 
          })
        }

        if (this.type === 'advance' && !!minAdvanceBookingFare && minAdvanceBookingFare <= amount && (!this.campaign_id || (this.campaign_info.assignBookingType === 'now' || !this.campaign_info.assignBookingType))) {
          this.advanceFareModalVisible = true
          this.advanceFareInfo = {
            title: $.store.app.strings.advance_title,
            desc_1: $.store.app.strings.advance_desc_1,
            desc_2: $.store.app.strings.advance_desc_2,
            minFare: minAdvanceBookingFare,
            cancelLabel: $.store.app.strings.cancel.toUpperCase(),
            onCancel:  () => {
              this.advanceFareModalVisible = false
              this.advanceFareInfo = {}
              this.type = 'now'
              this.getFare()
            },
            confirmLabel: $.store.app.strings.proceed.toUpperCase(),
            onConfirm:  () => {
              this.advanceFareModalVisible = false
              this.advanceFareInfo = {}
              } 
            }
          } else {
            this.advanceFareModalVisible = false
            this.advanceFareInfo = {}
          }

          if (this.type === 'advance') {
            if (this.campaign_info.activePeriodsType && this.campaign_info.activePeriodsType === 'selected' && this.campaign_info.activePeriods || this.campaign_info.assignBookingType === 'advanced'){
              const timePeriod = this.campaign_info['activePeriods']
              if (timePeriod) {
                const time = timePeriod[this.day] || []
                const bookingTime = parseInt(moment(this.booking_at).utc().format('HHmm'), 10);
                let found = false 

                for(let i = 0; i < time.length; i++) {
                  const { start, end } = time[i]
                  
                  if (start <= bookingTime && bookingTime <= end) {
                    found = true
                    break
                  }
                }
                if (!found || !time.length){
                  this.campaignErrorModalVisible = true
                  this.campaignInfo = {
                    onConfirm: () => {
                      this.campaignErrorModalVisible = false
                      this.campaignInfo = {}
                      $.store.circle.advanceOptions = true
                    }
                  }
                }
              }
            }
          }

        this.routeSummary = fareResponse.routeSummary || ''
        this.fareLoading = false  
      } catch (e) {
        console.log('getFareV1', e)
        this.fare = -1
        this.status = STATUS_DEFINE.PASSENGER_CONFIRM
        this.fareLoading = false 

        if (e.response.data && e.response.data.code.indexOf('PROMO') > -1) {
          let errorMsg = ''
          switch(e.response.data.code) {
            case 'PROMO_ONLY_NEW_USER':
              errorMsg = $.store.app.strings.promo_error_new_user
              break
            case 'PROMO_EXPIRED':
              errorMsg = $.store.app.strings.promo_error_expired
              break
            case 'PROMO_REACH_LIMIT':
              errorMsg = $.store.app.strings.promo_error_fully_redeemed
              break
            case 'PROMO_OUT_OF_ZONE':
              errorMsg = $.store.app.strings.promo_error_out_of_zone
              break
            case 'PROMO_USER_REDEEMED':
              errorMsg = $.store.app.strings.promo_error_redeemed
              break
            default:
              errorMsg = $.store.app.strings.invalid_code_desc
              break
          }

          $.store.passenger.promoInfoModalVisible = true
          $.store.passenger.promo_info = {
            name: $.store.app.strings.invalid_code.toUpperCase(),
            description: errorMsg,
            confirmLabel: $.store.app.strings.okay.toUpperCase(),
            onConfirm: () => {
              $.store.passenger.promoInfoModalVisible = false
              this.promo_code = ''
              this.getFare()
              console.log('Should have called getFare function')
            }
          }
        } else {
          $.define.func.showMessage($.store.app.strings.timeout_try_again)
        }
      }
    }

    async getFareV2(groups: Array = [], confirmFare = false) {
      try {
        if (this.type === 'now') this.booking_at = moment().toISOString()

        let group_ids = groups.map(group => group._id)

        const postData = {
          "from_lat": this.from.coords.latitude,
          "from_lng": this.from.coords.longitude,
          "destination_lat": this.destination.coords.latitude,
          "destination_lng": this.destination.coords.longitude,
          "assign_type": this.assign_type,
          "booking_at": this.booking_at,
          "group_ids": group_ids,
          "promo_code": !!this.promo_code ? encodeURIComponent(this.promo_code) : '',
          "fareChecksum": this.fareChecksum,
          "distance": this.distance,
          "campaign_ids": this.campaign_id
        }

        const fareResponse = await Session.Booking.Post('v2/fares', postData)

        const { distance = 0, fareChecksum = '', fare } = fareResponse

        if (confirmFare) {
          const { amount, promo } = fare[0]
          this.checkPromo(promo)
          
          this.fare = parseFloat(amount)
        }

        this.distance = distance
        this.fareChecksum = fareChecksum

        this.routeSummary = fareResponse.routeSummary || ''
        this.fareLoading = false

        return fare
      } catch (e) {
        this.fareLoading = false
        if (e.response.data && e.response.data.code.indexOf('PROMO') > -1) {
          let errorMsg = ''
          switch(e.response.data.code) {
            case 'PROMO_ONLY_NEW_USER':
              errorMsg = $.store.app.strings.promo_error_new_user
              break
            case 'PROMO_EXPIRED':
              errorMsg = $.store.app.strings.promo_error_expired
              break
            case 'PROMO_REACH_LIMIT':
              errorMsg = $.store.app.strings.promo_error_fully_redeemed
              break
            case 'PROMO_OUT_OF_ZONE':
              errorMsg = $.store.app.strings.promo_error_out_of_zone
              break
            case 'PROMO_USER_REDEEMED':
              errorMsg = $.store.app.strings.promo_error_redeemed
              break
            default:
              errorMsg = $.store.app.strings.invalid_code_desc
              break
          }

          $.store.passenger.promoInfoModalVisible = true
          $.store.passenger.promo_info = {
            name: $.store.app.strings.invalid_code.toUpperCase(),
            description: errorMsg,
            confirmLabel: $.store.app.strings.okay.toUpperCase(),
            onConfirm: () => {
              $.store.passenger.promoInfoModalVisible = false
              this.promo_code = ''
            } 
          }
        } else {
          $.define.func.showMessage($.store.app.strings.timeout_try_again)
        }

        return false
      }
    }

    initBookingTimer () {
      this.timerStartedAt = new Date()
      this.bookingTimer = setInterval(() => {
        this.timerSeconds = moment(this.timerStartedAt).isValid()
        ? 60 - moment().diff(moment(this.timerStartedAt), 's')
        : -1
        if (this.timerSeconds < 0) {
          this.bookingTimer && clearInterval(this.bookingTimer)
        }
      }, 1000)
    }

    @action
    async booking () {
      try {
        this.fareLoading = true
        this.shownSponsor = false

        const { coords, placeId } = this.from
        if (!placeId) await this.setFromAddress(coords)

        const bookingResponse = await Session.Booking.Post('v1', this.model)
        if (this.model.type === 'advance') {
          this.reset()
          
          toastShort($.store.app.strings.advance_success)
          $.store.trip.activeTrip = bookingResponse.doc
          global.$.navigator.dispatch(
            NavigationActions.navigate({
              routeName: 'TripAdvanceDetail',
              params: { bookingId: bookingResponse.doc._id }
            })
          )
        } else {
          if (this.joy_location_id) this.joy_location_info = bookingResponse.joy_location_info
          this.id = bookingResponse.doc._id
          this.status = STATUS_DEFINE.WAIT_DRIVER_CONFRIM
        }
        
        this.initBookingTimer()
      } catch (e) {
        this.fareLoading = false
        console.log(e)
        if (e.response.data && e.response.data.code === 'NO_AVAILABLE_DRIVER') {
          $.define.func.showMessage($.store.app.strings.no_driver)
        } else if (e.response.data && e.response.data.code === 'ADV_BKNG_NOT_AVAILABLE') {
          $.define.func.showMessage($.store.app.strings.advance_err)
        } else if (e.response.data && e.response.data.code === 'ADVANCE_BOOKING_MINIMUM') {
          $.define.func.showMessage($.store.app.strings.advance_err_min)
        } else if (e.response.data && e.response.data.code === 'NO_AVAILABLE_DRIVER') {
          $.define.func.showMessage($.store.app.strings.no_driver)
        } else if (e.response.data && e.response.data.code === 'NO_FRIENDS_AVAILABLE') {
          $.define.func.showMessage($.store.app.strings.no_friend_available)
        } else if (e.response.data && e.response.data.code === 'FARE_MISMATCHED') {
          this.fare = (e.response.data && e.response.data.message.fare[0].amount)
          $.define.func.showMessage($.store.app.strings.fare_mismatched)
        } else if (e.response.data && e.response.data.code === 'DUPLICATE_BOOKING') {
          toastShort($.store.app.strings.duplicate_booking)
          let bookings = []
          if(e.response.data.booking_id !== ""){
            bookings = await $.method.session.Booking.Get('v1/bookings?role=passenger&sort=-booking_at&type=advance&limit=1')
            const { _id } = bookings[0]

            this.reset()
            $.store.trip.activeTrip = bookings[0]
            global.$.navigator.dispatch(NavigationActions.navigate({
              routeName: 'TripAdvanceDetail', params: { bookingId: _id }
            }))
          } else{
            toastShort($.store.app.strings.duplicate_booking)
          }

          if (this.model.type === 'advance') {
            bookings = await $.method.session.Booking.Get('v1/bookings?role=passenger&sort=-booking_at&type=advance&limit=1')
          } else {
            bookings = await $.method.session.Booking.Get('v1/bookings?role=passenger&sort=-booking_at&type=advance&limit=1')
          } 

          if (bookings.length > 0) {
            
            const { _id } = bookings[0]

            this.reset()
            $.store.trip.activeTrip = bookings[0]
            global.$.navigator.dispatch(NavigationActions.navigate({
              routeName: 'TripAdvanceDetail', params: { bookingId: _id }
            }))
          }
        } else if (e.response.data && e.response.data.code === 'UNKNOWN') {
          $.define.func.showMessage($.store.app.strings.campaign_handle_error)
        } else {
          // $.define.func.showMessage('Something went wrong, please try again in awhile.')
        }

        this.status = STATUS_DEFINE.PASSENGER_CONFIRM
        return
      } finally {
        this.fareLoading = false
        // $.store.app.hideLoading()
      }
    }

    @action.bound 
    async cancel () {
      const isIos = Platform.OS === 'ios';

      if (!this.id) return
      try {
        !isIos && $.store.app.showLoading()
        const response = await Session.Booking.Put(`v1/${this.id}`, { action: 'cancel' })
        this.fareLoading = false
        !isIos && $.store.app.hideLoading()
        if (response.isSuccess) {
          if (!this.destination) return this.reset()
          this.status = STATUS_DEFINE.PASSENGER_CONFIRM
        }
      } catch (e) {
        this.fareLoading = false
        $.store.app.hideLoading()
      }
    }
    
    @action.bound 
    async completed () {
      try {
        const data = await Session.Booking.Put(`v1/${this.id}`, { action: 'completed' })
        if (data && data.isSuccess) {
          this.status = STATUS_DEFINE.SUCCESS
          return data.isSuccess
        }
      } catch (e) {
        $.define.func.showMessage($.store.app.strings.timeout_try_again)
      }
      return false
    }

    @action.bound 
    async attachStatus () {
      if (!this.id || this.id.length === 0) return null
      try {
        const bookingModel = await Session.Booking.Get(`v1/bookings/${this.id}`)
        this.ProcessStatus(bookingModel)
      } catch (e) {
        console.log(e)
      }
    }

    async ProcessStatus (bookingModel) {
      const { driver_id, status, driver_info, vehicle_info, fare, fareObj, additional_fees, driverETA, joy_location_id = '', joy_location_info = {}, from, destination, promo_code, campaign_info, code, assign_type, group_info, type } = bookingModel
      const _status = status.toUpperCase()
      
      this.type = type
      this.booking_status = status
      this.booking_code = code
      this.driver_info = driver_info
      this.driver_id = driver_id
      this.vehicle_info = vehicle_info
      this.from = new AddressModel(from, from.coords)
      this.joy_location_id = joy_location_id
      this.joy_location_info = joy_location_info
      this.destination = new AddressModel(destination, destination.coords)
      this.groupName = assign_type === 'selected_circle' ? $.store.app.strings.circle_of_friend : group_info.name
      this.group_info = group_info
      this.campaign_info = campaign_info

      this.fareObj = fareObj
      const { amount, promo } = fareObj

      this.fare = amount
      this.promo_info = !!promo ? promo : {}
      this.promo_code = !!promo ? this.promo_code : ''

      if (['ON_THE_WAY', 'ARRIVED', 'ON_BOARD'].indexOf(_status) > -1) {
        if (driverETA && driverETA.lat && driverETA.lng) {
          this.driverLocation = {
            _id: driver_id,  
            type: 'rookie', 
            coords:{ latitude: parseFloat(driverETA.lat), 
            longitude: parseFloat(driverETA.lng) }, 
            marker: !!campaign_info && campaign_info.markers && campaign_info.markers.length ? campaign_info.markers[0] : null
          }
        } else {
          this.driverLocation = {}
        }

        this.driverETA = driverETA && driverETA.duration ? driverETA.duration : 0
      } else { 
        this.driverLocation = {}
        this.driverETA = 0
      }
      
      switch (_status) {
        case 'NO_TAKER':
          this.noDriverModal = true
          // $.define.func.showMessage($.store.app.strings.No_Taker)
          this.status = STATUS_DEFINE.PASSENGER_CONFIRM
          break
        case 'NO_DRIVER_AVAILABLE':
          this.noDriverModal = true
          // $.define.func.showMessage($.store.app.strings.No_Taker)
          this.status = STATUS_DEFINE.PASSENGER_CONFIRM
          break
        case 'CONFIRMED':
          if (this.countDriver == true) {this.driverFoundModal = false}
          // this.status = STATUS_DEFINE.DRIVER_CONFRIM
          if (!joy_location_id) {
            await $.store.joy.getSponsorLocationList()
            if ($.store.joy.sponsorLocationList.length && !this.shownSponsor) {
              this.shownSponsor = true
              this.sponsorVisible = true
            }
          }
          this.reset()
          break
        case 'ON_THE_WAY':
          if (!this.countDriver && this.type!== 'advance' ) { this.driverFoundModal = true }
          this.status = STATUS_DEFINE.ON_THE_WAY
          if (!joy_location_id) {
            await $.store.joy.getSponsorLocationList()
            if ($.store.joy.sponsorLocationList.length && !this.shownSponsor) {
              this.shownSponsor = true
              this.sponsorVisible = true
            }
          }
          break
        case 'ARRIVED':
          this.status = STATUS_DEFINE.DRIVER_ARRIVED
          break
        case 'ON_BOARD':
          this.status = STATUS_DEFINE.ON_BOARD
          break
        case 'FINALIZE_TOTAL_FARE':
          this.status = STATUS_DEFINE.FINALIZE_TOTAL_FARE
          this.additional_fees = additional_fees
          this.sponsorVisible = false
          global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'PassengerFare' }))
          break
        case 'COMPLETED':
          this.status = STATUS_DEFINE.SUCCESS
          this.additional_fees = additional_fees
          this.sponsorVisible = false
          global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'PassengerFare' }))
          break
        case 'CANCELLED_BY_DRIVER':
          $.define.func.showMessage($.store.app.strings.drive_cancel_order)
          this.reset()
          break
        case 'NO_SHOW':
          $.define.func.showMessage($.store.app.strings.No_Show)
          this.reset()
          break
        case 'PENDING_ACCEPTANCE':
          break
        default:
          // $.define.func.showMessage('Something went wrong, please try again later.')
          // this.status = STATUS_DEFINE.PASSENGER_CONFIRM
          break
      }
    }

    @action
    async restore () { }

    @action
    async autoUpdateLocation () { }

    @action
    async getFavorite () {
      try {
        if(this.from === null ) return
        const resp = await $.method.session.User.Get('v1/favPlaces')
        const favorite = resp.map(pipe => {
          const distance = AddressDispose.distance(Object.assign({}, { lat: this.from.coords.latitude, lng: this.from.coords.longitude }), pipe.coords)
          return Object.assign({}, pipe, { star: true }, { distance })
        })
        this.favorite = favorite
      } catch (e) {
        console.log(e)
      }
    }

    @action
    async onPressStar(selectData, index) {
      try {
        let {
          _id = (
            this.favorite.find(pipe => pipe.placeId === selectData.placeId) ||
            {}
          )._id,
          coords,
          name
        } = selectData

        if (!coords) {
          const res = await Session.Lookup.Get(`v1/lookup/places/${selectData.placeId}`)
          coords = res.coords
          name = res.name
        }
        
        const { latitude, longitude, lat, lng } = coords
        coords = {
          lat: latitude ? latitude : lat,
          lng: longitude ? longitude : lng
        }
        const body = Object.assign({}, selectData, { name, coords })
        if (_id) {
          // REMOVE
          const resp = await $.method.session.User.Delete(
            `v1/favPlaces/${_id}`,
            body
          )
          const favorite = this.favorite.filter(
            pipe => pipe.placeId !== selectData.placeId
          )

          this.favorite = favorite
          let places = null
          const pl = this.places.map((item, index) => {
            if (item.placeId === selectData.placeId) {
              if (item.star) {
                places = Object.assign(item, { star: false })
              }
              return (this.places[index] = places)
            }
            return this.places[index]
          })

          //  const places = Object.assign(this.places,{star:true})
          this.places = pl
        } else {
          // APPEND
          const resp = await $.method.session.User.Post(
            'v1/favPlaces/user',
            body
          )
          const copy = [...this.favorite]
          this.favorite.push(Object.assign(resp, { star: true }))
          let places = null
          const pl = this.places.map((item, index) => {
            if (item.placeId === selectData.placeId) {
              if (item.star) {
                places = Object.assign(item, { star: false })
              } else {
                places = Object.assign(item, { star: true })
              }
              return (this.places[index] = places)
            }
            return this.places[index]
          })

          //  const places = Object.assign(this.places,{star:true})
          this.places = pl
        }
      } catch (e) {
        /*  */
        console.log(e)
      }
    }
    @action
    enableChatButton (enable) {
      if(Platform.OS === 'android' || Platform.OS === 'ios'){
        if(Platform.OS === 'android' && enable === 'load' && this.loaded === false){
          this.chatButton = true
        }
        if(enable === true) {
          setTimeout(()=>{
            this.chatButton = false
          }, 4000);
          clearTimeout();
          this.loaded = true
        }
        else if(enable === false){
          this.chatButton = false
        }
        else{
          if (Platform.OS === 'android') {
            setTimeout(()=>{
              this.chatButton = false
            }, 6500);
            clearTimeout();
            this.loaded = true
          }
        }
      }
      else{
        this.chatButton = false
      }
    }

    async recoverBooking () {
      const { user = {} } = $.store.account
      try {
        const resp = await $.method.session.Booking.Get('v1/activeBookings')
        if (resp.length > 0) {
          const { status } = resp[0]
          const _status = status.toUpperCase()
          if (resp[0].passenger_id === user._id) {
            $.store.passenger.id = resp[0]._id

            if (['ON_THE_WAY', 'ARRIVED', 'ON_BOARD'].indexOf(_status) > -1) await this.attachStatus()

            switch (_status) {
              case 'ON_THE_WAY':
                $.store.passenger.status = STATUS_DEFINE.ON_THE_WAY
                break
              case 'ARRIVED':
                $.store.passenger.status = STATUS_DEFINE.DRIVER_ARRIVED
                break
              case 'ON_BOARD':
                $.store.passenger.status = STATUS_DEFINE.ON_BOARD
                break
              case 'FINALIZE_TOTAL_FARE':
                $.store.passenger.status = STATUS_DEFINE.FINALIZE_TOTAL_FARE
                this.attachStatus(resp[0]._id)
                break
              case 'PENDING_ACCEPTANCE':
                const bookingModel = await Session.Booking.Get(`v1/bookings/${resp[0]._id}`)
                const { type, _id } = bookingModel

                if (type === 'now') {
                  this.reset()
                  $.store.trip.activeTrip = bookingModel
                  global.$.navigator.dispatch(NavigationActions.navigate({
                    routeName: 'TripAdvanceDetail', params: { bookingId: _id }
                  }))
                }
            }
          } else {
            await $.store.jobs.recoverActiveBooking(resp[0]._id)
            if (_status === 'FINALIZE_TOTAL_FARE' || _status === 'COMPLETED_BY_PASSENGER') {
              const bookingDoc = await Session.Booking.Get(`v1/bookings/${resp[0]._id}`)
              const { _id, fare, additional_fees, status, fareObj } = bookingDoc
              global.$.navigator.dispatch(NavigationActions.navigate({
                routeName: 'JobsFare', params: { _id, fare, additional_fees, status, isSubmit: true, fareObj }
              }))
            } else {
              global.$.navigator.dispatch(NavigationActions.navigate({
                routeName: 'JobsListDetail', params: { type: 'active_booking' }
              }))
            }
          }
        }
      } catch (e) {
        console.log(e)
      }
    }

    async getDriverLocation () {
      try {
        const result = await $.method.session.Location.Get(`v1?reqUser_id=${this.driver_id}&userRole=passenger`)
        if (result && result._id && result.latitude && result.longitude) {
          this.driverLocation = {
            _id: result._id,
            type: 'rookie',
            coords: {
              latitude: result.latitude,
              longitude: result.longitude
            }
          }

          $.store.circle.setCirclePoint([])
        }
      } catch (e) {
        console.log('errorr getting driver location', e)
      }
    }
  },

  Autorun: store => {

    var bookingListener = undefined
    var keep = false

    const autorunFunctionLoopCall = async () => {
      await store.passenger.attachStatus()
      if (keep) bookingListener = setTimeout(autorunFunctionLoopCall, 2000)
    }

    const autorunFunctionStatusListener = async (bookingId, bookingStatus) => {
      bookingListener && clearTimeout(bookingListener)
      bookingListener = undefined
      keep = false

      if (bookingStatus >= STATUS_DEFINE.WAIT_DRIVER_CONFRIM && store.passenger.id && store.passenger.id.length > 0 && bookingStatus < STATUS_DEFINE.FINALIZE_TOTAL_FARE) {
        keep = true
        bookingListener = setTimeout(autorunFunctionLoopCall, 1000)

      } else if (bookingStatus.status === STATUS_DEFINE.FINALIZE_TOTAL_FARE || bookingStatus.status === STATUS_DEFINE.SUCCESS) {
        keep = false
        bookingListener = setTimeout(autorunFunctionLoopCall, 1000)
        
      }
    }
    autorun(() => autorunFunctionStatusListener(store.passenger.id, store.passenger.status))

    const autorunActiveBookingListener = async (authToken) => {
      const { user = {} } = store.account
      if (!authToken || authToken.length === 0 || user.length === 0) return null
      store.passenger.recoverBooking()
    }
    autorun(() => autorunActiveBookingListener(store.account.authToken))

    let driverListener
    const driverFunction = async (driver) => {
      driverListener && clearTimeout(driverListener)
      driverListener = undefined
      if (driver && 'userId' in driver) {

      }
    }
    autorun(() => driverFunction(store.passenger.driver_info))
  }
}

const STATUS_DEFINE = {
  ERROR: -10,
  NO_TAKER: -1,
  PICK_ADDRESS: 0,
  PASSENGER_CONFIRM: 1,
  WAIT_RESPONSE: 2,
  WAIT_DRIVER_CONFRIM: 3,
  DRIVER_CONFRIM: 4,
  ON_THE_WAY: 5,
  DRIVER_ARRIVED: 6,
  WAIT_PASSENGER: 7,
  ON_BOARD: 8,
  ON_RATING: 9,
  CANCELLED_BY_DRIVER: 10,
  FINALIZE_TOTAL_FARE: 11,
  COMPLETED_BY_PASSENGER: 12,
  SUCCESS: 13,
  CONFIRMED: 14
}

const AddressDispose = {
  processAddress: placeDetails => {
    if (!placeDetails) return {}
    let result = {}

    if (placeDetails.name) result.name = placeDetails.name

    if (placeDetails.geometry) {
      result.lat =
        typeof placeDetails.geometry.location.lat === 'function'
          ? placeDetails.geometry.location.lat()
          : placeDetails.geometry.location.lat
      result.lng =
        typeof placeDetails.geometry.location.lng === 'function'
          ? placeDetails.geometry.location.lng()
          : placeDetails.geometry.location.lng
    }

    if (placeDetails.international_phone_number) {
      result.phoneNo = placeDetails.international_phone_number
    } else if (placeDetails.formatted_phone_number) {
      result.phoneNo = placeDetails.formatted_phone_number
    }

    for (let i = 0; i < placeDetails.address_components.length; i++) {
      var address_component = placeDetails.address_components[i]

      if (
        address_component.types.indexOf('street_number') >= 0 ||
        address_component.types.indexOf('premise') >= 0
      ) {
        result.address1 = address_component.long_name
      } else if (address_component.types.indexOf('route') >= 0) {
        result.address2 = address_component.long_name
      } else if (
        address_component.types.indexOf('sublocality') >= 0 ||
        address_component.types.indexOf('sublocality_level_1') >= 0
      ) {
        result.area = address_component.long_name
      } else if (address_component.types.indexOf('locality') >= 0) {
        result.town = address_component.long_name
      } else if (
        address_component.types.indexOf('administrative_area_level_1') >= 0
      ) {
        result.state = address_component.long_name
      } else if (address_component.types.indexOf('postal_code') >= 0) {
        result.postcode = address_component.long_name
      } else if (address_component.types.indexOf('country') >= 0) {
        result.country = address_component.long_name
      }
    }
    return result
  },

  formatAddress: (address, format = 'short') => {
    if (!address) return 'n/a'
    var result = ''

    if (format === 'short') {
      if (address.name) return address.name
      if (address.address1) result += ', ' + address.address1
      if (address.address2) result += ', ' + address.address2
      if (result.substr(0, 2) === ', ') result = result.substr(2)
    } else {
      if (address.name) result += address.name
      if (address.address1) result += ', ' + address.address1
      if (address.address2) result += ', ' + address.address2
      if (address.area) result += ', ' + address.area
      if (address.town) result += ', ' + address.town
      if (address.state) result += ', ' + address.state
      if (address.postcode) result += ', ' + address.postcode
      if (address.country) result += ', ' + address.country
      if (result.substr(0, 2) === ', ') result = result.substr(2)
    }

    return result
  },

  decode: (str, precision) => {
    var index = 0
    var lat = 0
    var lng = 0
    var coordinates = []
    var shift = 0
    var result = 0
    var byte = null
    var latitude_change
    var longitude_change
    var factor = Math.pow(10, precision || 5)

    while (index < str.length) {
      byte = null
      shift = 0
      result = 0

      do {
        byte = str.charCodeAt(index++) - 63
        result |= (byte & 0x1f) << shift
        shift += 5
      } while (byte >= 0x20)

      latitude_change = result & 1 ? ~(result >> 1) : result >> 1
      shift = result = 0

      do {
        byte = str.charCodeAt(index++) - 63
        result |= (byte & 0x1f) << shift
        shift += 5
      } while (byte >= 0x20)

      longitude_change = result & 1 ? ~(result >> 1) : result >> 1
      lat += latitude_change
      lng += longitude_change

      coordinates.push([lat / factor, lng / factor])
    }
    return coordinates
  },

  distance: (a, b) => {
    if (!a.lat || !a.lng || !b.lat || !b.lng) return 'n/a'
    const R = 6371 // Radius of the earth in km
    const deg2rad = deg => deg * (Math.PI / 180)
    const dLat = deg2rad(b.lat - a.lat)
    const dLon = deg2rad(b.lng - a.lng)
    const x =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(deg2rad(a.lat)) *
      Math.cos(deg2rad(b.lat)) *
      Math.sin(dLon / 2) *
      Math.sin(dLon / 2)
    const c = 2 * Math.atan2(Math.sqrt(x), Math.sqrt(1 - x))
    const d = R * c
    return d
  }
}

class AddressModel {
  constructor (address, coords) {
    try {
      if (address.placeId) {
        this.name = address.name
        this.placeId = address.placeId
        this.address = address.address
        this.coords = {
          latitude: address.coords.latitude ? parseFloat(address.coords.latitude) : parseFloat(address.coords.lat),
          longitude: address.coords.longitude ? parseFloat(address.coords.longitude) : parseFloat(address.coords.lng)
        } // Deep Clone
      } else {
        const placeDetail = AddressDispose.processAddress(address)
        this.name = AddressDispose.formatAddress(placeDetail)
        this.placeId = address.place_id
        this.address = AddressDispose.formatAddress(placeDetail, 'full')
        this.coords = {
          latitude: placeDetail.latitude ? parseFloat(placeDetail.latitude) : parseFloat(placeDetail.lat),
          longitude: placeDetail.longitude ? parseFloat(placeDetail.longitude) : parseFloat(placeDetail.lng),
        }
      }
    } catch (e) {
      if(address.joy_location_id) {
        this.name = address.name
        this.address = address.address
        this.joy_location_id = address.joy_location_id
      } else {
        this.name = 'Pin Location'
        this.address = 'Pin Location'
      }

      this.placeId = ''
      this.coords = {
        latitude: coords.latitude ? parseFloat(coords.latitude) : parseFloat(coords.lat),
        longitude: coords.longitude ? parseFloat(coords.longitude) : parseFloat(coords.lng)
      }
    }
  }

  @computed
  get model () {
    return {
      address: this.address,
      coords: {
        lat: parseFloat(this.coords.latitude),
        lng: parseFloat(this.coords.longitude)
      },
      name: this.name,
      placeId: this.placeId,
      joy_location_id: this.joy_location_id
    }
  }

  address: String = ''

  coords: Object = { latitude: 0, longitude: 0 }

  name: String = ''

  placeId: String = ''

  joy_location_id: String = ''

  ready: Boolean = false
}