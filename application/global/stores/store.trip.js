// @flow

import { observable, action, autorun, computed } from 'mobx'
import { Alert } from 'react-native'
import { NavigationActions } from 'react-navigation'
import BaseStore from './base.store'
import Session from '../method/session'
export default {
  Name: 'trip',
  Store: class tripStore extends BaseStore {
    constructor (store) {
      super(store)
    }

    // loading
    @observable loading = false
    // trip
    @observable activeTrip = {}
    // Booking Path
    @observable bookingPath = {}
    // Trip List 
    @observable tripList = []

    
    @action.bound async cancelTrip (bookingId = '') {
      try {
        Alert.alert($.store.app.strings.cancel_trip, $.store.app.strings.cancel_trip_passenger, [
          { text: $.store.app.strings.cancel },
          {
            text: $.store.app.strings.confirm,
            onPress: async () => {
              try {
                const response = await Session.Booking.Put(`v1/${bookingId}`, { action: 'cancel' })
                if (response.isSuccess) {
                  await this.getTripList()
                  $.store.app.hideLoading()
                  global.$.navigator.dispatch(NavigationActions.back())
                } 
              } catch (e) {
              }
            }
          }
        ])
      } catch (e) {
      }
    }
    @action.bound async getTripList () {
      try {
        this.loading = true
        const data = await $.method.session.Booking.Get('v1/bookings?role=passenger&sort=-booking_at')
        const trips = data.filter(pipe => (
          pipe.status === 'Completed_by_Passenger' ||
          pipe.status === 'Completed' ||
          pipe.status === 'No_Show' ||
          pipe.status === 'Cancelled_by_Driver' ||
          (pipe.status === 'Cancelled_by_Passenger' && pipe.driver_id) ||
          pipe.status === 'Confirmed' ||
          pipe.status === 'Pending_Acceptance' ||
          pipe.status === 'Cancelled_by_System' ||
          pipe.status === 'On_The_Way' ||
          pipe.status === 'Arrived' ||
          pipe.status === 'On_Board'
        ))
        this.tripList = trips
        this.loading = false
      } catch (e) {
        this.loading = false
      }
    }
    @action.bound async getActiveTrip () {
      try {
        if (!this.activeTrip || !this.activeTrip.hasOwnProperty('_id')) return null
        let bookingDetail = await Session.Booking.Get(`v1/bookings/${this.activeTrip._id}`)
        this.activeTrip = bookingDetail

        if (bookingDetail.status !== 'Pending_Acceptance' && bookingDetail.status !== 'Confirmed') {
          this.activeTrip = {}
          $.store.passenger.recoverBooking()
          global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'Main' }))
        }
      } catch (e) {
        console.log('getActiveTrip:', e)
      }
    }
    @computed get actualFrom () {
      let from = {}
      if (this.activeTrip.hasOwnProperty('status')) {
        from.coords = {
          latitude: this.activeTrip.from.coords.lat ? this.activeTrip.from.coords.lat : this.activeTrip.from.coords.latitude,
          longitude: this.activeTrip.from.coords.lng ? this.activeTrip.from.coords.lng : this.activeTrip.from.coords.longitude
        }
      }
      return from
    }

    @computed get destination () {
      let destination = {}
      if (this.activeTrip.hasOwnProperty('status')) {
        destination.coords = {
          latitude: this.activeTrip.destination.coords.lat ? this.activeTrip.destination.coords.lat : this.activeTrip.destination.coords.latitude,
          longitude: this.activeTrip.destination.coords.lng ? this.activeTrip.destination.coords.lng : this.activeTrip.destination.coords.longitude
        }
      }
      return destination
    }
  },
  Autorun: (store) => {
    const autorunFunctionPickAddress = async (activeJob) => {
      if (activeJob.hasOwnProperty('status')) {
        store.trip.bookingPath = await store.passenger.directions(
          store.trip.actualFrom.coords,
          store.trip.destination.coords
        )
      }
    }
 
    let bookingTimer = null

    const autorunBookingListener = (activeTrip) => {
      bookingTimer && clearInterval(bookingTimer)
      bookingTimer = undefined

      if (activeTrip.hasOwnProperty('status') && (activeTrip.status === 'Pending_Acceptance' || activeTrip.status === 'Confirmed')) {
        bookingTimer = setInterval(() => store.trip.getActiveTrip(), 2000)
      }
    }

    autorun(() => autorunBookingListener(store.trip.activeTrip))
  }
}