// @flow
import {Vibration} from 'react-native'
import { observable, action, computed, autorun } from 'mobx'
import { chat } from '../service'
import BaseStore from './base.store'
import moment from 'moment'
import Utils from '../method/utils'
import {persist} from 'mobx-persist/lib/index'

export default {
  Name: 'chat',
  Store: class ChatStore extends BaseStore {
    io = chat

    /**
     * 是否在聊天窗口
     */
    @observable chatWindowOpened: Boolean = false
    /**
     * 聊天消息息列表
     *
     */
    @persist('object') @observable chatList:Object = { }

    /**
     * 临时消息息列表
     *
     */
    @persist('list') @observable msgList:Array = []

    sound = null

    @observable chatingId:String = ''
    /**
     * 发送消息
     *
     */
    @action.bound async SendMsg (data:Object, target:String) {
      const nextmessage = Object.assign({}, this.chatList)
      if (!nextmessage.hasOwnProperty(target)) nextmessage[target] = []
      const len = nextmessage[target].length
      const existIndex = nextmessage[target] ? nextmessage[target].findIndex(item => item.key == data.key) : -1
      if (existIndex < 0) {
        data.key = len === 0 ? 0 : nextmessage[target][len - 1].key + 1
      }
      const {type, to_id, from_id, content} = data
      const now = moment().toISOString()
      data.time = now
      data.sendStatus = 1
      data.isRead = true
      this.processChatData(data, target)
      this.processMsgData(data, 'send')
      try {
        let sendData = {}
        const to = type === 'friend' ? 'to' : 'group'
        sendData[to] = to_id
        sendData.user = from_id
        sendData.content = content
        sendData.time = now
        const sendResult = await this.send(sendData)
        if (sendResult.success) {
          this.sendResult({target, id: data.key, type: 2})
        } else {
          this.sendResult({target, id: data.key, type: 3})
        }
      } catch (e) {
        this.sendResult({target, id: data.key, type: 3})
      }
    }

    /**
     * 处理发送消息状态
     *
     */
    sendResult (changeData = {}) {
      const nextmessage = Object.assign({}, this.chatList)
      const index = nextmessage[changeData.target].findIndex(item => item.key == changeData.id)
      nextmessage[changeData.target][index].sendStatus = changeData.type
      this.chatList = nextmessage
    }

    /**
     * 处理临时消息数据结构
     *
     */
    async processMsgData (changeData = {}, sourceType = 'receive') {
      const {type} = changeData
      const myself = $.store.account.user._id
      const self = changeData.from_id === myself
      const target = self ? changeData.to_id : changeData.from_id
      const targetKey = `${type}_${target}`
      const nextList = this.msgList.filter(item => item.key != targetKey)
      const prevItem = this.msgList.find(item => item.key == targetKey)
      let userInfo = {}
      const info = $.store.circle.friendData.friends.filter(item => item.friend_id === target)
      if (info.length <= 0) {
        const resp = await this.getUserInfo(target)
        if (resp.success) userInfo = { ...resp.data, _id: target }
      } else {
        userInfo = info[0].friend_info
      }
      const currentItem = {
        key: targetKey,
        type,
        friend_info: userInfo,
        time: changeData.time,
        firstMsg: changeData.content
      }
      if (changeData.isRead === undefined) { changeData.isRead = false }
      const isRead = (this.chatingId.length > 0 && this.chatingId === target) ? true : changeData.isRead
      const unReadNumber = isRead ? 0 : prevItem ? prevItem.unReadNumber + 1 : 1
      Object.assign(currentItem, {isRead, unReadNumber})
      nextList.unshift(currentItem)
      this.msgList = nextList
    }

    /**
     * 处理当前消息渲染
     *
     */

    processChatData (data, target) {
      const nextmessage = Object.assign({}, this.chatList)
      if (!nextmessage.hasOwnProperty(target)) nextmessage[target] = []
      const len = nextmessage[target].length
      const existIndex = nextmessage[target] ? nextmessage[target].findIndex(item => item.key == data.key) : -1
      if (existIndex < 0) {
        if (len === 0) {
          data['renderTime'] = true
        } else {
          const toCompaire = new Date(nextmessage[target][len - 1].time).getTime()
          const delay = 5 * 60 * 1000 //  五分钟渲染一次
          data['renderTime'] = (new Date(data.time).getTime() - toCompaire) > delay
        }
        nextmessage[target].push(data)
      } else {
        nextmessage[target][existIndex] = data
      }
      this.chatList = nextmessage
    }

    /**
     * 监听是否在聊天窗口中
     *
     */
    @action.bound isOpen (open:Boolean = false, info = {}) {
      this.chatWindowOpened = open
      if (open) {
        this.markRead(info)
      }
    }

    /**
     * 接受消息的事件方法
     * @param {*} message
     */
    @action.bound async recived (message) {
      console.log(message)
      const msgType = message.type.toLowerCase()
      const target = `${msgType}_${message.user}`
      let data = {}
      data.content = message.content
      data.content.id = message.id
      data.to_id = $.store.account.user._id
      data.from_id = message.user
      data.time = message.time
      data.type = msgType
      const nextmessage = Object.assign({}, this.chatList)
      if (!nextmessage.hasOwnProperty(target)) nextmessage[target] = []
      const len = nextmessage[target].length
      data.key = len === 0 ? 0 : nextmessage[target][len - 1].key + 1
      this.io.emit('onMessageRecived', { id: message.id, user: message.user })
      this.processChatData(data, target)
      this.processMsgData(data, 'receive')
    }

    @action.bound removeMsg (target) {
      const nextmessage = Object.assign({}, this.chatList)
      if (!nextmessage.hasOwnProperty(target)) nextmessage[target] = []
      const len = nextmessage[target].length
      if (len > 20) {
        nextmessage[target] = nextmessage[target].slice(len - 20)
        this.chatList = nextmessage
      }
    }

    @action.bound removeChatMsg (target) {
      const nextList = this.msgList.filter(item => item.key !== target)
      this.msgList = nextList
    }

    @action.bound setChatId (id = '') {
      this.chatingId = id
    }

    @action.bound async addFilePath (path, id, target) {
      const nextmessage = Object.assign({}, this.chatList)
      if (!nextmessage.hasOwnProperty(target)) nextmessage[target] = []
      nextmessage[target] = nextmessage[target].map((item) => Object.assign({}, item, {
        content: Object.assign({}, item.content, (() => {
          if (item.content.id !== undefined && item.content.id === id) {
            return { filePath: path }
          }
        })())
      }))
      this.chatList = nextmessage
    }

    async getUserInfo (id) {
      return new Promise(resolve => this.io.emit('onUserData', id).once('onRecivedUserData', resolve))
    }

    /**
     * 发送消息
     * @param {*} message
     */
    @action async send (message) {
      return new Promise(resolve => this.io.emit('sendMessage', message).once('sendMessageCallback', resolve))
    }

    sendEta(passenger_id, input) {
      const targetKey = `friend_${passenger_id}`

      const msgSend = {
        from_id: $.store.account.user._id,
        to_id: passenger_id,
        type: 'friend',
        content: {
          type: 'text',
          content: `Hi, I'm on the way. My ETA is ${input} minutes. Please wait for me.`
        }
      }

      this.SendMsg(msgSend, targetKey)
    }

    @action bind (user) {
      this.io.emit('onBind', user)
      this.io.event('onRecived', this.recived)
    }

    /**
     * 未读消息总数
     *
     */
    @computed get totalUnread () {
      let total = 0
      this.msgList.map(pipe => total += pipe.unReadNumber)
      return total
    }

    /**
     * 订单中的未读消息总数(仅司机端)
     *
     */
    @computed get driverUnread () {
      let target = `friend_${$.store.jobs.active_job.passenger_info._id}`
      const resp = this.msgList.filter(pipe => pipe.key === target)
      return resp.length > 0 ? resp[0].unReadNumber : 0
    }
    @computed get pasengerUnread () {
      console.log($.store.trip.activeTrip)
      let target = `friend_${$.store.trip.activeTrip.driver_id}`
      const resp = this.msgList.filter(pipe => pipe.key === target)
      return resp.length > 0 ? resp[0].unReadNumber : 0
    }
    /**
     * 标记已读
     *
     */
    markRead (info) {
      const {type, id} = info
      if (type === 'FRIEND') {
        const nextList = this.msgList.map(item => {
          if (item.friend_info._id === id) {
            item.isRead = true
            item.unReadNumber = 0
          }
          return item
        })
        this.msgList = nextList
      }
    }

    // 震动
    mobileShock () {
      Vibration.vibrate()
    }

    // 铃声
    async bell () {
      if (!this.sound) this.sound = await Utils.LoadSound('ringtong_chat.mp3')
      if (this.sound) this.sound.play()
    }

    @action.bound processStatus () {
      let total = 0
      const timeOut = 10000
      const now = new Date().getTime()
      const target = `friend_${this.chatingId}`
      const nextmessage = Object.assign({}, this.chatList)
      if (!nextmessage.hasOwnProperty(target)) nextmessage[target] = []
      let currentList = nextmessage[target].map(item => Object.assign({}, item, {
        sendStatus: (() => {
          if (item.sendStatus === 1 && (new Date(item.time).getTime() + timeOut <= now)) {
            total += 1
            return 3
          }
          if (item.sendStatus === 2) return 2
          if (item.sendStatus === 3) return 3
          return 1
        })()
      }))
      nextmessage[target] = currentList
      if (total > 0) this.chatList = nextmessage
    }
  },

  Autorun: (store) => {
    var timeListener
    var keep = false
    // 注册设备
    const autorunConnectSuccess = async (token, connected) => {
      if (!token || token.length === 0) return null
      if (!connected) return null
      const model = {
        id: store.account.user._id,
        token: store.account.authToken,
        fullName: store.account.user.fullName,
        userId: store.account.user.userId,
        avatars: store.account.user.avatars,
        email: store.account.user.email,
        phoneCountryCode: store.account.user.phoneCountryCode,
        phoneNo: store.account.user.phoneNo
      }
      store.chat.bind(model)
    }
    autorun(() => autorunConnectSuccess(store.account.authToken, store.chat.io.connected), { delay: 500 })

    const autoBell = async (total, isOpen, token) => {
      if (!token || token.length === 0) return null
      if (total === 0) return null
      if (isOpen) return null
      store.chat.mobileShock()
      store.chat.bell()
    }
    autorun(() => autoBell(store.chat.totalUnread, store.chat.chatWindowOpened, store.account.authToken), { delay: 500 })

    const timeOut = async (chatId, token) => {
      if (!token || token.length === 0) return null
      timeListener && clearTimeout(timeListener)
      timeListener = undefined
      keep = false
      if (chatId.length > 0) {
        keep = true
        timeListener = setTimeout(recordTime, 1000)
      }
    }
    const recordTime = () => {
      store.chat.processStatus()
      if (keep) timeListener = setTimeout(recordTime, 4000)
    }

    autorun(() => timeOut(store.chat.chatingId, store.account.authToken))
  }
}
