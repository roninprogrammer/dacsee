// @flow

import { observable, action, autorun } from 'mobx'
import { Alert } from 'react-native'
import { NavigationActions } from 'react-navigation'
import BaseStore from './base.store'
import Session from '../method/session'
export default {
  Name: 'joy',
  Store: class joyStore extends BaseStore {
    constructor(store) {
      super(store)
    }

    // loading
    @observable loading = false
    // Joy Listing
    @observable joyLoading = false
    @observable joyList = []
    @observable joyDetail = {}
    @observable pendingJoy = 0
    // Sponsored Location Listing
    @observable sponsorLocationList = []
    @observable sponsorLocationNearbyList = []
    @observable sponsorLocationDetail = {}

    @action.bound async searchSponsorLocationList( keywords, coords) {
      try {
        this.loading = true
        const latitude = coords.latitude ? parseFloat(coords.latitude) : parseFloat(coords.lat)
        const longitude = coords.longitude ? parseFloat(coords.longitude) : parseFloat(coords.lng)

        let url = `v1/locations?keyword=${keywords}&latitude=${latitude}&longitude=${longitude}`

        const data = await $.method.session.Joy.Get(url)
        
        this.loading = false
        if (Array.isArray(data) && data.length) {
          this.sponsorLocationList = data
          return data
        } else {
          this.sponsorLocationList = []
          return []
        }
        
      } catch (e) {
        console.log(e)
        this.loading = false
      }
    }

    @action.bound async getNearby() {
      try {
        this.loading = true
        const { coords } = $.store.app
        const latitude = coords.latitude ? parseFloat(coords.latitude) : parseFloat(coords.lat)
        const longitude = coords.longitude ? parseFloat(coords.longitude) : parseFloat(coords.lng)

        let url = `v1/locations?latitude=${latitude}&longitude=${longitude}&limit=10`

        const data = await $.method.session.Joy.Get(url)

        this.loading = false
        if (Array.isArray(data) && data.length) {
          this.sponsorLocationNearbyList = data
          return data
        } else {
          this.sponsorLocationNearbyList = []
          return []
        }
        
      } catch (e) {
        console.log(e)
        this.loading = false
      }
    }

    @action.bound async getSponsorLocationList(options = {}) {
      try {
        this.loading = true
        const coords = $.store.passenger.destination.coords
        const latitude = coords.latitude ? parseFloat(coords.latitude) : parseFloat(coords.lat)
        const longitude = coords.longitude ? parseFloat(coords.longitude) : parseFloat(coords.lng)
        const { skip = 0, limit = 20 } = options
        let url = `v1/locations?latitude=${latitude}&longitude=${longitude}&skip=${skip}&limit=${limit}`
        const data = await $.method.session.Joy.Get(url)
        this.loading = false
        if (Array.isArray(data) && data.length) {
          this.sponsorLocationList = data
          return data
        } else {
          this.sponsorLocationList = []
          return []
        }
        
      } catch (e) {
        $.define.func.showMessage(e.response.data.message)
        this.loading = false
      }
    }

    @action.bound async getSponsorLocationDetail(id) {
      try {
        this.loading = true
        const data = await $.method.session.Joy.Get(`v1/locations/${id}`)
        this.sponsorLocationDetail = data
        this.loading = false
      } catch (e) {
        $.define.func.showMessage(e.response.data.message)
        this.loading = false
      }
    }

    @action.bound async getJoyList() {
      try {
        this.joyLoading = true
        this.loading = true
        const data = await $.method.session.Joy.Get('v1/userRewards', {}, {
          sort: '-_createdOn'
        })
        this.joyList = data
        this.joyLoading = false
        this.loading = false
      } catch (e) {
        $.define.func.showMessage(e.response.data.message)
        this.loading = false
        this.joyLoading = false
      }
    }

    @action.bound async getJoyDetail(id) {
      try {
        this.loading = true
        const data = await $.method.session.Joy.Get(`v1/userRewards/${id}`)
        this.joyDetail = data
        this.loading = false
        return data
      } catch (e) {
        $.define.func.showMessage(e.response.data.message)
        this.loading = false
      }
    }

    @action.bound async markAsReadJoy(id){
      try{
        const data = await $.method.session.Joy.Put(`v1/userRewards/${id}/markAsRead`)
        $.store.joy.getJoyList()
        $.store.joy.getPendingJoy()
      } catch(e){
        $.define.func.showMessage(e.response.data.message)
      }
    }

    @action async uploadJoyReceipt(id, base64) {
      try {
        const data = await $.method.session.Joy.Post(`v1/userRewards/${id}/receipt`, {
          data: `data:image/jpeg;base64,${base64}`
        })
        this.joyDetail = data
        await $.store.joy.getJoyDetail(id)
        global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'JoyDetailScreen' }))
        $.define.func.showMessage($.store.app.strings.receipt_uploaded)
      } catch (e) {
        $.define.func.showMessage(e.response.data.message)
      }
    }

    @action async deleteJoyReceipt(joyId, imgId) {
      try {
        const data = await $.method.session.Joy.Delete(`v1/userRewards/${joyId}/receipt/${imgId}`)
        this.joyDetail = data
        await $.store.joy.getJoyDetail(joyId)
        global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'JoyDetailScreen' }))
        $.define.func.showMessage($.store.app.strings.receipt_delete_success)
      } catch (e) {
        $.define.func.showMessage(e.response.data.message)
      }
    }

    @action async submitJoyReward(id) {
      try {
        const data = await $.method.session.Joy.Put(`v1/userRewards/${id}/submit`)
        this.joyDetail = data
        await $.store.joy.getJoyList()
        $.store.joy.getPendingJoy()
        $.define.func.showMessage($.store.app.strings.reward_submit_1)
      } catch (e) {
        $.define.func.showMessage(e.response.data.message)
      }
    }

    @action async getPendingJoy() {
      try {
        this.loading = true
        const data = await $.method.session.Joy.Get(`v1/userRewards?status=new,pendingSubmission&resultType=countOnly`)
        this.pendingJoy = data
        this.loading = false
      } catch (e) {
        console.log(e.response.data.message)
        this.loading = false
      }
    }
  },



  Autorun: (store) => {}
}