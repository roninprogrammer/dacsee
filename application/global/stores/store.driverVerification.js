// @flow

import { observable, action, autorun, computed, toJS } from 'mobx'
import { Alert, Platform } from 'react-native'
import { NavigationActions } from 'react-navigation'
import BaseStore from './base.store'
import moment from 'moment'
import Session from '../method/session'
import axios from 'axios'

export default {
  Name: 'driverVerification',
  Store: class driverVerificationStore extends BaseStore {
    constructor(store) {
      super(store)
    }

    dvFormLive: Object = {}
    @observable dvForm: Object = {}
    @observable dvPages: Array = []
    @observable dvCurrentPage: Number = 0
    @observable dvCurrentPageInfo: Object = {}
    @observable dvUnsavedData: Boolean = false
    @observable showAuthLetter: Boolean = false
    @observable dvVehicleModels: Array = []
    @observable clearValue: Boolean = false
    @observable showSkipModal: Boolean = false
    @observable nextLabel: String = $.store.app.strings.next && $.store.app.strings.next.toUpperCase()
    @observable skipAction: String = 'Next'
    @observable reminderModal = false
    @observable formType = 'driverRegistration'

    // Get Driver Verification Request
    @action async getVerificationRequest(formType) {
      const { Driver } = $.method.session
      const { app } = $.store

      this.formType = formType

      try {
        if (Platform.OS === 'android') {
          $.store.app.showLoading()
        }  
        let data = await Driver.Get(`v1/requests/own?formType=${formType}`)
        this.dvForm = observable.object(data);
        this.dvFormLive = Object.assign({}, data)
        const { status } = this.dvForm

        switch (status) {
          case 'pendingCreation': {
            // Create Request
            $.store.app.hideLoading()
            this.showReminderModal()
            let data = await Driver.Post('v1/requests/own', { formType })
            this.dvForm = observable.object(data);
            this.dvFormLive = Object.assign({}, data)
            this.dvPages = this.dvForm.form.pages
            this.dvCurrentPage = 0
            this.dvCurrentPageInfo = this.dvForm.form.pages[this.dvCurrentPage]
            this.checkConditions()
            break;
          }
          case 'pendingPsvCreation': {
            $.store.app.hideLoading()
            this.showReminderModal()
            let data = await Driver.Post('v1/requests/own', { formType })
            this.dvForm = observable.object(data);
            this.dvFormLive = Object.assign({}, data)
            this.dvPages = this.dvForm.form.pages
            this.dvCurrentPage = 0
            this.dvCurrentPageInfo = this.dvForm.form.pages[this.dvCurrentPage]
            this.checkConditions()
            break;
          }
          case 'pendingSubmission':
            $.store.app.hideLoading()
            this.showReminderModal()
            this.dvCurrentPage = 0
            this.dvPages = this.dvForm.form.pages
            this.dvCurrentPageInfo = this.dvForm.form.pages[this.dvCurrentPage]
            this.checkConditions()
            break;
          case 'formRejected':
            $.store.app.hideLoading()
            this.showReminderModal()
            this.dvCurrentPage = 0
            this.dvPages = this.dvForm.form.pages
            this.dvCurrentPageInfo = this.dvForm.form.pages[this.dvCurrentPage]
            this.checkConditions()
            break;
          case 'pendingVerification':
            // Show Summary Status
            $.store.app.hideLoading()
            global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'DriverVerificationForm' }))
            this.checkConditions()
            break;
          // case 'pendingUpdate':
          //   const updatePSV = await this.updateRequest()
          //   global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'DriverUpdateDocument' }))
          //   break;
          case 'pendingPsvSubmission':
            $.store.app.hideLoading()
            this.showReminderModal()
            this.dvCurrentPage = 0
            this.dvPages = this.dvForm.form.pages
            this.dvCurrentPageInfo = this.dvForm.form.pages[this.dvCurrentPage]
            this.checkConditions()
            break;
          case 'psvFormRejected':
            $.store.app.hideLoading()
            this.showReminderModal()
            this.dvCurrentPage = 0
            this.dvPages = this.dvForm.form.pages
            this.dvCurrentPageInfo = this.dvForm.form.pages[this.dvCurrentPage]
            this.checkConditions()
            break;
          case 'pendingPsvVerification':
            $.store.app.hideLoading()
            global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'PSVsubmission', type: 'psvRegistration' }))
            break;
          case 'completed':
            $.store.app.hideLoading()
            global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'DriverVerificationForm' , type:'driverRegistration'}))
            // Show Verified
            break;
          case 'disapproved':
            $.store.app.hideLoading()
            $.define.func.showMessage($.store.app.strings.driver_rejected)
            break;
          case 'pendingAPADVerification':
          case 'pendingAPADSubmission':
          case 'APADCompleted':
            $.store.app.hideLoading()
            global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'PSVsubmission', type: 'psvRegistration' }))
            break;
          default:
            $.store.app.hideLoading()
        }
        // $.store.app.hideLoading()
      } catch (e) {
        console.log(e)
      }
    }

    // Update Driver Verification Request
    @action.bound async updateRequest(data, formType) {
      try {
        if (Platform.OS === 'android') {
          $.store.app.showLoading()
        }  
        data.formType = formType;

        data.form.pages.forEach((page, p) => {
          page.fields.forEach((field, f) => {
            delete data.form.pages[p].fields[f].isRequired
            delete data.form.pages[p].fields[f].isVisible
            delete data.form.pages[p].fields[f].isVerified
          })
        })

        await $.method.session.Driver.Put(`v1/requests/own`, data)
        this.dvFormLive = toJS(data)
        $.store.app.hideLoading()
        return true
      } catch (e) {
        switch (e.response.data.code) {
          case 'INVALID_VEHICLE_MANUFACTURE_YEAR':
            $.define.func.showMessage($.store.app.strings.psv_form_error_msg_year)
            break
          case 'MISSING_PUSPAKOM_FIELDS':
            $.define.func.showMessage($.store.app.strings.psv_form_error_msg_puspakom)
            break
          case 'MISSING_MEDICAL_FIELDS':
            $.define.func.showMessage($.store.app.strings.psv_form_error_msg_medical)
            break
          case 'MISSING_EHAILING_INSURANCE_FIELD':
            $.define.func.showMessage($.store.app.strings.psv_form_error_msg_insurance)
            break
          case 'MISSING_AUTHORIZATION_LETTER_FIELD':
            $.define.func.showMessage($.store.app.strings.psv_form_error_msg_authorization)
            break
          default:
            $.define.func.showMessage($.store.app.strings.error_params_empty)
            break
        }
        $.store.app.hideLoading()
        return false
      }
    }

    @action.bound
    showReminderModal() {
      this.reminderModal = true
    }

    @action.bound
    hideReminderModal(navigate) {
      this.reminderModal = false

      if (navigate) {
        if (this.formType === 'driverRegistration') {
          global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'DriverVerificationForm', type: 'driverRegistration' }))
        } else {
          global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'PSVsubmission', type: 'psvRegistration' }))
        }
      }
    }

    validateField(type, f) {
      const { strings } = $.store.app
      const reFormat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      const spaceReg = /\S/
      let conditions = []

      switch(type) {
        case 'required':
          conditions = f.required || []
          break
        case 'visibilities':
          conditions = f.visibilities || []
          break
        case 'verifications':
          conditions = f.verifications || []
          break
      }

      if (!!conditions.length) {
        let flag = true
        let errorMsg = ''

        for(c of conditions) {
          let condition = true

          switch(c.type) {
            case 'field':
              const { fieldName = '', fieldValue = '' } = c
              const pageId = fieldName.split('.')[0]

              const fields = pageId === this.dvCurrentPageInfo.id ? this.dvCurrentPageInfo.fields : this.dvPages.find((page) => pageId === page.id).fields

              const list = fields.filter((f) => {
                return f.id === fieldName ? f.value === fieldValue : false
              })

              condition = !!list.length
              break

            case 'length':
              const { minValue, maxValue } = c

              const minCheck = typeof minValue === "undefined" ? true : f.value.length >= minValue
              const maxCheck = typeof maxValue === "undefined" ? true : f.value.length <= maxValue

              condition = minCheck && maxCheck
              break

            default:
              condition = type === 'visibilities' || type === 'verifications'
              break
          }

          if (!condition) {
            const { hint = strings.error_params_empty } = c

            flag = false
            errorMsg += hint
          }
        }

        if (!flag && type === 'verifications') $.define.func.showMessage(errorMsg)

        return flag

      } else if (type === 'required') {
        const isPuspakom = ['puspakomCertificate', 'puspakomExpiryDate'].indexOf(f.id.split('.')[1]) > -1

        if (isPuspakom) {
          if (this.formType === 'driverRegistration') return false

          const page = this.dvCurrentPageInfo.id === 'vehicleInfo' ? this.dvCurrentPageInfo : this.dvPages.find((page) => ['vehicleInfo', 'psvSubmission'].indexOf(page.id) > -1)
          const fields = page ? page.fields : []
          const manufactureYear = fields.find((field) => field.id.split('.')[1] === 'manufactureYear')

          return !!manufactureYear ? manufactureYear.value <= moment().year() - 3 : false

        } else {
          return typeof f.required === 'boolean' ? f.required : false
        }

      } else if (type === 'verifications') {
        if (this.dvCurrentPageInfo.allowSkip && f.type !== 'phone' && !f.value) {
          this.showSkipModal = true
          return false
        } else if (this.dvCurrentPageInfo.allowSkip && f.type === 'phone' && (!f.valueCountryCode || f.valuePhoneNo.length === 0 || !spaceReg.test(f.valuePhoneNo))) {
          this.showSkipModal = true
          return false
        } else if (f.type === 'email') {
          const validateEmail = reFormat.test(String(f.value).toLowerCase())
          if (!validateEmail) {
            $.define.func.showMessage(strings.pls_enter_correct_email)
            return false
          }
        } else if (f.id === "personalInfo.dob" && (f.maxYear || f.minYear) && f.value) {
          if (f.minYear) {
            const minDate = moment().add(f.minYear, 'year')
            const valueDate = moment(f.value)
            if (f.value && valueDate < minDate) {
              $.define.func.showMessage(strings.too_old_caption)
              return false
            }
          }
          if (f.maxYear) {
            const maxDate = moment().add(f.maxYear, 'year')
            const valueDate = moment(f.value)
            if (f.value && valueDate > maxDate) {
              $.define.func.showMessage(strings.too_young_caption)
              return false
            }
          }
        } else if (f.id === "personalInfo.myKAD") {
          if (f.value.length !== 14) {
            $.define.func.showMessage(strings.mykad_error_input)
            return false
          }
        } else if (f.type === 'phone' && (f.valuePhoneNo.length < 5 || f.valuePhoneNo.length > 13 || !spaceReg.test(f.valuePhoneNo))) {
          $.define.func.showMessage(strings.add_bank_error_1)
          return false
        } else if (f.type !== 'phone' && f.value.length <= 0) {
          $.define.func.showMessage(strings.add_bank_error_1)
          return false
        } else if (f.value === 'Others' && (!f.otherValue || !spaceReg.test(f.otherValue))) {
          $.define.func.showMessage(strings.add_bank_error_1)
          return false
        } else if (f.type !== 'phone' && !spaceReg.test(f.value)) {
          $.define.func.showMessage(strings.add_bank_error_1)
          return false
        } else if (f.id === 'uploadsInfo.grant' && f.value.length <= 0) {
          $.define.func.showMessage(strings.add_bank_error_1)
          return false
        } else if (f.id === 'psvSubmission.psvExpiryDate' && f.value.length <= 0) {
          $.define.func.showMessage(strings.add_bank_error_1)
          return false
        }
        return true
      } else {
        return true
      }
    }

    // Check field required and visibilities conditions
    checkConditions() {
      const currentPage = this.dvCurrentPage
      let { fields = [] } = this.dvCurrentPageInfo || {}

      for (i = 0, len = fields.length; i < len; i++) {
        try {
          fields[i].isVisible = this.validateField('visibilities', fields[i])
          fields[i].isRequired = fields[i].isVisible ? this.validateField('required', fields[i]) : false
        } catch (error) {
          console.log(error)
        }
      }
      
      this.dvForm.form.pages[currentPage].fields = fields
      this.dvCurrentPageInfo.fields = [] // TODO: trigger render
      this.dvCurrentPageInfo.fields = fields
    }

    // Validate Driver Verification Request
    @action validateVerificationField() {
      const data = this.dvForm
      const currentPage = this.dvCurrentPage
      let fields = data.form.pages[currentPage].fields

      for (i = 0, len = fields.length; i < len; i++) {
        let field = fields[i]
        let { value = "" } = field || {}

        try {
          fields[i].isVisible = this.validateField('visibilities', field)
          fields[i].isRequired = fields[i].isVisible ? this.validateField('required', field) : false

          if (fields[i].isRequired || value) {
            // Field is required or field is not empty
            fields[i].isVerified = this.validateField('verifications', field)
          } else {
            // Field is not required and empty
            fields[i].isVerified = true
          }
        } catch (error) {
          console.log(error)
        }
      }
      
      this.dvForm.form.pages[currentPage].fields = fields
      this.dvCurrentPageInfo.fields = [] // TODO: trigger render
      this.dvCurrentPageInfo.fields = fields

      return !fields.filter((field) => !field.isVerified).length
    }

    // Check if show skip button
    validateSkipBtn() {
      const allowSkip = this.dvCurrentPageInfo.allowSkip

      if (allowSkip) {
        for (f of this.dvCurrentPageInfo.fields) {
          if (this.dvCurrentPageInfo.allowSkip && f.type === 'phone' && !f.valueCountryCode && !f.valuePhoneNo) {
            return this.nextLabel = $.store.app.strings.skip_btn && $.store.app.strings.skip_btn.toUpperCase()
          } else if (this.dvCurrentPageInfo.allowSkip && f.type !== 'phone' && !f.value) {
            return this.nextLabel = $.store.app.strings.skip_btn && $.store.app.strings.skip_btn.toUpperCase()
          } else {
            return this.nextLabel = $.store.app.strings.next && $.store.app.strings.next.toUpperCase()
          }
        }
      }
    }

    // Changing Driver Verification Page ( Next )
    @action.bound async nextPage(formType) {
      const next = this.dvCurrentPage + 1
      const allowSkip = this.dvCurrentPageInfo.allowSkip
      if (next <= this.dvPages.length - 1) {
        this.skipAction = 'Next'
      } else {
        this.skipAction = 'Summary'
      }

      if (next <= this.dvPages.length - 1) {
        if (allowSkip && !this.dvUnsavedData) {
          this.dvUnsavedData = false
          this.dvCurrentPage = next
          this.dvCurrentPageInfo = this.dvPages[next]
          this.checkConditions()
          this.validateSkipBtn()
        } else {
          const validationResult = await this.validateVerificationField()
          if (validationResult && this.dvUnsavedData) {
            try {
              const resp = await this.updateRequest(this.dvForm, formType)
              if (!resp) {
                // $.define.func.showMessage($.store.app.strings.psv_form_error_msg_year)
                return
              }
              this.dvUnsavedData = false
              this.dvCurrentPage = next
              this.dvCurrentPageInfo = this.dvPages[next]
              this.checkConditions()
              this.validateSkipBtn()
            } catch (err) {
              console.log(err)
            }
          } else if (validationResult) {
            this.dvUnsavedData = false
            this.dvCurrentPage = next
            this.dvCurrentPageInfo = this.dvPages[next]
            this.checkConditions()
            this.validateSkipBtn()
          }
        }
      } else {
        if (this.dvUnsavedData) {
          const validationResult = await this.validateVerificationField()
          if (validationResult) {
            const isSuccess = await this.updateRequest(this.dvForm, formType)
            if (isSuccess) {
              this.dvUnsavedData = false
              global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'DriverVerificationSummary', params: { formType } }))
            }
          }
        } else {
          const validationResult = await this.validateVerificationField()
          if (validationResult) {
            const isSuccess = await this.updateRequest(this.dvForm, formType)
            if (isSuccess) {
              this.dvUnsavedData = false
              global.$.navigator.dispatch(NavigationActions.navigate({
                routeName: 'DriverVerificationSummary',
                params: { formType }
              }))
            }
          }
        }
      }
    }

    // Changing Driver Verification Page ( Previous )
    @action.bound async prevPage() {
      const prev = this.dvCurrentPage - 1
      const allowSkip = this.dvCurrentPageInfo.allowSkip
      this.skipAction = 'Prev'

      if (prev >= 0) {
        if (allowSkip && this.dvUnsavedData) {
          this.showSkipModal = true
          return
        } else {
          this.dvUnsavedData = false
          this.dvCurrentPage = prev
          this.dvCurrentPageInfo = this.dvPages[prev]
          this.checkConditions()
          this.validateSkipBtn()
          return
        }
      } else {
        // return error message
        return
      }
    }

    // Update Driver Verification Field
    @action updateFields(index, value, otherValue, countryCode, phoneNo, id) {
      let currentField = this.dvForm.form.pages[this.dvCurrentPage].fields[index]

      this.dvUnsavedData = true
      if (countryCode && ((phoneNo && phoneNo.length === 0) || phoneNo)) {
        currentField.valueCountryCode = countryCode
        currentField.valuePhoneNo = phoneNo
        this.validateSkipBtn()
      } else if (id === 'vehicleInfo.ownVehicle' && value === "Yes") {
        currentField.value = value
        this.showAuthLetter = false
        this.validateSkipBtn()
      } else if (id === 'vehicleInfo.ownVehicle' && value === "No") {
        currentField.value = value
        this.showAuthLetter = true
        this.validateSkipBtn()
      } else if (id === 'vehicleInfo.model') {
        currentField.value = value
        this.clearValue = false
        this.validateSkipBtn()
      } else if (id === 'vehicleInfo.manufacturer') {
        if (this.dvForm.form.pages[this.dvCurrentPage].fields[index].value !== value) {
          this.dvForm.form.pages[this.dvCurrentPage].fields[index + 1].value = ''
          this.clearValue = true
          currentField.value = value
          this.validateSkipBtn()
        }
      } else if (!otherValue) {
        currentField.value = value
        this.validateSkipBtn()
      } else {
        currentField.value = value
        currentField.otherValue = otherValue
        this.validateSkipBtn()
      }
    }

    // Get Vehicle Model 
    @action.bound async getVehicleModal(value, index) {
      let vehicleInfoModelsApiUrl = this.dvCurrentPageInfo.fields[index + 1].api.url;
      vehicleInfoModelsApiUrl = vehicleInfoModelsApiUrl.replace('{manufacturerName}', value);
      let models = await axios.get(vehicleInfoModelsApiUrl, {
        headers: { Authorization: $.store.account.authToken }
      });
      this.dvVehicleModels = models.data.reduce((acc, curr) => [...acc, curr.name], []);
    }

    // Hide Skip Modal
    @action hideSkipModal() {
      this.showSkipModal = false
    }

    // Clear Current Page Date 
    @action clearCurrentData() {
      const next = this.dvCurrentPage + 1
      const prev = this.dvCurrentPage - 1

      this.dvForm = observable.object(this.dvFormLive)
      this.dvPages = Array.from(this.dvForm.form.pages)
      this.dvCurrentPageInfo = Object.assign({}, this.dvForm.form.pages[this.dvCurrentPage])
      this.showSkipModal = false
      this.dvUnsavedData = false
      if (this.skipAction === 'Next') {
        this.dvCurrentPage = next
        this.dvCurrentPageInfo = this.dvPages[next]
        this.checkConditions()
      } else if (this.skipAction === 'Prev') {
        this.dvCurrentPage = prev
        this.dvCurrentPageInfo = this.dvPages[prev]
        this.checkConditions()
      } else if (this.skipAction === 'Summary') {
        this.dvCurrentPage = 0
        this.dvCurrentPageInfo = this.dvPages[0]
        this.checkConditions()
        global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'DriverVerificationSummary' }))
      }
    }

    // Clear Store
    @action clearDriverVerificationData() {
      this.dvForm = {}
      this.dvPages = []
      this.dvCurrentPage = 0
      this.dvCurrentPageInfo = {}
      this.dvUnsavedData = false
      this.showAuthLetter = false
      this.dvVehicleModels = []
      this.clearValue = false
      this.showSkipModal = false
      this.nextLabel = $.store.app.strings.next && $.store.app.strings.next.toUpperCase()
    }

    // Submit Request
    @action.bound async submitRequest(formType) {
      try {
        $.store.app.showLoading()
        await $.method.session.Driver.Post('v1/requests/own/submit', { formType })
        if (formType === 'driverRegistration') {
          $.define.func.showMessage($.store.app.strings.form_submit_successfully)
        } else {
          $.define.func.showMessage($.store.app.strings.psv_form_submit_successfully)
        }
        this.formSubmitted = true
        $.store.app.hideLoading()
        return true
      } catch (e) {
        switch (e.response.data.code) {
          case 'INVALID_VEHICLE_MANUFACTURE_YEAR':
            $.define.func.showMessage($.store.app.strings.psv_form_error_msg_year)
            break
          case 'MISSING_PUSPAKOM_FIELDS':
            $.define.func.showMessage($.store.app.strings.psv_form_error_msg_puspakom)
            break
          case 'MISSING_MEDICAL_FIELDS':
            $.define.func.showMessage($.store.app.strings.psv_form_error_msg_medical)
            break
          case 'MISSING_EHAILING_INSURANCE_FIELD':
            $.define.func.showMessage($.store.app.strings.psv_form_error_msg_insurance)
            break
          case 'MISSING_AUTHORIZATION_LETTER_FIELD':
            $.define.func.showMessage($.store.app.strings.psv_form_error_msg_authorization)
            break
          default:
            $.define.func.showMessage($.store.app.strings.error_params_empty)
            break
        }
        $.store.app.hideLoading()
        return false
      }
    }

  },



  Autorun: (store) => { }
}
