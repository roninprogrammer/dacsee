// @flow

import { AppState, Platform, BackHandler, Linking, ToastAndroid, Alert } from 'react-native'
import PushNotificationIOS from "@react-native-community/push-notification-ios"
import { observable, action, autorun } from 'mobx'
import { persist } from 'mobx-persist'
import splash from 'rn-splash-screen'
import { NavigationActions } from 'react-navigation'
import { zh_cn, en_us, mas } from '../localize/strings'
import moment from 'moment'
import Session from '../method/session'
import Utils from '../method/utils'
import Region from '../method/region'
import BaseStore from './base.store'
import BackgroundGeoLocation from 'react-native-background-geolocation'
import PushNotificationService from '../service/pushNotification'
import Device from 'react-native-device-info'
import { Version } from 'dacsee-utils'
import PopupService from '../../native/popup-service'
import { toastShort } from '../method/ToastUtil'
import CodePush from 'react-native-code-push'
import AndroidOpenSettings from 'react-native-android-open-settings';

const APPLICATION_STATUS = {
  background: -1,
  inactive: 0,
  active: 0
}

const STATUS_DEFINE = {
  ERROR: -10,
  NO_TAKER: -1,
  PICK_ADDRESS: 0,
  PASSENGER_CONFIRM: 1,
  WAIT_RESPONSE: 2,
  WAIT_DRIVER_CONFRIM: 3,
  DRIVER_CONFRIM: 4,
  ON_THE_WAY: 5,
  DRIVER_ARRIVED: 6,
  WAIT_PASSENGER: 7,
  ON_BOARD: 8,
  ON_RATING: 9,
  CANCEL_BY_DRIVER: 10,
  FINALIZE_TOTAL_FARE: 11,
  COMPLETED_BY_PASSENGER: 12,
  SUCCESS: 13,
  CONFIRMED: 14
}

export const MainTabEnum = {
  Passenger: 0,
  Driver: 1
}

export const CircleTabEnum = {
  Group: 0,
  Friend: 1
}

export const CircleSelectTabEnum = {
  Group: 0,
  Friend: 1
}
class SwitcherControl {
  @observable indexCircle: Int = CircleTabEnum.Group
  @observable indexTrip: Int = MainTabEnum.Passenger
  @observable indexSelect: Int = CircleSelectTabEnum.Group
  @action circleNavigate(value) {
    this.indexCircle = value
  }

  @action tripNavigate(value) {
    this.indexTrip = value
  }
  @action selectNavigate(value) {
    this.indexSelect = value
  }
}

class TabControl {
  @observable name = 'MainTab'

  @action navigate(name: String) {
    this.name = name
  }
}

export default {
  Name: 'app',
  Store: class AppStore extends BaseStore {
    constructor(store) {
      super(store)
    }

    control: Object = {
      tab: new TabControl(),
      header: new SwitcherControl(),
      navigator: {}
    }

    @observable region: String = 'MY'

    @persist('object') @observable coords: { latitude: Number, longitude: Number } = Region.getCoords(this.region)

    @observable googleService: Boolean = Platform.select({ android: false, ios: true })

    @persist @observable language = 'EN_US'

    @observable status = ''

    @observable updateStatus: Boolean = false

    @observable deviceToken = '' // PUSH NOTIFICATION TOKEN

    handledPushMessages = [] // handled push messages

    @observable booking_sound = null

    @observable booking_sound_new = null

    @observable chat_sound = null

    @observable strings = en_us

    @observable indicatorVisible = false

    @observable indicatorMsg = ''

    @observable msgVisible = false

    @observable notification = {}

    @action.bound
    showLoading(msg) {
      this.indicatorVisible = true
      this.indicatorMsg = !!msg ? msg : ''
    }

    @action.bound
    hideLoading() {
      this.indicatorVisible = false
      this.indicatorMsg = ''
    }

    @action
    updateCoords(coords) {
      this.coords = coords
    }

    @action
    async acceptSoundPlay() {
      if (!this.accept_sound) this.accept_sound = await Utils.LoadSound('accept.mp3')
      if (this.accept_sound) this.accept_sound.play()
    }

    @action
    async bookingSoundPlay() {
      if (!this.booking_sound) this.booking_sound = await Utils.LoadSound('ringtong_booking.mp3')
      if (this.booking_sound) this.booking_sound.play()
    }

    @action
    async newBookingSoundPlay() {
      if (!this.booking_sound_new) this.booking_sound_new = await Utils.LoadSound('new_booking.mp3')
      if (this.booking_sound_new) {
        this.booking_sound_new.play()
      } else {
        this.bookingSoundPlay()
      }
    }

    newBookingSoundStop() {
      if (this.booking_sound_new) this.booking_sound_new.stop()
    }

    @action
    async chatSoundPlay() {
      if (!this.chat_sound) this.chat_sound = await Utils.LoadSound('ringtong_chat.mp3')
      if (this.chat_sound) this.chat_sound.play()
    }

    @action.bound
    changeLanguage(lan: String) {
      switch (lan) {
        case 'ZH_CN':
          this.strings = zh_cn
          break
        case 'EN_US':
          this.strings = en_us
          break
        case 'MAS':
          this.strings = mas
      }
      this.language = lan
    }

    @action
    async waitNavigatorReady() {
      let loading = true
      while (loading) {
        if ($.navigator && global.$.navigator.dispatch) loading = false
        await $.define.func.delay(100)
      }
      return true
    }

    getParentRoute(name, route) {
      let result = null
      for (var i = 0; i < route.routes.length; i++) {
        if (route.routes[i].routeName === name) {
          result = route
        } else if (route.routes[i].routes && route.routes[i].routes.length > 0) {
          result = this.getParentRoute(name, route.routes[i])
        }

        if (result !== null) return result
      }
    }

    async initializationBackHandler() {
      if (Platform.OS === 'android') {
        BackHandler.addEventListener('hardwareBackPress', async () => {
          try {
            const nav = $.navigator.state.nav
            const app = this.getParentRoute('Main', nav)

            if ($.store.app.control.tab.name !== 'MainTab') {
              $.store.app.control.tab.navigate('MainTab')
              return true
            }

            if ($.store.app.control.header.indexTrip !== 0 ) {
              if ($.store.jobs.working === true && (!$.store.jobs.active_job.status || $.store.jobs.active_job.status === 'Completed') )  {
                Alert.alert(
                  'Reminder',
                  'By pressing Confirm, you will be offline!',
                  [
                    {
                      text: $.store.app.strings.cancel
                    }, {
                      text: $.store.app.strings.confirm, 
                      onPress: () =>  { 
                        $.store.jobs.working = false
                        $.store.app.control.header.indexTrip = 0
                      }
                    }
                  ]
                )
              } else {
                $.store.app.control.header.indexTrip = 0
              }
              return true
            }

            if ($.store.app.control.header.indexCircle !== 0) {
              $.store.app.control.header.indexCircle = 0
              return true
            }

            if (app.routes.length > 1) {
              if (app.routes[app.index].routeName === 'JobsListDetail') {
                const { type } = app.routes[app.index].params
                const { status } = $.store.jobs.active_job
                if ((type === 'active_booking') && (status === 'On_The_Way' ||
                  status === 'Arrived' ||
                  status === 'On_Board')) {
                  return true
                }
              }

              if (app.routes[app.index].routeName === 'JobsFare') {
                if (!app.routes[app.index].params.isSubmit) {
                  global.$.navigator.dispatch(NavigationActions.back())
                }
                return true
              }

              if (app.routes[app.index].routeName === 'PassengerFare') {
                return true
              }

              global.$.navigator.dispatch(NavigationActions.back())
              return true

            } else {
              if(app.routes[app.index].routeName === 'Main'){ 
                if ($.store.passenger.from !== null && $.store.passenger.destination !== null) {
                  if ( $.store.passenger.status === 5 || $.store.passenger.status === 6 || $.store.passenger.status === 8 ) {
                    return false
                  }
                  else {
                    $.store.passenger.status = 0
                    $.store.passenger.reset()
                    return true
                  }
                } 
              }
              if (this.lastBackPressed && this.lastBackPressed + 2000 >= Date.now()) {
                BackHandler.exitApp()
                return false
              }

              const { strings } = this
              this.lastBackPressed = Date.now()
              ToastAndroid.show(strings.press_back_again_android, 1000)
              return true
            }
          } catch (e) {
            return true
          }
        })
      }
    }

    async getBookingDetail(booking_id: String) {
      const fields = [
        '_id', 'assign_type', 'booking_at', 'country', 'destination',
        'fare', 'from', 'notes', 'status', 'payment_method', 'passenger_info', 'type'
      ]
      try {
        const _job = await Session.Booking.Get(`v1/bookings/${booking_id}?fields=${fields.join(',')}`)
        // console.log(_job)
        if (_job) return _job
        return null
      } catch (e) {
        return null
      }
    }

    async messageHandle() {
      let { foreground, userInfo = {}, data = {} } = this.notification
      
      data = data.target ? data : userInfo

      if (foreground) {
        this.timeouter && clearTimeout(this.timeouter)
        this.msgVisible = false
      }

      // booking
      if (data.target === 'booking') {
        const job = await this.getBookingDetail(data.booking_id)

        if (data.status === 'Pending_Acceptance' || data.status === 'Cancelled_by_Passenger' || data.status === 'Cancelled_by_System') {
          $.store.app.control.tab.navigate('MainTab')

          if ($.store.jobs.working) {
            $.store.app.control.header.indexTrip = 1
          } else {
            $.store.app.control.header.indexTrip = 0
          }

          const app = this.getParentRoute('Main', $.navigator.state.nav)

          if (app.routes[app.index].routeName === 'Main') return

          return global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'Main' }))
        }

        if (job.status === 'On_The_Way' || job.status === 'Arrived' || job.status === 'On_Board') {
          $.store.passenger.id = data.booking_id
          const _status = job.status.toUpperCase()
          $.store.passenger.status = STATUS_DEFINE[_status]
          $.store.app.control.tab.navigate('MainTab')
          $.store.app.control.header.indexTrip = 0
          const app = this.getParentRoute('Main', $.navigator.state.nav)
          if (app.routes[app.index].routeName === 'Main') return
          return global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'Main' }))
        }

        if (job.status === 'Finalize_Total_Fare') {
          const app = this.getParentRoute('PassengerFare', $.navigator.state.nav)
          if (app.routes[app.index].routeName === 'PassengerFare') return
          return global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'PassengerFare' }))
        }

        if (job.type === 'advance') {
          $.store.trip.activeTrip = job
          return global.$.navigator.dispatch(
            NavigationActions.navigate({
              routeName: 'TripAdvanceDetail',
              params: { bookingId: job.doc._id }
            })
          )
        }
      }

      if (data.target === 'etaUpdate') {
        $.store.jobs.etaUpdateModal = true
      }

      if (data.target === 'driver_online') {
        if ($.store.jobs.working) {
          BackgroundGeoLocation.stop(() => {
            BackgroundGeoLocation.start()
          })
        } else {
          $.store.jobs.workingChange(true)
        }
        this.control.tab.navigate('MainTab')
        this.control.header.indexTrip = 1
        if ($.store.passenger.status !== STATUS_DEFINE.FINALIZE_TOTAL_FARE) {
          // Temporary solution to avoid navigating away from passenger fare screen
          return global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'Main' }))
        }
      }

      if ($.store.passenger.status === STATUS_DEFINE.FINALIZE_TOTAL_FARE) {
        // Temporary solution to avoid navigating away from passenger fare screen
        return;
      }

      //inbox notification
      if (data.target === 'inbox') {
        this.updateStatus = true
        if (data.message_id == null) {
          return global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'InboxListingScreen' }))
        }
        else {
          $.store.inbox.getInboxList()
          const result = await $.store.inbox.getInboxDetail(data.message_id)
          if (result && result._id) {
            return global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'InboxDetailScreen', params: { id: data.message_id, from: 'notification' } }))
          }
          else {
            toastShort($.store.app.strings.no_message)
            return
          }
        }
      }

      if (data.target === 'friend_request_available') {
        // $.store.app.control.tab.navigate('CommunityTab')
        // $.store.app.control.header.indexCircle = 1
        const app = this.getParentRoute('Main', $.navigator.state.nav)
        if (app.routes[app.index].routeName === 'Main') return
        return global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'Main' }))
      }

      if (data.target === 'friend_request_accepted') {
        // $.store.app.control.tab.navigate('CommunityTab')
        // $.store.app.control.header.indexCircle = 1
        const app = this.getParentRoute('Main', $.navigator.state.nav)
        if (app.routes[app.index].routeName === 'Main') return
        return global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'Main' }))
      }

      if (data.target === 'group_invitation_available') {
        $.store.app.control.tab.navigate('CommunityTab')
        $.store.app.control.header.indexCircle = 0
        const app = this.getParentRoute('Main', $.navigator.state.nav)
        if (app.routes[app.index].routeName === 'Main') return
        return global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'Main' }))
      }

      if (data.target === 'join_group_request' || data.target === 'join_group_request_approved') {
        // $.store.app.control.tab.navigate('CommunityTab')
        // $.store.app.control.header.indexCircle = 0
        await $.store.circle.isMember(data.group_id)
        const membershipType = $.store.circle.groupData.memberShipType.membershipType
        const _id = data.group_id
        return global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'CircleGroupRoom', params: { membershipType, _id } }))
      }

      if (data.target === 'wallet_transfer') {
        // $.store.wallet.walletSelected. availableAmount=1
        // $.store.wallet.getWalletDetail('CR-CN')
        const app = this.getParentRoute('Main', $.navigator.state.nav)
        if (app.routes[app.index].routeName === 'WalletBalance') return
        return global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'WalletBalance' }))
      }

      if (data.target === 'joy_reward') {
        $.store.joy.getJoyList()
      }

      
    }

    // app setting
    @observable appSetting = {}

    @observable appUpdateModalVisible = false

    @observable runModalPopup = false

    @action.bound
    async checkAppSetting() {
      // get
      try {
        const runningBundle = await CodePush.getUpdateMetadata(CodePush.UpdateState.RUNNING)
        const rnVersion = runningBundle ? runningBundle.label : '0'

        this.appSetting = await Session.Lookup.Get(`v1/appSettings?platform=${Platform.OS}&version=${Device.getVersion()}.${rnVersion.replace('v', '')}`)
      } catch (e) {
        // ignore error
      }

      // parse
      if (!this.appSetting || !this.appSetting.status || !this.appSetting.version) this.appSetting = { status: 'online', version: '0.0.0' }

      // compare
      try {
        const currentAppVersion = `${Device.getVersion()}.${Device.getBuildNumber()}`
        if (Version.compare(currentAppVersion, this.appSetting.version) < 0) {
          this.appUpdateModalVisible = true
          $.store.app.updateStatus = true
        } else {
          this.appUpdateModalVisible = false
          this.runModalPopup = true
        }
      } catch (e) {
        // ignore error
      }
    }

    openAppStore() {
      Linking.openURL(this.appSetting.storeUrl).then(() => {

        //this.updateStatus = false
        BackHandler.exitApp()
      })
    }

    closeAppUpdateModal() {
      this.appUpdateModalVisible = false
      //this.updateStatus = false
    }

    @action
    async run() {

      PushNotificationService.checkNotificationPermission()
      
      //check automatic date and time is toggled if not 
      if(Platform.OS === 'android'){
      Device.isAutoDateAndTime().then(isAuto => {
        switch(isAuto) {
          case false:      
          Alert.alert(
            $.store.app.strings.reminder,
            $.store.app.strings.alert_automatic_datetime,
            [
             {
                text: $.store.app.strings.confirm, 
                onPress: () =>  { 
                  AndroidOpenSettings.dateSettings()
                }
              }
            ]
          );      
          break;
          
          }
      
      });
    }
    
      //check automatic date and time is toggled if not 
      if(Platform.OS === 'android'){
          Device.isAutoTimeZone().then(isTime => {
            switch(isTime) {
              case false:      
              Alert.alert(
                $.store.app.strings.reminder,
                $.store.app.strings.alert_automatic_datetime,
                [
                 {
                    text: $.store.app.strings.confirm, 
                    onPress: () =>  { 
                      AndroidOpenSettings.dateSettings()
                    }
                  }
                ]
              );      
              break;
              
              }
          
          });
      }
      
      // GoogleService
      this.googleService = Platform.select({
        android: await $.bridge.utils.google_service.support(),
        ios: true
      })

      // GPS SERVICE
      const permiss = await $.method.location.checkPermissions()
      await this.waitNavigatorReady()

      let TARGET_ROUTE = 'Auth'
      Linking.addEventListener('url', event => this.handleOpenURL(event.url))
      if (permiss.permissions) {
        const once = await $.method.location.once()
        const { latitude, longitude } = once.coords
        this.coords = { latitude, longitude }
      } else {
        $.method.location.requestPermission()
      }

      this.changeLanguage(this.language)

      /* 返回键监听 */
      this.initializationBackHandler()
      let url
      try {
        url = await Linking.getInitialURL()
      } catch (e) {
        console.log(e)
        url = null
      }

      if (url) {
        if ($.store.account.authToken.length > 0) {
          return this.handleOpenURL(url)
        }
        this.handleOpenURL(url)
      }

      let checkNotifications = false
      if ($.store.account.authToken.length > 0) {
        TARGET_ROUTE = 'App'
        checkNotifications = Platform.OS !== 'ios'
      }

      let navigatorLoaded = false
      while (!navigatorLoaded) {
        navigatorLoaded = global.$ && global.$.navigator
        if (navigatorLoaded) global.$.navigator.dispatch(NavigationActions.navigate({ routeName: TARGET_ROUTE }))
      }

      if (checkNotifications && PopupService.pendingNotification) {
        const notification = PopupService.pendingNotification
        let ready = false;
        while (!ready) {
          const app = this.getParentRoute('Main', $.navigator.state.nav)
          ready = app.routes[app.index].routeName === 'Main';
          if (ready) this.onPushNotification(notification)
        }
        PopupService.pendingNotification = null
      }

      if (Platform.OS === 'ios') splash.hide()

      if ($.store.account.authToken.length) {
        $.store.circle.getFriends()
        $.store.circle.getGroups()
        $.store.circle.getDefaultGroup()
      }
    }

    // PUSH NOTIFICATION Callbacks
    @action.bound
    async onPushRegister(session = {}) {
      this.deviceToken = session.token
    }

    @action.bound
    async onPushNotification(notification = {}) {
      let userLoggedIn = $ && $.store && $.store.account && $.store.account.authToken;
      if (!userLoggedIn) {
        await $.define.func.delay(3000)
        return this.onPushNotification(notification)
      }

      this.notification = notification
      const isAndroid = Platform.OS === 'android';
      let { cd, foreground, userInteraction, c_subject, c_message, userInfo = {} } = this.notification

      if (!isAndroid && this.notification.data) {
        ({ cd } = this.notification.data)
      }

      const data = cd ? (typeof cd === 'string' ? JSON.parse(cd) : cd) : userInfo

      const isNewBooking = data.hasOwnProperty('status') && data.status === 'Pending_Acceptance'

      const isBooking = data.hasOwnProperty('status') && data.hasOwnProperty('type')

      if (isBooking) {
        if (data.status === 'Confirmed' && data.type === 'advance') {
          await $.store.trip.getActiveTrip()
          $.store.passenger.driverFoundModal = true
        }
        if (data.status === 'On_the_way' && data.type === 'now') {
          await $.store.passenger.recoverBooking()
          $.store.passenger.driverFoundModal = true
        }
        if (data.status === 'Arrived') {
          await $.store.passenger.recoverBooking()
          $.store.passenger.driverFoundModal = true
        }
        if (data.status === 'Cancelled_by_Passenger') {
          PushNotificationService.localPushCancel({ id: '1' })
          PushNotificationService.localPushCancel({ id: '2' })
          PushNotificationService.localPushCancel({ id: '3' })
        }
      }

      if (isNewBooking && data.booking_id) {
        try {
          if (this.handledPushMessages.includes(data.booking_id)) return;

          this.handledPushMessages.push(data.booking_id)
        } catch (error) {
          console.log(error);
        }
      }

      if (foreground) {
        if (data.target === 'booking') {
          if (data.hasOwnProperty('status') && data.status === 'Pending_Acceptance') {
            if (Platform.OS === 'ios') this.newBookingSoundPlay()
          } else if (data.hasOwnProperty('status') && (data.status !== 'On_Board' && data.status !== 'Finalize_Total_Fare')) {
            this.bookingSoundPlay()
          }

          // Booking has been cancelled or assigned to other driver
          if (data.hasOwnProperty('action') && data.action === 'booking_not_available') {
            let message = this.strings.booking_unavailable

            if (data.hasOwnProperty('status')) {
              switch (data.status) {
                case 'No_Taker':
                  message = this.strings.booking_unavailable
                  break
                case 'Cancelled_by_Passenger':
                  message = 'Passenger has Cancelled the Booking'
                  break
                case 'Confirmed':
                case 'On_The_Way':
                  message = 'Booking has been accepted by another driver'
                  break
              }
            }

            PopupService.removeBookingView(data.booking_id, message)
            // PopupService.dismissActivity(message)
          }
        } else {
          switch(data.sound) {
            case 'accept':
              this.acceptSoundPlay()
              break
            case 'chat':
              this.chatSoundPlay()
              break
            default:
              this.bookingSoundPlay()
              break
          }
        }

        this.timeouter && clearTimeout(this.timeouter)
        this.msgVisible = true
        this.timeouter = setTimeout(() => {
          this.msgVisible = false
        }, 5000)

      } else {
        if (userInteraction) {
          this.messageHandle()
        } else if (isAndroid) {
          
          PushNotificationService.localPush(c_subject, c_message, data)
        }
      }

      if (isAndroid && isNewBooking) {
        this.notifyBooking(data)
      }

      if (isNewBooking) {
        const { booking_id } = data

        $.store.app.logBooking(booking_id, 'receive')
      }

      if (data.target === 'friend_request_available') $.store.circle.getFriendRequests()
      if (data.target === 'friend_request_accepted') $.store.circle.getFriends()
      if (data.target === 'group_invitation_available') $.store.circle.getInvitations()

      if ('finish' in notification) notification.finish(PushNotificationIOS.FetchResult.NoData)
    }

    async notifyBooking(data) {
      try {
        const { booking_id, code = '', destination, from, assign_type, payment_method, group_info, fareObj, booking_at, type, notes = "", incentive = 0 } = data

        if (moment().diff(booking_at, 'seconds') > 60) return

        const { name: destinationName, address: destinationAdd } = destination
        const { name: fromName, address: fromAdd } = from

        const groupName = assign_type === 'selected_circle' ? this.strings.circle_of_friend : group_info.name.toUpperCase()
        const bookingTime = type === 'now' ? this.strings.now : moment(booking_at).format('ddd, DD MMM hh:mm A') // Uncomment when type is included

        const { latitude = 0, longitude = 0 } = this.coords
        const { lng, lat } = from.coords

        let distance = '<100M'
        let calcDistance = parseFloat(Utils.distance(longitude, latitude, lng, lat) / 1000)

        distance = (calcDistance <= 0.1) ? '<100M' : `${calcDistance.toFixed(1)}KM`
        distance = 'Estimated Distance ' + distance

        const { promo, surge, campaign = false, originalAmount = 0, amount } = fareObj

        const fare = !!originalAmount ? originalAmount : amount
        const hasIncentive = !!incentive ? `+ RM ${incentive} (BONUS)` : ''

        const hasPromo = !!promo
        const hasSurge = !!surge
        const hasCampaign = !!campaign

        const rejectBtn = this.strings.reject.toUpperCase()
        const acceptBtn = this.strings.accept.toUpperCase()

        const paymentMethod = payment_method == 'Cash' ? this.strings.cash : payment_method

        const bookingInfo = [
          booking_id,
          code,
          groupName,
          'RM ' + fare.toString(),
          bookingTime,
          type,
          paymentMethod,
          fromName,
          fromAdd,
          destinationName,
          destinationAdd,
          distance,
          hasPromo.toString(),
          hasSurge.toString(),
          hasCampaign.toString(),
          notes,
          rejectBtn,
          acceptBtn,
          hasIncentive
        ]

        try {
          await PopupService.addBookingView(...bookingInfo)
          // await PopupService.notifyUser(...bookingInfo)

        } catch (error) {
          console.log('Popup error ', error);
          if (error && error.code && error.code === 'E_ACTIVITY_DOES_NOT_EXIST') {
            try {
              let launchStatus = await PopupService.launchApp()

              console.log('launchStatus ', launchStatus)

              let appActive = false;
              let retries = 3;

              while (!appActive && retries >= 0) {
                console.log('Notifiying driver on new booking...', retries)
                appActive = AppState.currentState === 'active'
                retries--;
                appActive = appActive || retries === 0

                appActive = launchStatus === 'SUCCESS'
                retries--;
                if (appActive) {
                  await PopupService.addBookingView(...bookingInfo)
                  // await PopupService.notifyUser(...bookingInfo)
                } else {
                  await $.define.func.delay(2000)
                }
              }

            } catch (error2) {
              console.log('Popup error2 ', error2);
            }
          }
        }
      } catch (error) {
        console.log('getBookingDetail:', error)
      }
    }

    async logBooking(id, action, errorCode = 0, errorMessage = '') {
      try {
        let postData = {
          "source": "app",
          "type": "booking_" + action,
          "info": id,
          "timestamp": new Date(),
          "appInfo": Device.getVersion(),
          "deviceInfo": {
            "platform": Device.getSystemName(),
            "version": Device.getSystemVersion(),
            "model": Device.getModel(),
            "manufacturer": Device.getBrand()
          },
          "errorCode": errorCode,
          "errorMessage": errorMessage
        }

        const _log = await Session.Log.Post(`v1/logs`, postData)
      } catch (e) {

      }
    }

    @action.bound
    handleOpenURL(url) {
      const _url = $.method.device.Platform.Android ? url.replace('dacsee://dacsee/', '') : url.replace('dacsee://', '')
      const _args = _url.split('/')
      if (!_args || _args.length === 0) return undefined
      switch (_args[0]) {
        case 'invite':
          this.handleInvite(_args[1], _args[2].replace('#Intent;package=com.stage.dacsee;end', ''))
          break
        case 'group_invite':
          console.log(_args)
          this.handleGroupInvite(_args[1], _args[2], _args[3].replace('#Intent;package=com.stage.dacsee;end', ''))
          break
        default:
          break
      }
    }

    @action.bound
    handleInvite(userId, id) {
      if (!$.store.account.authToken.length > 0) {
        $.store.account.setReferrerValue(userId)
      } else {
        global.$.navigator.dispatch(NavigationActions.navigate(
          {
            routeName: 'FriendsRequest',
            params: { referrer: userId, isInvite: true }
          }
        ))
      }
    }

    @action.bound
    async handleGroupInvite(userId, user_id, group_id) {
      if (!$.store.account.authToken.length > 0) {
        $.store.account.setReferrerValue(userId)
        $.store.account.setReferrerGroupValue(group_id)
      } else {
        // navigate to group details page
        await $.store.circle.isMember(group_id)
        let isMember = $.store.circle.groupData.memberShipType.hasOwnProperty('_id')
        let membershipType = isMember ? $.store.circle.groupData.memberShipType.membershipType : ''
        await $.store.circle.getGroupInfo(group_id)
        global.$.navigator.dispatch(NavigationActions.navigate({
          routeName: 'CircleGroupDetail',
          params: Object.assign({}, $.store.circle.groupData.groupInfo, { isMember, membershipType })
        }))
      }
    }

    @action.bound
    async sendReceipt(id) {
      let result = 'unsuccess'
      try {
        const res = await Session.Booking.Get(`v1/bookings/${id}/receipt`)
        if (res.isSuccess && res.isSuccess === true) {
          result = 'success'
        }
      } catch (e) {
        if (e.response.data.code == 'INVALID_INPUT' && e.response.data.message == 'Invalid Input: Email is not defined') {
          result = 'email'
        }
        console.log(e.response.data)
      }
      return result
    }
  },
  Autorun: async (store) => {
    if (Platform.OS !== 'ios') PopupService.reactStoresHydrated()
  }
}
