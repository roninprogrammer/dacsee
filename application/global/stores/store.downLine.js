import { Alert, ListView } from 'react-native'
import { observable, action, computed } from 'mobx'
import BaseStore from './base.store'
import _ from 'lodash'
import { NavigationActions, StackActions } from 'react-navigation'
import { persist, create } from 'mobx-persist'
import moment from 'moment'


class FriendData {
  // @observable loading = false
  @observable selectAll = false //全选
  @observable teamList = {}
  @observable searchFriends = {}
  @observable lvlList = {}
  @observable newMemberLvl1 = []
  @observable newMemberLvl2 = []
  @observable newMemberLvl3 = []

}
export default {
  Name: 'downLine',
  Store: class DownLineStore extends BaseStore {
    constructor(store) {
      super(store)
      this.friendData = new FriendData()
    }

    @action.bound
    async getLevelList() {
      let data = {}
      try {
        data = await $.method.session.User.Get('v1/downline/level/0')
        this.friendData.lvlList = data
      } catch (e) {
        console.log(e)
      }
    }

    @action.bound
    async getNewMemberLevel1(){
      let data = {}
      let newMember = []
      try {
        data = await $.method.session.User.Get('v1/downline/level/1')
        data.users.forEach((res) => {
          const now = moment();
          const date = moment(res.joinedOn, ['DDMMMYYYY']);
          if(now.isoWeek() === date.isoWeek() && now.isoWeekYear() === date.isoWeekYear()){
            console.log(date);
            newMember.push(res._id);
          }
        });
        this.friendData.newMemberLvl1 = newMember
      } catch (e) {
        console.log(e)
      }
    }

    @action.bound
    async getNewMemberLevel2(){
      let data = {}
      let newMember = []
      try {
        data = await $.method.session.User.Get('v1/downline/level/2')
        data.users.forEach((res) => {
          const now = moment();
          const date = moment(res.joinedOn, ['DDMMMYYYY']);
          if(now.isoWeek() === date.isoWeek() && now.isoWeekYear() === date.isoWeekYear()){
            console.log(date);
            newMember.push(res._id);
          }
        });
        this.friendData.newMemberLvl2 = newMember
      } catch (e) {
        console.log(e)
      }
    }

    @action.bound
    async getNewMemberLevel3(){
      let data = {}
      let newMember = []
      try {
        data = await $.method.session.User.Get('v1/downline/level/3')
        data.users.forEach((res) => {
          const now = moment();
          const date = moment(res.joinedOn, ['DDMMMYYYY']);
          if(now.isoWeek() === date.isoWeek() && now.isoWeekYear() === date.isoWeekYear()){
            console.log(date);
            newMember.push(res._id);
          }
        });
        this.friendData.newMemberLvl3 = newMember
      } catch (e) {
        console.log(e)
      }
    }

    @action.bound
    async getTeamList() {
      let data = {}
      try {
        data = await $.method.session.User.Get('v1/downline/summary')
        this.friendData.teamList = data
      } catch (e) {
        console.log(e)
      }
    }

    @action
    async getSearchFriends(text) {
      let data = {}
      try {
        data = await $.method.session.User.Get(`v1/downline/summary?&skip=0&keyword=${text}`)
        this.friendData.searchFriends = data
      } catch (e) {
        console.log(e)
      }
    }
  },
  Autorun: (store) => {

  }
}
