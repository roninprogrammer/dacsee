import { Platform, NativeModules } from 'react-native'

/*
* Google服务可用性检查
* */
const google_service = Platform.select({
  ios: {
    support: async () => {
      return true
    }
  },
  android: {
    support: async () => {
      const { UtilDevice = {} } = NativeModules
      if (UtilDevice.getGMSStatus) {
        const message = await new Promise((resolve) => UtilDevice.getGMSStatus(({ message }) => resolve(message)))
        if (message === 'SUCCESS' || message === 'SERVICE_VERSION_UPDATE_REQUIRED') return true
      }
      return false
    }
  }
})

export default {
  utils: {
    google_service,

  }
}