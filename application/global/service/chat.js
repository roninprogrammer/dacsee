import { observable } from 'mobx'
import config from '../../../app.env'

import io from 'socket.io-client'

class ChatService {

  socket: io = null
  
  @observable connected: Boolean = false

  constructor(server, options) {
    this.socket = io(server, options)
    this.socket.on('connect', () => this.connected = true)
    this.socket.on('disconnect', () => this.connected = false)
  }

  emit(event, data = {}) {
    return this.socket.emit(event, data)
  }

  event(event, fn) {
    if (typeof(fn) !== 'function') return null
    this.socket.removeEventListener(event)
    return this.socket.on(event, fn)
  }
}


const serverAddress = config.env.server['Chat']
const socketOptions = { reconnection: true, reconnectionDelay: 500 }
const socket = new ChatService(serverAddress, socketOptions)

export default socket