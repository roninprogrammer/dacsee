
import BackgroundGeoLocation from 'react-native-background-geolocation'
import { Platform, Alert, AsyncStorage } from 'react-native';
import StackTrace from 'stacktrace-js';

let HeadlessTask = async (event) => {
    let params = event.params

    // console.log('[BackgroundGeolocation HeadlessTask] -', event.name, params);

    let coords = {}
    let latitude = 0
    let longitude = 0 

    switch (event.name) {
        case 'location':
            coords = params.coords
            latitude = coords.latitude ? coords.latitude : 0
            longitude = coords.longitude ? coords.longitude : 0

            try {
                if (params.activity.type === 'unknown') return

                $.store.jobs.uploadLocation(coords, $.store.vehicles.vehiclesData[0]._id, $.store.account.authToken)

            } catch (e) {
                console.log(e)
            }

            $.store.app.updateCoords({ latitude, longitude })
            break;
        case 'activitychange':
            if (params.activity !== 'still') return

            const location = await new Promise((resolve, reject) => {
                BackgroundGeoLocation.getCurrentPosition({
                    timeout: 10000,
                    enableHighAccuracy: true
                }).then((location) => {
                    resolve(location)
                })
            })

            coords = location.coords
            latitude = coords.latitude ? coords.latitude : 0
            longitude = coords.longitude ? coords.longitude : 0

            try {
                if (params.activity === 'unknown') return

                $.store.jobs.uploadLocation(coords, $.store.vehicles.vehiclesData[0]._id, $.store.account.authToken)

            } catch (e) {
                console.log(e)
            }

            $.store.app.updateCoords({ latitude, longitude })
            break;
    }
}

const opts = {
    reset: true,

    foregroundService: true, // True will shows notification
    notificationText: 'Online',
    stopOnTerminate: true, // False will not stop the plugin
    enableHeadless: false, // Require Register Headless
    heartbeatInterval: 0,

    stopAfterElapsedMinutes: 0,
    stopOnStationary: false,
    desiredAccuracy: BackgroundGeoLocation.DESIRED_ACCURACY_HIGH, // Default to 10
    distanceFilter: 0,
    desiredOdometerAccuracy: 0,
    disableElasticity: true,

    interval: 10000,
    fastestInterval: 10000,
    activitiesInterval: 10000,

    // ACTIVITY
    activityRecognitionInterval: 0, // Change back to 10000
    minimumActivityRecognitionConfidence: 0,
    stopTimeout: 600,
    stopDetectionDelay: 600,
    disableStopDetection: true,

    // ANDROID
    locationUpdateInterval: 10000,
    fastestLocationUpdateInterval: 10000,
    deferTime: 0,
    allowIdenticalLocations: true,
    notificationPriority: BackgroundGeoLocation.NOTIFICATION_PRIORITY_HIGH,

    // IOS
    preventSuspend: true,
    stationaryRadius: 0,
    locationAuthorizationRequestt: 'Always',
    useSignificantChangesOnly: false,
    pausesLocationUpdatesAutomatically: false,
}

export default class BackgroundLocationService {
    static setConfig() {
        BackgroundGeoLocation.setConfig(opts)
    }
    static start() {
        BackgroundGeoLocation.start(() => {
            // console.log('BackgroundGeoLocation is started')
        })
    }
    static stop() {
        BackgroundGeoLocation.stop(() => {
            // console.log('BackgroundGeoLocation is stopped')
        })
    }
    static init() {
        BackgroundGeoLocation.setConfig(opts)

        BackgroundGeoLocation.on('location', (location) => {
            // console.log('LOCATION CHANGED: ', location)

            const { coords = {} } = location
            const { latitude = 0, longitude = 0 } = coords

            try {
                if (!$.store.vehicles.vehiclesData.length || location.activity.type === 'unknown') return
            
                $.store.jobs.uploadLocation(coords, $.store.vehicles.vehiclesData[0]._id, $.store.account.authToken)

            } catch (e) {
                console.log(e)
            }

            $.store.app.updateCoords({ latitude, longitude })
        }, this.onError)

        BackgroundGeoLocation.on('activitychange', async (activity) => {
            // console.log('ACTIVITY CHANGED: ', activity) // eg: 'on_foot', 'still', 'in_vehicle'

            if (activity.activity === 'still') {
                const { coords } = await new Promise((resolve, reject) => {
                    BackgroundGeoLocation.getCurrentPosition({
                        timeout: 10000,
                        enableHighAccuracy: true
                    }).then((location) => {
                        resolve(location)
                    })
                })
                const { latitude = 0, longitude = 0} = coords

                try {
                    if (!$.store.vehicles.vehiclesData.length || activity.type === 'unknown') return

                    $.store.jobs.uploadLocation(coords, $.store.vehicles.vehiclesData[0]._id, $.store.account.authToken)

                } catch (e) {
                    console.log(e)
                }

                $.store.app.updateCoords({ latitude, longitude })
            }
        })

        BackgroundGeoLocation.on('motionchange', (location) => {
            // console.log('MOTION CHANGED: ', location.isMoving, location)
        })

        BackgroundGeoLocation.on('providerchange', (provider) => {
            // console.log('PROVIDER CHANGED: ', provider)
            const { gps = false, enabled = false, network = false } = provider

            $.store.jobs.providerInfo = { gps, enabled, network }
            $.store.jobs.checkProviderStatus()
        })

        BackgroundGeoLocation.ready(opts, async (state) => {
            // console.log('BackgroundGeoLocation is configured')

            const { coords } = await new Promise((resolve, reject) => {
                BackgroundGeoLocation.getCurrentPosition({
                    timeout: 10000,
                    enableHighAccuracy: true
                }).then((location) => {
                    resolve(location)
                })
            })

            const {latitude = 0, longitude = 0} = coords
            
            $.store.app.updateCoords({ latitude, longitude })
            $.store.passenger.setAddressCoords({ longitude, latitude })

            if ($.store.jobs.working) {
                this.start()
            } else {
                this.stop()
            }
        })
    }
    static registerHeadlessTask() {
        BackgroundGeoLocation.registerHeadlessTask(HeadlessTask);
    }
    static removeListeners() {
        BackgroundGeoLocation.removeListeners()
    }
}