import Permissions from 'react-native-permissions'

const PERMISSION_TYPE = {
    Location: 'location',
    Camera: 'camera',
    Microphone: 'microphone',
    Photos: 'photo',
    Contacts: 'contacts',
    Events: 'event',
    Bluetooth: 'bluetooth',
    Reminders: 'reminder',
    Push_Notifications: 'notification',
    Background_Refresh: 'backgroundRefresh',
    Speech_Recognition: 'speechRecognition',
    mediaLibrary: 'mediaLibrary',
    Motion_Activity: 'motion',
    Storage: 'storage',
    Phone_Call: 'callPhone',
    Read_SMS: 'readSms',
    Receive_SMS: 'receiveSms',
}

export default class PermissionService {
    static permissionList = []

    static init() {
        PermissionService.permissionList = Permissions.getTypes()

        console.log('Available permissions ', PermissionService.permissionList )
    }

    static getPermission(type) {
        
        if (PermissionService.permissionList.indexOf(PERMISSION_TYPE[type]) > -1) {
            console.log('Getting permission for ' + PERMISSION_TYPE[type])
            Permissions.check(PERMISSION_TYPE[type]).then(response => {

                // Response is one of: 'authorized', 'denied', 'restricted', or 'undetermined'
                console.log(type + 'Permission is ' + response)
            })
        } else {
            // Permission not available
            console.log('No permission for ' + PERMISSION_TYPE[type])
        }
    }
}

// PermissionService.init();