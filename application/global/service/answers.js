import { Answers } from 'react-native-fabric';

export default class AnswersService {
  static logContentView (contentName) {
    if (excludedContentView.indexOf(contentName) !== -1) return

    const contentType = contentName
    const contentID = contentName
    const customAttributes = {}

    Answers.logContentView(contentName, contentType, contentID, customAttributes)
  }
  static logSearch (query) {
    Answers.logSearch(query)
  }
  static logCustom (name) {
    Answers.logCustom(name, {
      userId: $.store.account.user.userId
    })
  }
}

const excludedContentView = [
  'Main'
]