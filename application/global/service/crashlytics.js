import { Crashlytics } from 'react-native-fabric';
import { Platform } from 'react-native';
import StackTrace from 'stacktrace-js';

export default class CrashlyticsService {

  static init (captureInDebugMode = false) {
    //  FIXME:
    // if (__DEV__ && !captureInDebugMode) {
    //   return;
    // }

    const originalErrorHandler = global.ErrorUtils.getGlobalHandler();
    const errorHandler = (e) => {
      CrashlyticsService.recordIssue(e)
      if (originalErrorHandler) {
        originalErrorHandler(e);
      }
    }
    global.ErrorUtils.setGlobalHandler(errorHandler);
  }

  static setUserName(userName) {
    Crashlytics.setUserName(userName)
  }

  static setUserIdentifier(userId) {
    Crashlytics.setUserIdentifier(userId)
  }

  static log(message) {
    if (!message) return;
    Crashlytics.log(message)
  }

  static logError(e) {
    Platform.OS === 'ios'
      ? Crashlytics.recordError(e)
      : Crashlytics.logException(e)
  }

  static async recordIssue(e) {
    const stack = await StackTrace.fromError(e, {offline: true})
    const formattedStack = stack.map(row => {
      let { source, lineNumber } = row
      if(!lineNumber) lineNumber = parseInt(source.split(':').slice(-2, -1)) || 0
      return {fileName: e.message, lineNumber, functionName: source}
    });
    Crashlytics.recordCustomExceptionName(e.message, e.message, formattedStack)
  }

  static crash () { // simulate a native crash
    Crashlytics.crash()
  }
}