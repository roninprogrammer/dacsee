import RN_PushNotification from "react-native-push-notification";
import Config from "../../../app.env";
import { Platform, Alert } from "react-native";
import PushNotificationIOS from "@react-native-community/push-notification-ios";
import PopupService from "../../native/popup-service";
import AndroidOpenSettings from 'react-native-android-open-settings';
import device from 'react-native-device-info'

export default class PushNotificationService {
  static DEVICE_TOKEN = null;
  static pendingNotification = null;

  static init() {
    PushNotificationService.onNotification = (notification = {}) => {
      const { userInteraction, c_subject, c_message, cd } = notification;
      const data = cd ? JSON.parse(cd) : {};
      const isAndroid = Platform.OS === 'android';
      const isNewBooking =
        data.target === "booking" &&
        data.hasOwnProperty("status") &&
        data.status === "Pending_Acceptance";

      if (!userInteraction && Platform.OS !== "ios" && c_subject && c_message) {
        this.localPush(c_subject, c_message, data)
      }

      if (isAndroid && isNewBooking) {
        try {
          PopupService.pendingNotification = notification
          PopupService.launchApp()
        } catch (error) {
          console.log("PopupService Error:", error)
        }
        return;
      }

      if (isNewBooking) {
        const { booking_id } = notification;

        $.store.app.logBooking(booking_id, "receive")
      }

      if ("finish" in notification)
        notification.finish(PushNotificationIOS.FetchResult.NoData);
    };
    PushNotificationService.onRegister = device => {
      PushNotificationService.DEVICE_TOKEN = (device && device.token) || null;
    };
  }

  static localPush(title, message, additionalData = {}) {
    try {
      const { target } = additionalData;

      const payload = {
        title,
        message,
        priority: "max",
        visibility: "public",
        importance: "high",
        vibrate: true,
        vibration: 300,
        userInfo: additionalData,
        channelName: additionalData.sound || "Default alarm notification",
        playSound:
          title === "New Booking" && Platform.OS === 'android' ? false : true
      };

      if (additionalData.sound) {
        payload.soundName =
          Platform.OS === 'android'
            ? additionalData.sound + ".mp3"
            : additionalData.sound + ".caf";
      }
      RN_PushNotification.localNotification(payload);
    } catch (error) {
      console.log(error);
    }
  }

  static localPushSchedule(data) {
    try {
      const payload = {
        ...data,
        priority: "max",
        visibility: "public",
        importance: "high",
        vibrate: true,
        vibration: 300,
        soundName: 'default'
      };

      RN_PushNotification.localNotificationSchedule(payload);
    } catch (error) {
      console.log(error);
    }
  }

  static localPushCancel(data) {
    try {
      RN_PushNotification.cancelLocalNotifications(data);
    } catch (error) {
      console.log(error);
    }
  }

  static setCallbacks(onRegister, onNotification) {
    PushNotificationService.onRegister = onRegister
    PushNotificationService.onNotification = onNotification
  }

  static configure() {
    RN_PushNotification.configure({
      onRegister: device => {
        if (PushNotificationService.onRegister) {
          PushNotificationService.onRegister(device)
        }
      },
      onNotification: notification => {
        if (PushNotificationService.onNotification) {
          PushNotificationService.onNotification(notification)
        }
      },

      //default: all - Permissions to register - IOS only
      permissions: { alert: true, badge: true, sound: true },

      //Should the initial notification be popped automatically
      popInitialNotification: true,
      requestPermissions: true,
      senderID: Config.env.senderID
    });
  }


  //Initializing check whether Notification is turn on 
  static checkNotificationPermission(){
   if(Platform.OS === 'android'){
    RN_PushNotification.checkPermissions(({ alert }) => {
      switch(alert) {
      case false:      
      Alert.alert(
        $.store.app.strings.reminder,
        $.store.app.strings.alert_push_notification,
        [
         {
            text: $.store.app.strings.confirm, 
            onPress: () =>  { 
              AndroidOpenSettings.appNotificationSettings(device.getBundleId())
            }
          }

        ]
      );      
      break
     
      }
    });
  } 
 }

}

PushNotificationService.init();
