import React, { Component } from "react"
import { NetInfo, View, StatusBar, Animated, Easing, AppState, StyleSheet} from "react-native"
// import NetInfo from "@react-native-community/netinfo";
import { DCColor } from 'dacsee-utils'
// import * as Animatable from 'react-native-animatable';

export default class OfflineBar extends Component {
    constructor(props){
        super(props);
        this.triggerAnimation = this.triggerAnimation.bind(this);
        this.state = {
            isConnected: true,
            animated: true,
          }   
    }
  animationConstants = {
    DURATION: 400,
    TO_VALUE: 3,
    INPUT_RANGE: [0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4],
    OUTPUT_RANGE: [0, -15, 0, 15, 0, -15, 0, 15, 0]
  };
  setNetworkStatus = status => {
    this.setState({ isConnected: status });
    if (status) {
      this.triggerAnimation();
    }
  };

 
  _handleAppStateChange = nextAppState => {
    if (nextAppState === "active") {
      NetInfo.isConnected.fetch().then(this.setNetworkStatus);
    }
  };

  componentDidMount() {
    this.triggerAnimation();
  }
  componentWillMount() {
    NetInfo.isConnected.addEventListener(
      "connectionChange",
      this.setNetworkStatus
    );
    AppState.addEventListener("change", this._handleAppStateChange);
    this.animation = new Animated.Value(0);

    
  }
  componentWillUnMount() {
    NetInfo.isConnected.removeEventListener(
      "connectionChange",
      this.setNetworkStatus
    );
    AppState.removeEventListener("change", this._handleAppStateChange);
  }


  triggerAnimation = () => {
    this.animation.setValue(0);
    Animated.timing(this.animation, {
      duration: this.animationConstants.DURATION,
      toValue: this.animationConstants.TO_VALUE,
      useNativeDriver: true,
      ease: Easing.bounce
    }).start();
  };


  _onChangeAnimated = () => {
    this.setState({animated: !this.state.animated});
  };
  
  render() {
    const interpolated = this.animation.interpolate({
      inputRange: this.animationConstants.INPUT_RANGE,
      outputRange: this.animationConstants.OUTPUT_RANGE
    });
    
    const animationStyle = {
      transform: [{ translateX: interpolated }]
    };

    const { strings } = $.store.app

    return !this.state.isConnected ? (
      <View style={[styles.container]}>
        <StatusBar backgroundColor = {[DCColor.BGColor('primary-1')]} />
        {/* <Animatable.Text animation="pulse" easing="ease-out" iterationCount="infinite" style={[styles.offlineText, animationStyle]}>
             {$.store.app.strings.no
              _internet_connection}
        </Animatable.Text> */}

    <Animated.Text style={[styles.offlineText, animationStyle]}>
        {$.store.app.strings.no_internet_connection}
      </Animated.Text>
      </View>
    ) : null;
  }

}

const styles = StyleSheet.create ({
    container: {
        backgroundColor: DCColor.BGColor('primary-1')
    },
    offlineText: {
        color: 'white',
        padding: 10,
        textAlign : 'center',
        fontSize: 15

    }
});