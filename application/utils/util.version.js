const compare = (v1, v2) => {
  const regExStrip0 = /(\.0+)+$/
  const segmentsV1 = v1.replace(regExStrip0, '').split('.')
  const segmentsV2 = v2.replace(regExStrip0, '').split('.')
  const len = Math.min(segmentsV1.length, segmentsV2.length)

  for (let i = 0; i < len; i++) {
      const diff = parseInt(segmentsV1[i], 10) - parseInt(segmentsV2[i], 10)
      if (diff) return diff
  }

  return segmentsV1.length - segmentsV2.length
}

export default {
  compare
}