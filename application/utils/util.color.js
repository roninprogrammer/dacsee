export default {
    BGColor: (color) => {
        switch (color) {
            case 'primary-1': {
                return '#FFC411'
                break
            }
            case 'primary-2': {
                return '#212322'
                break
            }
            case 'secondary-1': {
                return '#489FDF'
                break
            }
            case 'blue': {
                return '#489fdf'
                break
            }
            case 'yellow': {
                return '#ffc411'
                break
            }
            case 'dark': {
                return '#4d4c4d'
                break
            }
            case 'red': {
                return '#dd1c1a'
                break
            }
            case 'grey': {
                return '#4D4C4D'
                break
            }
            case 'green': {
                return '#7ED321'
                break
            }
            case 'disable': {
                return '#F2F2F2'
                break
            }
            default: {
                return null
            }    
        }
    }
}