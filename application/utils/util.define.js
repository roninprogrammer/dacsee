import DeviceInfo from 'react-native-device-info'
import { Platform } from 'react-native'

const TextInputArgs = {
  returnKeyType: 'next',
  autoCapitalize: 'none',
  selectTextOnFocus: false,
  autoCorrect: false,
  clearTextOnFocus: true,
  underlineColorAndroid: 'transparent'
}

const system = {
  ios: {
    std: (DeviceInfo.getModel().startsWith('iPhone') && !DeviceInfo.getModel().endsWith('Plus')),
    plus: (DeviceInfo.getModel().startsWith('iPhone') && DeviceInfo.getModel().endsWith('Plus')),
    x: DeviceInfo.getModel().indexOf('X') > -1 && Platform.OS === 'ios',
    mini: DeviceInfo.getModel().startsWith('iPad Mini'),
    air: DeviceInfo.getModel().startsWith('iPad Air'),
    pro: DeviceInfo.getModel().startsWith('iPad Pro')
  },
  android: {

  }
}

export default {
  TextInputArgs,
  FixPlusPixel: system.ios.plus ? 1 : 1,
  system: system
}
