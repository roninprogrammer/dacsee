const getHeaderPicUrl = (avatars, useLarge = false) => {
  let url = 'https://storage.googleapis.com/dacsee-service-user/_shared/default-profile.jpg'

  if (!avatars) avatars = []

  if (avatars.length !== 0) {
    let lastest = avatars[avatars.length - 1]
    url = useLarge ? lastest.url:lastest.urlSmall ? lastest.urlSmall:lastest.url
  }
  return url
}
export default { getHeaderPicUrl }
