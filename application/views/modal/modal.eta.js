import React, { Component } from 'react'
import { Text, Modal, TouchableOpacity, View, Linking, Image, StyleSheet, Dimensions } from 'react-native'
import { inject, observer } from 'mobx-react'
import { TextFont, DCColor } from 'dacsee-utils'
import { DCUserInfoCont } from '../components/'
import Resources from 'dacsee-resources'

const { width, height } = Dimensions.get('window')
const MAX_INPUT = 30

@inject('app', 'jobs')
@observer
export default class EtaUpdateModal extends Component {
  constructor(props) {
    super(props)
    this.state = {
      value: 0
    }
  }

  componentDidMount() {
    this.setState({ value: '0' })
  }

  inputHandler = (input) => {
    const { value } = this.state
    let newValue = '0'

    if (input) {
      if (parseInt(value)) {
        newValue = value.concat(input.toString())
      } else {
        newValue = input.toString() // replace
      }
    } else {
      if (parseInt(value)) {
        newValue = value.concat(input.toString())
      } else {
        newValue = value // ignore
      }
    }

    if (parseInt(newValue) > MAX_INPUT) {
      $.define.func.showMessage(`ETA cannot be longer than ${MAX_INPUT} mins.`) // notify
      newValue = '0' // reset
    }

    this.setState({ value: newValue})
  }

  defaultHandler = (input) => {
    this.setState({ value: input.toString()})
    // this.submitHandler()
  }

  backHandler = () => {
    const { value } = this.state
    let newValue = '0'

    if (value.length > 1) {
      newValue = value.slice(0, -1)
    } else {
      newValue = '0'
    }

    this.setState({ value: newValue })
  }

  submitHandler = () => {
    const { value } = this.state
    const { _id, passenger_info } = this.props.jobs.active_job
    const input = parseInt(value)

    if (input > MAX_INPUT) {
      $.define.func.showMessage(`ETA cannot be longer than ${MAX_INPUT} mins.`) // notify
      this.setState({ value: '0' }) // reset

    } else if (input === 0) {
      $.define.func.showMessage(`ETA cannot be 0 mins. If you have Arrived, go to booking details and press on 'Arrived'.`) // notify
      this.setState({ value: '0' }) // reset
      
    } else {
      // submit
      $.store.jobs.bookingHandle({
        action: "update_eta",
        minutes: input
      }, _id)

      // send message
      $.store.chat.sendEta(passenger_info._id, input)

      // reschedule
      $.store.jobs.rescheduleEtaUpdate(input)

      // redirect to navigation app
      if ($.store.jobs.navigationOptions === 'waze') {
        const url = Platform.OS === 'android' ? 'https://waze.com/ul' : 'waze://'

        Linking.openURL(url)
      } else {
        const url = Platform.OS === 'android' ? 'https://www.google.com/maps' : 'comgooglemaps://'

        Linking.openURL(url)
      }

      $.store.jobs.etaUpdateModal = false
      this.setState({ value: '0'})
    }
  }

  callHandler = () => {
    const { passenger_info: { phoneCountryCode, phoneNo} } = $.store.jobs.active_job

    if (!!phoneCountryCode && !!phoneNo) Linking.openURL(`tel:${phoneCountryCode}${phoneNo}`)
  }

  cancelHandler = () => {
    $.store.jobs.etaUpdateModal = false
    this.setState({ value: '0'})
  }

  render() {
    const {
      mainCont,
      actionCont,
      dcIcon,
      dcBtn,
      btnCont,
      dcImg,
      dcCancelBtn,
      fBtn,
      resultCont,
      numberCont,
      numberRowCont,
      number,
      defaultCont,
      smallCircle,
      textResult,
      textMedium,
      textSmall,
      textXSmall,
      textDefault
    } = styles
    
    const { value= '0' } = this.state

    return (
      <Modal
        transparent={true}
        onRequestClose={() => $.store.jobs.etaUpdateModal = false}
        visible={$.store.jobs.etaUpdateModal}
        animationType={'fade'}
      >
        <View style={mainCont}>
          <View style={resultCont}>
            <Text style={textSmall}>Update ETA</Text>
            <Text style={textResult}>{value} mins</Text>
          </View>

          <View style={defaultCont}>
            <TouchableOpacity onPress={() => this.defaultHandler(5)} style={smallCircle}>
              <Text style={textDefault}>5 mins</Text>
            </TouchableOpacity>
            
            <TouchableOpacity onPress={() => this.defaultHandler(10)} style={smallCircle}>
              <Text style={textDefault}>10 mins</Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => this.defaultHandler(15)} style={smallCircle}>
              <Text style={textDefault}>15 mins</Text>
            </TouchableOpacity>

            {/* <TouchableOpacity onPress={() => this.callHandler()} style={smallCircle}>
              <Image source={Resources.image.phone} style={dcIcon} />
            </TouchableOpacity> */}

            <TouchableOpacity style={btnCont} onPress={() => this.callHandler()}>
              <Image source={Resources.image.phone} style={dcIcon} />
            </TouchableOpacity>

          </View>
          
          <View style={numberCont}>
            <View style={numberRowCont}>
              <TouchableOpacity onPress={() => this.inputHandler(1)} style={number}>
                <Text style={textMedium}>1</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.inputHandler(2)} style={number}>
                <Text style={textMedium}>2</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.inputHandler(3)} style={number}>
                <Text style={textMedium}>3</Text>
              </TouchableOpacity>
            </View>

            <View style={numberRowCont}>
              <TouchableOpacity onPress={() => this.inputHandler(4)} style={number}>
                <Text style={textMedium}>4</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.inputHandler(5)} style={number}>
                <Text style={textMedium}>5</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.inputHandler(6)} style={number}>
                <Text style={textMedium}>6</Text>
              </TouchableOpacity> 
            </View>

            <View style={numberRowCont}>
              <TouchableOpacity onPress={() => this.inputHandler(7)} style={number}>
                <Text style={textMedium}>7</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.inputHandler(8)} style={number}>
                <Text style={textMedium}>8</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.inputHandler(9)} style={number}>
                <Text style={textMedium}>9</Text>
              </TouchableOpacity>
            </View>

            <View style={numberRowCont}>
              <TouchableOpacity onPress={() => this.inputHandler()} style={number}>
                <Text style={textMedium}></Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.inputHandler(0)} style={number}>
                <Text style={textMedium}>0</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.backHandler()} style={number}>
                <Image source={Resources.image.backspace} style={[dcIcon, { width: 35, height: 35 }]} />
              </TouchableOpacity> 
            </View>
          </View>

          <View style={actionCont}>
            <TouchableOpacity style={dcCancelBtn} onPress={() => {
              this.cancelHandler()
            }}>
              <Image style={dcImg} source={Resources.image.close} />
            </TouchableOpacity>

            <TouchableOpacity style={dcBtn} onPress={() => {
              this.submitHandler()
            }}>
              <Text style={textSmall}>UPDATE ETA</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    )
  }
}

const styles = StyleSheet.create({
  mainCont: {
    flex: 1,
    height: height * 0.8,
    width,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'flex-start'
  },
  resultCont: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    width,
    backgroundColor: DCColor.BGColor('primary-1'),
    padding: 10
  },
  numberCont: {
    flex: 2,
    width,
    backgroundColor: 'white',
    padding: 10
  },
  numberRowCont: {
    flex: 1,
    flexDirection: 'row',
  },
  number: {
    flex: 1,
    width: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
  defaultCont: {
    width,
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    paddingVertical: 10,
    backgroundColor: DCColor.BGColor('primary-2'),
    flexDirection: 'row'
  },
  smallCircle: {
    borderRadius: 20,
    justifyContent:'center',
    alignItems: 'center',
    paddingHorizontal: 15,
    paddingVertical: 10,
    backgroundColor: DCColor.BGColor('yellow')
  },
  textDefault: {
    color: DCColor.BGColor('primary-2'),
    fontSize: TextFont.TextSize(15),
    fontWeight: 'bold'
  },
  textResult: {
    color: DCColor.BGColor('primary-2'),
    fontSize: TextFont.TextSize(60),
    fontWeight: 'bold',
    paddingTop: 20,
    paddingBottom: 10,
  },
  textMedium: {
    color: DCColor.BGColor('primary-2'),
    fontSize: TextFont.TextSize(35),
    fontWeight: 'bold'
  },
  textSmall: {
    color: DCColor.BGColor('primary-2'),
    fontSize: TextFont.TextSize(15),
    fontWeight: 'bold'
  },
  textXSmall: {
    color: DCColor.BGColor('primary-2'),
    fontSize: TextFont.TextSize(12),
  },
  dcIcon: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
    tintColor: DCColor.BGColor('primary-2')
  },
  actionCont: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingRight: 30,
    paddingLeft: 30,
    paddingBottom: 30,
    paddingTop: 10,
  },
  dcCancelBtn: {
    width: 55,
    height: 55,
    borderRadius: 55,
    backgroundColor: 'rgba(0, 0, 0, 0.1)',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 10
  },
  dcImg: {
    height: 25,
    width: 25,
    resizeMode: 'contain',
    opacity: .3
  },
  dcBtn: {
    flex: 1,
    height: 55,
    borderRadius: 4,
    backgroundColor: DCColor.BGColor('primary-1'),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  btnCont: {
    position: 'relative',
    width: 55,
    height: 55,
    borderRadius: 55 / 2,
    borderColor: 'rgba(0, 0, 0, 0.5)',
    borderWidth: 5,
    backgroundColor: DCColor.BGColor('primary-1'),
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  fBtn: {
    fontSize: TextFont.TextSize(15),
    fontWeight: '700',
    color: DCColor.BGColor('primary-2')
  },
})