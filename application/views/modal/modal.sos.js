import React, { Component } from 'react'
import { Text, View, TouchableOpacity, Image, Modal, StyleSheet, Linking } from 'react-native'
import { inject, observer } from 'mobx-react'
import { Screen, DCColor } from 'dacsee-utils'
import Resources from 'dacsee-resources'

const { height, width } = Screen.window

@inject('app', 'passenger', 'account', 'circle')
@observer
export default class SosModel extends Component {

  constructor(props) {
    super(props)

  }

  call = () => {
    Linking.openURL('tel:999')
  }

  render() {
    const { visible, close = () => { } } = this.props
    const { strings } = this.props.app

    return (
      <Modal
        animationType='fade'           //渐变
        transparent={true}             // 不透明
        visible={visible}    // 根据isModal决定是否显示
        onRequestClose={() => this.props.close()}  // android必须实现 安卓返回键调用
      >
        <View style={{ width: width, height: height, justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgba(57, 56, 67, 0.4)' }}>
          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Image source={Resources.image.sos} style={{ marginBottom: -45, zIndex: 200, width: 150, height: 112 }} />
            <View>
              <View style={{ height: 45, backgroundColor: '#FF233A', borderTopLeftRadius: 8, borderTopRightRadius: 8 }} />
              <View style={styles.card}>
                <Text style={{ fontWeight: 'bold', fontSize: 18 }}>{strings.emergency}</Text>
                <Text style={{ marginTop: 8 }}>{strings.call_police_toast}</Text>
                <View style={{ alignItems: 'center', marginTop: 25 }}>
                  <TouchableOpacity style={styles.button} onPress={this.call}>
                    <Text style={{ fontWeight: 'bold', fontSize: 14 }}>{strings.call_police}</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[styles.button, { backgroundColor: '#C5C5C5' }]} onPress={close}>
                    <Text style={{ fontWeight: 'bold', fontSize: 14 }}>{strings.cancel}</Text>
                  </TouchableOpacity>
                </View>
                <Text style={{ marginTop: 20 }}>* {strings.not_abuse_feature}</Text>
              </View>
            </View>
          </View>
        </View>
      </Modal>
    )
  }
}

const styles = StyleSheet.create({
  card: {
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 25,
    paddingTop: 25,
    borderBottomLeftRadius: 8,
    borderBottomRightRadius: 8,
    paddingBottom: 30
  },
  button: {
    height: 50,
    width: 270,
    borderRadius: 30,
    borderColor: '#fff',
    borderWidth: 6,
    backgroundColor: DCColor.BGColor('primary-1'),
    shadowOffset: { width: 0, height: 1 },
    shadowColor: '#000',
    shadowOpacity: .4,
    elevation: 2,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 10
  }
})