import React, { Component } from 'react'
import { Text, Modal, TouchableOpacity, View, Image, StyleSheet, Dimensions, TextInput, KeyboardAvoidingView, ScrollView, Platform } from 'react-native'
import { inject, observer } from 'mobx-react'
import { TextFont, DCColor } from 'dacsee-utils'
import Resources from 'dacsee-resources'

const { width, height } = Dimensions.get('window')


@inject('app')
@observer
export default class DCBookingOptionModal extends Component {
  constructor(props) {
    super(props)
    this.state = {
      driverNoteLength: 0
    }
  }

  render() {
    const { strings } = $.store.app
    const {
      onHide,
      visible,
      backdropHide,
      driverNoteLimit = 100,
      driverNotePlaceholder = strings.default_driver_note_placeholder,
      driverNoteDefaultValue,
      onChangeDriverNote,
      confirmBtnLabel = strings.confirm,
      confirmFunction
    } = this.props
    const {
      mainCont,
      dcCard,
      dcCol,
      iconCont,
      dcIcon,
      fTitle,
      dcRow,
      fCaption,
      dcTextarea,
      dcBtn,
      fBtn,
    } = styles
    const { driverNoteLength } = this.state

    return (
      <Modal
        transparent={true}
        onRequestClose={onHide}
        visible={visible}
        animationType={'slide'}
      >
        <TouchableOpacity style={mainCont} activeOpacity={1} onPress={backdropHide ? onHide : null}>
          <KeyboardAvoidingView behavior='padding' enabled style={{ width: '100%', alignItems: 'flex-end' }}>
            <TouchableOpacity style={dcCard} activeOpacity={1}>
              <View style={dcCol}>
                <TouchableOpacity style={iconCont} onPress={onHide}>
                  <Image style={dcIcon} source={Resources.image.back_icon} />
                </TouchableOpacity>
                <Text style={fTitle}>{strings.driver_note_label}</Text>
              </View>
              <View style={dcRow}>
                <TextInput
                  style={dcTextarea}
                  placeholder={driverNotePlaceholder}
                  defaultValue={driverNoteDefaultValue}
                  multiline={true}
                  returnKeyType={'done'}
                  numberOfLines={3}
                  maxLength={driverNoteLimit}
                  blurOnSubmit={true}
                  placeholderTextColor='rgba(0, 0, 0, 0.2)'
                  onChangeText={text => {
                    onChangeDriverNote && onChangeDriverNote(text)
                    this.setState({ driverNoteLength: text.length })
                  }}
                />
                <Text style={[fCaption, { width: '100%', textAlign: 'right' }]}>{driverNoteLength} / {driverNoteLimit}</Text>
              </View>
              <View style={dcCol}>
                <TouchableOpacity style={dcBtn} onPress={confirmFunction}>
                  <Text style={fBtn}>{confirmBtnLabel && confirmBtnLabel.toUpperCase()}</Text>
                </TouchableOpacity>
              </View>
            </TouchableOpacity>
          </KeyboardAvoidingView>
        </TouchableOpacity>
      </Modal>
    )
  }
}

const styles = StyleSheet.create({
  mainCont: {
    width,
    height,
    backgroundColor: 'rgba(0, 0, 0, 0.65)',
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    padding: 20,
    paddingBottom: 30
  },
  dcCard: {
    width: '100%',
    backgroundColor: 'white',
    borderRadius: 4,
    shadowOffset: { width: 0, height: -1 },
    shadowColor: '#000',
    shadowOpacity: 0.05,
    elevation: 4
  },
  dcCol: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    borderBottomWidth: 1,
    borderColor: 'rgba(0, 0, 0, 0.1)',
    padding: 15
  },
  iconCont: {
    width: 25,
    height: 25,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  dcIcon: {
    width: 15,
    height: 15,
    resizeMode: 'contain',
    opacity: .15,
    marginRight: 15
  },
  fTitle: {
    fontSize: TextFont.TextSize(18),
    fontWeight: '700',
    color: DCColor.BGColor('primary-2')
  },
  dcRow: {
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    borderBottomWidth: 1,
    borderColor: 'rgba(0, 0, 0, 0.1)',
    padding: 15
  },
  fCaption: {
    fontSize: TextFont.TextSize(12),
    fontWeight: '400',
    color: 'rgba(0, 0,0, 0.5)'
  },
  dcTextarea: {
    width: '100%',
    borderRadius: 4,
    backgroundColor: 'rgba(0, 0, 0, 0.05)',
    padding: 15,
    fontSize: TextFont.TextSize(12),
    fontWeight: '400',
    textAlignVertical: 'top',
  },
  dcBtn: {
    borderRadius: 4,
    backgroundColor: DCColor.BGColor('primary-1'),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height: 55,
    width: '100%'
  },
  fBtn: {
    fontSize: TextFont.TextSize(15),
    fontWeight: '900',
    color: DCColor.BGColor('primary-2')
  }
})