import React, { Component } from 'react'
import { Text, View, TouchableOpacity, Modal, StyleSheet, Image, ScrollView } from 'react-native'
import { Screen, TextFont, DCColor } from 'dacsee-utils'
import Resources from "dacsee-resources"
import _ from 'lodash'
import { inject, observer } from 'mobx-react'

const { height, width } = Screen.window
const styles = StyleSheet.create({
  backdropStyle: {
    width,
    height,
    backgroundColor: 'rgba(57, 56, 67, 0.2)',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20
  },
  dcCard: {
    backgroundColor: 'white',
    borderRadius: 8,
    minWidth: width * 0.8,
    maxHeight: height * 0.8
  },
  headCont: {
    padding: 30,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%'
  },
  dcImg: {
    height: 100,
    width: 100,
    resizeMode: 'contain',
    marginBottom: 20
  },
  fTitle: {
    fontSize: TextFont.TextSize(18),
    fontWeight: '600',
    color: 'rgba(0, 0, 0, 0.75)',
    textAlign: 'center',
    marginBottom: 10
  },
  fCaption: {
    fontSize: TextFont.TextSize(14),
    fontWeight: '400',
    color: 'rgba(0, 0, 0, 0.45)',
    textAlign: 'center',
  },
  bodyCont: {
    width: '100%',
    backgroundColor: 'rgba(0, 0, 0, 0.05)',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 30
  },
  dcCol: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingTop: 3,
    paddingBottom: 3,
  }, 
  fBody: {
    fontSize: TextFont.TextSize(14),
    fontWeight: '500',
    color: 'rgba(0, 0, 0, 0.75)',
    textAlign: 'center',
  },
  dcDot: {
    fontSize: TextFont.TextSize(14),
    fontWeight: '500',
    color: 'rgba(0, 0, 0, 0.75)',
    textAlign: 'center',
    marginRight: 5
  },
  actionCont: {
    width: '100%',
    padding: 15
  },
  dcBtn: {
    borderRadius: 8,
    backgroundColor: DCColor.BGColor('primary-1'),
    width: '100%',
    height: 45,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  fButton: {
    fontSize: TextFont.TextSize(15),
    fontWeight: '500',
    color: 'rgba(0, 0, 0, 0.75)',
    textAlign: 'center',
  }
})

@inject('app', 'vehicles', 'driverVerification')
@observer
export default class ReminderModal extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }

  render() {
    const { strings } = this.props.app
    const { formType = 'driverRegistration' } = this.props.driverVerification
    const {
      backdropStyle,
      dcCard,
      headCont,
      dcImg,
      fTitle,
      fCaption,
      bodyCont,
      dcCol,
      fBody,
      dcDot,
      actionCont,
      dcBtn,
      fButton
    } = styles

    return (
      <Modal
        animationType='fade'
        transparent={true}
        visible={this.props.driverVerification.reminderModal}
        onRequestClose={() => this.props.driverVerification.hideReminderModal()}
        onBackdropPress={() => this.props.driverVerification.hideReminderModal()}
      >
        <View style={backdropStyle}>
          <View style={dcCard}>
            <View style={headCont}>
              <Image style={dcImg} source={Resources.image.attention_icon2}/>
              <Text style={fTitle}>{strings.pocket_list && strings.pocket_list.toUpperCase()}</Text>
              <Text style={fCaption}>{strings.pocket_list_caption}</Text>
            </View>
            <ScrollView style={{flex: 1, flexDirection: 'column'}}>
              {
                formType === 'driverRegistration' ? (
                  <View style={bodyCont}>
                    <View style={dcCol}>
                      <Text style={dcDot}>{'\u2022'}</Text>
                      <Text style={fBody}>{strings.pocket_driver_list_1}</Text>
                    </View>
                    <View style={dcCol}>
                      <Text style={dcDot}>{'\u2022'}</Text>
                      <Text style={fBody}>{strings.pocket_driver_list_2}</Text>
                    </View>
                    <View style={dcCol}>
                      <Text style={dcDot}>{'\u2022'}</Text>
                      <Text style={fBody}>{strings.pocket_driver_list_3}</Text>
                    </View>
                    <View style={dcCol}>
                      <Text style={dcDot}>{'\u2022'}</Text>
                      <Text style={fBody}>{strings.pocket_driver_list_4}</Text>
                    </View>
                    <View style={dcCol}>
                      <Text style={dcDot}>{'\u2022'}</Text>
                      <Text style={fBody}>{strings.pocket_driver_list_5}</Text>
                    </View>
                  </View>
                ) : (
                  <View style={bodyCont}>
                    <View style={dcCol}>
                      <Text style={dcDot}>{'\u2022'}</Text>
                      <Text style={fBody}>{strings.pocket_evp_list_1}</Text>
                    </View>
                    <View style={dcCol}>
                      <Text style={dcDot}>{'\u2022'}</Text>
                      <Text style={fBody}>{strings.pocket_evp_list_2}</Text>
                    </View>
                    <View style={dcCol}>
                      <Text style={dcDot}>{'\u2022'}</Text>
                      <Text style={fBody}>{strings.pocket_evp_list_3}</Text>
                    </View>
                    <View style={dcCol}>
                      <Text style={dcDot}>{'\u2022'}</Text>
                      <Text style={fBody}>{strings.pocket_evp_list_4}</Text>
                    </View>
                  </View>
                )
              }
              
            </ScrollView>
            <View style={actionCont}>
              <TouchableOpacity style={dcBtn} onPress={() => this.props.driverVerification.hideReminderModal(true)}>
                <Text style={fButton}>{strings.okay}</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    )
  }
}
