import React, { Component } from 'react'
import { View, Image, Text, TouchableOpacity } from 'react-native'
import { inject, observer } from 'mobx-react'
import { TextFont, Screen, Define } from 'dacsee-utils'
import Resources from 'dacsee-resources'
const { width } = Screen.window
@inject('app')
@observer
export default class MessageModal extends Component {

  render() {
    let { msgVisible = true, notification ,strings} = this.props.app
    let { title, message, c_subject, c_message } = notification
    if (message && message.body && message.title) {
      title = message.title
      message = message.body
    }

    if (c_subject === 'New Booking' || title === 'New Booking'){
      msgVisible = false
    }

    const top_height = Define.system.ios.x ? 98 : 74
    return (msgVisible && c_message !== 'Have a safe trip.' && c_message !== 'Total fare has been updated' && c_message !== 'Your Booking is Confirmed.' && c_message !== 'Your Driver has Arrived') && (
      <View style={{ position: 'absolute', top: top_height, marginHorizontal: 20, borderRadius: 8, backgroundColor: '#fff', justifyContent: 'center', alignItems: 'center' }}>
        <TouchableOpacity onPress={() => $.store.app.messageHandle()}
          style={{ margin: 5, borderRadius: 7, backgroundColor: '#FFB452' }}>
          <View style={{ flexDirection: 'row', alignItems: 'center', marginVertical: 10,width:width-40 }}>
            <View style={{ width: 50, alignItems: 'flex-end' }}>
              <Image style={{ height: 40, width: 40 }} source={Resources.image.booking_detail_bell} />
            </View>
            <View style={{ marginLeft: 10,flex:1 }}>
              <Text style={{ fontSize: TextFont.TextSize(16), fontWeight: 'bold' }}>
                {title || c_subject}
              </Text>
              <Text numberOfLines={5} style={{ fontSize: TextFont.TextSize(14), }}>
                {message || c_message}
              </Text>
            </View>
            <Text style={{ fontSize: TextFont.TextSize(16),marginHorizontal:8 }}>{strings.confirm}</Text>
          </View>
        </TouchableOpacity>
      </View>
    )
  }
}