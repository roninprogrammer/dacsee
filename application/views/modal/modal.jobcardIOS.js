import React, { Component } from 'react'
import { Text, View, TouchableOpacity, Modal, StyleSheet, Image, Alert } from 'react-native'
import { Screen, Define, TextFont, DCColor } from 'dacsee-utils'
import Resources from "../../resources"
import Material from 'react-native-vector-icons/MaterialIcons'
import { inject, observer, observe } from 'mobx-react'
import moment from 'moment'
import Swiper from 'react-native-swiper'

const { height , width } = Screen.window

@inject('app','jobs')
@observer
export default class JobCardIOS extends Component {
  constructor(props){
    super(props)
  }

  distanceCal = (coords, from) => {
    const { latitude = 0, longitude = 0 } = coords
    const { lng, lat } = from

    let calcDistance = parseFloat($.method.utils.distance(longitude, latitude, lng, lat) / 1000)
    const distance = (calcDistance <= 0.1) ? '<100M' : calcDistance.toFixed(1) + 'KM'

    return distance
  }

  render() {
    
    const { strings , coords} = this.props.app
    const { await_jobs } = this.props.jobs
    const { visible, onClose, onNavigate, index } = this.props
    const { 
      backdropStyle, 
      mainCont,
      container,
      spaceBetween,
      center,
      textSmall,
      textMedium,
      textLarge,
      imgCont, 
      groupText, 
      smallCircle, 
      clickCont, 
      rejectBtn, 
      acceptBtn, 
      wordBtn, 
      greyCircle,
    } = styles

    return (
      <Modal
        animationType='fade'
        transparent={false}
        visible={visible}
        onRequestClose={onClose}
      >
        <Swiper 
          horizontal={true} 
          showsPagination={true} 
          showsButtons={true} 
          loop={false}
          index={index}
          dot={<View style={{backgroundColor:'rgba(0,0,0,.2)', width: 12, height: 12,borderRadius:6, margin: 3}}></View>}
          activeDot={<View style={{backgroundColor: DCColor.BGColor('yellow'), width: 12, height: 12,borderRadius: 6, margin: 3}}></View>}
          paginationStyle={{ bottom: height - 80}}
          nextButton={<Text style={{fontSize: 130,  color: DCColor.BGColor('yellow')}}>›</Text>} 
          prevButton={<Text style={{fontSize: 130,  color: DCColor.BGColor('yellow')}}>‹</Text>}
        >
          {
            await_jobs.map((item, index) => (                    
              <View key={index} style={backdropStyle}>
                <View style={mainCont}>

                  <View style={[container, spaceBetween, { backgroundColor: DCColor.BGColor('primary-1'), borderTopLeftRadius: 10, borderTopRightRadius: 10 }]}>
                    <Image style={imgCont} source={Resources.image.logo_landscape_border}/>
                    <Text style={textSmall}>{item.code}</Text>
                  </View>

                  <View style={[container, { backgroundColor: DCColor.BGColor('primary-1') }]}>
                    <Text style={groupText}>{item.assign_type === 'selected_circle' ? strings.circle_of_friend : item.group_info.name }</Text>
                  </View>

                  <View style={[container, center, { backgroundColor: DCColor.BGColor('primary-2') }]}>
                    <Text style={[textLarge, { color: DCColor.BGColor('primary-1')}]}>RM {item.fare}</Text>

                    {
                      !!item.fareObj.incentiveAmount ? (
                        <Text style={[textLarge, { color: DCColor.BGColor('primary-1')}]}> + RM {item.fareObj.incentiveAmount} (BONUS)</Text>
                      ) : null
                    }
                  </View>

                  <View style={[container, center, { backgroundColor: DCColor.BGColor('primary-2') }]}>
                    <View style={[smallCircle, { backgroundColor: 'black' }]}>
                      <Text style={[textSmall, { color: 'white' }]}>Cash</Text>
                    </View>
                    
                    {
                      item.fareObj.surgeFactor > 1 && (
                        <View style={smallCircle}>
                          <Text style={textSmall}>HIGH FARE</Text>
                        </View>
                      )
                    }

                    {
                      !!item.fareObj.promo && (
                        <View style={smallCircle}>
                          <Text style={textSmall}>PROMO</Text>
                        </View>
                      )
                    }
                  </View>

                  {
                    item.type === 'advance' ? (
                      <View style={[container, spaceBetween, { backgroundColor: DCColor.BGColor('red') }]}>
                        <Text style={[textMedium, { color: 'white'}]}>ADVANCE</Text>
                        <View style={greyCircle}>
                          <Text style={[textMedium, { color: 'white' }]}>{item.type === 'now' ? strings.now : moment(item.booking_at).format('ddd, DD MMM hh:mm A')}</Text>
                        </View>
                      </View>
                    ) : null
                  }

                  <View style={[container, { backgroundColor: 'white', paddingBottom: 0 }]}>
                    <Text style={[textSmall, { fontWeight: 'normal' }]}>{'Estimated Distance ' + this.distanceCal(coords, item.from.coords)}</Text> 
                  </View>

                  <View style={[container, { backgroundColor: 'white', paddingBottom: 0 }]}>
                    <Text style={textMedium}>{item.from.name}</Text>
                    <Text style={[textSmall, { fontWeight: 'normal', textAlign: 'center' }]}>{item.from.address}</Text> 
                  </View>

                  <Material name={'arrow-drop-down'} size={30}/>

                  <View style={[container, { backgroundColor: 'white', paddingTop: 0  }]}>
                    <Text style={textMedium}>{item.destination.name}</Text>
                    <Text style={[textSmall, { fontWeight: 'normal', textAlign: 'center' }]}>{item.destination.address}</Text>
                  </View>

                  {
                    !!item.notes ? (
                      <View style={[container, { backgroundColor: 'rgba(221, 28, 26, 0.25)' }]}>
                        <Text style={textMedium}>NOTE FROM PASSENGER</Text>
                        <Text style={textSmall}>{item.notes}</Text>   
                      </View>
                    ) : null
                  }

                  <View style={clickCont}>
                    <TouchableOpacity style={rejectBtn} onPress={() => onClose(item)}>
                      <Text style={wordBtn}>REJECT</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={acceptBtn} onPress={()=> onNavigate(item)}>
                      <Text style={wordBtn}>ACCEPT</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            ))
          }
        </Swiper>
      </Modal>
    )
  }
}

const styles = StyleSheet.create({
  backdropStyle: {
    width,
    height,
    backgroundColor: 'rgba(57, 56, 67, 0.2)',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20    
  },
  mainCont:{
    backgroundColor: 'white',
    borderRadius: 10,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    maxHeight: (height * 0.9)
  },
  container: {
    width: (width * 0.9),
    alignItems: 'center',
    paddingHorizontal: 15,
    paddingVertical: 10
  },
  spaceBetween: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  center: {
    flexDirection: 'row',
    justifyContent: 'center'
  },
  textSmall: {
    color: DCColor.BGColor('primary-2'),
    fontSize: TextFont.TextSize(12),
    fontWeight: 'bold'
  },
  textMedium: {
    color: DCColor.BGColor('primary-2'),
    fontSize: TextFont.TextSize(15),
    fontWeight: 'bold'
  },
  textLarge: {
    color: DCColor.BGColor('primary-2'),
    fontSize: TextFont.TextSize(25),
    fontWeight: 'bold'
  },
  imgCont:{
    resizeMode: 'contain',
    height: 20,
    maxWidth: 80 
  },
  groupText: {
    color: DCColor.BGColor('primary-1'),
    backgroundColor: DCColor.BGColor('dark'),
    borderRadius: 10,
    fontSize: TextFont.TextSize(15),
    fontWeight: 'bold',
    paddingHorizontal: 20,
    paddingVertical: 10
  },
  smallCircle:{
    backgroundColor: DCColor.BGColor('yellow'),
    borderRadius: 20,
    justifyContent:'center',
    alignItems: 'center',
    paddingHorizontal: 10,
    paddingVertical: 5,
    marginRight: 5
  },
  clickCont: {
    flexDirection: 'row',
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    width: (width * 0.9),
    height: 50
  },
  rejectBtn: {
    backgroundColor:'rgba(0, 0, 0, 0.05)',
    alignItems: 'center',
    justifyContent: 'center',
    width: ((width * 0.9))/2,
    maxHeight: (height/1.5),
    borderBottomLeftRadius: 10
  },
  acceptBtn: {
    backgroundColor: DCColor.BGColor('yellow'),
    alignItems: 'center',
    justifyContent:'center',
    width: ((width * 0.9))/2,
    maxHeight: (height/1.5),
    borderBottomRightRadius: 10,
  },
  wordBtn: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: TextFont.TextSize(16),
    textAlign: 'center'
  },
  greyCircle: {
    backgroundColor: DCColor.BGColor('grey'),
    borderRadius: 10,
    justifyContent:'center',
    alignItems: 'center',
    paddingHorizontal: 10,
    paddingVertical: 5
  }
})