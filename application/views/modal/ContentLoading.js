import React, { PureComponent } from "react";
import { Animated, View, StyleSheet } from "react-native";

class ContentLoading extends PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      animatedStartValue: new Animated.Value(0),
    }
  }

  startAnimation = () => {
    Animated.loop(
      Animated.sequence([
        Animated.timing(this.state.animatedStartValue, {
          toValue: 1,
          duration: 500,
        }),
        Animated.timing(this.state.animatedStartValue, {
          toValue: 0,
          duration: 500
        })
      ]),
    ).start()
  }

  componentDidMount() {
    this.startAnimation();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.loading !== this.props.loading) {
      if (this.props.loading) {
        this.startAnimation();
      }
    }
  }

  render() {
    const interpolatedBackground = this.state.animatedStartValue.interpolate({
      inputRange: [0, 1],
      outputRange: ['rgba(192, 192, 192, 1)', 'rgba(220, 220, 220, 1)']
    });
    if (loading === false) {
      return this.props.children || null;
    }
    const {
      title,
      avatar,
      paragraph,
      loading,
      rows
    } = this.props;
    const {
      container,
      avatarCont,
      circleImage,
      squareImage,
      updownCont,
      titleCont,
      paragraphCont,
      paragraphRow
    } = styles
    return (
      <View style={container}>
        {avatar ?
          <View style={avatarCont}>
            <Animated.View style={avatar === 'circle' ? [circleImage, { backgroundColor: interpolatedBackground }] : [squareImage, { backgroundColor: interpolatedBackground }]} />
          </View>
          : null
        }
        <View style={updownCont}>
          {
            title &&
            <Animated.View style={[titleCont, { backgroundColor: interpolatedBackground }]} />
          }

          {
            paragraph &&
            <View style={paragraphCont}>
              {[...Array(rows)].map((item, idx) =>
                <Animated.View key={idx} style={[paragraphRow, { backgroundColor: interpolatedBackground }]} />
              )}
            </View>
          }
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 5,
    paddingHorizontal: 10,
  },
  avatarCont: {
    height: '100%',
    flexDirection: 'row',
    alignItems: 'flex-start', 
    justifyContent: 'center'
  },
  circleImage: {
    borderRadius: 20,
    height: 40,
    width: 40,
    marginRight: 10
  },
  squareImage: {
    height: 40,
    width: 40,
    marginRight: 10
  },
  updownCont: {
    flexDirection: 'column',
    flex: 1
  },
  titleCont: {
    width: '60%',
    height: '20%',
    marginBottom: 5
  },
  paragraphCont: {
  },
  paragraphRow: {
    marginVertical: 5,
    height: '15%',
    width: '90%'
  }

})

export default ContentLoading;
