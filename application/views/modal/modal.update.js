import React, { Component } from 'react'
import { View, Modal, Text, TouchableOpacity, Image, Alert } from 'react-native'
import * as Progress from 'react-native-progress'
import CodePush from 'react-native-code-push'
import Device from 'react-native-device-info'
import { inject, observer } from 'mobx-react'
import { System, DCColor, Screen, TextFont } from 'dacsee-utils'
import Resources from 'dacsee-resources'

const delay = (ms) => new Promise(resolve => setTimeout(() => resolve(), ms))

@inject('app')
@observer
class CodePushComponent extends Component {
  constructor(props) {
    super(props)
    this.state = {
      indeterminate: true,
      progress: 0,
      appPatchVisible: false,
    }
    this.running = false
  }

  async componentDidMount() {
    const remoteBundle = await CodePush.checkForUpdate($.config.env.deployment_key)

    if (this.running) return
    if (remoteBundle) {
      $.store.app.updateStatus = true

      this.downloadBundle(remoteBundle)
    } else {
      this.props.app.checkAppSetting()
    }
  }

  async downloadBundle(remoteBundle) {
    $.store.app.updateStatus = true

    let userLoggedIn = $.store.account && $.store.account.authToken

    if (!!userLoggedIn) {
      // CodePush.disallowRestart()
    } else {
      this.setState({ appPatchVisible: true })
    }

    if (!this.running) {
      this.running = true
      // await delay(3100)
      this.setState({ indeterminate: false })
      const bundle = await remoteBundle.download(progress => {
        const { receivedBytes, totalBytes } = progress
        const rate = (receivedBytes / totalBytes).toFixed(2)
        if (receivedBytes < totalBytes) {
          this.setState({ progress: parseFloat(rate) })
        } else {
          this.setState({ progress: 1 })
        }
      })

      // await delay(1500)
      this.setState({ indeterminate: true })
      await bundle.install()

      // await delay(1000)
      this.setState({ indeterminate: false, progress: 100 })

      // await delay(2000)
      this.setState({ appPatchVisible: false })
      this.running = false

      await CodePush.notifyAppReady()

      if (userLoggedIn) {
        Alert.alert(
          $.store.app.strings.app_update_codepush_title,
          $.store.app.strings.app_update_codepush_desc, [
            {
              text: $.store.app.strings.app_update_later
            }, {
              text: $.store.app.strings.app_update_restart,
              onPress: () => {
                CodePush.restartApp()
              }
            }
          ])
      } else {
        await CodePush.restartApp()
      }
    }
  }

  componentWillUnmount() {
    CodePush.allowRestart()
  }

  render() {
    const { indeterminate, progress, appPatchVisible } = this.state
    const Staging = Device.getBundleId().indexOf('stage') >= 0
    const { app } = this.props
    const { appSetting, appUpdateModalVisible } = app

    if (appPatchVisible) {
      return (
        <Modal onRequestClose={() => { }} animationType={'slide'} transparent={false} visible={appPatchVisible}>
          <View style={{ flex: 1 }}>
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
              <View style={{ height: 60, justifyContent: 'space-between', alignItems: 'center' }}>
                <Text style={{ color: '#999', fontSize: TextFont.TextSize(24), fontWeight: '200', backgroundColor: 'transparent' }}>DACSEE</Text>
                <Text style={{ fontSize: TextFont.TextSize(16), backgroundColor: 'transparent', color: '#333', fontWeight: '600' }}>{app.strings.new_update}</Text>
              </View>
              <View style={{ justifyContent: 'center', height: 120, alignItems: 'center', backgroundColor: 'transparent' }}>
                <Progress.Bar
                  width={Screen.window.width - 80}
                  height={2}
                  borderRadius={1}
                  useNativeDriver={true}
                  borderWidth={0}
                  progress={progress}
                  indeterminate={indeterminate}
                  color={'#666'}
                  unfilledColor={'#eee'}
                />
              </View>
              <View style={{ position: 'absolute', bottom: 0, left: 0, right: 0, height: 76, justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ color: '#999', fontSize: TextFont.TextSize(12), fontWeight: '200', backgroundColor: 'transparent' }}>{`${Staging ? 'STAGING' : ''}`}</Text>
                <Text style={{ color: '#999', fontSize: TextFont.TextSize(12), fontWeight: '200', backgroundColor: 'transparent' }}>{`${System.Version}-${Device.getBuildNumber()}`}</Text>
              </View>
            </View>
          </View>
        </Modal>
      )
    } else if (appUpdateModalVisible) {
      const { height, width } = Screen.window

      if (appSetting.versionType === 'hard') {
        return (
          <Modal onRequestClose={() => { }} animationType={'slide'} transparent={false} visible={appUpdateModalVisible}>
            <View style={{ width, height, justifyContent: 'flex-start', alignItems: 'center', backgroundColor: 'white', flexDirection: 'column' }}>
              <View style={{ height: 45, position: 'relative', backgroundColor: DCColor.BGColor('primary-1'), width: '100%', justifyContent: 'center', alignItems: 'center', flexDirection: 'column' }}>
                <Text style={{ fontSize: TextFont.TextSize(20), color: 'white', textAlign: 'center', fontWeight: '900' }}>DACSEE</Text>
                <View style={{ position: 'absolute', bottom: 0, width: '100%', height: 1, backgroundColor: 'rgba(255, 255, 255, 0.2)' }} />
              </View>
              <View style={{ backgroundColor: DCColor.BGColor('primary-1'), justifyContent: 'center', alignItems: 'center', padding: 10, width: '100%', height: 150 }}>
                <Image style={{ width: 100, height: 100 }} source={Resources.image.ufo} />
              </View>
              <View style={{ flex: 1, padding: 35, width: '100%', justifyContent: 'flex-start', alignItems: 'center' }}>
                <Text style={{ fontSize: TextFont.TextSize(30), width: '100%', textAlign: 'center', color: 'black', fontWeight: '900' }}>{app.strings.app_update_title_hard}</Text>
                <Text style={{ fontSize: TextFont.TextSize(15), color: 'rgba(0, 0, 0, 0.45)', marginBottom: 8 }}>{`(${app.strings.version} ${appSetting.version})`}</Text>
                <View style={{ width: '40%', height: 10, backgroundColor: '#DD1C1A', borderRadius: 10, marginBottom: 25 }} />
                <Text style={{ fontSize: TextFont.TextSize(18), color: '#898989', textAlign: 'center' }}>{app.strings.app_update_desc_hard}</Text>
              </View>
              <View style={{ width: '100%', flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                <View style={{ height: 1, width: '90%', backgroundColor: 'rgba(0, 0, 0, 0.05)', marginBottom: 30 }} />
                <View style={{ width: '100%', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around', marginBottom: 35 }}>
                  <TouchableOpacity onPress={() => this.props.app.openAppStore()} activeOpacity={.7}>
                    <Text style={{ color: '#000', fontSize: TextFont.TextSize(26), fontWeight: '900', color: DCColor.BGColor('primary-1') }}>{app.strings.app_update_now}</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </Modal>
        )
      } else {
        return (
          <Modal onRequestClose={() => { }} animationType={'slide'} transparent={false} visible={appUpdateModalVisible}>
            <View style={{ width, height, justifyContent: 'flex-start', alignItems: 'center', backgroundColor: 'white', flexDirection: 'column' }}>
              <View style={{ height: 45, backgroundColor: DCColor.BGColor('primary-1'), width: '100%', justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
                <Text style={{ fontSize: TextFont.TextSize(20), color: 'white', textAlign: 'center', fontWeight: '900' }}>DACSEE</Text>
                <View style={{ position: 'absolute', bottom: 0, width: '100%', height: 1, backgroundColor: 'rgba(255, 255, 255, 0.2)' }} />
              </View>
              <View style={{ backgroundColor: DCColor.BGColor('primary-1'), justifyContent: 'center', alignItems: 'center', padding: 10, width: '100%', height: 150 }}>
                <Image style={{ width: 100, height: 100 }} source={Resources.image.ufo} />
              </View>
              <View style={{ flex: 1, padding: 35, width: '100%', justifyContent: 'flex-start', alignItems: 'center' }}>
                <Text style={{ fontSize: TextFont.TextSize(30), width: '100%', textAlign: 'center', color: 'black', marginBottom: 3, fontWeight: '900' }}>{app.strings.app_update_title_soft}</Text>
                <Text style={{ fontSize: TextFont.TextSize(15), color: 'rgba(0, 0, 0, 0.45)', marginBottom: 8 }}>{`(${app.strings.version} ${appSetting.version})`}</Text>
                <View style={{ width: '40%', height: 10, backgroundColor: '#DD1C1A', borderRadius: 10, marginBottom: 25 }} />
                <Text style={{ fontSize: TextFont.TextSize(18), color: '#898989', textAlign: 'center' }}>{app.strings.app_update_desc_soft}</Text>
              </View>
              <View style={{ width: '100%', flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                <View style={{ height: 1, width: '90%', backgroundColor: 'rgba(0, 0, 0, 0.05)', marginBottom: 30 }} />
                <View style={{ width: '100%', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around', marginBottom: 35 }}>
                  <TouchableOpacity onPress={() => this.props.app.closeAppUpdateModal()} activeOpacity={.7}>
                    <Text style={{ color: '#000', fontSize: TextFont.TextSize(26), fontWeight: '900', color: DCColor.BGColor('primary-1') }}>{app.strings.app_update_later}</Text>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => this.props.app.openAppStore()} activeOpacity={.7}>
                    <Text style={{ color: '#000', fontSize: TextFont.TextSize(26), fontWeight: '900', color: DCColor.BGColor('primary-1') }}>{app.strings.app_update_now}</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </Modal>
        )
      }
    } else {
      return null
    }
  }
}

export default CodePushComponent