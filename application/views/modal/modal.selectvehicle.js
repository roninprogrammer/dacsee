import React, { Component } from 'react'
import { Text, View, TouchableOpacity, Modal, StyleSheet, Image, Alert } from 'react-native'
import Material from 'react-native-vector-icons/MaterialIcons'
import { inject, observer } from 'mobx-react'
import { Screen, TextFont, DCColor } from 'dacsee-utils'
import Resources from "dacsee-resources"

const { height, width } = Screen.window
const styles = StyleSheet.create({
  backdropStyle: {
    width,
    height,
    backgroundColor: 'rgba(0, 0, 0, .65)',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20
  },
  mainCont: {
    backgroundColor: 'white',
    borderRadius: 10,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    width: width - 50,
    maxHeight: (height / 1.5)
  },
  fixedCont: {
    padding: 30,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  imgCircle: {
    backgroundColor: 'rgba(0, 0, 0, 0.05)',
    height: 100,
    width: 100,
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20
  },
  imgCont: {
    paddingTop: 50,
    height: 100,
    width: 100,
  },
  mainTitle: {
    fontWeight: '900',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: TextFont.TextSize(15),
    color: 'black',
    paddingTop: 10
  },
  subsentences: {
    fontWeight: '200',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: TextFont.TextSize(12),
    paddingBottom: 10
  },
  smallCont: {
    backgroundColor: 'rgba(0, 0, 0, 0.05)',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
    height: 50,
    paddingHorizontal: 25,
    borderBottomColor: 'rgba(0, 0, 0 , 0.25)',
    borderBottomWidth: 0.25
  },
  vehicleCont: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  imgIcon: {
    height: 30,
    width: 30,
    marginRight: 10
  },
  carType: {
    fontSize: TextFont.TextSize(12),
    color: 'black',
    fontWeight: '500'
  },
  tickCircle: {
    width: 23,
    height: 23,
    borderRadius: 13,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    backgroundColor: '#e7e7e7',
  },
  rowCont: {
    padding: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  cancelBtn: {
    flex: 1,
    borderRadius: 6,
    backgroundColor: 'rgba(0, 0, 0, 0.05)',
    justifyContent: 'center',
    alignItems: 'center',
    height: 45,
    marginRight: 5
  },
  confirmButton: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 45,
    borderRadius: 6,
    backgroundColor: DCColor.BGColor('primary-1'),
    marginLeft: 5
  },
  textStyle: {
    fontSize: TextFont.TextSize(15),
    color: 'black',
    fontWeight: 'bold',
    justifyContent: 'center',
    alignItems: 'center'
  },
  cancelBtnText: {
    fontWeight: '500',
    color: 'rgba(0, 0, 0, 0.45)'
  }
})

@inject('app', 'vehicles')
@observer
export default class SelectVehicles extends Component {
  constructor(props) {
    super(props)
  }

  async submit(_id) {
    const res = await $.store.vehicles.updateVehicleCategory(_id)
    if (res === true) {
      Alert.alert(this.props.app.strings.vehicle_title, this.props.app.strings.vehicle_update_success,
        [
          { text: 'OK', onPress: () => this.props.onClose() },
        ],
        { cancelable: false })
    }
    else if (res === '0 selected') {
      Alert.alert(this.props.app.strings.vehicle_title_empty_select, this.props.app.strings.vehicle_category_select_empty_toast,
        [
          { text: 'OK' },
        ],
        { cancelable: false })
    }
  }

  render() {
    const { strings } = this.props.app
    const { vehiclesData } = $.store.vehicles
    const { availableCategories, _id } = vehiclesData[0]
    const { visible, onClose } = this.props
    const { backdropStyle, mainCont, cancelBtnText, imgCircle, imgCont, mainTitle, subsentences, smallCont, vehicleCont, imgIcon, carType, tickCircle, rowCont, cancelBtn, confirmButton, textStyle } = styles
    return (
      <Modal
        animationType='fade'
        transparent={true}
        visible={visible}
        onRequestClose={onClose}
      >
        <View style={backdropStyle}>
          <View style={mainCont}>
            <View style={{ alignItems: 'center', paddingTop: 20, width: '100%' }}>
              <View style={imgCircle}>
                <Image style={imgCont} source={Resources.image.attention_icon} />
              </View>
              <Text style={mainTitle}>{strings.receive_job_from}</Text>
              <Text style={subsentences}>{strings.vehicle_sentences}</Text>
              {availableCategories.length > 0 && availableCategories.map((i, idx) =>
                <TouchableOpacity key={idx} style={smallCont} onPress={() => this.props.vehicles.getCheckVehicle(idx)}>
                  <View style={vehicleCont}>
                    <Image style={imgIcon} source={{ uri: i.avatars[i.avatars.length - 1].url }} />
                    <Text style={carType}>{i.name}</Text>
                  </View>
                  <View style={[tickCircle, { backgroundColor: $.store.vehicles.vehiclesData[0].availableCategories[idx].checked ? DCColor.BGColor('green') : '#e7e7e7' }]}>
                    {$.store.vehicles.vehiclesData[0].availableCategories[idx].checked ?
                      <Material name='check' color='white' size={22} /> : null
                    }
                  </View>
                </TouchableOpacity>
              )}
              <View style={rowCont}>
                <TouchableOpacity style={cancelBtn} onPress={onClose}>
                  <Text style={[textStyle, cancelBtnText]}>{strings.cancel.toUpperCase()}</Text>
                </TouchableOpacity>
                <TouchableOpacity style={confirmButton} onPress={() => this.submit(_id)}>
                  <Text style={textStyle}>{strings.confirm.toUpperCase()}</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </Modal>
    )
  }
}