import React, { Component } from 'react'
import { Text, Modal, TouchableOpacity, View, Image, StyleSheet, Dimensions, ScrollView, SafeAreaView } from 'react-native'
import { inject, observer } from 'mobx-react'
import { TextFont, DCColor, Avatars } from 'dacsee-utils'
import { DriverInfoCont } from 'global-component'
import Resources from 'dacsee-resources'
import moment from 'moment'


const { width } = Dimensions.get('window')

@inject('app')
@observer
export default class DCBookingDetailModal extends Component {
  
  componentWillUnmount() {
    const {unmountFunction} = this.props

    unmountFunction && this.props.unmountFunction()
  }

  render() {
    const {
      onHide,
      visible,
      bookingStatus,
      bookingETA,
      bookingETAUpdate,
      advDate,
      advTime,
      avatar,
      name,
      userId,
      licenseNumber,
      rating,
      callFunction,
      messageFunction,
      bookingId,
      pickupAddress,
      pickupNavigateFunction,
      dropoffAddress,
      dropoffNavigateFunction,
      campaignImg,
      campaignName,
      driverNote,
      price,
      paymentType,
      rejectFunction,
      rejectBtnLabel,
      acceptFunction,
      acceptBtnLabel,
      unreadChat
    } = this.props
    const {
      mainCont,
      headCont,
      collapseCont,
      collapseBar,
      collapseIcon,
      bookingStatusCont,
      fBody,
      bodyCont,
      dcCol,
      fCaption,
      dcBookingId,
      addressCont,
      addressIndicator,
      addressBox,
      fTitle,
      navBtn,
      dcIcon,
      campaignAvatar,
      dcTag,
      actionCont,
      dcBtn,
      fBtn
    } = styles
    const { strings } = $.store.app

    return (
      <Modal
        transparent={true}
        onRequestClose={onHide}
        visible={visible}
        animationType={'slide'}
      >
        <SafeAreaView style={{ flex: 1 }}>
          <View style={mainCont}>
            <View style={headCont}>
              <TouchableOpacity style={collapseCont} activeOpacity={1} onPress={onHide}>
                <View style={collapseBar} onPress={onHide}>
                  <Image style={collapseIcon} source={Resources.image.collapse_icon} />
                </View>
                {bookingStatus ?
                  <View style={bookingStatusCont}>
                    {(() => {
                      switch (bookingStatus.toUpperCase()) {
                        case 'PENDING_ASSIGNMENT':
                          return <Text style={fBody}>{strings.confirm_wait_order}</Text>
                        case 'PENDING_ACCEPTANCE':
                          return <Text style={fBody}>{strings.confirm_wait_order}</Text>
                        case 'CONFIRMED':
                          return <Text style={fBody}>{strings.booking_status_confirmed}</Text>
                        case 'ON_THE_WAY':
                          return <Text style={fBody}>{strings.on_the_way}</Text>
                        case 'ARRIVED':
                          return <Text style={fBody}>{strings.booking_status_arrived}</Text>
                        case 'NO_SHOW':
                          return <Text style={fBody}>{strings.No_Show}</Text>
                        case 'ON_BOARD':
                          return <Text style={fBody}>{strings.booking_status_on_board}</Text>
                        case 'COMPLETED_BY_PASSENGER':
                          return <Text style={fBody}>{strings.booking_status_completed_by_passenger}</Text>
                        case 'FINALIZE_TOTAL_FARE':
                          return <Text style={fBody}>{strings.booking_status_finalize_total_fare}</Text>
                        case 'COMPLETED':
                          return <Text style={fBody}>{strings.Completed}</Text>
                        case 'CANCELLED_BY_PASSENGER':
                          return <Text style={fBody}>{strings.Cancelled_by_Passenger}</Text>
                        case 'CANCELLED_BY_DRIVER':
                          return <Text style={fBody}>{strings.Cancelled_by_Driver}</Text>
                        case 'CANCELLED_BY_SYSTEM':
                          return <Text style={fBody}>{strings.Cancelled_by_System}</Text>
                        case 'REJECTED_BY_DRIVER':
                          return <Text style={fBody}>{strings.Rejected_by_Driver}</Text>
                        case 'NO_DRIVER_AVAILABLE':
                          return <Text style={fBody}>{strings.booking_status_no_driver_avaiable}</Text>
                        case 'NO_TAKER':
                          return <Text style={fBody}>{strings.No_Taker}</Text>
                        default:
                          return null
                      }
                    })()}

                  {
                    bookingETA ? (
                      bookingETA >= 3600 ? (
                        <Text style={[fBody, { marginLeft: 5, color: 'rgba(255, 255, 255, 0.45)' }]}>ETA ~ {moment.utc(bookingETA*1000).add(1, 'm').format('h[h] m[m]')}</Text>
                      ) : (
                        bookingETA >= 60 ? (
                          <Text style={[fBody, { marginLeft: 5, color: 'rgba(255, 255, 255, 0.45)' }]}>ETA ~ {moment.utc(bookingETA*1000).add(1, 'm').format('m')}{strings.eta_minutes}</Text>
                        ) : (
                          <Text style={[fBody, { marginLeft: 5, color: 'rgba(255, 255, 255, 0.45)' }]}>ETA ~ 1{strings.eta_minutes}</Text>
                        )
                      )
                    ) : null
                  }

                  {
                    bookingETAUpdate ? (
                      <TouchableOpacity onPress={bookingETAUpdate}>
                        <Image style={[dcIcon, { marginLeft: 5, width: 20, height: 20, tintColor: 'rgba(255, 255, 255, 0.45)' }]} source={Resources.image.edit} />
                      </TouchableOpacity>
                    ) : null
                  }

                  </View>
                  : null
                }
              </TouchableOpacity>
            </View>
            {(avatar || name || userId || rating || callFunction || messageFunction) ?
              <DriverInfoCont
                avatar={avatar}
                name={name}
                id={userId}
                rating={rating}
                callFunction={callFunction}
                messageFunction={messageFunction}
                unreadChat={unreadChat}
              />
              : null
            }
            {(advTime || advDate) ?
              <View style={[dcCol, { backgroundColor: DCColor.BGColor('red'), height: 50 }]}>
                <Text style={[fTitle, { color: 'white', flex: 1, textAlign: 'left' }]}>{strings.advance_booking}</Text>
                <View style={[dcTag, { backgroundColor: 'rgba(0, 0, 0, 0.7)', height: '100%' }]}>
                  <Text style={[fCaption, { color: 'white', fontWeight: '900' }]}>{advDate}</Text>
                  <Text style={[fCaption, { color: 'white', marginLeft: 5, fontWeight: '900', color: DCColor.BGColor('primary-1') }]}>{advTime}</Text>
                </View>
              </View>
              : null
            }
            <ScrollView contentContainerStyle={bodyCont}>
              {licenseNumber ? 
              <View style={dcCol}>
                  <Text style={fCaption}>{strings.booking_detail_evp_number_label}</Text>
                  <Text style={[fCaption, dcBookingId]}>{licenseNumber}</Text>
              </View>
              : null
              }
              <View style={dcCol}>
                <Text style={fCaption}>{strings.booking_id}</Text>
                <Text style={[fCaption, dcBookingId]}>{bookingId}</Text>
              </View>
              <View style={dcCol}>
                <View style={addressCont}>
                  <View style={addressIndicator} />
                  <View style={addressBox}>
                    <Text style={fTitle}>{strings.pick_up_address}</Text>
                    <Text numberOfLines={2} ellipsizeMode='tail' style={fCaption}>{pickupAddress}</Text>
                  </View>
                  {pickupNavigateFunction ?
                    <TouchableOpacity style={[navBtn, { marginLeft: 30 }]} onPress={pickupNavigateFunction}>
                      <Image style={[dcIcon, { tintColor: 'white' }]} source={Resources.image.navigate_icon} />
                    </TouchableOpacity>
                    : null
                  }
                </View>
              </View>
              <View style={dcCol}>
                <View style={addressCont}>
                  <View style={[addressIndicator, { backgroundColor: DCColor.BGColor('primary-1') }]} />
                  <View style={addressBox}>
                    <Text style={fTitle}>{strings.drop_off_address}</Text>
                    <Text numberOfLines={2} ellipsizeMode='tail' style={fCaption}>{dropoffAddress}</Text>
                  </View>
                  {dropoffNavigateFunction ?
                    <TouchableOpacity style={[navBtn, { marginLeft: 30, backgroundColor: DCColor.BGColor('primary-1') }]} onPress={dropoffNavigateFunction}>
                      <Image style={dcIcon} source={Resources.image.navigate_icon} />
                    </TouchableOpacity>
                    : null
                  }
                </View>
              </View>
              <View style={dcCol}>
                <Image source={!!campaignImg ? { uri: Avatars.getHeaderPicUrl(campaignImg) } : Resources.image.booking_detail_car} style={!!campaignImg ? campaignAvatar : [campaignAvatar, { borderRadius: 0 }]} />
                <Text style={fTitle}>{campaignName}</Text>
              </View>
              <View style={[dcCol, { backgroundColor: DCColor.BGColor('disable'), borderColor: 'transparent' }]}>
                <Image style={dcIcon} source={Resources.image.driver_note_icon} />
                <Text style={[fCaption, { marginLeft: 20 }]}>{driverNote}</Text>
              </View>
              <View style={[dcCol, { borderColor: 'transparent' }]}>
                <Image style={dcIcon} source={Resources.image.payment_type_icon} />
                <Text style={[fCaption, { marginLeft: 20, color: DCColor.BGColor('primary-2'), fontWeight: '700' }]}>RM {price ? price.toFixed(2) : '0.00'}</Text>
                <View style={dcTag}>
                  <Text style={[fCaption, { color: DCColor.BGColor('primary-2'), fontWeight: '700' }]}>{paymentType}</Text>
                </View>
              </View>
            </ScrollView>
            {(rejectFunction || acceptFunction) ?
              <View style={actionCont}>
                {rejectFunction ?
                  <TouchableOpacity style={[dcBtn, { backgroundColor: DCColor.BGColor('red'), marginRight: 10 }]} onPress={rejectFunction}>
                    <Text style={[fBtn, { color: 'white' }]}>{rejectBtnLabel}</Text>
                  </TouchableOpacity>
                  : null
                }
                {acceptFunction ?
                  <TouchableOpacity style={dcBtn} onPress={acceptFunction}>
                    <Text style={fBtn}>{acceptBtnLabel}</Text>
                  </TouchableOpacity>
                  : null
                }
              </View>
              : null
            }
          </View>
        </SafeAreaView>
      </Modal>
    )
  }
}

const styles = StyleSheet.create({
  mainCont: {
    width,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'flex-start'
  },
  headCont: {
    width,
    backgroundColor: DCColor.BGColor('primary-2')
  },
  collapseCont: {
    backgroundColor: DCColor.BGColor('primary-2'),
  },
  collapseBar: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 5,
  },
  collapseIcon: {
    resizeMode: 'contain',
    tintColor: DCColor.BGColor('primary-1'),
    opacity: .4,
    height: 9,
    width: 35
  },
  bookingStatusCont: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: 10,
    borderBottomWidth: 1,
    borderColor: 'rgba(255, 255, 255, 0.1)'
  },
  fBody: {
    fontSize: TextFont.TextSize(15),
    fontWeight: '900',
    color: DCColor.BGColor('primary-1'),
  },
  bodyCont: {
    width,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    flex: 1,
  },
  dcCol: {
    width,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderColor: 'rgba(0, 0, 0, 0.05)',
    paddingVertical: 10,
    paddingHorizontal: 15,
  },
  fCaption: {
    fontSize: TextFont.TextSize(12),
    fontWeight: '400',
    color: 'rgba(0, 0, 0, 0.45)',
    textAlign: 'left',
  },
  dcBookingId: {
    fontWeight: '700',
    color: DCColor.BGColor('primary-2'),
    textAlign: 'right',
    flex: 1
  },
  addressCont: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    flex: 1,
  },
  addressIndicator: {
    width: 10,
    height: 10,
    padding: 5,
    borderRadius: 5,
    backgroundColor: DCColor.BGColor('secondary-1'),
    marginRight: 20,
    marginLeft: 10,
    marginTop: 6
  },
  addressBox: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  fTitle: {
    fontSize: TextFont.TextSize(15),
    fontWeight: '700',
    textAlign: 'left'
  },
  navBtn: {
    height: 45,
    width: 45,
    borderRadius: 45/2,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: DCColor.BGColor('secondary-1'),
    shadowOffset: { width: 0, height: -1 },
    shadowColor: '#000',
    shadowOpacity: 0.05,
    elevation: 3
  },
  dcIcon: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
    tintColor: DCColor.BGColor('primary-2')
  },
  campaignAvatar: {
    width: 25,
    height: 25,
    borderRadius: 25/2,
    resizeMode: 'cover',
    marginRight: 15
  },
  dcTag: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: DCColor.BGColor('primary-1'),
    borderRadius: 50,
    padding: 1,
    paddingHorizontal: 8,
    marginLeft: 10
  },
  actionCont: {
    width,
    padding: 20,
    paddingVertical: 40,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  dcBtn: {
    width: '100%',
    borderRadius: 4,
    height: 55,
    backgroundColor: DCColor.BGColor('primary-1'),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1
  },
  fBtn: {
    fontSize: TextFont.TextSize(15),
    fontWeight: '700',
    color: DCColor.BGColor('primary-2')
  }
})