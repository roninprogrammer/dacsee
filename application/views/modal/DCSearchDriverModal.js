import React, { Component } from 'react'
import { Text, Modal, TouchableOpacity, View, Image, StyleSheet, Dimensions, Animated, SafeAreaView } from 'react-native'
import { inject, observer } from 'mobx-react'
import Lottie from 'lottie-react-native'
import { TextFont, DCColor } from 'dacsee-utils'
import Resources from 'dacsee-resources'

const { width } = Dimensions.get('window')

@inject('app')
@observer
export default class DCSearchDriverModal extends Component {

  constructor(props) {
    super(props)
    this.radar = new Animated.Value(0)
  }

  componentWillUpdate() {
    Animated.loop(Animated.timing(this.radar, { toValue: 1, fromValue: 0, duration: 2500 })).start()
  }

  render() {
    const {
      onHide,
      visible,
      price,
      pickUpAddress,
      dropOffAddress,
      cancelFunction
    } = this.props
    const {
      mainCont,
      animationCont,
      lottieItem,
      dcCard,
      headCont,
      fTitle,
      priceTag,
      fDisplay,
      bodyCont,
      dcCol,
      addressIndicator,
      actionCont,
      actionBox,
      dcBtn,
      dcIcon
    } = styles

    const {strings} = $.store.app

    return (
      <Modal
        transparent={true}
        onRequestClose={onHide}
        visible={visible}
        animationType={'slide'}
      >
        <SafeAreaView style={{flex: 1}}>
        <View style={mainCont}>
          <View style={animationCont}>
            <Lottie style={lottieItem} progress={this.radar} source={require('../../res/animation/ufo_radar_bg.json')} />
          </View>
          <View style={dcCard}>
            <View style={headCont}>
              <Text style={fTitle}>{strings.confirm_wait_order}</Text>
              <View style={priceTag}>
                <Text style={fDisplay}>RM{price}</Text>
              </View>
            </View>
            <View style={bodyCont}>
              <View style={dcCol}>
                <View style={addressIndicator} />
                <Text style={[fTitle, { color: DCColor.BGColor('primary-2') }]}>{pickUpAddress}</Text>
              </View>
              <View style={dcCol}>
                <View style={[addressIndicator, { backgroundColor: DCColor.BGColor('primary-1') }]} />
                <Text style={[fTitle, { color: DCColor.BGColor('primary-2') }]}>{dropOffAddress}</Text>
              </View>
            </View>
          </View>
          <View style={actionCont}>
            <View style={actionBox}>
              <TouchableOpacity style={dcBtn} onPress={cancelFunction}>
                <Image style={dcIcon} source={Resources.image.close} />
              </TouchableOpacity>
            </View>
          </View>
        </View>
        </SafeAreaView>
      </Modal>
    )
  }
}

const styles = StyleSheet.create({
  mainCont: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    width,
    backgroundColor: DCColor.BGColor('primary-1'),
    padding: 40
  },
  animationCont: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  lottieItem: {
    width: width * .7,
    height: width * .7,
  },
  dcCard: {
    width: '100%',
    borderRadius: 4,
    backgroundColor: 'white',
    shadowOffset: { width: 0, height: -1 },
    shadowColor: '#000',
    shadowOpacity: 0.05,
    elevation: 4
  },
  headCont: {
    backgroundColor: DCColor.BGColor('primary-2'),
    borderTopLeftRadius: 4,
    borderTopRightRadius: 4,
    padding: 15,
    paddingVertical: 10,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  fTitle: {
    fontSize: TextFont.TextSize(15),
    fontWeight: '700',
    color: 'white',
    marginBottom: 5
  },
  priceTag: {
    backgroundColor: 'rgba(0, 0, 0, 0.75)',
    padding: 5,
    paddingHorizontal: 15,
    borderRadius: 100
  },
  fDisplay: {
    fontSize: TextFont.TextSize(22),
    fontWeight: '700',
    color: DCColor.BGColor('primary-1')
  },
  bodyCont: {
    padding: 20,
    paddingVertical: 10,
    paddingBottom: 0,
    marginBottom: 15
  },
  dcCol: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingVertical: 5
  },
  addressIndicator: {
    width: 10,
    height: 10,
    borderRadius: 15,
    backgroundColor: DCColor.BGColor('secondary-1'),
    marginRight: 15
  },
  actionCont: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  actionBox: {
    backgroundColor: 'rgba(0, 0, 0, 0.15)',
    width: 80,
    height: 80,
    borderRadius: 80,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20
  },
  dcBtn: {
    borderRadius: 65,
    width: 65,
    height: 65,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    shadowOffset: { width: 0, height: -1 },
    shadowColor: '#000',
    shadowOpacity: 0.05,
    elevation: 4,
    backgroundColor: 'white'
  },
  dcIcon: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
    opacity: .3
  }
})