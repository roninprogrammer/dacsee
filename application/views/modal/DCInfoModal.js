import React, { Component } from 'react'
import { Text, Modal, TouchableOpacity, View, Image, StyleSheet, Dimensions } from 'react-native'
import { inject, observer } from 'mobx-react'
import { TextFont, DCColor, Icons } from 'dacsee-utils'
import Resources from 'dacsee-resources'

const { width } = Dimensions.get('window')

@inject('app')
@observer
export default class DCInfoModal extends Component {
  constructor(props) {
    super(props)

    this.state = {
      star: []
    }
  }

  componentDidMount() {
    const { rating } = this.props
    this.driverRating(rating)
  }

  driverRating = (i) => {
    switch (i) {
      case 1:
        this.star = [true, false, false, false, false]
        break
      case 2:
        this.star = [true, true, false, false, false]
        break
      case 3:
        this.star = [true, true, true, false, false]
        break
      case 4:
        this.star = [true, true, true, true, false]
        break
      default:
        this.star = [true, true, true, true, true]
        break
    }
  }

  render() {
    const {
      onHide,
      visible,
      rating,
      rateFunction,
      children,
      confirmBtn,
      errorBtn,
      cancelBtn,
      label,
      caption,
      licenseNumber,
      onPress,
    } = this.props
    const {
      mainCont,
      graphicCont,
      bodyCont,
      fTitle,
      dcCol,
      fCaption,
      fCaptionBold,
      ratingCont,
      ratingIcon,
      actionCont,
      dcCancelBtn,
      dcImg,
      dcBtn,
      fBtn,
    } = styles
    const { strings } = $.store.app

    this.driverRating(rating)

    return (
      <Modal
        transparent={true}
        onRequestClose={onHide}
        visible={visible}
        animationType={'slide'}
      >
        <View style={mainCont}>
          <View style={graphicCont}>
            {children}
          </View>
          <View style={bodyCont}>
            <Text style={fTitle}>{label}</Text>
            <View style={rating ? [dcCol, { marginBottom: 5 }] : dcCol}>
              <Text style={licenseNumber ? fCaptionBold : fCaption}>{caption}</Text>
              {licenseNumber ?
                <Text style={fCaption}>({strings.booking_detail_evp_number_label}: {licenseNumber})</Text>
                : null
              }
            </View>
            {rating ?
              <View style={rateFunction ? [ratingCont, { justifyContent: 'space-between' }] : ratingCont}>
                {this.star && this.star.map((rate, index) => (
                  <TouchableOpacity key={index} style={ratingIcon} activeOpacity={rateFunction ? .3 : 1} onPress={() => rateFunction(index)}>
                    {Icons.Generator.Awesome('star', rateFunction ? width * .09 : 12, rate ? DCColor.BGColor('primary-1') : DCColor.BGColor('disable'))}
                  </TouchableOpacity>
                ))}
              </View>
              : null
            }
            <View style={actionCont}>
              {cancelBtn ?
                <TouchableOpacity style={dcCancelBtn} onPress={onHide}>
                  <Image style={dcImg} source={Resources.image.close} />
                </TouchableOpacity>
                : null
              }
              {confirmBtn ?
                <TouchableOpacity style={dcBtn} onPress={onPress}>
                  <Text style={fBtn}>{confirmBtn.toUpperCase()}</Text>
                </TouchableOpacity>
                : null
              }
              {errorBtn ?
                <TouchableOpacity style={[dcBtn, { backgroundColor: DCColor.BGColor('red') }]} onPress={onPress}>
                  <Text style={[fBtn, { color: 'white' }]}>{errorBtn && errorBtn.toUpperCase()}</Text>
                </TouchableOpacity>
                : null
              }
            </View>
          </View>
        </View>
      </Modal>
    )
  }
}

const styles = StyleSheet.create({
  mainCont: {
    flex: 1,
    width,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'flex-start'
  },
  graphicCont: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    width,
    backgroundColor: DCColor.BGColor('primary-1'),
    padding: 30
  },
  bodyCont: {
    width,
    backgroundColor: 'white',
    padding: 30
  },
  fTitle: {
    fontSize: TextFont.TextSize(18),
    fontWeight: '700',
    color: DCColor.BGColor('primary-2')
  },
  dcCol: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginBottom: 50,
    width: '100%'
  },
  fCaption: {
    fontSize: TextFont.TextSize(12),
    fontWeight: '400',
    color: 'rgba(0, 0, 0, 0.45)',
  },
  fCaptionBold: {
    fontSize: TextFont.TextSize(12),
    fontWeight: '900',
    color: 'rgba(0, 0, 0, 0.75)',
    marginRight: 5
  },
  ratingCont: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    marginBottom: 50
  },
  ratingIcon: {
    marginRight: 3
  },
  actionCont: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  dcCancelBtn: {
    width: 55,
    height: 55,
    borderRadius: 55,
    backgroundColor: 'rgba(0, 0, 0, 0.1)',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 10
  },
  dcImg: {
    height: 25,
    width: 25,
    resizeMode: 'contain',
    opacity: .3
  },
  dcBtn: {
    flex: 1,
    height: 55,
    borderRadius: 4,
    backgroundColor: DCColor.BGColor('primary-1'),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  fBtn: {
    fontSize: TextFont.TextSize(15),
    fontWeight: '700',
    color: DCColor.BGColor('primary-2')
  },
})