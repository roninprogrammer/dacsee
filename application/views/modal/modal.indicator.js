import React, { Component } from 'react'
import { Text, Modal, View, ActivityIndicator } from 'react-native'
import { inject, observer } from 'mobx-react'
import { TextFont } from 'dacsee-utils'
@inject('app')
@observer
export default class Indicator extends Component {
  render () {
    const { indicatorVisible, indicatorMsg } = this.props.app
    const size = !!indicatorMsg ? 150 : 80

    return (
      <Modal
        transparent= {true}
        onRequestClose={() => {}}
        visible={indicatorVisible}
        animationType={'fade'}
      >
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <View style={{ justifyContent: 'center', alignItems: 'center', width: size, height: size, backgroundColor: 'rgba(51, 51, 51, 0.75)', borderRadius: 10 }}>
            <ActivityIndicator size='large' color='white' />
            {
              !!indicatorMsg ?
                <Text style={{textAlign: 'center', fontSize: TextFont.TextSize(12), fontWeight: '600', color: 'white', marginTop: 10}}>{indicatorMsg}</Text>
                : null
            }
          </View>
        </View>
      </Modal>
    )
  }
}
