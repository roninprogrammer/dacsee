/* eslint-disable */
import React, { Component } from 'react'
import { View, ScrollView, TouchableOpacity, Text, Image, TextInput, ListView, FlatList, Platform, StatusBar, StyleSheet } from 'react-native'
import { computed, toJS } from 'mobx'
import { inject, observer } from 'mobx-react'
import { debounce } from 'lodash'
import { Icons, Define, TextFont, DCColor } from 'dacsee-utils'
import { Button } from '../../components'
import SponsorListItem from '../main/sponsorLocation/component/Sponsor.List.Item'
import SponsorInfo from '../main/sponsorLocation/component/Sponsor.info';
import Resources from 'dacsee-resources'
import DcEmptyState from '../../components/emptyState'
import { AnswersSrv } from '../../global/service'

const dataContrast = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })
const dataJoyContrast = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })

@inject('passenger', 'app', 'joy')
@observer
export default class SelectAddressModal extends Component {
  static navigationOptions = { header: null }

  @computed get dataSourceJoy() {
    if ($.method.utils.isArray(this.props.joy.sponsorLocationList) && this.state.keywords.length !== 0) {
      return this.props.joy.sponsorLocationList
    } else if (this.state.keywords.length === 0) {
      return this.props.joy.sponsorLocationNearbyList
    } else {
      return []
    }
  }

  @computed get dataSource() {
    let places = []
    let sponsorList = []
    let featuredList = []

    if ($.method.utils.isArray(this.props.passenger.places) && this.state.keywords.length !== 0) {
      places = toJS(this.props.passenger.places)
    } else {
      places = toJS(this.props.passenger.favorite)
    }

    if (this.state.type === 'DestinationAddress') {
      if ($.method.utils.isArray(this.props.joy.sponsorLocationList) && this.state.keywords.length) {
        sponsorList = this.props.joy.sponsorLocationList.slice(0, 3) // Limit Temporarily
      } else if ($.method.utils.isArray(this.props.joy.sponsorLocationNearbyList) && !this.state.keywords.length) {
        // sponsorList = this.props.joy.sponsorLocationNearbyList.slice() // Excluded Temporarily
      }

      featuredList = sponsorList.filter((joy) => joy.type === 'featured').map((joy) => {
        return {
          _id: joy._id,
          tagline: joy.tagline,
          name: joy.name,
          address: joy.address,
          coords: {
            lat: joy.coords.lat ? parseFloat(joy.coords.lat) : 0,
            lng: joy.coords.lng ? parseFloat(joy.coords.lng) : 0
          },
          distance: 0,
          placeId: null,
          joy: true
        }
      })

      places = featuredList.concat(places)
    }

    return places
  }

  constructor(props) {
    super(props)
    this.searchPlaces = debounce(this.searchPlaces, 500)
    this.state = {
      type: 'FromAddress',
      keywords: '',
      selectedTab: 'searchPlaces',
      onPickAddress: null
    }
  }

  async getPlaces(keywords) {
    if (keywords.length < 3) {
      this.props.passenger.places = []
      this.props.joy.sponsorLocationList = []

    } else {

      if (this.state.type === 'DestinationAddress') await this.props.joy.searchSponsorLocationList(keywords, $.store.passenger.from.coords)

      if (keywords.length >= 15 && !$.method.utils.isArray(this.props.passenger.places)) return

      await this.props.passenger.onAutoComplete(keywords, this.props.app.coords)
    }
  }

  searchPlaces(text) {
    this.setState({ keywords: text })
    this.getPlaces(text)
  }

  componentDidMount() {
    const { type = 'FromAddress', keywords = '', selectedTab = 'searchPlaces', onPickAddress } = this.props.navigation.state.params
    this.setState({
      type: type,
      keywords: keywords,
      selectedTab: selectedTab,
      onPickAddress: onPickAddress
    })

    this.getPlaces(keywords)

    if (keywords.length < 3) {
      this.props.passenger.places = []
      this.props.joy.getNearby()
      this.props.passenger.getFavorite()
    }
  }

  componentWillUnmount() {
    this.dispose && this.dispose()
  }

  handleTab = (selectedTab) => {
    this.setState({
      selectedTab
    })
  }

  _PlacesListComponent = () => {
    const { passenger } = this.props
    const { onPickAddress, keywords, type } = this.state

    return (
      <ScrollView
        showsVerticalScrollIndicator={true}
        keyboardShouldPersistTaps={'handled'}
        keyboardDismissMode={'on-drag'}
      >
        <FlatList
          keyboardShouldPersistTaps='handled'
          removeClippedSubviews={false}
          keyExtractor={(item, index) => item._id}
          data={this.dataSource}
          renderItem={({ item }) => (
            <PickAddressRowItem
              onPress={() => {
                AnswersSrv.logSearch(keywords)
                onPickAddress && onPickAddress(item)
                if (this.state.type === 'FromAddress') {
                  this.props.navigation.navigate('Main')
                }
              }}
              onPressInfo={() => {
                if (item.joy) {
                  this.props.navigation.navigate('SponsorDetailScreen', {
                    id: item._id,
                    backToAddressPicker: true,
                    selectedTab: 'searchPlaces',
                    keywords: keywords
                  })
                }
              }}
              onPressStar={() => passenger.onPressStar(item)}
              data={item}
            />
          )}
        />        
      </ScrollView>
    )
  }

  _JoyPlacesListComponent = () => {
    return (
      <View style={{ flex: 1, backgroundColor: 'white' }}>
        <View style={{ width: '100%', padding: 5, backgroundColor: '#f2f2f2' }}>
          <SponsorInfo
            icon={Resources.image.sponsored_location}
            label={this.props.app.strings.sponsored_location}
          />
        </View>
        <FlatList
          removeClippedSubviews={false}
          keyboardShouldPersistTaps='handled'
          style={{ flex: 1 }}
          data={this.dataSourceJoy}
          keyExtractor={(item, index) => item._id}
          renderItem={({ item }) => this._renderJoyItem(item)}
        />
      </View>
    )
  }

  _emptyState = () => {
    return (
      <DcEmptyState
        icon={Resources.image.sponsored_location}
        label={this.props.app.strings.no_sponsored_location}
        labelStyle={{ width: '100%', textAlign: 'center', paddingHorizontal: 15 }}
      />
    )
  }

  _renderJoyItem = (item) => {
    return (
      <SponsorListItem
        url={$.method.utils.getPicSource(item.avatars, useLarge = false, Resources.image.joy_placeholder)}
        title={item.name}
        desc={item.shortDesc}
        buttonDesc={item.buttonDesc}
        onPress={() => {
          $.store.passenger.setJoyLocation(item)
          if ($.store.passenger.status >= 4) {
            this.props.navigation.navigate('Main') // Skip CircleSelect - Default to first official group
          }
        }}
        infoPress={() => this.props.navigation.navigate('SponsorDetailScreen', {
          id: item._id,
          backToAddressPicker: true,
          selectedTab: 'joyPlaces',
          keywords: this.state.keywords
        })}
      />
    )
  }

  render() {
    const { TabCont, TabItem, TabText, ActiveText, UnderLineActive, UnderLine } = styles
    const { app, passenger } = this.props
    const { selectedTab, type, keywords } = this.state
    const onPressCancel = () => this.props.navigation.navigate('Main')

    let showPlaceholder = $.method.utils.isArray($.store.joy.sponsorLocationList) ? false : keywords.length === 0 && $.method.utils.isArray($.store.joy.sponsorLocationNearbyList) ? false : true

    return (
      <View style={{ flex: 1, backgroundColor: 'white' }}>
        <StatusBar animated={true} hidden={false} backgroundColor={'white'} barStyle={'dark-content'} />
        <View style={{ paddingTop: Define.system.ios.x ? 22 : 0 }}>
          <View style={{
            height: Platform.select({ ios: 64, android: 44 }),
            backgroundColor: 'white',
            justifyContent: 'flex-end'
          }}>
            <View style={{ justifyContent: 'space-between', alignItems: 'center', flexDirection: 'row', height: 44 }}>
              <View style={{ flex: 1, paddingHorizontal: 12 }}>
                <TextInput
                  {...Define.TextInputArgs}
                  defaultValue={keywords}
                  clearTextOnFocus={false}
                  returnKeyType='next'
                  onChangeText={(text) => this.searchPlaces(text)}
                  placeholder={app.strings.please_enter_keywords}
                  style={[
                    { height: 44, fontWeight: '400', fontSize: TextFont.TextSize(15), color: '#666' }
                  ]} 
                  autoFocus={true}
                />
              </View>
              <View style={{ height: 22, width: 1, backgroundColor: '#e2e2e2' }} />
              <View style={{ width: 70 }}>
                <Button onPress={onPressCancel} style={{ justifyContent: 'center', height: 44 }}>
                  <Text style={{ fontWeight: '400', fontSize: TextFont.TextSize(15), color: '#888' }}>{app.strings.cancel}</Text>
                </Button>
              </View>
            </View>
          </View>
          <View style={{ height: .8, backgroundColor: '#e2e2e2' }} />
        </View>

        {
          type === 'FromAddress' && (
            this._PlacesListComponent()
          )
        }

        {
          type === 'DestinationAddress' && (
            <View style={TabCont}>
              <TouchableOpacity style={TabItem} onPress={() => this.handleTab('searchPlaces')}>
                <Text style={selectedTab === 'searchPlaces' ? ActiveText : TabText}>PLACES</Text>
                <View style={selectedTab === 'searchPlaces' && UnderLine} />
              </TouchableOpacity>

              <TouchableOpacity style={TabItem} onPress={() => this.handleTab('joyPlaces')}>
                <Text style={selectedTab === 'joyPlaces' ? ActiveText : TabText}>JOY LOCATIONS</Text>
                <View style={selectedTab === 'joyPlaces' && UnderLine} />
              </TouchableOpacity>
            </View>
          )
        }

        {
          type === 'DestinationAddress' && selectedTab === 'searchPlaces' && (
            this._PlacesListComponent()
          )
        }

        {
          type === 'DestinationAddress' && selectedTab === 'joyPlaces' && !showPlaceholder && (
            this._JoyPlacesListComponent()
          )
        }

        {
          type === 'DestinationAddress' && selectedTab === 'joyPlaces' && showPlaceholder && (
            this._emptyState()
          )
        }

      </View>
    )
  }
}

class PickAddressRowItem extends Component {

  render() {
    const {
      onPress,
      onPressStar = () => { },
      onPressInfo = () => { },
      data
    } = this.props
    const { address, name, star, distance, joy = false, tagline = '', _id = '', hasSubplace, level } = data

    return (
      <TouchableOpacity
        onPress={onPress}
        style={{ paddingVertical: 15, flex: 1, backgroundColor: 'white', alignItems: 'flex-start', backgroundColor: level === 2 ? 'rgba(0, 0, 0, 0.02)' : 'white', borderColor: '#e1e1e1', borderBottomWidth: StyleSheet.hairlineWidth }}
      >
        {
          joy ? (
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
              <View style={{ width: 48, justifyContent: 'center', alignItems: 'center' }}>
                <Image style={{ height: 25, width: 25, resizeMode: 'contain' }} source={Resources.image.sponsored_location} />
              </View>
              <View style={{ flex: 1, alignItems: 'flex-start', paddingRight: 12 }}>
                <View style={{ alignItems: 'center', backgroundColor: DCColor.BGColor('primary-1'), borderRadius: 5, borderWidth: 0.5, borderColor: '#333', paddingHorizontal: 5, paddingVertical: 2 }}>
                  <Text numberOfLines={1} style={{ color: '#333', fontSize: TextFont.TextSize(10), fontWeight: '600' }}>{tagline}</Text>
                </View>

                <Text numberOfLines={1} style={{ color: '#333', fontSize: TextFont.TextSize(15), fontWeight: '600', marginBottom: 4 }}>{name}</Text>
                <Text numberOfLines={1} style={{ color: '#666', fontSize: TextFont.TextSize(11), fontWeight: '400' }}>{address}</Text>
                {/* <Text numberOfLines={1} style={{color: '#666', fontSize: TextFont.TextSize(11), fontWeight: '400'}}>{`${parseFloat(distance) < 0.1 ? (0.1).toFixed(2) : parseFloat(distance).toFixed(2)}km · ${address}`}</Text> */}
              </View>
              <TouchableOpacity style={{ width: 48, justifyContent: 'center', alignItems: 'center' }} onPress={onPressInfo}>
                <Image style={{ height: 25, width: 25, resizeMode: 'contain', tintColor: '#D9D9D9' }} source={Resources.image.info_icon} />
              </TouchableOpacity>
            </View>
          ) : (
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
              {level === 2 && (
                <View style={{ height: 5, width: 5, borderRadius: 5, marginLeft: 30, backgroundColor: DCColor.BGColor('primary-1') }} />
              )}
              <View style={!hasSubplace ? { flex: 1, paddingRight: 12, marginLeft: 15 } : { flex: 1, paddingRight: 12, marginLeft: 15 }}>
                <Text numberOfLines={1} style={!hasSubplace && level !== 1 ? { color: '#333', fontSize: TextFont.TextSize(13), fontWeight: '500', marginBottom: 4 } : { color: '#333', fontSize: TextFont.TextSize(15), fontWeight: '600', marginBottom: 4 }}>{name}</Text>
                {level === 1 ?
                  <Text numberOfLines={1} style={{ color: '#666', fontSize: TextFont.TextSize(11), fontWeight: '400' }}>{address}</Text>
                  : null
                }
                {/* <Text numberOfLines={1} style={{color: '#666', fontSize: TextFont.TextSize(11), fontWeight: '400'}}>{`${parseFloat(distance) < 0.1 ? (0.1).toFixed(2) : parseFloat(distance).toFixed(2)}km · ${address}`}</Text> */}
              </View>
              {!hasSubplace && (
                <TouchableOpacity style={{ width: 48, justifyContent: 'center', alignItems: 'center', }} activeOpacity={0.7} onPress={onPressStar}>
                  {
                    star ?
                      Icons.Generator.Material('star', 24, '#f3ae3d') :
                      Icons.Generator.Material('star-border', 24, '#e3e3e3')
                  }
                </TouchableOpacity>
              )}
            </View>
          )
        }
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 0.3,
    position: 'relative'
  },
  map: {
    flex: 1
  },
  TabCont: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    height: 45,
    backgroundColor: 'white',
    flexDirection: 'row'
  },
  TabItem: {
    flex: 1,
    height: 45,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
    flexDirection: 'row'
  },
  TabText: {
    fontSize: TextFont.TextSize(15),
    color: 'rgba(0, 0, 0, 0.45)',
    textAlign: 'center',
    fontWeight: '400'
  },
  ActiveText: {
    fontSize: TextFont.TextSize(15),
    textAlign: 'center',
    fontWeight: '900',
    color: 'rgba(0, 0, 0, 0.75)',
  },
  UnderLine: {
    width: '100%',
    height: 3,
    backgroundColor: DCColor.BGColor('primary-1'),
    position: 'absolute',
    bottom: 0,
    left: 0
  },
  circle: {
    width: 10,
    height: 10,
    borderRadius: 10,
    backgroundColor: 'red',
    marginLeft: 10
  },
  square: {
    // width:40,
    // height:40,
    justifyContent: 'center',
    flexDirection: 'column',
    alignItems: 'center'
  },
  textBox: {
    padding: 8,
    backgroundColor: 'white',
    borderRadius: 5,
    borderWidth: 1,
    borderColor: 'rgba(0, 0, 0, 0.45)',
    marginBottom: 3
  },
  pinText: {
    fontSize: TextFont.TextSize(12),
    fontWeight: '600',
    color: 'rgba(0, 0, 0, 0.75)',
    textAlign: 'center'
  }
})