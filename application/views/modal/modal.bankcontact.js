import React, { Component } from 'react'
import { Text, View, TouchableOpacity, Modal, StyleSheet, Image } from 'react-native'
import { inject, observer } from 'mobx-react'
import { Screen, TextFont, DCColor } from 'dacsee-utils'
import Resources from "dacsee-resources"

const { height, width } = Screen.window
const styles = StyleSheet.create({
    backdropStyle: {
        width,
        height,
        backgroundColor: 'rgba(57, 56, 67, 0.2)',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 20
    },
    mainCont: {
        backgroundColor: 'white',
        borderRadius: 10,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        width: width - 50,
        maxHeight: (height / 1.5)
    },
    imgCircle: {
        backgroundColor: 'rgba(0, 0, 0, 0.05)',
        height: 100,
        width: 100,
        borderRadius: 50,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 20
    },
    imgCont: {
        paddingTop: 50,
        height: 100,
        width: 100,
    },
    mainTitle: {
        fontWeight: '900',
        justifyContent: 'center',
        alignItems: 'center',
        fontSize: TextFont.TextSize(20),
        color: 'black',
        paddingTop: 10,
        paddingBottom: 5
    },
    subsentences: {
        fontWeight: '200',
        justifyContent: 'center',
        alignItems: 'center',
        fontSize: TextFont.TextSize(13),
        textAlign: 'center'
    },
    rowCont: {
        padding: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    confirmButton: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        height: 35,
        borderRadius: 6,
        backgroundColor: DCColor.BGColor('primary-1'),
        marginLeft: 5
    },
    textStyle: {
        fontSize: TextFont.TextSize(14),
        color: 'black',
        fontWeight: '500',
        justifyContent: 'center',
        alignItems: 'center'
    }
})

@inject('app', 'bank')
@observer
export default class BankContact extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        const { strings } = this.props.app
        const { visible, onClose } = this.props
        const { backdropStyle, mainCont, imgCircle, imgCont, mainTitle, subsentences, rowCont, confirmButton, textStyle } = styles

        return (
            <Modal
                animationType='fade'
                transparent={true}
                visible={visible}
            // onRequestClose={onClose}
            >
                <View style={backdropStyle}>
                    <View style={mainCont}>
                        <View style={{ alignItems: 'center', paddingTop: 20, width: '100%' }}>
                            <View style={imgCircle}>
                                <Image style={imgCont} source={Resources.image.attention_icon} />
                            </View>
                            <Text style={mainTitle}>{strings.modal_bank_title}</Text>
                            <Text style={subsentences}>{strings.modal_bank_caption1}</Text>
                            <Text style={subsentences}>{strings.modal_bank_caption2}</Text>
                            <View style={rowCont}>
                                <TouchableOpacity style={confirmButton} onPress={onClose}>
                                    <Text style={textStyle}>Ok</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </View>
            </Modal>
        )
    }
}