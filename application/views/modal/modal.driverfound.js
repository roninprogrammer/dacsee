import React, { Component } from 'react'
import { Text, Modal, TouchableOpacity, View, StyleSheet, Dimensions } from 'react-native'
import { inject, observer } from 'mobx-react'
import { TextFont, DCColor } from 'dacsee-utils'
import { DCUserInfoCont } from '../components/'

const { width } = Dimensions.get('window')

@inject('app', 'trip', 'passenger')
@observer
export default class DriverFoundModal extends Component {
  render() {
    const {
      mainCont,
      graphicCont,
      bodyCont,
      fTitle,
      fCaption,
      actionCont,
      dcBtn,
      fBtn,
    } = styles
    const { strings } = $.store.app
    const { activeTrip } = $.store.trip
    const { driver_info, vehicle_info, booking_status } = $.store.passenger

    return (
      <Modal
        transparent={true}
        onRequestClose={() => $.store.passenger.driverFoundModal = false}
        visible={$.store.passenger.driverFoundModal}
        animationType={'slide'}
      >
        <View style={mainCont}>
          <View style={graphicCont}>
            {activeTrip.status === 'Confirmed' ?
              <DCUserInfoCont
                avatars={activeTrip.driver_info && activeTrip.driver_info.avatars}
                fullName={activeTrip.driver_info && activeTrip.driver_info.fullName}
                manufacturer={activeTrip.vehicle_info && activeTrip.vehicle_info.manufacturer}
                model={activeTrip.vehicle_info && activeTrip.vehicle_info.model}
                registrationNo={activeTrip.vehicle_info && activeTrip.vehicle_info.registrationNo}
              />
              : null
            }
            {(booking_status === 'On_The_Way' || booking_status === 'Arrived') ?
              <DCUserInfoCont
                avatars={driver_info && driver_info.avatars}
                fullName={driver_info && driver_info.fullName}
                manufacturer={vehicle_info && vehicle_info.manufacturer}
                model={vehicle_info && vehicle_info.model}
                registrationNo={vehicle_info && vehicle_info.registrationNo}
              />
              : null
            }
          </View>
          <View style={bodyCont}>
            <Text style={fTitle}>{booking_status === 'Arrived' ? strings.driver_arrive_modal_label : strings.driver_found_modal_label}</Text>
            <Text style={fCaption}>{booking_status === 'Arrived' ? strings.driver_arrive_modal_caption : strings.driver_found_modal_caption}</Text>
            <View style={actionCont}>
              <TouchableOpacity style={dcBtn} onPress={() => {
                $.store.passenger.driverFoundModal = false
                $.store.passenger.countDriver = true
              }}>
                <Text style={fBtn}>{strings.okay.toUpperCase()}</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    )
  }
}

const styles = StyleSheet.create({
  mainCont: {
    flex: 1,
    width,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'flex-start'
  },
  graphicCont: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    width,
    backgroundColor: DCColor.BGColor('primary-1'),
    padding: 30
  },
  bodyCont: {
    width,
    backgroundColor: 'white',
    padding: 30
  },
  fTitle: {
    fontSize: TextFont.TextSize(18),
    fontWeight: '700',
    color: DCColor.BGColor('primary-2')
  },
  fCaption: {
    fontSize: TextFont.TextSize(12),
    height: TextFont.TextSize(32),
    fontWeight: '400',
    color: 'rgba(0, 0, 0, 0.45)',
    marginTop: 5,
    marginBottom: 50
  },
  actionCont: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  dcBtn: {
    flex: 1,
    height: 55,
    borderRadius: 4,
    backgroundColor: DCColor.BGColor('primary-1'),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  fBtn: {
    fontSize: TextFont.TextSize(15),
    fontWeight: '700',
    color: DCColor.BGColor('primary-2')
  },
})