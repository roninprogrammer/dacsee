import React, { Component } from 'react'
import { View, ListView, Text, Image, StatusBar, TouchableOpacity } from 'react-native'
import { inject, observer } from 'mobx-react'
import { Icons, DCColor, Avatars } from 'dacsee-utils'

const dataContrast = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1._id !== r2._id, sectionHeaderHasChanged: (s1, s2) => s1 !== s2 })

@inject('app', 'account')
@observer
export default class LoginSelectAccountScreen extends Component {

  static navigationOptions = ({ navigation }) => {

    const { strings } = $.store.app
    return {
      drawerLockMode: 'locked-closed',
      title: strings.login_select_account
    }
  }

  constructor(props) {
    super(props)
    this.loginData = props.account.loginData
    this.judgePhoneCode = props.account.judgePhoneCode
    this.emailBindAccount = props.account.emailBindAccount
    this.socialBindAccount = props.account.socialBindAccount
    this.sendVerificationCode = props.account.sendVerificationCode
  }

  async onSelect(row) {
    const { type } = this.props.navigation.state.params
    const { _id, phoneCountryCode, phoneNo } = row
    const { strings } = this.props
    switch (type) {
      case 'PHONE_LOGIN':
        this.judgePhoneCode(_id)
        break
      case 'EMAIL_BIND':
        this.emailBindAccount(_id)
        break
      case 'SOCIAL_BIND':
        this.socialBindAccount(_id)
        break
      default:
        break
    }

    this.props.navigation.goBack()

  }

  render() {
    const { type } = this.props.navigation.state.params
    const { strings } = this.props.app
    const { isMail } = this.loginData

    return (
      <View style={{ flex: 1, backgroundColor: '#fff' }}>
        <StatusBar animated={true} hidden={false} backgroundColor={DCColor.BGColor('primary-1')} barStyle={'dark-content'} />
        {(type === 'EMAIL_BIND' || type === 'SOCIAL_BIND') &&
          <View style={{ marginHorizontal: 20, marginVertical: 15, }}>
            <Text style={{ color: '#999999', fontSize: 13 }}>{strings.bind_account}</Text>
          </View >
        }
        {type === 'EMAIL_LOGIN' &&
          <View style={{ marginHorizontal: 20, marginVertical: 15, }}>
            <Text style={{ color: '#999999', fontSize: 13 }}>{strings.select_account}</Text>
          </View >
        }
        <ListView
          removeClippedSubviews={false}
          enableEmptySections={true}
          dataSource={dataContrast.cloneWithRows(this.loginData.mulitiple_account.slice())}
          renderRow={(row) => (<RowItem isMail={isMail} onPress={() => this.onSelect(row)} data={row} strings={strings} />)}
          renderSeparator={() => (
            <View style={{ backgroundColor: '#f2f2f2', height: .8 }}></View>
          )}
          style={{ backgroundColor: 'white', flex: 1 }}
        />
      </View>
    )
  }
}

class RowItem extends Component {
  render() {
    const { onPress = () => { }, isMail, strings } = this.props
    const { _id, avatars = [], fullName, phoneNo } = this.props.data

    return (
      <TouchableOpacity onPress={onPress} activeOpacity={.7} style={{ height: 68, backgroundColor: 'white', justifyContent: 'center' }}>
        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', paddingHorizontal: 15 }}>
          <View style={{ width: 68, justifyContent: 'center' }}>
            <Image style={{ width: 48, height: 48, borderRadius: 24 }} source={{ uri: Avatars.getHeaderPicUrl(avatars) }} />
          </View>
          <View style={{ flex: 1 }}>
            <Text style={{ fontSize: 16, color: '#333', fontWeight: '400', marginBottom: 4 }}>{fullName}</Text>
            {isMail && <Text style={{ fontSize: 12, color: '#666', fontWeight: '400' }}>{phoneNo ? strings.active : strings.inactive}</Text>}
          </View>
          <View style={{ width: 45, alignItems: 'flex-end' }}>
            {Icons.Generator.Material('keyboard-arrow-right', 26, '#999')}
          </View>
        </View>
      </TouchableOpacity>
    )
  }
}
