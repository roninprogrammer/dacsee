import React, { Component } from 'react'
import { Text, View, StatusBar, TouchableOpacity, TextInput, Platform, ListView } from 'react-native'
import { Icons } from 'dacsee-utils'
import { inject, observer } from 'mobx-react'


const dataContrast = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })

@inject('app')
@observer
export default class PickerCountryComponent extends Component {
  static navigationOptions = { header: null }

  constructor(props) {
    super(props)
    this.state = {
      source: dataContrast.cloneWithRows([])
    }
  }

  async componentDidMount() {
    const { strings } = this.props.app
    this.setState({
      source: dataContrast.cloneWithRows([{
        key: '2',
        name: strings.malaysia,
        code: '+60'
      }, {
        key: '1',
        name: strings.china,
        code: '+86'
      }, {
        key: '3',
        name: strings.southkorea,
        code: '+82'
      }, {
        key: '4',
        name: strings.japan,
        code: '+81'
      }, {
        key: '5',
        name: strings.indonesia,
        code: '+62'
      }, {
        key: '6',
        name: strings.singapore,
        code: '+65'
      }])
    })
  }
  componentWillUnmount() { }

  render() {
    const { onPress = () => { } } = this.props.navigation.state.params
    const { strings } = this.props.app
    const headHeight = Platform.OS === 'android' ? 54 : $.method.device.ios.x ? 88 : 64
    return (
      <View style={{ flex: 1 }}>
        {Platform.OS === 'ios' && <StatusBar animated hidden={false} backgroundColor={'white'} barStyle={'dark-content'} />}
        <View style={{ height: headHeight, backgroundColor: '#f2f2f2', borderBottomWidth: 0.8, borderColor: '#ccc', justifyContent: 'flex-end' }}>
          <View style={{ height: 54, flexDirection: 'row' }}>
            <TouchableOpacity
              activeOpacity={0.7}
              style={{ width: 54, paddingLeft: 8, justifyContent: 'center', alignItems: 'flex-start' }}
              onPress={() => this.props.navigation.goBack()}
            >
              {Icons.Generator.Material('keyboard-arrow-left', 30, '#2f2f2f')}
            </TouchableOpacity>
            <TextInput
              placeholder={strings.choose_country}
              style={{ height: 54, flex: 1 }}
            />
          </View>
        </View>
        <View style={{ flex: 1 }}>
          <ListView
            removeClippedSubviews={false}
            style={{ backgroundColor: '#f8f8f8' }}
            enableEmptySections
            dataSource={this.state.source}
            renderSeparator={() => (
              <View style={{ height: 0.8, backgroundColor: '#eee' }} />
            )}
            renderRow={(row) => (
              <TouchableOpacity activeOpacity={0.7} onPress={() => {
                onPress(row.code, row.name)
                this.props.navigation.goBack()
              }} style={{ flex: 1, height: 52, paddingHorizontal: 15, justifyContent: 'center', backgroundColor: 'white' }}>
                <Text style={{ color: '#333', fontSize: 16, fontWeight: '600' }}>{`${row.name}(${row.code})`}</Text>
              </TouchableOpacity>
            )}
          />
        </View>
      </View>
    )
  }
}
