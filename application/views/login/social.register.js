/* global store */
import React, { Component } from 'react'
import { Text, View, Image, TextInput, ScrollView } from 'react-native'
import { DCColor } from 'dacsee-utils'
import { Button } from '../../components'
import { CountDownButton } from './components'
import { inject, observer } from 'mobx-react'

@inject('app', 'account')
@observer
export default class SocialRegisterScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const { strings } = $.store.app
    return {
      drawerLockMode: 'locked-closed',
      title: strings.social_register
    }
  }

  constructor(props) {
    super(props)

  }

  // async _fetchData() {
  //   const body = {
  //     fullName: this.state.fullName,
  //     phoneCountryCode: this.state.phoneCountryCode,
  //     phoneNo: this.state.phoneNo,
  //     phoneVerificationCode: this.state.phoneVerificationCode,
  //     oAuth: {
  //       provider: this.props.navigation.state.params.provider,
  //       id: this.state.userInfo.uid,
  //       accessToken: this.state.userInfo.accessToken
  //     },
  //     referralUserId: this.state.referralUserId
  //   }
  //   try {
  //     this.props.dispatch(application.showProgress())
  //     const data = await Session.User.Post('v1/register', body)
  //     this.props.dispatch(account.saveLogin(data))
  //     this.props.dispatch(application.hideProgress())
  //     this.props.dispatch(application.updatePushToken())
  //   } catch (e) {
  //     this.props.dispatch(application.hideProgress())
  //     if (e.response && e.response.data.code === 'MULTIPLE_USER_ACCOUNT') {

  //       this.props.navigation.navigate({ routeName: 'LoginSelectAccount', params: { data: e.response.data.data, value: body, type: 'BIND_PHONE' } })
  //     }
  //   } finally {
  //     this.setState({
  //       registering: false
  //     })
  //   }
  // }
  // sendCode = async () => {
  //   const { phoneCountryCode, phoneNo } = this.state
  //   let body = { phoneCountryCode: phoneCountryCode, phoneNo: phoneNo }
  //   try {
  //     const data = await Session.User.Post('v1/sendVerificationCode/phone', body)
  //     this.props.dispatch(application.showMessage(this.props.strings.alert_sent_code))
  //     return data
  //   } catch (e) {
  //     this.props.dispatch(application.showMessage(this.props.strings.unable_connect_server_pls_retry_later))
  //     return false
  //   }
  // }
  render() {
    const { strings } = $.store.app
    const { account } = this.props
    const { loginData } = this.props.account
    const { fullName, phoneCountryCode, socialUsetInfo, phoneNo, phoneVerificationCode, loginLoading, referralUserId } = loginData
    const { iconurl, name, uid } = socialUsetInfo
    console.log()
    return (
      <ScrollView style={{ flex: 1, backgroundColor: 'white' }} horizontal={false} keyboardDismissMode={'interactive'} >
        <View style={{ paddingTop: 20, alignItems: 'center' }}>
          <Image source={{ uri: iconurl }} style={{ width: 50, height: 50, borderRadius: 25 }} />
        </View>
        <View style={{ padding: 20 }}>
          <View style={{ borderBottomWidth: 1, borderBottomColor: '#a5a5a5' }}>
            <Text style={{ fontSize: 12, color: '#666' }}>{strings.name}</Text>
            <View style={{ flexDirection: 'row', alignItems: 'center', height: 38, marginTop: 6 }}>
              {/* <Text style={{ fontSize: 14 }}>RM</Text> */}
              <TextInput style={{ flex: 1, fontSize: 14 }}
                placeholder={strings.enter_name}
                underlineColorAndroid="transparent"
                onChangeText={(value) => loginData.fullName = value}
                value={fullName} />
            </View>
          </View>

          <View style={{ paddingTop: 20, borderBottomWidth: 1, borderBottomColor: '#a5a5a5' }}>
            <Text style={{ fontSize: 12, color: '#666' }}>{strings.phone}</Text>
            <View style={{ flex: 1, flexDirection: 'row', marginTop: 12, height: 38, }}>
              <Button style={{ marginRight: 10, height: 38, width: 60, justifyContent: 'center' }}
                onPress={() => this.props.navigation.navigate('PickerCountry',
                  {
                    onPress: (code) => {
                      loginData.phoneCountryCode = code
                      account.clearTimeInterval()
                    }
                  }
                )} >
                <Text style={{}}>{phoneCountryCode}</Text>
              </Button>
              <TextInput style={{ flex: 1, fontSize: 14, justifyContent: 'center' }}
                underlineColorAndroid="transparent"
                placeholder={strings.enter_phone}
                returnKeyType={'done'} keyboardType={'numeric'}
                onChangeText={(value) => loginData.phoneNo = value} />
            </View>
          </View>

          <View style={{ paddingTop: 20, justifyContent: 'flex-end' }}>
            <Text style={{ fontSize: 12, color: '#666' }}>
              {strings.verification_code}
            </Text>
            <View style={{ flex: 1, flexDirection: 'row', height: 38, }}>
              <TextInput style={{ flex: 1, fontSize: 14, height: 38, borderBottomWidth: 1, borderBottomColor: '#a5a5a5' }} underlineColorAndroid="transparent" placeholder={strings.enter_code} keyboardType={'numeric'}
                onChangeText={(value) => loginData.phoneVerificationCode = value} />
              <CountDownButton style={{ flex: 1, backgroundColor: DCColor.BGColor('primary-1'), borderRadius: 6, marginLeft: 10 }} />
            </View>
          </View>

          <View style={{ paddingTop: 20, borderBottomWidth: 1, borderBottomColor: '#a5a5a5', }}>
            <Text style={{ fontSize: 12, color: '#666' }}>{strings.refer_account}</Text>
            <TextInput style={{ fontSize: 14, height: 38, marginTop: 6 }} placeholder={strings.enter_referee_id} underlineColorAndroid="transparent" returnKeyType={'done'} onChangeText={(value) => this.setState({ referralUserId: value, resetCount: true })} />
          </View>

          <View style={{ paddingTop: 30, flex: 1, flexDirection: 'row', justifyContent: 'center' }}>
            <Button disabled={loginLoading} onPress={() => {
              // this.state.referralUserId == ''
              if (fullName == '' |
                phoneNo == '' |
                phoneVerificationCode == '') {
                $.define.func.showMessage(strings.pls_finish_info)
              } else {
                account.registerSocialUser()
              }
            }} style={[{ width: 240, height: 44, borderRadius: 4 }, loginLoading ? { backgroundColor: '#a5a5a5' } : { backgroundColor: DCColor.BGColor('primary-1') }]}>
              {
                loginLoading ?
                  <Text style={{ fontSize: 20, color: 'white' }}>{`${strings.registering}...`}</Text> :
                  <Text style={{ fontSize: 20, color: 'white' }}>{strings.register}</Text>
              }
            </Button>
          </View>
        </View>
      </ScrollView>
    )
  }
}
