import React, { Component } from "react";
import CodePush from "react-native-code-push";
import {
  Text,
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  Modal,
  StatusBar,
  Dimensions,
  KeyboardAvoidingView,
  ScrollView,
  Platform
} from "react-native";
import { inject, observer } from "mobx-react";
import { observable } from "mobx";
import { Icons, TextFont, DCColor, System } from "dacsee-utils";
import { LoginButton, CountDownButton, LoginInput } from "./components";
import resources from "dacsee-resources";



const { width, height } = Dimensions.get("window");

@inject("app", "account")
@observer
export default class LoginScreen extends Component {
  @observable referral_editable = true;
  static navigationOptions = { header: null };

  constructor(props) {
    super(props);

    this.loginData = props.account.loginData;
    this.loginWithPhone = props.account.loginWithPhone;
    this.judgePhoneCode = props.account.judgePhoneCode;
    this.registerPhoneUser = props.account.registerPhoneUser;
    this.clearTimeInterval = props.account.clearTimeInterval;
    this.referral_editable = !(this.loginData.referralUserId.length > 0);

    this.state = {
      v1: "",
      v2: "",
      v3: "",
      v4: "",
      rnVersion: "",
      country: "Malaysia"
    };
    this.codeInput = {};
  }

 

  async componentDidMount() {
    const runningBundle = await CodePush.getUpdateMetadata(
      CodePush.UpdateState.RUNNING
    );

    this.setState({
      rnVersion: runningBundle ? runningBundle.label : "0"
    });

  }

  componentWillUnmount() {

  }

  isPhone(val, code) {
    let reg;
    switch (code) {
      case "+86":
        reg = /^1[345789]\d{9}$/;
        break;
      case "+60":
        reg = /^\d{6,11}$/;
        break;
      case "+82":
        reg = /^\d{6,11}$/;
        break;
      case "+81":
        reg = /^\d{6,11}$/;
        break;
      case "+62":
        reg = /^\d{6,11}$/;
        break;
      default:
        break;
    }
    return reg.test(val);
  }

  phoneHandle(stage) {
    const { phoneNo, phoneCountryCode } = this.loginData;
    const { showMessage } = $.define.func;
    const { strings } = this.props.app;

    switch (stage) {
      case 1:
        if (this.isPhone(phoneNo, phoneCountryCode)) {
          this.loginData.TNCVisible = true;
        } else {
          showMessage(strings.pls_enter_correct_num);
        }
        break;
      case 2:
        this.judgePhoneCode();
        break;
      case 3:
        this.registerPhoneUser();
        break;
      default:
        break;
    }
  }

  render() {
    const { rnVersion } = this.state;
    const { strings } = this.props.app;
    const {
      stage,
      phoneCountryCode,
      phoneNo,
      TNCVisible,
      referralUserId,
      phoneVerificationCode
    } = this.loginData;
    const {
      mainCont,
      logoCont,
      appLogo,
      copiesCont,
      fDisplay,
      versionCont,
      fCaption,
      fSubCaption,
      loginActionCont,
      dcRow,
      dcInput,
      fBody,
      footCont,
      backBtn,
      dcRightIcon,
      loginCont,
      loginContIcon,
      cameraCont,
      cameraImage,
      registerCont,
      backdropCont,
      modalMainCont,
      dcCard,
      modalText,
      actionCont,
      buttonCont,
      buttonText
    } = styles;
    return (
      <View style={{ height: height, backgroundColor: "white" }}>
        <StatusBar
          animated
          hidden={false}
          backgroundColor={"white"}
          barStyle={"dark-content"}
        />
        {/* Enter Phone Number */}
        {stage === 0 && (
          <KeyboardAvoidingView
            contentContainerStyle={[
              mainCont,
              { width: "100%", minHeight: height }
            ]}
            style={[mainCont, { minHeight: height }]}
            keyboardVerticalOffset={30}
            behavior={"padding"}
            enabled
          >
            <ScrollView
              keyboardShouldPersistTaps={"handled"}
              style={{ height, width }}
            >
              <View style={logoCont}>
                <Image
                  style={appLogo}
                  source={resources.image.logo_landscape}
                />
              </View>

              <View style={copiesCont}>
                <Text style={fDisplay}>{strings.phone_line_1}</Text>
                <Text style={fDisplay}>{strings.phone_line_2}</Text>
              </View>

              <View style={loginActionCont}>
                <TouchableOpacity
                  onPress={() =>
                    this.props.navigation.navigate("PickerCountry", {
                      onPress: (code, name) => {
                        this.setState({ country: name });
                        this.loginData.phoneCountryCode = code;
                        this.clearTimeInterval();
                      }
                    })
                  }
                  activeOpacity={0.9}
                  style={[
                    dcInput,
                    { width: 300, justifyContent: "space-between" }
                  ]}
                >
                  <Text style={fBody}>{this.state.country}</Text>
                  <Image
                    style={dcRightIcon}
                    source={resources.image.right_icon}
                  />
                </TouchableOpacity>

                <View style={dcRow}>
                  <View style={[dcInput, { width: "80%" }]}>
                    <Text style={fBody}>{phoneCountryCode}</Text>
                    <LoginInput
                      placeholder="123456789"
                      onChangeText={text => {
                        this.loginData.phoneNo = text;
                      }}
                      value={phoneNo}
                      style={[fBody, { flex: 1, textAlign: "left" }]}
                      inputSet={{
                        keyboardType: "numeric",
                        autoFocus: true
                      }}
                    />
                  </View>
                  <LoginButton
                    onPress={() => {
                      this.phoneHandle(1);
                    }}
                    style={loginCont}
                  >
                    <View style={loginContIcon}>
                      {Icons.Generator.Ion(
                        "md-arrow-forward",
                        34,
                        DCColor.BGColor("primary-2")
                      )}
                    </View>
                  </LoginButton>
                </View>
              </View>
            </ScrollView>
            <View
              style={[
                versionCont,
                { position: "absolute", flex: 0.1, bottom: -5 }
              ]}
            >
              <Text style={fCaption}>
                {" "}
                {`dacsee v${System.Version} - ${rnVersion.replace("v", "")}`}
              </Text>
            </View>
          </KeyboardAvoidingView>
        )}

        {/* Enter Verification Code */}
        {stage === 1 && (
          <ScrollView keyboardShouldPersistTaps={"handled"}>
            <KeyboardAvoidingView
              contentContainerStyle={[
                mainCont,
                { width: "100%", minHeight: height }
              ]}
              style={[mainCont, { minHeight: height }]}
              enabled
            >
              <View
                style={[
                  logoCont,
                  { paddingVertical: 20, paddingHorizontal: 20 }
                ]}
              >
                <TouchableOpacity
                  activeOpacity={0.7}
                  style={backBtn}
                  onPress={() => {
                    this.loginData.stage = 0;
                    this.loginData.phoneVerificationCode = "";
                  }}
                >
                  {Icons.Generator.Material(
                    "keyboard-arrow-left",
                    38,
                    DCColor.BGColor("primary-2")
                  )}
                </TouchableOpacity>
              </View>

              <View style={copiesCont}>
                <Text style={[fDisplay, { marginBottom: 20 }]}>
                  {strings.verification}
                </Text>
                <Text style={fBody}>{strings.already_send_code}</Text>
                <Text style={[fBody, { fontWeight: "800" }]}>
                  {this.loginData.phoneCountryCode}
                  {this.loginData.phoneNo}
                </Text>
              </View>

              <View style={loginActionCont}>
                <LoginInput
                  maxLength={4}
                  placeholder={strings.enter_verification_code}
                  onChangeText={code => {
                    this.loginData.phoneVerificationCode = code;
                    if (this.loginData.phoneVerificationCode.length === 4) {
                      this.phoneHandle(2);
                    }
                  }}
                  value={phoneVerificationCode}
                  style={[dcInput, { flex: 1 }]}
                  inputSet={{
                    keyboardType: "numeric",
                    autoFocus: true
                  }}
                />
              </View>

              <View style={[copiesCont, { paddingVertical: 40 }]}>
                <Text style={fBody}>{strings.dint_received_code}</Text>
                <CountDownButton
                  sendCode={() => {
                    return true;
                  }}
                  style={{ flex: 1 }}
                />
              </View>
            </KeyboardAvoidingView>
          </ScrollView>
        )}

        {/* Registration */}
        {stage === 2 && (
          <KeyboardAvoidingView
            contentContainerStyle={[
              mainCont,
              { width: "100%", minHeight: height }
            ]}
            style={[mainCont, { minHeight: height }]}
            keyboardVerticalOffset={30}
            behavior={"padding"}
            enabled
          >
            <ScrollView
              keyboardShouldPersistTaps={"handled"}
              style={{ height, width }}
            >
              <View
                style={[
                  logoCont,
                  {
                    paddingVertical: 10,
                    paddingHorizontal: 20,
                    height: 30,
                    paddingTop: 60
                  }
                ]}
              >
                <TouchableOpacity
                  activeOpacity={0.7}
                  style={backBtn}
                  onPress={() => {
                    this.loginData.stage = 0;
                  }}
                >
                  {Icons.Generator.Material(
                    "keyboard-arrow-left",
                    38,
                    DCColor.BGColor("primary-2")
                  )}
                </TouchableOpacity>
              </View>

              <View style={{ paddingHorizontal: 40 }}>
                <Image
                  style={appLogo}
                  source={resources.image.logo_landscape}
                />
              </View>

              <View style={copiesCont}>
                <Text style={[fDisplay, { marginBottom: 10 }]}>
                  {strings.registration}
                </Text>
                <Text style={fBody}>{strings.registration_write_up}</Text>
              </View>

              <View style={loginActionCont}>
                <LoginInput
                  userStyles={styles.userStyles}
                  placeholder={strings.enter_name_login}
                  onChangeText={text => {
                    this.loginData.fullName = text;
                  }}
                  style={[dcInput, { marginTop: 10 }]}
                  inputSet={{
                    keyboardType: "default",
                    autoFocus: true
                  }}
                  autoCapitalize="words"
                  value={this.loginData.fullName}
                />

                <View style={dcRow}>
                  <View style={[dcInput, { flex: 1 }]}>
                    <LoginInput
                      userStyles={styles.userStyles}
                      value={referralUserId}
                      editable={this.referral_editable}
                      placeholder={`${strings.enter_referee_id} (${
                        strings.optional
                      })`}
                      onChangeText={text => {
                        this.loginData.referralUserId = text;
                      }}
                      style={[fBody, { flex: 1 }]}
                      inputSet={{
                        keyboardType: "default"
                      }}
                    />
                  </View>

                  <LoginButton
                    onPress={() =>
                      this.props.navigation.navigate("ReferrerScanQRCode")
                    }
                    style={cameraCont}
                  >
                    <View style={cameraImage}>
                      {Icons.Generator.Awesome(
                        "camera",
                        20,
                        DCColor.BGColor("primary-2")
                      )}
                    </View>
                  </LoginButton>
                </View>
                {/* <View
                  style={{
                    marginVertical: 20,
                    justifyContent: "flex-start",
                    alignItems: "flex-start"
                  }}
                >
                  <TouchableOpacity
                    activeOpacity={0.7}
                    onPress={() =>
                      this.props.navigation.navigate("SettingWetView", {
                        title: strings.terms,
                        source: {
                          uri: "https://dacsee.com/#/apps-terms-conditions"
                        }
                      })
                    }
                    style={{ marginVertical: 15 }}
                  >
                    <Text
                      style={[
                        fBody,
                        {
                          color: DCColor.BGColor("primary-1"),
                          textDecorationLine: "underline"
                        }
                      ]}
                    >
                      {strings.user_protocol}
                    </Text>
                  </TouchableOpacity>
                </View> */}
              </View>

              <View style={[footCont, { marginTop: 20 }]}>
              <Text>
                <Text style={[fCaption, { textAlign: "left" }]}>
                  {strings.active_account_tip}
                </Text>
                {" "}
               
                <Text 
                onPress={() => this.props.navigation.navigate('SettingWetView', {
                  title: strings.terms,
                  source: { uri: 'https://dacsee.com/#/apps-terms-conditions' }
                })}
                style={[fSubCaption]}>
                   {strings.user_protocol}
                </Text>
              </Text>
                <LoginButton
                  onPress={() => this.phoneHandle(3)}
                  style={registerCont}
                >
                  <Text
                    style={{ color: "#000", fontSize: 16, fontWeight: "bold" }}
                  >
                    {strings.register.toUpperCase()}
                  </Text>
                </LoginButton>
              </View>
            </ScrollView>
          </KeyboardAvoidingView>
        )}

        <Modal
          transparent
          onRequestClose={() => {}}
          visible={TNCVisible}
          animationType={"fade"}
        >
          <View style={backdropCont}>
            <View style={modalMainCont}>
              <View style={dcCard}>
                <Text
                  style={[
                    modalText,
                    { color: "#000", fontSize: TextFont.TextSize(18) }
                  ]}
                >
                  {strings.terms}
                </Text>
                <Text
                  style={[
                    modalText,
                    {
                      color: "#8C8C8C",
                      fontSize: TextFont.TextSize(14),
                      marginTop: 20
                    }
                  ]}
                >
                  {strings.aleady_terms}
                </Text>
                <Text
                  onPress={() => {
                    this.loginData.TNCVisible = false;
                    this.props.navigation.navigate("SettingWetView", {
                      title: strings.terms,
                      callback: () => {
                        this.loginData.TNCVisible = true;
                      },
                      source: {
                        uri: "https://dacsee.com/#/apps-terms-conditions"
                      }
                    });
                  }}
                  style={[
                    modalText,
                    {
                      color: "#FFDA9C",
                      fontSize: TextFont.TextSize(14),
                      marginTop: 10
                    }
                  ]}
                >
                  {strings.read_terms}
                </Text>
              </View>
              <View style={actionCont}>
                <TouchableOpacity
                  onPress={() => {
                    this.loginData.TNCVisible = false;
                  }}
                  activeOpacity={0.7}
                  style={[
                    buttonCont,
                    { backgroundColor: "#EFEFEF", borderBottomLeftRadius: 12 }
                  ]}
                >
                  <Text style={[buttonText, { color: "rgba(0, 0, 0, 0.5)" }]}>
                    {strings.cancel.toUpperCase()}
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => {
                    this.loginData.TNCVisible = false;
                    this.loginData.stage = 1;
                    this.loginWithPhone();
                    $.store.app.hideLoading();
                  }}
                  activeOpacity={0.7}
                  style={[
                    buttonCont,
                    {
                      backgroundColor: DCColor.BGColor("primary-1"),
                      borderBottomRightRadius: 12
                    }
                  ]}
                >
                  <Text style={[buttonText, { color: "#000" }]}>
                    {strings.confirm.toUpperCase()}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainCont: {
    flex: 1,
    flexDirection: "column",
    alignItems: "flex-start",
    justifyContent: "flex-start"
  },
  logoCont: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    width: "100%",
    paddingHorizontal: 40,
    paddingVertical: height * 0.05
  },
  backBtn: {
    width: 50,
    height: 50,
    marginRight: 5,
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center"
  },
  appLogo: {
    resizeMode: "contain",
    height: 80,
    width: 150
  },
  copiesCont: {
    width: "100%",
    paddingHorizontal: 40,
    paddingBottom: 40
  },
  fDisplay: {
    fontSize: TextFont.TextSize(30),
    color: DCColor.BGColor("primary-2"),
    fontWeight: "600"
  },
  versionCont: {
    flexDirection: "row",
    alignItems: "flex-end",
    justifyContent: "flex-end",
    flex: 1,
    padding: 35
  },
  fCaption: {
    width: "100%",
    fontSize: TextFont.TextSize(10),
    color: DCColor.BGColor("primary-2"),
    fontWeight: "400",
    textAlign: "center",
    opacity: 0.75
  },
  fSubCaption: {
    width: "100%",
    fontSize: TextFont.TextSize(10),
    color: DCColor.BGColor("primary-1"),
    fontWeight: "400",
    opacity: 0.75,
    textDecorationLine: 'underline' 
  },
  loginActionCont: {
    width: "100%",
    paddingHorizontal: 40
  },
  dcRow: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-start"
  },
  dcInput: {
    backgroundColor: "rgba(0, 0, 0, 0.1)",
    borderRadius: 22,
    height: 44,
    paddingHorizontal: 15,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-start"
  },
  fBody: {
    fontSize: TextFont.TextSize(15),
    color: DCColor.BGColor("primary-2"),
    fontWeight: "400",
    textAlign: "left"
  },
  footCont: {
    paddingHorizontal: 40,
    paddingBottom: 10,
    width: "100%",
    marginBottom: 20
  },
  userStyles: {
    height: 19,
    width: 16,
    position: "absolute",
    left: 20
  },
  dcCol: {
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center"
  },
  dcRightIcon: {
    width: 8,
    height: 8,
    resizeMode: "contain"
  },
  loginCont: {
    marginVertical: 5,
    height: 45,
    borderRadius: 25,
    justifyContent: "center",
    marginBottom: height * 0.01
  },
  loginContIcon: {
    width: 35,
    height: 35,
    margin: 5,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 25
  },
  cameraCont: {
    marginVertical: 5,
    height: 45,
    borderRadius: 25,
    justifyContent: "center",
    marginBottom: height * 0.01
  },
  cameraImage: {
    width: 35,
    height: 35,
    margin: 5,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 25
  },
  registerCont: {
    marginVertical: 15,
    height: 50,
    borderRadius: 25,
    justifyContent: "center"
  },
  backdropCont: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0, 0, 0, 0.65)"
  },
  modalMainCont: {
    borderRadius: 12,
    backgroundColor: "#fff"
  },
  dcCard: {
    paddingVertical: 25,
    paddingHorizontal: 15,
    width: width - 40,
    alignItems: "center"
  },
  modalText: {
    textAlign: "center",
    fontWeight: "600"
  },
  actionCont: {
    flexDirection: "row",
    alignItems: "center",
    width: width - 40,
    justifyContent: "space-between"
  },
  buttonCont: {
    flex: 1,
    paddingVertical: 20,
    justifyContent: "center",
    alignItems: "center"
  },
  buttonText: {
    fontSize: TextFont.TextSize(15),
    fontWeight: "600"
  }
});
