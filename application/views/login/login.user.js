import React, { Component } from 'react'
import {
  Dimensions,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity
} from 'react-native'
import resources from 'dacsee-resources'
import { inject, observer } from 'mobx-react'
import Icon from 'react-native-vector-icons/Ionicons'
import CountDown from './components/button.count.down'
const { width, height } = Dimensions.get('window')
@inject('app')
@observer
export default class LoginActivity extends Component {
  static navigationOptions = { header: null }
  constructor(props) {
    super(props)
  }
  render() {
    const { strings } = this.props.app
    let { account, setValue } = this.props.login
    return (
      <View style={{ flex: 1, backgroundColor: $.define.color.white }}>
        <View style={{ height: height * .3, alignItems: 'center', justifyContent: 'center' }}>
          <Image style={{ height: 68, width: 68 }} source={resources.image.logo} />
        </View>
        <View style={{ marginHorizontal: 40, width: width - 80 }} >
          <EditView name={strings.phone_or_email} icon='md-person' onChangeText={(val) => {
            setValue({ phoneNo: val, phoneCountryCode: '+86' })
          }} />
          <CountDown />
          <EditView name={'输入密码'} icon='md-unlock' onChangeText={(val) => account.password = val} />
          <LoginButton title={strings.login} onPress={()=>this.props.navigation.navigate('LoginSelectAccount')}/>
        </View>
        <View style={{ marginTop: 20 }}>
          <TouchableOpacity onPress={() => console.log('忘记密码')}>
            <Text style={{ color: '#aeadb9', textAlign: 'center', marginTop: 10 }}>{'忘记密码'}</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => console.log('注册')}>
            <Text style={{ color: '#0fdeae', textAlign: 'center', marginTop: 10 }} >{strings.register}</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}
const EditView = (props) => {
  const { name, icon, onChangeText } = props
  return (
    <View style={styles.edit_contain}>
      <Icon name={icon} color='#d3d2dc' size={16} style={{ marginHorizontal: 10 }} />
      <TextInput placeholder={name} underlineColorAndroid={'transparent'} 
        onChangeText={onChangeText} 
        style={{ width:width-120,height:44,paddingHorizontal:6,paddingVertical:4 }} />
    </View>
  )
}
const LoginButton = (props) => {
  const { title ,onPress} = props
  return (
    <TouchableOpacity style={[styles.button_contain,styles.shadow]} onPress={onPress}>
      <Text style={{ color: $.define.color.white, fontSize: $.define.fontSize.middle }}>{title}</Text>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  button_contain: {
    backgroundColor: '#45e2be', borderRadius: 10,
    alignItems: 'center', justifyContent: 'center',
    marginVertical: 20, height: 50, width: width - 80
  },
  edit_contain: {
    backgroundColor: '#f6f5fb', borderRadius: 10,
    flexDirection: 'row', alignItems: 'center', marginBottom: 10
  },
  shadow: {
    elevation:4,
    shadowOffset: { width: 0, height: 0 },
    shadowColor: '#dfdfdf',
    shadowOpacity: 1,
    shadowRadius: 3
  },
})