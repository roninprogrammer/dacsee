import React, { Component } from 'react'
import { TouchableOpacity, Text, StyleSheet } from 'react-native'
import { DCColor, TextFont } from 'dacsee-utils'

export default class SocialComponent extends Component {
  render() {
    const { title, onPress, style } = this.props
    const {
      mainCont,
      fBtn
    } = styles
    return (
      <TouchableOpacity onPress={onPress}
        style={[mainCont, style]}>
        {this.props.children}
        {title && <Text style={fBtn}>{title}</Text>}
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  mainCont: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: DCColor.BGColor('primary-1'),
    marginLeft: 10
  },
  fBtn: {
    color: DCColor.BGColor('primary-2'),
    fontSize: TextFont.TextSize(15),
    fontWeight: '700',
  }
})