import React, { Component } from 'react'
import { Image, TouchableOpacity, Text, View, Dimensions } from 'react-native'
import { SvgIcon, iconPath } from '../../chat/chatIcon'
const { width, height } = Dimensions.get('window')

export default class SocialComponent extends Component {
  render() {
    const {title ,onPress}=this.props
    return (
      <View style={{  alignItems: 'center' }}>
        {/* <Text style={{ color: '#fff',marginBottom:15}}>{this.props.title}</Text>
        <View style={{  flexDirection: 'row',width:width-70, alignItems: 'center', justifyContent: 'center' }}>
          <SocialIcon  backgroundColor={'#3b5998'} color={['#fff']} path={iconPath.facebook} 
            onPress={()=>onPress(7,'facebook')} />
          <SocialIcon backgroundColor={'#dc4e41'} color={['#fff']} path={iconPath.gplus} 
            onPress={()=>onPress(10,'google')} />
          <SocialIcon backgroundColor={'#fff'} color={['#fff', '#fff', '#E79014', '#E79014', '#D62629', '#333']} path={iconPath.weibo} 
            onPress={()=>onPress(1,'weibo')} />
          <SocialIcon backgroundColor={'#fff'} color={['#4caf50']} path={iconPath.wechat}
            onPress={()=>onPress(2,'wechat')} />
        </View> */}
      </View>
    )
  }
}

const SocialIcon = (props) => {
  const { backgroundColor, path, color, onPress } = props
  return (
    <TouchableOpacity onPress={onPress}
      style={{
        width: 50, height: 50, justifyContent: 'center',marginHorizontal:(width-270)/8,
        alignItems: 'center', backgroundColor: backgroundColor, borderRadius: 25, 
      }}>
      <SvgIcon size={30} fill={color} path={path} />
    </TouchableOpacity>
  )
}
