import React, { Component } from 'react'
import { Image, Text, View, Dimensions } from 'react-native'
const { width, height } = Dimensions.get('window')
import resources from 'dacsee-resources'
export default class LogoComponent extends Component {
  render() {
    return (
      <View style={{ height: height * 0.45, alignItems: 'center', justifyContent: 'center' }}>
        <Image style={{ width: height* 0.25, height: height* 0.2 }} source={resources.image.login_logo} />
        <Text style={{ fontSize: 40, color: '#fff', fontWeight: 'bold', marginTop: 10 }}>{'DACSEE'}</Text>
      </View>
    )
  }
}
