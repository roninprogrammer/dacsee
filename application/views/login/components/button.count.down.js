import React, { Component } from 'react'
import {TextInput, Text, StyleSheet, TouchableOpacity, View, Dimensions} from 'react-native'
import { inject, observer } from 'mobx-react'
import Icon from 'react-native-vector-icons/Ionicons'
const { width, height } = Dimensions.get('window')
@inject('app','login')
@observer
export default class CountDownButton extends Component {
  constructor(props) {
    super(props)
    this.state = {
      time: 30,
      started: false
    }
  }
  componentDidMount() {
    if (!this.props.stop) {
      this.startInterval()
    }
  }
  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.stop) {
      this.resetInterval()
    }
  }
  resetInterval() {
    this.timer && clearInterval(this.timer)
    this.setState({ started: false, time: 30 })
  }
  startInterval() {
    this.setState({ started: true })
    this.timer = setInterval(() => {
      if (this.state.time === 0) {
        this.timer && clearInterval(this.timer)
        this.setState({ started: false, time: 30 })
      }
      this.setState({ time: this.state.time - 1 })
    }, 1000)
  }
  startCountDown = async () => {
    if (!this.state.started) {
      let status = await this.props.sendCode()
      if (status) {
        this.startInterval()
      }
    }
  }
  componentWillUnMount() {
    this.timer && clearInterval(this.timer)
  }
  // render() {
  //   const { show, style, i18n } = this.props
  //   const { time, started } = this.state
  //   return (
  //     <TouchableOpacity activeOpacity={started?1:.9} onPress={this.startCountDown} style={[styles.buttonWrap, style]} >
  //       <Text style={{ textAlign: 'center', fontSize: 15, fontWeight: '600', color: 'white'}}>{started ? `${this.state.time} ${i18n.seconds_later}` : i18n.send_code}</Text>
  //     </TouchableOpacity>
  //   )
  // }
  render() {
    const {strings}=this.props.app
    const {sendVerificationCode =()=>{},account} =this.props.login
    return (
      <View style={{ backgroundColor: '#f6f5fb', borderRadius: 10, flexDirection: 'row', alignItems: 'center', marginBottom: 10 }}>
        <Icon name={'md-chatbubbles'} color='#d3d2dc' size={16} style={{ marginHorizontal: 10 }} />
        <TextInput placeholder={strings.enter_code} underlineColorAndroid={'transparent'} onChangeText={(val)=>this.setState({code:val})} style={{ width: (width-120)/1.6,height:44,paddingHorizontal:6,paddingVertical:4  }} />
        <TouchableOpacity onPress={()=>sendVerificationCode()} style={{borderLeftWidth:1,alignItems:'center',justifyContent:'center',borderColor:$.define.color.white ,width: (width-120)*.375,height:40, }}>
          <Text style={{marginLeft:5}}>{strings.send_code}</Text>
        </TouchableOpacity>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  buttonWrap: {
    alignItems: 'center',
    justifyContent: 'center'
  }
})