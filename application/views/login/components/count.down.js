import React, { Component } from 'react'
import { Text, StyleSheet, TouchableOpacity } from 'react-native'
import { inject, observer } from 'mobx-react'
import { Icons, TextFont, DCColor, System } from 'dacsee-utils'
import moment from 'moment'

@inject('app', 'account')
@observer
export default class CountDownButton extends Component {
  constructor(props) {
    super(props)
    this.loginData = props.account.loginData
    this.clearTimeInterval = props.account.clearTimeInterval
    this.sendVerificationCode = props.account.sendVerificationCode
    
  }

  componentWillUnmount() {
    this.clearTimeInterval()
  }

  startCountDown(){
    const { counttime, sendCodeLoading } = this.loginData
    if ( sendCodeLoading || counttime !== 0) return
    this.sendVerificationCode()
  }
  render() {
    const { show, style } = this.props
    const { strings } = this.props.app
    const { counttime } = this.loginData
    
    return (
      <TouchableOpacity activeOpacity={counttime > 0 ? 1 : .9} onPress={() =>this.startCountDown() }
        style={[styles.buttonWrap, style]} >
        <Text style={counttime === 0 ? styles.countText : styles.blackText}>
          {counttime === 0 ? strings.resend_code.toUpperCase() : `${strings.request_code} ${moment.utc(counttime * 1000).format("HH:mm:ss")} `}
        </Text>
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  buttonWrap: {
    alignItems: 'flex-start',
    justifyContent: 'center'
  },
  countText: {
    fontSize: TextFont.TextSize(15),
    fontWeight:'800',
    textDecorationLine: 'underline', 
    color: DCColor.BGColor('primary-1'),
    textAlign:'left'
  },
  blackText: {
    fontSize: TextFont.TextSize(15),
    fontWeight:'800',
    color: DCColor.BGColor('primary-2'),
    textAlign:'left'
  }
})