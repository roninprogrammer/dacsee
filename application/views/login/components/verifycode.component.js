import React, { Component } from 'react'
import { TextInput, StyleSheet, View, Dimensions } from 'react-native'
import { DCColor } from 'dacsee-utils'

const { width } = Dimensions.get('window')
const codeInputProps = {
  maxLength: 1,
  returnKeyType: 'next',
  placeholderTextColor: '#eee',
  keyboardType: 'numeric',
  clearTextOnFocus: false,
  autoCapitalize: 'none',
  selectTextOnFocus: false,
  autoCorrect: false,
  underlineColorAndroid: 'transparent',
  selectTextOnFocus: true,
  selectionColor: DCColor.BGColor('primary-1')
}
const codeInputWidth = ((width - 70) - 20 * 3) / 4

export default class VerifyCodeInput extends Component {
  constructor(props) {
    super(props)
    this.code = {
      v1: '', v2: '', v3: '', v4: ''
    }
    this.codeInput = {}
  }

  checkCode(n, value, prev, next) {
    this.code[n] = value
    let { v1, v2, v3, v4 } = this.code

    const code = v1 + v2 + v3 + v4
    
    if (value.length === 0) {
      this.codeInput[prev].focus()
    } else if (code.length === 4) {
      this.props.endInput(code)
    } else {
      this.codeInput[next].focus()
    }
  }

  render() {
    return (
      <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
        <TextInput 
          ref={e => this.codeInput.v1 = e} 
          style={styles.codeInput} 
          onChangeText={text => {
            this.checkCode('v1', text, 'v1', 'v2')
          }}
          {...codeInputProps}
          autoFocus={true}
        />

        <TextInput 
          ref={e => this.codeInput.v2 = e} 
          style={styles.codeInput}  
          onChangeText={text => {
            this.checkCode('v2', text, 'v1', 'v3')
          }}
          {...codeInputProps}
        />

        <TextInput 
          ref={e => this.codeInput.v3 = e} 
          style={styles.codeInput} 
          onChangeText={text => {
            this.checkCode('v3', text, 'v2', 'v4')
          }}
          {...codeInputProps}
        />

        <TextInput 
          ref={e => this.codeInput.v4 = e} 
          style={styles.codeInput} 
          onChangeText={text => {
            this.checkCode('v4', text, 'v3', 'v4')
          }}
          {...codeInputProps}
        />
      </View>
    )
  }
}
const styles = StyleSheet.create({
  codeInput: {
    color: DCColor.BGColor('primary-2'),
    fontWeight: '600',
    textAlign: 'center',
    fontSize: 16,
    borderRadius: 22,
    backgroundColor: 'rgba(0, 0, 0, 0.1)',
    height: 44,
    paddingHorizontal: 6,
    width: codeInputWidth,
  }
})

