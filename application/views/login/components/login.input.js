import React, { Component } from 'react'
import { TextInput, StyleSheet, Text, View, Image } from 'react-native'
import { TextFont, DCColor } from 'dacsee-utils'

export default class LoginInput extends Component {
  render() {
    const { value, placeholder, onChangeText, style, inputSet, source, autoCapitalize = 'none' } = this.props
    const { dcIcon, fBody } = styles
    return (
      <View style={style}>
        {source && (
          <Image source={source} style={dcIcon} />
        )}
        <TextInput
          {...inputSet}
          returnKeyType='next'
          autoCapitalize={autoCapitalize}
          selectTextOnFocus={false}
          autoCorrect={false}
          clearTextOnFocus={false}
          underlineColorAndroid='transparent'
          placeholderTextColor={'rgba(0, 0, 0, 0.25)'}
          placeholder={placeholder}
          onChangeText={onChangeText}        
          selectionColor={DCColor.BGColor('primary-2')}
          style={[fBody, { flex: 1, textAlign: 'center' }]}
        >
          {value && <Text style={{ backgroundColor: 'transparent' }}>{value}</Text>}
        </TextInput>
      </View>
    )
  }
}
LoginInput.defaultProps = {
  nameImg: Image.propTypes.source
}

const styles = StyleSheet.create({
  fBody: {
    fontSize: TextFont.TextSize(15),
    color: DCColor.BGColor('primary-2'),
    fontWeight: '500'
  },
  dcIcon: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
    marginRight: 15,
    tintColor: DCColor.BGColor('primary-2')
  }
})

