import React, { Component } from 'react'
import { View, Image, StatusBar, TouchableOpacity, Platform, Alert, AlertIOS } from 'react-native'
import Resources from 'dacsee-resources'
import Swiper from 'react-native-swiper';
import moment from 'moment'
import { Screen, Define, Icons, DCColor } from 'dacsee-utils'
import { inject, observer } from 'mobx-react'

const { width } = Screen.window

@inject('app', 'joy')
@observer
export default class JoyImgViewer extends Component {

  static navigationOptions = ({ navigation }) => {
    const { strings } = $.store.app
    return {
      title: strings.receipt,
      drawerLockMode: 'locked-closed',
      headerStyle: {
        backgroundColor: DCColor.BGColor('primary-1'),
        shadowColor: 'transparent',
        shadowOpacity: 0,
        borderBottomWidth: 0,
        borderBottomColor: 'transparent',
        elevation: 0,
      }
    }
  }

  constructor(props) {
    super(props)
    this.state = {
      currentImg: '',
      currentJoy: ''
    }
  }

  componentDidMount() {
    const { id } = this.props.navigation.state.params
    const { receipt } = this.props.joy.joyDetail

    this.setState({
      currentJoy: id
    })

    if (receipt && receipt.photos.length > 0) {
      this.setState({
        currentImg: receipt.photos[0]._id,
      })
    }
  }

  onImgChange(idx) {
    const { receipt } = this.props.joy.joyDetail
    if (receipt) {
      this.setState({
        currentImg: receipt.photos[idx]._id
      })
    }
  }

  onDelete() {
    const { strings } = this.props.app
    const { deleteJoyReceipt } = this.props.joy
    const { currentImg, currentJoy } = this.state

    if (Platform.OS === 'ios') {
      AlertIOS.alert(
        (strings.delete_photo), (strings.delete_photo_desc),
        [
          {
            text: strings.cancel,
            style: 'cancel'
          },
          {
            text: strings.del,
            onPress: () => deleteJoyReceipt(currentJoy, currentImg),
            style: 'destructive'
          }
        ]
      )
    } else {
      Alert.alert(
        (strings.delete_photo), (strings.delete_photo_desc),
        [
          { text: (strings.cancel) },
          { text: (strings.del), onPress: () => deleteJoyReceipt(currentJoy, currentImg) },
        ]
      )
    }
  }

  render() {
    const { receipt, status } = this.props.joy.joyDetail
    const validDate = this.props.joy.joyDetail.submissionInfo && (this.props.joy.joyDetail.submissionInfo.expiredOn || this.props.joy.joyDetail.submissionInfo.endOn)
    const today = new Date()
    const isExpired = moment(today).isAfter(validDate)

    return (
      <View style={{ flex: 1, backgroundColor: '#000' }}>
        <StatusBar animated={true} hidden={false} backgroundColor={'#151416'} barStyle={'dark-content'} />
        <View style={{ flex: 1, top: Define.system.ios.x ? -44 : -22, justifyContent: 'center', alignItems: 'center' }}>
          {receipt && receipt.photos.length > 0 && (status === 'pendingSubmission' || status === 'rejected') && !isExpired && (
            <TouchableOpacity style={{ width, position: 'absolute', bottom: 20, alignItems: 'center' }} onPress={() => this.onDelete()}>
              {Icons.Generator.Material('delete', 30, DCColor.BGColor('disable'))}
            </TouchableOpacity>
          )}
          <View style={{ width, height: width }}>
            {!receipt ?
              <Image style={{ width: width, height: width, resizeMode: 'contain' }} source={Resources.image.joy_placeholder} />
              :
              <Swiper
                horizontal={true}
                loop={false}
                dotColor='rgba(255, 255, 255, 0.45)'
                activeDotColor={DCColor.BGColor('primary-1')}
                onIndexChanged={(index) => this.onImgChange(index)}
              >
                {receipt && receipt.photos.map((i, idx) => (
                  <Image key={idx} style={{ width, height: width }} source={{ uri: i.url }} />
                ))}
              </Swiper>
            }
          </View>
        </View>
      </View>
    )
  }
}