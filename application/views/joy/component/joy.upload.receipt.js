import React, { Component } from 'react'
import { View, TouchableOpacity, Image, Alert, AlertIOS, StatusBar, ActivityIndicator, Platform } from 'react-native'
import ActionSheet from 'react-native-actionsheet'
import ImagePicker from 'react-native-image-picker'
import Swiper from 'react-native-swiper';
import { inject, observer } from 'mobx-react'
import Resources from 'dacsee-resources'
import { DCColor, Icons, Screen, Define } from 'dacsee-utils'

const { width } = Screen.window

@inject('app', 'joy')
@observer
export default class JoyUploadReceipt extends Component {

  static navigationOptions = ({ navigation }) => {
    const { strings } = $.store.app
    return {
      title: strings.upload_receipt,
      drawerLockMode: 'locked-closed',
      headerRight: (
        <TouchableOpacity
          activeOpacity={0.7}
          style={{ top: -0.5, width: 54, paddingRight: 20, justifyContent: 'center', alignItems: 'flex-end' }}
          onPress={navigation.state.params && navigation.state.params.rightPress}
        >
          {Icons.Generator.Material('more-horiz', 28, 'white', { style: { left: 8 } })}
        </TouchableOpacity>
      ),
      headerStyle: {
        backgroundColor: DCColor.BGColor('primary-1'),
        shadowColor: 'transparent',
        shadowOpacity: 0,
        borderBottomWidth: 0,
        borderBottomColor: 'transparent',
        elevation: 0,
      }
    }
  }

  constructor(props) {
    super(props)
    this.state = {
      media: {},
      uploading: false,
      currentJoy: '',
      currentImg: ''
    }
  }

  componentDidMount() {
    const { id } = this.props.navigation.state.params
    const { receipt } = this.props.joy.joyDetail

    this.setState({
      currentJoy: id
    })

    if (receipt && receipt.photos.length > 0) {
      this.setState({
        currentImg: receipt.photos[0]._id,
      })
    }


    this.props.navigation.setParams({
      rightPress: this._show,
    })
    this.ActionSheet.show()
  }

  componentWillUnmount() {
    this.subscription && this.subscription.remove()
  }

  _show = () => {
    this.ActionSheet.show()
  }

  _getOptions = () => {
    const { strings } = this.props.app
    return {
      title: strings.album,
      storageOptions: { skipBackup: true, path: 'images' },
      quality: 0.8,
      mediaType: 'photo',
      cancelButtonTitle: strings.cancel,
      takePhotoButtonTitle: strings.take_photo,
      chooseFromLibraryButtonTitle: strings.select_from_album,
      allowsEditing: true,
      noData: false,
      maxWidth: 1000,
      maxHeight: 1000,
      permissionDenied: {
        title: strings.refuse_visit, text: strings.pls_auth,
        reTryTitle: strings.retry, okTitle: strings.okay
      }
    }
  }

  _pressActionSheet(index) {
    const { receipt } = this.props.joy.joyDetail
    const { strings } = this.props.app
    if (index === 0) {
      ImagePicker.launchCamera(this._getOptions(), (response) => {
        if (response.didCancel) {
          return
        } else if (response.error) {
          let label = response.error.startsWith('Photo') ? strings.photo : strings.camera
          return Alert.alert(`${strings.pls_auth_fore} ${label} ${strings.pls_auth_back} ${label}。`)
        } else {
          this.setState({
            media: {
              id: `${(new Date).getTime()}`,
              uri: response.uri,
              feature: 'image'
            }
          })
          this.uploadImage(response.data)
        }
      })
    }

    if (index === 1) {
      ImagePicker.launchImageLibrary(this._getOptions(), (response) => {
        if (response.didCancel) {
          return
        } else if (response.error) {
          let label = response.error.startsWith('Photo') ? strings.photo : strings.camera
          return Alert.alert(`${strings.pls_auth_fore} ${label} ${strings.pls_auth_back} ${label}。`)
        } else {
          this.setState({
            media: {
              id: `${(new Date).getTime()}`,
              uri: response.uri,
              feature: 'image'
            }
          })
          this.uploadImage(response.data)
        }
      })
    }

    if (receipt && receipt.photos.length > 0 && index === 2) {
      this.onDelete()
    }
  }

  async uploadImage(base64) {
    const id = this.props.navigation.state.params.id
    this.setState({ uploading: true })
    await this.props.joy.uploadJoyReceipt(id, base64)
    this.setState({ uploading: false })
  }

  onImgChange(idx) {
    const { receipt } = this.props.joy.joyDetail
    if (receipt) {
      this.setState({
        currentImg: receipt.photos[idx]._id
      })
    }
  }

  onDelete() {
    const { strings } = this.props.app
    const { deleteJoyReceipt } = this.props.joy
    const {currentImg, currentJoy} = this.state

    if (Platform.OS === 'ios') {
      AlertIOS.alert(
        (strings.delete_photo), (strings.delete_photo_desc),
        [
          {
            text: strings.cancel,
            style: 'cancel'
          },
          {
            text: strings.del,
            onPress: () => deleteJoyReceipt(currentJoy, currentImg),
            style: 'destructive'
          }
        ]
      )
    } else {
      Alert.alert(
        (strings.delete_photo), (strings.delete_photo_desc),
        [
          { text: (strings.cancel) },
          { text: (strings.del), onPress: () => deleteJoyReceipt(currentJoy, currentImg) },
        ]
      )
    }
  }

  render() {
    const { strings } = this.props.app
    const { uploading = false } = this.state
    const { receipt } = this.props.joy.joyDetail
    return (
      <View style={{ flex: 1, backgroundColor: '#000' }}>
        <StatusBar animated={true} hidden={false} backgroundColor={'#151416'} barStyle={'dark-content'} />
        <View style={{ flex: 1, top: Define.system.ios.x ? -44 : -22, justifyContent: 'center', alignItems: 'center', position: 'relative' }}>
          { receipt && receipt.photos.length > 0 && (
            <TouchableOpacity style={{ width, position: 'absolute', bottom: 20, alignItems: 'center' }} onPress={() => this.onDelete()}>
              {Icons.Generator.Material('delete', 30, DCColor.BGColor('disable'))}
            </TouchableOpacity>
          )}
          <View style={{ width, height: width }}>
            {(receipt && receipt.photos.length > 0) ?
              <Swiper
                horizontal={true}
                loop={false}
                dotColor='rgba(255, 255, 255, 0.45)'
                activeDotColor={DCColor.BGColor('primary-1')}
                onIndexChanged={(index) => this.onImgChange(index)}
              >
                {receipt && receipt.photos.map((i, idx) => (
                  <Image key={idx} style={{ width, height: width }} source={{ uri: i.url }} />
                ))}
              </Swiper>
              : <Image style={{ width: width, height: width, resizeMode: 'contain' }} source={Resources.image.joy_placeholder} />
            }
          </View>
        </View>
        {
          uploading && (
            <View style={{ flex: 1, backgroundColor: '#00000066', justifyContent: 'center', alignItems: 'center', position: 'absolute', left: 0, right: 0, top: 0, bottom: 0 }}>
              <View style={{ top: Define.system.ios.x ? -44 : -22 }}>
                <ActivityIndicator size='small' color='#d0d0d0' />
              </View>
            </View>
          )
        }
        <ActionSheet
          ref={e => this.ActionSheet = e}
          options={receipt && receipt.photos.length > 0 ? [strings.take_photo, strings.select_from_album, strings.delete_photo, strings.cancel] : [strings.take_photo, strings.select_from_album, strings.cancel]}

          cancelButtonIndex={receipt && receipt.photos.length > 0 ? 3 : 2}
          onPress={this._pressActionSheet.bind(this)}
        />
      </View>
    )
  }
}