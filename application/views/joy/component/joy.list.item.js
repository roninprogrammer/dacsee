import React, { Component } from 'react'
import { Text, View, Image, StyleSheet, TouchableOpacity } from 'react-native'
import moment from 'moment'
import { Screen, DCColor, TextFont } from 'dacsee-utils'

const { width } = Screen.window
const styles = StyleSheet.create({
    itemCont: {
        width,
    },
    dcCol: {
        width,
        flexDirection: 'row',
        alignContent: 'center',
        justifyContent: 'center',
        flexWrap: 'nowrap'
    },
    dcImg: {
        width: width / 100 * 29,
        height: width / 100 * 29,
        resizeMode: 'cover'
    },
    dataCont: {
        flex: 1,
        padding: 10
    },
    fTitle: {
        fontSize: TextFont.TextSize(16),
        fontWeight: '800',
        color: 'rgba(0, 0, 0, 0.75)',
        textAlign: 'left'
    },
    fCaption: {
        fontSize: TextFont.TextSize(12),
        color: 'rgba(0, 0, 0, 0.5)',
        textAlign: 'left',
        fontWeight: '400',
        marginBottom: 10
    },
    actionCont: {
        flexDirection: 'row',
        alignContent: 'center',
        justifyContent: 'center',
    },
    statusCont: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginRight: 10,
    },
    fStatus: {
        marginBottom: 0
    },
    dcDot: {
        height: 5,
        width: 5,
        borderRadius: 5,
        marginRight: 5,
        backgroundColor: DCColor.BGColor('primary-1')
    },
    cExpired: {
        backgroundColor: '#ccc'
    },
    cComplete: {
        backgroundColor: DCColor.BGColor('primary-1')
    },
    cError: {
        backgroundColor: DCColor.BGColor('red')
    },
    dcBtn: {
        borderRadius: 5,
        height: 35,
        width: '45%',
        backgroundColor: DCColor.BGColor('primary-1'),
        shadowOffset: { width: 0, height: -1 },
        shadowColor: '#000',
        shadowOpacity: 0.1,
        elevation: 0.5,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    btnText: {
        fontSize: TextFont.TextSize(14),
        fontWeight: '800',
        color: 'white',
        textAlign: 'center'
    },
    underLine: {
        height: 1,
        width,
        backgroundColor: DCColor.BGColor('disable')
    }
})

export default class JoyListItem extends Component {
    constructor(props) {
        super(props)

        this.state = {
            img: '',
            title: '',
            description: '',
            validDate: null,
            status: ''
        }
    }

    componentDidMount() {
        const { img, title, description, validDate, status } = this.props
        const today = new Date()
        const isExpired = moment(today).isAfter(validDate)
        
        this.setState({
            img, 
            title, 
            description, 
            validDate, 
            status: isExpired ? 'expired' : status
        })
    }

    componentDidUpdate(prevProps) {
        const { status, validDate } = this.props
        const today = new Date()
        const isExpired = moment(today).isAfter(validDate)

        if (status !== prevProps.status || validDate !== prevProps.validDate) {

            this.setState({
                status: isExpired? 'expired' : status,
                validDate
            })
        }
    }

    render() {
        const {
            itemCont,
            dcCol,
            dcImg,
            dataCont,
            fTitle,
            fCaption,
            actionCont,
            statusCont,
            dcDot,
            fStatus,
            cExpired,
            cComplete,
            cError,
            dcBtn,
            btnText,
            underLine
        } = styles

        const { onPress, strings, to } = this.props
        const { img, title, description, validDate, status } = this.state
        return (
            <View style={itemCont}>
                <TouchableOpacity onPress={to} style={dcCol}>
                    {img !== '' && (
                        <Image style={dcImg} source={{uri: img}} />
                    )}
                    <View style={dataCont}>
                        <Text style={fTitle} numberOfLines={1} ellipsizeMode='tail'>{title}</Text>
                        <Text style={fCaption} numberOfLines={1} ellipsizeMode='tail'>{description}</Text>
                        {status === 'pendingSubmission' && (
                            <View style={actionCont}>
                                <View style={statusCont}>
                                    <Text style={[fCaption, fStatus]}>{strings.valid_until}: {moment(validDate).format('DD MMM YYYY')}</Text>
                                </View>
                                <TouchableOpacity style={dcBtn} onPress={onPress}>
                                    <Text style={btnText}>{strings.upload_now}</Text>
                                </TouchableOpacity>
                            </View>
                        )}
                        {status === 'pendingVerification' && (
                            <View style={actionCont}>
                                <View style={statusCont}>
                                    <View style={dcDot} />
                                    <Text style={[fCaption, fStatus]}>{strings.pending_verification}</Text>
                                </View>
                            </View>
                        )}
                        {status === 'completed' && (
                            <View style={actionCont}>
                                <View style={statusCont}>
                                    <View style={[dcDot, cComplete]} />
                                    <Text style={[fCaption, fStatus]}>{strings.redeemed}</Text>
                                </View>
                            </View>
                        )}
                        {status === 'disapproved' && (
                            <View style={actionCont}>
                                <View style={statusCont}>
                                    <View style={[dcDot, cError]} />
                                    <Text style={[fCaption, fStatus]}>{strings.disapproved}</Text>
                                </View>
                            </View>
                        )}
                        {status === 'rejected' && (
                            <View style={actionCont}>
                                <View style={statusCont}>
                                    <View style={[dcDot, cError]} />
                                    <Text style={[fCaption, fStatus]}>{strings.resubmit_reward}</Text>
                                </View>
                            </View>
                        )}
                        {status === 'expired' && (
                            <View style={actionCont}>
                                <View style={statusCont}>
                                    <View style={[dcDot, cExpired]} />
                                    <Text style={[fCaption, fStatus]}>{strings.expired}</Text>
                                </View>
                            </View>
                        )}
                        {status === 'new' && (
                            <View style={actionCont}>
                                <View style={statusCont}>
                                    <View style={dcDot} />
                                    <Text style={[fCaption, fStatus]}>{strings.new}</Text>
                                </View>
                            </View>
                        )}
                        {status === 'read' && (
                            <View style={actionCont}>
                                <View style={statusCont}>
                                    <View style={[dcDot, cExpired]} />
                                    <Text style={[fCaption, fStatus]}>{strings.read}</Text>
                                </View>
                            </View>
                        )}
                    </View>
                </TouchableOpacity>
                <View style={underLine}/>
            </View>
        )
    }
}