import React, { Component } from 'react'
import { Text, View, Image, StyleSheet, ScrollView, Animated, TouchableOpacity, SafeAreaView, Alert, BackHandler } from 'react-native'
import { inject, observer } from 'mobx-react'
import moment from 'moment'
import { Screen, TextFont, DCColor } from 'dacsee-utils'
import Resources from 'dacsee-resources'
import Barcode from 'react-native-barcode-builder'

const { width } = Screen.window
const styles = StyleSheet.create({
  mainCont: {
    width,
    height: '100%',
    flexDirection: 'column',
    flexWrap: 'nowrap',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    backgroundColor: 'white'
  },
  scrollCont: {
    flex: 1,
    width
  },
  imgCont: {
    width,
    height: 250,
    resizeMode: 'cover',

  },
  underLine: {
    width,
    height: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.1)'
  },
  titleCont: {
    width,
    padding: 20,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'flex-start'
  },
  fTitle: {
    fontSize: TextFont.TextSize(18),
    color: 'rgba(0, 0, 0, 0.75)',
    flex: 1,
    textAlign: 'left',
    marginBottom: 3,
    fontWeight: '900',
  },
  fCaption: {
    fontSize: TextFont.TextSize(12),
    color: 'rgba(0, 0, 0, 0.45)',
    flex: 1,
    textAlign: 'left',
    marginBottom: 20
  },
  fBody: {
    fontSize: TextFont.TextSize(14),
    color: 'rgba(0, 0, 0, 0.5)',
    flex: 1,
    textAlign: 'left',
    fontWeight: '400'
  },
  bodyCont: {
    width,
    flex: 1,
    backgroundColor: '#F3F3F3',
    padding: 20,
  },
  fSubTitle: {
    fontSize: TextFont.TextSize(18),
    color: 'rgba(0, 0, 0, 0.75)',
    flex: 1,
    textAlign: 'left',
    marginBottom: 5,
    fontWeight: '800'
  },
  aLeft: {
    textAlign: 'left',
    marginBottom: 10
  },
  aCenter: {
    textAlign: 'left',
    marginBottom: 0
  },
  actionCont: {
    flexDirection: 'row',
    width,
    height: 80,
    padding: 15,
    flexWrap: 'nowrap',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white'
  },
  dcBtn: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    borderRadius: 8,
    height: 50,
    backgroundColor: DCColor.BGColor('primary-1'),
    shadowOffset: { width: 0, height: -1 },
    shadowColor: '#000',
    shadowOpacity: 0.1,
    elevation: 0.5
  },
  btnFont: {
    fontSize: TextFont.TextSize(16),
    color: 'rgba(0, 0, 0, 0.75)',
    fontWeight: '800',
    textAlign: 'center'
  },
  receiptCont: {
    width,
    flexDirection: 'column',
    alignContent: 'flex-start',
    justifyContent: 'flex-start',
  },
  uploadCont: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
    width: '100%',
    height: 65,
  },
  uploadBtn: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    height: '100%',
    borderRadius: 5,
    shadowOffset: { width: 0, height: -1 },
    shadowColor: '#000',
    shadowOpacity: 0.05,
    elevation: 0.5,
    backgroundColor: DCColor.BGColor('primary-1')
  },
  headCont: {
    width,
    height: 40,
    paddingHorizontal: 20,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  dcLine: {
    height: 5,
    backgroundColor: DCColor.BGColor('primary-1'),
    width
  },
  receiptBox: {
    flexDirection: 'row',
    width,
    padding: 15,
    paddingHorizontal: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  receiptImgBox: {
    width: 80,
    height: 80,
    position: 'relative',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 20
  },
  receiptImg: {
    position: 'absolute',
    left: 0,
    width: 80,
    height: 80,
    borderRadius: 3,
    borderWidth: 1,
    borderColor: 'white'
  },
  dataCont: {
    flex: 1,
    flexDirection: 'column',
    alignContent: 'center',
    justifyContent: 'center',
    minHeight: 80,
    height: 80
  },
  dcStatus: {
    fontSize: TextFont.TextSize(18),
    fontWeight: '800',
    color: 'rgba(0, 0, 0, 0.75)',
    textAlign: 'left',
    marginBottom: 0
  },
  fDesc: {
    flex: 0,
    marginBottom: 0,
    textAlign: 'left'
  },
  retakeBtn: {
    width: '100%',
    height: 40,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 8,
    backgroundColor: DCColor.BGColor('primary-1'),
    shadowOffset: { width: 0, height: -1 },
    shadowColor: '#000',
    shadowOpacity: 0.1,
    elevation: 0.5
  },
})

@inject('app', 'joy')
@observer
export default class JoyDetailScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const { strings } = $.store.app
    return {
      drawerLockMode: 'locked-closed',
      title: strings.joy_details
    }
  }

  constructor(props) {
    super(props)
    this.state = {
      redeemStatus: 'new',
      initOpacity: new Animated.Value(0.0)
    }
  }

  async componentDidMount() {
    const { goBack } = this.props.navigation;
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
      goBack();
      return true
    })

    const data = await this.props.joy.getJoyDetail(this.props.navigation.state.params.id)
    if (data && data._id) {
      if (data.status === 'new') {
        this.props.joy.markAsReadJoy(data._id);
      }
      else {
        await $.store.joy.getJoyList();
      }
    }
    else {
      Alert.alert(
        $.store.app.strings.alert_joyTitle,
        $.store.app.strings.alert_nojoy,
        [
          { text: 'OK', onPress: () => goBack() },
        ],
        { cancelable: false }
      )
    }
  }

  componentWillUnmount() {
    this.backHandler.remove()
  }

  onLoad = () => {
    Animated.timing(this.state.initOpacity, {
      toValue: 1,
      duration: 250,
      useNativeDriver: true
    }).start()
  }

  onSubmit = async () => {
    const { joyDetail } = this.props.joy
    if (joyDetail.receipt) {
      await this.props.joy.submitJoyReward(this.props.navigation.state.params.id)
    } else {
      $.define.func.showMessage(this.props.app.strings.error_reward_submit_1)
    }
  }

  barcodeFlat = (format) => {
    if (format === 'EAN13') {
      return true
    }
    else {
      return false
    }

  }

  barcodeLength = (format) => {
    if (format === 'EAN13') {
      return 2
    }
    else {
      return 1.2
    }
  }


  render() {
    const { initOpacity } = this.state
    const { _id, status, name, avatars, shortDesc, longDesc, tnc, submissionInfo, sourceInfo, receipt, disapprovedReason, rejectedReason, barcode = null } = this.props.joy.joyDetail
    const { strings } = this.props.app
    const validDate = this.props.joy.joyDetail.submissionInfo && (this.props.joy.joyDetail.submissionInfo.expiredOn || this.props.joy.joyDetail.submissionInfo.endOn)
    const today = new Date()
    const isExpired = moment(today).isAfter(validDate)
    const {
      mainCont,
      scrollCont,
      imgCont,
      underLine,
      titleCont,
      fTitle,
      fCaption,
      fBody,
      bodyCont,
      fSubTitle,
      aLeft,
      aCenter,
      btnFont,
      receiptCont,
      uploadCont,
      uploadBtn,
      headCont,
      dcLine,
      receiptBox,
      receiptImgBox,
      receiptImg,
      dataCont,
      dcStatus,
      fDesc,
      retakeBtn
    } = styles
    return (
      <SafeAreaView style={mainCont}>
        <ScrollView style={scrollCont}>
          {avatars && (
            <Animated.Image
              style={[imgCont, { opacity: initOpacity }]}
              source={$.method.utils.getPicSource(avatars, useLarge, Resources.image.joy_placeholder)}
              onLoad={this.onLoad}
            />
          )}
          <View style={underLine} />
          <View style={titleCont}>
            <Text style={fTitle}>{name ? name : null}</Text>
            {submissionInfo && (submissionInfo.expiredOn || submissionInfo.endOn) ? <Text style={fCaption}>({moment(submissionInfo.expiredOn || submissionInfo.endOn).format('DD MMM YYYY')})</Text> : null}
            <Text style={fBody}>{longDesc ? longDesc : shortDesc}</Text>
          </View>
          {barcode == null ? null
            :
            <View style={{ alignSelf: 'center', paddingVertical: 15, width: '100%' }}>
              <Barcode width={this.barcodeLength(barcode.format)} value={barcode.value} format={barcode.format} flat={this.barcodeFlat(barcode.format)} />
              <Text style={{ textAlign: 'center', fontSize: TextFont.TextSize(16) }}>{barcode.value}</Text>
            </View>
          }
          <View style={bodyCont}>
            <View>
              <Text style={fSubTitle}>{strings.terms}</Text>
              <Text style={[fBody, aLeft]}>{tnc ? tnc : null}</Text>
            </View>
          </View>
        </ScrollView>
        {!isExpired && (status === "pendingSubmission" || status === "rejected") && (!receipt || receipt.photos.length === 0) && (
          <View style={receiptCont}>
            <View style={uploadCont}>
              <TouchableOpacity style={uploadBtn} onPress={() => this.props.navigation.navigate('JoyUploadReceipt', { id: _id })}>
                <Text style={btnFont}>{strings.upload_receipt_now.toUpperCase()}</Text>
              </TouchableOpacity>
            </View>
          </View>
        )}
        {((receipt && receipt.photos.length > 0) || status === "pendingVerification" || status === "completed") && (
          <View style={receiptCont}>
            <View style={underLine} />
            <View style={headCont}>
              <Text style={[fSubTitle, aCenter]}>{strings.receipt}</Text>
            </View>
            <View style={dcLine} />
            <View style={receiptBox}>
              <View style={receiptImgBox}>
                <TouchableOpacity style={[receiptImg, { zIndex: 1 }]} onPress={() => this.props.navigation.navigate('JoyImgViewer', { id: _id })}>
                  <Image style={{ width: '100%', height: '100%', resizeMode: 'cover' }} source={$.method.utils.getPicSource(receipt.photos, useLarge = false, Resources.image.joy_placeholder)} />
                </TouchableOpacity>
                {receipt && receipt.photos.length > 1 && (
                  <TouchableOpacity style={[receiptImg, { left: 20, width: 70, height: 70, zIndex: 0 }]} onPress={() => this.props.navigation.navigate('JoyImgViewer')}>
                    <Image style={{ width: '100%', height: '100%', resizeMode: 'cover' }} source={{ uri: receipt.photos[receipt.photos.length - 2].urlSmall }} />
                  </TouchableOpacity>
                )}
              </View>
              <View style={dataCont}>
                {!isExpired && (() => {
                  switch (status) {
                    case 'pendingSubmission':
                      return <Text style={dcStatus}>{strings.pending_submission}</Text>
                    case 'pendingVerification':
                      return <Text style={dcStatus}>{strings.pending_verification}</Text>
                    case 'pendingCompletion':
                      return <Text style={dcStatus}>{strings.pending_completion}</Text>
                    case 'rejected':
                      return <Text style={dcStatus}>{strings.resubmit_reward}</Text>
                    case 'disapproved':
                      return <Text style={dcStatus}>{strings.disapproved}</Text>
                    case 'completed':
                      return <Text style={dcStatus}>{strings.redeemed}</Text>
                    default:
                      return
                  }
                })()}
                {status === 'disapproved' && (
                  <Text style={[fCaption, fDesc]}>{strings.reason} {disapprovedReason}</Text>
                )}
                {status === 'rejected' && (
                  <Text style={[fCaption, fDesc]}>{strings.reason} {rejectedReason}</Text>
                )}
                {isExpired && (
                  <Text style={dcStatus}>{strings.expired}</Text>
                )}
                {(status === 'pendingSubmission' || status === 'rejected') && !isExpired && (
                  <TouchableOpacity style={retakeBtn} onPress={() => this.props.navigation.navigate('JoyUploadReceipt', { id: _id })}>
                    <Text style={btnFont}>{strings.add_receipt.toUpperCase()}</Text>
                  </TouchableOpacity>
                )}
              </View>
            </View>
          </View>
        )}
        {!isExpired && receipt && receipt.photos.length > 0 && (status === 'pendingSubmission' || status === 'rejected') && !isExpired && (
          <View style={[uploadCont, { borderTopWidth: 1, borderColor: 'rgba(0, 0, 0, 0.1)' }]}>
            <TouchableOpacity style={[uploadBtn, { backgroundColor: DCColor.BGColor('primary-1') }]} onPress={() => this.onSubmit()}>
              <Text style={[btnFont, { color: 'white' }]}>{strings.sub.toUpperCase()}</Text>
            </TouchableOpacity>
          </View>
        )}
      </SafeAreaView>
    )

  }
}

