import React, { Component } from 'react'
import { View, StyleSheet, FlatList } from 'react-native'
import { inject, observer } from 'mobx-react'
import { Screen } from 'dacsee-utils'
import JoyListItem from './component/joy.list.item'
import DcEmptyState from '../../components/emptyState'

const { width } = Screen.window
const styles = StyleSheet.create({
    mainCont: {
        width,
        height: '100%',
        flexDirection: 'column',
        flexWrap: 'nowrap',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        backgroundColor: 'white'
    },
    scrollCont: {
        flex: 1,
        width,
    }
})

@inject('app', 'joy')
@observer
export default class MyJoyScreen extends Component {
    constructor(props) {
        super(props)
    }

    componentWillMount() {
        this.props.joy.getJoyList()
    }

    uploadPhoto = () => {
    }

    _keyExtractor = (item) => (item._id)

    _renderItem = ({ item }) => {
        return (
            <JoyListItem
                img={item.avatars && item.avatars.length > 0 && item.avatars[item.avatars.length - 1].urlSmall}
                title={item.name}
                description={item.shortDesc}
                validDate={item.submissionInfo && (item.submissionInfo.expiredOn || item.submissionInfo.endOn)}
                status={item.status}
                onPress={(item.status === 'pendingSubmission' || item.status === 'pendingVerification') && (() => this.props.navigation.navigate('JoyDetailScreen', { id: item._id }))}
                strings={this.props.app.strings}
                to={() => this.props.navigation.navigate(
                    'JoyDetailScreen',
                    {
                        id: item._id
                    }
                )}
            />
        )
    }

    render() {
        const {
            mainCont,
            scrollCont,
        } = styles
        return (
            <View style={mainCont}>
                {this.props.joy.joyList && this.props.joy.joyList.length ?
                    <View style={scrollCont}>
                        <FlatList
                            refreshing={this.props.joy.joyLoading}
                            onRefresh={() => this.props.joy.getJoyList()}
                            data={this.props.joy.joyList}
                            renderItem={this._renderItem}
                            keyExtractor={this._keyExtractor}
                        />
                    </View>
                    : <DcEmptyState
                        title={this.props.app.strings.no_reward_1}
                        label={this.props.app.strings.no_reward_2}
                        step1={this.props.app.strings.no_reward_step_1}
                        step2={this.props.app.strings.no_reward_step_2}
                        step3={this.props.app.strings.no_reward_step_3}
                        step4={this.props.app.strings.no_reward_step_4}
                    />
                }
            </View>
        )
    }
}

