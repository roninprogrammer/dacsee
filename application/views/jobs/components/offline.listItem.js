import React, { Component } from 'react'
import { Text, View, TouchableOpacity, StyleSheet } from 'react-native'
import moment from 'moment'
import { inject, observer } from 'mobx-react'
import { Screen, DCColor, TextFont } from 'dacsee-utils'

const { height, width } = Screen.window

@inject('app', 'jobs')
@observer
export default class OfflineListItem extends Component {
  constructor(props) {
    super(props)
    this.state = {
      distance: 0
    }
  }
  _getStatus(str) {
    let json = {}
    let { strings } = this.props.app
    switch (str) {
      case 'Pending_Acceptance':
        json.text = strings.Pending_Acceptance
        json.color = DCColor.BGColor('green')
        return json
      case 'On_The_Way':
        json.text = strings.On_The_Way
        json.color = DCColor.BGColor('green')
        return json
      case 'Arrived':
        json.text = strings.Arrived
        json.color = DCColor.BGColor('green')
        return json
      case 'No_Show':
        json.text = strings.No_Show
        json.color = '#ccc'
        return json
      case 'On_Board':  
        json.text = strings.On_Board
        json.color = DCColor.BGColor('green')
        return json
      case 'Completed':
        json.text = strings.Completed
        json.color = DCColor.BGColor('green')
        return json
      case 'Cancelled_by_Passenger':
        json.text = strings.Cancelled_by_Passenger
        json.color = '#ccc'
        return json
      case 'Cancelled_by_System':
        json.text = strings.Cancelled_by_System
        json.color = '#ccc'
        return json
      case 'Cancelled_by_Driver':
        json.text = strings.Cancelled_by_Driver
        json.color = '#ccc'
        return json
      case 'Rejected_by_Driver':
        json.text = strings.Rejected_by_Driver
        json.color = '#red'
        return json
      case 'No_Taker':
        json.text = strings.No_Taker
        json.color = '#ccc'
        return json
    }
  }
  render() {
    const { itemData, itemIndex, onPress = () => { }, working } = this.props
    const { from, destination, booking_at, status, additional_fees = 0, fareObj } = itemData
    const { originalAmount = 0, amount = 0, incentiveAmount = 0 } = fareObj

    let fare = !!originalAmount ? originalAmount : amount

    fare = !!incentiveAmount ? fare + incentiveAmount : fare

    const optionObject = this._getStatus(status)
    return (
      <TouchableOpacity activeOpacity={0.7} onPress={onPress}>
        <View style={[styles.container, { marginTop: itemIndex == 0 ? 15 : 0 }]}>
          <View style={[styles.text_cell, { justifyContent: 'space-between', paddingTop: 15 }]}>
            <View style={{ flexDirection: 'row' }}>
              <Text style={styles.orderDate}>{moment(booking_at).format('MM-DD HH:mm')}</Text>
              <Text style={[styles.order_status, { color: optionObject.color }]}>{optionObject.text}</Text>
            </View>
            <Text style={styles.fare}>{working ? '' : `RM ${(parseFloat(fare) + parseFloat(additional_fees)).toFixed(2)}`}</Text>
          </View>
          <View style={styles.text_cell}>
            <View style={[styles.dot, { backgroundColor: DCColor.BGColor('primary-1') }]} />
            <Text style={styles.adress}>{from.name}</Text>
          </View>
          <View style={styles.text_cell}>
            <View style={[styles.dot, { backgroundColor: DCColor.BGColor('green') }]} />
            <Text style={styles.adress}>{destination.name}</Text>
          </View>
          {working ?
            <View style={[styles.text_cell, { justifyContent: 'space-between' }]}>
              <View style={{ flexDirection: 'row' }}>
                <Text style={styles.orderDate}>{}</Text>
                <Text style={styles.order_status}>{'~15km'}</Text>
              </View>
              <Text style={styles.fare}>{fare}</Text>
            </View>
            : null
          }
        </View>
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    width: width - 20,
    marginLeft: 10,
    marginBottom: 15,
    backgroundColor: '#fff',
    flex: 1,
    paddingBottom: 15,
    paddingHorizontal: 15,
    borderRadius: 6
  },
  info_cell: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    flex: 1
  },
  text_cell: {
    flexDirection: 'row',
    flex: 1,
    alignItems: 'center',
    paddingTop: 10
  },
  orderDate: {
    fontSize: TextFont.TextSize(14),
    fontWeight: 'bold',
    color: '#aaaaaa'
  },
  order_status: {
    fontSize: TextFont.TextSize(14),
    fontWeight: 'bold',
    marginLeft: 15
  },
  fare: {
    fontSize: TextFont.TextSize(16),
    fontWeight: 'bold',
    color: '#000'
  },
  dot: {
    height: 10,
    width: 10,
    borderRadius: 5
  },
  adress: {
    fontSize: TextFont.TextSize(16),
    fontWeight: 'bold',
    color: '#404040',
    marginLeft: 15
  },
  content: {
    height: 40,
    width: width - 40,
    position: 'absolute', top: 5,
    left: 0,
    backgroundColor: '#ccc',
    flexDirection: 'row',
    borderRadius: 20,
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  circle: {
    height: 50,
    width: height / 13,
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center'
  }
})
