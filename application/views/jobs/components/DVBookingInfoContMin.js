import React from 'react'
import moment from 'moment'
import { Text, TouchableOpacity, View, Image, StyleSheet, Dimensions } from 'react-native'
import { TextFont, DCColor, Avatars } from 'dacsee-utils'
import Resources from 'dacsee-resources'
import { DriverInfoCont } from 'global-component'

const { width } = Dimensions.get('window')

const DVBookingInfoContMin = ({
  expandFunction,
  bookingStatus,
  psAvatar,
  psAvatarFunction,
  psName,
  psId,
  psRating,
  callFunction,
  messageFunction,
  advTime,
  advDate,
  bookingETA,
  bookingETAUpdate,
  estDistance,
  bookingId,
  pickupAddress,
  pickupNavigateFunction,
  dropoffAddress,
  dropoffNavigateFunction,
  campaignImg,
  campaignName,
  driverNote,
  unreadChat
}) => {
  const {
    mainCont,
    expandCont,
    expandBar,
    expandIcon,
    bookingStatusCont,
    fBody,
    dcCol,
    fCaption,
    dcBookingId,
    addressCont,
    addressIndicator,
    addressBox,
    fTitle,
    navBtn,
    dcIcon,
    campaignAvatar,
    dcTag,
  } = BookingInfoStyle
  const { strings } = $.store.app

  return (
    <View style={mainCont}>
      {(expandFunction || bookingStatus) ?
        <TouchableOpacity style={expandCont} activeOpacity={1} onPress={expandFunction}>
          {expandFunction ?
            <View style={expandBar}>
              <Image style={expandIcon} source={Resources.image.expand_icon} />
            </View>
            : null
          }
          {bookingStatus ?
            <View style={bookingStatusCont}>
              {(() => {
                switch (bookingStatus && bookingStatus.toUpperCase()) {
                  case 'PENDING_ASSIGNMENT':
                    return <Text style={[fBody, { color: DCColor.BGColor('primary-1') }]}>{strings.booking_status_pending_assignment}</Text>
                  case 'PENDING_ACCEPTANCE':
                    return <Text style={[fBody, { color: DCColor.BGColor('primary-1') }]}>{strings.friend_waitfor_accept}</Text>
                  case 'CONFIRMED':
                    return <Text style={[fBody, { color: DCColor.BGColor('primary-1') }]}>{strings.booking_status_confirmed}</Text>
                  case 'ON_THE_WAY':
                    return <Text style={[fBody, { color: DCColor.BGColor('primary-1') }]}>{strings.on_the_way}</Text>
                  case 'ARRIVED':
                    return <Text style={[fBody, { color: DCColor.BGColor('primary-1') }]}>{strings.booking_status_arrived}</Text>
                  case 'NO_SHOW':
                    return <Text style={[fBody, { color: DCColor.BGColor('primary-1') }]}>{strings.No_Show}</Text>
                  case 'ON_BOARD':
                    return <Text style={[fBody, { color: DCColor.BGColor('primary-1') }]}>{strings.booking_status_on_board}</Text>
                  case 'COMPLETED_BY_PASSENGER':
                    return <Text style={[fBody, { color: DCColor.BGColor('primary-1') }]}>{strings.booking_status_completed_by_passenger}</Text>
                  case 'FINALIZE_TOTAL_FARE':
                    return <Text style={[fBody, { color: DCColor.BGColor('primary-1') }]}>{strings.booking_status_finalize_total_fare}</Text>
                  case 'COMPLETED':
                    return <Text style={[fBody, { color: DCColor.BGColor('primary-1') }]}>{strings.Completed}</Text>
                  case 'CANCELLED_BY_PASSENGER':
                    return <Text style={[fBody, { color: DCColor.BGColor('primary-1') }]}>{strings.Cancelled_by_Passenger}</Text>
                  case 'CANCELLED_BY_DRIVER':
                    return <Text style={[fBody, { color: DCColor.BGColor('primary-1') }]}>{strings.Cancelled_by_Driver}</Text>
                  case 'CANCELLED_BY_SYSTEM':
                    return <Text style={[fBody, { color: DCColor.BGColor('primary-1') }]}>{strings.Cancelled_by_System}</Text>
                  case 'REJECTED_BY_DRIVER':
                    return <Text style={[fBody, { color: DCColor.BGColor('primary-1') }]}>{strings.Rejected_by_Driver}</Text>
                  case 'NO_DRIVER_AVAILABLE':
                    return <Text style={[fBody, { color: DCColor.BGColor('primary-1') }]}>{strings.booking_status_no_driver_avaiable}</Text>
                  case 'NO_TAKER':
                    return <Text style={[fBody, { color: DCColor.BGColor('primary-1') }]}>{strings.No_Taker}</Text>
                  default:
                    return null
                }
              })()}

              {
                bookingETA ? (
                  bookingETA >= 3600 ? (
                    <Text style={[fBody, { marginLeft: 5, color: 'rgba(255, 255, 255, 0.45)' }]}>ETA ~ {moment.utc(bookingETA*1000).add(1, 'm').format('h[h] m[m]')}</Text>
                  ) : (
                    bookingETA >= 60 ? (
                      <Text style={[fBody, { marginLeft: 5, color: 'rgba(255, 255, 255, 0.45)' }]}>ETA ~ {moment.utc(bookingETA*1000).add(1, 'm').format('m')}{strings.eta_minutes}</Text>
                    ) : (
                      <Text style={[fBody, { marginLeft: 5, color: 'rgba(255, 255, 255, 0.45)' }]}>ETA ~ 1{strings.eta_minutes}</Text>
                    )
                  )
                ) : null
              }

              {
                bookingETAUpdate ? (
                  <TouchableOpacity onPress={bookingETAUpdate}>
                    <Image style={[dcIcon, { marginLeft: 5, width: 20, height: 20, tintColor: 'rgba(255, 255, 255, 0.45)' }]} source={Resources.image.edit} />
                  </TouchableOpacity>
                ) : null
              }
            </View>
            : null
          }
        </TouchableOpacity>
        : null
      }
      {(psAvatar || psAvatarFunction || psId || psRating || callFunction || messageFunction) ?
        <DriverInfoCont
          avatar={psAvatar}
          avatarFunction={psAvatarFunction}
          name={psName}
          id={psId}
          rating={psRating}
          callFunction={callFunction}
          messageFunction={messageFunction}
          unreadChat={unreadChat}
        />
        : null
      }
      {(advDate || advTime) ?
        <View style={[dcCol, { backgroundColor: DCColor.BGColor('red'), height: 50 }]}>
          <Text style={[fTitle, { color: 'white', flex: 1, textAlign: 'left' }]}>{strings.advance_booking}</Text>
          <View style={[dcTag, { backgroundColor: 'rgba(0, 0, 0, 0.7)', height: '100%' }]}>
            <Text style={[fCaption, { color: 'white', fontWeight: '900' }]}>{advDate}</Text>
            <Text style={[fCaption, { color: 'white', marginLeft: 5, fontWeight: '900', color: DCColor.BGColor('primary-1') }]}>{advTime}</Text>
          </View>
        </View>
        : null
      }
      {estDistance ?
        <View style={[dcCol, { backgroundColor: DCColor.BGColor('primary-2') }]}>
          <Text style={[fTitle, { color: 'white' }]}>Estimated Distance</Text>
          <Text style={[fTitle, { color: 'rgba(255, 255, 255, .35)', flex: 1, textAlign: 'right' }]}>{estDistance} KM</Text>
        </View>
        : null
      }
      {bookingId ?
        <View style={dcCol}>
          <Text style={fCaption}>{strings.booking_id}</Text>
          <Text style={[fCaption, dcBookingId]}>{bookingId}</Text>
        </View>
        : null
      }
      {pickupAddress ?
        <View style={dcCol}>
          <View style={addressCont}>
            <View style={addressIndicator} />
            <View style={addressBox}>
              <Text style={fTitle}>{strings.pick_up_address}</Text>
              <Text numberOfLines={2} ellipsizeMode='tail' style={fCaption}>{pickupAddress}</Text>
            </View>
            {pickupNavigateFunction ?
              <TouchableOpacity style={[navBtn, { marginLeft: 30 }]} onPress={pickupNavigateFunction}>
                <Image style={[dcIcon, { tintColor: 'white' }]} source={Resources.image.navigate_icon} />
              </TouchableOpacity>
              : null
            }
          </View>
        </View>
        : null
      }
      {dropoffAddress ?
        <View style={dcCol}>
          <View style={addressCont}>
            <View style={[addressIndicator, { backgroundColor: DCColor.BGColor('primary-1') }]} />
            <View style={addressBox}>
              <Text style={fTitle}>{strings.drop_off_address}</Text>
              <Text numberOfLines={2} ellipsizeMode='tail' style={fCaption}>{dropoffAddress}</Text>
            </View>
            {dropoffNavigateFunction ?
              <TouchableOpacity style={[navBtn, { marginLeft: 30, backgroundColor: DCColor.BGColor('primary-1') }]} onPress={dropoffNavigateFunction}>
                <Image style={dcIcon} source={Resources.image.navigate_icon} />
              </TouchableOpacity>
              : null
            }
          </View>
        </View>
        : null
      }
      <View style={dcCol}>
        <Image source={campaignImg ? { uri: Avatars.getHeaderPicUrl(campaignImg) } : Resources.image.booking_detail_car} style={campaignImg ? campaignAvatar : [campaignAvatar, { borderRadius: 0 }]} />
        <Text style={fTitle}>{campaignName}</Text>
      </View>
      <View style={[dcCol, { backgroundColor: DCColor.BGColor('disable'), borderColor: 'transparent' }]}>
        <Image style={dcIcon} source={Resources.image.driver_note_icon} />
        <Text style={[fCaption, { marginLeft: 20 }]}>{driverNote}</Text>
      </View>
    </View>
  )
}

const BookingInfoStyle = StyleSheet.create({
  mainCont: {
    width,
    backgroundColor: 'white',
  },
  expandCont: {
    backgroundColor: DCColor.BGColor('primary-2'),
  },
  expandBar: {
    padding: 8,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: "center",
  },
  expandIcon: {
    resizeMode: 'contain',
    tintColor: DCColor.BGColor('primary-1'),
    opacity: .4,
    height: 9,
    width: 35
  },
  bookingStatusCont: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: 10,
  },
  dcCol: {
    width,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderColor: 'rgba(0, 0, 0, 0.05)',
    paddingVertical: 10,
    paddingHorizontal: 15,
  },
  fBody: {
    fontSize: TextFont.TextSize(15),
    fontWeight: '900',
    color: DCColor.BGColor('primary-2')
  },
  fCaption: {
    fontSize: TextFont.TextSize(12),
    fontWeight: '400',
    color: 'rgba(0, 0, 0, 0.45)',
    textAlign: 'left',
  },
  dcBookingId: {
    fontWeight: '700',
    color: DCColor.BGColor('primary-2'),
    textAlign: 'right',
    flex: 1
  },
  addressCont: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    flex: 1,
  },
  addressIndicator: {
    width: 10,
    height: 10,
    padding: 5,
    borderRadius: 5,
    backgroundColor: DCColor.BGColor('secondary-1'),
    marginRight: 20,
    marginLeft: 10,
    marginTop: 6
  },
  addressBox: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  fTitle: {
    fontSize: TextFont.TextSize(15),
    fontWeight: '700',
    textAlign: 'left'
  },
  navBtn: {
    height: 45,
    width: 45,
    borderRadius: 45 / 2,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: DCColor.BGColor('secondary-1'),
    shadowOffset: { width: 0, height: -1 },
    shadowColor: '#000',
    shadowOpacity: 0.05,
    elevation: 3
  },
  dcIcon: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
    tintColor: DCColor.BGColor('primary-2')
  },
  campaignAvatar: {
    width: 25,
    height: 25,
    borderRadius: 25 / 2,
    resizeMode: 'cover',
    marginRight: 15
  },
  dcTag: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: DCColor.BGColor('primary-1'),
    borderRadius: 50,
    padding: 1,
    paddingHorizontal: 8,
    marginLeft: 10
  },
})

export default DVBookingInfoContMin
