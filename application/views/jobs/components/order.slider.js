import React, { Component } from 'react'
import {
  Text,
  View,
  Dimensions,
  Animated,
  StyleSheet
} from 'react-native'
import { Icons } from 'dacsee-utils'
import { inject, observer } from 'mobx-react'

const { width } = Dimensions.get('window')

@inject('app')
@observer
export default class OrderSlider extends Component {

  render() {
    const { currentPosition } = this.props
    const { strings } = this.props.app

    return (
      <View style={styles.container}>
        <View style={styles.content}>
          <Text style={{ fontSize: 15, color: '#fff', marginLeft: 10 }}>{strings.reject}</Text>
          <Text style={{ fontSize: 15, color: '#fff', marginRight: 10 }}>{strings.accept}</Text>
        </View>
        <Animated.View
          style={[{ transform: [{ translateX: currentPosition }], }, styles.circle]}>
          {Icons.Generator.Material('code', 24, '#fff')}
        </Animated.View>
      </View>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    width: width - 50, height: 30, flexDirection: 'row', justifyContent: 'center', alignItems: 'center'
  },
  content: {
    height: 20, width: width - 50, position: 'absolute', top: 5, left: 0, backgroundColor: '#ccc', flexDirection: 'row', borderRadius: 20, justifyContent: 'space-between', alignItems: 'center'
  },
  circle: {
    height: 30, width: 50, borderRadius: 25, justifyContent: 'center', flexDirection: 'row', alignItems: 'center', backgroundColor: '#fdb21a'
  }
})
