import React, { Component } from 'react'
import { Text, View, Image, Animated, PanResponder, StyleSheet } from 'react-native'
import { inject, observer } from 'mobx-react'
import moment from 'moment'
import Material from 'react-native-vector-icons/Octicons'
import Material2 from 'react-native-vector-icons/MaterialIcons'

import OrderSlider from './order.slider'
import { Screen, UtilMath, Avatars, TextFont, DCColor } from 'dacsee-utils'
import Utils from '../../../global/method/utils'

const { height, width } = Screen.window

@inject('app', 'jobs')
@observer
export default class OfflineListItem extends Component {
  constructor(props) {
    super(props)
    this.sliderWidth = 0
    this.currentPosition = new Animated.Value(0)
    this.createPanResponder()
    this.state = {
      canScroll: true,
      distance: 0
    }
  }

  onLayout(e) {
    this.sliderWidth = e.nativeEvent.layout.width
  }
  createPanResponder() {
    this.panResponder = PanResponder.create({
      onStartShouldSetPanResponder: (e, gestureState) => this.state.canScroll,
      onStartShouldSetPanResponderCapture: (e, gestureState) => false,
      onMoveShouldSetPanResponder: (e, gestureState) => true,
      onMoveShouldSetPanResponderCapture: (e, gestureState) => false,
      onPanResponderGrant: (e, gestureState) => this.onPanResponderGrant(e, gestureState),
      onPanResponderMove: (e, gestureState) => this.onPanResponderMove(e, gestureState),
      onPanResponderTerminationRequest: (e, gestureState) => false,
      onPanResponderRelease: (e, gestureState) => this.onPanResponderRelease(e, gestureState),
      onPanResponderTerminate: (e, gestureState) => null,
      onShouldBlockNativeResponder: (e, gestureState) => true
    })
  }
  onPanResponderGrant(e, gestureState) {
    this.props.sliderScorll(false)
    this.currentPosition.stopAnimation()
    this.prevTouches = e.nativeEvent.touches
    this.firstTouche = e.nativeEvent.touches
  }
  getIsMid(x) {
    let minX = width / 2 - this.sliderWidth / 4
    let maxX = width / 2 + this.sliderWidth / 4
    if (x < minX) return -1
    if (x > maxX) return 1
    return 0
  }
  getIsOut(x) {
    let minX = width / 2 - this.sliderWidth / 2 + 25
    let maxX = width / 2 + this.sliderWidth / 2 - 25
    let type = x <= minX ? minX : x >= maxX ? maxX : 0
    return type
  }

  onPanResponderMove(e, gestureState) {
    let { touches } = e.nativeEvent
    let prevTouches = this.prevTouches
    this.prevTouches = touches
    if (this.firstTouche[0].pageX < (width / 2 - 80) || this.firstTouche[0].pageX > (width / 2 + 80)) {
      return
    }
    if (touches.length != 1 || touches[0].identifier != prevTouches[0].identifier) {
      return
    }
    if (this.getIsOut(touches[0].pageX) !== 0 && this.getIsOut(prevTouches[0].pageX) === 0) {
      let scaleX = this.currentPosition._value < 0 ? -(this.sliderWidth / 2 - 25) : (this.sliderWidth / 2 - 25)
      this.currentPosition.setValue(scaleX)
      this.prevTouches[0].pageX = this.getIsOut(touches[0].pageX)
      return
    } else if (this.getIsOut(touches[0].pageX) !== 0 && this.getIsOut(prevTouches[0].pageX !== 0)) {
      return
    }
    let dy = touches[0].pageX - prevTouches[0].pageX
    let pos = this.currentPosition._value + dy
    this.currentPosition.setValue(pos)
  }
  async onPanResponderRelease(e, gestureState) {
    const { itemData, jobs } = this.props
    const { _id } = itemData
    this.props.sliderScorll(true)
    if (this.firstTouche[0].pageX < (width / 2 - 80) || this.firstTouche[0].pageX > (width / 2 + 80)) {
      return
    }
    let status = this.getIsMid(this.prevTouches[0].pageX)
    switch (status) {
      case 0: this.currentPosition.setValue(0)
        break
      case -1: this.currentPosition.setValue(-(this.sliderWidth / 2 - 25))
        break
      case 1: this.currentPosition.setValue(this.sliderWidth / 2 - 25.5)
        break
    }
    if (status !== 0) {
      this.setState({ canScroll: false })
      jobs.bookingHandle({ action: status === -1 ? 'reject' : 'accept' }, _id)
    }
  }
  handlePositionChange(value) {

  }
  UNSAFE_componentWillReceiveProps(nextProps) {
    const { latitude = 0, longitude = 0 } = nextProps.app.coords
    const { lng, lat } = nextProps.itemData.from.coords
    let distance = parseFloat(UtilMath.distance(longitude, latitude, lng, lat) / 1000).toFixed(1)
    if (!(this.state.distance - distance === 0)) {
      this.setState({ distance })
    }
  }

  distanceCal = (coords, from) => {
    const { latitude = 0, longitude = 0 } = coords
    const { lng, lat } = from

    let calcDistance = parseFloat(Utils.distance(longitude, latitude, lng, lat) / 1000)
    distance = (calcDistance <= 0.1) ? '<100M' : calcDistance.toFixed(1) + 'KM'

    return distance;
  }
  
  render () {
    const { strings, coords } = this.props.app
    const { itemData, itemDay, onPress = () => { } } = this.props
    const { from, destination, booking_at, assign_type, group_info, payment_method, status, type, passenger_info, fareObj ,fare} = itemData

    const { promo, originalAmount = 0, amount } = fareObj

    // const fare = !!originalAmount ? originalAmount : amount

    const { avatars, fullName } = passenger_info
    return (
      <View style={[styles.container, styles.shadow]}>
        <View style={{ flexDirection: 'row', flex: 1, alignItems: 'center', borderBottomWidth: 1, padding: 10, borderBottomColor: '#ccc', justifyContent: 'space-between' }}>
          <Image
            source={{ uri: Avatars.getHeaderPicUrl(avatars) }}
            style={{ width: 20, height: 20, borderRadius: 10 }} />
          <View style={{ flexDirection: 'row', paddingHorizontal: 10, justifyContent: 'center', alignItems: 'center' }}>
            <Material name='clock' size={15} />
            <Text style={styles.orderDate}>{type === 'now' ? strings.now : moment(booking_at).format('MM-DD (HH:mm)')}</Text>
          </View>
          <View style={{ flexDirection: 'row', paddingHorizontal: 10, justifyContent: 'center', alignItems: 'center', width: (width / 2) - 5 }}>
          <Material name='location' size={16} />
            <Text style={styles.distance}>{this.distanceCal(coords, from.coords)}</Text>
            <Material2 name='attach-money' size={16} paddingLeft={15}/>
            <Text style={styles.address}>RM {fare} </Text>
          </View>
          <Material2 name='play-circle-filled' size={18} />

        </View>
        {
          status === 'Pending_Acceptance' && this.state.canScroll &&
          <View onLayout={e => this.onLayout(e)} {...this.panResponder.panHandlers} style={{ alignItems: 'center', paddingTop: 10, paddingBottom: 5 }}>
            <OrderSlider currentPosition={this.currentPosition} />
          </View>
        }
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    width: width - 10,
    marginLeft: 5,
    marginRight: 5,
    backgroundColor: '#fff',
    flex: 1
  },
  info_cell: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    flex: 1
  },
  shadow: {
    elevation: 2,
    shadowOffset: { width: 0, height: 0 },
    shadowColor: 'rgba(0,0,0,0.4)',
    shadowOpacity: .4,
    shadowRadius: 3
  },
  text_cell: {
    flexDirection: 'row',
    flex: 1,
    alignItems: 'center',
    paddingBottom: 15,
    marginHorizontal: 10
  },
  orderDate: {
    fontSize: TextFont.TextSize(12),
    fontWeight: 'bold',
    color: '#000000',
    paddingLeft: 3
  },
  order_status: {
    fontSize: TextFont.TextSize(14),
    fontWeight: 'bold',
    color: DCColor.BGColor('green'),
    marginLeft: 2
  },
  fare: {
    fontSize: TextFont.TextSize(16),
    fontWeight: 'bold',
    color: '#000'
  },
  dot: {
    height: 10,
    width: 10,
    borderRadius: 5
  },
  address: {
    fontSize: TextFont.TextSize(12),
    fontWeight: 'bold',
    color: '#000000',
    paddingLeft: 3
  },
  content: {
    height: 40, width: width - 40,
    position: 'absolute',
    top: 5,
    left: 0,
    backgroundColor: '#ccc',
    flexDirection: 'row',
    borderRadius: 20,
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  circle: {
    height: 50,
    width: height / 13,
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center'
  },
  advance: {
    height: 32,
    backgroundColor: DCColor.BGColor('primary-1'),
    marginTop: 0,
    borderTopEndRadius: 6,
    borderTopStartRadius: 6,
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingHorizontal: 10
  },
  distance: {
    fontSize: TextFont.TextSize(12), 
    fontWeight: 'bold', 
    color: '#000000', 
    paddingRight: 30
  },
})
