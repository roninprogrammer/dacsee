import React, { Component } from 'react'
import { Text, View, TouchableOpacity, ScrollView, Image, StyleSheet, Alert } from 'react-native'
import InteractionManager from 'InteractionManager'
import { inject, observer } from 'mobx-react'
import { observable } from 'mobx'
import { Screen, Icons, DCColor, TextFont } from 'dacsee-utils'
import Resources from 'dacsee-resources'
import BlockPassenger from './components/modal.block.passenger'

const { height } = Screen.window

@inject('app', 'jobs', 'account', 'circle')
@observer
export default class BookingRateScreen extends Component {
  @observable star = [true, true, true, true, true]
  @observable rate_index: Number = 5
  static navigationOptions = ({ navigation }) => {
    return {
      drawerLockMode: 'locked-closed',
      title: $.store.app.strings.passenger_rate,
      headerLeft: (
        <TouchableOpacity
          activeOpacity={0.7}
          style={{ top: 1, width: 54, paddingLeft: 8, justifyContent: 'center', alignItems: 'flex-start' }}
          onPress={() => {
            navigation.state.params.rightPress && navigation.state.params.rightPress()
          }}
        >
          {Icons.Generator.Material('keyboard-arrow-left', 30, DCColor.BGColor('primary-2'))}
        </TouchableOpacity>
      )
    }
  }

  constructor(props) {
    super(props)
    this.state = {
      reasonModal: false,
    }
  }

  componentDidMount() {
    InteractionManager.runAfterInteractions()
    this.props.navigation.setParams({
      rightPress: () => {
        this.props.navigation.popToTop()
      }
    })
    this.props.jobs.getBlockReasons('driverBlock')
  }

  componentWillUnmount() {
  }

  pressStar = (i) => {
    this.rate_index = parseInt(i)
    switch (i) {
      case 0:
        this.star = [true, false, false, false, false]
        break
      case 1:
        this.star = [true, true, false, false, false]
        break
      case 2:
        this.star = [true, true, true, false, false]
        break
      case 3:
        this.star = [true, true, true, true, false]
        break
      default:
        this.star = [true, true, true, true, true]
        break
    }
  }

  _press = async (type) => {
    const { passenger_info } = this.props.jobs.active_job
    switch (type) {
      case 0:
        // 加好友
        await this.props.jobs.bookingRating(this.rate_index)
        await this.props.circle.sendRequest(passenger_info._id)
        await $.store.jobs.getDriverDashboard()
        await $.store.wallet.getDriverCreditWallet()
        this.props.navigation.popToTop()
        break
      case 1:
        // 忽略
        await this.props.jobs.bookingRating(this.rate_index)
        await $.store.jobs.getDriverDashboard()
        await $.store.wallet.getDriverCreditWallet()
        this.props.navigation.popToTop()
        break
      // case 2:
      //   await this.props.jobs.bookingRating(this.rate_index)
      //   await this.blockFriend()
      //   this.props.navigation.popToTop()
      //   break
    }
  }

  async blockFriend(reason, text) {
    const { strings } = this.props.app
    const { passenger_info } = this.props.jobs.active_job
    this.props.app.showLoading()
    try {
      if (text !== '') {
        const data = await $.method.session.Circle.Put('v1/block', { action: 'block', user_id: passenger_info._id, blockReason_id: reason, otherReason: text })
        this.props.app.hideLoading()
        if (data.isSuccess) {
          this.props.circle.getFriends()
          await $.store.jobs.getDriverDashboard()
          await $.store.wallet.getDriverCreditWallet()
          return $.define.func.showMessage(strings.block_success)
        }
        $.define.func.showMessage(strings.block_failed)
      }
      else {
        const data = await $.method.session.Circle.Put('v1/block', { action: 'block', user_id: passenger_info._id, blockReason_id: reason })
        this.props.app.hideLoading()
        if (data.isSuccess) {
          this.props.circle.getFriends()
          await $.store.jobs.getDriverDashboard()
          await $.store.wallet.getDriverCreditWallet()
          return $.define.func.showMessage(strings.block_success)
        }
        $.define.func.showMessage(strings.block_failed)
      }
    } catch (e) {
      this.props.app.hideLoading()
      $.define.func.showMessage(strings.timeout_try_again)
    }
  }
  showReasonDialog = () => {
    this.setState({
      reasonModal: true
    })
  }

  onHide = () => {
    this.setState({
      reasonModal: false
    })
  }

  confirm = async (reason_id, text, key) => {

    if (key == 5 && text == '' || text == ' ') {
      Alert.alert($.store.app.strings.provide_reason_passenger)
    }
    else {
      this.onHide();
      await this.props.jobs.bookingRating(this.rate_index)
      await this.blockFriend(reason_id, text)
      this.props.navigation.popToTop()
    }
  }

  render() {
    const { passenger_info, driver_id, from, destination, fare, payment_method } = this.props.jobs.active_job
    const { strings } = this.props.app
    const buttons = [strings.yes, strings.no]
    const { btnText } = styles
    return (
      <View style={{ flex: 1, backgroundColor: '#fff' }}>
        <BlockPassenger visible={this.state.reasonModal} onClose={() => this.onHide()} confirm={(reason, text, key) => this.confirm(reason, text, key)} />
        <ScrollView>
          <View style={{ minHeight: height - 90 }}>
            <View style={{ paddingHorizontal: 20 }}>
              <HeaderView driver={passenger_info} strings={strings} />
              <View style={{ height: 1, backgroundColor: '#D8D8D8' }} />

              <View style={{ paddingTop: 15 }}>
                <Text style={{ fontSize: 24, fontWeight: 'bold' }}>{strings.rating}</Text>
                <Text style={{ color: '#bbb', marginTop: 5 }}>{strings.how_rate_passenger}</Text>
                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around', marginTop: 15 }}>
                  {this.star.map((item, index) =>
                    <TouchableOpacity key={index} onPress={() => this.pressStar(index)} activeOpacity={1}>
                      {Icons.Generator.Awesome('star', 50, item ? DCColor.BGColor('primary-1') : 'rgba(96,96,96,0.2)')}
                    </TouchableOpacity>
                  )}
                </View>
              </View>
            </View>
            <View style={styles.add} >
              <Image source={Resources.image.rate_bg}
              />
              <View style={{ marginVertical: 5, alignItems: 'center' }}>
                <Text style={{ fontWeight: 'bold', fontSize: 22 }}>{strings.add_to_circle}</Text>
                <Text >{strings.would_you_like_to_add_passenger_friend}</Text>
              </View>
              <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, marginBottom: 10 }}>
                {buttons.map((item, index) =>
                  <TouchableOpacity onPress={() => this._press(index)} activeOpacity={0.7} style={[styles.button, index === 1 && { marginHorizontal: 10 }]} key={index}>
                    <Text style={btnText}>{item}</Text>
                  </TouchableOpacity>
                )}
                <TouchableOpacity style={{ backgroundColor: DCColor.BGColor('red'), padding: 10, borderRadius: 28, borderWidth: 6, borderColor: '#FFF' }} onPress={() => { this.showReasonDialog() }}>
                  <Text style={[btnText, {color: 'white'}]}>{strings.block_passenger}</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    )
  }
}

const HeaderView = (props) => {
  const { driver, strings } = props
  const { fullName = 'yanmaol', avatars, userId = 'TM-100489' } = driver
  return (
    <View style={{ flexDirection: 'row', alignItems: 'center', paddingVertical: 20 }}>
      <Image
        style={{ width: 50, height: 50, borderRadius: 25, marginRight: 10 }}
        source={{ uri: $.method.utils.getHeaderPicUrl(avatars) }}
      />
      <View>
        <Text style={{ fontWeight: 'bold', fontSize: 17, marginBottom: 4 }}>{fullName}</Text>
        <Text>{`${strings.passenger} ID:${userId}`}</Text>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  button: {
    paddingVertical: 10,
    width: 80,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 28,
    borderWidth: 6,
    borderColor: '#fff'
  },
  btnText: {
    fontSize: TextFont.TextSize(14),
    fontWeight: 'bold',
    color: DCColor.BGColor('primary-2')
  },
  add: {
    backgroundColor: DCColor.BGColor('primary-1'),
    flex: 1,
    borderTopRightRadius: 35,
    borderTopLeftRadius: 35,
    marginTop: 34,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20,
    shadowOffset: { width: 0, height: 1 },
    shadowColor: '#000',
    shadowOpacity: 0.4,
    elevation: 2
  }
})
