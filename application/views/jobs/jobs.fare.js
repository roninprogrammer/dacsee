import React, { Component } from 'react'
import { Text, View, Image, ScrollView, TouchableOpacity, TextInput, StyleSheet } from 'react-native'
import { Screen, DCColor, TextFont } from 'dacsee-utils'
import Resources from 'dacsee-resources'
import { inject, observer } from 'mobx-react'
import PromoDetailModel from '../main/promo/component/modal.promo.detail'
import { BlockPassenger } from './components'
import { DCInfoModal } from '../modal'
import { DCUserInfoCont } from 'global-component' 
import HeaderTab from '../../components/header-tab'

const { width } = Screen.window

@inject('app', 'jobs')
@observer
export default class JobsFareScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const { control } = $.store.app
    const navMap = {
      drawerLockMode: 'locked-closed',
      headerTitle: (
        <HeaderTab onlyTitle={false} active={control.header.indexTrip} onPress={index => control.header.tripNavigate(index)}>
          <Image source={Resources.image.logo_landscape_border} style={{ width: 100, height: 60, resizeMode: 'contain', marginRight: 10 }} />
        </HeaderTab>
      ),
      headerStyle: {
        backgroundColor: DCColor.BGColor('primary-1'),
        shadowColor: 'transparent',
        shadowOpacity: 0,
        borderBottomWidth: 0,
        borderBottomColor: 'transparent',
        elevation: 0
      }
    }

    navMap.headerLeft = null
    navMap.gesturesEnabled = false

    return navMap
  }

  constructor(props) {
    super(props)
    this.state = {
      editor: true,
      tollsFare: '',
      otherFare: 0,
      reasonModal: false,
      ratingVisible: false,
      modalType: 'rating',
      modalProps: {},
      rating: 5,
      passenger_id: ''
    }
  }

  componentWillUnmount() {
    const { _id, isSubmit } = this.props.navigation.state.params
    const { bookingHandle } = this.props.jobs
    if (!this.isCollect && isSubmit) {
      bookingHandle({ action: 'completed' }, _id)
      this.props.navigation.popToTop()
    }
  }

  async componentDidMount() {
    const { jobs } = this.props 

    const { status, additional_fees = 0 } = this.props.navigation.state.params
    const nonEditableStatuses = ['Finalize_Total_Fare', 'Completed_by_Passenger']
    if (nonEditableStatuses.includes(status)) this.setState({ editor: false, tollsFare: additional_fees })

    jobs.getBlockReasons('driverBlock')
    this.setState({ passenger_id: jobs.active_job.passenger_info._id })
  }

  showBlockReason = () => {
    this.setState({ reasonModal: true })
  }

  onHideBlockReason = () => {
    this.setState({ reasonModal: false })
  }

  onBlock = async (reason_id, text) => {
    const { strings } = this.props.app
    const { passenger_id } = this.state

    try {
      const data = await $.method.session.Circle.Put('v1/block', { 
        action: 'block', 
        user_id: passenger_id, 
        blockReason_id: reason_id, 
        otherReason: text 
      })
        
      if (data.isSuccess) {
        $.store.circle.getFriends()
        return $.define.func.showMessage(strings.block_success)
      }

      this.completeRating()
      $.define.func.showMessage(strings.block_failed)
    } catch (e) {
      $.define.func.showMessage(strings.timeout_try_again)
    }
  }

  confirmReason = async (reason_id, text, key) => {
    if (key == 5 && text == '' || text == ' ') {
      Alert.alert($.store.app.strings.provide_reason_driver)
    } else {
      this.onHideBlockReason()
      this.completeRating()

      await this.onBlock(reason_id, text)
    }
  }

  onHideRating = () => {
    this.setState({ ratingVisible: false, modalType: 'rating', rating: 5 })
  }

  completeRating = () => {
    $.store.jobs.getDriverDashboard()
    $.store.wallet.getDriverCreditWallet()
    this.props.navigation.popToTop()
  }

  getPayMentText(key) {
    const { strings } = this.props.app
    let pay = ''
    switch (key) {
      case 'cash':
        pay = strings.cash.toUpperCase()
        break
    }
    return pay
  }

  numberState = () => {
    this.setState({ tollsFare: '' })
    alert(this.props.app.strings.number_only)
  }

  async submit() {
    const { _id } = this.props.navigation.state.params
    const { bookingHandle } = this.props.jobs
    let { tollsFare, editor } = this.state

    if (editor) {
      if (tollsFare === '') {
        tollsFare = 0
      } else if (isNaN(parseFloat(tollsFare))) {
        return $.define.func.showMessage(this.props.app.strings.number_only)
      }

      $.store.passenger.fare_info.additional_fees = tollsFare

      await bookingHandle({ action: 'finalize_total_fare', additional_fees: parseFloat(tollsFare) }, _id)
      this.props.navigation.setParams({ isSubmit: true })
      this.setState({ editor: false, tollsFare })
    } else {
      this.isCollect = true
      bookingHandle({ action: 'completed' }, _id)

      this.showModal('rating')
    }
  }

  passengerRating = (i) => {
    let { modalProps, rating } = this.state

    rating = i + 1
    modalProps.rating = i + 1

    this.setState({ rating: rating, modalProps: modalProps })
  }

  showModal = (type) => {
    const { jobs, app } = this.props
    const { strings } = app
    const { passenger_info } = jobs.active_job
    let { rating = 5 } = this.state

    let modalProps = {}

    switch(type) {
      case 'rating':
        modalProps = {
          rating: rating,
          rateFunction: (index) => this.passengerRating(index),
          confirmBtn: strings.rating_submit,
          label: strings.rating_label,
          caption: strings.rating_passenger_caption,
          onPress: async () => {
            // Submit Rating
            const { rating: usrRating } = this.state

            await jobs.bookingRating(usrRating)

            if (usrRating >= 4) {
              const found = $.store.circle.friendData.friends.filter((friend) => {
                return friend.friend_id === passenger_info._id
              })
              
              if(found && found.length) {
                this.onHideRating()
                this.completeRating()
              } else {
                this.showModal('addFriend')
              }
              
            } else if (usrRating == 1) {
              this.showModal('block')
            } else {
              this.onHideRating()
              this.completeRating()
            }
          }
        }
        break
      
      case 'block':
        modalProps = {
          errorBtn: strings.block_passenger_submit,
          label: strings.block_passenger_label,
          caption: strings.block_passenger_caption,
          onPress: () => {
            // Block Driver
            this.onHideRating()
            this.showBlockReason()
          }
        }
        break
      
      case 'addFriend':
        modalProps = {
          confirmBtn: strings.friend_add_submit,
          label: strings.friend_add_label,
          caption: strings.friend_add_passenger_caption,
          onPress: () => {
            // Add Friend
            $.store.circle.sendRequest(passenger_info._id)
            this.onHideRating()
            this.completeRating()
          }
        }
        break
      default:
        this.onHideRating()
        this.completeRating()
        break
    }

    this.setState({ 
      modalType: type,
      modalProps: modalProps,
      rating: rating,
      ratingVisible: true
    })
  }

  render() {
    const {
      mainCont,
      bodyCont,
      dcCol,
      fTitle,
      dcTag,
      fTag,
      fareCont,
      notiCont,
      dcIcon,
      fCaption,
      actionCont,
      dcBtn,
      fBtn
    } = styles
    
    const { active_job = {} } = this.props.jobs
    const { passenger_info, fareObj } = active_job

    let { amount = 0, promo_discount = 0, originalAmount = 0, campaign_discount = 0, incentiveAmount = 0 } = fareObj
    let total = 0

    const { strings } = this.props.app
    const { editor, tollsFare } = this.state
    const additional_fees = isNaN(parseFloat(tollsFare)) ? 0 : parseFloat(tollsFare)

    originalAmount = parseFloat(originalAmount)
    promo_discount = parseFloat(promo_discount)
    campaign_discount = parseFloat(campaign_discount)
    amount = !!originalAmount ? originalAmount : parseFloat(amount)
    total = amount + additional_fees - promo_discount - campaign_discount

    const { modalProps = {}, ratingVisible = false, reasonModal = false } = this.state

    return (
      <View style={mainCont}>
        <BlockPassenger 
          visible={reasonModal} 
          onClose={() => {
            this.onHideBlockReason()
            this.completeRating()
          }} 
          confirm={(reason, text, key) => this.confirmReason(reason, text, key)} 
        />
        <ScrollView style={{ flex: 1 }}>
          <View style={bodyCont}>
            <View style={[dcCol, { marginBottom: 20 }]}>
              <Text style={fTitle}>{strings.trip_fare}</Text>
              <View style={dcTag}>
                <Text style={fTag}>{this.getPayMentText('cash')}</Text>
              </View>
            </View>
            <View style={fareCont}>
              <CarCell
                left={strings.total_fare}
                right={`RM ${parseFloat(amount).toFixed(2)}`}
              />
              <CarCell
                left={strings.tolls_fare}
                right={`RM ${parseFloat(tollsFare).toFixed(2)}`}
                editor={editor}
                onChangeText={(val) => Number(val) >= 0 ? this.setState({ tollsFare: val }) : this.numberState()}
                changValue={tollsFare}
              />
              {
                !!promo_discount && (
                  <CarCell
                    onPressPromo={() => {
                      $.store.jobs.fareInfoModalVisible = true
                    }}
                    left={`${strings.promo}`}
                    right={`- RM ${promo_discount.toFixed(2)}`}
                  />
                )
              }

              {
                !!campaign_discount && (
                  <CarCell
                    left={`Joy Promotion`}
                    right={`- RM ${campaign_discount.toFixed(2)}`}
                  />
                )
              }

              <CarCell
                left={strings.fare_collect_cash.toUpperCase()}
                right={`RM ${total.toFixed(2)}`}
                background={DCColor.BGColor('primary-2')}
                color="rgba(255, 255, 255, 0.75)"
              />
            </View>
            
            {
              !!incentiveAmount && (
                <View style={[fareCont, { marginTop: 8 }]}>
                  <CarCell
                    onPressPromo={() => {
                      $.store.jobs.fareInfoModalVisible = true
                    }}
                    left={strings.bonus_label}
                    right={`+ RM ${incentiveAmount.toFixed(2)}`}
                  />
                </View>
              )
            }

            <View style={notiCont}>
              <Image style={dcIcon} source={Resources.image.attention_icon} />
              <Text style={fCaption}>{strings.fare_message}</Text>
            </View>
          </View>
        </ScrollView>
        <View style={actionCont}>
          <TouchableOpacity
            onPress={this.submit.bind(this)}
            style={dcBtn}
          >
            <Text style={fBtn}>{editor ? strings.confirm.toUpperCase() : strings.fare_collect.toUpperCase()}</Text>
          </TouchableOpacity>
        </View>
        <PromoDetailModel />

        <DCInfoModal
          onHide={() => {
            this.onHideRating()
            this.completeRating()
          }}
          visible={ratingVisible}
          cancelBtn={true}
          {...modalProps}
        >
          <DCUserInfoCont
            avatars={passenger_info && passenger_info.avatars}
            fullName={passenger_info && passenger_info.fullName}
            userId={passenger_info && passenger_info.userId}
          />
        </DCInfoModal>
      </View>
    )
  }
}

const CarCell = (props) => {
  const {
    tCol,
    tLeftCont,
    tableFont,
    promoInfo,
    promoIcon
  } = styles

  return (
    <View style={props.background ? [tCol, { backgroundColor: props.background }] : tCol}>
      <View style={tLeftCont}>
        <Text style={props.color ? [tableFont, { color: props.color, fontWeight: '400' }] : [tableFont, { fontWeight: '400' }]}>{props.left}</Text>
        {
          props.onPressPromo && (
            <TouchableOpacity style={promoInfo} onPress={props.onPressPromo}>
              <Text style={promoIcon}>?</Text>
            </TouchableOpacity>
          )
        }
      </View>
      {
        props.editor ? (
          <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', width: width / 2 - 40, marginHorizontal: 5, backgroundColor: '#ececec', borderRadius: 6, height: 38 }}>
            <Text style={[props.color ? [tableFont, { color: props.color, marginLeft: 10 }] : [tableFont, { marginLeft: 10 }]]}>RM</Text>
            <TextInput
              onChangeText={props.onChangeText}
              placeholder={'0.00'}
              keyboardType="numeric"
              value={props.changValue}
              underlineColorAndroid='transparent'
              holderTextColor={'#dbdbdd'}
              style={{ height: 50, paddingLeft: 8, paddingRight: 10, flex: 1, textAlign: 'right' }}
            />
          </View>
        ) : <Text style={props.color ? [tableFont, { color: props.color }] : tableFont}>{props.right}</Text>
      }
    </View>
  )
}

const styles = StyleSheet.create({
  mainCont: {
    width,
    height: '100%',
    backgroundColor: 'white',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  bodyCont: {
    width,
    padding: 20,
  },
  dcCol: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start'
  },
  fTitle: {
    fontSize: TextFont.TextSize(18),
    fontWeight: '900',
    color: DCColor.BGColor('primary-2')
  },
  dcTag: {
    paddingHorizontal: 10,
    backgroundColor: DCColor.BGColor('primary-1'),
    borderRadius: 50,
    marginLeft: 10,
  },
  fTag: {
    fontSize: TextFont.TextSize(12),
    fontWeight: '500',
    color: DCColor.BGColor('primary-2')
  },
  fareCont: {
    borderWidth: 1,
    borderRadius: 8,
    borderColor: '#ccc',
    overflow: 'hidden'
  },
  tCol: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: 15,
    alignItems: 'center',
    height: 50,
    backgroundColor: '#fff',
    borderBottomWidth: 1,
    borderColor: '#ccc'
  },
  tLeftCont: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    flex: 1
  },
  tableFont: {
    color: '#000',
    fontSize: TextFont.TextSize(12),
    fontWeight: '700',
    marginRight: 15
  },
  promoInfo: { 
    marginLeft: 5, 
    height: 30, 
    width: 30, 
    borderRadius: 70, 
    backgroundColor: DCColor.BGColor('disable'), 
    justifyContent: 'center', 
    alignItems: 'center' 
  },
  promoIcon: {
    fontSize: TextFont.TextSize(12), 
    color: DCColor.BGColor('grey')
  },
  notiCont: {
    marginTop: 8,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.1)',
    borderRadius: 4,
    padding: 10
  },
  dcIcon: {
    width: 30,
    height: 30,
    resizeMode: 'contain',
    marginRight: 5
  },
  fCaption: {
    fontSize: TextFont.TextSize(12),
    fontWeight: '400',
    color: 'rgba(0, 0, 0, 0.65)'
  },
  actionCont: {
    width,
    paddingHorizontal: 20,
    paddingBottom: 40,
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'center'
  },
  dcBtn: {
    width: '100%',
    borderRadius: 4,
    height: 55,
    backgroundColor: DCColor.BGColor('primary-1'),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  fBtn: {
    fontSize: TextFont.TextSize(15),
    fontWeight: '700',
    color: DCColor.BGColor('primary-2')
  }
})