import React, { Component } from 'react'
import { View, Modal, Text, Image, StyleSheet, TouchableOpacity, Linking } from 'react-native'
import { inject, observer } from 'mobx-react'
import Resources from 'dacsee-resources'
import { Screen, DCColor, TextFont } from 'dacsee-utils'

const { width, height } = Screen.window
const styles = StyleSheet.create({
  dcBackDrop: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.75)',
    width,
    height
  },
  scrollCont: {
    width: '100%',
    maxHeight: height * .8
  },
  dcCard: {
    maxHeight: height * .8,
    width: width / 100 * 90,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    shadowOffset: { width: 0, height: 2 },
    shadowColor: 'rgba(0, 0, 0, 0.1)',
    shadowRadius: 3,
    borderRadius: 14,
    backgroundColor: 'white',
    padding: 20
  },
  dcImg: {
    width: 120,
    height: 120,
    resizeMode: 'contain',
    marginBottom: 20
  },
  fTitle: {
    width: '100%',
    textAlign: 'center',
    fontSize: TextFont.TextSize(20),
    fontWeight: '800',
    color: 'rgba(0, 0, 0, 0.75)',
    marginBottom: 10
  },
  bodyCont: {
    width: '100%',
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    marginBottom: 10
  },
  fBody: {
    color: '#777',
    textAlign: 'left',
    fontSize: TextFont.TextSize(16),
    fontWeight: '200',
  },
  actionCont: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  dcBtn: {
    height: 45,
    flex: 2,
    backgroundColor: DCColor.BGColor('primary-1'),
    borderRadius: 8,
    alignItems: 'center',
    justifyContent: 'center'
  },
  fButton: {
    fontSize: TextFont.TextSize(15),
    fontWeight: '600',
    color: 'rgba(0, 0, 0, 0.75)',
    textAlign: 'center'
  },
  cancelBtn: {
    backgroundColor: DCColor.BGColor('disable'),
    marginRight: 15,
    flex: 1
  },
  cLink: {
    color: '#347ab7'
  },
  languageCont: {
    width: '100%',
  },
  dcCol: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    marginBottom: 10
  },
  labelCont: {
    width: '50%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  phoneCont: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  dcIcon: {
    height: 15,
    width: 15,
    resizeMode: 'contain'
  }
})

@inject('app', 'jobs')
@observer
export default class DriverOnlineVerifyView extends Component {

  constructor(props) {
    super(props)
  }

  render() {
    const {
      dcBackDrop,
      dcCard,
      dcImg,
      fTitle,
      bodyCont,
      fBody,
      actionCont,
      dcBtn,
      fButton,
      cancelBtn,
      cLink,
      languageCont,
      dcCol,
      labelCont,
      phoneCont,
      dcIcon
    } = styles
    const { jobs } = this.props
    const visible = jobs.showOnlineVerify

    const { strings } = this.props.app

    return (
      <Modal onRequestClose={() => { }} visible={visible} transparent={true}
        onBackButtonPress={() => jobs.closeOnlineVerifyModal()}
        onBackdropPress={() => jobs.closeOnlineVerifyModal()}
        onRequestClose={() => jobs.closeOnlineVerifyModal()}
      >
        <View style={dcBackDrop}>
          <View style={dcCard}>
            <Image style={dcImg} source={require('../../resources/images/online_verify.png')} />
            <Text style={fTitle}>{strings.upgradeDriver && strings.upgradeDriver.toUpperCase()}</Text>
            <Text style={[bodyCont, { textAlign: 'center', marginBottom: 20 }]}>
              <Text style={fBody}>{strings.upgradeDriverDesc}</Text>
            </Text>
            <Text style={[bodyCont, { textAlign: 'left', marginBottom: 5 }]}>
              <Text style={[fBody, { color: 'rgba(0, 0, 0, 0.75)' }]}>{strings.contact_us_desc_1}</Text>
            </Text>
            <View style={[languageCont, { marginBottom: 20 }]}>
              <View style={dcCol}>
                <View style={labelCont}>
                  <Text style={fBody}>{strings.en} / {strings.chinese}</Text>
                  <Text style={[fBody, { marginRight: 5 }]}>:</Text>
                </View>
                <View style={phoneCont}>
                  <Image style={dcIcon} source={Resources.image.whatsapp} />
                  <Text onPress={() => Linking.openURL('http://wa.me/601115369986')} style={[fBody, cLink, { marginBottom: 0, marginLeft: 10 }]}>+601115369986</Text>
                </View>
              </View>
              <View style={dcCol}>
                <View style={labelCont}>
                  <Text style={fBody}>{strings.mas}</Text>
                  <Text style={[fBody, { marginRight: 5 }]}>:</Text>
                </View>
                <View style={phoneCont}>
                  <Image style={dcIcon} source={Resources.image.whatsapp} />
                  <Text onPress={() => Linking.openURL('http://wa.me/601118509986')} style={[fBody, cLink, { marginBottom: 0, marginLeft: 10 }]}>+601118509986</Text>
                </View>
              </View>
            </View>
            <View style={actionCont}>
              <TouchableOpacity style={[dcBtn, cancelBtn]} onPress={() => jobs.closeOnlineVerifyModal()}>
                <Text style={fButton}>{strings.cancel && strings.cancel.toUpperCase()}</Text>
              </TouchableOpacity>
              <TouchableOpacity style={dcBtn} onPress={() => jobs.closeOnlineVerifyModal(true)}>
                <Text style={fButton}>{strings.upgradeDriver && strings.upgradeDriver.toUpperCase()}</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    )
  }
}
