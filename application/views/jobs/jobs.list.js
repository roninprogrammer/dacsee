import React, { Component } from 'react'
import { Text, View, Image, RefreshControl, FlatList } from 'react-native'
import InteractionManager from 'InteractionManager'
import moment from 'moment'
import { inject, observer } from 'mobx-react'
import OfflineListItem from './components/offline.listItem'
import Schedule from './components/calendars/agenda'
import { Screen, DCColor, TextFont } from 'dacsee-utils'
import Resources from 'dacsee-resources'

const { height } = Screen.window

@inject('app', 'jobs')
@observer
export default class JobsListScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const { strings } = $.store.app
    return {
      drawerLockMode: 'locked-closed',
      title: strings.job_list
    }
  }

  constructor(props) {
    super(props)
    const todayUtc = moment(new Date()).format('YYYY-MM-DD')
    const currentMonth = this.getIntervalMonth('2018-01-01', todayUtc)
    this.state = {
      selectedDate: todayUtc,
      currentMonth: currentMonth
    }
  }

  async componentDidMount() {
    InteractionManager.runAfterInteractions()
    this.props.jobs.getJobList()
  }

  getIntervalMonth(startDate, endStart) {
    startDate = startDate.split('-')
    // 得到月数
    startDate = parseInt(startDate[0]) * 12 + parseInt(startDate[1])
    // 拆分年月日
    endStart = endStart.split('-')
    // 得到月数
    endStart = parseInt(endStart[0]) * 12 + parseInt(endStart[1])
    return Math.abs(startDate - endStart)
  }

  goJobsListDetail(row) {
    this.props.jobs.active_job = row
    this.props.navigation.navigate('JobsListDetail', { type: 'out_job' })
  }
  render() {
    const { jobs } = this.props
    const { getJobList } = jobs
    return (
      <View style={{ flex: 1 }}>
        <Schedule
          onDayPress={day => {
            const dateStr = moment(day.timestamp).toISOString()
            this.setState({
              selectedDate: dateStr
            })
            getJobList(dateStr)
          }}
          selected={this.state.selectedDate}
          pastScrollRange={this.state.currentMonth}
          futureScrollRange={3}
        >
          {/* 渲染job列表 */}
          {this.rederJobsList(this.state.selectedDate)}
        </Schedule>
      </View>
    )
  }
  rederJobsList(dateStr) {
    const { app, jobs } = this.props
    const { loading, getJobList, jobList } = jobs
    const refreshControl = (
      <RefreshControl
        refreshing={loading}
        onRefresh={() => {
          getJobList(dateStr)
        }}
        title={app.strings.pull_refresh}
        colors={['#ffffff']}
        progressBackgroundColor={DCColor.BGColor('primary-1')}
      />
    )
    return (
      <View style={{ marginTop: 104, flex: 1 }}>
        <FlatList
          refreshControl={refreshControl}
          data={jobList}
          enableEmptySections={true}
          ListEmptyComponent={() =>
            <View style={{ height: height - 200, justifyContent: 'center', alignItems: 'center' }} >
              <Image source={Resources.image.joblist_empty} style={{ width: 100, height: 100 }} />
              <Text style={{ marginTop: 20, color: '#777', fontSize: TextFont.TextSize(18), fontWeight: '400' }} >
                {this.props.app.strings.no_job}
              </Text>
            </View>
          }
          keyExtractor={(item, index) => item._id}
          renderItem={(row) => (
            <OfflineListItem
              itemIndex={row.index}
              itemData={row.item}
              onPress={() => this.goJobsListDetail(row.item)}
            />
          )}
        />
      </View>
    )
  }
}
