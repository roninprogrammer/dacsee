import React, { Component } from 'react'
import {
  Text,
  View,
  StyleSheet,
  Platform,
  Linking,
  Alert,
  PixelRatio,
  Image,
  TouchableOpacity
} from 'react-native'
import {
  inject,
  observer
} from 'mobx-react'
import { NavigationActions } from 'react-navigation'
import moment from 'moment'
import ActionSheet from 'react-native-actionsheet'
import {
  Screen,
  Define,
  TextFont,
  DCColor,
  Avatars,
  Icons
} from 'dacsee-utils'
import { Button } from '../../components'
import Resources from 'dacsee-resources'
import { MapView } from '../../components/map'
import CancelJobModel from './components/modal.cancel.job'
import { DVBookingInfoContMin } from './components'
import {
  DCBookingDetailModal,
  DCInfoModal,
  EtaUpdateModal,
  SosModal
} from '../modal'
import HeaderTab from '../../components/header-tab'

const { height, width } = Screen.window

const navigateTo = async (index, destination) => {
  $.store.jobs.navigationOptions = index === 0 ? 'maps' : 'waze'

  if (index === 0) {
    await Linking.openURL(`https://www.google.com/maps/dir/?api=1&destination=${destination.name}&destination_place_id=${destination.placeId}&dir_action=navigate`)
  } else if (index === 1) {
    await Linking.openURL(`https://waze.com/ul?ll=${destination.coords.lat},${destination.coords.lng}&navigate=yes`)
  }
}

const BookingDetailBottomView = (props) => {
  const fare = props.fare
  const getOption = props.getOption
  const strings = props.strings
  const optionObject = props.optionObject
  const chineseStatus = props.chineseStatus
  const payment_method = props.payment_method

  return (
    <View style={[styles.JobDetailWrap, { height: 70, width, backgroundColor: '#fff' }]}>
      <View style={{ height: 1, backgroundColor: '#d7d7d7' }} />
      <View style={{ flexDirection: 'row', flex: 1, justifyContent: 'space-between', alignItems: 'center' }}>
        <View style={{ flexDirection: 'column', justifyContent: 'flex-start', alignItems: 'flex-start', width: width * 0.3, height: 70 }}>
          {/* PAYMENT METHOD */}
          <View style={{ paddingTop: 10, height: 30 }}>
            <View style={{ flexDirection: 'row', flex: 1, justifyContent: 'flex-start' }}>
              <View style={{ marginLeft: 10, flexWrap: 'wrap', height: 20, borderRadius: 80, paddingVertical: 3, paddingHorizontal: 10, backgroundColor: DCColor.BGColor('primary-1'), justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ fontSize: TextFont.TextSize(11), color: 'rgba(0, 0, 0, 0.75)', fontWeight: 'bold' }}>{payment_method === 'Cash' ? strings.cash : payment_method}</Text>
              </View>
            </View>
          </View>
          {/* FARE */}
          <Text style={{ fontSize: 14, height: 40, paddingLeft: 10, color: 'rgba(0, 0, 0, 0.75)' }}>
            RM<Text style={{ fontSize: Platform.OS === 'android' ? (20 / PixelRatio.getFontScale()) : 20, fontWeight: 'bold' }}>{fare.toFixed(2)}</Text>
          </Text>
        </View>
        {getOption ?
          <View style={{ flexDirection: 'row', justifyContent: 'flex-end', width: width * 0.7, paddingRight: 10, paddingVertical: 10 }} >
            {!!optionObject.left &&
              <Button style={{ borderRadius: 5, flex: 1, height: 50, backgroundColor: DCColor.BGColor('red'), marginRight: 5 }} onPress={optionObject.leftAction} >
                <Text style={{ fontSize: TextFont.TextSize(16), fontWeight: 'bold', textAlign: 'center', color: 'white' }}>{optionObject.left && strings[optionObject.left].toUpperCase()}</Text>
              </Button>
            }
            {!!optionObject.right &&
              <Button style={{ borderRadius: 5, flex: 1, height: 50, backgroundColor: DCColor.BGColor('primary-1') }} onPress={optionObject.rightAction}>
                <Text style={{ fontSize: TextFont.TextSize(16), textAlign: 'center', fontWeight: 'bold', color: DCColor.BGColor('primary-2') }} >{optionObject.right && strings[optionObject.right].toUpperCase()}</Text>
              </Button>
            }
          </View>
          : <View style={{ justifyContent: 'center', alignItems: 'center', marginRight: 20 }}>
            <Text style={{ fontSize: TextFont.TextSize(15), fontWeight: 'bold' }}>{chineseStatus}</Text>
          </View>
        }
      </View>
    </View>
  )
}

@inject('app', 'jobs', 'chat')
@observer
export default class JobsListDetailScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const { control } = $.store.app
    const { leftBtn } = navigation.state.params
    const jobDetail = $.store.jobs.active_job
    const navMap = {
      drawerLockMode: 'locked-closed',
      headerTitle: (
        <HeaderTab onlyTitle={false} active={control.header.indexTrip} onPress={index => control.header.tripNavigate(index)}>
          <Image source={Resources.image.logo_landscape_border} style={{ width: 100, height: 60, resizeMode: 'contain', marginRight: 10 }} />
        </HeaderTab>
      )
    }
    const noBackStatuses = [
      'On_The_Way', 'Arrived', 'On_Board',
      'Finalize_Total_Fare', 'Completed_by_Passenger'
    ]

    if (noBackStatuses.includes(jobDetail.status) || leftBtn) {
      navMap.headerLeft = null
      navMap.gesturesEnabled = false
    }
    return navMap
  }

  constructor(props) {
    super(props)
    this.state = {
      route: {
        routeBounds: {
          northEast: { longitude: 0, latitude: 0 },
          southWest: { longitude: 0, latitude: 0 }
        },
        routeCenterPoint: { longitude: 0, latitude: 0 },
        routeLength: 0,
        routeNaviPoint: [],
        routeTime: 0,
        routeTollCost: 0
      },
      mapReady: false,
      showDetailModal: false,
      passengerVisible: false,
      showSosModal: false,
      noShowModal: false,
      setMapToTrackDriver: true
    }
  }

  _getStatus(status) {
    const { _id, fare, additional_fees = 0, fareObj, booking_at } = this.props.jobs.active_job
    const { bookingHandle, cancelHandle } = this.props.jobs
    const { strings } = this.props.app
    let sameday = moment(booking_at).isSame(moment(), 'day')
    const time = moment(booking_at).format(`${sameday ? `[${strings.today}]` : 'YYYY-MM-DD'} HH:mm`)

    switch (status) {
      case 'Pending_Acceptance':
        return {
          left: 'reject',
          right: 'accept',
          leftAction: async () => {
            Alert.alert(strings.reject_order, strings.confirm_reject_order, [
              { text: strings.cancel },
              {
                text: strings.confirm,
                onPress: async () => {
                  this.onHide('showDetailModal')
                  bookingHandle({ action: 'reject' }, _id)
                }
              }
            ])
          },
          rightAction: async () => {
            this.onHide('showDetailModal')
            bookingHandle({ action: 'accept' }, _id)
          }
        }
      case 'Confirmed':
        return {
          left: 'cancel',
          right: 'on_the_way',
          leftAction: async () => {
            this.onHide('showDetailModal')
            cancelHandle(_id)
          },
          rightAction: async () => {
            Alert.alert(strings.reminder, `This is an advance booking for ${time}, Are you sure you are heading there?`,
              [
                { text: strings.cancel },
                {
                  text: strings.continue_,
                  onPress: async () => {
                    this.onHide('showDetailModal')
                    this.props.navigation.setParams({ leftBtn: true })
                    bookingHandle({ action: 'on_the_way' }, _id)
                  }
                }
              ])
          }
        }
      case 'On_The_Way':
        return {
          left: 'cancel',
          right: 'Arrived',
          leftAction: async () => {
            this.onHide('showDetailModal')
            cancelHandle(_id)
          },
          rightAction: async () => {
            Alert.alert(strings.arrive_place, strings.touch_continue_notice_passengers, [
              { text: strings.cancel },
              {
                text: strings.continue_,
                onPress: async () => {
                  this.onHide('showDetailModal')
                  bookingHandle({ action: 'arrived' }, _id)
                }
              }
            ])
          }
        }
      case 'Arrived':
        return {
          left: 'No_Show',
          right: 'On_Board',
          leftAction: () => {
            this.onHide('showDetailModal')
            this.onShow('noShowModal')
          },
          rightAction: () => {
            Alert.alert(strings.start_trip, strings.touch_continue_start_trip, [
              { text: strings.cancel },
              {
                text: strings.continue_,
                onPress: async () => {
                  this.onHide('showDetailModal')
                  bookingHandle({ action: 'on_board' }, _id)
                }
              }
            ])
          }
        }
      case 'On_Board':
        return {
          // left: 'cancel',
          right: 'Drop_Off',
          // leftAction: () => {
          //   this.onHide('showDetailModal')
          //   cancelHandle(_id)
          // },
          rightAction: () => {
            Alert.alert(strings.finish_trip, strings.arrive_touch_finish, [
              { text: strings.cancel },
              {
                text: strings.continue_,
                onPress: async () => {
                  this.onHide('showDetailModal')
                  this.props.navigation.navigate('JobsFare', { _id, fare, fareObj })
                }
              }
            ])
          }
        }
      case 'Finalize_Total_Fare':
      case 'Completed_by_Passenger':
        return {
          right: 'Drop_Off',
          leftAction: () => { },
          rightAction: () => {
            this.onHide('showDetailModal')
            this.props.navigation.navigate('JobsFare', { _id, fare, additional_fees, status, isSubmit: true, fareObj })
          }
        }
      default:
        return {}
    }
  }

  _getOptionable(status) {
    const validStatuses = [
      'Pending_Acceptance', 'On_The_Way', 'Arrived', 'On_Board',
      'Confirmed', 'Finalize_Total_Fare', 'Completed_by_Passenger'
    ]
    return status && validStatuses.includes(status)
  }

  _statusInChinese(str) {
    const { strings } = this.props.app
    switch (str) {
      case 'Pending_Acceptance':
        return strings.Pending_Acceptance
      case 'On_The_Way':
        return strings.On_The_Way
      case 'Arrived':
        return strings.Arrived
      case 'No_Show':
        return strings.No_Show
      case 'On_Board':
        return strings.On_Board
      case 'Completed':
        return strings.Completed
      case 'Cancelled_by_Passenger':
        return strings.Cancelled_by_Passenger
      case 'Cancelled_by_System':
        return strings.Cancelled_by_System
      case 'Cancelled_by_Driver':
        return strings.Cancelled_by_Driver
      case 'Rejected_by_Driver':
        return strings.Rejected_by_Driver
      case 'No_Taker':
        return strings.No_Taker
    }
  }

  componentDidMount() {
    const { jobs } = this.props
    const { active_job = {} } = jobs
    const { status = 'Pending_Acceptance', fareObj } = active_job

    const { promo, campaign_discount = 0 } = fareObj

    if ((!!promo || !!campaign_discount) && ['CONFIRMED', 'ON_THE_WAY', 'ARRIVED', 'ON_BOARD'].indexOf(status.toUpperCase()) > -1) {
      $.store.jobs.fareInfoModalVisible = true
    }
  }

  componentWillUnmount() {
    this.dispose && this.dispose()
  }

  moveCurrentLocation(coords, zoom) {
    if (this.state.mapReady && this.map && this.map.handle) {
      this.map.handle.moveTo(coords, zoom)
    }
  }

  componentWillReceiveProps(nextProps) {
    const { jobs, app } = nextProps
    const { status } = jobs.active_job
    if (status === 'On_The_Way' || status === 'On_Board') {
      const coords = {
        latitude: app.coords.lat ? app.coords.lat : app.coords.latitude,
        longitude: app.coords.lng ? app.coords.lng : app.coords.longitude
      }
      this.moveCurrentLocation(coords)
    }
  }

  onMapReady() {
    this.setState({ mapReady: true })
    const { jobs, app } = this.props
    const { active_job = {} } = jobs
    const { status = 'Pending_Acceptance', from, destination, distance } = active_job
    const target = status === 'Pending_Acceptance' ? from.coords : app.coords
    const to = status === 'Pending_Acceptance' ? destination.coords : from.coords
    const FromCoords = {
      latitude: target.lat ? target.lat : target.latitude,
      longitude: target.lng ? target.lng : target.longitude
    }
    const DestinationCoords = {
      latitude: to.lat ? to.lat : to.latitude,
      longitude: to.lng ? to.lng : to.longitude
    }

    this.moveCurrentLocation($.method.utils.getRegion([FromCoords, DestinationCoords]), $.method.utils.mathDistanceZoom(distance))
  }

  onHide = (modal) => {
    this.setState({
      [modal]: false
    })
  }

  onShow = (modal) => {
    this.setState({
      [modal]: true
    })
  }

  onRecenter = () => {
    const { app } = this.props
    const coords = {
      latitude: app.coords.lat ? app.coords.lat : app.coords.latitude,
      longitude: app.coords.lng ? app.coords.lng : app.coords.longitude
    }
    this.moveCurrentLocation(coords)
    this.setState({
      setMapToTrackDriver: true
    })
  }

  render() {
    const {
      sosBtn,
      fBtn,
      recenterBtn
    } = styles
    const { app, jobs, chat } = this.props
    const { strings } = app
    const { totalUnread } = chat
    const { active_job = {}, actualFrom, bookingHandle } = jobs
    const { destination, from, payment_method, group_info, campaign_info, assign_type, passenger_info, notes, type, fareObj, code, distance, booking_at, _id, driverETA } = active_job
    const { mapReady = false, showDetailModal, passengerVisible, showSosModal, noShowModal, setMapToTrackDriver } = this.state
    const { status = 'Pending_Acceptance' } = active_job
    const optionObject = this._getStatus(status)
    const getOption = this._getOptionable(status)
    const chineseStatus = this._statusInChinese(status)
    let extraMapProps = {}

    // Advance Booking
    let sameday = moment(booking_at).isSame(moment(), 'day')


    const bookingInit = active_job.status.toUpperCase() === 'PENDING_ACCEPTANCE'
    const showPickup = active_job.status.toUpperCase() === 'CONFIRMED' || active_job.status.toUpperCase() === 'ON_THE_WAY' || active_job.status.toUpperCase() === 'ARRIVED'
    const showDropoff = active_job.status.toUpperCase() === 'CONFIRMED' || active_job.status.toUpperCase() === 'ON_BOARD'
    const friendData = Object.assign({}, { friend_info: Object.assign({}, { _id: passenger_info._id }, passenger_info) }, { friend_id: passenger_info._id })

    // TODO refactor fare calculation part
    let { additional_fees = 0 } = active_job
    let { amount = 0, promo_discount = 0, originalAmount = 0, promo, campaign_discount = 0 } = fareObj
    let total = 0

    additional_fees = parseFloat(additional_fees)
    originalAmount = parseFloat(originalAmount)
    promo_discount = parseFloat(promo_discount)
    campaign_discount = parseFloat(campaign_discount)
    const fare = !!originalAmount ? originalAmount : parseFloat(amount)
    total = fare + additional_fees - promo_discount - campaign_discount

    // TODO End
    if (actualFrom && jobs.destination && app.coords.latitude !== 0 && app.coords.longitude !== 0 && mapReady) {
      extraMapProps = {
        ...extraMapProps,
        initialRegion: {
          ...app.coords,
          latitudeDelta: 0.014,
          longitudeDelta: 0.014 * (width / height)
        },
        from: actualFrom,
        destination: destination
      }
    }

    return (
      <View style={{ flex: 1, position: 'relative' }}>
        <TouchableOpacity style={sosBtn} onPress={() => this.onShow('showSosModal')}>
          <Text style={fBtn}>SOS</Text>
        </TouchableOpacity>
        <MapView
          routes={jobs.bookingPath.routes}
          showNavigator={false}
          region_type={app.region}
          style={{ flex: 1, height, width }}
          ref={e => { this.map = e }}
          onMapReady={() => this.onMapReady()}
          onMoveShouldSetResponder={(e) => { setTimeout(() => { this.setState({ setMapToTrackDriver: false }) }, 750) }}
          {...extraMapProps}
        />
        <View>
          {!setMapToTrackDriver ?
            <TouchableOpacity style={recenterBtn} onPress={() => this.onRecenter()}>
              {Icons.Generator.Awesome('map-marker', 20, '#999')}
            </TouchableOpacity>
            : null
          }
          <DVBookingInfoContMin
            bookingStatus={active_job.status}
            bookingETA={active_job.status.toUpperCase() === 'ON_THE_WAY' ? driverETA && driverETA.duration : null}
            bookingETAUpdate={active_job.status.toUpperCase() === 'ON_THE_WAY' ? () => $.store.jobs.etaUpdateModal = true : null}
            expandFunction={!bookingInit ? () => this.onShow('showDetailModal') : null}
            psAvatar={!bookingInit ? passenger_info && passenger_info.avatars : null}
            psAvatarFunction={!bookingInit ? () => this.onShow('passengerVisible') : null}
            psName={!bookingInit ? passenger_info && passenger_info.fullName : null}
            psId={!bookingInit ? passenger_info && passenger_info.userId : null}
            psRating={!bookingInit && passenger_info && passenger_info.rating ? passenger_info.rating : null}
            messageFunction={!bookingInit ? () => global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'ChatWindow', params: { friendData, chatType: 'friend' } })) : null}
            unreadChat={totalUnread}
            callFunction={!bookingInit ? () => passenger_info && Linking.openURL(`tel:${passenger_info.phoneCountryCode}${passenger_info.phoneNo}`) : null}
            advTime={type === 'advance' && bookingInit ? moment(booking_at).format('hh:mm A') : null}
            advDate={type === 'advance' && bookingInit ? sameday ? strings.today : moment(booking_at).format('ddd, DD MMM') : null}
            estDistance={type !== 'advance' ? bookingInit ? (distance / 1000).toFixed(1) : null : null}
            bookingId={code}
            pickupAddress={bookingInit || showPickup ? from.name !== 'Pin Location' ? `${from.name}, ${from.address}` : from.address : null}
            pickupNavigateFunction={!bookingInit ? () => {
              if (showPickup) {
                this.fromAction.show()
              } else return null
            } : null}
            dropoffAddress={bookingInit || showDropoff ? `${destination.name}, ${destination.address}` : null}
            dropoffNavigateFunction={!bookingInit ? () => showDropoff ? this.destinationAction.show() : null : null}
            campaignName={assign_type !== 'selected_circle' ? group_info.name : strings.circle_of_friend}
            campaignImg={assign_type !== 'selected_circle' ? group_info && group_info.avatars ? group_info && group_info.avatars : campaign_info && campaign_info.avatarws : null}
            driverNote={notes && notes !== '' ? notes : strings.note_to_driver}
          />
          <BookingDetailBottomView
            fare={total}
            getOption={getOption}
            strings={strings}
            optionObject={optionObject}
            chineseStatus={chineseStatus}
            additional_fees={additional_fees}
            promo={promo}
            campaign={!!campaign_discount}
            payment_method={payment_method}
          />
        </View>
        <DCBookingDetailModal
          visible={showDetailModal}
          bookingETA={active_job.status.toUpperCase() === 'ON_THE_WAY' ? driverETA && driverETA.duration : null}
          bookingETAUpdate={active_job.status.toUpperCase() === 'ON_THE_WAY' ? () => $.store.jobs.etaUpdateModal = true : null}
          onHide={() => this.onHide('showDetailModal')}
          bookingStatus={active_job.status}
          advTime={type === 'advance' ? moment(new Date(booking_at)).format('hh:mm A') : null}
          advDate={type === 'advance' ? sameday ? strings.today : moment(booking_at).format('ddd, DD MMM') : null}
          avatar={passenger_info ? passenger_info.avatars : null}
          name={passenger_info ? passenger_info.fullName : null}
          userId={passenger_info ? passenger_info.userId : null}
          rating={passenger_info && passenger_info.rating ? passenger_info.rating : null}
          messageFunction={() => {
            this.onHide('showDetailModal')
            global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'ChatWindow', params: { friendData, chatType: 'friend' } }))
          }}
          unreadChat={totalUnread}
          callFunction={() => passenger_info && Linking.openURL(`tel:${passenger_info.phoneCountryCode}${passenger_info.phoneNo}`)}
          bookingId={code}
          pickupAddress={from.name !== 'Pin Location' ? `${from.name}, ${from.address}` : from.address}
          pickupNavigateFunction={() => {
            if (type === 'advance') {
              let sameday = moment(booking_at).isSame(moment(), 'day')
              const time = moment(booking_at).format(`${sameday ? `[${strings.today}]` : 'YYYY-MM-DD'} HH:mm`)

              Alert.alert(
                strings.reminder,
                `This is an advance booking for ${time}, Are you sure you are heading there?`,
                [
                  {
                    text: strings.cancel
                  }, {
                    text: strings.continue_,
                    onPress: async () => {
                      this.fromAction.show()
                    }
                  }
                ]
              )
            } else {
              this.fromAction.show()
            }
          }
          }
          dropoffAddress={`${destination.name}, ${destination.address}`}
          dropoffNavigateFunction={() => this.destinationAction.show()}
          campaignName={assign_type !== 'selected_circle' ? group_info.name : strings.circle_of_friend}
          campaignImg={assign_type !== 'selected_circle' ? group_info && group_info.avatars ? group_info && group_info.avatars : campaign_info && campaign_info.avatarws : null}
          driverNote={notes && notes !== '' ? notes : strings.note_to_driver}
          price={total}
          paymentType={payment_method}
          rejectBtnLabel={optionObject ? optionObject.left && strings[optionObject.left].toUpperCase() : null}
          rejectFunction={optionObject ? optionObject.leftAction : null}
          acceptBtnLabel={optionObject ? optionObject.right && strings[optionObject.right].toUpperCase() : null}
          acceptFunction={optionObject ? optionObject.rightAction : null}
        />
        <ActionSheet
          ref={o => { this.fromAction = o }}
          title={strings.navigation}
          message={strings.please_make_sure_installed}
          options={[strings.google_maps, 'Waze', strings.cancel]}
          cancelButtonIndex={2}
          onPress={(index) => {
            $.store.jobs.scheduleEtaUpdate()
            navigateTo(index, from)
          }}
        />
        <ActionSheet
          ref={o => { this.destinationAction = o }}
          title={strings.navigation}
          message={strings.please_make_sure_installed}
          options={[strings.google_maps, 'Waze', strings.cancel]}
          cancelButtonIndex={2}
          onPress={(index) => navigateTo(index, destination)}
        />
        <DCInfoModal
          onHide={() => this.onHide('passengerVisible')}
          confirmBtn={app.strings.okay}
          onPress={() => this.onHide('passengerVisible')}
          visible={passengerVisible}
          label={passenger_info && passenger_info.fullName}
          caption={passenger_info && passenger_info.userId}
          rating={passenger_info && passenger_info.rating ? passenger_info.rating : null}
        >
          <Image
            resizeMode="cover"
            style={{ position: 'absolute', top: 0, left: 0, bottom: 0, right: 0 }}
            source={{ uri: Avatars.getHeaderPicUrl(passenger_info && passenger_info.avatars, true) }}
          />
        </DCInfoModal>

        <EtaUpdateModal />
        <CancelJobModel />

        <DCInfoModal
          onHide={() => this.onHide('noShowModal')}
          visible={noShowModal}
          label={strings.No_Show}
          caption={strings.passenger_no_show_modal_caption}
          errorBtn={strings.No_Show}
          cancelBtn
          onPress={() => {
            this.props.navigation.goBack()
            bookingHandle({ action: 'no_show' }, _id)
          }
          }
        >
          <View style={{ width: '100%', padding: 20 }}>
            <Image style={{ width: '100%', resizeMode: 'contain' }} source={Resources.image.system_cause_error} />
          </View>
        </DCInfoModal>
        <SosModal
          close={() => this.onHide('showSosModal')}
          visible={showSosModal}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  JobDetailWrap: Platform.select({
    ios: { paddingBottom: Define.system.ios.x ? 22 : 0 },
    android: { paddingBottom: 0 }
  }),
  circle: {
    height: 20,
    borderRadius: 10,
    position: 'absolute',
    backgroundColor: '#e4393c',
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 999,
    top: 4,
    left: 20
  },
  sosBtn: {
    position: 'absolute',
    top: 10,
    right: 10,
    padding: 5,
    paddingHorizontal: 25,
    backgroundColor: DCColor.BGColor('red'),
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 80,
    borderWidth: 5,
    borderColor: 'white',
    shadowOffset: { width: 0, height: -1 },
    shadowColor: '#000',
    shadowOpacity: 0.05,
    elevation: 2,
    zIndex: 999
  },
  fBtn: {
    fontSize: TextFont.TextSize(15),
    fontWeight: '700',
    color: 'white'
  },
  recenterBtn: {
    position: 'absolute',
    top: -45,
    right: 10,
    width: 35,
    height: 35,
    borderRadius: 8,
    backgroundColor: 'white',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    shadowOffset: { width: 0, height: -1 },
    shadowColor: '#000',
    shadowOpacity: 0.05,
    elevation: 4
  },
})
