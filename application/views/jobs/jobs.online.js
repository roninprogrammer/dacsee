import React, { Component } from 'react'
import { Text, View, Image, TouchableOpacity, Alert, FlatList, Switch, SafeAreaView, StyleSheet, Button, ImageBackground, Animated, Platform } from 'react-native'
import moment from 'moment'
import { observable } from 'mobx'
import { inject, observer } from 'mobx-react'
import Material from 'react-native-vector-icons/MaterialIcons'
import Lottie from 'lottie-react-native'
import OnlineVerifyModal from './jobs.modal.online.verify'
import { Screen, TextFont, DCColor } from 'dacsee-utils'
import Resources from 'dacsee-resources'
import OnlineListItem from './components/online.listItem'
import { MapView } from '../../components/map'
import JobCardIOS from '../modal/modal.jobcardIOS'

const { height, width } = Screen.window
let locationData = {}
const styles = StyleSheet.create({
  container: {
    flex: 0.3,
    position: 'relative'
  },
  map: {
    flex: 1
  },
  TabCont: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    height: 45,
    backgroundColor: 'white',
    flexDirection: 'row'
  },
  TabItem: {
    flex: 1,
    height: 45,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
    flexDirection: 'row'
  },
  TabText: {
    fontSize: TextFont.TextSize(15),
    color: 'rgba(0, 0, 0, 0.45)',
    textAlign: 'center',
    fontWeight: '400'
  },
  ActiveText: {
    fontSize: TextFont.TextSize(15),
    textAlign: 'center',
    fontWeight: '900',
    color: 'rgba(0, 0, 0, 0.75)',
  },
  UnderLine: {
    width: '100%',
    height: 3,
    backgroundColor: DCColor.BGColor('primary-1'),
    position: 'absolute',
    bottom: 0,
    left: 0
  },
  circle: {
    width: 10,
    height: 10,
    borderRadius: 10,
    backgroundColor: 'red',
    marginLeft: 10
  },
  square: {
    justifyContent: 'center',
    flexDirection: 'column',
    alignItems: 'center'
  },
  textBox: {
    padding: 8,
    backgroundColor: 'white',
    borderRadius: 5,
    borderWidth: 1,
    borderColor: 'rgba(0, 0, 0, 0.45)',
    marginBottom: 3
  },
  pinText: {
    fontSize: TextFont.TextSize(12),
    fontWeight: '600',
    color: 'rgba(0, 0, 0, 0.75)',
    textAlign: 'center'
  },
  valueText: {
    fontSize: TextFont.TextSize(18),
    color: DCColor.BGColor('primary-2'),
    fontWeight: 'bold'
  },
  headerValueText: {
    fontSize: TextFont.TextSize(18),
    color: DCColor.BGColor('primary-1'),
    fontWeight: 'bold'
  },
  text: {
    fontSize: TextFont.TextSize(10),
    color: DCColor.BGColor('primary-2'),
    fontWeight: '500'
  },
  headertext: {
    fontSize: TextFont.TextSize(10),
    color: '#00000080',
    fontWeight: '500'
  },
  advance: {
    height: 32,
    backgroundColor: DCColor.BGColor('primary-1'),
    marginTop: 5,
    justifyContent: 'center',
    alignItems: 'flex-start',
    marginLeft: 5,
    marginRight: 5,
    paddingHorizontal: 10,
    borderTopLeftRadius: 2,
    borderTopRightRadius: 2
  }
});

let zoomLoc = { latitudeDelta: .014, longitudeDelta: .014 }
@inject('app', 'vehicles', 'jobs', 'account', 'passenger')
@observer
export default class JobsOnlineScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: false,
      arr: [],
      scrolllist: true,
      selectedTab: 'myJob',
      showDetails: true,
      showAdvanced: true,
      zoom: false,
      showModal: true,
      index: 0,
    }
    this.radar = new Animated.Value(0)
    this.marker = {
      latitude: 3.0327,
      longitude: 101.6188,
    }
    this.getCurrentLoc = false
    this.sound = null
  }
  @observable getCurrentLoc: Boolean = false
  componentWillMount() {
    $.store.jobs.getDriverDashboard()
    $.store.wallet.getDriverCreditWallet()
  }

  componentDidMount() {
    Animated.loop(Animated.timing(this.radar, { toValue: 1, fromValue: 0, duration: 2500 })).start()
  }

  componentDidUpdate(prevProps) {
    Animated.loop(Animated.timing(this.radar, { toValue: 1, fromValue: 0, duration: 2500 })).start()
  }

  componentWillUnmount() {
    this.subscription && this.subscription.remove()
  }

  sliderScorll(start) {
    this.setState({
      scrolllist: start
    })
  }

  goJobsListDetail(row) {
    this.props.navigation.navigate('JobsListDetail', { jobDetail: row })
  }

  _ListEmptyComponent = () => {
    return (
      <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
        <Image source={Resources.image.joblist_empty} />
        <Text style={{ color: '#999', fontSize: TextFont.TextSize(18), fontWeight: '600', textAlign: 'center', marginTop: 20 }}>
          {this.props.app.strings.wait_for_order}
        </Text>
        <Text style={{ color: '#999', fontSize: TextFont.TextSize(18), fontWeight: '600', textAlign: 'center', marginTop: 20 }}>
          {this.props.app.strings.last_location_update}
        </Text>
        <Text style={{ color: '#999', fontSize: TextFont.TextSize(16), fontWeight: '400', textAlign: 'center', marginTop: 20 }}>
          {moment(this.props.jobs.lastLocationUpdate.on).format('YYYY-MM-DD HH:mm:ss')}
        </Text>
      </View>
    )
  }
 
  handleTab = (selectedTab) => {
    this.setState({
      selectedTab
    })
  }

  detailShow() {
    if (this.state.showDetails === true) {
      this.setState({ showDetails: false });
    }
    else {
      this.setState({ showDetails: true });
    }
  }
  advancedShow() {
    if (this.state.showAdvanced === true) {
      this.setState({ showAdvanced: false });
    }
    else {
      this.setState({ showAdvanced: true });
    }
  }

  moveCurrentLocation() {
    locationData = $.store.jobs.lastLocationUpdate.coords
  }

  showModalOption(await_jobs = []){
    if(await_jobs){
      $.store.jobs.gotNewJob = true;
    }
  }

  render () {
    const { app, jobs, account, passenger } = this.props
    const { working, await_jobs, advanceBooking, workingChange } = jobs
    const { strings } = app
    const { isElite = false } = account
    const { selectedTab } = this.state

    locationData = jobs.lastLocationUpdate.coords
    let Loc = {
      latitude: locationData ? locationData.latitude : 0,
      longitude: locationData ? locationData.longitude : 0,
      latitudeDelta: zoomLoc.latitudeDelta,
      longitudeDelta: zoomLoc.longitudeDelta
    }

    return (
      <SafeAreaView style={{ flex: 1 }}>
        <View style={{ flex: 1 }}>
          <MapView
            scrollEnabled={false}
            zoomEnabled={false}
            region={Loc}
            origin={null}
            destination={null}
            showNavigator={false}
            showRouter={false}
            region_type={app.region}
            style={{ flex: 1, height, width }}
            ref={e => { passenger.mapRefs = e }}
          />

          {
            working === false ? (
              <View style={{ flex: 1, height, width, backgroundColor: '#00000070', position: 'absolute' }}></View>
            ) : null
          }

          <View style={{ flex: 1, position: 'absolute', height: 50, width: width }}>
            <View style={{ height: 50, paddingHorizontal: 20, paddingTop: 3, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', backgroundColor: 'rgba(255, 255, 255, 1)', elevation: 1 }}>
              <View style={{ alignItems: 'center' }}>
                <Text style={styles.headerValueText}>{$.store.jobs.driverDashboard.weeklyOnlineHours < 1 ? '< 1' : $.store.jobs.driverDashboard.weeklyOnlineHours}</Text>
                <Text style={styles.headertext}>{$.store.app.strings.weekly_online}</Text>
              </View>
              <View style={{ alignItems: 'center' }}>
                <Text style={styles.headerValueText}>{isNaN($.store.wallet.driverCreditWallet.availableAmount) ? 0 : parseFloat($.store.wallet.driverCreditWallet.availableAmount).toFixed(2)}</Text>
                <Text style={styles.headertext}>{$.store.app.strings.credit_balance_driver}</Text>
              </View>
              <View style={{ alignItems: 'center' }}>
                <Text style={styles.headerValueText}>{$.store.jobs.driverDashboard.weeklyCompleted}</Text>
                <Text style={styles.headertext}>{$.store.app.strings.job_completed}</Text>
              </View>
            </View>
            {
              working === true ? (
                <View style={{ position: 'absolute', opacity: 0.7, top: ($.define.screen.content_height - 135) / 2, left: (width - 68) / 2 }}>
                  <Lottie style={{ width: 68, height: 68 }} progress={this.radar} source={require('../../res/animation/radar.json')} />
                </View>
              ) : null
            }

            {
              working === true ? (
                <View style={{ position: 'absolute', top: ($.define.screen.content_height) / 2 - 85, left: (width - 46) / 2 }}>
                  <View style={{ shadowOffset: { width: 0, height: 1 }, shadowColor: '#000', shadowOpacity: 0.15 }}>
                    <ImageBackground style={[
                      { width: 46, height: 52, alignItems: 'center' }]} source={require('../../res/images/pick-pin.png')} >
                      <Image style={[
                        { width: 40, height: 40, borderRadius: 20, top: 3 }
                      ]} source={{ uri: $.method.utils.getHeaderPicUrl(account.user.avatars) }} />
                    </ImageBackground>
                  </View>
                </View>
              ) : null
            }
            <View>
              <View>
                <View>
                {
                  selectedTab === 'myJob' && (await_jobs.length > 0) ? (
                    <View style={styles.advance}>
                      <Text>{$.store.app.strings.incoming_booking}</Text>
                    </View>
                  ) : null
                }

                {
                  selectedTab === 'myJob' && (await_jobs.length) > 0 && Platform.OS === 'ios' ? (
                    <FlatList
                      data={(await_jobs || [])}
                      keyExtractor={(item) => item._id}
                      renderItem={(row) =>
                        <TouchableOpacity
                          onPress={() => {
                            this.setState({
                              index: row.index
                            }); 
                            this.showModalOption(await_jobs);
                        }}
                          activeOpacity={0.7}style={{ marginTop: row.index === 0 ? 0 : 0 }}>
                          <OnlineListItem itemData={row.item}/>
                        </TouchableOpacity>}
                    />
                  ) : selectedTab === 'myJob' && (await_jobs.length) > 0 && Platform.OS === 'android' && (
                    <FlatList
                      data={(await_jobs || [])}
                      keyExtractor={(item) => item._id}
                      renderItem={(row) =>
                        <TouchableOpacity
                          onPress={() => {
                            this.props.jobs.active_job = row.item
                            this.props.navigation.navigate('JobsListDetail', { type: 'active_booking' })
                          }}
                          activeOpacity={0.7}style={{ marginTop: row.index === 0 ? 0 : 0 }}>
                          <OnlineListItem itemData={row.item} sliderScorll={(start) => this.sliderScorll(start)} />
                          {/* // sliderChange={(status) => this.sliderChange(status, row._id)} strings={strings} */}
                        </TouchableOpacity>}
                    /> 
                  )
                  //( selectedTab === 'myJob' && working && this._ListEmptyComponent()) 
                }
                </View>

                {
                  selectedTab === 'myJob' && (await_jobs.length > 0 ) && Platform.OS === 'ios' && $.store.jobs.gotNewJob === true ? (
                    <JobCardIOS 
                      // visible={this.state.showModal}
                      visible={true}
                      index={this.state.index}
                      data={(await_jobs || [])}
                      onClose={(item) => {
                        this.props.jobs.active_job = item  
                        this.props.jobs.bookingHandle({ action: 'reject' }, this.props.jobs.active_job._id)
                      }}
                      onNavigate={(item) => {
                        this.props.jobs.bookingHandle({ action: 'accept' }, item._id)
                        this.setState({index: 0})
                        $.store.jobs.gotNewJob = false
                      }}
                    /> 
                  ) : null
                }
              </View>
              <View>
                {
                  selectedTab === 'myJob' && (advanceBooking.length) > 0 ? (
                    <View style={[styles.advance, { flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }]}>
                      <Text>{$.store.app.strings.you_have} {$.store.jobs.advanceBooking.length} {$.store.app.strings.advance_booking.toUpperCase()}</Text>
                      <TouchableOpacity onPress={() => this.advancedShow()}>
                        <Material name={this.state.showAdvanced === true ? 'keyboard-arrow-up' : 'keyboard-arrow-down'} size={25} />
                      </TouchableOpacity>
                    </View>
                  ) : null
                }

                {
                  selectedTab === 'myJob' && this.state.showAdvanced === true && (advanceBooking.length) > 0 ? (
                    <FlatList
                      flashScrollIndicators
                      style={{ height: height / 3 }}
                      showsHorizontalScrollIndicator={false}
                      data={(advanceBooking || [])}
                      keyExtractor={(item) => item._id}
                      renderItem={(row) =>
                        <TouchableOpacity
                          onPress={() => {
                            this.props.jobs.active_job = row.item
                            this.props.navigation.navigate('JobsListDetail', { type: 'active_booking' })
                          }}
                          activeOpacity={0.7} style={{ marginTop: row.index === 0 ? 0 : 0 }}>
                          <OnlineListItem itemData={row.item} sliderScorll={(start) => this.sliderScorll(start)} />
                          {/* // sliderChange={(status) => this.sliderChange(status, row._id)} strings={strings} */}
                        </TouchableOpacity>}
                    />
                  ) : null //( selectedTab === 'myJob' && working && this._ListEmptyComponent()) 
                }
              </View>
            </View>
          </View>
          <View style={{ width: 32, height: 32, flexDirection: 'row', marginLeft: 6, marginRight: 6, marginBottom: 3, bottom: this.state.showDetails === true ? 160 : 44, alignItems: 'center' }}>
            <View style={{ alignItems: 'flex-start' }}>
              {this.state.showDetails === true ? <Image style={{ marginLeft: 3, width: 55, height: 16 }} source={Resources.image.google_icon} /> : null}
            </View>
          </View>
          <View style={{ flex: 1, position: 'absolute', paddingHorizontal: 20, paddingTop: 7, height: this.state.showDetails === true ? 160 : 44, bottom: 0, width: width, alignItems: 'center', backgroundColor: DCColor.BGColor('primary-1'), borderTopLeftRadius: 15, borderTopRightRadius: 15 }}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 20, width: width }}>
              {
                isElite ? (
                  <View style={{ justifyContent: 'center', paddingLeft: 5 }}>
                    <Text style={{ color: '#fff', fontWeight: '600' }}>ELITE DRIVER</Text>
                  </View>
                ) : (
                  <Image source={Resources.image.logo_landscape_border} style={{ width: 80, height: 30, resizeMode: 'contain', marginRight: 10, justifyContent: 'center' }} />
                )
              }

              <View style={{ position: 'relative', paddingLeft: 20 }}>
                <TouchableOpacity onPress={() => this.detailShow()}>
                  <Material name={this.state.showDetails === true ? 'keyboard-arrow-down' : 'keyboard-arrow-up'} size={30} color={DCColor.BGColor('primary-2')} />
                </TouchableOpacity>
              </View>
              <View style={{ height: 30, width: 120, backgroundColor: 'rgba(0, 0, 0, .1)', borderRadius: 15, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                <Text style={{ color: DCColor.BGColor('primary-2'), paddingLeft: 10, paddingRight: 5, fontWeight: 'bold' }}>{working ? $.store.app.strings.online_job : $.store.app.strings.offline_job}</Text>
                <Switch
                  trackColor='#cdcdcd'
                  thumbColor={working ? Platform.OS === 'ios' ? '#ffffff' : DCColor.BGColor('green') : '#ffffff'}
                  style={Platform.OS === 'ios' ? { backgroundColor: '#cdcdcd', borderRadius: 17 } : { borderWidth: 1 }}
                  value={working} onValueChange={(working) => {
                    if (this.props.vehicles.vehiclesData.length === 0) {
                      Alert.alert('', strings.first_add_your_vehicle, [{ text: strings.confirm }])
                      // global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'VehicleAdd'}))
                    } else {
                      workingChange(working)
                    }
                  }}
                />
              </View>
            </View>
            {
              this.state.showDetails === true ? (
                <View style={{ height: 130, width: width, paddingTop: 5, paddingHorizontal: 20 }}>
                  <View style={{ flexDirection: 'row', justifyContent: 'space-between', backgroundColor: 'rgba(0, 0, 0, .1)', height: 110, paddingHorizontal: 20, borderRadius: 7 }}>
                    <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                      <Image source={Resources.image.rating_icon} style={{ width: 50, height: 50, resizeMode: 'contain' }} />
                      {$.store.jobs.driverDashboard.recentRatingAvg > 0 ? <Text style={styles.valueText}>{$.store.jobs.driverDashboard.recentRatingAvg}</Text> : <Text style={styles.valueText}>{$.store.jobs.driverDashboard.recentRatingAvg}</Text>}
                      <Text style={styles.text}>{$.store.app.strings.average_rating}</Text>
                    </View>
                    <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                      <Image source={Resources.image.miss_icon} style={{ width: 50, height: 50, resizeMode: 'contain' }} />
                      <Text style={styles.valueText}>{$.store.jobs.driverDashboard.weeklyMissed}</Text>
                      <Text style={styles.text}>{$.store.app.strings.job_missed}</Text>
                    </View>
                    <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                      <Image source={Resources.image.cancel_icon} style={{ width: 50, height: 50, resizeMode: 'contain' }} />
                      <Text style={styles.valueText}>{$.store.jobs.driverDashboard.recentCancellationPct}</Text>
                      <Text style={styles.text}>{$.store.app.strings.cancel_percentage}</Text>
                    </View>
                  </View>
                </View>
              ) : null
            }
          </View>
          <OnlineVerifyModal />
        </View>
      </SafeAreaView>
    )

  }
}

