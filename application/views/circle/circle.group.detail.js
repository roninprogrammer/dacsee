import React, { Component } from 'react'
import { Text, View, TouchableOpacity, Image, StyleSheet, } from 'react-native'
import { inject, observer } from 'mobx-react'
import _ from 'lodash'
import { Icons, Screen, Define, TextFont, DCColor } from 'dacsee-utils'

const { width } = Screen.window

@inject('app', 'circle', 'account')
@observer
export default class GroupDetail extends Component {
  constructor(props) {
    super(props)
    this.groupData = props.circle.groupData
    this.leaveGroup = props.circle.leaveGroup
    if (props.account.referralGroupId.length > 0) props.account.setReferrerGroupValue('')
  }
  static navigationOptions = ({ navigation }) => {
    const { strings } = $.store.app
    return {
      drawerLockMode: 'locked-closed',
      title: strings.group_detail
    }
  }

  async groupHandle(_id, isMember) {
    if (isMember) {
      await this.leaveGroup(_id)
      this.props.circle.getInvitations()
      this.props.circle.getGroups()
    } else {
      const { options } = this.props.navigation.state.params
      const { joinRequireApproval = false } = options
      await this.props.circle.joinGroup(_id, joinRequireApproval)
      this.props.navigation.goBack()
    }
  }
  getTypeData(type, strings) {
    let data = {}
    switch (type) {
      case 'public':
        data.text = strings.public
        data.icon = 'lock-open'
        data.iconColor = DCColor.BGColor('primary-1')
        break
      case 'reserved':
        data.text = strings.official
        data.icon = 'lock'
        data.iconColor = DCColor.BGColor('primary-1')
        break
      default:
        data.text = strings.private
        data.icon = 'lock'
        data.iconColor = '#797979'
    }
    return data
  }
  render() {
    const { strings } = this.props.app
    const { _id, name, description, type, membersCount, avatars, isMember, membershipType, tagline } = this.props.navigation.state.params

    const _getTypeData = this.getTypeData(type, strings)
    return (
      <View style={{ flex: 1, backgroundColor: '#f1f1f1' }}>
        <View style={{ flex: 1, alignItems: 'center', marginHorizontal: 40, marginBottom: 20, justifyContent: 'center' }}>
          <View style={styles.header}>
            <Image style={{ width: 100, height: 100, borderRadius: 50 }} source={{ uri: $.method.utils.getHeaderPicUrl(avatars) }} />

          </View>
          <View style={{ flexDirection: 'row', justifyContent: 'center', marginBottom: 10, alignItems: 'center', backgroundColor: '#d7d7d7', borderRadius: 10, paddingHorizontal: 6, paddingVertical: 2 }}>
            {Icons.Generator.Material(_getTypeData.icon, 15, _getTypeData.iconColor)}
            <Text style={{ fontSize: 13, color: '#000', opacity: 0.8 }}>{_getTypeData.text}</Text>
          </View>
          <Text style={{ marginLeft: 5, color: '#3C3C3C', fontSize: 18, opacity: 0.8, marginBottom: 10 }}>{name}</Text>
          {tagline ?
            <Text style={{ marginLeft: 5, color: '#3C3C3C', fontSize: 14, opacity: 0.8, marginBottom: 10 }}>{tagline}</Text>
            : null
          }
          {type !== 'reserved' && <Text style={{ marginLeft: 5, color: '#9F9F9F', opacity: 0.8, marginBottom: 10 }}>{`${membersCount} ${strings.member}`}</Text>}
          <Text style={{ marginLeft: 5, textAlign: 'center', color: '#545454', opacity: 0.8 }}>{description}</Text>
        </View>
        {
          type === 'reserved' || membershipType === 'creator' ? null
            : <View style={styles.bottomButton}>
              <TouchableOpacity onPress={() => this.groupHandle(_id, isMember)} activeOpacity={0.7} style={styles.confirmButton}>
                <Text style={{ fontSize: TextFont.TextSize(18), fontWeight: '400', color: 'white' }}>
                  {isMember ? strings.exit_group : strings.join_group}
                </Text>
              </TouchableOpacity>
            </View>
        }
      </View >
    )
  }
}

const styles = StyleSheet.create({
  circle: {
    width: 23,
    height: 23,
    borderRadius: 18,
    justifyContent: 'center',
    alignItems: 'center'
  },
  header: {
    marginVertical: 20,
    alignItems: 'center',
    justifyContent: 'center'
  },
  headerCircle: {
    height: 100,
    width: 100,
    borderRadius: 50,
    backgroundColor: '#A6A6A6',
    alignItems: 'center',
    justifyContent: 'center'
  },
  bottomButton: {
    justifyContent: 'center',
    alignItems: 'center',
    height: Define.system.ios.x ? 110 : 78,
    width: width - 90,
    marginHorizontal: 45
  },
  confirmButton: {
    justifyContent: 'center',
    alignItems: 'center',
    width: width - 90,
    height: 60,
    borderRadius: 36,
    backgroundColor: '#7dd320',
    borderStyle: 'solid',
    borderWidth: 5,
    borderColor: '#ffffff',
    shadowColor: 'rgba(0, 0, 0, 0.15)',
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowRadius: 3,
    shadowOpacity: 1
  }
})
