/* global store */
import React, { Component } from 'react'
import {
  Text,
  View,
  TouchableOpacity,
  ScrollView,
  Image,
  ActivityIndicator,
  StyleSheet
} from 'react-native'
import InteractionManager from 'InteractionManager'
import { inject, observer } from 'mobx-react'
import { Screen, TextFont, Avatars, DCColor } from 'dacsee-utils'

const { height } = Screen.window

@inject('app', 'circle', 'account')
@observer
export default class FriendsCircleComponent extends Component {

  static navigationOptions = ({ navigation }) => {
    const { strings } = $.store.app
    return {
      drawerLockMode: 'locked-closed',
      title: strings.send_friend_request,
      headerStyle: {
        backgroundColor: DCColor.BGColor('primary-1'),
        shadowColor: 'transparent',
        shadowOpacity: 0,
        borderBottomWidth: 0,
        borderBottomColor: 'transparent',
        elevation: 0,
      },
    }
  }
  constructor(props) {
    super(props)
    this.state = {}
  }

  async componentDidMount() {
    const { strings } = this.props.app
    const { referrer, id, isInvite = false, avatars = '', fullName = '', userId = '' } = this.props.navigation.state.params
    const accountUserId = this.props.account.user._id

    InteractionManager.runAfterInteractions()

    if (!isInvite) {
      const { avatars = '', fullName = '', userId = '' } = this.props.navigation.state.params
      this.setState({ invite_id: id, avatars, fullName, userId })
    } else {
      if (accountUserId === referrer) {
        return $.define.func.showMessage(strings.unable_to_add_yourselves)
      }
      const data = await $.method.session.User.Get(`v1/search?country=CN&userId=${referrer}`)
      if (!data || data.length === 0) return $.define.func.showMessage(strings.invitation_failure)
      const { _id, avatars, fullName, userId } = data[0]
      this.setState({ invite_id: _id, avatars, fullName, userId })
    }
  }


  render() {
    const { invite_id, avatars, fullName, userId } = this.state
    const { strings } = this.props.app
    const { sendRequest } = this.props.circle
    return (
      <View style={{ flex: 1, backgroundColor: 'white' }}>
        <ScrollView contentContainerStyle={{ flex: 1, backgroundColor: 'white' }} style={{ flex: 1 }}>
          <View style={{ backgroundColor: DCColor, height: height * 0.25, alignItems: 'center' }}>
            <Image style={styles.avatar} source={{ uri: Avatars.getHeaderPicUrl(avatars) }} />
            {
              !fullName && (
                <View style={{ position: 'absolute', marginTop: 10, width: 90, height: 90, borderRadius: 45, justifyContent: 'center', alignItems: 'center', backgroundColor: '#ddd' }}>
                  <ActivityIndicator size="small" color="#333" />
                </View>
              )
            }
            <Text style={{ color: 'white', fontSize: TextFont.TextSize(25), marginTop: 10 }}>{fullName}</Text>
            {/*<Text style={{ color: '#666', fontSize: TextFont.TextSize(12), fontWeight: '400' }}>{ userId }</Text>*/}
          </View>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', height: 44, backgroundColor: 'white', marginTop: 10, paddingHorizontal: 20 }}>
            <Text style={{ fontSize: TextFont.TextSize(16), color: '#000', }}>{strings.userid}</Text>
            <Text style={{ fontSize: TextFont.TextSize(16), color: '#000', opacity: 0.7, }}>{userId}</Text>
          </View>
        </ScrollView>
        {fullName && (
          <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
            <TouchableOpacity onPress={() => {
              sendRequest(invite_id, true)
            }} activeOpacity={.7} style={styles.sendRequest}>
              <Text style={{ color: 'black', fontSize: TextFont.TextSize(18), fontWeight: 'bold' }}>{strings.send_friend_request}</Text>
            </TouchableOpacity>
          </View>
        )}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f2f2f2'
  },
  avatar: {
    width: 90,
    height: 90,
    borderRadius: 45,
    borderWidth: 4,
    borderColor: '#106e9d',
    marginTop: 10,
    backgroundColor: DCColor.BGColor('primary-1'),

  },
  sendRequest: {
    height: 66,
    marginBottom: 20,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 60,
    borderRadius: 36,
    backgroundColor: DCColor.BGColor('green'),
    borderStyle: 'solid',
    borderWidth: 5,
    borderColor: '#ffffff',
    shadowColor: 'rgba(0, 0, 0, 0.15)',
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowRadius: 3,
    shadowOpacity: 1
  }
})