import React, { Component } from 'react'
import {
  Dimensions,
  StyleSheet,
  Text,
  View,
  Image,
  ListView,
  TextInput,
  TouchableOpacity,
  RefreshControl,
  SafeAreaView,
  InteractionManager
} from 'react-native'
import { inject, observer } from 'mobx-react'
import { observable } from 'mobx'
import { NavigationActions } from 'react-navigation'
import Material from 'react-native-vector-icons/MaterialIcons'
import Awesome from 'react-native-vector-icons/FontAwesome'

const { width } = Dimensions.get('window')
const dataContrast = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1._id !== r2._id, sectionHeaderHasChanged: (s1, s2) => s1 !== s2 })

@inject('app', 'circle')
@observer
export default class CircleGroupScreen extends Component {
  constructor(props) {
    super(props)
    this.groupData = props.circle.groupData
    this.selectGroup = props.circle.selectGroup
    this.confirmGroupSelect = props.circle.confirmGroupSelect
    this.getGroups = props.circle.getGroups
    this.getInvitations = props.circle.getInvitations
    this.groupRequest = props.circle.groupRequest
    this.searchGroup = props.circle.searchGroup
    this.joinGroup = props.circle.joinGroup
  }

  @observable isSearch: Boolean = false
  @observable search_value: String = ''

  componentDidMount() {
    InteractionManager.runAfterInteractions()
    this.getInvitations()
    this.getGroups()
  }

  componentWillUnmount() {

  }

  _renderFooter = () => {
    const { strings } = this.props.app
    return (
      this.groupData.groups.length === 0 && this.groupData.invitations.length
        ? <View>
          <View style={{ marginTop: 108, alignItems: 'center' }}>
            <Image style={{ marginBottom: 18 }} source={require('../../resources/images/friend-empty-state.png')} />
            <Text style={{ color: '#666', fontSize: 22, fontWeight: '600', textAlign: 'center', marginBottom: 6 }}>
              {strings.no_friend}
            </Text>
            <Text style={{ color: '#999', fontSize: 15, fontWeight: '400', textAlign: 'center' }}>
              {strings.clickto_add_friend}
            </Text>
          </View>
        </View> : null
    )
  }

  renderSectionHeader = (data, section) => {
    const { strings } = this.props.app
    return (
      data.length > 0 ? <View style={[
        {
          flexDirection: 'row',
          paddingHorizontal: 10,
          height: 40,
          alignItems: 'center',
          justifyContent: 'space-between',
          borderRadius: 6,
          backgroundColor: 'white',
          marginBottom: 10,
          marginTop: section !== '0' ? 10 : 0
        }, styles.shadow, this.isSearch && { backgroundColor: DCColor.BGColor('primary-1') }]}>
        <Text style={{ fontSize: 12, color: '#8c8c8c', fontWeight: '600' }}>{this.isSearch ? strings.search_result : section === '0' ? strings.friend_waitfor_accept : section === '1' ? strings.official_group : section === '2' ? strings.my_group : strings.featured_group}</Text>
      </View> : null
    )
  }

  getTypeData(type) {
    let data = {}
    const { strings } = this.props.app
    switch (type) {
      case 'public':
        data.text = strings.public
        data.icon = 'lock-open'
        data.iconColor = DCColor.BGColor('primary-1')
        break
      case 'reserved':
        data.text = strings.official
        data.icon = 'lock'
        data.iconColor = '#797979'
        break
      default:
        data.text = strings.private
        data.icon = 'lock'
        data.iconColor = '#797979'
    }

    return data
  }

  renderRequest(data) {
    const { _id, group_info = {}, requestor_info = {} } = data
    const { name, group_avatars = group_info.avatars } = group_info
    const { friend_name = requestor_info.fullName } = requestor_info

    return (
      <TouchableOpacity activeOpacity={0.7}
        style={[{ paddingHorizontal: 10, height: 84, borderRadius: 6, backgroundColor: 'white', justifyContent: 'space-between', alignItems: 'center', flexDirection: 'row', marginBottom: 10 }, styles.shadow]}>
        <View style={{ justifyContent: 'center', marginRight: 10 }}>
          <Image style={{ width: 56, height: 56, borderRadius: 28 }} source={{ uri: $.method.utils.getHeaderPicUrl(group_avatars) }} />
        </View>
        <View style={{ flex: 1, marginRight: 10, justifyContent: 'center' }}>
          <Text ellipsizeMode={'middle'} numberOfLines={1} style={{ fontSize: 14, color: '#333', fontWeight: '400' }}>{name}</Text>
          <Text ellipsizeMode={'middle'} numberOfLines={1} style={{ fontSize: 14, color: '#333', fontWeight: '400' }}>{`${friend_name}${this.props.app.strings.who_invite}`}</Text>

        </View>
        <View style={{ marginRight: 0, flexDirection: 'row', justifyContent: 'space-between' }}>
          <TouchableOpacity onPress={() => this.groupRequest('reject', _id)} activeOpacity={0.7} style={{ marginRight: 15, width: 30, height: 30, borderRadius: 18, backgroundColor: DCColor.BGColor('red'), justifyContent: 'center', alignItems: 'center' }}>
            <Material name='close' color='white' size={18} />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.groupRequest('accept', _id)} activeOpacity={0.7} style={{ width: 30, height: 30, borderRadius: 18, backgroundColor: DCColor.BGColor('green'), justifyContent: 'center', alignItems: 'center' }}>
            <Material name='check' color='white' size={18} />
          </TouchableOpacity>
        </View>
      </TouchableOpacity>
    )
  }

  renderSearchItem(data) {
    const { _id, name, avatars, membersCount, type, isMember, options = {} } = data
    const { joinRequireApproval = false } = options
    const getTypeData = this.getTypeData(type)
    const { strings } = this.props.app

    return (
      <TouchableOpacity onPress={() => {
        global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'CircleGroupDetail', params: data }))
      }}
        activeOpacity={0.7} style={[{ height: 84, borderRadius: 6, paddingHorizontal: 10, marginBottom: 10, backgroundColor: 'white', justifyContent: 'space-between', alignItems: 'center', flexDirection: 'row' }, styles.shadow]}>
        <View style={{ justifyContent: 'center' }}>
          <Image style={{ width: 56, height: 56, borderRadius: 28 }} source={{ uri: $.method.utils.getHeaderPicUrl(avatars) }} />
        </View>
        <View style={{ flex: 1, marginLeft: 10 }}>
          <Text style={{ fontSize: 16, color: '#000033', fontWeight: '400', marginBottom: 5 }}>{name}</Text>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', paddingHorizontal: 5, paddingVertical: 0 }}>
              <Material name={getTypeData.icon} color={getTypeData.iconColor} size={15} />
              <Text style={{ fontSize: 13, color: '#000', opacity: 0.8 }}>{getTypeData.text}</Text>
            </View>
            {type !== 'reserved' && <Text style={{ marginLeft: 5, color: '#000', opacity: 0.8 }}>{`${membersCount} ${strings.member}`}</Text>}
          </View>
        </View>
        {!isMember && type !== 'reserved' && <TouchableOpacity onPress={() =>
          this.joinGroup(_id, joinRequireApproval, this.search_value)
        } style={{ justifyContent: 'center', alignItems: 'center', borderRadius: 6, width: 80, height: 36, backgroundColor: '#7dd320' }}>
          <Text>{strings.join_group}</Text>
        </TouchableOpacity>}

      </TouchableOpacity>
    )
  }

  renderItem = (data, section, rowId) => {
    const { _id, name, type = 'public', membersCount, avatars, checked, membershipType, tagline } = data
    const { strings, circle } = this.props
    const { checkPermission } = circle

    const getTypeData = this.getTypeData(type)

    return (
      <TouchableOpacity
        onPress={() => {
          if (type === 'reserved') {
            global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'CircleGroupDetail', params: data }))
          } else {
            if (section === '3') {
              return global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'CircleGroupDetail', params: data }))
            }
            $.store.circle.groupData.members = {}
            global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'CircleGroupRoom', params: { membershipType, _id } }))
          }
        }}
        activeOpacity={0.7} key={section} style={[{ height: 84, borderRadius: 6, paddingHorizontal: 10, marginBottom: 10, backgroundColor: 'white', justifyContent: 'space-between', alignItems: 'center', flexDirection: 'row' }, styles.shadow]}>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <View style={{ justifyContent: 'center' }}>
            <Image style={{ width: 56, height: 56, borderRadius: 28 }} source={{ uri: $.method.utils.getHeaderPicUrl(avatars) }} />
          </View>
          <View style={{ marginLeft: 10 }}>
            <Text style={{ fontSize: 16, color: '#000033', fontWeight: '400', marginBottom: 5 }} numberOfLines={1} >{name}</Text>
            {
              tagline.length > 0 && (
                <Text style={{ fontSize: 14, color: '#666', marginBottom: 5, width: width * 0.6 }} numberOfLines={1} >{tagline}</Text>
              )
            }
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', paddingVertical: 0 }}>
                <Material name={getTypeData.icon} color={getTypeData.iconColor} size={15} />
                <Text style={{ fontSize: 13, color: '#000', opacity: 0.8 }}>{getTypeData.text}</Text>
              </View>
              {type !== 'reserved' && <Text style={{ marginLeft: 5, color: '#000', opacity: 0.8 }}>({membersCount})</Text>}
            </View>
          </View>
        </View>
        {membershipType === 'creator' && <View style={{ width: 26, height: 26, borderRadius: 13, justifyContent: 'center', alignItems: 'center', backgroundColor: '#F2F2F2' }}>
          <Awesome name={'star'} color={DCColor.BGColor('primary-1')} size={12} />
        </View>}

      </TouchableOpacity>
    )
  }

  onChangeText(text) {
    if (text === '') {
      this.isSearch = false
    } else {
      if (!this.isSearch) this.isSearch = true
      this.searchGroup(text)
    }

    this.search_value = text
  }

  render() {
    const { loading } = this.props
    const { strings } = this.props.app
    const { invitations = [], groups = {}, searchGroups = [], groups_official = [], groups_my = [], groups_featured = [] } = this.groupData
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: '#f1f1f1' }}>
        <View style={{ paddingHorizontal: 15 }}>
          <HeaderSearchBar name={strings.search_group} onChangeText={(text) => this.onChangeText(text)} isSearch={this.isSearch} />
        </View>
        <View style={{ flex: 1 }}>
          <ListView
            removeClippedSubviews={false}
            contentContainerStyle={{ paddingHorizontal: 15 }}
            enableEmptySections
            refreshControl={<RefreshControl
              refreshing={this.props.circle.circleLoading}
              onRefresh={() => this.componentDidMount()}
              title={strings.pull_refresh}
              colors={['#ffffff']}
              progressBackgroundColor={DCColor.BGColor('primary-1')}
            />}
            renderSectionHeader={(data, section) => this.renderSectionHeader(data, section)}
            dataSource={this.isSearch
              ? dataContrast.cloneWithRows(searchGroups.slice())
              : dataContrast.cloneWithRowsAndSections([invitations.slice(), groups_official.slice(), groups_my.slice(), groups_featured.slice()])}
            renderRow={(data, section, rowId) => this.isSearch
              ? this.renderSearchItem(data, section, rowId)
              : section === '0' ? this.renderRequest(data)
                : this.renderItem(data, section, rowId)}
          />
        </View>
      </SafeAreaView >
    )
  }
}

const HeaderSearchBar = (props) => {
  const { name, onChangeText = () => { }, onFocus, isSearch } = props
  return (
    <View style={[styles.shadow, styles.search_bar]}>
      <View style={{ width: 40, height: 32, alignItems: 'center', justifyContent: 'center' }}>
        <Material name='search' color='#D1D1DE' size={18} />
      </View>
      <TextInput
        ref={e => this.input = e}
        underlineColorAndroid={'transparent'}
        placeholderTextColor={'#D1D1DE'}
        placeholder={name}
        style={{ height: 42, width: width - (isSearch ? 110 : 70), paddingLeft: 0, color: '#5c5c5c' }}
        onChangeText={(text) => onChangeText(text)} />
      <View style={{ width: 40, height: 32, alignItems: 'center', justifyContent: 'center' }}>
        {isSearch ?
          <TouchableOpacity style={{ width: 30, height: 30, borderRadius: 18, justifyContent: 'center', alignItems: 'center', backgroundColor: DCColor.BGColor('red') }} activeOpacity={0.7}
            onPress={() => {
              this.input.clear()
              this.input.blur()
              onChangeText('')
              setTimeout(() => { this.input.setNativeProps({ text: '' }) })
            }}
          >
            <Material name='close' color='#fff' size={18} />
          </TouchableOpacity> : null
        }
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  bottomButton: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 84,
    width: width,
    paddingHorizontal: 40,
    position: 'absolute',
    bottom: 0
  },
  confirmButton: {
    justifyContent: 'center',
    alignItems: 'center',
    width: width - 120,
    height: 60,
    borderRadius: 12,
    backgroundColor: '#45e2be'
  },
  search_bar: {
    flexDirection: 'row',
    marginVertical: 10,
    // marginHorizontal: 15,
    backgroundColor: '#FDFDFE',
    borderRadius: 10,
    alignItems: 'center'
  },
  circle: {
    width: 23,
    height: 23,
    borderRadius: 13,
    justifyContent: 'center',
    alignItems: 'center'

  },
  shadow: {
    elevation: 2,
    shadowOffset: { width: 0, height: 0 },
    shadowColor: 'rgba(0,0,0,0.4)',
    shadowOpacity: 0.4,
    shadowRadius: 3
  },
  button_contain: {
    backgroundColor: '#45e2be',
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 20,
    height: 50,
    width: width - 80
  },
  edit_contain: {
    backgroundColor: '#f6f5fb',
    borderRadius: 10,
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 10
  }
})
