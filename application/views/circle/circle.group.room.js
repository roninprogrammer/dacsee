/**
 * @flow
 * Created by Mollet on 2018/5/10.
 */

import React, { Component } from 'react'
import { Text, View, TouchableOpacity, ListView, TextInput, Image, RefreshControl, StyleSheet, Linking, ActivityIndicator } from 'react-native'
import { inject, observer } from 'mobx-react'
import _ from 'lodash'
import Material from 'react-native-vector-icons/MaterialIcons'
import Resources from 'dacsee-resources'
import Awesome from 'react-native-vector-icons/FontAwesome'
import { observable } from 'mobx'
import { Icons, Screen, Define, DCColor, TextFont, Avatars } from 'dacsee-utils'
import ModalGroup from './component/modal.invite'

const { width } = Screen.window

const RenderItem = observer(({
  data, section, rowId, groupData, membersLength,
  fetchingMembers, removeMember, navigation, isMgmt: appUserIsMgmt,
  isCreator: appUserIsCreator
}) => {
  const { avatars = [], fullName, membershipType, phoneNo, phoneCountryCode, membershipOptions = {} } = data
  const { membershipType: AppUserMembershipType, _id } = navigation.state.params;
  const canRemove = () => {
    if (appUserIsCreator && membershipType !== 'creator') {
      return true
    } else if (appUserIsMgmt && membershipType === 'standard') {
      return true
    }
    return false
  }
  const pomise = canRemove()
  const { type } = groupData.groupInfo
  // const { membersLength, fetchingMembers = false } = this.state
  const isLastItem = parseInt(rowId) === (membersLength - 1)

  return (
    <View style={{ flex: 1 }}>
      <TouchableOpacity
        onPress={() => {
          let groupDatail = {}
          groupDatail.group_id = _id
          groupDatail.type = pomise ? 'ADMIN' : 'NORMAL'
          groupDatail.main_type = navigation.state.params.membershipType
          groupDatail.group_info = data
          navigation.navigate('FriendsDetail', { groupDatail, type: 'GROUP' })
        }}
        activeOpacity={0.7} key={section} style={{ height: 84, justifyContent: 'space-between', alignItems: 'center', flexDirection: 'row' }}>
        <View style={{ justifyContent: 'center' }}>
          <Image style={{ width: 56, height: 56, borderRadius: 28 }} source={{ uri: Avatars.getHeaderPicUrl(avatars) }} />
        </View>
        <View style={{ flex: 1, marginLeft: 10, justifyContent: 'center' }}>
          <Text style={{ fontSize: TextFont.TextSize(16), color: '#333', fontWeight: '400', marginBottom: 5 }}>{fullName}</Text>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            {membershipType !== 'creator' && membershipOptions.hasOwnProperty('disallowBookings') && !membershipOptions.disallowMakeBookings ?
              <Awesome name={'user'} color={'#AAA'} size={14} style={{ marginRight: 5 }} />
              : null
            }
            {membershipType !== 'creator' && membershipOptions.hasOwnProperty('disallowMakeBookings') && !membershipOptions.disallowBookings ?
              <Material name={'directions-car'} color={'#AAA'} size={17} style={{ marginRight: 5 }} />
              : null
            }
          </View>
        </View>

        {
          pomise && type !== 'reserved' && phoneCountryCode &&
          <TouchableOpacity activeOpacity={0.7} onPress={() => Linking.openURL(`tel:${phoneCountryCode}${phoneNo}`)}>
            <Image source={Resources.image.booking_detail_phone} style={{ marginRight: 15, height: 28, width: 28 }} />
          </TouchableOpacity>
        }
        {
          pomise && type !== 'reserved' &&
          <TouchableOpacity onPress={() => removeMember(data.membership_id)}
            activeOpacity={0.7} style={{ width: 32, justifyContent: 'center', height: 84 }}>
            <Material name={'remove-circle'} color={'red'} size={32} />
          </TouchableOpacity>
        }
      </TouchableOpacity>
      {isLastItem && fetchingMembers &&
        (<ActivityIndicator size="small" color="#333" />)
      }
    </View>
  )
})

const dataContrast = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1._id !== r2._id, sectionHeaderHasChanged: (s1, s2) => s1 !== s2 })

@inject('app', 'circle', 'account')
@observer
export default class GroupDetail extends Component {
  @observable isSearch: Boolean = false
  @observable searchValue: String = ''
  @observable searchMembers: Array = []
  @observable modalVisible: Boolean = false
  @observable driverCheck: Boolean = true
  @observable passengerCheck: Boolean = true
  @observable selectInfo: Object = {}
  LISTING_BATCH_SIZE = 25;
  static navigationOptions = ({ navigation }) => {
    const { strings } = $.store.app
    const { params } = navigation.state
    const { _id, membershipType, rightPress } = params
    return {
      drawerLockMode: 'locked-closed',
      title: strings.group_detail,
      headerRight: params && membershipType !== 'standard' && rightPress && (
        <TouchableOpacity
          activeOpacity={0.7}
          style={{ paddingRight: 15, justifyContent: 'center', alignItems: 'flex-end' }}
          onPress={params && rightPress}
        >
          <Text style={{ fontSize: TextFont.TextSize(16), color: '#fff', fontWeight: '600' }}>{strings.edit}</Text>
        </TouchableOpacity>
      )
    }
  }

  constructor(props) {
    super(props)
    this.groupData = props.circle.groupData
    this.getMembers = props.circle.getMembers
    this.updateMemberInfo = props.circle.updateMemberInfo
    this.getGroupInfo = props.circle.getGroupInfo
    this.getNewMembers = props.circle.getNewMembers
    this.joinGroupRequest = props.circle.joinGroupRequest
    this.searchGroupMembersAndRequests = props.circle.searchGroupMembersAndRequests
    this.changeMemberShip = props.circle.changeMemberShip
    this.showLoading = props.app.showLoading;
    this.hideLoading = props.app.hideLoading;
    this.appUser_id = props.account.user._id;
  }

  componentWillMount() {
    const { membershipType } = this.props.navigation.state.params
    const isMgmt = ['admin', 'creator'].includes(membershipType)
    const isCreator = 'creator' === membershipType
    this.setState({ isMgmt, isCreator })
  }

  async componentDidMount() {
    const { _id, membershipType } = this.props.navigation.state.params
    const { isMgmt } = this.state;
    await this.getGroupInfo(_id)
    const { type } = this.groupData.groupInfo
    if (type !== 'reserved') {
      if (isMgmt) this.getNewRequests(true)
      await this.fetchMembersBatch(true)
      if (membershipType && membershipType === 'creator') {
        this.props.navigation.setParams({
          rightPress: () => {
            this.groupData.newGroup = Object.assign({}, _.omit(this.groupData.groupInfo, '_id'))
            this.props.navigation.navigate('CircleGroupAdd', { _id })
          }
        })
      }
    }
    this.setState({ componentDidMount: true }); // workaround for lazy loading members
  }

  componentDidUpdate() {
    const { _id } = this.props.navigation.state.params
    const { newMembers, members } = this.groupData
    const requestList = newMembers[_id] || []
    const membersList = members[_id] || []
    const { requestsLength = 0, membersLength = 0 } = this.state
    const stateUpdate = {};
    if (requestsLength !== requestList.length) stateUpdate.requestsLength = requestList.length
    if (membersLength !== membersList.length) stateUpdate.membersLength = membersList.length
    if (stateUpdate.requestsLength || stateUpdate.membersLength) this.setState(stateUpdate)
  }

  _refreshControl = (loading, i18n) => (
    <RefreshControl
      refreshing={loading}
      onRefresh={() => console.log('......')}
      title={i18n.pull_refresh}
      colors={['#ffffff']}
      progressBackgroundColor={DCColor.BGColor('primary-1')}
    />
  )

  _renderFooter = () => {
    const { strings } = this.props.app
    return (
      this.groupData.members.length === 0 && this.groupData.newMembers.length === 0
        ? <View style={{ marginTop: 108, alignItems: 'center' }}>
          <Image style={{ marginBottom: 18 }} source={require('../../resources/images/friend-empty-state.png')} />
          <Text style={{ color: '#666', fontSize: TextFont.TextSize(22), fontWeight: '600', textAlign: 'center', marginBottom: 6 }}>
            {strings.no_friend}
          </Text>
          <Text style={{ color: '#999', fontSize: TextFont.TextSize(15), fontWeight: '400', textAlign: 'center' }}>
            {strings.clickto_add_friend}
          </Text>
        </View> : null
    )
  }

  renderSectionHeader = (data, section) => {
    const { strings } = this.props.app
    return (data.length > 0) && (
      <View style={{ marginVertical: 15 }}>
        <Text style={{ fontSize: TextFont.TextSize(12), color: '#8c8c8c', fontWeight: '600' }}>
          {section === '0' ? strings.friend_waitfor_accept : strings.member_list}
        </Text>
      </View>
    )
  }
  friendRequest() {

  }

  async getNewRequests(initialLoad) {
    const { newMembers = {} } = this.groupData
    const { _id } = this.props.navigation.state.params
    if (initialLoad && newMembers[_id] && Array.isArray(newMembers[_id].slice()) && newMembers[_id].length >= this.LISTING_BATCH_SIZE) {
      return this.setState({ hasMoreRequests: true })
    }

    const { _id: group_id } = this.props.navigation.state.params
    this.setState({ fetchingNewRequests: true })
    const newMembersBatch = await this.getNewMembers(group_id, this.LISTING_BATCH_SIZE)
    const hasMoreRequests = !newMembersBatch || (Array.isArray(newMembersBatch) && newMembersBatch.length === this.LISTING_BATCH_SIZE)
    this.setState({ fetchingNewRequests: false, hasMoreRequests })
  }

  async fetchMembersBatch(initialLoad) {
    if (this.isSearch) return this.fetchSearchResults()
    const { fetchingMembers, hasMoreMembers = true } = this.state;
    if (fetchingMembers || !hasMoreMembers) return;

    this.setState({ fetchingMembers: true })
    const { members = {} } = this.groupData
    const { _id: group_id } = this.props.navigation.state.params
    if (initialLoad && members[group_id] && Array.isArray(members[group_id].slice()) && members[group_id].length >= this.LISTING_BATCH_SIZE) {
      return this.setState({ hasMoreMembers: true, fetchingMembers: false })
    }
    const membersBatch = await this.getMembers(group_id, this.LISTING_BATCH_SIZE)
    let stillHasMoreMembers = !membersBatch || (Array.isArray(membersBatch) && membersBatch.length === this.LISTING_BATCH_SIZE)
    this.setState({ fetchingMembers: false, hasMoreMembers: stillHasMoreMembers })
  }

  async fetchSearchResults() { // TODO: enhance for requests as well
    const { fetchingSearchResults, searchTxt, searchResults = [] } = this.state
    if (fetchingSearchResults || !searchTxt || searchTxt.length < 3) return
    this.setState({ fetchingSearchResults: true })
    const { _id } = this.props.navigation.state.params
    const results = await this.searchGroupMembersAndRequests(
      _id, searchTxt, searchResults.length, this.LISTING_BATCH_SIZE, true, false
    )
    const stateUpdate = {
      fetchingSearchResults: false
    }
    if (results && results.members && Array.isArray(results.members)) {
      stateUpdate.searchResults = [...searchResults, ...results.members]
    }
    this.setState(stateUpdate)
  }

  async updateGroupRequest(action, request_id) {
    const { _id: group_id } = this.props.navigation.state.params
    this.showLoading()
    const result = this.joinGroupRequest(action, request_id, group_id)
    this.hideLoading()
    return result;
  }

  async approveJoinGroupRequest(request) {
    const { _id: group_id } = this.props.navigation.state.params
    const result = await this.updateGroupRequest('accept', request._id)
    if (result && result.membership_id) {
      const { members } = this.groupData;
      const currentMembers = members[group_id] || [];
      if (currentMembers.length < this.LISTING_BATCH_SIZE) await this.fetchMembersBatch()
      this.selectInfo = { ...request, membership_id: result.membership_id }
      this.modalVisible = true
    }
  }

  async membershipOptionsModalSubmit() {
    const { membership_id } = this.selectInfo;
    if (!membership_id) return;
    const { _id: group_id } = this.props.navigation.state.params
    const options = {
      membershipOptions: {
        disallowBookings: !this.driverCheck,
        disallowMakeBookings: false
      }
    }
    try {
      const resp = await this.changeMemberShip(membership_id, options, group_id)
      if (resp) {
        this.modalVisible = false
        this.driverCheck = false
        this.passengerCheck = false
        this.selectInfo = {}
      }
    } catch (error) {
      console.log(error)
    }
  }

  renderRow = (data, section, rowId) =>
    this.isSearch ? (
      <RenderItem
        data={data}
        section={section}
        rowId={rowId}
        groupData={this.groupData}
        removeMember={(membership_id) => this.removeMember(membership_id)}
        isSearch={true}
        {...this.props}
        {...this.state}
      />
    ) : section === '0' ?
        this.renderRequest(data, section, rowId)
        : <RenderItem
          data={data}
          section={section}
          rowId={rowId}
          groupData={this.groupData}
          removeMember={(membership_id) => this.removeMember(membership_id)}
          {...this.props}
          {...this.state}
        />


  renderRequest(data, section, rowId) {

    const { _id, user_info, type, requestor_id } = data
    const { avatars = [], fullName, phoneCountryCode, phoneNo } = user_info
    const { requestsLength, fetchingNewRequests = false, hasMoreRequests } = this.state
    const isLastItem = parseInt(rowId) === (requestsLength - 1)
    return (
      <View style={{ flex: 1 }}>
        <TouchableOpacity onPress={() => this.friendRequest('Detail', _id, data)} activeOpacity={0.7} style={[{ height: 84, paddingHorizontal: 10, borderRadius: 6, marginBottom: 10, backgroundColor: 'white', justifyContent: 'space-between', alignItems: 'center', flexDirection: 'row' }, styles.shadow]}>
          <View style={{ justifyContent: 'center', marginRight: 10 }}>
            <Image style={{ width: 56, height: 56, borderRadius: 28 }} source={{ uri: Avatars.getHeaderPicUrl(avatars) }} />
          </View>
          <View style={{ flex: 1 }}>
            <Text style={{ fontSize: TextFont.TextSize(16), color: '#333', fontWeight: '400' }}>{fullName}</Text>
          </View>
          {type === 'group_invitation'
            ? <View style={{ marginRight: 0, flexDirection: 'row', justifyContent: 'space-between' }}>
              {/* <TouchableOpacity activeOpacity={0.7} onPress={() => Linking.openURL(`tel:${phoneCountryCode}${phoneNo}`)}>
              <Image source={Resources.image.booking_detail_phone} style={{ marginRight: 15, height: 28, width: 28 }} />
            </TouchableOpacity> */}
              {this.appUser_id === requestor_id && (
                <TouchableOpacity
                  onPress={() => this.updateGroupRequest('cancel', _id)}
                  activeOpacity={0.7}
                  style={{
                    marginRight: 15,
                    width: 30,
                    height: 30,
                    borderRadius: 18,
                    backgroundColor: DCColor.BGColor('red'),
                    justifyContent: 'center',
                    alignItems: 'center'
                  }}
                >
                  {Icons.Generator.Material('close', 18, 'white')}
                </TouchableOpacity>
              )}
            </View>
            : <View style={{ marginRight: 0, flexDirection: 'row', justifyContent: 'space-between' }}>
              <TouchableOpacity activeOpacity={0.7} onPress={() => Linking.openURL(`tel:${phoneCountryCode}${phoneNo}`)}>
                <Image source={Resources.image.booking_detail_phone} style={{ marginRight: 15, height: 28, width: 28 }} />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.updateGroupRequest('reject', _id)}
                activeOpacity={0.7}
                style={{
                  marginRight: 15,
                  width: 30,
                  height: 30,
                  borderRadius: 18,
                  backgroundColor: DCColor.BGColor('red'),
                  justifyContent: 'center',
                  alignItems: 'center'
                }}
              >
                {Icons.Generator.Material('close', 18, 'white')}
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.approveJoinGroupRequest(data)}
                activeOpacity={0.7}
                style={{
                  width: 30,
                  height: 30,
                  borderRadius: 18,
                  backgroundColor: DCColor.BGColor('green'),
                  justifyContent: 'center',
                  alignItems: 'center'
                }}
              >
                {Icons.Generator.Material('check', 18, 'white')}
              </TouchableOpacity>
            </View>}
        </TouchableOpacity>

        {isLastItem && hasMoreRequests && !fetchingNewRequests &&
          <View style={styles.loadMoreContainer}>
            <TouchableOpacity
              activeOpacity={0.7}
              style={styles.loadMoreButton}
              onPress={() => this.getNewRequests()}
            >
              <Text
                style={{ fontSize: TextFont.TextSize(14), fontWeight: '400', color: 'white' }}>
                Load More
                    </Text>
            </TouchableOpacity>
          </View>
        }
        {isLastItem && fetchingNewRequests &&
          <ActivityIndicator size="small" color="#333" />
        }
      </View>
    )
  }

  async removeMember(membership_id) {
    const { _id } = this.props.navigation.state.params
    // let status = await this.updateMemberInfo({ status: 'inactive' }, membership_id, _id)
    this.showLoading();
    let status = await this.changeMemberShip(membership_id, { status: 'inactive' }, _id)
    this.hideLoading();
    // if (status) await this.getGroupInfo(_id)
  }

  onPressDetail(touch) {
    const { _id, avatars } = this.groupData.groupInfo
    const { membershipType } = this.props.navigation.state.params
    switch (touch) {
      case 'left':
        this.props.navigation.navigate('ProfileChangeAvatar', { type: _id, avatars })
        break
      case 'center':
        this.props.navigation.navigate('CircleGroupDetail', Object.assign({}, this.groupData.groupInfo, { isMember: true, membershipType }))
        break
      case 'right':
        this.groupData.newGroup = Object.assign({}, _.omit(this.groupData.groupInfo, '_id'))
        this.props.navigation.navigate('CircleGroupAdd', { groupInfo: this.groupData.groupInfo })
    }
  }
  onSearchTxtChange(text) {
    if (!text || text === '' || text.length < 3) {
      this.isSearch = false
      this.setState({ searchTxt: text, searchResults: [] })
    } else {
      if (!this.isSearch) this.isSearch = true
      this.setState({ searchTxt: text }, _.debounce(this.fetchSearchResults, 500));
    }
  }
  render() {
    const { _id, membershipType } = this.props.navigation.state.params
    const { isMgmt, isCreator, componentDidMount, searchResults = [], searchTxt = '' } = this.state
    const { strings } = this.props.app
    const { members, newMembers } = this.groupData
    const memberList = members[_id] || []
    const requestList = newMembers[_id] || []
    const { type = 'reserved', options = {} } = this.groupData.groupInfo
    return (
      <View style={{ flex: 1, backgroundColor: '#f1f1f1' }}>
        <DetailView groupInfo={this.groupData.groupInfo._id === _id ? this.groupData.groupInfo : {}}
          onPress={(touch) => this.onPressDetail(touch)}
          edit={isCreator} strings={strings} membershipType={membershipType} />

        {isMgmt &&
          <View style={{ paddingHorizontal: 15 }}>
            <HeaderSearchBar
              name={strings.search_member}
              // search_value={this.searchValue}
              value={searchTxt}
              isSearch={this.isSearch}
              onChangeText={(text) => this.onSearchTxtChange(text)} />
          </View>
        }

        <View style={{ flex: 1 }}>
          <ListView
            removeClippedSubviews={false}
            contentContainerStyle={{ paddingHorizontal: 25 }}
            enableEmptySections
            renderSectionHeader={(data, section) => this.renderSectionHeader(data, section)}
            dataSource={this.isSearch
              ? dataContrast.cloneWithRows(searchResults)
              : dataContrast.cloneWithRowsAndSections([requestList.slice(), memberList.slice()])}
            renderRow={(data, section, rowId) => this.renderRow(data, section, rowId)}
            renderSeparator={() => (
              <View style={{ height: 0.8, backgroundColor: '#e8e8e8' }} />
            )}
            onEndReached={() =>
              this.isSearch
                ? this.fetchSearchResults()
                : this.fetchMembersBatch(!componentDidMount)
            }
          />
        </View>

        {((isMgmt && type !== 'reserved') || (type === 'public' && !options.joinRequireApproval)) &&
          <View style={styles.bottomButton}>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('CircleGroupInvite', { group_id: _id })}
              activeOpacity={0.7} style={styles.confirmButton}>
              <Text style={{ fontSize: TextFont.TextSize(18), fontWeight: '400', color: 'white' }}>
                {strings.invite_friends}
              </Text>
            </TouchableOpacity>
          </View>
        }

        <ModalGroup
          visible={this.modalVisible}
          onClose={() => {
            this.modalVisible = false
            this.driverCheck = false
            this.passengerCheck = false
            this.selectInfo = {}
          }}
          strings={strings}
          info={this.selectInfo}
          driverCheck={this.driverCheck}
          passengerCheck={this.passengerCheck}
          onSubmit={async () => this.membershipOptionsModalSubmit()}
          onPress={(type, staus) => {
            if (type === 'driver') {
              this.driverCheck = staus
            } else {
              this.passengerCheck = staus
            }
          }}
        />
      </View >
    )
  }
}
const getTypeData = (type, strings) => {
  let data = {}
  switch (type) {
    case 'public':
      data.text = strings.public
      data.icon = 'lock-open'
      data.iconColor = DCColor.BGColor('primary-1')
      break
    case 'reserved':
      data.text = strings.official
      data.icon = 'lock'
      data.iconColor = DCColor.BGColor('primary-1')
      break
    default:
      data.text = strings.private
      data.icon = 'lock'
      data.iconColor = '#797979'
  }
  return data
}
const DetailView = (props) => {
  const { groupInfo, onPress = () => { }, strings, edit, membershipType } = props
  const { avatars, type = 'public', membersCount = 0, name = '', tagline = '' } = groupInfo
  const _getTypeData = getTypeData(type, strings)
  return (
    <View style={{ flexDirection: 'row', alignItems: 'center', backgroundColor: '#fff', paddingHorizontal: 20, paddingVertical: 10, justifyContent: 'space-between' }}>
      <View style={{ flexDirection: 'row', alignItems: 'center' }}>
        <TouchableOpacity onPress={() => edit ? onPress('left') : null} style={{ justifyContent: 'center' }}>
          <Image style={{ width: 56, height: 56, borderRadius: 28 }} source={{ uri: $.method.utils.getHeaderPicUrl(avatars) }} />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => onPress('center')} style={{ marginLeft: 10 }}>
          <Text style={{ fontSize: 16, color: '#000033', fontWeight: '400', marginBottom: 5 }}>{name}</Text>
          {tagline ? <Text numberOfLines={1} style={{ width: width * 0.65, marginBottom: 5, color: '#666' }}>{tagline}</Text> : null}
          {type !== 'reserved' && <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', paddingRight: 5, paddingVertical: 0 }}>
              <Material name={_getTypeData.icon} color={_getTypeData.iconColor} size={15} />
              <Text style={{ fontSize: 13, color: '#000', opacity: 0.8 }}>{_getTypeData.text}</Text>
            </View>
            <Text style={{ marginLeft: 5, color: '#000', opacity: 0.8 }}>{`${membersCount} ${strings.member}`}</Text>
          </View>}
        </TouchableOpacity>
      </View>
      {membershipType === 'creator' &&
        <View style={{ justifyContent: 'center', alignItems: 'center', width: 26, height: 26, borderRadius: 13, backgroundColor: '#F2F2F2' }}>
          {Icons.Generator.Awesome('star', 12, DCColor.BGColor('primary-1'))}
        </View>
      }
    </View>
  )
}
const HeaderSearchBar = (props) => {
  const { name, onChangeText = () => { }, isSearch, value = '' } = props
  return (
    <View style={[styles.shadow, styles.search_bar]}>
      <View style={{ width: 40, height: 32, alignItems: 'center', justifyContent: 'center' }}>
        <Material name='search' color='#D1D1DE' size={18} />
      </View>
      <TextInput
        ref={e => this.input = e}
        underlineColorAndroid={'transparent'}
        placeholderTextColor={'#D1D1DE'}
        placeholder={name}
        value={value}
        // onFocus={onFocus}
        style={{ height: 42, width: width - (isSearch ? 110 : 70), paddingLeft: 0, color: '#5c5c5c' }}
        onChangeText={(txt) => onChangeText(txt)} />
      <View style={{ width: 40, height: 32, alignItems: 'center', justifyContent: 'center' }}
      >
        {isSearch ?
          <TouchableOpacity
            style={{ width: 30, height: 30, borderRadius: 18, justifyContent: 'center', alignItems: 'center', backgroundColor: DCColor.BGColor('red') }} activeOpacity={0.7}
            onPress={() => {
              this.input.clear()
              this.input.blur()
              onChangeText('')
              setTimeout(() => { this.input.setNativeProps({ text: '' }) })
            }}
          >
            <Material name='close' color='#fff' size={18} />
          </TouchableOpacity> : null
        }
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  circle: {
    width: 23,
    height: 23,
    borderRadius: 18,
    justifyContent: 'center',
    alignItems: 'center'
  },
  shadow: {
    elevation: 2,
    shadowOffset: { width: 0, height: 0 },
    shadowColor: 'rgba(0,0,0,0.4)',
    shadowOpacity: 0.4,
    shadowRadius: 3
  },
  bottomButton: {
    justifyContent: 'center',
    alignItems: 'center',
    height: Define.system.ios.x ? 110 : 78,
    width: width - 90,
    marginHorizontal: 45
  },
  confirmButton: {
    justifyContent: 'center',
    alignItems: 'center',
    width: width - 90,
    height: 60,
    borderRadius: 36,
    backgroundColor: '#7dd320',
    borderStyle: 'solid',
    borderWidth: 5,
    borderColor: '#ffffff',
    shadowColor: 'rgba(0, 0, 0, 0.15)',
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowRadius: 3,
    shadowOpacity: 1
  },
  loadMoreContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    height: Define.system.ios.x ? 110 : 78,
    width: width - 150,
    marginHorizontal: 50
  },
  loadMoreButton: {
    justifyContent: 'center',
    alignItems: 'center',
    width: width - 150,
    height: 40,
    borderRadius: 30,
    backgroundColor: DCColor.BGColor('green'),
    borderStyle: 'solid',
    borderWidth: 3,
    borderColor: '#ffffff',
    shadowColor: 'rgba(0, 0, 0, 0.15)',
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowRadius: 3,
    shadowOpacity: 1
  },
  search_bar: {
    flexDirection: 'row',
    marginVertical: 10,
    backgroundColor: '#FDFDFE',
    borderRadius: 10,
    alignItems: 'center'
  }
})
