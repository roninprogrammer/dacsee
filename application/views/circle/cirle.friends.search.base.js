import React, { Component } from 'react'
import { Text, View, TouchableOpacity, Image, ActivityIndicator, FlatList } from 'react-native'
import { Icons, TextFont, Avatars } from 'dacsee-utils'
import { inject, observer } from 'mobx-react'


@inject('app', 'account', 'circle')
@observer
export default class FriendsSearchBase extends Component {

  static navigationOptions = ({ navigation }) => {
    const { strings } = $.store.app
    const { search } = navigation.state.params
    return {
      drawerLockMode: 'locked-closed',
      title: strings.search_friend
    }
  }

  constructor(props) {
    super(props)
  }

  componentDidMount() {
    const { value, countryCode, type } = this.props.navigation.state.params
    const { circle } = this.props
    const { searchAccount } = circle
    searchAccount(type, value, countryCode)
    //  try {
    //   const {country} = this.props.account
    //   const searchRet = await Session.User.Get(`v1/search?country=${country}${searchStr}`)
    //   this.setState({ dataSource: dataContrast.cloneWithRows(searchRet) })
    // } catch (e) {
    //   try {
    //     const searchRet = await Session.User.Get(`v1/search?country=CN&${searchStr}`)
    //     this.setState({ dataSource: dataContrast.cloneWithRows(searchRet) })
    //   } catch (err) {
    //     this.props.navigation.goBack()
    //   }
    // }
  }

  async _itemClick(data) {
    const { strings } = this.props.app
    try {
      const res = await $.method.session.Circle.Get(`v1/circleStatus?user_id=${data._id}`)
      const isFriend = res.isFriend
      if (isFriend) {
        //重写数据格式保证两端统一
        const friendData = Object.assign({}, { friend_info: data }, { friend_id: data._id }, { _id: res.connection_id })
        this.props.navigation.navigate('FriendsDetail', { friendData, type: 'FRIEND' })
      } else {
        this.props.navigation.navigate('FriendsRequest', { referrer: data.userId, id: data._id, avatars: data.avatars, fullName: data.fullName, userId: data.userId })
      }
    } catch (e) {
      $.define.func.showMessage(strings.unable_connect_server_pls_retry_later)
      this.props.navigation.goBack()
    }
  }

  render() {
    const { strings } = this.props.app
    const { friendData } = this.props.circle
    const { searchAccouts } = friendData
    return (
      <View style={{ flex: 1 }}>
        <FlatList
          data={searchAccouts.slice()}
          keyExtractor={(item, index) => item._id}
          renderItem={({ item, index }) => <TouchableOpacity activeOpacity={.7} onPress={() => this._itemClick(item)} style={{ height: 58, backgroundColor: 'white', paddingHorizontal: 15 }}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', flex: 1 }}>
              <View style={{ justifyContent: 'center', marginRight: 12 }}>
                <Image style={{ width: 42, height: 42, borderRadius: 21 }} source={{ uri: Avatars.getHeaderPicUrl(item.avatars) }} />
              </View>
              <View style={{ flex: 1 }}>
                <Text style={{ fontSize: TextFont.TextSize(16), color: '#666', marginBottom: 2 }}>{item.fullName}</Text>
                <Text style={{ fontSize: TextFont.TextSize(12), color: '#666' }}>{item.userId}</Text>
              </View>
              <View>
                {Icons.Generator.Material('chevron-right', 26, '#999')}
              </View>
            </View>
          </TouchableOpacity>
          }
          ListEmptyComponent={() => <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', top: 150 }}>
            <ActivityIndicator size='large' color='#999' />
            <Text style={{ color: '#666', fontWeight: '600', marginTop: 12 }}>{strings.pls_wait}</Text>
          </View>
          }
        />
      </View>
    )
  }
}
