import React, { Component } from 'react'
import { View, TouchableOpacity, StyleSheet, ActivityIndicator} from 'react-native'
import { RNCamera } from 'react-native-camera'
import { Icons, DCColor, System, Screen, Define } from 'dacsee-utils'
import {inject, observer} from 'mobx-react'
const { width } = Screen.window

const flashModeOrder = {
  off: 'on',
  on: 'auto',
  auto: 'torch',
  torch: 'off',
};

@inject('app', 'circle')
@observer
export default class CommonScanQRCode extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      drawerLockMode: 'locked-closed',
      headerStyle: {
        backgroundColor: DCColor.BGColor('primary-1'),
        shadowColor: 'transparent',
        shadowOpacity: 0,
        borderBottomWidth: 0,
        borderBottomColor: 'transparent',
        elevation: 0
      }
    }
  }

  constructor (props) {
    super(props)
    this.state = {
      active: true,
      flash: 'off',
    }
  }


  componentDidMount () {}

  _handlePress(){
    console.log("pressed already")
  }

  _scanResult (code) {
    if (code.data) {
      this.setState({active: false})
      const url = code.data
      var params = url.split('?')[1]
      var referrer = ''
      
      if (params) {
        params = params.split('&')

        for (var i = 0; i < params.length; i++) {
          var a = params[i].split('=')
          var paramName = a[0];

          if (paramName === 'ref' || paramName === 'referrer') {
            referrer = a[1]
          }
        }
      }

      if (referrer.length == 10) {
        this.handleInvite(referrer)
      } else {
        $.define.func.showMessage('Invalid QR Code')
        this.props.navigation.goBack()
      }
    }
  }

  async handleInvite (userId) {
    try {
      const res = await $.method.session.Circle.Get(`v1/circleStatus?userId=${userId}`)
      const isFriend = res.isFriend

      if (isFriend) {
        const user_id = res.user_id
        const connection_id = res.connection_id
        const searchRet = await $.method.session.User.Get(`v1/search?country=CN&fullNameOrUserId=${userId}`)
        const friendData = Object.assign({}, { friend_info: searchRet[0] }, { friend_id: user_id }, { connection_id })

        this.props.navigation.replace('FriendsDetail', { friendData, type: 'FRIEND' })

      } else {
        this.props.navigation.replace('FriendsRequest', { referrer: userId, isInvite: true })
      }
    } catch (e) {
      if (e.response.data.code == 'INVALID_USER') {
        $.define.func.showMessage('Invalid QR Code')
        this.props.navigation.goBack()
      }
    }
  }

  toggleFlash() {
    this.setState({
      flash: flashModeOrder[this.state.flash],
    });
  }


  render () {
    const {active} = this.state
    return (
      <View style={{ flex: 1 }}>
        {
          active ? (
            <RNCamera
              BarCodeType={RNCamera.Constants.BarCodeType.qr}
              style={{ flex: 1 }}
              flashMode={this.state.flash}
              ref={cam => { this.camera = cam }}
              androidCameraPermissionOptions={{
                title: this.props.app.strings.refuse_visit_camera,
                message: this.props.app.strings.pls_allow_camera_try
              }}
              // permissionDialogTitle={this.props.app.strings.refuse_visit_camera}
              // permissionDialogMessage={this.props.app.strings.pls_allow_camera_try}
              onBarCodeRead={(code) => this._scanResult(code)} 
            >
              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <View style={{ top: Define.system.ios.x ? -44 : -22, width: width * 0.6, height: width * 0.6 }}>
                  <View style={[styles.codeBorder, { top: 0, left: 0, height: 1.2, width: width * 0.2 }]} />
                  <View style={[styles.codeBorder, { top: 0, left: 0, height: width * 0.2, width: 1.2 }]} />

                  <View style={[styles.codeBorder, { bottom: 0, left: 0, height: width * 0.2, width: 1.2 }]} />
                  <View style={[styles.codeBorder, { bottom: 0, left: 0, height: 1.2, width: width * 0.2 }]} />

                  <View style={[styles.codeBorder, { bottom: 0, right: 0, height: width * 0.2, width: 1.2 }]} />
                  <View style={[styles.codeBorder, { bottom: 0, right: 0, height: 1.2, width: width * 0.2 }]} />

                  <View style={[styles.codeBorder, { top: 0, right: 0, height: width * 0.2, width: 1.2 }]} />
                  <View style={[styles.codeBorder, { top: 0, right: 0, height: 1.2, width: width * 0.2 }]} />
                </View>
              </View>
              <View style={{ position: 'absolute', bottom: 48, left: 0, right: 0, justifyContent: 'center', alignItems: 'center' }}>
                <TouchableOpacity onPress={this.toggleFlash.bind(this)} activeOpacity={0.7} style={{ backgroundColor: '#ffffffcc', height: 44, width: 44, justifyContent: 'center', alignItems: 'center', borderRadius: 22 }}>
                  { Icons.Generator.Material('lightbulb-outline', 28, '#666') }
                </TouchableOpacity>
              </View>
            </RNCamera>
          ) : (
            <View style={{flex: 1, backgroundColor: 'black', justifyContent: 'center', alignItems: 'center'}}>
              <ActivityIndicator />
            </View>
          )
        }

      </View>
    )
  }
}

const styles = StyleSheet.create({
  codeBorder: { backgroundColor: '#ffffffcc', position: 'absolute' }
})
