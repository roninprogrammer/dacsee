import React, { Component } from 'react'
import {
  Dimensions,
  StyleSheet,
  Text,
  View,
  Image,
  ListView,
  TextInput,
  TouchableOpacity,
  RefreshControl,
  SafeAreaView,
  InteractionManager
} from 'react-native'
const { width, height } = Dimensions.get('window')
import Material from 'react-native-vector-icons/MaterialIcons'
import { inject, observer, observe } from 'mobx-react'

@inject('app', 'circle')
@observer
export default class HeaderSearchBar extends Component {
  constructor (props) {
    super(props)
    this.state = {
      search_value: ''
    }
  }
  render () {
    const {placeholder , autoFocus, search_value, onFocus, onChangeText} = this.props
    return (
      <View style={[styles.shadow, styles.search_bar]}>
        <View style={{ width: 40, height: 32, alignItems: 'center', justifyContent: 'center' }}>
          <Material name='search' color='#D1D1DE' size={18} />
        </View>
        <TextInput
          underlineColorAndroid={'transparent'}
          placeholderTextColor={'#D1D1DE'}
          clearTextOnFocus={false}
          autoFocus={autoFocus}
          placeholder={placeholder}
          onFocus={onFocus}
          value={search_value}
          style={{ height: 42, width: width - 70, paddingLeft: 0, color: '#D1D1DE' }}
          onChangeText={(text) => onChangeText(text)} />
      </View>
    )
  }
}
const styles = StyleSheet.create({
  search_bar: {
    flexDirection: 'row',
    marginVertical: 10,
    backgroundColor: '#FDFDFE',
    borderRadius: 10, alignItems: 'center'
  },
  shadow: {
    elevation: 2,
    shadowOffset: { width: 0, height: 0 },
    shadowColor: 'rgba(0,0,0,0.4)',
    shadowOpacity: 0.4,
    shadowRadius: 3
  }
})
