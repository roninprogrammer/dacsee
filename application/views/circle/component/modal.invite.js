import React, { Component } from 'react'
import { Text, View, TouchableOpacity, StyleSheet, Image, Modal } from 'react-native'
import Material from 'react-native-vector-icons/MaterialIcons'
import { Screen, DCColor } from 'dacsee-utils'

const { height, width } = Screen.window

export default class ModalInvite extends Component {
  constructor(props) {
    super(props)
    this.state = { selectIndex: 0 }
  }

  render() {
    const { visible, strings, info, onPress, driverCheck, onClose, onSubmit } = this.props
    const { user_info = {} } = info

    return (
      <Modal
        animationType='fade' // 渐变
        transparent // 不透明
        visible={visible} // 根据isModal决定是否显示
        onRequestClose={() => this.props.onClose()} // android必须实现 安卓返回键调用
      >
        <View style={[
          { width: width, height: height, justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgba(57, 56, 67, 0.4)' },
          { shadowOffset: { width: 0, height: 3 }, shadowColor: '#999', shadowOpacity: 0.5 }
        ]}>
          <View style={{ alignItems: 'center', justifyContent: 'center' }}>
            <Image
              style={{ width: 80, height: 80, borderRadius: 40, borderWidth: 4, borderColor: '#fff', marginBottom: -40, zIndex: 200 }}
              source={{ uri: $.method.utils.getHeaderPicUrl(user_info.avatars) }}
            />
            <View style={{ backgroundColor: '#fff', borderRadius: 12, width: width * 0.7, justifyContent: 'center', alignItems: 'center', paddingTop: 45 }}>
              <View>
                <Text style={{ fontSize: 18, fontWeight: 'bold', textAlign: 'center' }}>{user_info.fullName}</Text>
                <Text>{strings.community_as}</Text>
              </View>
              <View style={{ backgroundColor: '#ddd', width: '100%', paddingHorizontal: 25, marginTop: 18 }}>
                <View style={{ flexDirection: 'row', alignItems: 'center', paddingVertical: 8, justifyContent: 'space-between' }}>
                  <Text style={{ fontWeight: 'bold', fontSize: 16 }}>{strings.driver}</Text>
                  <TouchableOpacity
                    style={[{
                      width: 22,
                      height: 22,
                      borderRadius: 11,
                      justifyContent: 'center',
                      alignItems: 'center'
                    }, { backgroundColor: driverCheck ? DCColor.BGColor('green') : '#fff' }]}
                    activeOpacity={0.7}
                    onPress={() => onPress('driver', !driverCheck)}
                  >
                    {<Material name='check' color={driverCheck ? '#fff' : '#e7e7e7'} size={18} />}
                  </TouchableOpacity>
                </View>
              </View>
              <View style={{ flexDirection: 'row', alignItems: 'center', paddingHorizontal: 16, justifyContent: 'space-between', width: '100%', marginVertical: 8 }}>
                <TouchableOpacity
                  style={{ backgroundColor: '#ddd', height: 40, borderRadius: 16, justifyContent: 'center', alignItems: 'center', width: '35%' }}
                  activeOpacity={0.7}
                  onPress={() => onClose()}
                >
                  <Text style={{ fontWeight: 'bold', fontSize: 14 }}>{strings.cancel}</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={{ backgroundColor: DCColor.BGColor('green'), height: 40, borderRadius: 16, justifyContent: 'center', alignItems: 'center', width: '60%' }}
                  activeOpacity={0.7}
                  onPress={() => onSubmit()}
                >
                  <Text style={{ fontWeight: 'bold', fontSize: 14 }}>{strings.confirm}</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </Modal >
    )
  }
}

const styles = StyleSheet.create({
  outerCircle: {
    height: 20,
    width: 20,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    backgroundColor: '#D8D8D8'
  },
  innerCircle: {
    height: 14,
    width: 14,
    borderRadius: 7
  }
})
