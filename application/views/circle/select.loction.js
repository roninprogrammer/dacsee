import React, { Component } from 'react'
import { Text, View, TouchableOpacity, TextInput, Platform, ListView } from 'react-native'
import { inject, observer } from 'mobx-react'
import { Icons, Define } from 'dacsee-utils'

const dataContrast = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })

@inject('app', 'circle')
@observer
export default class PickerLoctionComponent extends Component {
  static navigationOptions = { header: null }
  constructor(props) {
    super(props)
    const { parent_id, state } = props.navigation.state.params
    this.groupData = props.circle.groupData
    this.getData = state === 'state' ? props.circle.getCities : props.circle.getCountries
  }

  async componentDidMount() {
    const { parent_id } = this.props.navigation.state.params
    await this.getData(parent_id)
  }
  componentWillUnmount() { }


  render() {
    const { strings } = this.props.app
    let { parent_id, state, onPress = () => { } } = this.props.navigation.state.params
    return (
      <View style={{ flex: 1 }}>
        <View style={{ height: Platform.OS === 'android' ? 54 : Define.system.ios.x ? 88 : 64, backgroundColor: '#f2f2f2', borderBottomWidth: .8, borderColor: '#ccc', justifyContent: 'flex-end' }}>
          <View style={{ height: 54, flexDirection: 'row' }}>
            <TouchableOpacity
              activeOpacity={0.7}
              style={{ width: 54, paddingLeft: 8, justifyContent: 'center', alignItems: 'flex-start' }}
              onPress={() => this.props.navigation.goBack()}
            >
              {Icons.Generator.Material('keyboard-arrow-left', 30, '#2f2f2f')}
            </TouchableOpacity>
            <TextInput
              editable={false}
              placeholder={state === 'state' ? strings.select_city : strings.select_country}
              style={{ height: 54, flex: 1 }}
            />
          </View>
        </View>
        <View style={{ flex: 1 }}>
          <ListView
            removeClippedSubviews={false}
            style={{ backgroundColor: '#f8f8f8' }}
            enableEmptySections={true}
            dataSource={dataContrast.cloneWithRows(state === 'state' ? this.groupData.cities.slice() : this.groupData.countries.slice())}
            renderSeparator={() => (
              <View style={{ height: .8, backgroundColor: '#eee' }}></View>
            )}
            renderRow={(row) => (
              <TouchableOpacity activeOpacity={.7} onPress={() => {
                onPress(row)
                this.groupData.newGroup.location = row
                this.props.navigation.goBack()
              }} style={{ flex: 1, height: 52, justifyContent: 'center', backgroundColor: 'white' }}>
                <View style={{ flexDirection: 'row', paddingHorizontal: 15, alignItems: 'center' }}>
                  <Text style={{ color: '#333', fontSize: 16, fontWeight: '600' }}>{row.name}</Text>
                </View>
              </TouchableOpacity>
            )}
          />
        </View>
      </View>
    )
  }
}
