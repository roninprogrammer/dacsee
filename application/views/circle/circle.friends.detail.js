import React, { Component } from 'react'
import { Text, View, TouchableOpacity, Switch, StyleSheet, Image, ScrollView, Alert } from 'react-native'
import InteractionManager from 'InteractionManager'
import { inject, observer } from 'mobx-react'
import { Screen, TextFont, Define, DCColor } from 'dacsee-utils'

const { width, height } = Screen.window

@inject('app', 'circle')
@observer
export default class FriendsCircleDetailComponent extends Component {
  static navigationOptions = ({ navigation }) => {
    const { strings } = $.store.app
    return {
      drawerLockMode: 'locked-closed',
      title: strings.friend_detail,
      headerStyle: {
        backgroundColor: DCColor.BGColor('primary-1'),
        shadowColor: 'transparent',
        shadowOpacity: 0,
        borderBottomWidth: 0,
        borderBottomColor: 'transparent',
        elevation: 0
      }
    }
  }

  constructor(props) {
    super(props)
    this.groupData = props.circle.groupData
    // this.updateMemberInfo = props.circle.updateMemberInfo
    this.changeMemberShip = props.circle.changeMemberShip
    const { groupDatail = {}, friendData = {} } = this.props.navigation.state.params
    const { group_info = {} } = groupDatail
    const { blocked = false } = friendData
    const { membershipOptions = {}, membershipType } = group_info
    const { disallowBookings } = membershipOptions
    this.state = {
      loading: false,
      disallowBookings: !disallowBookings,
      isAdmin: membershipType === 'admin',
      blocked: blocked
    }
    this.showLoading = props.app.showLoading;
    this.hideLoading = props.app.hideLoading;
  }

  componentDidMount() {
    InteractionManager.runAfterInteractions()
  }

  onPressRequest = (type) => {
    const { circle = {}, navigation } = this.props
    const { params = {} } = navigation.state
    const { requestData = {} } = params
    const { friendRequest } = circle
    friendRequest(type, requestData._id, true)
  }

  async disallowMemberBooking(disallowBookings) {
    this.showLoading();
    this.setState({ disallowBookings })
    const { groupDatail = {} } = this.props.navigation.state.params
    const { group_info = {}, group_id } = groupDatail
    // let status = await this.updateMemberInfo({ membershipOptions: { disallowBookings: !disallowBookings } }, group_info.membership_id, group_id)
    let status = await this.changeMemberShip(
      group_info.membership_id,
      { membershipOptions: { disallowBookings: !disallowBookings } },
      group_id
    )
    if (!status) {
      this.setState({ disallowBookings: !disallowBookings })
    }
    this.hideLoading()
  }
  async updateAdmin(isAdmin) {
    this.showLoading()
    const { groupDatail = {} } = this.props.navigation.state.params
    const { group_info = {}, group_id } = groupDatail
    // let status = await this.updateMemberInfo({ membershipType: isAdmin ? 'standard' : 'admin' }, group_info.membership_id, group_id)
    let status = await this.changeMemberShip(
      group_info.membership_id,
      { membershipType: isAdmin ? 'standard' : 'admin' },
      group_id
    )
    if (status) {
      this.setState({ isAdmin: !isAdmin })
    }
    this.hideLoading()
  }
  async blockFriend(action, _id) {
    const { strings } = this.props.app
    try {
      const data = await $.method.session.Circle.Put('v1/block', { action: action ? 'block' : 'unblock', user_id: _id })
      console.log(data)
      if (data.isSuccess) {
        this.setState({ blocked: action })
        const _friends = await $.method.session.Circle.Get('v1/circle')
        if (_friends.length > 0) this.props.circle.friendData.friends = _friends
        if (action) $.define.func.showMessage(strings.block_success)
        return
      }
      $.define.func.showMessage(strings.block_failed)
    } catch (e) {
      $.define.func.showMessage(strings.timeout_try_again)
    }
  }
  render() {
    const { strings } = this.props.app
    const { isAdmin, disallowBookings, blocked } = this.state
    const { type, friendData = {}, requestData = {}, groupDatail = {} } = this.props.navigation.state.params
    const { fullName, userId, avatars } = type === 'REQUEST' ? requestData.requestor_info : type === 'GROUP' ? groupDatail.group_info : friendData.friend_info
    return (
      <View style={{ flex: 1, backgroundColor: 'white' }}>
        <ScrollView contentContainerStyle={{}} style={{ backgroundColor: 'white' }}>
          <View style={{ height: height * 0.25, backgroundColor: DCColor.BGColor('primary-1'), alignItems: 'center', paddingHorizontal: 25 }}>
            <View style={{ marginTop: 10 }}>
              <Image style={styles.avatar} source={{ uri: $.method.utils.getHeaderPicUrl(avatars) }} />
            </View>
            <Text ellipsizeMode={'middle'} numberOfLines={1} style={{ color: 'white', fontSize: TextFont.TextSize(22), marginTop: 10 }}>{fullName}</Text>
          </View>
          <View style={{ paddingHorizontal: 20, marginTop: 10 }}>
            <ListItem strings={strings.userid} type={'text'} params={userId} />
            <ListItem strings={strings.country} type={'text'} params={strings.no_content} />
            {(type === 'GROUP' && groupDatail.type === 'ADMIN') &&
              <ListItem strings={strings.driver} type={'switch'} params={disallowBookings}
                onChange={(status) => this.disallowMemberBooking(status)} />
            }

          </View>
        </ScrollView>

        {type === 'FRIEND' && (
          <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginHorizontal: 20, marginBottom: Define.system.ios.x ? 42 : 20 }}>
            <TouchableOpacity onPress={async () => {
              Alert.alert(strings.friend_delete, strings.del_friend_confirm, [{
                text: strings.confirm,
                onPress: async () => {
                  this.props.circle.delFriend(friendData._id)
                }
              }, { text: strings.cancel }])
            }}
              activeOpacity={0.7} style={{ width: width / 3, height: 40, borderRadius: 25, backgroundColor: '#D8D8D8', justifyContent: 'center', alignItems: 'center' }}>
              <Text style={{ color: '#000', fontSize: TextFont.TextSize(15), fontWeight: '600' }}>{strings.friend_delete}</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={async () => {
              Alert.alert(blocked ? strings.unblock : strings.block_friend,
                blocked ? strings.unblock_toast : strings.block_toast_, [{
                  text: strings.confirm,
                  onPress: async () => {
                    this.blockFriend(!blocked, friendData.friend_id)
                  }
                }, { text: strings.cancel }])
            }}
              activeOpacity={0.7} style={{ width: width / 3, height: 40, borderRadius: 25, backgroundColor: DCColor.BGColor('primary-1'), justifyContent: 'center', alignItems: 'center' }}>
              <Text style={{ color: '#000', fontSize: TextFont.TextSize(15), fontWeight: '600' }}>{blocked ? strings.unblock : strings.block_friend}</Text>
            </TouchableOpacity>
          </View>

        )}
        {type === 'REQUEST' && (
          <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginHorizontal: 20, marginBottom: Define.system.ios.x ? 42 : 20 }}>
            <TouchableOpacity onPress={() => this.onPressRequest('reject')}
              activeOpacity={0.7} style={{ width: width / 3, height: 40, borderRadius: 25, backgroundColor: '#D8D8D8', justifyContent: 'center', alignItems: 'center' }}>
              <Text style={{ color: '#000', fontSize: TextFont.TextSize(15), fontWeight: '600' }}>{strings.reject}</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.onPressRequest('accept')}
              activeOpacity={0.7} style={{ width: width / 3, height: 40, borderRadius: 25, backgroundColor: DCColor.BGColor('primary-1'), justifyContent: 'center', alignItems: 'center' }}>
              <Text style={{ color: '#000', fontSize: TextFont.TextSize(15), fontWeight: '600' }}>{strings.accept}</Text>
            </TouchableOpacity>
          </View>
        )}
        {(type === 'GROUP' && groupDatail.main_type === 'creator' && groupDatail.membershipType !== 'creator' && groupDatail.type === 'ADMIN') && (
          <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
            <TouchableOpacity onPress={async () => {
              Alert.alert(isAdmin ? strings.remove_admin : strings.assign_admin, isAdmin === 'admin' ? strings.remove_admin_toast : strings.assign_admin_toast, [{
                text: strings.confirm,
                onPress: () => this.updateAdmin(isAdmin)
              }, { text: strings.cancel }])
            }} activeOpacity={0.7} style={styles.sendRequest}>
              <Text style={{ color: 'white', fontSize: TextFont.TextSize(18), fontWeight: 'bold' }}>{isAdmin ? strings.remove_admin : strings.assign_admin}</Text>
            </TouchableOpacity>
          </View>
        )}
      </View>
    )
  }
}

function ListItem(props) {
  const { strings, params, type, onChange } = props
  return (
    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', height: 44 }}>
      <Text style={{ fontSize: TextFont.TextSize(16), color: '#999', fontWeight: '400' }}>{strings}</Text>
      {type === 'switch' && (<Switch value={params} onValueChange={(msg) => onChange(msg)} />)}
      {type === 'text' && (<Text style={{ top: -1.5, fontSize: TextFont.TextSize(16), color: '#333', fontWeight: '400' }}>{params}</Text>)}
    </View>
  )
}

const styles = StyleSheet.create({
  avatar: {
    width: 90,
    height: 90,
    borderRadius: 45,
    borderWidth: 4,
    borderColor: '#106e9d',
    backgroundColor: DCColor.BGColor('primary-1')
  },
  sendRequest: {
    height: 66,
    marginBottom: Define.system.ios.x ? 42 : 20,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 40,
    borderRadius: 36,
    backgroundColor: DCColor.BGColor('red'),
    borderStyle: 'solid',
    borderWidth: 5,
    borderColor: '#ffffff',
    shadowColor: 'rgba(0, 0, 0, 0.15)',
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowRadius: 3,
    shadowOpacity: 1,
    elevation: 2
  }
})
