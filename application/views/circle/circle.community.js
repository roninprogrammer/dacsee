import React, { Component } from 'react'
import {
  Dimensions,
  StyleSheet,
  Text,
  View,
  Image,
  FlatList,
  TouchableOpacity,
  ScrollView,
  ActivityIndicator,
  Animated,
  StatusBar
} from 'react-native'
import { inject, observer } from 'mobx-react'
import Resources from 'dacsee-resources'
import { DCColor, TextFont } from '../../utils'

const { width, height } = Dimensions.get('window')

@inject('app', 'circle', 'vehicles')
@observer
export default class CircleCommunityScreen extends Component {
  constructor(props) {
    super(props)

    this.state = {
      official_groups: [],
      official_loading: false,
      campaign_groups: [],
      campaign_loading: false,
      community_loading: false
    }

    this.groupData = props.circle.groupData
    this.getUserCampaigns = props.circle.getUserCampaigns
    this.category_ids = props.vehicles.vehiclesData.length ? props.vehicles.vehiclesData[0].category_ids ? props.vehicles.vehiclesData[0].category_ids : [] : []
    this.loading = true
  }

  async componentDidMount() {
    this.setState({ official_loading: true, community_loading: true })

    const official_groups = this.groupData.groups_official.filter((group) => {
      return this.category_ids.indexOf(group.options.vehicle_category_id) > -1
    })

    this.setState({ official_groups: official_groups, campaign_loading: true, official_loading: false })

    await this.getUserCampaigns() // Get active campaigns

    this.setState({ campaign_groups: this.groupData.userCampaigns, campaign_loading: false, community_loading: false })

    this.loading = false
  }

  getTypeData(type) {
    let data = {}
    const { strings } = this.props.app
    switch (type) {
      case 'public':
        data.text = strings.public
        data.icon = Resources.image.public_group_icon
        data.iconColor = DCColor.BGColor('primary-1')
        break
      case 'reserved':
        data.text = strings.official
        data.icon = Resources.image.official_group_icon
        data.iconColor = '#797979'
        break
      default:
        data.text = strings.private
        data.icon = Resources.image.private_group_icon
        data.iconColor = '#797979'
    }
    return data
  }

  communityDetail(community) {
    if (community.type === 'reserved') {
      // Official Group Detail
      this.props.navigation.navigate('CommunityDetailScreen', {
        name: community.name,
        description: community.description,
        tagline: community.tagline,
        avatars: community.avatars || [],
        type: community.type
      })
    } else {
      // Community Detail
      this.props.navigation.navigate('SpecialDriverDetailScreen', {
        name: community.name,
        description: community.tagline,
        tnc: community.tnc || '',
        avatars: community.avatars || []
      })
    }
  }

  communityCard = ({ item, index }) => {
    const { name, tagline, type, avatars } = item

    let opacityValue = 1

    if (!!this.loading) {
      opacityValue = new Animated.Value(0)

      Animated.timing(opacityValue, {
        toValue: 1,
        duration: 800,
        delay: index * 150
      }).start();
    }

    const {
      specialDriverCont,
      imgCont,
      specialImg,
      specialDriverBox,
      sGroupLabelBox,
      sGroupLabel,
      sGroupCaption,
      sBottomCont,
      tagCont,
      tagBox,
      dcIcon,
      tagText,
    } = styles
    const getTypeData = this.getTypeData(type)

    return (
      <TouchableOpacity onPress={() => this.communityDetail(item)} >
        <Animated.View key={index} style={[specialDriverCont, { opacity: opacityValue }]} >
          <View style={imgCont} >
            <Image style={specialImg} source={$.method.utils.getPicSource(avatars, false, Resources.image.img_placeholder)} />
          </View>

          <View activeOpacity={1} style={specialDriverBox} >
            <View style={sGroupLabelBox}>
              <Text style={sGroupLabel} numberOfLines={1} ellipsizeMode='tail'>{name}</Text>
              {
                tagline ? (
                  <Text style={sGroupCaption} numberOfLines={2} ellipsizeMode="tail">{tagline}</Text>
                ) : null
              }
            </View>
            <View style={sBottomCont}>
              <View style={tagCont}>
                <View style={tagBox}>
                  <Image source={getTypeData.icon} style={dcIcon} />
                  <Text style={tagText}>{getTypeData.text}</Text>
                </View>
              </View>
            </View>
          </View>
        </Animated.View>
      </TouchableOpacity>
    )
  }

  render() {
    const {
      mainCont,
      bodyCont,
      titleCont,
      fTitle,
      emptyCont,
      emptyImg,
      dataCont,
      emptyTitle,
      emptyCaption
    } = styles

    const { official_groups = [], official_loading = false, campaign_groups = [], campaign_loading = false, community_loading = false } = this.state
    const { strings } = this.props.app

    return (
      <ScrollView style={mainCont}>
        <StatusBar
          animated={true}
          hidden={false}
          backgroundColor={DCColor.BGColor('primary-1')}
          barStyle={'dark-content'}
        />

        <View style={bodyCont}>
          {
            community_loading ? (
              <View style={emptyCont}>
                <View style={[dataCont, { height: height * .30 }]}>
                  <ActivityIndicator size="large" color={'rgba(0, 0, 0, 0.75)'} />
                </View>
              </View>
            ) : !official_groups.length && !campaign_groups.length ? (
              <View style={emptyCont}>
                <Image style={emptyImg} source={Resources.image.emptystate_no_special_driver} />
                <View style={dataCont}>
                  <Text style={emptyTitle}>{strings.community_empty}</Text>
                </View>
              </View>
            ) : null
          }

          {
            !!official_groups && official_groups.length ? (
              <View style={bodyCont}>
                <View style={titleCont}>
                  <Text style={fTitle}>{strings.official_group}</Text>
                </View>
                <View style={{ flex: 1 }}>
                    <FlatList
                      removeClippedSubviews={false}
                      keyExtractor={(item, index) => item._id}
                      data={official_groups}
                      renderItem={this.communityCard}
                    />
                </View>
              </View>
            ) : null
          }
          
          {
            !!campaign_groups && campaign_groups.length ? (
              <View style={{flex: 1}}>
                <View style={titleCont}>
                  <Text style={fTitle}>{strings.joy_group}</Text>
                </View>
                <View style={{ flex: 1 }}>
                  <FlatList
                    removeClippedSubviews={false}
                    keyExtractor={(item, index) => item._id}
                    data={campaign_groups}
                    renderItem={this.communityCard}
                  />
                </View>
              </View>
            ) : null
          }
        </View>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  mainCont: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#f2f2f2',
    position: 'relative'
  },
  tabCont: {
    backgroundColor: DCColor.BGColor('primary-1'),
    paddingHorizontal: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  tabItem: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    paddingVertical: 15,
    position: 'relative'
  },
  fCaption: {
    fontSize: TextFont.TextSize(13),
    color: 'rgba(255, 255, 255, 0.65)',
    fontWeight: '500',
    textAlign: "center"
  },
  activeFont: {
    color: 'white',
    fontWeight: '600'
  },
  dcTriangle: {
    position: 'absolute',
    bottom: 0,
    width: 0,
    height: 0,
    backgroundColor: 'transparent',
    borderStyle: 'solid',
    borderLeftWidth: 7,
    borderRightWidth: 7,
    borderBottomWidth: width * .03,
    borderLeftColor: 'transparent',
    borderRightColor: 'transparent',
    borderBottomColor: '#f2f2f2'
  },
  bodyCont: {
    flex: 1,
    backgroundColor: '#f2f2f2',
    padding: 5
  },
  actionCont: {
    backgroundColor: 'white',
  },
  priceCont: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.07)',
    borderBottomWidth: 1,
    borderTopWidth: 1,
    padding: 10,
    borderColor: 'rgba(0, 0, 0, 0.05)'
  },
  successCont: {
    backgroundColor: 'rgba(126, 211, 33, .2)'
  },
  errorCont: {
    backgroundColor: 'rgba(250, 0 , 0, .2)'
  },
  closeCont: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: 20,
    height: 20,
    borderRadius: 35,
    backgroundColor: 'rgba(0, 0, 0, 0.25)',
    marginRight: 5
  },
  closeIcon: {
    width: 10,
    height: 10,
    resizeMode: 'contain',
    tintColor: 'rgba(0, 0, 0, 0.75)'
  },
  promoCont: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexDirection: 'row',
  },
  promoBox: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    flexDirection: 'column',
  },
  promoText: {
    fontSize: TextFont.TextSize(11),
    color: 'rgba(0, 0, 0, 0.75)',
    fontWeight: '500',
    textAlign: 'left'
  },
  discountText: {
    fontSize: TextFont.TextSize(11),
    color: 'rgba(0, 0, 0, 0.45)',
    fontWeight: '400',
    textAlign: 'right',
    paddingRight: 5,
    textDecorationLine: 'line-through'
  },
  priceText: {
    fontSize: TextFont.TextSize(15),
    color: 'rgba(0, 0, 0, 0.95)',
    fontWeight: '700',
    textAlign: 'right',
    paddingRight: 5,
  },
  buttonCont: {
    padding: 10,
  },
  dcBtn: {
    backgroundColor: DCColor.BGColor('primary-1'),
    borderRadius: 4,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 15,
    elevation: 2,
    shadowOffset: { width: 0, height: 0 },
    shadowColor: 'rgba(0,0,0,0.4)',
    shadowOpacity: 0.4,
    shadowRadius: 3
  },
  btnText: {
    fontSize: TextFont.TextSize(18),
    fontWeight: '700',
    color: 'rgba(0, 0, 0, 0.75)'
  },
  titleCont: {
    padding: 10,
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start'
  },
  fTitle: {
    fontSize: TextFont.TextSize(18),
    fontWeight: '700',
    color: 'rgba(0, 0, 0, 0.75)',
    textAlign: 'left'
  },
  dcHorizontalScroll: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start'
  },
  dcGroupCont: {
    borderRadius: 6,
    minWidth: width * .68,
    margin: 5,
    elevation: 2,
    shadowOffset: { width: 0, height: 0 },
    shadowColor: 'rgba(0,0,0,0.4)',
    shadowOpacity: 0.4,
    shadowRadius: 3
  },
  dcGroupBox: {
    backgroundColor: 'white',
    borderRadius: 6,
    padding: 5,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    overflow: 'hidden',
    position: 'relative',
  },
  groupAvatar: {
    height: 45,
    width: 45,
    resizeMode: 'contain',
    zIndex: 10,
    marginHorizontal: 10
  },
  groupLabelCont: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  groupLabel: {
    fontSize: TextFont.TextSize(15),
    fontWeight: '900',
    color: 'rgba(0, 0, 0, 0.95)',
    textAlign: 'center',
  },
  groupCaptionCont: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  groupCaption: {
    fontSize: TextFont.TextSize(14),
    fontWeight: '400',
    color: 'rgba(0, 0, 0, 0.75)',
    textAlign: 'center',
    marginRight: 5,
  },
  groupInfoIcon: {
    width: TextFont.TextSize(13),
    height: TextFont.TextSize(13),
    resizeMode: 'contain',
    tintColor: 'rgba(0, 0, 0, 0.25)',
  },
  actionBox: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'flex-end'
  },
  groupCheckBox: {
    height: 25,
    width: 25,
    borderRadius: 20,
    backgroundColor: '#999999',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 10,
    marginBottom: 10
  },
  groupPriceTag: {
    borderRadius: 6,
    borderBottomLeftRadius: 15,
    backgroundColor: DCColor.BGColor('primary-1'),
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 5,
    paddingHorizontal: 15,
    minWidth: 30,
  },
  groupPrice: {
    fontSize: TextFont.TextSize(14),
    color: 'rgba(0, 0, 0, 0.95)',
    fontWeight: '800',
    textAlign: 'right',
    paddingLeft: 5,
  },
  dcRadarCont: {
    position: 'absolute',
    right: -width * .25,
  },
  dcRadar1: {
    width: width * .6,
    height: width * .6,
    borderRadius: width * .35,
    backgroundColor: 'rgba(255, 196, 17, .4)',
    opacity: .5,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  dcRadar2: {
    width: width * .45,
    height: width * .45,
    borderRadius: width * .35,
    backgroundColor: 'rgba(255, 196, 17, .4)',
    zIndex: 5
  },
  specialDriverCont: {
    position: 'relative',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: 15,
    minHeight: 100,
    overflow: 'hidden'
  },
  imgCont: {
    width: 95,
    height: 110,
    borderRadius: 5,
    marginRight: 10,
    borderColor: 'rgba(0, 0, 0, 0.05)',
    borderWidth: 1,
    position: 'absolute',
    left: 10,
    elevation: 6,
    shadowOffset: { width: 0, height: 0 },
    shadowColor: 'rgba(0,0,0,0.4)',
    shadowOpacity: 0.2,
    shadowRadius: 3,
    zIndex: 10,
    backgroundColor: 'white'
  },
  specialImg: {
    borderRadius: 5,
    width: '100%',
    height: '100%',
    resizeMode: 'cover'
  },
  specialDriverBox: {
    backgroundColor: 'white',
    flex: 1,
    borderRadius: 3,
    flexDirection: 'column',
    padding: 5,
    paddingLeft: 107,
    justifyContent: 'center',
    alignItems: 'flex-start',
    elevation: 2,
    shadowOffset: { width: 0, height: 0 },
    shadowColor: 'rgba(0,0,0,0.4)',
    shadowOpacity: 0.2,
    shadowRadius: 3,
    position: 'relative',
    minHeight: 90
  },
  sGroupLabelBox: {
    paddingTop: 5,
    paddingRight: 30,
    flex: 1
  },
  sGroupLabel: {
    fontSize: TextFont.TextSize(14),
    fontWeight: '600',
    color: 'rgba(0, 0, 0, 0.95)',
    textAlign: 'left',
  },
  sGroupCaption: {
    fontSize: TextFont.TextSize(12),
    fontWeight: '400',
    color: 'rgba(0, 0, 0, 0.5)',
    textAlign: 'left',
  },
  sBottomCont: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
  },
  tagCont: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingBottom: 5
  },
  tagBox: {
    borderRadius: 50,
    padding: 2,
    paddingHorizontal: 5,
    backgroundColor: 'rgba(0, 0, 0, 0.05)',
    marginRight: 5,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  dcCheckBox: {
    height: 25,
    width: 25,
    borderRadius: 20,
    backgroundColor: '#999999',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 5,
    zIndex: 10,
    position: 'absolute',
    top: 5,
    right: 0
  },
  dcIcon: {
    width: 10,
    height: 10,
    marginRight: 5,
    resizeMode: 'contain'
  },
  tagText: {
    fontSize: TextFont.TextSize(11),
    fontWeight: '400',
    color: 'rgba(0, 0, 0, 0.75)',
    textAlign: 'left',
  },
  sPriceTag: {
    backgroundColor: DCColor.BGColor('primary-1'),
    borderRadius: 6,
    borderBottomLeftRadius: 15,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 5,
    paddingHorizontal: 15,
    minWidth: 50,
  },
  sPriceText: {
    fontSize: TextFont.TextSize(14),
    color: 'rgba(0, 0, 0, 0.95)',
    fontWeight: '800',
    textAlign: 'right',
    paddingLeft: 5,
  },
  infoBox: {
    width: 15,
    height: 15,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  infoIcon: {
    width: '100%',
    height: '100%',
    resizeMode: 'contain',
    tintColor: 'rgba(0, 0, 0, 0.25)',
  },
  emptyCont: {
    height: height * 0.8,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  emptyImg: {
    height: height * .25,
    width: height * .25,
    resizeMode: 'contain'
  },
  dataCont: {
    width: width * .65,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  emptyTitle: {
    fontSize: TextFont.TextSize(16),
    fontWeight: '700',
    color: 'rgba(0, 0, 0, 0.75)',
    textAlign: 'center',
    marginBottom: 2
  },
  emptyCaption: {
    fontSize: TextFont.TextSize(13),
    fontWeight: '500',
    color: 'rgba(0, 0, 0, 0.45)',
    textAlign: 'center'
  }
})