/* global store */

import React, { Component } from 'react'
import { Text, View, TouchableOpacity, ScrollView, TextInput, Clipboard } from 'react-native'
import { Icons, Define, System, TextFont, DCColor } from 'dacsee-utils'
import { SvgIcon, iconPath } from '../chat/chatIcon'
import { inject, observer } from 'mobx-react'


@inject('app', 'account')
@observer
export default class FriendsCircleAddComponent extends Component {
  static navigationOptions = ({ navigation }) => {
    const { strings } = $.store.app
    return {
      drawerLockMode: 'locked-closed',
      title: strings.friend_add
    }
  }

  render() {
    const { strings, language } = this.props.app
    return (
      <View style={{ flex: 1 }}>
        <ScrollView contentContainerStyle={{ paddingHorizontal: 22, paddingVertical: 22 }} style={{ flex: 1 }}>
          {/* PHONE NO. */}
          <BlockWrap
            iconBackgroundColor={DCColor.BGColor('primary-1')}
            icon={Icons.Generator.Awesome('phone', 36, 'white')}
            title={strings.phone}
            describer={strings.search_byphone}
            isPhoneNo
            placeholder={'13x xxxx xxxx'}
            canInput
            navigation={this.props.navigation}
            onPress={(value, countryCode) => {
              if (value.length === 0) return $.define.func.showMessage(strings.pls_enter_correct_num)
              this.props.navigation.navigate('FriendsSearchBase', { type: 'phone', value, countryCode })
            }}
          />

          {/* EMAIL ADDRESS */}
          <BlockWrap
            iconBackgroundColor={DCColor.BGColor('green')}
            icon={Icons.Generator.Awesome('envelope', 28, 'white')}
            title={strings.email}
            describer={strings.search_byemail}
            placeholder={'example@mail.com'}
            canInput
            onPress={(value) => {
              if (value.length === 0 || !System.Rules.isMail(value)) return $.define.func.showMessage(strings.pls_enter_correct_email)
              this.props.navigation.navigate('FriendsSearchBase', { type: 'email', value })
            }}
          />

          {/* PHONE NO. */}
          <BlockWrap
            iconBackgroundColor={DCColor.BGColor('primary-1')}
            icon={Icons.Generator.Awesome('address-card', 28, 'white')}
            title={strings.name_or_userid}
            describer={strings.search_byname_userid}
            placeholder={''}
            canInput
            navigation={this.props.navigation}
            onPress={(value) => {
              if (value.length < 2) return $.define.func.showMessage(strings.pls_enter_two_chars)
              this.props.navigation.navigate('FriendsSearchBase', { type: 'fullName', value })
            }}
          />

          {/* SCAN QRCODE SESSION */}
          <BlockWrap
            iconBackgroundColor={DCColor.BGColor('primary-1')}
            icon={Icons.Generator.Awesome('qrcode', 32, 'white')}
            title={strings.qr_code}
            describer={strings.scan_qr_code}
            onPress={(value) => {
              this.props.navigation.navigate('CommonScanQRCode', { strings })
            }}
          />

          {/* INVAITE LINK */}
          <BlockWrap
            iconBackgroundColor={DCColor.BGColor('primary-1')}
            icon={<SvgIcon path={iconPath.hyperlinks} size={32} fill={['#fff']} />}
            title={strings.invite_link}
            describer={strings.copy_to_clipboard}
            onPress={(value) => {
              // 老的url
              let url = `http://clientstaging.incredible-qr.com/clients/dacsee/redirect-page/?referrer=${this.props.account.user.userId}&id=${this.props.account.user._id}`
              // 新的url
              const linkTitle = 'You are invited by your friend to Join DACSEE Social Ride Sharing\r\n 1) Download DACSEE App from Google Play or App store\r\n 2) Click on the invite link below\r\n'
              const linkFooter = ` 3) Complete your registration with Referrer Code = ${this.props.account.user.userId}`
              url = `${linkTitle}https://invite.dacsee.com/?referrer=${this.props.account.user.userId}&id=${this.props.account.user._id}&language=${language}\r\n${linkFooter}`
              Clipboard.setString(url)
              $.define.func.showMessage('Success')
            }}
          />
        </ScrollView>
      </View>
    )
  }
}

class BlockWrap extends Component {
  constructor(props) {
    super(props)
    this.state = {
      value: '',
      countryCode: '+60'
    }
  }

  render() {
    const { canInput, title, describer, iconBackgroundColor, icon, isPhoneNo, placeholder, onPress = () => { } } = this.props
    const { countryCode, value } = this.state
    const WrapComponent = canInput ? View : TouchableOpacity

    return (
      <WrapComponent activeOpacity={0.7} onPress={() => onPress()} style={[
        { shadowOffset: { width: 0, height: 2 }, shadowColor: '#999', shadowOpacity: 0.3, shadowRadius: 3 },
        { marginBottom: 15, height: 105, borderRadius: 17, backgroundColor: 'white', flexDirection: 'row', alignItems: 'center', paddingHorizontal: 13 }
      ]}>
        <IconWrap backgroundColor={iconBackgroundColor}>{icon}</IconWrap>
        <View style={{ justifyContent: 'center', marginLeft: 12, flex: 1 }}>
          <Text style={{ fontSize: TextFont.TextSize(16), color: 'black', marginBottom: canInput ? 2 : 6 }}>{title}</Text>
          <Text style={{ fontSize: TextFont.TextSize(11), color: '#969696' }}>{describer}</Text>
          {
            canInput && (
              <View style={{ marginTop: 6 }}>
                <View style={{ flexDirection: 'row', height: 36, borderRadius: 6, backgroundColor: '#e8e8e8', paddingHorizontal: 6, alignItems: 'center' }}>
                  {
                    isPhoneNo && (
                      <TouchableOpacity onPress={() => this.props.navigation.navigate('PublicPickerCountry', {
                        onPress: (code, name) => this.setState({ countryCode: code })
                      })} activeOpacity={0.7} style={[
                        { shadowOffset: { width: 0, height: 1 }, shadowColor: '#999', shadowOpacity: 0.5, shadowRadius: 3 },
                        { marginRight: 5, flexDirection: 'row', height: 26, borderRadius: 15, backgroundColor: DCColor.BGColor('primary-1'), width: 58, justifyContent: 'center', alignItems: 'center' }
                      ]}>
                        <Text style={{ color: 'white', fontSize: TextFont.TextSize(13), fontWeight: '400' }}>{countryCode}</Text>
                        {Icons.Generator.Material('arrow-drop-down', 14, 'white')}
                      </TouchableOpacity>
                    )
                  }
                  <TextInput {...Define.TextInputArgs} onChangeText={value => this.setState({ value: (value || '').trim() })} keyboardType={isPhoneNo ? 'numeric' : 'default'} placeholder={placeholder} style={{ flex: 1, fontSize: TextFont.TextSize(13) }} />
                  <TouchableOpacity onPress={() => onPress(value, countryCode)} activeOpacity={0.7} style={[
                    { shadowOffset: { width: 0, height: 1 }, shadowColor: '#999', shadowOpacity: 0.5, shadowRadius: 3 },
                    { backgroundColor: DCColor.BGColor('primary-1'), width: 26, height: 26, borderRadius: 15, justifyContent: 'center', alignItems: 'center' }
                  ]}>
                    {Icons.Generator.Material('search', 18, 'black')}
                  </TouchableOpacity>
                </View>
              </View>
            )
          }
        </View>
      </WrapComponent>
    )
  }
}

class IconWrap extends Component {
  render() {
    return (
      <View style={[
        { width: 76, height: 76, borderRadius: 38, justifyContent: 'center', alignItems: 'center' },
        { borderColor: '#EAEAEA', borderWidth: 6 },
        { backgroundColor: this.props.backgroundColor }
      ]}>
        {this.props.children}
      </View>
    )
  }
}

class ItemPerson extends Component {
  render() {
    const { icon, label, describer, onPress = () => { } } = this.props.data
    return (
      <TouchableOpacity activeOpacity={0.7} onPress={onPress} style={{ height: 60, backgroundColor: 'white', justifyContent: 'space-between', alignItems: 'center', flexDirection: 'row', paddingHorizontal: 15 }}>
        <View style={{ justifyContent: 'center', width: 60 }}>
          {icon}
        </View>
        <View style={{ flex: 1 }}>
          <Text style={{ fontSize: TextFont.TextSize(16), color: '#333', fontWeight: '400', marginBottom: 2 }}>{label}</Text>
          <Text style={{ fontSize: TextFont.TextSize(13), color: '#999' }}>{describer}</Text>
        </View>
      </TouchableOpacity>
    )
  }
}
