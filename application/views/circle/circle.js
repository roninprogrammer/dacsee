import React, { Component } from 'react'
import { View, StyleSheet, TouchableOpacity } from 'react-native'
import Material from 'react-native-vector-icons/MaterialIcons'
import { NavigationActions } from 'react-navigation'
import { inject, observer } from 'mobx-react'
import { Screen, DCColor } from 'dacsee-utils'
import CircleFriendScreen from './circle.friends'
import CircleCommunityScreen from './circle.community'
import HeaderTab from '../../components/header-tab'

const { width } = Screen.window

@inject('app')
@observer
export default class MyCircleScreen extends Component {
  constructor(props) {
    super(props)
  }

  static navigationOptions = ({ navigation }) => {
    const { strings, control } = $.store.app

    return {
      drawerLockMode: 'locked-closed',
      headerTitle: (<NavigationHeader />),
      headerRight: (
        <NavigatorHeaderRightButton navigation={navigation} />
      )
    }
  }

  render() {
    const { control } = this.props.app
    const { mainCont } = styles

    return (
      <View style={{flex: 1, width}}>
        {
          control.header.indexCircle ? (
            <CircleFriendScreen navigation={this.props.navigation} />
          ) : (
            <CircleCommunityScreen navigation={this.props.navigation} />
          )
        }
      </View>
    )
  }
}
@inject('app')
@observer
class NavigationHeader extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    const { app = {} } = this.props
    const { control, strings } = app

    return (
      <HeaderTab onlyTitle={false} active={control.header.indexCircle} onPress={index => control.header.circleNavigate(index)}>
        <HeaderTab.Item>{strings.group}</HeaderTab.Item>
        <HeaderTab.Item>{strings.friend}</HeaderTab.Item>
      </HeaderTab>
    )
  }
}

@inject('app')
@observer
class NavigatorHeaderRightButton extends Component {
  render() {
    const { app } = this.props
    return (app.control.header.indexCircle === 1) ? (
      <TouchableOpacity
        activeOpacity={0.7}
        style={{ top: 1, width: 54, paddingRight: 18, justifyContent: 'center', alignItems: 'flex-end' }}
        onPress={() => {
          global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'CircleFriendAdd', params: {} }))
        }}
      >
        <Material name={'add'} size={30} color={DCColor.BGColor('primary-2')} />
      </TouchableOpacity>
    ) : null
  }
}

const styles = StyleSheet.create({
  mainCont: {
    flexDirection: 'column',
    alignContent: 'flex-start',
    justifyContent: 'flex-start',
    flex: 1
  }
})