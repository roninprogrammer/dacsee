import React, { Component } from 'react'
import { Text, View, StyleSheet, TouchableOpacity, Image, FlatList, SafeAreaView, Clipboard } from 'react-native'
import { inject, observer } from 'mobx-react'
import { DCColor, TextFont } from 'dacsee-utils'
import { SvgIcon, iconPath } from '../chat/chatIcon'

@inject('app', 'circle', 'account')
@observer
export default class GroupInviteScreen extends Component {
  constructor(props) {
    super(props)
    this.groupData = props.circle.groupData
    this.friendData = props.circle.friendData
    this.sendInvite = props.circle.sendInvite
  }

  static navigationOptions = ({ navigation }) => {
    const { strings } = $.store.app
    return {
      drawerLockMode: 'locked-closed',
      title: strings.invite_friends
    }
  }

  componentWillUnmount() { }
  async itemPress(item) {
    const { groupInfo = {} } = this.groupData
    await this.sendInvite(groupInfo._id, item.friend_id)
  }

  getLink = () => {
    const { language } = this.props.app
    const { group_id } = this.props.navigation.state.params
    let linkStart = `You are invited by your friend to Join DACSEE Social Ride Sharing\r\n 1) Download DACSEE App from Google Play or App store\r\n 2) Click on the invite link below\r\n`
    let linkEnd = `\r\n 3) Complete your registration with Referrer Code = ${this.props.account.user.userId}`
    let http_url = $.config.env.groupInviteLink
    let args = `?referrer=${this.props.account.user.userId}&id=${this.props.account.user._id}&group_id=${group_id}&language=${language}`
    return `${linkStart}${http_url}${args}${linkEnd}`
  }

  render() {
    const { strings } = this.props.app
    const { getInviteUsers } = this.props.circle
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <View style={{ flex: 1, paddingHorizontal: 20 }}>
          <FlatList
            data={getInviteUsers}
            style={{ paddingTop: 10 }}
            keyExtractor={(item, index) => item._id}
            ListHeaderComponent={() =>
              <Invite
                icon={<SvgIcon path={iconPath.hyperlinks} size={32} fill={['#fff']} />}
                title={strings.invite_link}
                describer={strings.copy_to_clipboard}
                onPress={() => {
                  const url = this.getLink()
                  Clipboard.setString(url)
                  $.define.func.showMessage('Success')
                }
                }
              />
            }
            renderItem={({ item }) => <ItemList {...item.friend_info} ismember={item.ismember} strings={strings}
              itemPress={() => item.ismember ? null : this.itemPress(item)}
            />}
          />
        </View>
      </SafeAreaView>
    )
  }
}
const ItemList = (props) => {
  const { avatars, fullName, itemPress, strings, ismember } = props
  return (
    <TouchableOpacity
      activeOpacity={1}
      style={[{
        height: 84,
        borderRadius: 6,
        paddingHorizontal: 10,
        backgroundColor: 'white',
        alignItems: 'center',
        flexDirection: 'row',
        marginBottom: 10
      }, styles.shadow]}
    >
      <View style={{ justifyContent: 'center', marginRight: 10 }}>
        <Image style={{ width: 56, height: 56, borderRadius: 28 }} source={{ uri: $.method.utils.getHeaderPicUrl(avatars) }} />
      </View>
      <View style={{ flex: 1, marginRight: 10, justifyContent: 'center' }}>
        <Text style={{ fontSize: TextFont.TextSize(16), color: '#333', fontWeight: '400', marginBottom: 5 }}>{fullName}</Text>
        <View style={{ flexDirection: 'row' }}>
        </View>
      </View>
      <TouchableOpacity activeOpacity={ismember ? 1 : 0.7} onPress={itemPress} style={{ justifyContent: 'center', alignItems: 'center', borderRadius: 6, width: 80, height: 36, backgroundColor: ismember ? '#ccc' : '#7dd320' }}>
        <Text>{strings.invite}</Text>
      </TouchableOpacity>
    </TouchableOpacity>
  )
}

const Invite = (props) => {
  const { icon, title, describer, onPress } = props
  return (
    <TouchableOpacity style={[{ flexDirection: 'row', alignItems: 'center', backgroundColor: '#fff', borderRadius: 6, padding: 15 },
    { shadowOffset: { width: 0, height: 2 }, shadowColor: '#999', shadowOpacity: 0.3, shadowRadius: 3 }, {
      marginBottom: 15
    }
    ]} onPress={onPress} activeOpacity={0.7}
    >
      <View style={[
        { width: 76, height: 76, borderRadius: 38, justifyContent: 'center', alignItems: 'center' },
        { borderColor: '#EAEAEA', borderWidth: 6 },
        { backgroundColor: DCColor.BGColor('primary-1') }]}>
        {icon}
      </View>
      <View style={{ justifyContent: 'center', marginLeft: 12, flex: 1 }}>
        <Text style={{ fontSize: TextFont.TextSize(16), color: 'black', marginBottom: 6 }}>{title}</Text>
        <Text style={{ fontSize: TextFont.TextSize(11), color: '#969696' }}>{describer}</Text>
      </View>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  shadow: {
    elevation: 2,
    shadowOffset: { width: 0, height: 0 },
    shadowColor: 'rgba(0,0,0,0.4)',
    shadowOpacity: 0.4,
    shadowRadius: 3
  }
})
