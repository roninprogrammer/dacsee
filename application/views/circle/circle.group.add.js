/* global store */

import React, { Component } from 'react'
import { Text, View, Switch, TouchableOpacity, StyleSheet, ScrollView, TextInput, Image, Alert, } from 'react-native'
import { inject, observer } from 'mobx-react'
import { observable } from 'mobx'
import ActionSheet from 'react-native-actionsheet'
import ImagePicker from 'react-native-image-picker'
import { Icons, Screen, Define, DCColor, TextFont } from 'dacsee-utils'

const { width } = Screen.window

const showMember = ['all', 'driverOnly', 'none']
@inject('app', 'circle')
@observer
export default class FriendsGroupAddComponent extends Component {
  constructor(props) {
    super(props)
    this.groupData = props.circle.groupData
    this.createNewGroup = props.circle.createNewGroup
    this.updateGroupDetail = props.circle.updateGroupDetail
    const { _id } = props.navigation.state.params
    const { name, tagline, description, type, options = {}, location = {}, avatars } = this.groupData.newGroup
    const { canonicalName = '', parent_id = '' } = location
    const { strings } = props.app
    this.uri = _id ? $.method.utils.getHeaderPicUrl(avatars) : ''
    const index = _id ? showMember.findIndex((i) => i === options.memberListingMode) : 3
    this.state = {
      country: _id ? canonicalName.split(',')[1] : strings.select_country,
      city: _id ? canonicalName.split(',')[0] : strings.select_city,
      parent_id: _id ? parent_id : '',
      name: _id ? name : '',
      tagline: _id ? tagline : '',
      owned: _id ? (type === 'private') : false,
      verification: _id ? options.joinRequireApproval : false,
      memberOnly: _id ? options.disallowBookingFromNonMembers : false,
      memberListingMode: index === -1 ? 3 : index,
      description: _id ? description : ''
    }
  }
  @observable uri: String = ''
  @observable base64: String = ''
  static navigationOptions = ({ navigation }) => {
    const { _id } = navigation.state.params
    const { strings } = $.store.app
    return {
      drawerLockMode: 'locked-closed',
      title: _id ? strings.edit_community : strings.group_add
    }
  }
  _getOptions = () => {
    const { strings } = this.props.app
    return {
      title: strings.album,
      storageOptions: { skipBackup: true, path: 'images' },
      quality: 0.8,
      mediaType: 'photo',
      cancelButtonTitle: strings.cancel,
      takePhotoButtonTitle: strings.take_photo,
      chooseFromLibraryButtonTitle: strings.select_from_album,
      allowsEditing: true,
      noData: false,
      maxWidth: 1000,
      maxHeight: 1000,
      permissionDenied: {
        title: strings.refuse_visit,
        text: strings.pls_auth,
        reTryTitle: strings.retry,
        okTitle: strings.okay
      }
    }
  }

  _pressActionSheet(index) {
    const { _id } = this.props.navigation.state.params
    const { strings } = this.props.app
    if (index === 0) {
      ImagePicker.launchCamera(this._getOptions(), (response) => {
        if (response.didCancel) {

        } else if (response.error) {
          let label = response.error.startsWith('Photo') ? strings.photo : strings.camera
          return Alert.alert(`${strings.pls_auth_fore} ${label} ${strings.pls_auth_back} ${label}。`)
        } else {
          this.uri = response.uri
          this.base64 = response.data
          if (_id) this.props.circle.uploadGroupAvatar(_id, response.data)
        }
      })
    }
    if (index === 1) {
      ImagePicker.launchImageLibrary(this._getOptions(), (response) => {
        if (response.didCancel) {

        } else if (response.error) {
          let label = response.error.startsWith('Photo') ? strings.photo : strings.camera
          return Alert.alert(`${strings.pls_auth_fore} ${label} ${strings.pls_auth_back} ${label}。`)
        } else {
          this.uri = response.uri
          this.base64 = response.data
          if (_id) this.props.circle.uploadGroupAvatar(_id, response.data)
        }
      })
    }
  }

  componentDidMount() {
    const { _id = '' } = this.props.navigation.state.params
    if (!_id) {
      this.groupData.newGroup.type = 'public'
      this.groupData.newGroup.options = { joinRequireApproval: false }
    }
  }
  async createGroup() {
    const { owned, verification, memberOnly, memberListingMode } = this.state
    const { _id } = this.props.navigation.state.params
    const { strings } = this.props.app
    const { name, location } = this.groupData.newGroup
    if (!name || !location || !this.state.city || memberListingMode === 3) {
      $.define.func.showMessage($.store.app.strings.pls_finish_info)
      return
    }
    if (owned) {
      this.groupData.newGroup.type = 'private'
      this.groupData.newGroup.options = {}
    } else {
      this.groupData.newGroup.type = 'public'
      this.groupData.newGroup.options = { joinRequireApproval: verification, disallowBookingFromNonMembers: memberOnly }
    }
    this.groupData.newGroup.options.memberListingMode = memberListingMode > 2 ? 'all' : showMember[memberListingMode]
    if (_id) {
      await this.updateGroupDetail(_id)
    } else {
      await this.createNewGroup(this.base64)
      // this.uploadImage(_id, this.base64)
    }
  }

  render() {
    const { strings } = this.props.app
    const { name, tagline, owned, verification, memberOnly, memberListingMode, description, country, city } = this.state
    const { _id = '' } = this.props.navigation.state.params
    const memberShowText = [strings.all, strings.driver_only, strings.none, strings.please_select]
    const memberListSheet = [strings.all, strings.driver_only, strings.none, strings.cancel]
    return (
      <View style={{ flex: 1, backgroundColor: '#fff' }}>
        <ScrollView>
          <View style={styles.header}>
            <TouchableOpacity onPress={() => this.ActionSheet.show()} style={styles.headerCircle}>
              {this.uri === '' ? Icons.Generator.Material('photo-camera', 36, '#fff')
                : <Image style={{ width: 80, height: 80, borderRadius: 40 }} source={{ uri: this.uri }} />}
            </TouchableOpacity>
          </View>
          <View style={{ marginHorizontal: 20 }}>
            <SetCell title={`${strings.group_name}*`} type={'Single_Input'} msg={strings.enter_here} dafult={name}
              onChange={(name) => { this.groupData.newGroup.name = name }} />
            <SetCell title={strings.private_group} type={'Switch'} msg={this.state.owned}
              onChange={(owned) => this.setState({ owned })} />
            {!owned && (<SetCell title={strings.join_group_verify} type={'Switch'} msg={verification}
              onChange={(verification) => this.setState({ verification })} />)}
            {!owned && (<SetCell title={strings.allow_member_only} type={'Switch'} msg={memberOnly}
              onChange={(memberOnly) => this.setState({ memberOnly })} />)}
            <SetCell title={`${strings.member_list_show}*`} type={'Single_Select'} msg={memberShowText[memberListingMode]}
              onChange={() => { this.memberListSheet.show() }} />
            <SetCell title={strings.description} type={'Multi_Input'} msg={strings.enter_here} dafult={description}
              onChange={(description) => { this.groupData.newGroup.description = description }} />
            <SetCell title={strings.tagline} type={'Single_Input'} msg={strings.enter_here} dafult={tagline}
              onChange={(tagline) => { this.groupData.newGroup.tagline = tagline }} />
            <SetCell title={`${strings.country}*`} type={'Single_Select'} msg={country}
              onChange={() => this.props.navigation.navigate('PickerLoction', {
                onPress: (data) => this.setState({ country: data.name, parent_id: data._id, city: '' }),
                state: 'country'
              })} />
            <SetCell title={`${strings.state}*`} type={'Single_Select'} msg={city}
              onChange={() => {
                if (!this.state.parent_id) return
                this.props.navigation.navigate('PickerLoction', {
                  onPress: (data) => this.setState({ city: data.name }),
                  state: 'state',
                  parent_id: this.state.parent_id
                })
              }} />
          </View>
          <View style={styles.bottom}>
            <TouchableOpacity style={styles.bottomButton} onPress={() => this.createGroup()}>
              <Text style={{ color: '#000', fontWeight: '600', fontSize: TextFont.TextSize(16) }}>{_id ? strings.save : strings.create_community}</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
        <ActionSheet
          ref={e => { this.ActionSheet = e }}
          options={[strings.take_photo, strings.select_from_album, strings.cancel]}
          cancelButtonIndex={2}
          onPress={this._pressActionSheet.bind(this)}
        />
        <ActionSheet
          ref={e => { this.memberListSheet = e }}
          options={memberListSheet}
          cancelButtonIndex={3}
          onPress={(index) => { this.setState({ memberListingMode: index }) }}
        />
      </View>
    )
  }
}
const SetCell = (props) => {
  const { type, title, msg, onChange, dafult } = props
  return (
    <View>
      <View style={styles.itemTouchable}>
        <Text style={styles.itemTitle}>{title}</Text>
        <View style={styles.itemRight}>
          {type === 'Switch' && (<Switch value={msg} onValueChange={(msg) => onChange(msg)} />)}
        </View>
      </View>
      {type === 'Single_Input' && (
        <TextInput
          {...Define.TextInputArgs}
          defaultValue={dafult}
          placeholder={msg} style={styles.itemInput}
          onChangeText={(text) => onChange(text)} />)}
      {type === 'Multi_Input' && (
        <TextInput
          {...Define.TextInputArgs} multiline
          placeholder={msg}
          defaultValue={dafult}
          onChangeText={(text) => onChange(text)}
          style={[styles.itemInput, { borderRadius: 8, height: 64, textAlignVertical: 'top' }]} />)}
      {type === 'Single_Select' && (
        <TouchableOpacity onPress={onChange} activeOpacity={0.7} style={styles.itemSelect}>
          <Text style={styles.itemMsg}>{msg}</Text>
          <View style={styles.itemRight}>
            {Icons.Generator.Material('chevron-right', 24, '#bbb')}
          </View>
        </TouchableOpacity>
      )}
    </View>
  )
}
const styles = StyleSheet.create({
  header: {
    marginVertical: 20, 
    alignItems: 'center', 
    justifyContent: 'center'
  },
  bottom: {
    marginTop: 20, 
    marginBottom: Define.system.ios.x ? 20 + 22 : 22, 
    alignItems: 'center', 
    justifyContent: 'center'
  },
  bottomButton: {
    backgroundColor: DCColor.BGColor('green'), 
    width: width - 80, height: 50, 
    borderRadius: 25, 
    alignItems: 'center', 
    justifyContent: 'center'
  },
  headerCircle: {
    height: 80, 
    width: 80, 
    borderRadius: 40, 
    backgroundColor: '#A6A6A6', 
    alignItems: 'center', 
    justifyContent: 'center'
  },
  itemTouchable: {
    flexDirection: 'row', 
    justifyContent: 'space-between', 
    alignItems: 'center', 
    height: 44
  },
  itemValue: {
    color: '#666', 
    fontSize: TextFont.TextSize(14), 
    marginLeft: 10
  },
  itemInput: {
    backgroundColor: '#F6F6F6', 
    borderRadius: 4, 
    paddingLeft: 10, 
    paddingVertical: 6
  },
  itemTitle: {
    color: '#666', 
    fontSize: TextFont.TextSize(14), 
    width: width - 100
  },
  itemMsg: {
    color: '#333', 
    fontSize: TextFont.TextSize(14), 
    marginLeft: 10
  },
  itemSelect: {
    flexDirection: 'row', 
    justifyContent: 'space-between', 
    alignItems: 'center', 
    height: 44, 
    backgroundColor: '#F6F6F6', 
    borderRadius: 4
  },
  itemRight: {
    justifyContent: 'center', 
    flexDirection: 'row', 
    alignItems: 'center', 
    width: 60
  }
})
