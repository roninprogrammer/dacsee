import React, { Component } from 'react'
import { Dimensions, StyleSheet, Text, View, Image, ListView, TouchableOpacity, RefreshControl, SafeAreaView, InteractionManager, TextInput } from 'react-native'
import { inject, observer } from 'mobx-react'
import { observable } from 'mobx'
import Material from 'react-native-vector-icons/MaterialIcons'
import { TextFont, DCColor } from 'dacsee-utils'
import { toastLong } from '../../global/method/ToastUtil'
import Resources from 'dacsee-resources'

const { width } = Dimensions.get('window')
const dataContrast = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })

const styles = StyleSheet.create({
  avatar: {
    width: 56,
    height: 56,
    borderRadius: 28
  },

  listCont: {
    height: 84,
    borderRadius: 6,
    paddingHorizontal: 10,
    backgroundColor: 'white',
    alignItems: 'center',
    flexDirection: 'row',
    borderBottomColor: 'black',
    borderBottomWidth: 0.3,
  },

  onlineCircle: {
    right: 2,
    bottom: 1,
    position: 'absolute',
    backgroundColor: DCColor.BGColor('green'),
    width: 16,
    height: 16,
    borderRadius: 8
  },

  offlineCircle: {
    right: 2,
    bottom: 1,
    position: 'absolute',
    backgroundColor: '#cdcdcd',
    width: 16,
    height: 16,
    borderRadius: 8
  },

  bottomButton: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    paddingHorizontal: 40,
    margin: 10,
    borderRadius: 5
  },

  shadow: {
    elevation: 2,
    shadowOffset: { width: 0, height: 0 },
    shadowColor: 'rgba(0,0,0,0.4)',
    shadowOpacity: 0.4,
    shadowRadius: 3
  },

  circle: {
    backgroundColor: 'rgba(0,0,0,0.1)',
    width: 25,
    height: 25,
    borderRadius: 12.5,
    alignItems: 'center',
    justifyContent: 'center',
  },

  sheader: {
    flexDirection: 'row',
    height: 10,
    alignItems: 'center',
    paddingLeft: 15,
    backgroundColor: 'rgba(0,0,0,0.05)',
    marginBottom: 10,
  },
  search_bar: {
    flexDirection: 'row',
    marginVertical: 10,
    backgroundColor: 'rgba(0,0,0,0.05)',
    borderRadius: 10,
    alignItems: 'center'
  },
  subCont: {
    flexDirection: 'row',
    width: '100%',
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative',
    paddingHorizontal: 15,
    shadowOffset: { width: 0, height: -1 },
    shadowColor: '#000',
    shadowOpacity: 0.2
  },
  imgCont: {
    padding: 10,
    borderBottomLeftRadius: 5,
    borderTopLeftRadius: 5,
  },
  dcImg: {
    height: 35,
    width: 35,
    resizeMode: 'contain',
  },
  dcText: {
    flex: 1,
    fontWeight: '400',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: TextFont.TextSize(12),
    lineHeight: TextFont.TextSize(13),
    color: 'rgba(0, 0, 0, 0.5)',
    fontWeight: '500',
    marginLeft: 10,
    marginRight: 10,
  },
  dcIcon: {
    width: 8,
    height: 8, resizeMode: 'contain',
    marginRight: 10
  },

})

@inject('app', 'circle')
@observer
export default class Friend extends Component {


  static navigationOptions = ({ navigation }) => {
    const { strings } = $.store.app
    return {
      drawerLockMode: 'locked-closed',
      title: strings.recommend_friend
    }
  }

  @observable isSearch: Boolean = false
  @observable search_value: String = ''
  @observable searchFriends: Array = []
  constructor(props) {
    super(props)
    const { friendID, rowID } = this.props.navigation.state.params
    $.store.circle.checkedData = false
    this.friendData = props.circle.friendData
    this.selectAllFriendToggle = props.circle.selectAllFriendToggle
    this.getFriends = props.circle.getFriends
    const { select_friends = [] } = this.friendData
    let _friends = this.friendData.friends.map(pipe => Object.assign({}, pipe, {
      checked: typeof (select_friends) === 'string' ? false : select_friends.find(sub => sub._id === pipe._id) !== undefined
    }))
    const dataFriends = _friends.filter(function (item) {
      return item.friend_id !== friendID && item.friend_info._id !== friendID;
    })
    this.friendData.check_all_friend = dataFriends
    this.state = {
      friendID: friendID
    }
  }
  componentDidMount() {
    InteractionManager.runAfterInteractions()
    this.friendData.allFriend = false
    $.store.circle.pressSend = true
    // this.getFriends()
  }
  onChangeText(text) {
    if (text === '') {
      this.isSearch = false
    } else {
      if (!this.isSearch) this.isSearch = true
      this.searchFriend(text)
    }
    this.search_value = text
  }

  async searchFriend(value: String) {
    const { check_all_friend } = this.props.circle.friendData
    try {
      const data = await $.method.session.Circle.Get(`v1/circleSearch?fullName=${value}`)
      let friends = data.friends.map(pipe => Object.assign({}, pipe, {
        checked: check_all_friend.find(sub => sub._id === pipe._id).checked
      }))
      this.searchFriends = friends || []
    } catch (e) {
      this.searchFriends = []
    }
  }

  onPressCheckFriend(itemData: Object, index: Int) {
    if (this.isSearch) {
      let _clone = this.searchFriends.slice()
      _clone[index].checked = !_clone[index].checked
      this.searchFriends = _clone
      index = this.friendData.check_all_friend.findIndex((item) => item._id === itemData._id)
    }
    this.props.circle.onPressCheckFriend(itemData, index)
  }

  async sendFriendSuggest(friendID) {
    try {
      if ($.store.circle.pressSend === true) {
        $.store.circle.pressSend = false
        const res = await $.store.circle.sendSuggestFriend(friendID)
        if (res) {
          // Alert.alert('Suggested Friend Sent Successful', 'The suggested friend have successful sent to selected friends');
          await this.getFriends()
          $.store.circle.online = false
          $.store.circle.driver = false
          toastLong($.store.app.strings.recommend_friend_sent_success)
          this.props.navigation.goBack()
        }
        else {
          $.store.circle.pressSend = true
          toastLong($.store.app.strings.recommend_friend_sent_unsuccess)
        }
      }
    } catch (e) {
      toastLong($.store.app.strings.recommend_friend_sent_unsuccess)
    }
  }

  renderItem(data, index) {
    const { friend_info, checked } = data
    const { _id, userId, avatars, fullName, label = [], labeltype, onlineStatus } = friend_info
    const { avatar, circle, shadow, onlineCircle, offlineCircle, listCont } = styles

    return (
      <View
        activeOpacity={0.7} style={[listCont, shadow]}>
        <View style={{ justifyContent: 'center', marginRight: 10 }}>
          <Image style={avatar} source={{ uri: $.method.utils.getHeaderPicUrl(avatars) }} />
          {/* 2018年6月29日隐藏绿点在线zhuagtai */}
          <View style={onlineStatus === 'user-online' ? onlineCircle : offlineCircle} />
        </View>
        <View style={{ flex: 1, marginRight: 10, justifyContent: 'center' }}>
          <View style={{ justifyContent: 'center' }}>
            <Text ellipsizeMode={'middle'} numberOfLines={1} style={{ fontSize: TextFont.TextSize(15), color: '#333', fontWeight: '400' }}>{fullName}</Text>
            <View style={{ backgroundColor: DCColor.BGColor('grey'), borderRadius: 20, width: 85 }}>
              <Text style={{ fontSize: TextFont.TextSize(11), color: 'white', fontWeight: '400', margin: 5, paddingLeft: 5 }}>{userId}</Text>
            </View>
          </View>
        </View>
        <TouchableOpacity onPress={() => this.onPressCheckFriend(data, parseInt(index))}
          activeOpacity={0.7} style={{ width: 25, height: 84, alignItems: 'center', justifyContent: 'center' }}>
          <View style={[circle, { backgroundColor: checked ? DCColor.BGColor('green') : '#e7e7e7' }]}>
            {checked ? <Material name='check' color='white' size={18} /> : null}
          </View>
        </TouchableOpacity>
      </View>
    )
  }

  render() {
    const { strings } = this.props.app
    const { sheader, bottomButton, circle, subCont, imgCont, dcImg, dcText } = styles

    const { check_all_friend = [], allFriend, search_friends = [] } = this.friendData
    // const _check = check_all_friend.filter(item => item.checked)
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: 'white' }}>
        <View style={{ paddingHorizontal: 15 }}>
          <HeaderSearchBar
            name={strings.search_name_phone_email}
            search_value={this.search_value}
            isSearch={this.isSearch}
            onChangeText={(text) => this.onChangeText(text)} />
        </View>
        {(!this.isSearch) &&
          <View style={{ height: 40, width, paddingHorizontal: 10, flexDirection: 'row', justifyContent: 'space-between' }}>
            <Text style={{ fontSize: 20, color: 'black', fontWeight: '600' }}>{strings.select_all}</Text>
            {!this.isSearch && <TouchableOpacity onPress={() => this.selectAllFriendToggle()} activeOpacity={0.7}
              style={[styles.circle, { backgroundColor: allFriend ? DCColor.BGColor('green') : '#e7e7e7' }]}>
              {allFriend && <Material name='check' color='white' size={18} />}
            </TouchableOpacity>}
          </View>
        }
        <View style={sheader} />
        {/* <View style={sheader}/> */}
        <View style={{ flex: 1 }}>
          {(this.isSearch && this.searchFriends.length === 0) ?
            <View style={subCont}>
              <View style={imgCont}>
                <Image style={dcImg} source={Resources.image.attention_icon} />
              </View>
              <Text style={dcText}>{strings.no_found_friends}</Text>
            </View>
            : null
          }
          <ListView
            enableEmptySections
            refreshControl={<RefreshControl
              refreshing={this.props.circle.circleLoading}
              onRefresh={() => this.componentDidMount()}
              title={strings.pull_refresh}
              colors={['#ffffff']}
              progressBackgroundColor={DCColor.BGColor('primary-1')}
            />}
            dataSource={this.isSearch
              ? dataContrast.cloneWithRows(this.searchFriends.slice())
              : dataContrast.cloneWithRows(check_all_friend.slice())}
            renderRow={(data, section, rowId) => this.renderItem(data, rowId)}
          />
        </View>

        <View style={{ justifyContent: 'flex-end' }}>
          <TouchableOpacity style={[bottomButton, { backgroundColor: $.store.circle.checkedData === true ? DCColor.BGColor('primary-1') : '#cdcdcd' }]} disabled={$.store.circle.checkedData === true ? false : true} onPress={() => this.sendFriendSuggest(this.state.friendID)}>
            <Text style={{ fontSize: 20, color: 'black', fontWeight: '600' }}>{$.store.app.strings.send}</Text>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    )
  }
}
const HeaderSearchBar = (props) => {
  const { name, onChangeText = () => { }, isSearch } = props
  return (
    <View style={[styles.shadow, styles.search_bar]}>
      <View style={{ width: 40, height: 32, alignItems: 'center', justifyContent: 'center' }}>
        <Material name='search' size={18} />
      </View>
      <TextInput
        ref={e => this.input = e}
        underlineColorAndroid={'transparent'}
        placeholderTextColor={'#D1D1DE'}
        placeholder={name}
        // onFocus={onFocus}
        style={{ height: 45, width: width - (isSearch ? 110 : 70), paddingLeft: 0, color: '#5c5c5c' }}
        onChangeText={(text) => onChangeText(text)} />
      <View style={{ width: 40, height: 32, alignItems: 'center', justifyContent: 'center' }}
      >
        {isSearch
          ? <TouchableOpacity style={{ width: 30, height: 30, borderRadius: 18, justifyContent: 'center', alignItems: 'center', backgroundColor: DCColor.BGColor('red') }} activeOpacity={0.7}
            onPress={() => {
              this.input.clear()
              this.input.blur()
              onChangeText('')
              setTimeout(() => { this.input.setNativeProps({ text: '' }) })
            }}>
            <Material name='close' color='#fff' size={18} />
          </TouchableOpacity> : null
        }
      </View>
    </View>
  )
}
