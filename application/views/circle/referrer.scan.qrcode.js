import React, { Component } from 'react'
import { View, TouchableOpacity, StyleSheet, ActivityIndicator } from 'react-native'
import { RNCamera } from 'react-native-camera'
import { Icons, DCColor, Screen, Define } from 'dacsee-utils'
import Material from 'react-native-vector-icons/MaterialIcons'
import { inject, observer } from 'mobx-react'


const { width } = Screen.window

const flashModeOrder = {
  off: 'on',
  on: 'auto',
  auto: 'torch',
  torch: 'off',
};

@inject('app')
@observer
class NavigatorHeaderLeftButton extends Component {
  render() {
    return (
      <TouchableOpacity
        activeOpacity={0.7}
        style={{ top: 1, width: 54, paddingLeft: 8, justifyContent: 'center', alignItems: 'flex-start' }}
        onPress={() => this.props.navigation.goBack()}
      >
        <Material name={'keyboard-arrow-left'} size={30} color={DCColor.BGColor('primary-2')} />
      </TouchableOpacity>
    )
  }
}

@inject('app', 'circle')
@observer
export default class ReferrerScanQRCode extends Component {
  
  static navigationOptions = ({ navigation }) => {
    return {
      drawerLockMode: 'locked-closed',
      headerStyle: {
        backgroundColor: DCColor.BGColor('primary-1'),
        shadowColor: 'transparent',
        shadowOpacity: 0,
        borderBottomWidth: 0,
        borderBottomColor: 'transparent',
        elevation: 0
      },
      headerLeft: (
        <NavigatorHeaderLeftButton navigation={navigation} />
      )
    }
  }

  constructor (props) {
    super(props)
    this.state = {
      active: true,
      flash: 'off',
    }
  }

  componentDidMount () {
    console.log('开始')
  }

  toggleFlash() {
    this.setState({
      flash: flashModeOrder[this.state.flash],
    });
  }

  _scanResult (code) {
    if (code.data) {
      this.setState({active: false})
      const url = code.data
      var params = url.split('?')[1]
      var referrer = ''
      
      if (params) {
        params = params.split('&')

        for (var i = 0; i < params.length; i++) {
          var a = params[i].split('=')
          var paramName = a[0];

          if (paramName === 'ref' || paramName === 'referrer') {
            referrer = a[1]
          }
        }
      }

      if (referrer.length == 10) {
        $.store.account.loginData.referralUserId = referrer
      } else {
        $.define.func.showMessage('Invalid QR Code')
      }
      
      this.props.navigation.navigate('Login')
    }
  }

  render () {
    const { active } = this.state
    return (
      <View style={{ flex: 1 }}>
        { 
          active ? (
            <RNCamera
              BarCodeType={RNCamera.Constants.BarCodeType.qr}
              flashMode={this.state.flash}
              style={{ flex: 1 }}
              ref={cam => { this.camera = cam }}
              flashMode={ this.state.isTorchOn ? RNCamera.Constants.FlashMode.torch : RNCamera.Constants.FlashMode.off}
              androidCameraPermissionOptions={{
                title: this.props.app.strings.refuse_visit_camera,
                message: this.props.app.strings.pls_allow_camera_try
              }}
              onBarCodeRead={(code) => this._scanResult(code)}
            >
              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <View style={{ top: Define.system.ios.x ? -44 : -22, width: width * 0.6, height: width * 0.6 }}>
                  <View style={[styles.codeBorder, { top: 0, left: 0, height: 1.2, width: width * 0.2 }]} />
                  <View style={[styles.codeBorder, { top: 0, left: 0, height: width * 0.2, width: 1.2 }]} />

                  <View style={[styles.codeBorder, { bottom: 0, left: 0, height: width * 0.2, width: 1.2 }]} />
                  <View style={[styles.codeBorder, { bottom: 0, left: 0, height: 1.2, width: width * 0.2 }]} />

                  <View style={[styles.codeBorder, { bottom: 0, right: 0, height: width * 0.2, width: 1.2 }]} />
                  <View style={[styles.codeBorder, { bottom: 0, right: 0, height: 1.2, width: width * 0.2 }]} />

                  <View style={[styles.codeBorder, { top: 0, right: 0, height: width * 0.2, width: 1.2 }]} />
                  <View style={[styles.codeBorder, { top: 0, right: 0, height: 1.2, width: width * 0.2 }]} />
                </View>
              </View>
              <View style={{ position: 'absolute', bottom: 48, left: 0, right: 0, justifyContent: 'center', alignItems: 'center' }}>
                <TouchableOpacity onPress={this.toggleFlash.bind(this)}  activeOpacity={0.7} style={{ backgroundColor: '#ffffffcc', height: 44, width: 44, justifyContent: 'center', alignItems: 'center', borderRadius: 22 }}>
                  { Icons.Generator.Material('lightbulb-outline', 28, '#666') }
                </TouchableOpacity>
              </View>
            </RNCamera>
          ) : (
            <View style={{flex: 1, backgroundColor: 'black', justifyContent: 'center', alignItems: 'center'}}>
              <ActivityIndicator />
            </View>
          )
        }
      </View>
    )
  }



}

const styles = StyleSheet.create({
  codeBorder: { backgroundColor: '#ffffffcc', position: 'absolute' }
})
