import React, { Component } from 'react'
import {
  Dimensions,
  StyleSheet,
  Text,
  View,
  Image,
  ListView,
  TextInput,
  TouchableOpacity,
  RefreshControl,
  SafeAreaView,
  InteractionManager
} from 'react-native'
import Resources from 'dacsee-resources'
import { inject, observer } from 'mobx-react'
import { observable } from 'mobx'
import Material from 'react-native-vector-icons/MaterialIcons'
import { NavigationActions } from 'react-navigation'
import { TextFont, DCColor } from 'dacsee-utils'
import { toastLong } from '../../global/method/ToastUtil'

const { width } = Dimensions.get('window')
const dataContrast = new ListView.DataSource(
  {
    rowHasChanged: (r1, r2) => r1 !== r2,
    sectionHeaderHasChanged: (s1, s2) => s1 !== s2
  })
@inject('app', 'circle')
@observer
export default class Friend extends Component {
  static navigationOptions = { header: null }
  @observable isSearch: Boolean = false
  @observable search_value: String = ''
  constructor(props) {
    super(props)
    this.friendData = props.circle.friendData
    this.toggerSelectAll = props.circle.toggerSelectAll
    this.onPressCheck = props.circle.onPressCheck
    this.confirmFriendSelect = props.circle.confirmFriendSelect
    this.friendRequest = props.circle.friendRequest
    this.getFriends = props.circle.getFriends
    this.getFriendRequests = props.circle.getFriendRequests
    this.searchFriend = props.circle.searchFriend
  }
  componentDidMount() {
    InteractionManager.runAfterInteractions()
    if ($.store.circle.online === true && $.store.circle.driver === true) {
      this.getFriends('online', true)
    }
    else if ($.store.circle.online === true && $.store.circle.driver === false) {
      this.getFriends('online', false)
    }
    else if ($.store.circle.online === false && $.store.circle.driver === true) {
      this.getFriends('all', true)
    }
    else {
      this.getFriends()
    }
    this.getFriendRequests()
  }

  renderFooter = () => {
    const { strings } = this.props.app
    return (
      (this.friendData.friends.length === 0 && this.friendData.requestors.length === 0 && $.store.circle.filter === false)
        ? <View>
          <View style={{ marginTop: 60, alignItems: 'center' }}>
            <Image style={{ marginBottom: 18 }} source={require('../../resources/images/friend-empty-state.png')} />
            <Text style={{ color: '#666', fontSize: 22, fontWeight: '600', textAlign: 'center', marginBottom: 6 }}>
              {strings.no_friend}
            </Text>
            <Text style={{ color: '#999', fontSize: 22, fontWeight: '400', textAlign: 'center' }}>
              {strings.clickto_add_friend}
            </Text>
          </View>
        </View>
        : null
    )
  }

  async changeFilter(type) {
    if (type === 'online') {
      if ($.store.circle.online === true) {
        if ($.store.circle.driver === true) {
          $.store.circle.online = false
          await this.getFriends('all', true)
          if (this.isSearch) {
            this.searchFriend(this.search_value, false, true)
          }
        }
        else {
          $.store.circle.online = false
          await this.getFriends('all', false)
          if (this.isSearch) {
            this.searchFriend(this.search_value, false, false)
          }
        }
      }
      else {
        if ($.store.circle.driver === true) {
          $.store.circle.online = true
          await this.getFriends('online', true)
          if (this.isSearch) {
            this.searchFriend(this.search_value, true, true)
          }
        }
        else {
          $.store.circle.online = true
          await this.getFriends('online', false)
          if (this.isSearch) {
            this.searchFriend(this.search_value, true, false)
          }
        }
      }
    }
    else if (type === 'driver') {
      if ($.store.circle.driver === true) {
        if ($.store.circle.online === true) {
          $.store.circle.driver = false
          await this.getFriends('online', false)
          if (this.isSearch) {
            this.searchFriend(this.search_value, true, false)
          }
        }
        else {
          $.store.circle.driver = false
          await this.getFriends('all', false)
          if (this.isSearch) {
            this.searchFriend(this.search_value, false, false)
          }
        }
      }
      else {
        if ($.store.circle.online === true) {
          $.store.circle.driver = true
          await this.getFriends('online', true)
          if (this.isSearch) {
            this.searchFriend(this.search_value, true, true)
          }
        }
        else {
          $.store.circle.driver = true
          await this.getFriends('all', true)
          if (this.isSearch) {
            this.searchFriend(this.search_value, false, true)
          }
        }
      }
    }
  }

  renderRequest(data) {
    const { _id, requestor_id, requestor_info } = data
    const { user_id = requestor_info._id, userId, fullName, label = [], avatars, labeltype, onlineStatus } = requestor_info
    return (
      <TouchableOpacity onPress={() =>
        global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'FriendsDetail', params: { requestData: data, type: 'REQUEST' } }))
      } activeOpacity={0.7} style={[{ paddingHorizontal: 10, height: 84, borderRadius: 6, backgroundColor: 'white', justifyContent: 'space-between', alignItems: 'center', flexDirection: 'row', marginBottom: 10 }, styles.shadow]}>
        <View style={{ justifyContent: 'center', marginRight: 10 }}>
          <Image style={{ width: 56, height: 56, borderRadius: 28 }} source={{ uri: $.method.utils.getHeaderPicUrl(avatars) }} />
          <View style={{ right: 2, bottom: 1, position: 'absolute', backgroundColor: onlineStatus === 'user-online' ? DCColor.BGColor('green') : '#cdcdcd', width: 16, height: 16, borderRadius: 8 }} />
        </View>
        <View style={{ flex: 1, marginRight: 10, justifyContent: 'center' }}>
          <View style={{ justifyContent: 'center' }}>
            <Text ellipsizeMode={'middle'} numberOfLines={1} style={{ fontSize: 14, color: '#333', fontWeight: '400' }}>{fullName}</Text>
            <Text style={{ fontSize: 14, color: '#333', fontWeight: '400', marginTop: 5 }}>{userId}</Text>
          </View>
        </View>
        <View style={{ marginRight: 0, flexDirection: 'row', justifyContent: 'space-between' }}>
          <TouchableOpacity onPress={() => this.friendRequest('reject', _id)} activeOpacity={0.7} style={{ marginRight: 15, width: 30, height: 30, borderRadius: 18, backgroundColor: DCColor.BGColor('red'), justifyContent: 'center', alignItems: 'center' }}>
            <Material name='close' color='white' size={18} />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.friendRequest('accept', _id)} activeOpacity={0.7} style={{ width: 30, height: 30, borderRadius: 18, backgroundColor: DCColor.BGColor('green'), justifyContent: 'center', alignItems: 'center' }}>
            <Material name='check' color='white' size={18} />
          </TouchableOpacity>
        </View>
      </TouchableOpacity>
    )
  }
  renderItem(data, section, rowID) {

    const { friend_info } = data
    const { _id, userId, avatars, fullName, onlineStatus } = friend_info
    //console.log(data)
    return (
      <TouchableOpacity onPress={() => {
        global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'ChatWindow', params: { friendData: data } }))
      }
      } activeOpacity={0.7} style={[{
        height: 84,
        borderRadius: 6,
        paddingHorizontal: 10,
        backgroundColor: 'white',
        alignItems: 'center',
        flexDirection: 'row',
        // marginBottom: 10
        borderBottomColor: 'black',
        borderBottomWidth: 0.3,
      }, styles.shadow]}>
        <View style={{ justifyContent: 'center', marginRight: 10 }}>
          <Image style={{ width: 56, height: 56, borderRadius: 28 }} source={{ uri: $.method.utils.getHeaderPicUrl(avatars) }} />
          {/* 2018年6月29日隐藏绿点在线zhuagtai */}
          <View style={{ right: 2, bottom: 1, position: 'absolute', backgroundColor: onlineStatus === 'user-online' ? DCColor.BGColor('green') : '#cdcdcd', width: 16, height: 16, borderRadius: 8 }} />
        </View>
        <View style={{ flex: 1, marginRight: 10, justifyContent: 'center' }}>
          <View style={{ justifyContent: 'center' }}>
            <Text ellipsizeMode={'middle'} numberOfLines={1} style={{ fontSize: TextFont.TextSize(15), color: '#333', fontWeight: '400' }}>{fullName}</Text>
            <View style={{ backgroundColor: DCColor.BGColor('grey'), borderRadius: 20, width: 85 }}>
              <Text style={{ fontSize: TextFont.TextSize(11), color: 'white', fontWeight: '400', margin: 5, paddingLeft: 5 }}>{userId}</Text>
            </View>
          </View>
        </View>
        <TouchableOpacity style={{ width: 35, height: 35, alignItems: 'center', justifyContent: 'center', backgroundColor: DCColor.BGColor('primary-1'), borderRadius: 5 }} onPress={() => ($.store.circle.friendData.friend_count) > 1
          ? global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'MultipleFriends', params: { friendID: _id, rowID: rowID } }))
          : toastLong($.store.app.strings.no_friend_to_share)
        }>
          <Material name='share' size={18} />
        </TouchableOpacity>
      </TouchableOpacity>
    )
  }
  onChangeText(text) {
    if (text === '') {
      this.isSearch = false
    } else {
      if (!this.isSearch) this.isSearch = true
      this.searchFriend(text, $.store.circle.online, $.store.circle.driver)
    }
    this.search_value = text
  }
  render() {
    const { strings } = this.props.app
    const { loading = false, requestors = [], friends = [], search_friends = [], friend_lists = [] } = this.friendData
    const { subCont, imgCont, dcImg, dcText } = styles
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: 'white' }}>
        <View style={{ paddingHorizontal: 15 }}>
          <HeaderSearchBar
            name={strings.search_name_phone_email}
            search_value={this.search_value}
            isSearch={this.isSearch}
            onChangeText={(text) => this.onChangeText(text)} />
        </View>
        <View style={styles.sheader}>
          <TouchableOpacity style={[styles.box, { backgroundColor: $.store.circle.online === true ? DCColor.BGColor('primary-1') : 'rgba(0,0,0,0.05)' }]} onPress={() => this.changeFilter('online')}>
            <Text style={{ fontSize: TextFont.TextSize(15), color: $.store.circle.online === true ? 'black' : 'rgba(0,0,0,0.15)', fontWeight: 'bold' }}>{strings.online}</Text>
          </TouchableOpacity>
          <TouchableOpacity style={[styles.box, { backgroundColor: $.store.circle.driver === true ? DCColor.BGColor('primary-1') : 'rgba(0,0,0,0.05)' }]} onPress={() => this.changeFilter('driver')}>
            <Text style={{ fontSize: TextFont.TextSize(15), color: $.store.circle.driver === true ? 'black' : 'rgba(0,0,0,0.15)', fontWeight: 'bold' }}>{strings.driver}</Text>
          </TouchableOpacity>
        </View>
        <View style={{ flex: 1 }}>
          {((this.isSearch && search_friends.length === 0) || (($.store.circle.online || $.store.circle.driver) && friend_lists.length === 0)) && $.store.circle.filter === true ?
            <View style={subCont}>
              <View style={imgCont}>
                <Image style={dcImg} source={Resources.image.attention_icon} />
              </View>
              <Text style={dcText}>{strings.no_found_friends}</Text>
            </View>
            : null
          }
          <ListView
            removeClippedSubviews={false}
            contentContainerStyle={{ paddingHorizontal: 15 }}
            enableEmptySections
            refreshControl={<RefreshControl
              refreshing={this.props.circle.circleLoading}
              onRefresh={() => this.componentDidMount()}
              title={strings.pull_refresh}
              colors={['#ffffff']}
              progressBackgroundColor={DCColor.BGColor('primary-1')}
            />}
            // renderSectionHeader={(data, section) => this.renderSectionHeader(data, section)}
            renderFooter={this.renderFooter}
            dataSource={this.isSearch
              ? dataContrast.cloneWithRows(search_friends.slice())
              : dataContrast.cloneWithRowsAndSections([requestors.slice(), friend_lists.slice()])}
            renderRow={(data, section, rowId) => this.isSearch
              ? this.renderItem(data, section, rowId)
              : section === '0' ? this.renderRequest(data)
                : this.renderItem(data, section, rowId)}
          />
        </View>
      </SafeAreaView>
    )
  }
}

const HeaderSearchBar = (props) => {
  const { name, onChangeText = () => { }, isSearch } = props
  return (
    <View style={[styles.shadow, styles.search_bar]}>
      <View style={{ width: 40, height: 32, alignItems: 'center', justifyContent: 'center' }}>
        <Material name='search' size={18} />
      </View>
      <TextInput
        ref={e => this.input = e}
        underlineColorAndroid={'transparent'}
        placeholderTextColor={'#D1D1DE'}
        placeholder={name}
        style={{ height: 45, width: width - (isSearch ? 110 : 70), paddingLeft: 0, color: '#5c5c5c' }}
        onChangeText={(text) => onChangeText(text)} />
      <View style={{ width: 40, height: 32, alignItems: 'center', justifyContent: 'center' }}
      >
        {isSearch
          ? <TouchableOpacity style={{ width: 30, height: 30, borderRadius: 18, justifyContent: 'center', alignItems: 'center', backgroundColor: DCColor.BGColor('red') }} activeOpacity={0.7}
            onPress={() => {
              // this.input.clear()
              // this.input.blur()
              onChangeText('')
              setTimeout(() => { this.input.setNativeProps({ text: '' }) })
            }}>
            <Material name='close' color='#fff' size={18} />
          </TouchableOpacity> : null
        }

      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  bottomButton: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 84,
    width: width,
    paddingHorizontal: 40,
    marginTop: -84
  },
  confirmButton: {
    justifyContent: 'center',
    alignItems: 'center',
    width: width - 120,
    height: 60,
    borderRadius: 12,
    backgroundColor: '#45e2be'
  },
  search_bar: {
    flexDirection: 'row',
    marginVertical: 10,
    backgroundColor: 'rgba(0,0,0,0.05)',
    borderRadius: 10,
    alignItems: 'center'
  },
  circle: {
    width: 23,
    height: 23,
    borderRadius: 13,
    justifyContent: 'center',
    alignItems: 'center'

  },
  shadow: {
    elevation: 2,
    shadowOffset: { width: 0, height: 0 },
    shadowColor: 'rgba(0,0,0,0.4)',
    shadowOpacity: 0.4,
    shadowRadius: 3
  },
  button_contain: {
    backgroundColor: '#45e2be',
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 20,
    height: 50,
    width: width - 80
  },
  edit_contain: {
    backgroundColor: '#f6f5fb',
    borderRadius: 10,
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 10
  },
  sheader: {
    flexDirection: 'row',
    height: 50,
    alignItems: 'center',
    paddingHorizontal: 10,
    paddingVertical: 20,
    backgroundColor: 'rgba(0,0,0,0.05)',
    marginBottom: 5,
  },
  box: {
    borderRadius: 8,
    flexDirection: 'row',
    height: 35,
    marginLeft: 7,
    alignItems: 'center',
    justifyContent: 'center',
    width: 85
  },
  subCont: {
    flexDirection: 'row',
    width: '100%',
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative',
    paddingHorizontal: 15,
    shadowOffset: { width: 0, height: -1 },
    shadowColor: '#000',
    shadowOpacity: 0.2
  },
  imgCont: {
    padding: 10,
    borderBottomLeftRadius: 5,
    borderTopLeftRadius: 5,
  },
  dcImg: {
    height: 35,
    width: 35,
    resizeMode: 'contain',
  },
  dcText: {
    flex: 1,
    fontWeight: '400',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: TextFont.TextSize(12),
    lineHeight: TextFont.TextSize(13),
    color: 'rgba(0, 0, 0, 0.5)',
    fontWeight: '500',
    marginLeft: 10,
    marginRight: 10,
  },
  dcIcon: {
    width: 8,
    height: 8, resizeMode: 'contain',
    marginRight: 10
  },
})
