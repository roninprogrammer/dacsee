import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { StyleSheet, ScrollView, View, Text, SafeAreaView, TouchableOpacity, Linking } from 'react-native'
import { Screen, TextFont, DCColor } from '../../utils'

const { width } = Screen.window

@inject('app', 'account')
@observer
export default class EmergencyContact extends Component {

  static navigationOptions = () => {
    const { strings } = $.store.app
    return {
      title: strings.settings_account_emergency_contact,
      drawerLockMode: 'locked-closed'
    }
  }

  constructor(props) {
    super(props)
    this.state = {
      contactInfo: []
    }
  }

  async componentDidMount() {
    this.props.app.showLoading()
    const resp = await $.method.session.User.Get('v1/contacts?type=emergency');

    if (resp.length > 0) {
      this.setState({ contactInfo : resp[0]})
    }
    this.props.app.hideLoading()
  }

  render() {
    const { mainCont, formCont, fTitle, fCaption, actionCont, updateCont, submitText } = styles
    const { strings } = $.store.app
    const { name , relationship, phoneCountryCode, phoneNo, email } = this.state.contactInfo

    return (
      <SafeAreaView style={mainCont}>
        <ScrollView>
          <View style={formCont}>
            <Text style={fTitle}>{strings.settings_account_emergency_contact_name}</Text>
            <Text style={fCaption}>{name ? name : strings.no_content}</Text>
          </View>

          <View style={formCont}>
            <Text style={fTitle}>{strings.settings_account_emergency_contact_relationship}</Text>
            <Text style={fCaption}>{relationship ? relationship : strings.no_content}</Text>
          </View>

          <View style={formCont}>
            <Text style={fTitle}>{strings.settings_account_emergency_contact_phone_number}</Text>
            <Text style={fCaption}>{phoneNo ? `${phoneCountryCode} ${phoneNo}` : strings.no_content}</Text>
          </View>

          <View style={formCont}>
            <Text style={fTitle}>{strings.settings_account_emergency_contact_email_address}</Text>
            <Text style={fCaption}>{email ? email : strings.no_content}</Text>
          </View>
        </ScrollView>
        <View style={actionCont}>
          <TouchableOpacity style={updateCont} onPress={() => Linking.openURL('http://bit.ly/dacsee-updatedocument')}>
            <Text style={submitText}>{strings.update_driver_document}</Text>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  mainCont: {
    width,
    height: '100%',
    flexDirection: 'column',
    flexWrap: 'nowrap',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  formCont: {
    width,
    padding: 20,
    backgroundColor: 'white',
    borderBottomColor: 'rgba(0, 0, 0, 0.05)',
    borderBottomWidth: 2
  },
  fTitle: {
    color: '#333', 
    fontSize: TextFont.TextSize(15), 
    fontWeight: '500' ,
  },
  fCaption: {
    color: 'rgba(0, 0, 0, 0.45)', 
    fontSize: TextFont.TextSize(12), 
    fontWeight: '400' ,
    paddingTop:10
  },
  actionCont:{
    height: '13%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    backgroundColor: 'white',
    borderTopColor: 'rgba(0, 0, 0, 0.25)',
    borderTopWidth: 2
  },
  updateCont: {
    width: '90%',
    height: 50,
    backgroundColor: DCColor.BGColor('primary-1'),
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 5
  },
  submitText: {
    fontSize: TextFont.TextSize(16),
    fontWeight: '700',
    color: 'rgba(0, 0, 0, 0.75)',
  }
})