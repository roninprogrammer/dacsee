import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { StyleSheet, ScrollView, View, Text, SafeAreaView, TextInput, TouchableOpacity } from 'react-native'
import { Screen, TextFont, DCColor } from 'dacsee-utils'
import SponsorInfo from '../main/sponsorLocation/component/Sponsor.info'
import Resources from 'dacsee-resources'

const { width } = Screen.window
const styles = StyleSheet.create({
  mainCont: {
    width,
    height: '100%',
    flexDirection: 'column',
    flexWrap: 'nowrap',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  scrollCont: {
    width,
    flex: 1
  },
  infoCont: {
    width,
    padding: 5
  },
  formCont: {
    width,
    padding: 20,
    backgroundColor: 'white'
  },
  itemCont: {
    flex: 1,
    marginBottom: 15,
  },
  resetCont: {
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'flex-start'
  },
  fLabel: {
    width: '100%',
    fontSize: TextFont.TextSize(14),
    fontWeight: '800',
    color: 'rgba(0, 0, 0, 0.65)',
    marginBottom: 10
  },
  dcInput: {
    flex: 1,
    borderRadius: 5,
    backgroundColor: 'rgba(0, 0, 0, 0.05)',
    textAlign: 'left',
    height: 45,
    paddingHorizontal: 10,
  },
  forgotCodeCont: {
    width: '100%',
    padding: 10
  },
  forgotText: {
    color: DCColor.BGColor('primary-1'),
    fontSize: TextFont.TextSize(16),
    fontWeight: '700',
    flex: 1,
    textAlign: 'center'
  },
  actionCont: {
    width,
    padding: 20
  },
  dcBtn: {
    flex: 1,
    height: 45,
    borderRadius: 22.5,
    backgroundColor: DCColor.BGColor('primary-1'),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  btnText: {
    fontSize: TextFont.TextSize(15),
    fontWeight: 'bold',
    color: 'rgba(0, 0, 0, 0.75)',
    textAlign: 'center'
  },
  dataCont: {
    flex: 1,
  },
  resendBtn: {
    padding: 10,
    height: 45,
    paddingHorizontal: 15,
    backgroundColor: DCColor.BGColor('primary-1'),
    borderRadius: 5,
    marginLeft: 10,
    alignItems: 'center',
    justifyContent: 'center'
  },
  resetText: {
    fontSize: TextFont.TextSize(15),
    fontWeight: 'bold',
    color: 'rgba(0, 0, 0, 0.75)'
  }
})

@inject('app', 'bank')
@observer
export default class SettingPinReset extends Component {

  static navigationOptions = () => {
    const { strings } = $.store.app
    return {
      title: strings.reset_pin,
      drawerLockMode: 'locked-closed'
    }
  }

  constructor(props) {
    super(props)
    this.state = {
      newCode: '',
      confirmCode: '',
      resetCode: '',
    }
  }

  async onSubmit() {
    const { strings } = this.props.app
    const { resetCode, newCode, confirmCode } = this.state
    const { bank } = this.props
    const pin = {
      resetCode,
      newPin: newCode
    }
    if (resetCode.length === 0 || newCode.length === 0 || confirmCode.length === 0) {
      $.define.func.showMessage(strings.add_bank_error_1)
    } else if (newCode.length < 0) {
      $.define.func.showMessage(strings.pin_error_1)
    } else if (newCode !== confirmCode) {
      $.define.func.showMessage(strings.pin_error_2)
    } else {
      const isSuccess = await bank.resetPin(pin)
      if (isSuccess) {
        this.props.navigation.navigate('SettingPinUpdate')
      }
    }
  }

  render() {
    const {
      mainCont,
      scrollCont,
      infoCont,
      formCont,
      itemCont,
      fLabel,
      dcInput,
      actionCont,
      dcBtn,
      btnText,
      resetCont,
      dataCont,
      resendBtn,
      resetText
    } = styles
    const { strings } = this.props.app
    const { resetCode, newCode, confirmCode } = this.state
    const { bank } = this.props

    return (
      <SafeAreaView style={mainCont}>
        <ScrollView style={scrollCont}>
          <View style={infoCont}>
            <SponsorInfo
              icon={Resources.image.attention_icon}
              label={strings.reset_code_info}
            />
          </View>
          <View style={formCont}>
            <View style={itemCont}>
              <Text style={fLabel}>{strings.new_pin.toUpperCase()}</Text>
              <TextInput
                style={dcInput}
                underlineColorAndroid='transparent'
                returnKeyType='done'
                maxLength={6}
                secureTextEntry={true}
                keyboardType="numeric"
                defaultValue={newCode}
                onChangeText={(text) => {
                  this.setState({
                    ...this.state,
                    newCode: text
                  })
                }}
                placeholder='Please enter here'
              />
            </View>
            <View style={itemCont}>
              <Text style={fLabel}>{strings.confirm_pin.toUpperCase()}</Text>
              <TextInput
                style={dcInput}
                underlineColorAndroid='transparent'
                returnKeyType='done'
                maxLength={6}
                secureTextEntry={true}
                keyboardType="numeric"
                defaultValue={confirmCode}
                onChangeText={(text) => {
                  this.setState({
                    ...this.state,
                    confirmCode: text
                  })
                }}
                placeholder='Please enter here'
              />
            </View>
            <View style={[itemCont, resetCont]}>
              <View style={dataCont}>
                <Text style={fLabel}>{strings.reset_code.toUpperCase()}</Text>
                <TextInput
                  style={dcInput}
                  underlineColorAndroid='transparent'
                  defaultValue={resetCode}
                  autoCapitalize='none'
                  onChangeText={(text) => {
                    this.setState({
                      ...this.state,
                      resetCode: text
                    })
                  }}
                  placeholder='Please enter here'
                />
              </View>
              <TouchableOpacity style={resendBtn} onPress={() => bank.getResetCode()}>
                {bank.counttime <= 0 ?
                  <Text style={resetText}>{strings.send_code.toUpperCase()}</Text>
                  : <Text style={resetText}>{bank.counttime} {strings.second}</Text>
                }

              </TouchableOpacity>
            </View>
          </View>
          <View style={actionCont}>
            <TouchableOpacity style={dcBtn} onPress={() => this.onSubmit()}>
              <Text style={btnText}>{strings.reset.toUpperCase()}</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </SafeAreaView>
    )
  }
}
