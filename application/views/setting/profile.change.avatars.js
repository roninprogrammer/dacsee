/* @flow
 *props: type: oneof['user',group_id],avatars
** global FormData */

import React, { Component } from 'react'
import { View, TouchableOpacity, Image, Alert, StatusBar, ActivityIndicator } from 'react-native'
import ActionSheet from 'react-native-actionsheet'
import ImagePicker from 'react-native-image-picker'
import { Icons, Screen, DCColor, Define, Avatars } from 'dacsee-utils'
import { inject, observer } from 'mobx-react'

const { width } = Screen.window

@inject('app', 'circle', 'account')
@observer
export default class ProfileChangeAvatarsScreen extends Component {


  static navigationOptions = ({ navigation }) => {
    const { strings } = $.store.app
    return {
      title: strings.update_avatar,
      drawerLockMode: 'locked-closed',
      headerRight: (
        <TouchableOpacity
          activeOpacity={0.7}
          style={{ top: -0.5, width: 54, paddingRight: 20, justifyContent: 'center', alignItems: 'flex-end' }}
          onPress={navigation.state.params && navigation.state.params.rightPress}
        >
          {Icons.Generator.Material('more-horiz', 28, DCColor.BGColor('primary-2'), { style: { left: 8 } })}
        </TouchableOpacity>
      ),
      headerStyle: {
        backgroundColor: DCColor.BGColor('primary-1'),
        shadowColor: 'transparent',
        shadowOpacity: 0,
        borderBottomWidth: 0,
        borderBottomColor: 'transparent',
        elevation: 0,
      }
    }
  }

  constructor(props) {
    super(props)
    this.uploadGroupAvatar = props.circle.uploadGroupAvatar
    this.groupData = props.circle.groupData
    this.state = {
      media: {},
      uploading: false
    }
  }

  componentDidMount() {
    this.props.navigation.setParams({
      rightPress: this._show
    })
  }

  componentWillUnmount() {
    this.subscription && this.subscription.remove()
  }

  _show = () => {
    this.ActionSheet.show()
  }

  _getOptions = () => {
    const { strings } = this.props.app
    return {
      title: strings.album,
      storageOptions: { skipBackup: true, path: 'images' },
      quality: 0.8,
      mediaType: 'photo',
      cancelButtonTitle: strings.cancel,
      takePhotoButtonTitle: strings.take_photo,
      chooseFromLibraryButtonTitle: strings.select_from_album,
      allowsEditing: true,
      noData: false,
      maxWidth: 1000,
      maxHeight: 1000,
      permissionDenied: {
        title: strings.refuse_visit, text: strings.pls_auth,
        reTryTitle: strings.retry, okTitle: strings.okay
      }
    }
  }

  _pressActionSheet(index) {
    const { strings } = this.props.app
    if (index === 0) {
      ImagePicker.launchCamera(this._getOptions(), (response) => {
        if (response.didCancel) {
          return
        } else if (response.error) {
          let label = response.error.startsWith('Photo') ? strings.photo : strings.camera
          return Alert.alert(`${strings.pls_auth_fore} ${label} ${strings.pls_auth_back} ${label}。`)
        } else {
          this.setState({
            media: {
              id: `${(new Date).getTime()}`,
              uri: response.uri,
              feature: 'image'
            }
          })
          this.uploadImage(response.data)
        }
      })
    }

    if (index === 1) {
      ImagePicker.launchImageLibrary(this._getOptions(), (response) => {
        if (response.didCancel) {
          return
        } else if (response.error) {
          let label = response.error.startsWith('Photo') ? strings.photo : strings.camera
          return Alert.alert(`${strings.pls_auth_fore} ${label} ${strings.pls_auth_back} ${label}。`)
        } else {
          this.setState({
            media: {
              id: `${(new Date).getTime()}`,
              uri: response.uri,
              feature: 'image'
            }
          })
          this.uploadImage(response.data)
        }
      })
    }
  }

  async uploadImage(base64) {
    this.setState({ uploading: true })
    const { type } = this.props.navigation.state.params
    if (type === 'user') {
      await this.props.account.changeAvatar(base64)
    } else {
      await this.uploadGroupAvatar(type, base64)
    }
    this.setState({ uploading: false })
  }

  render() {
    const { strings } = this.props.app
    const { account, circle } = this.props
    const { media, uploading = false } = this.state
    const { type } = this.props.navigation.state.params
    const avatars = type === 'user' ? account.user.avatars : circle.groupData.groupInfo.avatars
    return (
      <View style={{ flex: 1, backgroundColor: '#000' }}>
        <StatusBar animated={true} hidden={false} backgroundColor={'#151416'} barStyle={'dark-content'} />
        <View style={{ flex: 1, top: Define.system.ios.x ? -44 : -22, justifyContent: 'center', alignItems: 'center' }}>
          <View style={{ width: width, height: width }}>
            <Image style={{ width: width, height: width }} source={{ uri: Avatars.getHeaderPicUrl(avatars, true) }} />
          </View>
        </View>
        {
          uploading && (
            <View style={{ flex: 1, backgroundColor: '#00000066', justifyContent: 'center', alignItems: 'center', position: 'absolute', left: 0, right: 0, top: 0, bottom: 0 }}>
              <View style={{ top: Define.system.ios.x ? -44 : -22 }}>
                <ActivityIndicator size='small' color='#d0d0d0' />
              </View>
            </View>
          )
        }
        <ActionSheet
          ref={e => this.ActionSheet = e}
          options={[strings.take_photo, strings.select_from_album, strings.cancel]}
          cancelButtonIndex={2}
          onPress={this._pressActionSheet.bind(this)}
        />
      </View>
    )
  }
}