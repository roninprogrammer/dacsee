import React, { Component } from 'react'
import { StyleSheet, Text, View, Platform, Clipboard, TouchableOpacity, ScrollView } from 'react-native'
import QRCode from 'react-native-qrcode-svg'
import { inject, observer } from 'mobx-react'
import { SvgIcon, iconPath } from '../chat/chatIcon'
import { Screen, DCColor, TextFont } from 'dacsee-utils'

const { width } = Screen.window

@inject('app', 'account')
@observer
export default class SettingQrCode extends Component {

  static navigationOptions = ({ navigation }) => {
    const { title } = navigation.state.params
    return {
      drawerLockMode: 'locked-closed',
      title: title,
    }
  }

  constructor(props) {
    super(props)
  }

  render() {
    const { user } = this.props.account
    const { userId, _id, fullName } = user
    const { strings } = this.props.app
    return (
      <ScrollView style={{ backgroundColor: '#F3F3F3' }}>
        <View style={styles.container}>

          <Text style={{ textAlign: 'center', fontSize: TextFont.TextSize(14), color: '#000', opacity: 0.7, paddingHorizontal: 20, paddingTop: 20, }}>
            {this.props.app.strings.qr_code_introduce}
          </Text>
          <View style={{ marginVertical: 20 }}>
            <QRCode
              value={`https://invite.dacsee.com/?referrer=${userId}&id=${_id}`}
              size={200}
            />
          </View>
          <Text style={{ fontSize: 30, textAlign: 'center', marginTop: 10, color: '#000', fontWeight: 'bold' }}>{fullName}</Text>
          <Text style={{ fontSize: 16, color: '#333', fontWeight: '400', marginTop: 5, marginBottom: Platform.OS === 'android' ? 15 : 35 }}>{`${strings.userid}: ${userId}`}</Text>
          <Item
            iconBackgroundColor={DCColor.BGColor('primary-1')}
            icon={<SvgIcon path={iconPath.hyperlinks} size={32} fill={[DCColor.BGColor('primary-2')]} />}
            title={strings.invite_link}
            describer={strings.copy_to_clipboard}
            onPress={(value) => {
              // 老的url
              //let url = `http://clientstaging.incredible-qr.com/clients/dacsee/redirect-page/?referrer=${this.props.account.user.userId}&id=${this.props.account.user._id}`
              // 新的url
              // const linkTitle = 'You are invited by your friend to Join DACSEE Social Ride Sharing\r\n 1) Download DACSEE App from Google Play or App store\r\n 2) Click on the invite link below\r\n'
              // const linkFooter = ` 3) Complete your registration with Referrer Code = ${this.props.account.user.userId}`
              // url = `${linkTitle}https://invite.dacsee.com/?referrer=${this.props.account.user.userId}&id=${this.props.account.user._id}&language=${language}\r\n${linkFooter}`
              // Clipboard.setString(url)
              // $.define.func.showMessage('Success')
              // 老的url
              let url = `http://clientstaging.incredible-qr.com/clients/dacsee/redirect-page/?referrer=${this.props.account.user.userId}&id=${this.props.account.user._id}`
              // 新的url
              const linkTitle = 'Please register with DACSEE using my referral link below:'
              url = `${linkTitle}https://dacsee.com/#/register?ref=${this.props.account.user.userId}`
              Clipboard.setString(url)
              $.define.func.showMessage('Success')
            }}
          />

          <Item
            iconBackgroundColor={DCColor.BGColor('primary-1')}
            icon={<SvgIcon path={iconPath.hyperlinks} size={32} fill={[DCColor.BGColor('primary-2')]} />}
            title={'REFERRAL CODE'}
            describer={strings.copy_to_clipboard}
            onPress={(value) => {
              // 老的url
              // let url = `http://clientstaging.incredible-qr.com/clients/dacsee/redirect-page/?referrer=${this.props.account.user.userId}&id=${this.props.account.user._id}`
              // // 新的url
              // const linkTitle = 'Please register with DACSEE using my referral link below:'
              let url = `${this.props.account.user.userId}`
              Clipboard.setString(url)
              $.define.func.showMessage('Success')
            }}
          />
        </View>
      </ScrollView>
    )
  }
}

class Item extends Component {
  render() {
    const { iconBackgroundColor, title, icon, describer, onPress = () => { } } = this.props
    return (
      <TouchableOpacity onPress={() => onPress()} activeOpacity={0.7}>
        <View style={{ marginBottom: 15, width: width - 40, height: 80, borderRadius: 17, backgroundColor: 'white', flexDirection: 'row', alignItems: 'center', paddingHorizontal: 13 }}>
          <IconWrap backgroundColor={iconBackgroundColor}>{icon}</IconWrap>
          <View style={{ marginLeft: 30 }}>
            <Text style={{ fontSize: TextFont.TextSize(16), color: 'black', fontWeight: '400' }}>{title}</Text>
            <Text style={{ fontSize: TextFont.TextSize(11), color: '#969696' }}>{describer}</Text>
          </View>
        </View>
      </TouchableOpacity>
    )
  }
}

class IconWrap extends Component {
  render() {
    return (
      <View style={[
        { width: 76, height: 76, borderRadius: 38, justifyContent: 'center', alignItems: 'center' },
        { borderColor: '#EAEAEA', borderWidth: 6 },
        { backgroundColor: this.props.backgroundColor }
      ]}>
        {this.props.children}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#F3F3F3',
  },
})