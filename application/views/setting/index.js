// @flow
import React, { Component } from 'react'
import Settings from './settings'
import SettingAboutScreen from './setting.about'
import SettingFeedbackScreen from './setting.feedback'
import SettingHelpCenterScreen from './setting.help.center'
import SettingWetViewScreen from './setting.web.view'
import SettingQrCodeScreen from './setting.qrcode'
import SettingPinUpdate from './setting.pin.update'
import SettingPinReset from './setting.pin.reset'
import ProfileChangeAvatarScreen from './profile.change.avatars'
import BankDetailsScreen from './bank/bank.details'
import BankListScreen from './bank/bank.list'
import AddBankAccountScreen from './bank/add.bank.account'
import DriverPersonalInfo from './account.personal.info'
import VehicleInfo from './driver.vehicle.info'
import PSVDocumentation from './psv.documentation'
import EmergencyContact from './emergencyContact'
import EHailingPermit from './ehailing'
import SettingAccountScreen from './setting.account'
import { Icons, Session, Avatars } from 'dacsee-utils'
import { inject, observer } from 'mobx-react'
import {
  Text,
  View,
  Image
} from 'react-native'
import Resources from 'dacsee-resources'

// 主菜单
@inject('app', 'account')
@observer
class SettingMenuScreen extends Component {
  // static navigationOptions = { title: '设置' }
  static navigationOptions = ({ navigation }) => {
    const { strings } = $.store.app
    return {
      title: strings.settings
    }
  }
  render() {
    const { navigation, dispatch, app } = this.props
    const { strings } = app
    return (
      <Settings producer={[
        [{
          title: strings.profile, type: 'text', onPress: () => navigation.navigate('SettingAccount')
        }, {
          title: strings.privacy_setting, type: 'text', onPress: () => navigation.navigate('SettingPrivate')
        }],
        [{
          title: strings.language_region,
          type: 'text',
          onPress: () => navigation.navigate('SettingLanguageRegion', {
            refresh: () => this.props.navigation.setParams({})
          })
        }],
        [
          // {/*2018年6月29日隐藏帮助中心和反馈 */}
          //   {
          //   title: strings.feedback, type: 'text', onPress: async () => navigation.navigate('SettingFeedback', {
          //     strings
          //   })
          // }, {
          //   title: strings.help, type: 'text', onPress: () => navigation.navigate('SettingHelpCenter', {
          //     strings
          //   })
          // },
          {
            title: strings.about,
            type: 'text',
            onPress: () => navigation.navigate('SettingAbout', {
              strings
            })
          }],
        [{
          title: strings.logout,
          type: 'button',
          onPress: () => {
            this.props.account.logout()
          }
        }]
      ]} />
    )
  }
}

// 新消息通知
@inject('app')
@observer
class SettingMessageNotificationScreen extends Component {
  // static navigationOptions = { title: '新消息通知' }
  static navigationOptions = ({ navigation }) => {
    const { strings } = $.store.app
    return {
      title: strings.new_message_notification
    }
  }
  render() {
    return (
      <Settings producer={[
        [{
          title: '允许推送消息', type: 'switch', value: false, editable: false, onPress: () => { }
        }]
      ]} />
    )
  }
}

// 隐私设置
@inject('app', 'account')
@observer
class SettingPrivateScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const { strings } = $.store.app
    return {
      title: strings.privacy_setting
    }
  }
  render() {
    const { strings } = this.props.app
    const { user, privacySetting } = this.props.account
    const { pref } = user
    return (
      <Settings producer={[
        [{
          title: strings.allow_driver_send, type: 'switch', value: pref.booking.allowDriverFriendRequest, editable: false, onPress: (value) => { privacySetting(value, 'booking', 'allowDriverFriendRequest') }
        }, {
          title: strings.allow_passenger_send, type: 'switch', value: pref.booking.allowPassengerFriendRequest, editable: false, onPress: (value) => { privacySetting(value, 'booking', 'allowPassengerFriendRequest') }
        },
        {
          title: strings.allow_see_location, type: 'switch', value: pref.privacy.locationVisibility, editable: false, onPress: (value) => { privacySetting(value, 'privacy', 'locationVisibility') }
        },
        //2018年7月16日隐藏 #204BUG
        // {
        //   title: strings.allow_booking_downline, type: 'switch', value: pref.booking.allowRequestWhenOffline, editable: false, onPress: (value) => { privacySetting(value, 'booking', 'allowRequestWhenOffline') }
        // }, 
        {
          title: strings.allow_search_by_phone, type: 'switch', value: pref.privacy.phoneNoVisibility, editable: false, onPress: (value) => { privacySetting(value, 'privacy', 'phoneNoVisibility') }
        },
        {
          title: strings.allow_search_by_email, type: 'switch', value: pref.privacy.emailVisibility, editable: false, onPress: (value) => { privacySetting(value, 'privacy', 'emailVisibility') }
        }
        ]
      ]} />
    )
  }
}

// 语言和地区
@inject('app')
@observer
class SettingLanguageRegionScreen extends React.Component {
  static navigationOptions = ({ navigation }) => {
    const { strings } = $.store.app
    return {
      title: strings.language_region
    }
  }
  componentWillUnmount() {
    this.props.navigation.state.params.refresh()
  }
  render() {
    const { navigation, app } = this.props
    const { strings } = app
    return (
      <Settings producer={[
        [{
          title: strings.language,
          type: 'text',
          value: strings.current_language,
          editable: true,
          onPress: () => navigation.navigate('SettingLanguageChoose', {
            refresh: (data) => {
              this.props.navigation.setParams({})
            }
          })
        }
        ]
      ]} />
    )
  }
}

// 语言选择
@inject('app')
@observer
class SettingLanguageChooseScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const { strings } = $.store.app
    return {
      title: strings.language_select
    }
  }
  render() {
    const { navigation, dispatch, language, app } = this.props
    const { strings, changeLanguage } = app

    const { refresh } = navigation.state.params

    return (
      <Settings producer={[
        [{
          title: strings.cn_simple,
          type: 'radio',
          value: language === 'zh-CN',
          editable: false,
          onPress: () => {
            changeLanguage('ZH_CN')
            refresh('refresh')
            navigation.goBack()
          }
        }, {
          title: strings.mas,
          type: 'radio',
          value: language === 'mas',
          editable: false,
          onPress: () => {
            changeLanguage('MAS')
            refresh('refresh')
            navigation.goBack()
          }
        }, {
          title: strings.en,
          type: 'radio',
          value: language === 'en-US',
          editable: false,
          onPress: () => {
            changeLanguage('EN_US')
            refresh('refresh')
            navigation.goBack()
          }
        }]
      ]} />
    )
  }
}

export {
  SettingMenuScreen,
  SettingAboutScreen,
  SettingAccountScreen,
  SettingMessageNotificationScreen,
  SettingLanguageRegionScreen,
  SettingLanguageChooseScreen,
  SettingFeedbackScreen,
  SettingHelpCenterScreen,
  SettingWetViewScreen,
  ProfileChangeAvatarScreen,
  SettingPrivateScreen,
  BankDetailsScreen,
  SettingQrCodeScreen,
  BankListScreen,
  AddBankAccountScreen,
  SettingPinUpdate,
  SettingPinReset,
  DriverPersonalInfo,
  VehicleInfo,
  PSVDocumentation,
  EmergencyContact,
  EHailingPermit
}
