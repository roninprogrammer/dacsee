
import React, { Component } from 'react'
import { View, TouchableOpacity, Text, StyleSheet } from 'react-native'
import Settings from '../settings'
import { TextFont } from 'dacsee-utils'
import { inject, observer } from 'mobx-react'

@inject('app')
@observer
class BankDetailsScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const { title, option } = navigation.state.params
    const { strings } = $.store.app
    return {
      drawerLockMode: 'locked-closed',
      title: title,
      headerRight: (
        <TouchableOpacity
          activeOpacity={0.7}
          style={{ paddingRight: 15, justifyContent: 'center', alignItems: 'flex-end' }}
          onPress={() => { option.onsubmit() }}
        >
          <Text style={{ fontSize: TextFont.TextSize(16), color: 'white', fontWeight: '600' }}>{strings.finish}</Text>
        </TouchableOpacity>
      )
    }
  }

  _changeBankName(value) {
    this.props.dispatch(wallet.updateBankInfo({
      bank_info: Object.assign({}, this.props.wallet.bank_info, { name: value })
    }))
  }

  _changeBankAccount(value) {
    this.props.dispatch(wallet.updateBankInfo({
      bank_info: Object.assign({}, this.props.wallet.bank_info, { accountNo: value })
    }))
  }
  _changeBankHolderName(value) {
    this.props.dispatch(wallet.updateBankInfo({
      bank_info: Object.assign({}, this.props.wallet.bank_info, { accountHolderName: value })
    }))
  }



  componentWillUnmount() {
    this.props.dispatch(wallet.updateBankInfo({
      bank_info: {}
    }))
  }


  render() {
    const { user, navigation, wallet, app } = this.props
    const { strings } = app
    const { bankInfo } = user
    return (
      <View style={styles.container}>
        <View style={{ paddingTop: 16, paddingLeft: 12 }}>
          <Text>{strings.bank_detail_for_withdrawals}</Text>
        </View>
        <Settings producer={[
          [{
            title: strings.bank_name, type: 'text',
            value: wallet.bank_info.name || (bankInfo && bankInfo.name) || '',
            onPress: () => navigation.navigate('BankList', {
              title: strings.sel_bank,
              editorName: 'String',
              onPress: (val) => this._changeBankName(val)
            })
          }, {
            title: strings.bank_account, type: 'text',
            value: wallet.bank_info.accountNo || (bankInfo && bankInfo.accountNo) || '',
            onPress: () => navigation.navigate('FormEditor', {
              title: bankInfo == null ? strings.add_bank_account : strings.update_bank_account,
              editorName: 'String',
              option: {
                placeholder: strings.pls_enter_bank_account,
                keyboardType: 'numeric',
                value: wallet.bank_info.accountNo || (bankInfo && bankInfo.accountNo) || '',
                onChangeValue: (val) => this._changeBankAccount(val)
              }
            })
          }, {
            title: strings.bank_holder_name, type: 'text',
            value: wallet.bank_info.accountHolderName || (bankInfo && bankInfo.accountHolderName) || '',
            onPress: () => navigation.navigate('FormEditor', {
              title: bankInfo == null ? strings.add_bank_account : strings.update_bank_account,
              editorName: 'String',
              option: {
                placeholder: strings.pls_enter_holder_name,
                value: wallet.bank_info.accountHolderName || (bankInfo && bankInfo.accountHolderName) || '',
                onChangeValue: (val) => this._changeBankHolderName(val)
              }
            })
          }]
        ]}
        />
      </View>
    )
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f2f2f2'
  }
})

export default BankDetailsScreen