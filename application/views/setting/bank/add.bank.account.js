import React, { Component } from 'react'
import { KeyboardAvoidingView, View, TouchableOpacity, Text, StyleSheet, TextInput, ScrollView } from 'react-native'
import { inject, observer } from 'mobx-react'
import { Screen, TextFont, DCColor } from 'dacsee-utils'
import SelectBank from '../../../components/selectBank'

const { height, width } = Screen.window

const styles = StyleSheet.create({
    bigcont: {
        flexDirection: 'column',
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        width,
        flex: 1,
        backgroundColor: 'white'
    },
    dcForm: {
        flexDirection: 'column',
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        padding: 20,
        width,
    },
    itemCont: {
        flexDirection: 'column',
        width: '100%',
        marginBottom: 20,
        borderBottomWidth: 1,
        borderColor: 'rgba(0, 0, 0, 0.5)'
    },
    title: {
        width: '100%',
        fontSize: 16,
        opacity: 0.8,
        color: '#000',
    },
    cfillin: {
        width: '100%',
        height: 45,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
    },
    bottom: {
        flexDirection: 'row',
        alignItems: 'center',
        width,
        marginTop: 20
    },
    dcBtn: {
        flex: 0.45,
        height: 44,
        borderRadius: 30,
        backgroundColor: DCColor.BGColor('primary-1'),
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    btnText: {
        padding: 5,
        fontSize: TextFont.TextSize(15),
        fontWeight: 'bold',
        color: DCColor.BGColor('primary-2')
    },
    dcBackDrop: {
        width,
        height,
        position: 'absolute',
        top: 0,
        left: 0,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(0 ,0, 0, 0.75)'
    }

})
@inject('app', 'bank')
@observer


export default class AddBankAccountScreen extends Component {

    static navigationOptions = ({ navigation }) => {
        const { strings } = $.store.app
        const { title } = navigation.state.params
        return {
            drawerLockMode: 'locked-closed',
            title: title || strings.add_bank_account
        }
    }

    constructor(props) {
        super(props)
        const {
            mode = 'add',
            bankDetail = {}
        } = props.navigation.state.params
        const {
            _id = '',
            country = '',
            name = '',
            accountNo = '',
            accountHolderName = '',
            accountHolderIdNo = ''
        } = bankDetail
        this.state = {
            mode,
            submitFn: mode === 'edit' ? this.props.bank.updateBankInfo : this.props.bank.addBank,
            bank: name,
            bank_id: _id,
            form: { country, name, accountNo, accountHolderName, accountHolderIdNo },
            showSelectBank: false,
        }
    }

    async componentDidMount() {
        const { countryCode } = this.props.navigation.state.params
        const { mode, bank_id } = this.state
        const { bank } = this.props
        const { form } = this.state
        this.setState({
            form: {
                ...form,
                country: mode === 'edit' ? form.country : countryCode,
            },
        })
        await this.props.bank.getLookUpBankList(form.country || countryCode)
        this.setState({
        })
        if (mode === 'edit') {
            const bankDetail = await bank.getBankDetail(bank_id)
            if (!bankDetail) return navigation.goBack()
            this.setState({ name: bankDetail.name, form: { ...bankDetail } })
        }
    }

    runValidation = () => {
        const { form: { country, name, accountNo, accountHolderIdNo, accountHolderName } } = this.state
        const icRegexp = /[0-9]{6}[-]{1}[0-9]{2}[-]{1}[0-9]{4}/g

        if (accountHolderIdNo && accountHolderIdNo.length < 14) {
            $.define.func.showMessage(this.props.app.strings.pls_enter_ic)
            return false
        } else if (!country || !name || !accountNo || !accountHolderIdNo || !accountHolderName) {
            $.define.func.showMessage(this.props.app.strings.add_bank_error_1)
            return false
        } else if (!icRegexp.test(accountHolderIdNo) || accountHolderIdNo.length !== 14) {
            $.define.func.showMessage(this.props.app.strings.pls_enter_ic)
            return false
        } else {
            return true
        }
    }

    onSubmit = async () => {
        const {
            submitFn,
            bank_id: _id,
            form: { country, name, accountNo, accountHolderIdNo, accountHolderName }
        } = this.state
        if (this.runValidation()) {
            const isSuccess = await submitFn({
                _id,
                country,
                name,
                accountNo,
                accountHolderIdNo,
                accountHolderName
            });
            if (isSuccess) this.props.navigation.goBack()
        }
    }

    onchangeIC(text) {
        let modValue = '';
        let separator = '-';
        const pattern = /[^.]*\d+$|^.{0}$/g
        text = text.replace(/-/g, '');
        if (!pattern.test(text)) return;
        const length = text.length;
        if (length > 12) {
            modValue = text.slice(0, 14);
            modValue = text.slice(0, 6) + separator + text.slice(6, 8) + separator + text.slice(8, 12);
            this.setState({
                form: {
                    ...this.state.form,
                    accountHolderIdNo: modValue
                }
            })
        } else if (length > 8) {
            modValue = text.slice(0, 6) + separator + text.slice(6, 8) + separator + text.slice(8, length);
            this.setState({
                form: {
                    ...this.state.form,
                    accountHolderIdNo: modValue
                }
            })
        } else if (length > 6) {
            modValue = text.slice(0, 6) + separator + text.slice(6, length);
            this.setState({
                form: {
                    ...this.state.form,
                    accountHolderIdNo: modValue
                }
            })
        } else {
            this.setState({
                form: {
                    ...this.state.form,
                    accountHolderIdNo: text
                }
            })
        }
    }

    render() {
        const { strings, activeOpacity } = this.props.app
        const { showSelectBank, isLoading } = this.state
        const { bigcont, dcForm, itemCont, title, cfillin, bottom, btnText, dcBtn, dcBackDrop } = styles
        const { allBankList } = this.props.bank
        const { country, name, accountNo, accountHolderIdNo, accountHolderName } = this.state.form
        // let bankname = this.props.bank.selectedBank
        return (
            <View style={bigcont}>
                <ScrollView style={{ flex: 1 }} contentContainerStyle={{ flex: 1 }}>
                    <KeyboardAvoidingView behavior="position" contentContainerStyle={dcForm}>
                        <View style={itemCont}>
                            <Text style={title}>{strings.country}</Text>
                            <TextInput style={cfillin}
                                editable={false}
                                defaultValue={country}
                                underlineColorAndroid="transparent"
                            />
                        </View>
                        <View style={itemCont}>
                            <Text style={title}>{strings.bank_name}</Text>
                            <TouchableOpacity style={cfillin} onPress={() => this.setState({ showSelectBank: true })}>
                                <Text style={title}>{name === '' ? strings.pls_sel_bank : name}</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={itemCont}>
                            <Text style={title}>{strings.bank_account}</Text>
                            <TextInput
                                underlineColorAndroid="transparent"
                                defaultValue={accountNo}
                                style={cfillin}
                                keyboardType={'numeric'}
                                returnKeyType={'done'}
                                placeholder={strings.input_prompt}
                                onChangeText={(text) => this.setState({
                                    form: {
                                        ...this.state.form,
                                        accountNo: text
                                    }
                                })}
                            />
                        </View>
                        <View style={itemCont}>
                            <Text style={title}>{strings.bank_holder_name}</Text>
                            <TextInput
                                returnKeyType="done"
                                underlineColorAndroid="transparent"
                                defaultValue={accountHolderName}
                                style={cfillin}
                                placeholder={strings.input_prompt}
                                onChangeText={(text) => this.setState({
                                    form: {
                                        ...this.state.form,
                                        accountHolderName: text
                                    }
                                })}
                            />
                        </View>
                        <View style={[itemCont, { marginBottom: 0 }]}>
                            <Text style={title}>{strings.ic_number}</Text>
                            <TextInput
                                underlineColorAndroid="transparent"
                                defaultValue={accountHolderIdNo}
                                style={cfillin}
                                maxLength={14}
                                // enablesReturnKeyAutomatically={true}
                                keyboardType={'numeric'}
                                returnKeyType={'done'}
                                placeholder={strings.input_prompt}
                                onChangeText={(text) => this.onchangeIC(text)} />
                        </View>
                        <View style={bottom}>
                            <TouchableOpacity style={[dcBtn, { marginRight: 10, backgroundColor: 'rgba(0, 0, 0, 0.15)' }]} onPress={() => this.props.navigation.goBack()}>
                                <Text style={btnText}>{strings.cancel.toUpperCase()}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={dcBtn} onPress={this.onSubmit}>
                                <Text style={btnText}>{strings.sub.toUpperCase()}</Text>
                            </TouchableOpacity>
                        </View>
                    </KeyboardAvoidingView>
                </ScrollView>
                <SelectBank
                    visible={showSelectBank}
                    countryCode='MY'
                    onHide={() => this.setState({ showSelectBank: false })}
                    bankChange={(bank) => {
                        this.setState({ showSelectBank: false })
                        if (bank) {
                            this.setState({
                                form: {
                                    ...this.state.form,
                                    name: bank
                                }
                            })
                        }
                    }}
                />
            </View>
        )
    }
}