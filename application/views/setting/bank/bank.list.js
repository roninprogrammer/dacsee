import React, { Component } from 'react'
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  ListView
} from 'react-native'
import InteractionManager from 'InteractionManager'
import { Session } from 'dacsee-utils'
import {inject, observer} from 'mobx-react'

const dataContrast = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })

@inject('app')
@observer
export default class BankListScreen extends Component {

  static navigationOptions = ({ navigation }) => {
    const { title } = navigation.state.params
    return {
      drawerLockMode: 'locked-closed',
      title: title,
    }
  }

  async componentDidMount() {
    await InteractionManager.runAfterInteractions()
    const { strings }=this.props.app
    try {
      const { account } = this.props
      const data = await Session.Lookup.Get(`v1/lookup/banks?resultType=nameOnly&country=${account.country}`)
      this.props.dispatch(wallet.setBankValue({ bank_list: data }))
    } catch (e) {
      // this.props.dispatch(app.showMessage(strings.unable_connect_server_pls_retry_later))
    }
  }
  componentWillUnmount() { }


  render() {
    const { wallet={}, navigation } = this.props
    const {
      onPress = () => { }
    } = navigation.state.params
    return (
      <View style={styles.container}>
        <ListView 
          removeClippedSubviews={false} 
          dataSource={dataContrast.cloneWithRows(wallet.bank_list)}
          enableEmptySections={true}
          renderSeparator={() => (
            <View style={{ height: .8, backgroundColor: '#eee' }}></View>
          )}
          renderRow={(row) => (
            <TouchableOpacity activeOpacity={.7} onPress={() => {
              onPress(row)
              this.props.navigation.goBack()
            }} style={{ flex: 1, height: 52, justifyContent: 'center', backgroundColor: 'white' }}>
              <View style={{ flexDirection: 'row', paddingHorizontal: 15, alignItems: 'center' }}>
                <Text style={{ color: '#333', fontSize: 16, fontWeight: '600' }}>{row}</Text>
              </View>
            </TouchableOpacity>
          )}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
})

