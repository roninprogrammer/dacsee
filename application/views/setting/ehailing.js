import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { StyleSheet, ScrollView, View, Text, SafeAreaView, TouchableOpacity, Linking } from 'react-native'
import { Screen, TextFont, DCColor } from '../../utils'
import moment from 'moment'

const { width } = Screen.window

@inject('app', 'account', 'vehicles')
@observer
export default class EHailingPermit extends Component {

  static navigationOptions = () => {
    const { strings } = $.store.app
    return {
      title: strings.settings_account_ehailing,
      drawerLockMode: 'locked-closed'
    }
  }

  constructor(props) {
    super(props)
    this.state = {
      data: {}
    }
  }

  async componentDidMount() {
    this.props.app.showLoading()
    const resp = await $.store.vehicles.getVehiclesData()
    if (resp.length > 0) {
      this.setState({ data: resp[0]})
    }
    this.props.app.hideLoading()
  }

  render() {
    const { mainCont, formCont, fTitle, fCaption , actionCont, updateCont, submitText } = styles
    const { data } = this.state
    const { registrationNo, chassisNumber, model, maxSeatingCapacity } = data
    const { strings } = $.store.app
    const { fullName, custom = {}, driverInfo = {} } = $.store.account.user
    const { evpInfo } = custom || {}
    const { evpNo = '', mainNo = '', subNo = '', startOn = '', endOn = '' } = evpInfo || {}
    const { type = '' } = driverInfo || {}

    return (
      <SafeAreaView style={mainCont}>
        <View style={{flexDirection: 'column'}}>
          <ScrollView>
            <View style={formCont}>
              <Text style={fTitle}>{strings.settings_account_ehailing_license_holder_company}</Text>
              <Text style={fCaption}>DMD Technology Sdn.Bhd.</Text>
            </View>

            <View style={formCont}>
              <Text style={fTitle}>{strings.settings_account_ehailing_company_registration_no}</Text>
              <Text style={fCaption}>1262152-U</Text>
            </View>

            <View style={formCont}>
              <Text style={fTitle}>{strings.settings_account_vehicle_info_vehicle_model}</Text>
              <Text style={fCaption}>{model? model : strings.no_content}</Text>
            </View>

            <View style={formCont}>
              <Text style={fTitle}>{strings.settings_account_ehailing_driver_name}</Text>
              <Text style={fCaption}>{fullName? fullName : strings.no_content}</Text>
            </View>

            <View style={formCont}>
              <Text style={fTitle}>{strings.settings_account_ehailing_vehicle_plate_number}</Text>
              <Text style={fCaption}>{registrationNo? registrationNo : strings.no_content}</Text>
            </View>

            <View style={formCont}>
              <Text style={fTitle}>{strings.settings_account_ehailing_vehicle_chssis_no}</Text>
              <Text style={fCaption}>{chassisNumber? chassisNumber : strings.no_content}</Text>
            </View>

            <View style={formCont}>
              <Text style={fTitle}>{strings.settings_account_vehicle_info_vehicle_max_seating}</Text>
              <Text style={fCaption}>{maxSeatingCapacity? maxSeatingCapacity : strings.no_content}</Text>
            </View>

            <View style={formCont}>
              <Text style={fTitle}>{strings.settings_account_ehailing_car_type}</Text>
              <Text style={fCaption}>{type ? type : strings.no_content }</Text>
            </View>

            <View style={formCont}>
              <Text style={fTitle}>{strings.settings_account_ehailing_account_evpNo}</Text>
              <Text style={fCaption}>{ evpNo ? evpNo : strings.no_content}</Text>
            </View>

            <View style={formCont}>
              <Text style={fTitle}>{strings.settings_account_ehailing_permit_reference_no_main}</Text>
              <Text style={fCaption}>{ mainNo ? mainNo: strings.no_content}</Text>
            </View>

            <View style={formCont}>
              <Text style={fTitle}>{strings.settings_account_ehailing_permit_reference_no_sub}</Text>
              <Text style={fCaption}>{subNo? subNo : strings.no_content}</Text>
            </View>

            <View style={{flexDirection: 'row'}}>
              {/* <View style={indicator}/> */}
              <View style={formCont}>
                <Text style={fTitle}>{strings.settings_account_ehailing_permit_start_date}</Text>
                <Text style={fCaption}>{startOn ? moment(startOn).format('DD MMM YYYY'): strings.no_content}</Text>
              </View>
            </View>

            <View style={{flexDirection: 'row'}}>
              {/* <View style={indicator}/> */}
              <View style={formCont}>
                <Text style={fTitle}>{strings.settings_account_ehailing_permit_end_date}</Text>
                <Text style={fCaption}>{endOn ? moment(endOn).format('DD MMM YYYY'): strings.no_content}</Text>
              </View> 
            </View>
          </ScrollView>
          <View style={actionCont}>
            <TouchableOpacity style={updateCont} onPress={() => Linking.openURL('http://bit.ly/dacsee-updatedocument')}>
              <Text style={submitText}>{strings.update_driver_document}</Text>
            </TouchableOpacity>
          </View>
        </View>
      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  mainCont: {
    width,
    height: '100%',
    flexDirection: 'row',
    flexWrap: 'nowrap',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  formCont: {
    width,
    padding: 15,
    backgroundColor: 'white',
    borderBottomColor: 'rgba(0, 0, 0, 0.05)',
    borderBottomWidth: 2,
    flexDirection: 'column',
  },
  fTitle: {
    color: '#333', 
    fontSize: TextFont.TextSize(15), 
    fontWeight: '500' ,
  },
  fCaption: {
    color: 'rgba(0, 0, 0, 0.45)', 
    fontSize: TextFont.TextSize(12), 
    fontWeight: '400' ,
    paddingTop: 5
  },
  viewCont: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  imgBox: {
    height: 40,
    width: 40,
    borderColor: 'rgba(0, 0, 0, 0.1)',
    borderWidth: 1,
    borderRadius: 40,
    alignItems: 'center',
    justifyContent: 'center'
  },
  actionCont:{
    height: '13%',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    backgroundColor: 'white',
    borderTopColor: 'rgba(0, 0, 0, 0.25)',
    borderTopWidth: 2
  },
  updateCont: {
    width: '90%',
    height: 50,
    backgroundColor: DCColor.BGColor('primary-1'),
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 5
  },
  submitText: {
    fontSize: TextFont.TextSize(16),
    fontWeight: '700',
    color: 'rgba(0, 0, 0, 0.75)',
  }
})