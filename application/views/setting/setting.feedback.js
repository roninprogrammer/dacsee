/* @flow */

import React, { Component } from 'react'
import { View } from 'react-native'
import CodePush from 'react-native-code-push'

/*****************************************************************************************************/
/*****************************************************************************************************/

export default class SettingAboutScreen extends Component {

  static navigationOptions = () => {
    const { strings } = $.store.app
    return {
      title: strings.feedback,
      drawerLockMode: 'locked-closed'
    }
  }

  constructor(props) {
    super(props)
    this.state = {
      rnVersion: ''
    }
  }

  async componentDidMount() {
    const runningBundle = await CodePush.getUpdateMetadata(CodePush.UpdateState.RUNNING)
    this.setState({ rnVersion: (runningBundle ? runningBundle.label : '0') })
  }

  render() {
    const { navigate } = this.props.navigation
    const { rnVersion } = this.state

    return (
      <View style={{ flex: 1 }}>
        {/* <ScrollView contentContainerStyle={{ flex: 1 }} style={{ flex: 1, backgroundColor: '#f8f8f8' }}>
          <View style={{ justifyContent: 'center', alignItems: 'center', paddingVertical: 30 }}>
            <Image style={{ borderRadius: 11, width: 88, height: 88 }} source={resources.image.logo} />
            <View style={{ alignItems: 'center', marginTop: 25, height: 30, justifyContent: 'space-between' }}>
              <Text style={{ fontSize: TextFont.TextSize(15), color: '#666' }}>{`版本: ${System.Version}-${ rnVersion.replace('v', '') }`}</Text>
            </View>
            <View style={{ width: Screen.window.width, marginTop: 35, borderTopWidth: Define.system.ios.plus ? 1 : .5, borderBottomWidth: Define.system.ios.plus ? 1 : .5, borderColor: '#eaeaea' }}>
              <TouchableOpacity onPress={() => Alert.alert('', '应用上架后才能使用该功能')} activeOpacity={0.7} style={{ paddingHorizontal: 12, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', height: 44, backgroundColor: 'white' }}>
                <Text style={{ color: '#333', fontSize: TextFont.TextSize(15), fontWeight: '400' }}>去评分</Text>
                { Icons.Generator.Material('chevron-right', 24, '#bbb') }
              </TouchableOpacity>
              <View style={{ paddingLeft: 12, backgroundColor: 'white' }}><View style={{ borderTopWidth: .5, borderColor: '#eaeaea' }} /></View>
              <TouchableOpacity onPress={() => Alert.alert('', '还没有拿到文案')} activeOpacity={0.7} style={{ paddingHorizontal: 12, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', height: 44, backgroundColor: 'white' }}>
                <Text style={{ color: '#333', fontSize: TextFont.TextSize(15), fontWeight: '400' }}>隐私协议及使用条款</Text>
                { Icons.Generator.Material('chevron-right', 24, '#bbb') }
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
        <View style={{ position: 'absolute', bottom: 15, left: 0, right: 0, justifyContent: 'center', alignItems: 'center' }}>
          <Text style={{ color: '#b1b1b1', fontSize: TextFont.TextSize(11), backgroundColor: 'transparent' }}>2017-2018 DACSEE All rights reserved.</Text>
        </View> */}
      </View>
    )
  }
}
