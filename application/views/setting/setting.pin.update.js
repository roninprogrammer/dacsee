import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { StyleSheet, ScrollView, View, Text, SafeAreaView, TextInput, TouchableOpacity, Alert } from 'react-native'
import { Screen, TextFont, DCColor } from 'dacsee-utils'
import BankContact from '../modal/modal.bankcontact'

const { width } = Screen.window
const styles = StyleSheet.create({
  mainCont: {
    width,
    height: '100%',
    flexDirection: 'column',
    flexWrap: 'nowrap',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  scrollCont: {
    width,
    flex: 1
  },
  formCont: {
    width,
    padding: 20,
    backgroundColor: 'white'
  },
  itemCont: {
    flex: 1,
    marginBottom: 15,
  },
  fLabel: {
    width: '100%',
    fontSize: TextFont.TextSize(14),
    fontWeight: '800',
    color: 'rgba(0, 0, 0, 0.65)',
    marginBottom: 10
  },
  dcInput: {
    flex: 1,
    borderRadius: 5,
    backgroundColor: 'rgba(0, 0, 0, 0.05)',
    textAlign: 'left',
    height: 45,
    paddingHorizontal: 10,
  },
  forgotCodeCont: {
    width: '100%',
    padding: 10
  },
  forgotText: {
    color: DCColor.BGColor('primary-1'),
    fontSize: TextFont.TextSize(16),
    fontWeight: '700',
    flex: 1,
    textAlign: 'center'
  },
  actionCont: {
    width,
    padding: 20
  },
  dcBtn: {
    flex: 1,
    height: 45,
    borderRadius: 22.5,
    backgroundColor: DCColor.BGColor('primary-1'),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  btnText: {
    fontSize: TextFont.TextSize(15),
    fontWeight: 'bold',
    color: 'rgba(0, 0, 0, 0.75)',
    textAlign: 'center'
  }
})

@inject('app', 'bank', 'account')
@observer
export default class SettingPinUpdate extends Component {

  static navigationOptions = () => {
    const { strings } = $.store.app
    return {
      title: $.store.account.user && $.store.account.user.hasPin ? strings.change_pin : strings.pin_title,
      drawerLockMode: 'locked-closed'
    }
  }

  constructor(props) {
    super(props)
    this.state = {
      currentCode: '',
      newCode: '',
      confirmCode: '',
      showBankContact: true
    }
  }

  async componentDidMount() {
    await $.store.account.getProfile()
  }

  async onSubmit() {
    const { strings } = this.props.app
    const { currentCode, newCode, confirmCode } = this.state
    const pin = {
      oldPin: currentCode,
      newPin: newCode
    }
    if (currentCode.length === 0 || newCode.length === 0 || confirmCode.length === 0) {
      $.define.func.showMessage(strings.add_bank_error_1)
    } else if (newCode.length < 6) {
      $.define.func.showMessage(strings.pin_error_1)
    } else if (newCode !== confirmCode) {
      $.define.func.showMessage(strings.pin_error_2)
    } else {
      let result = await this.props.bank.updatePin(pin)
      if (result ===  true) {
      this.setState({
        currentCode: '',
        newCode: '',
        confirmCode: ''
      })
      this.props.navigation.goBack()
      }
      else {
        this.setState({
          currentCode: '',
          newCode: '',
          confirmCode: ''
        })
      }
    }
  }

  async onForgot() {
    const { strings } = this.props.app
    Alert.alert((strings.reset_pin), (strings.reset_pin_desc),
      [
        { text: (strings.cancel), onPress: () => console.log('pressed ok') },
        {
          text: (strings.confirm), onPress: async () => {
            const isSuccess = await this.props.bank.getResetCode()
            if (isSuccess) {
              this.setState({
                currentCode: '',
                newCode: '',
                confirmCode: ''
              }, () => this.props.navigation.navigate('SettingPinReset'))
            }
          }
        },
      ])
  }

  async onAdd() {
    const { strings } = this.props.app
    const { newCode, confirmCode } = this.state
    const pin = {
      newPin: newCode
    }
    if (newCode.length === 0 || confirmCode.length === 0) {
      $.define.func.showMessage(strings.add_bank_error_1)
    } else if (newCode.length < 6) {
      $.define.func.showMessage(strings.pin_error_1)
    } else if (newCode !== confirmCode) {
      $.define.func.showMessage(strings.pin_error_2)
    } else {
      let result = await this.props.bank.updatePin(pin)
      if (result) {
      this.setState({
        currentCode: '',
        newCode: '',
        confirmCode: ''
      })
      this.props.navigation.goBack()
      } else {
        this.setState({
          currentCode: '',
          newCode: '',
          confirmCode: ''
        })
      }
    }
  }

  onHide = async () => {
    this.setState({ showBankContact: false })
    this.props.navigation.goBack()
  }

  render() {
    const {
      mainCont,
      scrollCont,
      formCont,
      itemCont,
      fLabel,
      dcInput,
      forgotCodeCont,
      forgotText,
      actionCont,
      dcBtn,
      btnText
    } = styles
    const { strings } = this.props.app
    const { user } = this.props.account
    const { newCode, confirmCode, currentCode, showBankContact } = this.state

    return (
      <SafeAreaView style={mainCont}>
        <ScrollView style={scrollCont}>
          <View style={formCont}>
            {user.pinStatus === 'inactive' && <BankContact visible={showBankContact} onClose={() => this.onHide()} />}
            {user && user.hasPin && (
              <View style={itemCont}>
                <Text style={fLabel}>{strings.current_pin.toUpperCase()}</Text>
                <TextInput
                  style={dcInput}
                  underlineColorAndroid='transparent'
                  returnKeyType='done'
                  maxLength={6}
                  secureTextEntry={true}
                  keyboardType="numeric"
                  defaultValue={currentCode}
                  onChangeText={(text) => {
                    this.setState({
                      ...this.state,
                      currentCode: text
                    })
                  }}
                  placeholder='Please enter here'
                />
              </View>
            )}
            <View style={itemCont}>
              <Text style={fLabel}>{strings.new_pin.toUpperCase()}</Text>
              <TextInput
                style={dcInput}
                underlineColorAndroid='transparent'
                returnKeyType='done'
                maxLength={6}
                secureTextEntry={true}
                keyboardType="numeric"
                defaultValue={newCode}
                onChangeText={(text) => {
                  this.setState({
                    ...this.state,
                    newCode: text
                  })
                }}
                placeholder='Please enter here'
              />
            </View>
            <View style={itemCont}>
              <Text style={fLabel}>{strings.confirm_pin.toUpperCase()}</Text>
              <TextInput
                style={dcInput}
                underlineColorAndroid='transparent'
                returnKeyType='done'
                maxLength={6}
                secureTextEntry={true}
                keyboardType="numeric"
                defaultValue={confirmCode}
                onChangeText={(text) => {
                  this.setState({
                    ...this.state,
                    confirmCode: text
                  })
                }}
                placeholder='Please enter here'
              />
            </View>
            {user && user.hasPin && (
              <TouchableOpacity style={forgotCodeCont} onPress={() => this.onForgot()}>
                <Text style={forgotText}>{strings.forgot_code.toUpperCase()}</Text>
              </TouchableOpacity>
            )}
          </View>
          <View style={actionCont}>
            <TouchableOpacity style={dcBtn} onPress={() => { user.hasPin ? this.onSubmit() : this.onAdd() }}>
              <Text style={btnText}>{strings.confirm.toUpperCase()}</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </SafeAreaView>
    )
  }
}
