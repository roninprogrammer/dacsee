import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { StyleSheet, ScrollView, View, Text, SafeAreaView } from 'react-native'
import { Screen, TextFont } from '../../utils'
import moment from 'moment'

const { width } = Screen.window

@inject('app', 'account')
@observer
export default class DriverPersonalInfo extends Component {

  constructor(props) {
    super(props)
    this.state = {
      license: [],
      addressInfo: []
    }
  }

  static navigationOptions = () => {
    const { strings } = $.store.app
    return {
      title: strings.settings_account_personal_info,
      drawerLockMode: 'locked-closed'
    }
  }

  async componentDidMount() {
    this.props.app.showLoading()
    const data = await $.method.session.User.Get('v1/favPlaces?type=residence&ignoreLocation=true')
    const resp = await $.method.session.User.Get('v1/documents?type=drivingLicense');

    if (data && data.length) {
      this.setState({ addressInfo: data[0] });
    }

    if (resp && resp.length){
      this.setState({ license : resp[0] });
    }
    this.props.app.hideLoading()
  }


  render() {
    const { mainCont, formCont, fTitle, fCaption, indicator, specialCont } = styles
    const { strings } = $.store.app
    const { custom = {}, dob } = $.store.account.user
    const { operationCity = '' } = custom || {}
    const { license, addressInfo } = this.state
    const { className = '' } = license.custom || {}

    return (
      <SafeAreaView style={mainCont}>
        <ScrollView>
          <View style={formCont}>
            <Text style={fTitle}>{strings.settings_account_personal_info_city_operation}</Text>
            <Text style={fCaption}>{operationCity ? operationCity : strings.no_content}</Text>
          </View>

          <View style={formCont}>
            <Text style={fTitle}>{strings.settings_account_personal_info_dob}</Text>
            <Text style={fCaption}>{dob ? moment(dob).format('DD MMM YYYY'): strings.no_content}</Text>
          </View>

          <View style={formCont}>
            <Text style={fTitle}>{strings.settings_account_personal_info_address}</Text>
            <Text style={fCaption}>{addressInfo.address ? addressInfo.address: strings.no_content}</Text>
          </View>

          <View style={formCont}>
            <Text style={fTitle}>{strings.settings_account_personal_info_class_driving_license}</Text>
            <Text style={fCaption}>{className? className: strings.no_content}</Text>
          </View>

          <View style={{flexDirection: 'row'}}>
            {/* <View style={indicator}/> */}
            <View style={formCont}>
              <Text style={fTitle}>{strings.settings_account_personal_info__driving_license_expiry_date}</Text>
              <Text style={fCaption}>{moment(license.expiredOn).format('DD MMM YYYY')}</Text>
            </View>
          </View>
         </ScrollView>
      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  mainCont: {
    width,
    height: '100%',
    flexDirection: 'row',
    flexWrap: 'nowrap',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  formCont: {
    width,
    padding: 15,
    backgroundColor: 'white',
    borderBottomColor: 'rgba(0, 0, 0, 0.05)',
    borderBottomWidth: 2,
    flexDirection: 'column',
  },
  fTitle: {
    color: '#333', 
    fontSize: TextFont.TextSize(15), 
    fontWeight: '500' ,
  },
  fCaption: {
    color: 'rgba(0, 0, 0, 0.45)', 
    fontSize: TextFont.TextSize(12), 
    fontWeight: '400' ,
    paddingTop: 5
  },
  indicator: {
    backgroundColor: 'red',
    width: 10, 
    height: 70,
    marginLeft: 2
  },
  specialCont: {
    flexDirection: 'row', 
    backgroundColor: 'white'
  }
})