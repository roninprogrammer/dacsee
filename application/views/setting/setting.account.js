import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { StyleSheet, View, Text, TouchableOpacity, Dimensions, Image } from 'react-native'
import { TextFont, DCColor, Icons, Avatars } from '../../utils'
import { NavigationActions } from 'react-navigation'

const { width } = Dimensions.get('window')

@inject('app', 'account', 'bank')
@observer
export default class SettingAccountScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      selectedTab: 'Passenger'
    }
  }

  static navigationOptions = ({ navigation }) => {
    const { strings } = $.store.app
    return ({
      title: strings.profile,
      headerRight: (
        <TouchableOpacity
          style={{ paddingRight: 15, justifyContent: 'center', alignItems: 'flex-end' }}
          onPress={() => $.store.account.getProfile() }
        >
          <View>{Icons.Generator.Material('refresh', 24, 'black')}</View>
        </TouchableOpacity>
      )
    })
  }

  async componentDidMount() {
    await $.store.account.getProfile()
  }

  handleTab = (selectedTab) => {
    this.setState({ selectedTab })
  }

  render() {
    const { account = {}, app } = this.props
    const { user } = account
    const { strings } = app
    const { avatars = [] } = user
    const { bankList = [] } = this.props.bank
    const bankDetail = bankList.length > 0 ? bankList[bankList.length - 1] : null
    const hasBank = !!bankDetail
    const { selectedTab } = this.state
    const { mainCont, avatarInfoCont, circleCont, picCont, upDownCont, fTitle, fCaption, tabSection, listCont, tabCont, tabContColor, qrcodeCont, qrcodeIcon, navigateBtn, passengerCont, fDot, indicator } = styles
    return (
      <View style={{ flex: 1 }}>
        <View style={mainCont}>
          <View style={avatarInfoCont}>
            <TouchableOpacity style={circleCont} onPress={() => global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'ProfileChangeAvatar', params: { type: 'user', avatars } }))}>
              <Image style={picCont} source={{ uri: Avatars.getHeaderPicUrl(avatars) }} />
            </TouchableOpacity>
            <View style={upDownCont}>
              <Text style={fTitle}>{user.fullName}</Text>
              <Text style={fCaption}>{user.userId}</Text>
            </View>
          </View>

          <View style={{ flexDirection: 'row', alignItem: 'center' }}>
            <TouchableOpacity style={qrcodeCont} onPress={() => global.$.navigator.dispatch(NavigationActions.navigate({
              routeName: 'SettingQrCode',
              params: {
                title: strings.my_qr_code, editorName: 'String', option: { userId: user.userId, id: user._id }
              }
            }))}>
              <View style={qrcodeIcon}>{Icons.Generator.Awesome('qrcode', 25, 'black')}</View>
            </TouchableOpacity>
            {!user.verified &&
              <TouchableOpacity style={[qrcodeCont, { paddingRight: 0 }]} onPress={() => global.$.navigator.dispatch(NavigationActions.navigate({
                routeName: 'FormEditor', params: {
                  title: strings.update_fullname,
                  editorName: 'String',
                  option: {
                    placeholder: strings.pls_enter_fullname,
                    value: user.fullName,
                    onChangeValue: (val) => account.changeFullName(val)
                  }
                }
              }))}>
                <View style={[navigateBtn, { paddingLeft: 5, paddingRight: 20 }]}>{Icons.Generator.Material('chevron-right', 24, 'black')}</View>
              </TouchableOpacity>
            }
          </View>
        </View>
            
        {
          $.store.account.isDriver && (
            <View style={tabSection}>
              <TouchableOpacity style={selectedTab === 'Passenger' ? tabContColor : tabCont} onPress={() => this.handleTab('Passenger')}>
                <Text style={fTitle}>{strings.passenger}</Text>
              </TouchableOpacity>
              <TouchableOpacity style={selectedTab === 'Driver' ? tabContColor : tabCont} onPress={() => this.handleTab('Driver')}>
                <Text style={fTitle}>{strings.driver}</Text>
                {/* <View style={fDot} />      */}
              </TouchableOpacity>
            </View>
          )
        }

        {
          selectedTab == 'Passenger' && (
            <View style={{ backgroundColor: 'white' }}>
              <View style={passengerCont}>
                <View style={[upDownCont, { paddingVertical: 15 }]}>
                  <Text style={fTitle}>{strings.phone}</Text>
                  <Text style={fCaption}>{user.phoneCountryCode} {user.phoneNo}</Text>
                </View>
              </View>

              {
                !!user.referral_id ? (
                  <View style={passengerCont}>
                    <View style={[upDownCont, { paddingTop: 15, paddingBottom: 15 }]}>
                      <Text style={fTitle}>{strings.referee_id}</Text>
                      <Text style={fCaption}>{user.referral ? user.referral.userId : null}</Text>
                    </View>
                    <View style={navigateBtn} />
                  </View>
                ) : (
                  <TouchableOpacity style={passengerCont} onPress={() => global.$.navigator.dispatch(NavigationActions.navigate({
                    routeName: 'FormEditor', params: {
                      title: strings.update_referral_id,
                      editorName: 'String',
                      option: {
                        placeholder: strings.pls_enter_referral_id,
                        value: '',
                        onChangeValue: (val) => account.changeReferralUserId(val)
                      }
                    }
                  }))}>
                    <View style={upDownCont}>
                      <Text style={fTitle}>{strings.referee_id}</Text>
                      <Text style={fCaption}>{strings.pls_enter_referral_id}</Text>
                    </View>
                    <View style={navigateBtn}>{Icons.Generator.Material('chevron-right', 24, '#bbb')}</View>
                  </TouchableOpacity>
                )
              }

              {
                !user.verified ? (
                  <TouchableOpacity 
                    style={passengerCont} 
                    onPress={() => {
                      global.$.navigator.dispatch(NavigationActions.navigate({
                        routeName: 'FormEditor', params: {
                          title: strings.update_email,
                          editorName: 'String',
                          option: {
                            placeholder: strings.pls_enter_email,
                            value: user.email,
                            onChangeValue: (val) => account.changeEmail(val)
                          }
                        }
                      }))}
                    }
                  >
                    <View style={upDownCont}>
                      <Text style={fTitle}>{strings.email}</Text>
                      {
                        !user.email ?
                          <Text style={fCaption}>{strings.pls_enter_email}</Text>
                          :
                          <Text style={fCaption}>{user.email}</Text>
                      }
                    </View>
                    <View style={navigateBtn}>{Icons.Generator.Material('chevron-right', 24, '#bbb')}</View>
                  </TouchableOpacity>
                ) : ( 
                  <View style={passengerCont}>
                    <View style={[upDownCont, { paddingTop: 15, paddingBottom: 15 }]}>
                      <Text style={fTitle}>{strings.email}</Text>
                      <Text style={fCaption}>{user.email}</Text>
                    </View>
                  </View>
                )
              }

              <TouchableOpacity style={passengerCont} onPress={() => global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'SettingPinUpdate' }))}>
                <View style={upDownCont}>
                  <Text style={fTitle}>{strings.pin_code}</Text>
                  <Text style={fCaption}>{user.hasPin ? strings.change_pin : strings.add_new_pin}</Text>
                </View>
                <View style={navigateBtn}>{Icons.Generator.Material('chevron-right', 24, '#bbb')}</View>
              </TouchableOpacity>

              <TouchableOpacity style={passengerCont} onPress={() => global.$.navigator.dispatch(NavigationActions.navigate({
                routeName: 'AddBankAccount', params: {
                  title: strings.bank_detail,
                  countryCode: hasBank ? bankDetail.countryCode : 'MY',
                  mode: hasBank ? 'edit' : 'add',
                  bankDetail: hasBank ? bankDetail : {}
                }
              }))}>
                <View style={upDownCont}>
                  <Text style={fTitle}>{strings.bank_detail}</Text>
                  <Text style={fCaption}>{hasBank ? bankDetail.name : strings.addbank}</Text>
                </View>
                <View style={navigateBtn}>{Icons.Generator.Material('chevron-right', 24, '#bbb')}</View>
              </TouchableOpacity>
            </View>
          )
        }

        {
          selectedTab === 'Driver' && (
            <View>
              <TouchableOpacity style={listCont} onPress={() => global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'DriverPersonalInfo' }))}>
                <View style={{ flexDirection: 'row' }}>
                  {/* <View style={indicator}/> */}
                  <Text style={[fTitle, { padding: 25 }]}>{strings.settings_account_personal_info}</Text>
                </View>
                <View style={navigateBtn}>{Icons.Generator.Material('chevron-right', 24, '#bbb')}</View>
              </TouchableOpacity>

              <TouchableOpacity style={listCont} onPress={() => global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'VehicleInfo' }))}>
                <View style={{ flexDirection: 'row' }}>
                  {/* <View style={indicator}/> */}
                  <Text style={[fTitle, { padding: 25 }]}>{strings.settings_account_vehicle_info}</Text>
                </View>
                <View style={navigateBtn}>{Icons.Generator.Material('chevron-right', 24, '#bbb')}</View>
              </TouchableOpacity>

              <TouchableOpacity style={listCont} onPress={() => global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'PSVDocumentation' }))}>
                <View style={{ flexDirection: 'row' }}>
                  {/* <View style={indicator}/> */}
                  <Text style={[fTitle, { padding: 25 }]}>{strings.settings_account_psvdocumentation}</Text>
                </View>
                <View style={navigateBtn}>{Icons.Generator.Material('chevron-right', 24, '#bbb')}</View>
              </TouchableOpacity>

              <TouchableOpacity style={listCont} onPress={() => global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'EHailingPermit' }))}>
                <View style={{ flexDirection: 'row' }}>
                  {/* <View style={indicator}/> */}
                  <Text style={[fTitle, { padding: 25 }]}>{strings.settings_account_ehailing}</Text>
                </View>
                <View style={navigateBtn}>{Icons.Generator.Material('chevron-right', 24, '#bbb')}</View>
              </TouchableOpacity>

              <TouchableOpacity style={listCont} onPress={() => global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'EmergencyContact' }))}>
                <Text style={[fTitle, { padding: 25 }]}>{strings.settings_account_emergency_contact}</Text>
                <View style={navigateBtn}>{Icons.Generator.Material('chevron-right', 24, '#bbb')}</View>
              </TouchableOpacity>
            </View>
          )
        }
      </View>
    )
  }
}

const styles = StyleSheet.create({
  mainCont: {
    width,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: DCColor.BGColor('primary-1')
  },
  avatarInfoCont: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    padding: 20,
  },
  circleCont: {
    paddingRight: 10
  },
  picCont: {
    width: 68,
    height: 68,
    borderRadius: 34,
  },
  upDownCont: {
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'center',
    flex: 1
  },
  fTitle: {
    color: '#333',
    fontSize: TextFont.TextSize(15),
    fontWeight: '500',
  },
  fCaption: {
    color: 'rgba(0, 0, 0, 0.45)',
    fontSize: TextFont.TextSize(12),
    fontWeight: '200',
    paddingTop: 5
  },
  tabSection: {
    width,
    height: '10%',
    flexDirection: 'row',
    borderBottomColor: 'rgba(0, 0, 0, 0.05)',
    borderBottomWidth: 2,
    backgroundColor: 'white',
  },
  tabCont: {
    flexDirection: 'row',
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  tabContColor: {
    flexDirection: 'row',
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomColor: DCColor.BGColor('primary-1'),
    borderBottomWidth: 3
  },
  fDot: {
    height: 5,
    width: 5,
    backgroundColor: 'red',
    borderRadius: 2.5,
    paddingLeft: 5,
    marginBottom: 15
  },
  listCont: {
    width,
    paddingLeft: 5,
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomColor: 'rgba(0, 0, 0, 0.05)',
    borderBottomWidth: 2,
    backgroundColor: 'white',
  },
  passengerCont: {
    width,
    padding: 5,
    paddingLeft: 25,
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomColor: 'rgba(0, 0, 0, 0.05)',
    borderBottomWidth: 2,
  },
  qrcodeCont: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingRight: 15
  },
  qrcodeIcon: {
    height: 20,
    width: 20,
    resizeMode: 'contain'
  },
  navigateBtn: {
    justifyContent: 'flex-end',
    padding: 20
  },
  dcCard: {
    color: 'rgba(0, 0, 0, 0.45)',
    fontSize: TextFont.TextSize(12),
    fontWeight: '200'
  },
  indicator: {
    backgroundColor: 'red',
    width: 10,
    height: 70
  }
})
