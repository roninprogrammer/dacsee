import React, { Component } from 'react'
import { View,TouchableOpacity, WebView } from 'react-native'
import Material from 'react-native-vector-icons/MaterialIcons'
import { DCColor } from 'dacsee-utils'
/*****************************************************************************************************/
/*****************************************************************************************************/

export default class WebViewComponent extends Component {

  static navigationOptions = ({ navigation }) => {
    const { title ,callback } = navigation.state.params
    return {
      title,
      headerLeft:(<TouchableOpacity 
        activeOpacity={0.7} 
        style={{ top: 1, width: 54, paddingLeft: 8, justifyContent: 'center', alignItems: 'flex-start' }} 
        onPress={() =>{
          if(callback)callback()
          navigation.goBack()}}
      >
        <Material name={'keyboard-arrow-left'} size={30} color={DCColor.BGColor('primary-2')} />
      </TouchableOpacity>),
      drawerLockMode: 'locked-closed'
    }
  }
  
  render() {
    const { source } = this.props.navigation.state.params
    return (
      <View style={{ flex: 1 }}>
        <WebView source={source} />
      </View>
    )
  }

}