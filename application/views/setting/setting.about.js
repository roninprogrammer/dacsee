/* @flow */

import React, { Component } from 'react'
import { Text, View, TouchableOpacity, Image, Alert, ScrollView } from 'react-native'
import CodePush from 'react-native-code-push'
/*****************************************************************************************************/
import { Screen, Icons, Define, System, TextFont } from 'dacsee-utils'
import resources from 'dacsee-resources'
/*****************************************************************************************************/
import { inject, observer } from 'mobx-react'


@inject('app')
@observer
export default class SettingAboutScreen extends Component {

  static navigationOptions = () => {
    const { strings } = $.store.app
    return {
      title: strings.about,
      drawerLockMode: 'locked-closed'
    }
  }

  constructor(props) {
    super(props)
    this.state = {
      rnVersion: ''
    }
  }

  async componentDidMount() {
    const runningBundle = await CodePush.getUpdateMetadata(CodePush.UpdateState.RUNNING)
    this.setState({ rnVersion: (runningBundle ? runningBundle.label : '0') })
  }

  renderHtml(html: string) {
    return `
      <html>
        <head>
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <meta charset="utf-8" />
          <style>
            h3, h4 { font-weight: 'bold'; font-family: Arial}
            h3 { font-size: 16px; color: '#333' }
            h4 { font-size: 14px; color: '#666' }
            p { font-size: 13px; color: '#666'; font-family: Arial }
          </style>
        </head>
        <body>${html}</body>
      </html>
    `
  }

  render() {
    const { navigate, state } = this.props.navigation
    const { rnVersion } = this.state
    const { strings } = this.props.app
    return (
      <View style={{ flex: 1 }}>
        <ScrollView contentContainerStyle={{ flex: 1 }} style={{ flex: 1, backgroundColor: '#f8f8f8' }}>
          <View style={{ justifyContent: 'center', alignItems: 'center', paddingVertical: 30 }}>
            <Image style={{ width: 150, height: 150, resizeMode: 'contain' }} source={resources.image.logo_portrait_border} />
            <View style={{ alignItems: 'center', marginTop: 25, height: 30, justifyContent: 'space-between' }}>
              <Text style={{ fontSize: TextFont.TextSize(15), color: '#666' }}>{`${strings.version}: ${System.Version}-${rnVersion.replace('v', '')}`}</Text>
            </View>
            <View style={{ width: Screen.window.width, marginTop: 35, borderTopWidth: Define.system.ios.plus ? 1 : .5, borderBottomWidth: Define.system.ios.plus ? 1 : .5, borderColor: '#eaeaea' }}>
              <TouchableOpacity onPress={() => Alert.alert('', strings.app_not_ready_yet)} activeOpacity={0.7} style={{ paddingHorizontal: 12, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', height: 44, backgroundColor: 'white' }}>
                <Text style={{ color: '#333', fontSize: TextFont.TextSize(15), fontWeight: '400' }}>{strings.rating_appstore}</Text>
                {Icons.Generator.Material('chevron-right', 24, '#bbb')}
              </TouchableOpacity>
              <View style={{ paddingLeft: 12, backgroundColor: 'white' }}><View style={{ borderTopWidth: .5, borderColor: '#eaeaea' }} /></View>
              <TouchableOpacity onPress={() => navigate('SettingWetView', {
                title: strings.terms,
                source: { uri: 'https://dacsee.com/#/apps-terms-conditions' }
              })} activeOpacity={0.7} style={{ paddingHorizontal: 12, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', height: 44, backgroundColor: 'white' }}>
                <Text style={{ color: '#333', fontSize: TextFont.TextSize(15), fontWeight: '400' }}>{strings.terms}</Text>
                {Icons.Generator.Material('chevron-right', 24, '#bbb')}
              </TouchableOpacity>

            </View>
          </View>
        </ScrollView>
        <View style={{ position: 'absolute', bottom: 15, left: 0, right: 0, justifyContent: 'center', alignItems: 'center' }}>
          <Text style={{ color: '#b1b1b1', fontSize: TextFont.TextSize(11), backgroundColor: 'transparent' }}>2017-2018 DACSEE All rights reserved.</Text>
        </View>
      </View>
    )
  }
}
