import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { StyleSheet, ScrollView, View, Text, SafeAreaView, TouchableOpacity, Linking } from 'react-native'
import { Screen, TextFont, DCColor, Icons } from '../../utils'
import { NavigationActions } from 'react-navigation'
import moment from 'moment'

const { width } = Screen.window

@inject('app', 'account', 'vehicles')
@observer
export default class VehicleInfo extends Component {

  static navigationOptions = () => {
    const { strings } = $.store.app
    return {
      title: strings.settings_account_vehicle_info,
      drawerLockMode: 'locked-closed'
    }
  }

  constructor(props) {
    super(props)
    this.state = {
      data: {},
      insuranceInfo: [],
      roadTaxInfo: []
    }
  }

  async componentDidMount() {
    this.props.app.showLoading()
    const vehicleDoc = [];
    vehicleDoc.push(
      $.store.vehicles.getVehiclesData(),
      $.method.session.User.Get('v1/documents?type=vehicleInsurance'),
      // $.method.session.User.Get('v1/documents?type=vehicleRoadtax')
    );
    const getAllPromise = await Promise.all(vehicleDoc);
    const resp = getAllPromise[0]
    const insurance = getAllPromise[1];
    // const roadTax = getAllPromise[2];

    if (resp.length > 0) {
      this.setState({ data: resp[0]})
    }

    if (insurance && insurance.length) {
      this.setState({ insuranceInfo : insurance[0] });
    }

    // if (roadTax && roadTax.length){
    //   this.setState({ roadTaxInfo : roadTax[0] });
    // }
    this.props.app.hideLoading()
  }

  render() {
    const { mainCont, formCont, fTitle, fCaption , viewCont, imgBox, actionCont, updateCont, submitText , indicator, specialCont } = styles
    const { data, insuranceInfo, roadTaxInfo } = this.state
    const { registrationNo, manufacturer, model, color, maxSeatingCapacity } = data
    const { strings } = $.store.app
    const { expiredOn, custom } = insuranceInfo
    const { policyNo = '', policyProvider = '' } = custom || {}

    return (
      <SafeAreaView style={mainCont}>
        <View style={{flexDirection: 'column'}}>
          <ScrollView>
            <View style={formCont}>
              <Text style={fTitle}>{strings.settings_account_vehicle_info_plate_number}</Text>
              <Text style={fCaption}>{registrationNo ? registrationNo: strings.no_content}</Text>
            </View>

            <View style={formCont}>
              <Text style={fTitle}>{strings.settings_account_vehicle_info_vehicle_manufacturer}</Text>
              <Text style={fCaption}>{manufacturer? manufacturer: strings.no_content}</Text>
            </View>

            <View style={formCont}>
              <Text style={fTitle}>{strings.settings_account_vehicle_info_vehicle_model}</Text>
              <Text style={fCaption}>{model? model : strings.no_content}</Text>
            </View>

            <View style={formCont}>
              <Text style={fTitle}>{strings.settings_account_vehicle_info_vehicle_color}</Text>
              <Text style={fCaption}>{color? color : strings.no_content}</Text>
            </View>

            <View style={formCont}>
              <Text style={fTitle}>{strings.settings_account_vehicle_info_vehicle_max_seating}</Text>
              <Text style={fCaption}>{maxSeatingCapacity? maxSeatingCapacity : strings.no_content}</Text>
            </View>

            <View style={{flexDirection: 'row'}}>
              {/* <View style={indicator}/> */}
              <View style={formCont}>
                <Text style={fTitle}>{strings.settings_account_vehicle_info_vehicle_insurance_expiry_date}</Text>
                <Text style={fCaption}>{expiredOn ? moment(expiredOn).format('DD MMM YYYY'): strings.no_content}</Text>
              </View>
            </View>

            <View style={formCont}>
              <Text style={fTitle}>{strings.settings_account_vehicle_info_vehicle_insurance_policy_provider}</Text>
              <Text style={fCaption}>{policyProvider? policyProvider : strings.no_content}</Text>
            </View>

            <View style={formCont}>
              <Text style={fTitle}>{strings.settings_account_vehicle_info_vehicle_insurance_policy_number}</Text>
              <Text style={fCaption}>{policyNo? policyNo : strings.no_content}</Text>
            </View>

            {/* <View style={formCont}>
              {roadTaxInfo.attachment1 ? 
              <View style={viewCont}>
                <Text style={fTitle}>{strings.settings_account_vehicle_info_vehicle_roadtax}</Text>
                <TouchableOpacity style={imgBox}
                  onPress={() => global.$.navigator.dispatch(NavigationActions.navigate({
                      routeName: 'DriverImgViewer',
                      params: { img: roadTaxInfo.attachment1.url }
                  }))}>
                  {Icons.Generator.Material('search', 16, 'black')}
                </TouchableOpacity> 
              </View>
              : 
              <View>
                <Text style={fTitle}>{strings.settings_account_vehicle_info_vehicle_roadtax}</Text>
                <Text style={fCaption}>{strings.no_content}</Text>
              </View>
              }
            </View> */}

            {/* <View style={{flexDirection: 'row'}}>
              <View style={formCont}>
                <Text style={fTitle}>{strings.settings_account_vehicle_info_vehicle_roadtax_expiry_date}</Text>
                <Text style={fCaption}>{roadTaxInfo.expiredOn ? moment(roadTaxInfo.expiredOn).format('DD MMM YYYY') : strings.no_content}</Text>
              </View>
            </View> */}
          </ScrollView>
          <View style={actionCont}>
            <TouchableOpacity style={updateCont} onPress={() => Linking.openURL('http://bit.ly/dacsee-updatedocument')}>
              <Text style={submitText}>{strings.update_driver_document}</Text>
            </TouchableOpacity>
          </View>
        </View>
      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  mainCont: {
    width,
    height: '100%',
    flexDirection: 'row',
    flexWrap: 'nowrap',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  formCont: {
    width,
    padding: 15,
    backgroundColor: 'white',
    borderBottomColor: 'rgba(0, 0, 0, 0.05)',
    borderBottomWidth: 2,
    flexDirection: 'column',
  },
  fTitle: {
    color: '#333', 
    fontSize: TextFont.TextSize(15), 
    fontWeight: '500' ,
  },
  fCaption: {
    color: 'rgba(0, 0, 0, 0.45)', 
    fontSize: TextFont.TextSize(12), 
    fontWeight: '400' ,
    paddingTop: 5
  },
  viewCont: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  imgBox: {
    height: 40,
    width: 40,
    borderColor: 'rgba(0, 0, 0, 0.1)',
    borderWidth: 1,
    borderRadius: 40,
    alignItems: 'center',
    justifyContent: 'center'
  },
  actionCont:{
    height: '13%',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    backgroundColor: 'white',
    borderTopColor: 'rgba(0, 0, 0, 0.25)',
    borderTopWidth: 2
  },
  updateCont: {
    width: '90%',
    height: 50,
    backgroundColor: DCColor.BGColor('primary-1'),
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 5
  },
  submitText: {
    fontSize: TextFont.TextSize(16),
    fontWeight: '700',
    color: 'rgba(0, 0, 0, 0.75)',
  },
  indicator: {
    backgroundColor: 'red',
    width: 10, 
    height: 70,
    marginLeft: 2
  },
  specialCont: {
    flexDirection: 'row', 
    backgroundColor: 'white'
  }
})