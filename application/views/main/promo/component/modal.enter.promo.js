import React, { Component } from 'react'
import { Text, View, TouchableOpacity, KeyboardAvoidingView, Modal, StyleSheet, TextInput } from 'react-native'
import { inject, observer } from 'mobx-react'
import { Screen, TextFont, DCColor } from 'dacsee-utils'

const { height, width } = Screen.window
const styles = StyleSheet.create({
  dcBackDrop: {
    width,
    height,
    backgroundColor: 'rgba(0, 0, 0, .65)',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20
  },
  keyboardAvaoidingView: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  dcCard: {
    width: '90%',
    backgroundColor: 'white',
    padding: 20,
    borderRadius: 8,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: 'rgba(0, 0, 0, 0.15)',
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowRadius: 3,
    shadowOpacity: 1
  },
  fTitle: {
    textAlign: 'center',
    fontSize: TextFont.TextSize(16),
    fontWeight: '800',
    color: 'rgba(0, 0, 0, 0.75)',
    marginBottom: 5,
  },
  fCaption: {
    textAlign: 'center',
    fontSize: TextFont.TextSize(14),
    fontWeight: '600',
    color: 'rgba(0, 0, 0, 0.45)',
    marginBottom: 15,
  },
  dcInput: {
    width: '100%',
    backgroundColor: 'rgba(0, 0, 0 , 0.05)',
    marginBottom: 15,
    borderRadius: 5,
    height: 45,
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: TextFont.TextSize(18),
    fontWeight: '800',
    color: 'rgba(0, 0 , 0, 0.75)',
    textAlign: 'center'
  },
  actionCont: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  dcBtn: {
    borderRadius: 6,
    flex: 2,
    height: 45,
    backgroundColor: 'rgba(0, 0, 0, 0.05)',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  activeBtn: {
    flex: 3,
    backgroundColor: DCColor.BGColor('primary-1'),
    marginLeft: 10
  },
  btnText: {
    fontSize: TextFont.TextSize(16),
    fontWeight: '700',
    color: 'rgba(0, 0, 0, 0.45)',
    textAlign: 'center'
  },
  activeText: {
    color: 'rgba(0, 0, 0, 0.75)'
  }
})

@inject('app', 'passenger')
@observer
export default class PromoModel extends Component {
  constructor(props) {
    super(props)
    this.state = {
      promo_code: ''
    }
  }

  render() {
    const {
      dcBackDrop,
      keyboardAvaoidingView,
      dcCard,
      fTitle,
      fCaption,
      dcInput,
      actionCont,
      dcBtn,
      activeBtn,
      btnText,
      activeText
    } = styles

    const { app, passenger } = this.props
    const { strings } = app
    const visible = passenger.promoModalVisible

    return (
      <Modal
        animationType='fade'
        transparent={true}
        visible={visible}
        onRequestClose={() => {
          passenger.promoModalVisible = false
        }}
      >
        <View style={dcBackDrop}>
          <KeyboardAvoidingView behavior='padding' contentContainerStyle={keyboardAvaoidingView} style={keyboardAvaoidingView}>
            <View style={dcCard}>
              <Text style={fTitle}>{strings.promo_code}</Text>
              <Text style={fCaption}>{strings.promo_code_desc}</Text>
              <TextInput
                style={dcInput}
                placeholder={strings.enter_here}
                underlineColorAndroid='transparent'
                onChangeText={text => {
                  this.setState({ promo_code: text })
                }}
                autoCapitalize={'characters'}
                autoFocus={true}
              />
              <View style={actionCont}>
                <TouchableOpacity
                  style={dcBtn}
                  onPress={() => {
                    passenger.promoModalVisible = false
                    this.setState({ promo_code: '' })
                  }}
                >
                  <Text style={btnText}>{!!strings.cancel && strings.cancel.toUpperCase()}</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={[dcBtn, activeBtn]} onPress={() => {
                    passenger.promoModalVisible = false
                    passenger.applyPromo(this.state.promo_code)
                    this.setState({ promo_code: '' })
                  }}
                >
                  <Text style={[btnText, activeText]}>{!!strings.cancel && strings.confirm.toUpperCase()}</Text>
                </TouchableOpacity>
              </View>
            </View>
          </KeyboardAvoidingView>
        </View>
      </Modal >
    )
  }
}


