
import PromoModel from './modal.enter.promo'
import PromoDetailModel from './modal.promo.detail'
import FareAlertModel from './modal.fare.alert'

export {
  PromoModel,
  PromoDetailModel,
  FareAlertModel
}