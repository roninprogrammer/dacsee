import React, { Component } from 'react'
import { Text, View, ScrollView, TouchableOpacity, KeyboardAvoidingView, Modal, StyleSheet, Image } from 'react-native'
import { inject, observer } from 'mobx-react'
import { Screen, TextFont, DCColor } from 'dacsee-utils'
import Resources from 'dacsee-resources'

const { height, width } = Screen.window

@inject('app', 'jobs')
@observer
export default class FareAlertModel extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    const {
      dcBackDrop,
      keyboardAvaoidingView,
      dcCard,
      dcImg,
      hasImg,
      fTitle,
      fCaption,
      actionCont,
      dcBtn,
      activeBtn,
      btnText,
      activeText,
      fareLine,
      fareLineBottom
    } = styles

    const { jobs, app } = this.props
    const { strings } = app
    let { fareObj = {}, additional_fees = 0 } = jobs.active_job

    // fareObj breakdown
    let { amount = 0, promo_discount = 0, originalAmount = 0, campaign_discount = 0, incentiveAmount = 0 } = fareObj
    let total = 0

    additional_fees = parseFloat(additional_fees)
    originalAmount = parseFloat(originalAmount)
    promo_discount = parseFloat(promo_discount)
    campaign_discount = parseFloat(campaign_discount)

    amount = !!originalAmount ? originalAmount : parseFloat(amount)

    total = amount + additional_fees - promo_discount - campaign_discount

    const visible = jobs.fareInfoModalVisible

    return (
      <Modal
        animationType='fade'
        transparent={true}
        visible={visible}
        onRequestClose={() => {
          jobs.fareInfoModalVisible = false
        }}
      >
        <View style={dcBackDrop}>
          <KeyboardAvoidingView behavior='position' contentContainerStyle={keyboardAvaoidingView} style={keyboardAvaoidingView}>
            <View style={dcCard}>
              <Image style={dcImg} source={Resources.image.attention_icon} />
              <Text style={fTitle}>{strings.fare_breakdown_title}</Text>

              <ScrollView style={{ width: '100%' }}>
                <View style={{ width: '100%', marginTop: 15, borderWidth: 1, borderRadius: 4, borderColor: '#ccc' }}>
                  <FareLine label={strings.fare} amount={`RM ${parseFloat(amount).toFixed(2)}`} />

                  {!!additional_fees &&
                    <FareLine label={strings.tolls_fare} amount={`RM ${additional_fees.toFixed(2)}`} style={fareLine} />
                  }

                  {!!promo_discount &&
                    <FareLine label={strings.promo} amount={`- RM ${promo_discount.toFixed(2)}`} style={fareLine} />
                  }

                  {!!campaign_discount &&
                    <FareLine label={strings.fare_joy_promo} amount={`- RM ${campaign_discount.toFixed(2)}`} style={fareLine} />
                  }

                  <FareLine label={strings.fare_collect_cash.toUpperCase()} amount={`RM ${total.toFixed(2)}`} style={[fareLine, fareLineBottom, { backgroundColor: DCColor.BGColor('disable')}]} />
                </View>

                {
                  !!incentiveAmount && (
                    <View style={{ width: '100%', marginTop: 8, borderWidth: 1, borderRadius: 4, borderColor: '#ccc' }}>
                      <FareLine label={strings.bonus_label} amount={`+ RM ${incentiveAmount.toFixed(2)}`} style={[fareLine, { borderTopWidth: 0}]} />
                    </View>
                  )
                }
              </ScrollView>

              <Text style={fCaption}>{strings.fare_breakdown_placeholder()}</Text>

              <View style={actionCont}>
                <TouchableOpacity style={[dcBtn, activeBtn]} onPress={() => $.store.jobs.fareInfoModalVisible = false}>
                  <Text style={[btnText, activeText]}>{$.store.app.strings.okay.toUpperCase()}</Text>
                </TouchableOpacity>
              </View>
            </View>
          </KeyboardAvoidingView>
        </View>
      </Modal >
    )
  }
}

const FareLine = (props) => {
  const { label = '', amount = '', style } = props

  return (
    <View style={[{ flexDirection: 'row', justifyContent: 'space-between', paddingLeft: 15, alignItems: 'center', height: 50 }, style]}>
      <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', flex: 1 }}>
        <Text style={{ color: '#7f7f7f', fontSize: TextFont.TextSize(14), fontWeight: '600' }}>{label}</Text>
      </View>
      <Text style={{ color: '#000', fontSize: 14, fontWeight: '600', marginRight: 15 }}>{amount}</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  dcBackDrop: {
    width,
    height,
    backgroundColor: 'rgba(0, 0, 0, 0.65)',
    justifyContent: 'center',
    alignItems: 'center'
  },
  keyboardAvaoidingView: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  dcCard: {
    width: '90%',
    backgroundColor: 'white',
    padding: 20,
    borderRadius: 7,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: 'rgba(0, 0, 0, 0.15)',
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowRadius: 7,
    shadowOpacity: 1
  },
  scrollView: {
    height: 300,
    width: '100%',
  },
  dcImg: {
    height: width / 4,
    width: width / 4,
    resizeMode: 'contain',
    marginBottom: 10,
    marginTop: 10
  },
  hasImg: {
    width: '100%'
  },
  fTitle: {
    textAlign: 'center',
    fontSize: TextFont.TextSize(16),
    fontWeight: '800',
    color: 'rgba(0, 0, 0, 0.75)',
    marginBottom: 5,
  },
  fSubtitle: {
    textAlign: 'center',
    fontSize: TextFont.TextSize(14),
    fontWeight: '800',
    color: 'rgba(0, 0, 0, 0.45)',
    marginBottom: 10,
  },
  fCaption: {
    textAlign: 'left',
    color: 'rgba(0, 0, 0, 0.45)',
    marginTop: 15,
    marginBottom: 10,
  },
  actionCont: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  dcBtn: {
    borderRadius: 4,
    flex: 2,
    height: 45,
    backgroundColor: 'rgba(0, 0, 0, 0.05)',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  activeBtn: {
    flex: 3,
    backgroundColor: DCColor.BGColor('primary-1')
  },
  btnText: {
    fontSize: TextFont.TextSize(16),
    fontWeight: '700',
    color: 'rgba(0, 0, 0, 0.45)',
    textAlign: 'center'
  },
  activeText: {
    color: 'rgba(0, 0, 0, 0.75)'
  },
  fareLine: { 
    borderTopWidth: 1, 
    borderColor: '#ccc'
  },
  fareLineBottom: { 
    borderBottomLeftRadius: 4, 
    borderBottomRightRadius: 4
  }
})


