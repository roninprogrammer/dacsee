import SponsorDetailScreen from './sponsor.locations.details'
import SponsorLocationScreen from './sponsor.locations'

export {
  SponsorDetailScreen,
  SponsorLocationScreen
}