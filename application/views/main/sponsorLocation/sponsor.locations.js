import React, { Component } from 'react'
import { Text, View, StyleSheet, TouchableOpacity, SafeAreaView, ListView } from 'react-native'
import { inject, observer } from 'mobx-react'
import Resources from 'dacsee-resources'
import SponsorListItem from './component/Sponsor.List.Item'
import { Screen, TextFont, DCColor } from 'dacsee-utils'
import SponsorInfo from './component/Sponsor.info';

const { width } = Screen.window
const styles = StyleSheet.create({
  mainCont: {
    width,
    height: '100%',
    backgroundColor: '#F2F2F2',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    flexWrap: 'nowrap'
  },
  notiCont: {
    width,
    padding: 5
  },

  listCont: {
    flex: 1,
  },
  btnCont: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width,
    padding: 15
  },
  longbutton: {
    flex: 1,
    backgroundColor: DCColor.BGColor('primary-1'),
    height: 40,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative',
  },
  skipIcon: {
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: TextFont.TextSize(16),
    fontWeight: '600',
    color: 'black'
  }
})
const dataContrast = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1._id !== r2._id, sectionHeaderHasChanged: (s1, s2) => s1 !== s2 })

@inject('app', 'joy', 'passenger')
@observer
export default class SponsorLocationScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const { strings } = $.store.app
    return {
      drawerLockMode: 'locked-closed',
      title: strings.sponsored_title,
    }
  }

  constructor(props) {
    super(props)
    this.state = {
      fetching: false,
      hasMore: true,
      onEndReachedCalledDuringMomentum: true,
      locationsList: []
    }
    this.LISTING_BATCH_SIZE = 15
  }

  async componentDidMount() {
    this._onEndReached()
  }

  async fetchLocationsBatch(initialLoad) {
    const { fetching, hasMore } = this.state
    if (fetching || !hasMore) return

    this.setState({ fetching: true })
    const { locationsList = [] } = this.state
    const { getSponsorLocationList } = this.props.joy
    if (initialLoad && Array.isArray(locationsList) && locationsList.length >= this.LISTING_BATCH_SIZE) {
      return this.setState({ fetching: false, hasMore: true })
    }
    const locationsBatch = await getSponsorLocationList({ limit: this.LISTING_BATCH_SIZE, skip: locationsList.length }) || []
    const stillHasMore = !locationsBatch || (Array.isArray(locationsBatch) && locationsBatch.length === this.LISTING_BATCH_SIZE)
    this.setState({fetching: false, hasMore: stillHasMore, locationsList: [...locationsList, ...locationsBatch]})
  }

  _keyExtractor = (item) => (item._id)

  _renderItem = (item) => (
    <SponsorListItem
      url={$.method.utils.getPicSource(item.avatars, useLarge = false, Resources.image.joy_placeholder)}
      title={item.name}
      desc={item.shortDesc}
      buttonDesc={item.buttonDesc}
      onPress={() => {
        $.store.passenger.updateJoyLocation(item)
        $.store.passenger.enableChatButton('load')
        if ($.store.passenger.status >= 4) {
          this.props.navigation.navigate('Main') // Skip CircleSelect - Default to first official group
        }
      }}
      infoPress={() => this.props.navigation.navigate('SponsorDetailScreen', {
        id: item._id
      })}
    />
  )


  _onEndReached() {
    this.fetchLocationsBatch(this.state.locationsList.length === 0)
  }

  render() {
    const { strings } = this.props.app
    const {
      mainCont,
      notiCont,
      longbutton,
      btnCont,
      listCont,
      skipIcon
    } = styles

    return (
      <SafeAreaView style={mainCont}>
        <View style={notiCont}>
          <SponsorInfo
            icon={Resources.image.sponsored_location}
            label={strings.sponsored_location}
          />
        </View>
        <ListView 
          removeClippedSubviews={false} 
          style={listCont}
          enableEmptySections
          dataSource={dataContrast.cloneWithRows(this.state.locationsList)}
          keyExtractor={this._keyExtractor}
          renderRow={(rowData) => this._renderItem(rowData)}
          onEndReached={() => this._onEndReached()}
        />
        <View style={btnCont}>
          <TouchableOpacity style={longbutton} onPress={() => {
            this.props.navigation.navigate('Main')
          }}>
            <Text style={skipIcon}>CLOSE</Text>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    )

  }

}

