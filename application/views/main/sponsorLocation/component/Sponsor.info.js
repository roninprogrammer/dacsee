import React from 'react'
import { View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native'
import { TextFont } from 'dacsee-utils'
import Resource from 'dacsee-resources'

const styles = StyleSheet.create({
  subCont: {
    borderRadius: 8,
    flexDirection: 'row',
    width: '100%',
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative',
    shadowOffset: { width: 0, height: -1 },
    shadowColor: '#000',
    shadowOpacity: 0.2,
    elevation: 0.5
  },
  imgCont: {
    padding: 10,
    borderBottomLeftRadius: 5,
    borderTopLeftRadius: 5,
  },
  dcImg: {
    height: 35,
    width: 35,
    resizeMode: 'contain',
  },
  dcText: {
    flex: 1,
    fontWeight: '400',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: TextFont.TextSize(12),
    lineHeight: TextFont.TextSize(13),
    color: 'rgba(0, 0, 0, 0.5)',
    fontWeight: '500',
    marginLeft: 10,
    marginRight: 10,
  },
  dcIcon: {
    width: 8, 
    height: 8, resizeMode: 'contain',
    marginRight: 10
  }
})

const SponsorInfo = ({ icon, label, onPress, contStyle, imgContStyle, textStyle }) => {
  const {
    subCont,
    imgCont,
    dcImg,
    dcText,
    dcIcon
  } = styles

  return (
    <TouchableOpacity style={contStyle ? [subCont, contStyle] : subCont} onPress={onPress}>
      <View style={imgContStyle ? [imgCont, imgContStyle] : imgCont}>
        <Image style={dcImg} source={icon} />
      </View>
      <Text style={textStyle ? [dcText,  textStyle] : dcText}>{label}</Text>
      {onPress &&
        <Image style={dcIcon} source={Resource.image.right_icon} />
      }
    </TouchableOpacity>
  )
}

export default SponsorInfo
