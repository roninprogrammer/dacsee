import React, { Component } from 'react'
import { Text, View, TouchableOpacity, Image, Modal, StyleSheet } from 'react-native'
import { inject, observer } from 'mobx-react'
import { Screen, TextFont, DCColor } from 'dacsee-utils'
import Resources from 'dacsee-resources'

const { height, width } = Screen.window
const styles = StyleSheet.create({
  dcBackDrop: {
    width,
    height,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.75)'
  },
  dcCard: {
    width: '80%',
    padding: 20,
    borderRadius: 8,
    backgroundColor: 'white',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  dcImg: {
    height: 130,
    width: 130,
    resizeMode: 'contain',
    marginBottom: 30
  },
  fTitle: {
    textAlign: 'center',
    fontSize: TextFont.TextSize(18),
    lineHeight: TextFont.TextSize(18),
    fontWeight: '800',
    color: 'rgba(0, 0, 0, 0.75)',
    marginBottom: 40
  },
  fBody: {
    fontSize: TextFont.TextSize(16),
    color: 'rgba(0, 0, 0, 0.65)',
    lineHeight: (TextFont.TextSize(16) + 3),
    marginBottom: 40,
    textAlign: 'center'
  },
  actionCont: {
    width: '100%',
    height: 50,
    flexDirection: 'row',
    alignContent: 'center',
    justifyContent: 'center',
  },
  dcBtn: {
    flex: 1,
    borderRadius: 8,
    flex: 3,
    backgroundColor: DCColor.BGColor('primary-1'),
    padding: 10,
    paddingLeft: 10,
    paddingRight: 10,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  cancelBtn: {
    flex: 2,
    backgroundColor: DCColor.BGColor('disable'),
    marginRight: 10,
  },
  btnFont: {
    fontSize: TextFont.TextSize(16),
    fontWeight: '900',
    color: 'rgba(0, 0, 0, 0.9)',
    textAlign: 'center'
  },
  cancelFont: {
    color: 'rgba(0, 0, 0, 0.65)'
  }
})

@inject('app', 'passenger')
@observer
export default class SponsorLocationModal extends Component {

  constructor(props) {
    super(props)
    this.state = {
    }
  }

  render() {
    const {
      dcBackDrop,
      dcCard,
      dcImg,
      fTitle,
      actionCont,
      dcBtn,
      cancelBtn,
      btnFont,
      cancelFont
    } = styles
    const { app, passenger, navigation } = this.props
    const visible = passenger.sponsorVisible
    return (
      <Modal
        animationType='fade'
        transparent={true}
        visible={visible}
        onRequestClose={() => this.props.onClose()}
      >
        <View style={dcBackDrop}>
          <View style={dcCard}>
            <Image style={dcImg} source={Resources.image.sponsored_location} />
            <Text style={fTitle}>{app.strings.sponsored_modal_desc.toUpperCase()}</Text>
            <View style={actionCont}>
              <TouchableOpacity style={[dcBtn, cancelBtn]} onPress={() => {
                passenger.sponsorVisible = false
              }}>
                <Text style={[btnFont, cancelFont]}>{app.strings.cancel.toUpperCase()}</Text>
              </TouchableOpacity>
              <TouchableOpacity style={dcBtn} onPress={() => {
                passenger.sponsorVisible = false
                navigation.navigate('SponsorLocationScreen')
              }}>
                <Text style={btnFont}>{app.strings.get_it_now.toUpperCase()}</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal >
    )
  }

}