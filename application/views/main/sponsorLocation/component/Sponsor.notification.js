import React from 'react'
import { Text, View, Image, StyleSheet, TouchableOpacity } from 'react-native'
import { TextFont, DCColor } from 'dacsee-utils'
import Resource from 'dacsee-resources'


const styles = StyleSheet.create({
  mainCont: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    width: '100%',
    alignItems: 'center',
    borderRadius: 10,
    backgroundColor: 'white',
  },
  picCont: {
    resizeMode: 'cover',
    width: 80,
    height: 80,
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
  },
  topbottom: {
    flex: 1,
    padding: 10
  },
  leftRight: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  descCont: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'flex-start'
  },
  shopName: {
    fontWeight: '900',
    fontSize: TextFont.TextSize(16),
    color: 'rgba(0, 0, 0, 0.75)',
    textAlign: 'left'
  },
  caption: {
    fontWeight: '400',
    alignItems: 'center',
    fontSize: TextFont.TextSize(14),
    color: 'rgba(0, 0, 0, 0.5)',
    textAlign: 'left'
  },
  more: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  iconCont: {
    height: 25,
    width: 25,
    resizeMode: 'contain',
    tintColor: '#D9D9D9'
  },
  clickCont: {
    flex: 1,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: DCColor.BGColor('primary-1'),
    borderRadius: 5,
  },
  buttonText: {
    fontSize: TextFont.TextSize(16),
    color: 'white'
  }
})

const SponsorNotification = ({ shop, mainName, captionName, onPress, details, strings }) => {
  const {
    mainCont,
    picCont,
    topbottom,
    leftRight,
    descCont,
    shopName,
    caption,
    more,
    iconCont,
    clickCont,
    buttonText
  } = styles

  return (
    <View style={mainCont}>
      <Image style={picCont} source={shop} />
      <View style={topbottom}>
        <View style={leftRight}>
          <View style={descCont}>
            <Text style={shopName} numberOfLines={1} ellipsizeMode='tail'>{mainName}</Text>
            <Text style={caption} numberOfLines={1} ellipsizeMode='tail'>{captionName}</Text>
          </View>
          <TouchableOpacity style={more} onPress={details}>
            <Image style={iconCont} source={Resource.image.info_icon} />
          </TouchableOpacity>
        </View>
        <TouchableOpacity style={clickCont} onPress={onPress}>
          <Text style={buttonText}>{strings.more_sponsored_location}</Text>
        </TouchableOpacity>
      </View>
    </View>
  )
}

export default SponsorNotification
