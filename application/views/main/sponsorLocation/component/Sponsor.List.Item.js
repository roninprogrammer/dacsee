import React from 'react'
import { Text, View, Image, StyleSheet, TouchableOpacity } from 'react-native'
import { Screen, TextFont, DCColor } from 'dacsee-utils'
import Resources from 'dacsee-resources'

const { width } = Screen.window
const styles = StyleSheet.create({
  bigcont: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: 'white',
    width,
    borderBottomWidth: 0.5,
    borderColor: 'rgba(0, 0, 0, .45)'
  },
  restImage: {
    width: width / 100 * 30,
    height: width / 100 * 30,
    resizeMode: 'cover'
  },
  topbottom: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
  },
  rightcont: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 5
  },
  resname: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    backgroundColor: 'white',
  },
  maintitle: {
    fontSize: TextFont.TextSize(16),
    fontWeight: '900',
    color: 'rgba(0,0,0,0.75)',
    textAlign: 'left'
  },
  describe: {
    fontSize: TextFont.TextSize(12),
    fontWeight: '400',
    color: 'rgba(0,0,0,0.45)',
    textAlign: 'left'
  },
  dcnode: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  redeem: {
    height: 30,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: DCColor.BGColor('primary-1'),
    borderRadius: 6,
    flexDirection: 'row'
  },
  buttonFont: {
    fontSize: TextFont.TextSize(14),
    fontWeight: '700',
    color: DCColor.BGColor('primary-2')
  },
  dcIcon: {
    height: 25,
    width: 25,
    resizeMode: 'contain',
    tintColor: '#D9D9D9'
  },
})

const SponsorListItem = ({ url, title, desc, onPress, infoPress, buttonDesc }) => {
  const {
    bigcont,
    rightcont,
    resname,
    maintitle,
    restImage,
    describe,
    redeem,
    dcIcon,
    topbottom,
    buttonFont,
    dcnode
  } = styles

  return (
    <TouchableOpacity style={bigcont} onPress={infoPress}>
      <Image style={restImage} source={url} />
      <View style={topbottom}>
        <View style={rightcont}>
          <View style={resname}>
            <Text style={maintitle} numberOfLines={1} ellipsizeMode='tail'>{title}</Text>
            <Text style={describe} numberOfLines={1} ellipsizeMode='tail'>{desc}</Text>
          </View>
          <View style={dcnode}>
            <Image style={dcIcon} source={Resources.image.info_icon} />
          </View>
        </View>
        <TouchableOpacity style={redeem} onPress={onPress}>
          <Text style={buttonFont} >{buttonDesc}</Text>
        </TouchableOpacity>
      </View>
    </TouchableOpacity>
  )
}

export default SponsorListItem