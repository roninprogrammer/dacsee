import React, { Component } from 'react'
import { Text, View, StyleSheet, ScrollView, TouchableOpacity, Animated } from 'react-native'
import { inject, observer } from 'mobx-react'
import moment from 'moment'
import Material from 'react-native-vector-icons/MaterialIcons'
import { Screen, TextFont, DCColor } from 'dacsee-utils'

const { width } = Screen.window

@inject('app', 'passenger')
@observer
class NavigatorHeaderLeftButton extends Component {
  render() {
    const { app, passenger } = this.props
    const { backToMain = false, backToAddressPicker = false, selectedTab = 'joyPlaces', keywords = '' } = this.props.navigation.state.params

    return (
      <TouchableOpacity
        activeOpacity={0.7}
        style={{ top: 1, width: 54, paddingLeft: 8, justifyContent: 'center', alignItems: 'flex-start' }}
        onPress={() => {
          if (backToMain) {
            this.props.navigation.navigate('Main')
          } else if (backToAddressPicker) {
            this.props.navigation.navigate('PickerAddressModal', {
              type: 'DestinationAddress',
              selectedTab: selectedTab,
              keywords: keywords,
              onPickAddress: (item) => {
                if (item.joy) {
                  passenger.setJoyLocation(item)
                } else {
                  passenger.setDestinationAddress(item)
                }
              }
            })
          } else {
            this.props.navigation.navigate('SponsorLocationScreen')
          }
        }}
      >
        <Material name={'keyboard-arrow-left'} size={30} color={DCColor.BGColor('primary-2')} />
      </TouchableOpacity>
    )
  }
}

@inject('app', 'joy')
@observer
export default class SponsorDetailScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const { strings } = $.store.app
    return {
      drawerLockMode: 'locked-closed',
      title: strings.sponsored_details,
      headerLeft: (
        <NavigatorHeaderLeftButton navigation={navigation} />
      )
    }
  }

  constructor(props) {
    super(props)
    this.state = {
      redeemStatus: 'new',
      initOpacity: new Animated.Value(0.0)
    }
    this.getSponsorLocationDetail = props.joy.getSponsorLocationDetail
  }

  async componentDidMount() {
    await this.getSponsorLocationDetail(this.props.navigation.state.params.id)
  }

  async componentDidUpdate(prevProps) {
    const { navigation: { state: { params: { id: prevId } } } } = prevProps
    const { navigation: { state: { params: { id: currentId } } } } = this.props
    if (currentId !== prevId) {
      await this.getSponsorLocationDetail(currentId)
    }
  }

  onLoad = () => {
    Animated.timing(this.state.initOpacity, {
      toValue: 1,
      duration: 250,
      useNativeDriver: true
    }).start()
  }

  render() {
    const { initOpacity } = this.state
    const { name, endOn, longDesc, buttonDesc, tnc, avatars, _id: joy_location_id } = this.props.joy.sponsorLocationDetail
    const { app, passenger, navigation, joy } = this.props
    const { strings } = app
    const { backToAddressPicker = false, selectedTab = 'joyPlaces', keywords = '' } = this.props.navigation.state.params
    const {
      mainCont,
      scrollCont,
      imgCont,
      underLine,
      titleCont,
      fTitle,
      fCaption,
      fBody,
      bodyCont,
      fSubTitle,
      aLeft,
      actionCont,
      dcBtn,
      btnFont,
    } = styles

    return (
      <View style={mainCont}>
        <ScrollView style={scrollCont}>
          {avatars &&
            <Animated.Image
              style={[imgCont, { opacity: initOpacity }]}
              source={{ uri: avatars[avatars.length - 1].url }}
              onLoad={this.onLoad}
            />
          }
          <View style={underLine} />
          <View style={titleCont}>
            <Text style={fTitle}>{name ? name : null}</Text>
            {endOn ? <Text style={fCaption}>{strings.expiredOn} : {moment(endOn).format('DD MMM YYYY')}</Text> : null}
            <Text style={fBody}>{longDesc ? longDesc : null}</Text>
          </View>
          <View style={bodyCont}>
            <View>
              <Text style={fSubTitle}>{strings.terms}</Text>
              <Text style={[fBody, aLeft]}>{tnc ? tnc : null}</Text>
            </View>
          </View>
        </ScrollView>

        {$.store.passenger.joy_location_id && $.store.passenger.joy_location_id === joy.sponsorLocationDetail._id ?
          <View style={actionCont}>
            <TouchableOpacity style={dcBtn} onPress={() => {
              backToAddressPicker ?
                navigation.navigate('PickerAddressModal', {
                  type: 'DestinationAddress',
                  selectedTab: selectedTab,
                  keywords: keywords,
                  onPickAddress: (item) => {
                    if (item.joy) {
                      passenger.setJoyLocation(item)
                    } else {
                      passenger.setDestinationAddress(item)
                    }
                  }
                }) :
                navigation.navigate('Main')
            }}>
              <Text style={btnFont}>{strings.okay}</Text>
            </TouchableOpacity>
          </View>
          : null
        }

        {$.store.passenger.joy_location_id && $.store.passenger.joy_location_id !== joy.sponsorLocationDetail._id ?
          <View style={actionCont}>
            <TouchableOpacity style={dcBtn} onPress={() => {
              $.store.passenger.id ?
                $.store.passenger.updateJoyLocation(this.props.joy.sponsorLocationDetail) :
                $.store.passenger.setJoyLocation(this.props.joy.sponsorLocationDetail)
                if ($.store.passenger.status >= 4) {
                  this.props.navigation.navigate('Main') // Skip CircleSelect - Default to first official group
                }
            }}>
              <Text style={btnFont}>{strings.choose_sponsored}</Text>
            </TouchableOpacity>
          </View>
          : null
        }

        {!$.store.passenger.joy_location_id ?
          <View style={actionCont}>
            <TouchableOpacity style={dcBtn} onPress={() => {
              $.store.passenger.id ?
                $.store.passenger.updateJoyLocation(this.props.joy.sponsorLocationDetail) :
                $.store.passenger.setJoyLocation(this.props.joy.sponsorLocationDetail)
                if ($.store.passenger.status >= 4) {
                  this.props.navigation.navigate('Main') // Skip CircleSelect - Default to first official group
                }
            }}>
              <Text style={btnFont}>{buttonDesc ? buttonDesc.toUpperCase() : null}</Text>
            </TouchableOpacity>
          </View>
          : null
        }
      </View>
    )
  }
}

const styles = StyleSheet.create({
  mainCont: {
    width,
    height: '100%',
    flexDirection: 'column',
    flexWrap: 'nowrap',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    backgroundColor: 'white'
  },
  scrollCont: {
    flex: 1,
    width
  },
  imgCont: {
    width,
    height: 250,
    resizeMode: 'cover',

  },
  underLine: {
    width,
    height: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.1)'
  },
  titleCont: {
    width,
    padding: 25,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  fTitle: {
    fontSize: TextFont.TextSize(22),
    color: 'rgba(0, 0, 0, 0.75)',
    flex: 1,
    textAlign: 'center',
    marginBottom: 3,
    fontWeight: '900',
  },
  fCaption: {
    fontSize: TextFont.TextSize(14),
    color: 'rgba(0, 0, 0, 0.45)',
    flex: 1,
    textAlign: 'center',
    marginBottom: 20
  },
  fBody: {
    fontSize: TextFont.TextSize(15),
    color: 'rgba(0, 0, 0, 0.65)',
    flex: 1,
    textAlign: 'center',
    fontWeight: '700'
  },
  bodyCont: {
    width,
    flex: 1,
    backgroundColor: '#F3F3F3',
    padding: 35,
  },
  fSubTitle: {
    fontSize: TextFont.TextSize(18),
    color: 'rgba(0, 0, 0, 0.75)',
    flex: 1,
    textAlign: 'left',
    marginBottom: 5,
    fontWeight: '800'
  },
  aLeft: {
    textAlign: 'left',
    marginBottom: 10
  },
  aCenter: {
    textAlign: 'center',
    marginBottom: 0
  },
  actionCont: {
    flexDirection: 'row',
    width,
    height: 80,
    padding: 15,
    flexWrap: 'nowrap',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white'
  },
  dcBtn: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    borderRadius: 8,
    height: 50,
    backgroundColor: DCColor.BGColor('primary-1'),
    shadowOffset: { width: 0, height: -1 },
    shadowColor: '#000',
    shadowOpacity: 0.1,
    elevation: 0.5
  },
  btnFont: {
    fontSize: TextFont.TextSize(16),
    color: 'rgba(0, 0, 0, 0.75)',
    fontWeight: '800',
    textAlign: 'center'
  },
  receiptCont: {
    width,
    flexDirection: 'column',
    alignContent: 'flex-start',
    justifyContent: 'flex-start',
  },
  headCont: {
    width,
    height: 40,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  dcLine: {
    height: 5,
    backgroundColor: DCColor.BGColor('primary-1'),
    width
  },
  receiptBox: {
    flexDirection: 'row',
    width,
    padding: 15,
    alignItems: 'center',
    justifyContent: 'center',
  },
  receiptImg: {
    width: 80,
    height: 80,
    resizeMode: 'cover',
    marginRight: 15
  },
  dataCont: {
    flex: 1,
    flexDirection: 'column',
    alignContent: 'center',
    justifyContent: 'center',
    minHeight: 80,
    height: 80
  },
  dcStatus: {
    fontSize: TextFont.TextSize(18),
    fontWeight: '800',
    color: 'rgba(0, 0, 0, 0.75)',
    textAlign: 'left',
    marginBottom: 0
  },
  fDesc: {
    flex: 0,
    marginBottom: 0,
    textAlign: 'left'
  },
  retakeBtn: {
    width: '100%',
    height: 40,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 8,
    backgroundColor: DCColor.BGColor('primary-1'),
    shadowOffset: { width: 0, height: -1 },
    shadowColor: '#000',
    shadowOpacity: 0.1,
    elevation: 0.5
  }
})