/**
 * modal.remark
 * Created by yml on 2018/04/20.
 */
import React, { Component } from 'react'
import {
  View,
  Image,
  Modal,
  TouchableWithoutFeedback,
} from 'react-native'
import { Screen } from 'dacsee-utils'
const { height, width } = Screen.window


export default class ImageModel extends Component {

  constructor(props) {
    super(props)
    this.state={
      info:{},
      uri:''
    }
  }

  render() {
    const { visible, close = () => {} , uri} = this.props
    return (
      <Modal
        animationType='fade'           //渐变
        transparent={true}             // 不透明
        visible={visible}    // 根据isModal决定是否显示
        onRequestClose={() => this.props.close()}  // android必须实现 安卓返回键调用
      >
        <TouchableWithoutFeedback onPress={close}>
          <View style={{ width: width, height: height, justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgba(0,0,0,1)' }}>
            <Image
              style={{width:width,height:width}}
              source={{uri}}
            />
          </View>
        </TouchableWithoutFeedback>
      </Modal>
    )
  }

}
