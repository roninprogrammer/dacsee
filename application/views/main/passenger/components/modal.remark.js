import React, { Component } from 'react'
import { Text, View, TouchableOpacity, KeyboardAvoidingView, Image, Modal, TextInput } from 'react-native'
import { Screen, DCColor, TextFont } from 'dacsee-utils'
import Resources from 'dacsee-resources'

const { height, width } = Screen.window

export default class RemarkModel extends Component {
  constructor(props) {
    super(props)
    this.state = {
      remark: props.notes,
      remarkLength: 0
    }
  }

  UNSAFE_componentWillReceiveProps(props) {
    if (props.notes !== this.props.notes) {
      this.setState({ remark: props.notes })
    }
  }

  render() {
    let modalHeight = width
    const { visible, strings } = this.props
    const { remarkLength } = this.state
    return (
      <Modal
        animationType='fade'           //渐变
        transparent={true}             // 不透明
        visible={visible}    // 根据isModal决定是否显示
        onRequestClose={() => this.props.onClose()}  // android必须实现 安卓返回键调用
      >
        <View style={{ width: width, height: height, justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgba(57, 56, 67, 0.4)' }}>
          <KeyboardAvoidingView behavior='padding'>
            <View style={{ height: modalHeight, width: modalHeight - 60 }}>
              <Image style={{ height: 80, position: 'absolute', left: 10, top: 0, zIndex: 1 }} source={Resources.image.book_page} />
              <View style={{ height: 40, marginTop: 40, backgroundColor: '#FDC377', borderTopLeftRadius: 20, borderTopRightRadius: 20, width: modalHeight - 60 }} />
              <View style={{ flex: 1, padding: 15, backgroundColor: '#fff', borderBottomLeftRadius: 20, borderBottomRightRadius: 20 }}>
                <View style={{ flex: 1, }}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={{ color: '#000', fontSize: TextFont.TextSize(17), marginBottom: 5 }}>{strings.remarks}</Text>
                    <Text style={{ color: '#000', fontSize: TextFont.TextSize(14), paddingLeft: 10 }}>({remarkLength}/100)</Text>
                  </View>
                  <Text style={{ color: '#ccc', fontSize: TextFont.TextSize(14) }}>{strings.note_trip}</Text>
                  <TextInput defaultValue={$.store.passenger.notes}
                    multiline={true}
                    returnKeyType = {'done'}
                    numberOfLines = {3}
                    maxLength = {100}
                    blurOnSubmit={true}
                    clearTextOnFocus={false}
                    placeholder={strings.remarks_apliction}
                    onChangeText={text => { this.setState({ remark: text, remarkLength: text.length }) }}
                    style={{ backgroundColor: '#f1f1f1', textAlignVertical: 'top', paddingHorizontal: 8, flex: 1, marginTop: 15, borderRadius: 10 }} underlineColorAndro />
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 15, marginHorizontal: 10, justifyContent: 'space-between' }}>
                  <TouchableOpacity onPress={() => this.props.onClose('')}
                    activeOpacity={.7} style={{ width: 100, height: 40, borderRadius: 25, backgroundColor: '#D8D8D8', justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={{ color: '#000', fontSize: TextFont.TextSize(15), fontWeight: '600' }}>{strings.cancel}</Text>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => {
                    this.props.onClose(this.state.remark)
                  }} activeOpacity={.7} style={{ width: 100, height: 40, borderRadius: 25, backgroundColor: DCColor.BGColor('primary-1'), justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={{ color: '#000', fontSize: TextFont.TextSize(15), fontWeight: '600' }}>{strings.confirm}</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </KeyboardAvoidingView>
          <View style={{ height: 60 }} />
        </View>
      </Modal >
    )
  }

}