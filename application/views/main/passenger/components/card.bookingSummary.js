import React from 'react'
import moment from 'moment'
import { Text, View, TouchableOpacity, ActivityIndicator, StyleSheet, Image } from 'react-native'
import { TextFont, DCColor, Avatars } from 'dacsee-utils'
import Resources from 'dacsee-resources'

// Driver Selection Card Component
const DCCampaignCard = ({
  isCommunity,
  avatars,
  selectedFriendData,
  communityName,
  selectedDriverDesc,
  selectDriverFunction
}) => {
  const {
    dcCard,
    dcAvatar,
    dataCont,
    fBody,
    fCaption,
    dcIcon
  } = campaignCardStyles

  const { strings } = $.store.app


  return isCommunity ?
    <TouchableOpacity style={dcCard} onPress={selectDriverFunction}>
      <Image style={dcAvatar} source={{ uri: Avatars.getHeaderPicUrl(avatars) }} />
      <View style={dataCont}>
        <Text ellipsizeMode={'tail'} numberOfLines={1} style={fBody}>{communityName}</Text>
        {selectedDriverDesc ?
          <Text ellipsizeMode={'tail'} numberOfLines={1} style={fCaption}>{selectedDriverDesc}</Text>
          : null
        }
      </View>
      <Image style={dcIcon} source={Resources.image.right_icon} />
    </TouchableOpacity>
    : <TouchableOpacity style={dcCard} onPress={selectDriverFunction}>
      <Image style={dcAvatar} source={selectedFriendData && { uri: Avatars.getHeaderPicUrl(selectedFriendData[0].friend_info.avatars) }} />
      {selectedFriendData && selectedFriendData.length > 1 &&
        <Image style={[dcAvatar, { marginLeft: -35 }]} source={selectedFriendData && { uri: Avatars.getHeaderPicUrl(selectedFriendData[1].friend_info.avatars) }} />
      }
      <View style={dataCont}>
        <Text ellipsizeMode={'tail'} numberOfLines={1} style={fBody}>{strings.selected} {selectedFriendData && selectedFriendData.length} {strings.friends}</Text>
        {selectedDriverDesc ?
          <Text ellipsizeMode={'tail'} numberOfLines={1} style={fCaption}>{selectedDriverDesc}</Text>
          : null
        }
      </View>
      <Image style={dcIcon} source={Resources.image.right_icon} />
    </TouchableOpacity>
}

// Booking Summary Card Component
const DCBookingSummaryCard = ({
  pickupAddress,
  pickupFunction,
  dropoffAddress,
  dropoffFunction,
  isJoylocation,
  bookingDate,
  bookingTime = $.store.app.strings.now,
  bookingTimeFunction,
  promo = $.store.app.strings.promo,
  promoFunction,
  driverNote = $.store.app.strings.note,
  driverNoteFunction,
  bookFunction,
  price = 0
}) => {
  const {
    dcCard,
    addressCont,
    dcCol,
    addressIndicator,
    fBody,
    optionCont,
    optionItem,
    dcIcon,
    captionCont,
    fCaption,
    dcDivider,
    actionCont,
    dcBtn,
    fBtn,
    dcTag
  } = bookingSummaryCardStyles

  const { strings } = $.store.app
  const now = moment(new Date())

  return (
    <View style={dcCard}>
      <View style={addressCont}>
        <TouchableOpacity style={dcCol} onPress={pickupFunction}>
          <View style={addressIndicator} />
          <Text ellipsizeMode={'tail'} numberOfLines={1} style={fBody}>{pickupAddress}</Text>
        </TouchableOpacity>
        <TouchableOpacity style={dcCol} onPress={dropoffFunction}>
          <View style={[addressIndicator, { backgroundColor: DCColor.BGColor('primary-1') }]} />
          <Text ellipsizeMode={'tail'} numberOfLines={1} style={fBody}>{dropoffAddress}</Text>
          {isJoylocation ?
            <Image style={{ width: 20, height: 20 }} source={Resources.image.sponsored_location} />
            : null
          }
        </TouchableOpacity>
      </View>
      <View style={optionCont}>
        <TouchableOpacity style={optionItem} onPress={bookingTimeFunction}>
          <Image style={dcIcon} source={Resources.image.booking_time_icon} />
          <View style={bookingDate && moment(bookingDate) !== now && captionCont}>
            {bookingDate ?
              <Text style={fCaption}>{bookingDate}</Text>
              : null
            }
            {bookingTime ?
              <Text style={fCaption}>{bookingTime}</Text> :
              null
            }
          </View>
        </TouchableOpacity>
        <View style={dcDivider} />
        <TouchableOpacity style={optionItem} onPress={promoFunction}>
          <Image style={dcIcon} source={Resources.image.tag} />
          <View style={bookingDate && moment(bookingDate) !== now && captionCont}>
            <Text style={fCaption}>{promo}</Text>
          </View>
        </TouchableOpacity>
        <View style={dcDivider} />
        <TouchableOpacity style={optionItem} onPress={driverNoteFunction}>
          <Image style={dcIcon} source={Resources.image.driver_note_icon} />
          <View style={bookingDate && moment(bookingDate) !== now && captionCont}>
            <Text style={fCaption}>{driverNote}</Text>
          </View>
        </TouchableOpacity>
      </View>
      <View style={actionCont}>
        <TouchableOpacity style={dcBtn} onPress={bookFunction}>
          <Text style={[fBtn, { paddingLeft: 15, flex: 1 }]}>{strings.start.toUpperCase()}</Text>
          <View style={dcTag}>
            {price !== null && price > -1 ?
              <Text style={[fBtn, { color: 'white' }]}>{price === 0 ? 'FREE' : `RM ${price.toFixed(2)}`}</Text>
              : <ActivityIndicator color={'#fff'} />
            }
          </View>
        </TouchableOpacity>
      </View>
    </View>
  )
}

// Booking Summary Container Component
const DCBookingSummaryCont = ({
  isCommunity,
  selectedFriendData,
  communityAvatars,
  communityName,
  selectedDriverDesc,
  selectDriverFunction,
  pickupAddress,
  pickupFunction,
  dropoffAddress,
  dropoffFunction,
  isJoylocation,
  bookingDate,
  bookingTime,
  bookingTimeFunction,
  promo,
  promoFunction,
  driverNote,
  driverNoteFunction,
  bookFunction,
  price
}) => {
  const {
    mainCont
  } = styles
  return (
    <View style={mainCont}>
      <DCCampaignCard
        isCommunity={isCommunity}
        avatars={communityAvatars}
        selectedFriendData={selectedFriendData}
        communityName={communityName}
        selectedDriverDesc={selectedDriverDesc}
        selectDriverFunction={selectDriverFunction}
      />
      <DCBookingSummaryCard
        pickupAddress={pickupAddress}
        pickupFunction={pickupFunction}
        dropoffAddress={dropoffAddress}
        dropoffFunction={dropoffFunction}
        isJoylocation={isJoylocation}
        bookingDate={bookingDate}
        bookingTime={bookingTime}
        bookingTimeFunction={bookingTimeFunction}
        promo={promo}
        promoFunction={promoFunction}
        driverNote={driverNote}
        driverNoteFunction={driverNoteFunction}
        bookFunction={bookFunction}
        price={price}
      />
    </View>
  )
}

const campaignCardStyles = StyleSheet.create({
  dcCard: {
    width: '100%',
    padding: 7,
    paddingHorizontal: 10,
    backgroundColor: 'white',
    borderRadius: 4,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    shadowOffset: { width: 0, height: -1 },
    shadowColor: 'rgba(0, 0, 0, 0.1)',
    shadowOpacity: 0.05,
    elevation: 2,
    marginBottom: 8,
  },
  avatarCont: {
    position: 'relative',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  dcAvatar: {
    height: 55,
    width: 55,
    borderRadius: 55 / 2,
    resizeMode: 'cover',
    backgroundColor: 'rgba(0, 0, 0, 0.1)'
  },
  dataCont: {
    flex: 1,
    marginLeft: 15,
  },
  fBody: {
    fontSize: TextFont.TextSize(15),
    fontWeight: '700',
    color: DCColor.BGColor('primary-2'),
  },
  fCaption: {
    fontSize: TextFont.TextSize(12),
    fontWeight: '400',
    color: 'rgba(0, 0, 0, 0.45)',
  },
  dcIcon: {
    width: 7,
    height: 7,
    resizeMode: 'contain',
    marginRight: 10,
    tintColor: 'rgba(0, 0, 0, 0.15)'
  }
})

const bookingSummaryCardStyles = StyleSheet.create({
  dcCard: {
    width: '100%',
    paddingHorizontal: 10,
    backgroundColor: 'white',
    borderRadius: 4,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
    shadowOffset: { width: 0, height: -1 },
    shadowColor: 'rgba(0, 0, 0, 0.1)',
    shadowOpacity: 0.05,
    elevation: 2,
  },
  addressCont: {
    width: '100%',
    padding: 7,
    borderBottomWidth: 1,
    borderColor: 'rgba(0, 0, 0, 0.1)'
  },
  dcCol: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    padding: 8,
    paddingVertical: 10,
  },
  addressIndicator: {
    width: 10,
    height: 10,
    borderRadius: 5,
    backgroundColor: DCColor.BGColor('secondary-1'),
    marginRight: 15,
  },
  fBody: {
    fontSize: TextFont.TextSize(15),
    fontWeight: '700',
    color: DCColor.BGColor('primary-2'),
    flex: 1
  },
  optionCont: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  optionItem: {
    flex: 1,
    padding: 17,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  dcIcon: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
    marginBottom: 13,
    tintColor: DCColor.BGColor('primary-2')
  },
  captionCont: {
    minHeight: (TextFont.TextSize(12) * 2) + 8,
    width: '100%',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  fCaption: {
    fontSize: TextFont.TextSize(12),
    fontWeight: '400',
    color: 'rgba(0, 0, 0, 0.5)',
    textAlign: 'center'
  },
  dcDivider: {
    height: 25,
    width: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.1)'
  },
  actionCont: {
    width: '100%',
    paddingBottom: 10,
  },
  dcBtn: {
    width: '100%',
    borderRadius: 4,
    backgroundColor: DCColor.BGColor('primary-1'),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 12,
    height: 55,
    // shadowOffset: { width: 0, height: -1 },
    // shadowColor: 'rgba(0, 0, 0, 0.1)',
    // shadowOpacity: 0.05,
    // elevation: 1,
  },
  fBtn: {
    fontSize: TextFont.TextSize(15),
    fontWeight: '700',
    color: DCColor.BGColor('primary-2'),
    textAlign: 'left'
  },
  dcTag: {
    backgroundColor: 'rgba(0, 0, 0, 0.6)',
    borderRadius: 4,
    paddingHorizontal: 15,
    height: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  }
})

const styles = StyleSheet.create({
  mainCont: {
    width: '100%',
  }
})

export { DCBookingSummaryCont } 