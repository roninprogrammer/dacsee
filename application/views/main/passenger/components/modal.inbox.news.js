import React, { Component } from 'react'
import { Text, View, TouchableOpacity, Image, Modal, StyleSheet } from 'react-native'
import { inject, observer } from 'mobx-react'
import { NavigationActions } from 'react-navigation'
import Material from 'react-native-vector-icons/MaterialCommunityIcons'
import Swiper from 'react-native-swiper';
import { Screen, DCColor, TextFont } from 'dacsee-utils'
import Resources from 'dacsee-resources'

const { height, width } = Screen.window
const styles = StyleSheet.create({
  dcBackdrop: {
    width,
    height,
    backgroundColor: 'transparent',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
    padding: 30,
    borderRadius: 10,
  },
  dcCard: {
    width: '100%',
    height: 520,
    backgroundColor: DCColor.BGColor('primary-1'),
    alignItems: 'center',
    position: 'absolute'
  },
  dcCard_popup: {
    width: '100%',
    height: '86%',
    alignItems: 'center',
    backgroundColor: 'transparent',
  },
  dcImg: {
    width: '100%',
    height: '100%',
    resizeMode: 'cover',
    position: 'absolute',
  },
  titleCont: {
    backgroundColor: DCColor.BGColor('primary-1'),
    width: '100%',
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center'
  },
  fTitle: {
    fontSize: TextFont.TextSize(14),
    fontWeight: 'bold',
    textAlign: 'center'
  },
  bodyCont: {
    width: '100%',
    padding: 10
  },
  fDisplay: {
    fontSize: TextFont.TextSize(14),
    fontWeight: 'bold',
    color: DCColor.BGColor('primary-1')
  },
  reasonCont: {
    backgroundColor: '#fff',
  },
  actionCont: {
    flexDirection: 'row',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10
  },
  dcBtn: {
    flex: 1,
    height: 45
  },
  btnText: {
    fontSize: TextFont.TextSize(18),
    fontWeight: 'bold',
    textAlign: 'center'
  },
  cSub: {
    color: DCColor.BGColor('primary-1')
  },
  closeButton: {
    alignSelf: 'flex-end',
    padding: 10
  },
  button: {
    padding: 5,
    width: '100%',
    justifyContent: 'flex-end',
    paddingLeft: 20,
    paddingRight: 20,
    position: 'absolute',
    bottom: 60,
  },
  checkbox: {
    padding: 5,
    flexDirection: 'row',
    width: '90%',
    backgroundColor: 'rgba(0, 0, 0, 0.60)',
    position: 'absolute',
    bottom: 15,
    justifyContent: 'space-between',
    borderRadius: 7,
    borderWidth: 0.8,
    borderColor: 'white'
  },
  image: {
    marginTop: 10,
    width: 120,
    height: 120,
    resizeMode: 'contain',
    justifyContent: 'center',
  },
  details: {
    alignItems: 'center',
    paddingTop: 15,
    width: '70%'
  },
  detail_text: {
    textAlign: 'center',
    color: DCColor.BGColor('primary-2'),
    marginTop: 10,
  },
  text: {
    fontSize: TextFont.TextSize(16),
    fontWeight: 'bold',
    color: 'black'
  }
})

@inject('app', 'passenger', 'account', 'circle', 'inbox')
@observer
export default class InboxNews extends Component {
  constructor(props) {
    super(props)
    this.state = {
      checked: false,
    }
  }

  componentDidMount() {
    const info = $.store.inbox.inboxModalDetail
    this.setState({ infos: info })
  }

  _onpress(check) {
    if (check === true) {
      $.store.inbox.checkInbox = false
      this.setState({ checked: false })
    }
    else {
      $.store.inbox.checkInbox = true
      this.setState({ checked: true })
    }
  }

  callInboxDetail = (_id, idx) => {
    $.store.inbox.viewModal = false
    global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'InboxDetailScreen', params: { id: _id, index: idx, from: 'popup' } }))
  }

  render() {
    const { visible, onClose } = this.props
    return (
      <Modal
        animationType='fade'           //渐变
        transparent={true}             // 不透明
        visible={visible}    // 根据isModal决定是否显示
        onRequestClose={onClose}  // android必须实现 安卓返回键调用
      >
        <Swiper horizontal={true} showsPagination={false} showsButtons={true} loop={false} dotColor='rgba(255, 255, 255, 0.45)' activeDotColor={DCColor.BGColor('primary-1')} nextButton={<Text style={{ fontSize: 80, paddingRight: 15, color: 'white' }}>›</Text>} prevButton={<Text style={{ fontSize: 80, paddingLeft: 15, color: 'white' }}>‹</Text>}>
          {$.store.inbox.inboxModalDetail.map((i, idx) => (
            <View key={idx} style={styles.dcBackdrop}>
              {i.popupAvatars.length !== 0 ?
                <View style={styles.dcCard}>
                  <Image style={styles.dcImg} source={{ uri: i.popupAvatars[i.popupAvatars.length - 1].url }} />
                  <View style={styles.closeButton}>
                    <TouchableOpacity onPress={onClose}>
                      <View>
                        <Material name={'close-circle'} size={40} color={'white'} />
                      </View>
                    </TouchableOpacity>
                  </View>
                  <View style={[styles.button, {marginBottom: 5}]}>
                    <TouchableOpacity onPress={() => { this.callInboxDetail(i._id, idx) }}
                      style={{
                        backgroundColor: DCColor.BGColor('primary-1'), borderRadius: 5, shadowOffset: { width: 0, height: -1 }, shadowColor: '#000', shadowOpacity: 0.3, elevation: 0.3, borderColor: 'rgba(0, 0, 0, 0.1)'
                      }}
                    >
                      <View style={{ padding: 10, alignItems: 'center' }}>
                        <Text style={styles.text}>{$.store.app.strings.find_out_more}</Text>
                      </View>
                    </TouchableOpacity>
                  </View>
                  <View style={styles.checkbox}>
                    <View style={{ justifyContent: 'center', paddingLeft: 10 }}>
                      <Text style={{ color: 'white' }}>{$.store.app.strings.never_show}</Text>
                    </View>
                    <TouchableOpacity style={{ justifyContent: 'center', paddingRight: 10 }} onPress={() => { this._onpress(this.state.checked) }}>
                      <Material name={this.state.checked === true ? 'checkbox-marked-outline' : 'checkbox-blank-outline'} size={30} color={'white'} />
                    </TouchableOpacity>
                  </View>

                </View>
                :
                <View style={styles.dcCard}>
                  <View style={styles.closeButton}>
                    <TouchableOpacity onPress={onClose}>
                      <View>
                        <Material name={'close-circle'} size={40} color={'white'} />
                      </View>
                    </TouchableOpacity>
                  </View>
                  <Image style={styles.image} source={Resources.image.inbox_placeholder} />
                  <View style={styles.details}>
                    <Text numberOfLines={2} style={[styles.detail_text, { fontSize: TextFont.TextSize(24), fontWeight: 'bold' }]}>{i.title}</Text>
                    <Text numberOfLines={3} style={[styles.detail_text, { fontSize: TextFont.TextSize(16) }]}>{i.description}</Text>
                  </View>
                  <View style={[styles.button, {marginBottom: 5}]}>
                    <TouchableOpacity onPress={() => { this.callInboxDetail(i._id, idx) }} style={{ backgroundColor: 'white', borderRadius: 5, shadowOffset: { width: 0, height: -1 }, shadowColor: '#000', shadowOpacity: 0.3, elevation: 0.3, borderColor: 'rgba(0, 0, 0, 0.1)' }} >
                      <View style={{ padding: 10, alignItems: 'center' }}>
                        <Text style={styles.text}>{$.store.app.strings.find_out_more}</Text>
                      </View>
                    </TouchableOpacity>
                  </View>
                  <View style={styles.checkbox}>
                    <View style={{ justifyContent: 'center', paddingLeft: 10 }}>
                      <Text style={{ color: 'white' }}>{$.store.app.strings.never_show}</Text>
                    </View>
                    <TouchableOpacity style={{ justifyContent: 'center', paddingRight: 10 }} onPress={() => { this._onpress(this.state.checked) }}>
                      <Material name={this.state.checked === true ? 'checkbox-marked-outline' : 'checkbox-blank-outline'} size={30} color={'white'} />
                    </TouchableOpacity>
                  </View>
                </View>
              }
            </View>
          ))}
        </Swiper>
      </Modal>
    )
  }
}