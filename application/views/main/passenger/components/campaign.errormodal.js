import React, { Component } from 'react'
import { Text, View, TouchableOpacity, Modal, StyleSheet, Image, ScrollView } from 'react-native'
import { inject, observer } from 'mobx-react'
import { Screen, TextFont, DCColor } from 'dacsee-utils'
import Resources from 'dacsee-resources'
import moment from 'moment'

const { height, width } = Screen.window
const styles = StyleSheet.create({
  backdropStyle: {
    width,
    height,
    backgroundColor: 'rgba(0, 0, 0, .65)',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20
  },
  mainCont: {
    backgroundColor: 'white',
    borderRadius: 5,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    width: width - 50,
    height: height* '70%',
    padding: 20
  },
  imgCont: {
    padding: 50,
    height: 100,
    width: 100,
  },
  mainTitle: {
    fontWeight: '900',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: TextFont.TextSize(20),
    color: 'black',
    paddingHorizontal: 10,
    paddingVertical: 5
  },
  subsentences: {
    fontWeight: '500',
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    fontSize: TextFont.TextSize(14),
    paddingBottom: 20,
    paddingLeft: 10,
    paddingRight: 10,
    color: 'black'
  },
  textStyle: {
    fontSize: TextFont.TextSize(15),
    color: 'black',
    fontWeight: 'bold',
    justifyContent: 'center',
    alignItems: 'center'
  },
  campaignCont: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderWidth: 0.6,
    borderColor: 'rgba(0, 0, 0, 0.5)',
    padding: 5
  },
  timeText: {
    fontWeight: '400',
    justifyContent: 'center',
    alignItems: 'flex-start',
    fontSize: TextFont.TextSize(12),
    color: 'black',
    paddingLeft: 5
  },
  confirmButton: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 45,
    width: '95%',
    borderRadius: 6,
    backgroundColor: DCColor.BGColor('primary-1'),
    margin: 5,
    marginTop: 50
  },
  date: {
    fontWeight: '400',
    justifyContent: 'center',
    alignItems: 'flex-start',
    fontSize: TextFont.TextSize(12),
    color: 'rgba(0, 0, 0, 0.25)',
    paddingLeft: 5
  },
  dateCont: {
    flexDirection: 'row',
    justifyContent:'flex-end',
    paddingRight: 5
  },
  smallCont: {
    width: '100%', 
    height: (height*0.25), 
  },
  bigCont: {
    flexDirection: 'column',
    justifyContent:'flex-end',
    alignItems: 'flex-end',
    paddingRight: 5,
    flex: 1
  }

})

@inject('app', 'passenger')
@observer
export default class CampaignErrorModalVisible extends Component {
  constructor(props) {
    super(props)
  }
  getDay(day) {
    switch (day) {
      case '1':
        return 'Monday'
      case '2':
        return 'Tuesday'
      case '3':
        return 'Wednesday'
      case '4':
        return 'Thursday'
      case '5':
        return 'Friday'
      case '6':
        return 'Saturday'
      case '7':
        return 'Sunday'
      default:
        return null
    }
  }
  convertTime(utcTime) { 
    const time = ('0' + utcTime).slice(-4);
    return moment.utc(time, "HHmm").utcOffset("+08:00").format("HH:mm");

  }

  render() {
    const { strings } = this.props.app
    const visible = $.store.passenger.campaignErrorModalVisible
    const { avatars, name: campaignName, activePeriods = null } = $.store.passenger.campaign_info || {}
    let obj = activePeriods ? Object.keys(activePeriods) : {}
    const { onConfirm } = $.store.passenger.campaignInfo
    const { backdropStyle, mainCont, imgCont, mainTitle, subsentences, confirmButton, textStyle, campaignCont, timeText,date, dateCont,bigCont, smallCont } = styles

    return (
      <Modal
        animationType='fade'
        transparent={true}
        visible={visible}
        onRequestClose={onConfirm}
      >
        <View style={backdropStyle}>
          <View style={mainCont}>
            <Image style={imgCont} source={$.method.utils.getPicSource(avatars, false, Resources.image.img_placeholder)} />
            <Text style={mainTitle} numberOfLines={1} ellipsizeMode="tail">{campaignName}</Text>
            <Text style={subsentences}>{strings.campaign_error_modal_description}</Text>
            <View style={campaignCont}>
              <Text style={date}>{strings.campaign_error_modal_active_period}</Text>
            </View>
            <View style={smallCont }>  
              <ScrollView style={{width: '100%'}}>
              {obj.length > 0 && obj.map((item,index) => 
                <View key={index} style={campaignCont}>
                  <Text style={[timeText, {color: 'rgba(0, 0, 0, 0.25)'}]}>{this.getDay(item)}</Text>
                  <View style={bigCont}>
                  {activePeriods[item].map((time, idx) =>
                    <View key={idx} style={dateCont}>
                      <Text style={[timeText,{paddingLeft: 0}]}>{this.convertTime(time.start)}</Text> 
                      <Text style={[timeText,{paddingLeft: 0}]}> - </Text>
                      <Text style={[timeText,{paddingLeft: 0}]}>{this.convertTime(time.end)}</Text> 
                    </View>
                  )}
                  </View>
                </View>
              )} 
              </ScrollView>
            </View>
           
            <TouchableOpacity style={confirmButton} onPress={onConfirm}>
              <Text style={textStyle}>{strings.okay.toUpperCase()}</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    )
  }
}