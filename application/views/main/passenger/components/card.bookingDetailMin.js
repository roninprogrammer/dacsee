import React from 'react'
import { Text, View, TouchableOpacity, StyleSheet, Image, Dimensions } from 'react-native'
import { TextFont, DCColor } from 'dacsee-utils'
import Resources from 'dacsee-resources'
import { DriverInfoCont } from 'global-component'
import moment from 'moment'

const { width } = Dimensions.get('window')

// Booking Detail Container Component
const DCBookingDetailCont = ({
  expandFunction,
  avatar,
  avatarFunction,
  name,
  userId,
  callFunction,
  messageFunction,
  rating,
  advTime,
  advDate,
  bookingStatus,
  bookingETA,
  bookingId,
  paymentType,
  price,
  carManufacture,
  carModel,
  carColor,
  carPlateNumber,
  carFrontImg,
  carFrontFunction,
  carBackImg,
  carBackFunction,
  unreadChat
}) => {
  const {
    mainCont,
    expandCont,
    expandBar,
    expandIcon,
    bookingStatusCont,
    fBody,
    fCaption,
    dcCol,
    dcTag,
    vehicleCont,
    vehicleBox,
    vehicleImg
  } = styles
  const { strings } = $.store.app

  return (
    <View style={mainCont}>
      {(expandFunction || bookingStatus) ?
        <TouchableOpacity activeOpacity={1} style={expandCont} onPress={expandFunction}>
          {expandFunction ?
            <View style={expandBar}>
              <Image style={expandIcon} source={Resources.image.expand_icon} />
            </View>
            : null
          }
          {bookingStatus ?
            <View style={bookingStatusCont}>
              {(() => {
                switch (bookingStatus && bookingStatus.toUpperCase()) {
                  case 'PENDING_ASSIGNMENT':
                    return <Text style={[fBody, { color: DCColor.BGColor('primary-1') }]}>{strings.booking_status_pending_assignment}</Text>
                  case 'PENDING_ACCEPTANCE':
                    return <Text style={[fBody, { color: DCColor.BGColor('primary-1') }]}>{strings.friend_waitfor_accept}</Text>
                  case 'CONFIRMED':
                    return <Text style={[fBody, { color: DCColor.BGColor('primary-1') }]}>{strings.booking_status_confirmed}</Text>
                  case 'ON_THE_WAY':
                    return <Text style={[fBody, { color: DCColor.BGColor('primary-1') }]}>{strings.on_the_way}</Text>
                  case 'ARRIVED':
                    return <Text style={[fBody, { color: DCColor.BGColor('primary-1') }]}>{strings.booking_status_arrived}</Text>
                  case 'NO_SHOW':
                    return <Text style={[fBody, { color: DCColor.BGColor('primary-1') }]}>{strings.No_Show}</Text>
                  case 'ON_BOARD':
                    return <Text style={[fBody, { color: DCColor.BGColor('primary-1') }]}>{strings.booking_status_on_board}</Text>
                  case 'COMPLETED_BY_PASSENGER':
                    return <Text style={[fBody, { color: DCColor.BGColor('primary-1') }]}>{strings.booking_status_completed_by_passenger}</Text>
                  case 'FINALIZE_TOTAL_FARE':
                    return <Text style={[fBody, { color: DCColor.BGColor('primary-1') }]}>{strings.booking_status_finalize_total_fare}</Text>
                  case 'COMPLETED':
                    return <Text style={[fBody, { color: DCColor.BGColor('primary-1') }]}>{strings.Completed}</Text>
                  case 'CANCELLED_BY_PASSENGER':
                    return <Text style={[fBody, { color: DCColor.BGColor('primary-1') }]}>{strings.Cancelled_by_Passenger}</Text>
                  case 'CANCELLED_BY_DRIVER':
                    return <Text style={[fBody, { color: DCColor.BGColor('primary-1') }]}>{strings.Cancelled_by_Driver}</Text>
                  case 'CANCELLED_BY_SYSTEM':
                    return <Text style={[fBody, { color: DCColor.BGColor('primary-1') }]}>{strings.Cancelled_by_System}</Text>
                  case 'REJECTED_BY_DRIVER':
                    return <Text style={[fBody, { color: DCColor.BGColor('primary-1') }]}>{strings.Rejected_by_Driver}</Text>
                  case 'NO_DRIVER_AVAILABLE':
                    return <Text style={[fBody, { color: DCColor.BGColor('primary-1') }]}>{strings.booking_status_no_driver_avaiable}</Text>
                  case 'NO_TAKER':
                    return <Text style={[fBody, { color: DCColor.BGColor('primary-1') }]}>{strings.No_Taker}</Text>
                  default:
                    return null
                }
              })()}

              {
                bookingETA ? (
                  bookingETA >= 3600 ? (
                    <Text style={[fBody, { marginLeft: 5, color: 'rgba(255, 255, 255, 0.45)' }]}>ETA ~ {moment.utc(bookingETA*1000).add(1, 'm').format('h[h] m[m]')}</Text>
                  ) : (
                    bookingETA >= 60 ? (
                      <Text style={[fBody, { marginLeft: 5, color: 'rgba(255, 255, 255, 0.45)' }]}>ETA ~ {moment.utc(bookingETA*1000).add(1, 'm').format('m')}{strings.eta_minutes}</Text>
                    ) : (
                      <Text style={[fBody, { marginLeft: 5, color: 'rgba(255, 255, 255, 0.45)' }]}>ETA ~ 1{strings.eta_minutes}</Text>
                    )
                  )
                ) : null
              }
            </View>
            : null
          }
        </TouchableOpacity>
        : null
      }
      {(avatar || avatarFunction || name || userId || callFunction || messageFunction) ?
        <DriverInfoCont
          avatar={avatar}
          avatarFunction={avatarFunction}
          name={name}
          id={userId}
          callFunction={callFunction}
          messageFunction={messageFunction}
          rating={rating}
          unreadChat={unreadChat}
        />
        : null
      }
      {(advDate || advTime) ?
        <View style={[dcCol, { backgroundColor: DCColor.BGColor('red'), height: 50 }]}>
          <Text style={[fBody, { color: 'white', flex: 1, textAlign: 'left' }]}>{strings.advance_booking}</Text>
          <View style={[dcTag, { backgroundColor: 'rgba(0, 0, 0, 0.7)', height: '100%' }]}>
            <Text style={[fCaption, { color: 'white', fontWeight: '900' }]}>{advDate}</Text>
            <Text style={[fCaption, { color: 'white', marginLeft: 5, fontWeight: '900', color: DCColor.BGColor('primary-1') }]}>{advTime}</Text>
          </View>
        </View>
        : null
      }
      {(bookingId || paymentType || price) ?
        <View style={dcCol}>
          <Text style={[fBody, { flex: 1 }]}>{bookingId}</Text>
          <View style={dcTag}>
            <Text style={[fCaption, { fontWeight: '900' }]}>{paymentType}</Text>
          </View>
          <Text style={fBody}>RM{price && price.toFixed(2)}</Text>
        </View>
        : null
      }
      {(carManufacture || carModel || carColor || carPlateNumber) ?
        <View style={vehicleCont}>
          {(carManufacture || carModel || carColor) ?
            <View style={dcCol}>
              <Text style={[fBody, { width: '50%' }]}>{carManufacture} {carModel} ({carColor})</Text>
            </View>
            : null
          }
          {carPlateNumber ?
            <View style={dcCol}>
              <Text style={[fBody, { width: '50%' }]}>{carPlateNumber}</Text>
            </View>
            : null
          }
          <View style={vehicleBox}>
            <TouchableOpacity onPress={carFrontFunction} activeOpacity={1}>
              <Image style={[vehicleImg, { marginRight: 10 }]} source={carFrontImg ? { uri: carFrontImg } : Resources.image.placeholder_car_front} />
            </TouchableOpacity>
            <TouchableOpacity onPress={carBackFunction} activeOpacity={1}>
              <Image style={vehicleImg} source={carBackImg ? { uri: carBackImg } : Resources.image.placeholder_car_back} />
            </TouchableOpacity>
          </View>
        </View>
        : null
      }
    </View>
  )
}

const styles = StyleSheet.create({
  mainCont: {
    width,
    backgroundColor: 'white'
  },
  expandCont: {
    backgroundColor: DCColor.BGColor('primary-2'),
  },
  expandBar: {
    padding: 8,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: "center"
  },
  expandIcon: {
    resizeMode: 'contain',
    tintColor: DCColor.BGColor('primary-1'),
    opacity: .4,
    height: 9,
    width: 35
  },
  bookingStatusCont: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: 10,
  },
  fBody: {
    fontSize: TextFont.TextSize(15),
    fontWeight: '900',
    color: DCColor.BGColor('primary-2')
  },
  fCaption: {
    fontSize: TextFont.TextSize(12),
    fontWeight: '400',
    color: DCColor.BGColor('primary-2')
  },
  dcCol: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderColor: 'rgba(0, 0, 0, 0.1)',
    paddingHorizontal: 25,
    paddingVertical: 10
  },
  dcTag: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: DCColor.BGColor('primary-1'),
    borderRadius: 50,
    padding: 1,
    paddingHorizontal: 8,
    marginRight: 10
  },
  vehicleCont: {
    position: 'relative'
  },
  vehicleBox: {
    paddingRight: 25,
    right: 0,
    position: 'absolute',
    height: '100%',
    width: '50%',
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  vehicleImg: {
    height: (TextFont.TextSize(15) * 2) + 30,
    width: (TextFont.TextSize(15) * 2) + 30,
    borderRadius: ((TextFont.TextSize(15) * 2) + 30) / 2,
    backgroundColor: '#f1f1f1'
  }
})

export { DCBookingDetailCont } 