import React, { Component } from 'react'
import { Text, View, TouchableOpacity, KeyboardAvoidingView, Image, Modal, StyleSheet, Alert } from 'react-native'
import { inject, observer } from 'mobx-react'
import { Screen, DCColor, TextFont } from 'dacsee-utils'
import Resources from 'dacsee-resources'
import RadioButton from '../../../../components/radiobutton'

const { height, width } = Screen.window
const styles = StyleSheet.create({
  dcBackdrop: {
    width,
    height,
    backgroundColor: 'rgba(0, 0, 0, 0.75)',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
    padding: 20
  },
  dcCard: {
    width: '100%',
    backgroundColor: '#ffffff',
    alignItems: 'center',
    borderRadius: 10,
    position: 'absolute'
  },
  dcImg: {
    width: '100%',
    height: 100,
    resizeMode: 'cover',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  titleCont: {
    backgroundColor: DCColor.BGColor('primary-1'),
    width: '100%',
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center'
  },
  fTitle: {
    fontSize: TextFont.TextSize(14),
    fontWeight: 'bold',
    textAlign: 'center'
  },
  bodyCont: {
    width: '100%',
    padding: 10
  },
  fDisplay: {
    fontSize: TextFont.TextSize(14),
    fontWeight: 'bold',
    color: DCColor.BGColor('primary-1')
  },
  reasonCont: {
    backgroundColor: '#fff',
  },
  actionCont: {
    flexDirection: 'row',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10
  },
  dcBtn: {
    flex: 1,
    height: 45
  },
  btnText: {
    fontSize: TextFont.TextSize(18),
    fontWeight: 'bold',
    textAlign: 'center'
  },
  cSub: {
    color: DCColor.BGColor('primary-1')
  }
})

@inject('app', 'passenger', 'account', 'circle')
@observer
export default class BlockDriver extends Component {
  constructor(props) {
    super(props)
    this.state = {
      info: {},
      uri: '',
      checked: null,
      text: '',
      reason: {
        blockReason_id: '',
        action: 'block',
        user_id: ''
      }
    }
  }

  handleSelect = (key) => {
    if (key == 5) {
      this.setState({
        checked: key,
        reason: {
          ...this.state.reason,
          blockReason_id: this.props.passenger.blockReasons[key]._id
        }
      })
    }
    else {
      this.setState({
        checked: key,
        text: '',
        reason: {
          ...this.state.reason,
          blockReason_id: this.props.passenger.blockReasons[key]._id
        }
      })
    }

  }

  onChange = (text) => {
    this.setState({
      text
    })
  }

  render() {
    const {
      dcBackdrop,
      dcCard,
      dcImg,
      titleCont,
      fTitle,
      bodyCont,
      fDisplay,
      reasonCont,
      actionCont,
      dcBtn,
      btnText,
      cSub
    } = styles
    const { visible, onClose, confirm } = this.props
    
    return (
      <Modal
        animationType='fade'           //渐变
        transparent={true}             // 不透明
        visible={visible}    // 根据isModal决定是否显示
        onRequestClose={onClose}  // android必须实现 安卓返回键调用
      >
        <View style={dcBackdrop}>
          <KeyboardAvoidingView behavior='position' contentContainerStyle={{ width: '100%', height: '100%' }} style={{ width: '100%', height: '100%', flexDirection: 'row', alignItems: 'center' }}>
            <View style={dcCard}>
              <Image style={dcImg} source={Resources.image.popup_logo} />
              <View style={titleCont}>
                <Text style={fTitle}>{$.store.app.strings.BLOCKDRIVER}</Text>
              </View>
              <View style={bodyCont}>
                <Text style={fTitle}>{$.store.app.strings.confirm_block_driver}</Text>
              </View>
              <View style={bodyCont}>
                <Text style={fDisplay}>{$.store.app.strings.reason_to_block}</Text>
              </View>
              <View style={reasonCont}>
                <RadioButton
                  data={this.props.passenger.blockReasons.slice()}
                  checked={this.state.checked}
                  text={this.state.text}
                  //onSelect={(id) => this.onSelect(id)}
                  onChange={(text) => this.onChange(text)}
                  handleSelect={(key) => this.handleSelect(key)}
                />
              </View>
              <View style={actionCont}>
                <TouchableOpacity style={dcBtn} onPress={onClose}>
                  <Text style={btnText}>{$.store.app.strings.cancel}</Text>
                </TouchableOpacity>
                <TouchableOpacity style={dcBtn} onPress={() => this.state.checked == null ? Alert.alert($.store.app.strings.select_block_reason) : confirm(this.state.reason.blockReason_id, this.state.text, this.state.checked)}>
                  <Text style={[btnText, cSub]}>{$.store.app.strings.confirm}</Text>
                </TouchableOpacity>
              </View>
            </View>
          </KeyboardAvoidingView>
        </View>
      </Modal>
    )
  }
}