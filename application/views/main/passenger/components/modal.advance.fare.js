import React, { Component } from 'react'
import { Text, View, TouchableOpacity, KeyboardAvoidingView, Modal, StyleSheet, Image } from 'react-native'
import { inject, observer } from 'mobx-react'
import { Screen, TextFont, DCColor } from 'dacsee-utils'
import Resources from 'dacsee-resources'

const { height, width } = Screen.window

@inject('app', 'passenger')
@observer
export default class AdvanceFareModalVisible extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    const {
      dcBackDrop,
      keyboardAvaoidingView,
      dcCard,
      dcImg,
      fTitle,
      fCaption,
      actionCont,
      dcBtn,
      activeBtn,
      btnText,
      activeText
    } = styles
    const { passenger } = this.props
    const visible = passenger.advanceFareModalVisible
    const { title = '', desc_1 = '', desc_2 = '', minFare = 0, onConfirm, confirmLabel = '', onCancel, cancelLabel = '' } = passenger.advanceFareInfo

    return (
      <Modal
        animationType='fade'
        transparent={true}
        visible={visible}
        onRequestClose={() => {
          passenger.advanceFareModalVisible = false
        }}
      >
        <View style={dcBackDrop}>
          <KeyboardAvoidingView behavior='position' contentContainerStyle={keyboardAvaoidingView} style={keyboardAvaoidingView}>
            <View style={dcCard}>
              <Image style={dcImg} source={Resources.image.attention_icon} />
              <Text style={fTitle}>{title}</Text>
              <Text style={[fCaption, { marginBottom: 15 }]}>{`${desc_1}${minFare}${desc_2}`}</Text>
              <View style={actionCont}>
                <TouchableOpacity style={dcBtn} onPress={onCancel}>
                  <Text style={btnText}>{cancelLabel}</Text>
                </TouchableOpacity>
                <TouchableOpacity style={[dcBtn, activeBtn]} onPress={onConfirm}>
                  <Text style={[btnText, activeText]}>{confirmLabel}</Text>
                </TouchableOpacity>
              </View>
            </View>
          </KeyboardAvoidingView>
        </View>
      </Modal >
    )
  }
}

const styles = StyleSheet.create({
  dcBackDrop: {
    width,
    height,
    backgroundColor: 'rgba(0, 0, 0, 0.65)',
    justifyContent: 'center',
    alignItems: 'center'
  },
  keyboardAvaoidingView: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  dcCard: {
    width: '90%',
    backgroundColor: 'white',
    padding: 20,
    borderRadius: 8,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: 'rgba(0, 0, 0, 0.15)',
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowRadius: 3,
    shadowOpacity: 1
  },
  scrollView: {
    height: 300,
    width: '100%',
  },
  dcImg: {
    height: width / 3.5,
    width: width / 3.5,
    resizeMode: 'contain',
    marginBottom: 10,
    marginTop: 20
  },
  hasImg: {
    width: '100%'
  },
  fTitle: {
    textAlign: 'center',
    fontSize: TextFont.TextSize(16),
    fontWeight: '800',
    color: 'rgba(0, 0, 0, 0.75)',
    marginBottom: 5,
  },
  fSubtitle: {
    textAlign: 'center',
    fontSize: TextFont.TextSize(14),
    fontWeight: '800',
    color: 'rgba(0, 0, 0, 0.45)',
    marginBottom: 10,
  },
  fCaption: {
    textAlign: 'center',
    fontSize: TextFont.TextSize(14),
    fontWeight: '600',
    color: 'rgba(0, 0, 0, 0.45)',
    marginBottom: 50,
  },
  actionCont: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  dcBtn: {
    borderRadius: 6,
    flex: 2,
    height: 45,
    backgroundColor: 'rgba(0, 0, 0, 0.05)',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  activeBtn: {
    flex: 3,
    backgroundColor: DCColor.BGColor('primary-1'),
    marginLeft: 10
  },
  btnText: {
    fontSize: TextFont.TextSize(16),
    fontWeight: '700',
    color: 'rgba(0, 0, 0, 0.45)',
    textAlign: 'center'
  },
  activeText: {
    color: 'rgba(0, 0, 0, 0.75)'
  }
})


