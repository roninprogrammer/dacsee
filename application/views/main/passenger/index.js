/* global navigator */

import React, { Component } from 'react'
import {
  Text,
  View,
  Animated,
  TouchableOpacity,
  ActivityIndicator,
  Linking,
  StyleSheet,
  Image,
  Platform,
  ImageBackground
} from 'react-native'
import {
  NavigationActions,
  withNavigationFocus
} from 'react-navigation'
import {
  observable,
  toJS
} from 'mobx'
import {
  observer,
  inject
} from 'mobx-react'
import Lottie from 'lottie-react-native'
import moment from 'moment'
import BackgroundGeoLocation from 'react-native-background-geolocation'
import Resources from 'dacsee-resources'
import SponsorInfo from '../sponsorLocation/component/Sponsor.info'
import { MapView } from '../../../components/map'
import {
  Screen,
  Icons,
  TextFont,
  Avatars,
  DCColor
} from 'dacsee-utils'
import TimePicker from '../../../components/timePicker'
import PopupService from '../../../native/popup-service'
import {
  DCBookingDetailModal,
  DCBookingOptionModal,
  DCInfoModal,
  DriverFoundModal,
  SosModal
} from '../../modal'
import {
  PromoModel,
  PromoDetailModel,
  FareAlertModel
} from '../promo/component'
import {
  AdvanceFareModalVisible,
  ImageModal,
  InboxNews,
  CampaignErrorModalVisible,
  DCBookingSummaryCont,
  DCBookingDetailCont
} from './components'

const { height, width } = Screen.window
const STATUS_DEFINE = {
  ERROR: -10,
  NO_TAKER: -1,
  PICK_ADDRESS: 0,
  PASSENGER_CONFIRM: 1,
  WAIT_RESPONSE: 2,
  WAIT_DRIVER_CONFRIM: 3,
  DRIVER_CONFRIM: 4,
  ON_THE_WAY: 5,
  DRIVER_ARRIVED: 6,
  WAIT_PASSENGER: 7,
  ON_BOARD: 8,
  ON_RATING: 9,
  CANCEL_BY_DRIVER: 10,
  FINALIZE_TOTAL_FARE: 11,
  COMPLETED_BY_PASSENGER: 12,
  SUCCESS: 13,
  CONFIRMED: 14
}

@inject('app', 'passenger', 'account', 'circle', 'joy', 'chat')
@observer
class PassengerComponent extends Component {
  constructor(props) {
    super(props)
    this.state = {
      showSponsoredModal: !this.props.passenger.joy_location_id,
      inboxModal: false,
      haveRemarks: false,
      showPromoModal: false,
      showSearchDetail: false,
      showNote: false,
      remark: ''
    }
    this.radar = new Animated.Value(0)
    this.getCurrentLoc = false // Get current location status flag
  }
  @observable selectPayVisible: Boolean = false
  @observable timePickerVisible: Boolean = false
  @observable remarkVisible: Boolean = false
  @observable imageModalVisible: Boolean = false
  @observable selectCarVisible: Boolean = false
  @observable sosVisible: Boolean = false
  @observable activeImage: String = ''
  @observable getCurrentLoc: Boolean = false
  @observable panelVisible: Boolean = false
  @observable noteVisible: Boolean = false
  @observable driverVisible: Boolean = false
  @observable detailVisible: Boolean = false
  @observable cancelModal: Boolean = false

  async componentDidMount() {
    // await InteractionManager.runAfterInteractions()
    Animated.loop(Animated.timing(this.radar, { toValue: 1, fromValue: 0, duration: 2500 })).start()

    if (Platform.OS === 'android') PopupService.reactUILoaded()
  }

  componentDidUpdate(prevProps) {
    const { setMapToTrackDriver = true, initialRegionLoaded = false } = this.state;
    const { passenger: { driverLocation: { coords: currentCoords = {} }, status } } = this.props
    const { passenger: { driverLocation: { coords: prevCoords = {} } } } = prevProps

    Animated.loop(Animated.timing(this.radar, { toValue: 1, fromValue: 0, duration: 3000 })).start()

    if (
      (currentCoords.latitude !== prevCoords.longitude || currentCoords.longitude !== prevCoords.longitude)
      && setMapToTrackDriver
      && initialRegionLoaded
      && this.props.passenger.mapRefs
      && status >= STATUS_DEFINE.ON_THE_WAY
    ) {
      this.props.passenger.mapRefs.handle.moveTo(currentCoords)
    }

    if (status < STATUS_DEFINE.DRIVER_CONFRIM || status > STATUS_DEFINE.ON_BOARD) {
      this.driverVisible = false
      this.detailVisible = false
      $.store.passenger.driverFoundModal = false
    }
  }

  _getActiveBookingStatuses() {
    return [
      STATUS_DEFINE.ON_THE_WAY,
      STATUS_DEFINE.DRIVER_ARRIVED,
      STATUS_DEFINE.ON_BOARD,
      STATUS_DEFINE.FINALIZE_TOTAL_FARE
    ]
  }

  getCurrentLocation() {
    // Get Current Position
    $.store.app.showLoading($.store.app.strings.get_location)
    this.getCurrentLoc = true
    BackgroundGeoLocation.getCurrentPosition({
      timeout: 10,
      maximumAge: 30000
    }).then((location) => {
      BackgroundGeoLocation.stop(() => {
        console.log('BackgroundGeoLocation is stopped')
      })

      const { coords } = location
      this.props.app.coords = coords
      this.moveCurrentLocation()

      this.getCurrentLoc = false
      $.store.app.hideLoading()
    }).catch((error) => {
      this.moveCurrentLocation()

      this.getCurrentLoc = false
      this.locationError(error)
      $.store.app.hideLoading()
    })
  }

  locationError(error) {
    let errorMsg = ''
    switch (error) {
      case 0:
        errorMsg = 'Location unknown'
        break
      case 1:
        errorMsg = 'Location permission denied'
        break
      case 2:
        errorMsg = 'Network error'
        break
      case 408:
        errorMsg = 'Location timeout'
        break
      default:
        errorMsg = 'Something is wrong, please try again...'
        break
    }
    $.define.func.showMessage(errorMsg)
  }

  moveCurrentLocation() {
    const { app, passenger } = this.props

    if (passenger.mapRefs && passenger.mapRefs.handle) {
      passenger.mapRefs.handle.moveTo(app.coords)
    }
  }

  onPressFare() {
    const { passenger } = this.props
    const { fare, fareLoading, from } = passenger
    if (fare > -1 && !fareLoading) {
      passenger.mapRefs.handle.moveTo(from.coords)
      this.props.passenger.booking()
      this.props.joy.getSponsorLocationList(options = {
        skip: 0,
        limit: 1
      })
    } else if (fare < -1 && !fareLoading) {
      passenger.mapRefs.handle.moveTo(from.coords)
      this.props.passenger.getFare()
    }
  }

  getMarkers() {
    let markers = []
    const { mapReady } = this.state
    const { passenger, circle } = this.props
    const passengerHasActiveBooking = this._getActiveBookingStatuses().includes(passenger.status)

    if (!mapReady) return markers;

    if (Array.isArray(toJS(circle.circle_points)) && circle.circle_points.length && !passengerHasActiveBooking) {
      markers = toJS(circle.circle_points)
    } else if (passengerHasActiveBooking && passenger.driverLocation && passenger.driverLocation.coords) {
      markers = [passenger.driverLocation]
    }
    return markers;
  }

  onMapLayout() {
    if ((typeof this.props.isFocused === 'function' ? !this.props.isFocused() : !this.props.isFocused) || $.store.app.control.tab.name !== 'MainTab') return null

    this.props.passenger.setEventDisable(false)
    if (this.props.passenger.from === null) {
      this.moveCurrentLocation()
    } else {
      if (this.props.passenger.destination === null) {
        this.props.passenger.mapRefs.handle.moveTo(this.props.passenger.from.coords)
      } else {
        this.props.passenger.setEventDisable(true)
        this.props.passenger.mapRefs.handle.moveTo($.method.utils.getRegion([this.props.passenger.from.coords, this.props.passenger.destination.coords]))
      }
    }

    this.setState({ mapReady: true })
    $.store.passenger.enableChatButton(!!$.store.passenger.joy_location_id)
  }

  onShow = (modal) => {
    this.setState({
      [modal]: true
    })
  }

  onHide = (modal) => {
    this.setState({
      [modal]: false,
    })
  }

  onHideNote = async () => {
    this.noteVisible = false
  }

  onHideDriverModal = () => {
    this.driverVisible = false
  }

  onHideDetailModal = () => {
    this.detailVisible = false
  }

  onPressRejectBtn = (confirmed) => {
    const { passenger } = this.props

    if (passenger.status >= STATUS_DEFINE.DRIVER_CONFRIM && passenger.status <= STATUS_DEFINE.WAIT_PASSENGER) {
      if (confirmed) {
        passenger.cancel()
        this.cancelModal = false
      } else {
        this.detailVisible = false
        this.cancelModal = true
      }
    } else if (passenger.status === STATUS_DEFINE.ON_BOARD) {
      this.detailVisible = false
      this.sosVisible = true
    } else {
      return null
    }
  }

  render() {
    const { app, account, circle, passenger, chat } = this.props
    const { totalUnread } = chat
    const { from, destination, event_disable, status, moveMapFlag, fare, fareLoading, vehicle_info, group_info, campaign_info } = passenger
    const { setMapToTrackDriver = true, initialRegionLoaded = false, showSearchDetail } = this.state
    const { sosBtnCont, dcBtn, fBtn, recenterBtn, searchDriverCont, expandBar, expandIcon, bookingStatusCont, fBody } = styles
    const searchingDriver = status === STATUS_DEFINE.WAIT_RESPONSE || status === STATUS_DEFINE.WAIT_DRIVER_CONFRIM

    if (moveMapFlag) {
      this.moveCurrentLocation()
      passenger.changeMoveFlag(false)
    }

    let extraMapProps = {
      onLayout: () => this.onMapLayout()
    }

    if (app.coords.latitude !== 0 && app.coords.longitude !== 0 && !initialRegionLoaded) {
      extraMapProps = {
        ...extraMapProps,
        initialRegion: {
          ...app.coords,
          latitudeDelta: 0.014,
          longitudeDelta: 0.014 * (width / height)
        }
      }
    }

    return app.googleService ? (
      <View style={{ flex: 1, backgroundColor: 'white', width }}>
        <InboxNews visible={this.state.inboxModal} onClose={() => this.onHide('inboxModal')} />
        <View pointerEvents={searchingDriver ? "none" : null} style={{ width, flex: 1 }}>
          <MapView
            onRegionChangeComplete={event_disable ?
              () => { }
              : (coords) => {
                const { longitude, latitude } = coords

                if ((typeof this.props.isFocused === 'function' ? !this.props.isFocused() : !this.props.isFocused) || $.store.app.control.tab.name !== 'MainTab') return null

                if (latitude >= 0.85 && latitude <= 0.80) return null

                if (from !== null && $.method.utils.distance_v2({ lat: from.coords.latitude, lng: from.coords.longitude }, { lat: latitude, lng: longitude }) * 1000 <= 20) return null

                if (status === STATUS_DEFINE.PICK_ADDRESS) passenger.setAddressCoords({ longitude, latitude })

                if (!this.state.initialRegionLoaded) this.setState({ initialRegionLoaded: true })
              }
            }
            onMoveShouldSetResponder={(e) => {
              setTimeout(() => { this.setState({ setMapToTrackDriver: false }) }, 750)
            }}
            routes={passenger.destination_directions.routes}
            from={from}
            destination={destination}
            markers={searchingDriver ? null : this.getMarkers()}
            showNavigator={false}
            region_type={app.region}
            style={{ flex: 1, height, width }}
            ref={e => { passenger.mapRefs = e }}
            {...extraMapProps}
          />
        </View>

        {(status === STATUS_DEFINE.PICK_ADDRESS || searchingDriver) && (
          <View style={{ position: 'absolute', opacity: 0.7, top: searchingDriver ? (($.define.screen.content_height - (width * .75)) / 2) - 15: ($.define.screen.content_height - 68) / 2, left: searchingDriver ? (width - (width * .75)) / 2 : (width - 68) / 2 }}>
            <Lottie style={searchingDriver ? { width: width * .75, height: width * .75 } : { width: 68, height: 68 }} progress={this.radar} source={require('../../../res/animation/radar.json')} />
          </View>
        )
        }

        {(status === STATUS_DEFINE.PICK_ADDRESS || searchingDriver) && (
          <View style={{ position: 'absolute', top: ($.define.screen.content_height) / 2 - 52, left: (width - 46) / 2 }}>
            <View style={{ shadowOffset: { width: 0, height: 1 }, shadowColor: '#000', shadowOpacity: 0.15 }}>
              <ImageBackground style={[
                { width: 46, height: 52, alignItems: 'center' }]} source={require('../../../res/images/pick-pin.png')} >
                {!account.user.avatars ?
                  null :
                  <Image style={[
                    { width: 40, height: 40, borderRadius: 20, top: 3 }
                  ]} source={{ uri: Avatars.getHeaderPicUrl(account.user.avatars) }} />
                }
              </ImageBackground>
            </View>
          </View>
        )
        }

        {status === STATUS_DEFINE.PICK_ADDRESS && (
          <View style={{ position: 'absolute', bottom: 6, left: 0, right: 0, paddingHorizontal: 15 }}>
            <View style={{ marginBottom: 6, alignItems: 'flex-end' }}>
              <TouchableOpacity onPress={this.getCurrentLocation.bind(this)} activeOpacity={0.7} style={[
                { justifyContent: 'center', alignItems: 'center', backgroundColor: this.getCurrentLoc ? '#1F72FE' : 'white' },
                { borderRadius: 7, width: 32, height: 32 },
                { shadowOffset: { width: 0, height: 1 }, shadowColor: '#000', shadowOpacity: 0.15 }
              ]}>
                {Icons.Generator.Awesome('map-marker', 20, this.getCurrentLoc ? 'white' : '#999')}
              </TouchableOpacity>
            </View>

            <View style={{ shadowOffset: { width: 0, height: 1 }, shadowColor: '#000', shadowOpacity: 0.15 }}>
              <View style={{ backgroundColor: 'white', borderRadius: 14, flexDirection: 'row', paddingHorizontal: 22, paddingVertical: 6 }}>
                <View style={{ width: 28 }}>
                  <View style={{ justifyContent: 'center', height: 44, marginBottom: 10 }}>
                    {Icons.Generator.Awesome('circle', 14, DCColor.BGColor('secondary-1'))}
                  </View>
                  <View style={{ justifyContent: 'center', height: 44 }}>
                    {Icons.Generator.Awesome('circle', 14, DCColor.BGColor('primary-1'))}
                  </View>
                </View>
                <View style={{ flex: 1 }}>
                  <TouchableOpacity
                    activeOpacity={0.7}
                    style={{ height: 44, marginBottom: 10, justifyContent: 'center' }}
                    onPress={() => this.props.navigation.navigate('PickerAddressModal', {
                      type: 'FromAddress',
                      onPickAddress: (item) => {
                        passenger.getAddressFromPlaces(item)
                      }
                    })}
                  >
                    {from === null && (<Text numberOfLines={1} lineBreakMode={'middle'} style={{ color: '#000231' }}>{app.strings.get_location}</Text>)}
                    {from !== null && (<Text numberOfLines={1} lineBreakMode={'middle'} style={{ color: '#000231' }}>{from.name}</Text>)}
                  </TouchableOpacity>

                  <TouchableOpacity
                    activeOpacity={0.7}
                    style={{ height: 44, justifyContent: 'center' }}
                    onPress={() => {
                      this.props.navigation.navigate('PickerAddressModal', {
                        type: 'DestinationAddress',
                        onPickAddress: (item) => {
                          if (item.joy) {
                            passenger.setJoyLocation(item)
                          } else {
                            passenger.setDestinationAddress(item)
                          }
                        }
                      }), this.setState({ haveRemarks: false }), $.store.passenger.notes = ''
                    }}
                  >
                    {destination === null && (<Text numberOfLines={1} lineBreakMode={'middle'} style={{ color: '#a3a3ac' }}>{app.strings.destination}</Text>)}
                    {destination !== null && (<Text numberOfLines={1} lineBreakMode={'middle'} style={{ color: '#000231' }}>{destination.name}</Text>)}
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>
        )
        }

        {status === STATUS_DEFINE.PASSENGER_CONFIRM && (
          <View style={{ position: 'absolute', bottom: 6, left: 0, right: 0, paddingHorizontal: 15 }}>
            <DCBookingSummaryCont
              isCommunity={circle.groupData.select_group._id}
              selectedFriendData={circle.friendData.select_friends && circle.friendData.select_friends.length === 0 ? null : circle.friendData.select_friends}
              communityAvatars={circle.groupData && circle.groupData.select_group && circle.groupData.select_group.avatars}
              communityName={circle.groupData.select_group.name}
              selectedDriverDesc={circle.groupData.select_group.tagline ? circle.groupData.select_group.tagline : circle.groupData.select_group.campaign_info && circle.groupData.select_group.campaign_info.description}
              selectDriverFunction={() => global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'CircleSelect' }))}
              pickupAddress={from ? from.name : app.strings.get_location}
              pickupFunction={() => this.props.navigation.navigate('PickerAddressModal', {
                type: 'FromAddress',
                onPickAddress: (item) => {
                  passenger.getAddressFromPlaces(item)
                }
              })}
              dropoffAddress={destination ? destination.name : app.strings.destination}
              dropoffFunction={() => this.props.navigation.navigate('PickerAddressModal', {
                type: 'DestinationAddress',
                onPickAddress: (item) => {
                  if (item.joy) {
                    passenger.setJoyLocation(item)
                  } else {
                    passenger.setDestinationAddress(item)
                  }
                }
              })}
              isJoylocation={passenger.joy_location_id}
              bookingDate={passenger.type === 'now' ? $.store.app.strings.now : moment(new Date(passenger.booking_at)).format('DD MMM YY')}
              bookingTime={passenger.type === 'now' ? null : moment(new Date(passenger.booking_at)).format('hh:mm A')}
              bookingTimeFunction={() => this.timePickerVisible = true}
              promo={!!$.store.passenger.promo_code ? passenger.promo_code : app.strings.promo}
              promoFunction={() => !!$.store.passenger.promo_code ? passenger.showPromo() : passenger.enterPromo()}
              bookFunction={(!circle.groupData.select_group._id && circle.friendData.select_friends.length === 0) ? null : () => this.onPressFare()}
              price={fareLoading ? null : passenger.fare > -1 ? passenger.fare : null}
              driverNoteFunction={() => this.noteVisible = true}
            />

            <DCBookingOptionModal
              onHide={() => this.onHideNote()}
              confirmFunction={() => this.onHideNote()}
              visible={this.noteVisible}
              driverNoteDefaultValue={passenger.notes}
              onChangeDriverNote={(text) => passenger.notes = text}
              backdropHide
            />

            <TimePicker
              visible={$.store.circle.advanceOptions || this.timePickerVisible}
              strings={app.strings}
              time={passenger.type === 'now' ? 'now' : passenger.booking_at}
              dateChange={(type, time) => {
                if (type === 'now' && $.store.circle.isAdvanceCampaign === true) {
                  $.store.circle.setDefaultGroup()
                  global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'CircleSelect' }))
                }
                this.timePickerVisible = false
                $.store.circle.advanceOptions = false
                passenger.type = type
                passenger.booking_at = time
                if ($.store.circle.friendData.select_friends.length > 0 ||
                  $.store.circle.groupData.select_group.hasOwnProperty('_id')) $.store.passenger.getFare(passenger.booking_at)
              }}
            />

          </View>
        )
        }
        {searchingDriver &&
          <View>
            <TouchableOpacity style={searchDriverCont} activeOpacity={1} onPress={() => this.onShow('showSearchDetail')}>
              <View style={expandBar}>
                <Image style={expandIcon} source={Resources.image.expand_icon} />
              </View>
              <View style={bookingStatusCont}>
                <Text style={fBody}>{app.strings.confirm_wait_order}</Text>
              </View>
            </TouchableOpacity>

            <DCBookingDetailModal
              onHide={() => this.onHide('showSearchDetail')}
              visible={showSearchDetail}
              unMountFunction={() => this.onHide('showSearchDetail')}
              bookingStatus={passenger.booking_status}
              // bookingId={passenger.booking_code}
              pickupAddress={passenger.from.address}
              dropoffAddress={passenger.destination.address}
              campaignName={passenger.groupName}
              campaignImg={group_info ? group_info.avatars ? group_info.avatars : campaign_info && campaign_info.avatars : null}
              driverNote={passenger.notes !== '' ? passenger.notes : app.strings.note_to_driver}
              price={passenger.fare}
              paymentType={passenger.payment_method}
              rejectFunction={() => {
                this.onHide('showSearchDetail')
                passenger.cancel()
              }
              }
              rejectBtnLabel={app.strings.cancel_trip}
            />
          </View>
        }

        {
          status >= STATUS_DEFINE.ON_THE_WAY && !passenger.joy_location_id && (
            <View style={{ width, padding: 10, backgroundColor: 'transparent', position: 'absolute', top: 0 }}>
              {this.props.joy.sponsorLocationList.length > 0 && (
                <SponsorInfo
                  icon={Resources.image.cash_back}
                  label={this.props.app.strings.sponsored_bar_desc}
                  onPress={() => {
                    this.props.navigation.navigate('SponsorLocationScreen')
                  }}
                  contStyle={{ backgroundColor: DCColor.BGColor('primary-1'), shadowOpacity: 0.5, }}
                  imgContStyle={{ backgroundColor: '#606060' }}
                  textStyle={{ fontWeight: '700', color: 'rgba(0, 0, 0, 0.65)' }}
                />
              )}
            </View>
          )
        }

        {
          status >= STATUS_DEFINE.ON_THE_WAY && (
            <View style={{ width }}>
              {!setMapToTrackDriver ?
                <TouchableOpacity style={recenterBtn} onPress={() => {
                  this.moveCurrentLocation()
                  this.setState({ setMapToTrackDriver: true })
                }}>
                  {Icons.Generator.Awesome('map-marker', 20, '#999')}
                </TouchableOpacity>
                : null
              }
              <DCBookingDetailCont
                bookingStatus={passenger.booking_status}
                bookingETA={status === STATUS_DEFINE.ON_THE_WAY ? passenger.driverETA : null}
                bookingId={status === STATUS_DEFINE.ON_BOARD ? null : passenger.booking_code}
                avatar={passenger.driver_info && passenger.driver_info.avatars}
                avatarFunction={() => this.driverVisible = true}
                name={passenger.driver_info && passenger.driver_info.fullName}
                userId={passenger.driver_info ? passenger.driver_info.userId : null}
                rating={passenger.driver_info && passenger.driver_info.rating && passenger.driver_info.rating.toFixed()}
                callFunction={() => {
                  const { phoneNo, phoneCountryCode } = passenger.driver_info
                  Linking.openURL(`tel:${phoneCountryCode}${phoneNo}`)
                }}
                messageFunction={() => {
                  this.props.navigation.navigate('ChatWindow', {
                    friendData: {
                      friend_info: Object.assign({}, { _id: passenger.driver_id }, passenger.driver_info),
                      friend_id: passenger.driver_id
                    },
                    chatType: 'friend'
                  })
                }}
                unreadChat={totalUnread}
                paymentType={status === STATUS_DEFINE.ON_BOARD ? null : passenger.payment_method}
                price={status === STATUS_DEFINE.ON_BOARD ? null : passenger.fare}
                carManufacture={status === STATUS_DEFINE.ON_BOARD ? null : vehicle_info && vehicle_info.manufacturer}
                carModel={status === STATUS_DEFINE.ON_BOARD ? null : vehicle_info && vehicle_info.model}
                carColor={status === STATUS_DEFINE.ON_BOARD ? null : vehicle_info && vehicle_info.color}
                carPlateNumber={status === STATUS_DEFINE.ON_BOARD ? null : vehicle_info && vehicle_info && vehicle_info.registrationNo}
                carFrontImg={status >= STATUS_DEFINE.ON_THE_WAY ? vehicle_info && vehicle_info.frontPhoto && vehicle_info.frontPhoto.urlSmall : null}
                carFrontFunction={status < STATUS_DEFINE.ON_THE_WAY || !vehicle_info.frontPhoto ? null : () => {
                  this.imageModalVisible = true
                  this.activeImage = vehicle_info && vehicle_info.frontPhoto && vehicle_info.frontPhoto.url
                }}
                carBackImg={status >= STATUS_DEFINE.ON_THE_WAY ? vehicle_info && vehicle_info.backPhoto && vehicle_info.backPhoto.urlSmall : null}
                carBackFunction={status < STATUS_DEFINE.ON_THE_WAY || !vehicle_info.backPhoto ? null : () => {
                  this.imageModalVisible = true
                  this.activeImage = vehicle_info && vehicle_info.backPhoto && vehicle_info.backPhoto.url
                }}
                expandFunction={() => {
                  this.detailVisible = true
                }}
              />
              {status === STATUS_DEFINE.ON_BOARD ?
                <View style={sosBtnCont}>
                  <TouchableOpacity style={dcBtn} onPress={() => this.sosVisible = true}>
                    <Text style={fBtn}>SOS</Text>
                  </TouchableOpacity>
                </View>
                : null
              }

              <DCInfoModal
                onHide={() => this.onHideDriverModal()}
                confirmBtn={app.strings.okay}
                onPress={() => this.onHideDriverModal()}
                visible={this.driverVisible}
                label={passenger.driver_info && passenger.driver_info.fullName}
                licenseNumber={passenger.driver_info && passenger.driver_info.evpInfo ? passenger.driver_info.evpInfo.evpNo : null}
                caption={passenger.driver_info ? passenger.driver_info.userId : null}
                rating={passenger.driver_info && passenger.driver_info.rating && passenger.driver_info.rating.toFixed()}
              >
                <Image
                  resizeMode="contain"
                  style={{ position: 'absolute', top: 0, left: 0, bottom: 0, right: 0 }}
                  source={{ uri: Avatars.getHeaderPicUrl(passenger.driver_info && passenger.driver_info.avatars) }}
                />
              </DCInfoModal>

              <DCBookingDetailModal
                onHide={() => this.onHideDetailModal()}
                visible={this.detailVisible}
                unmountFunction={() => this.onHideDetailModal()}
                bookingETA={status === STATUS_DEFINE.ON_THE_WAY ? passenger.driverETA : null}
                bookingStatus={passenger.booking_status}
                avatar={passenger.driver_info && passenger.driver_info.avatars}
                name={passenger.driver_info && passenger.driver_info.fullName}
                licenseNumber={passenger.driver_info && passenger.driver_info.evpInfo ? passenger.driver_info.evpInfo.evpNo : null}
                userId={passenger.driver_info && passenger.driver_info.userId}
                rating={passenger.driver_info && passenger.driver_info.rating && passenger.driver_info.rating.toFixed()}
                callFunction={() => {
                  this.onHideDetailModal()

                  const { phoneNo, phoneCountryCode } = passenger.driver_info
                  Linking.openURL(`tel:${phoneCountryCode}${phoneNo}`)
                }}
                messageFunction={() => {
                  this.onHideDetailModal()

                  this.props.navigation.navigate('ChatWindow', {
                    friendData: {
                      friend_info: Object.assign({}, { _id: passenger.driver_id }, passenger.driver_info),
                      friend_id: passenger.driver_id
                    },
                    chatType: 'friend'
                  })
                }}
                unreadChat={totalUnread}
                bookingId={passenger.booking_code}
                pickupAddress={passenger.from.address}
                dropoffAddress={passenger.destination.address}
                campaignName={passenger.groupName}
                campaignImg={group_info ? group_info.avatars ? group_info.avatars : campaign_info && campaign_info.avatars : null}
                driverNote={passenger.notes !== '' ? passenger.notes : app.strings.note_to_driver}
                price={passenger.fare}
                paymentType={passenger.payment_method}
                rejectFunction={() => this.onPressRejectBtn()}
                rejectBtnLabel={passenger.status !== STATUS_DEFINE.ON_BOARD ? app.strings.cancel_trip.toUpperCase() : 'SOS'}
              />
            </View>
          )
        }

        {/* <DCSearchDriverModal
          onHide={() => passenger.cancel()}
          visible={passenger.status === 2 || passenger.status === 3}
          price={passenger.fare}
          pickUpAddress={from ? from.name : app.strings.get_location}
          dropOffAddress={destination ? destination.name : app.strings.destination}
          cancelFunction={() => passenger.cancel()}
        /> */}

        <DCInfoModal
          onHide={() => $.store.passenger.noDriverModal = false}
          visible={$.store.passenger.noDriverModal}
          label={app.strings.driver_not_found_modal_label}
          caption={app.strings.driver_not_found_modal_caption}
          confirmBtn={app.strings.driver_not_found_modal_confirmBtn}
          cancelBtn={true}
          onPress={() => {
            $.store.passenger.noDriverModal = false
            $.store.passenger.booking()
          }}
        >
          <View style={{ width: '100%', padding: 20 }}>
            <Image style={{ width: '100%', resizeMode: 'contain' }} source={Resources.image.system_cause_error} />
          </View>
        </DCInfoModal>

        <DriverFoundModal />


        <DCInfoModal
          onHide={() => this.cancelModal = false}
          visible={this.cancelModal}
          label={app.strings.cancel_trip}
          caption={app.strings.cancel_trip_passenger}
          errorBtn={app.strings.cancel_trip.toUpperCase()}
          cancelBtn={true}
          onPress={() => this.onPressRejectBtn(true)}
        >
          <View style={{ width: '100%', padding: 20 }}>
            <Image style={{ width: '100%', resizeMode: 'contain' }} source={Resources.image.user_cause_error} />
          </View>
        </DCInfoModal>

        {/* <SponsorLocationModal
          navigation={this.props.navigation}
        /> */}

        <SosModal
          close={() => this.sosVisible = false}
          visible={this.sosVisible}
        />

        <ImageModal
          close={() => this.imageModalVisible = false}
          visible={this.imageModalVisible}
          uri={this.activeImage}
        />

        <PromoModel />

        <PromoDetailModel />

        <FareAlertModel />

        <AdvanceFareModalVisible />

        <CampaignErrorModalVisible />

      </View>
    ) : (
        <View style={{ flex: 1, backgroundColor: 'white', width }}>
          <MapView
            initialRegion={app.coords}
            onRegionChangeComplete={(coords) => {
              const { longitude, latitude } = coords
              passenger.setPinLocation({ longitude, latitude })
            }}
            origin={null}
            destination={null}
            showNavigator={false}
            showRouter={false}
            region_type={app.region}
            style={{ flex: 1, height, width }}
            ref={e => passenger.mapRefs = e}
          />
        </View>
      )
  }
}

const styles = StyleSheet.create({
  car_cell: {
    color: '#9e9e9e', fontSize: TextFont.TextSize(15), marginBottom: 5
  },
  text_cell: {
    flexDirection: 'row', flex: 1, alignItems: 'center'
  },
  dot: {
    height: 10, width: 10, borderRadius: 5
  },
  adress: {
    fontSize: TextFont.TextSize(15), fontWeight: 'bold', color: '#404040', marginLeft: 15
  },
  shadow: {
    elevation: 2,
    shadowOffset: { width: 0, height: 1 },
    shadowColor: 'rgba(0,0,0,0.4)',
    shadowOpacity: 0.4,
    shadowRadius: 3
  },
  circle: {
    height: 20,
    borderRadius: 10,
    position: 'absolute',
    backgroundColor: '#e4393c',
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 999,
    top: 4,
    left: 20
  },
  recenterBtn: {
    backgroundColor: 'white',
    borderRadius: 5,
    shadowOffset: { width: 0, height: -1 },
    shadowColor: '#000',
    shadowOpacity: 0.1,
    elevation: 0.5,
    height: 30,
    width: 30,
  },
  bookBtn: {
    backgroundColor: '#eaeaea',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    height: 55,
    borderRadius: 4,
    marginVertical: 5,
    shadowOffset: { width: 0, height: 1 },
    shadowColor: '#000',
    shadowOpacity: 0.3,
  },
  activeBtn: {
    backgroundColor: DCColor.BGColor('primary-1')
  },
  dataCont: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    height: '100%',
    width: '100%',
    padding: 10,
    paddingLeft: 20,
  },
  btnFont: {
    flex: 1,
    fontSize: TextFont.TextSize(16),
    fontWeight: 'bold',
    color: DCColor.BGColor('primary-2')
  },
  priceCont: {
    paddingHorizontal: 15,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    borderRadius: 4,
    backgroundColor: 'rgba(0, 0, 0, 0.75)',
    height: '100%'
  },
  sosBtnCont: {
    width,
    paddingHorizontal: 25,
    paddingBottom: 40,
    backgroundColor: DCColor.BGColor('primary-2')
  },
  dcBtn: {
    backgroundColor: DCColor.BGColor('red'),
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    height: 50,
    borderRadius: 4,
  },
  fBtn: {
    fontSize: TextFont.TextSize(15),
    fontWeight: 'bold',
    color: 'white'
  },
  recenterBtn: {
    position: 'absolute',
    top: -45,
    right: 10,
    width: 35,
    height: 35,
    borderRadius: 8,
    backgroundColor: 'white',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    shadowOffset: { width: 0, height: -1 },
    shadowColor: '#000',
    shadowOpacity: 0.05,
    elevation: 4
  },
  searchDriverCont: {
    backgroundColor: DCColor.BGColor('primary-2'),
  },
  expandBar: {
    width,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 5,
  },
  expandIcon: {
    resizeMode: 'contain',
    tintColor: DCColor.BGColor('primary-1'),
    opacity: .4,
    height: 9,
    width: 35
  },
  bookingStatusCont: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
    borderBottomWidth: 1,
    borderColor: 'rgba(255, 255, 255, 0.1)'
  },
  fBody: {
    fontSize: TextFont.TextSize(15),
    fontWeight: '900',
    color: DCColor.BGColor('primary-1'),
  },
})

export default withNavigationFocus(PassengerComponent)
