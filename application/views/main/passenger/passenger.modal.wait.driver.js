/* eslint-disable */
import React, { Component } from 'react'
import {
  View, Modal, Text, Animated, Easing
} from 'react-native'
import { inject, observer } from 'mobx-react'
import Lottie from 'lottie-react-native'

import { Screen, TextFont } from 'dacsee-utils'
import { Button } from '../../../components'

@inject('app', 'passenger')
@observer
export default class DriverRespondView extends Component {

  constructor(props) {
    super(props)
    this.state = { visible: false, animation: null, active: false, animationProgress: new Animated.Value(0) }
  }

  startAnimation() {
    const animation = Animated.loop(Animated.timing(this.state.animationProgress, {
      toValue: 1,
      fromValue: 0,
      duration: 3000,
      useNativeDriver: true,
      easing: Easing.linear
    }))
    this.setState({ animation }, () => {
      animation.start()
    })
  }

  stopAnimation() {
    if (this.state.animation) {
      this.state.animation.stop()
    }
  }

  componentWillUpdate(){
    const { passenger } = this.props
    const { visible, active } = this.state;
    const visibleUpdate = passenger.status === 2 || passenger.status === 3
    const activeUpdate = passenger.status === 3
    if (visibleUpdate !== visible || active !== activeUpdate) {
      this.setState({ visible: visibleUpdate, active: activeUpdate })
    }
    if (visibleUpdate && !visible) {
      this.startAnimation()
    } else if (!visibleUpdate && visible) {
      
      this.stopAnimation()
      clearInterval(passenger.bookingTimer)
    }
  }

  componentWillUnmount() {
    clearInterval(this.props.passenger.bookingTimer);
  }

  render() {
    const { passenger, app } = this.props
    const { visible, active, animationProgress } = this.state

    return (
      <Modal 
        visible={visible} 
        animationType='fade'
        transparent={true} 
        onRequestClose={() => {}}
      >
        <View style={{ justifyContent: 'center', alignItems: 'center', backgroundColor: '#66666699', flex: 1 }}>
          <View style={[
            { width: Screen.window.width - 90, height: 296, backgroundColor: 'white', borderRadius: 14, flexDirection: 'column', alignItems: 'center', justifyContent: 'center' },
            { paddingTop: 15, paddingBottom: 10 },
            { shadowOffset: { width: 0, height: 2 }, shadowColor: '#666', shadowOpacity: .3, shadowRadius: 3 }
          ]}>
            <View style={[
              { height: 120, width: Screen.window.width - 90, justifyContent: 'center', alignItems: 'center' },
              { top: 35, position: 'absolute' }
            ]}>
            </View>
            <View style={{ height: 180, justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
              <Lottie style={{ width: 180, height: 180 }} progress={animationProgress} source={require('../../../res/animation/ufo_radar.json')} />
            </View>
            <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1, paddingHorizontal: 15 }}>
              { (passenger.status === 2) && (<Text style={{ color: '#777',textAlign:'center', fontSize: TextFont.TextSize(16), fontWeight: '200' }}>{app.strings.confirm_pls_wait}</Text>) }
              { (passenger.status === 3) && (<Text style={{ color: '#777',textAlign:'center', fontSize: TextFont.TextSize(16), fontWeight: '200' }}>{app.strings.confirm_wait_order}</Text>) }
            </View>
            <Button  disabled={!active} onPress={() => passenger.cancel()} style={{ backgroundColor: active ? '#e54224' : '#ccc', borderRadius: 4, height: 44, marginHorizontal: 10 }}>
              <View style={{ flexDirection: 'row', alignItems: 'center', width: '100%', position: 'relative' }}>
                <Text style={{ fontSize: TextFont.TextSize(16), color: 'white', width: '100%', textAlign: 'center' }}>{app.strings.cancel}</Text>
                { (passenger.timerSeconds > 0) && (<Text style={{ fontSize: TextFont.TextSize(16), color: 'white', backgroundColor: 'rgba(0, 0, 0, 0.4)', borderRadius: 20, marginLeft: 10, paddingLeft: 10, paddingRight: 10, position: 'absolute', right: 15 }}>{passenger.timerSeconds}</Text>)}
              </View>
            </Button>
          </View>
        </View>
      </Modal>
    )
  }
}
