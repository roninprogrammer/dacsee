import React, { Component } from 'react'
import { Text, View, Image, ScrollView, TouchableOpacity, TextInput, StyleSheet, Alert, Platform } from 'react-native'
import { inject, observer } from 'mobx-react'
import { Screen, DCColor, TextFont } from 'dacsee-utils'
import Resources from 'dacsee-resources'
import PromoDetailModel from '../promo/component/modal.promo.detail'
import { BlockDriver } from './components'
import { DCInfoModal } from '../../modal'
import { DCUserInfoCont } from 'global-component' 
import HeaderTab from '../../../components/header-tab'

const { width } = Screen.window

@inject('app', 'passenger')
@observer
export default class PassengerFareScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const { control } = $.store.app
    return {
      drawerLockMode: 'locked-closed',
      headerTitle: (
        <HeaderTab onlyTitle={false} active={control.header.indexTrip} onPress={index => control.header.tripNavigate(index)}>
          <Image source={Resources.image.logo_landscape_border} style={{ width: 100, height: 60, resizeMode: 'contain', marginRight: 10 }} />
        </HeaderTab>
      ),
      headerLeft: null,
      headerStyle: {
        backgroundColor: DCColor.BGColor('primary-1'),
        shadowColor: 'transparent',
        shadowOpacity: 0,
        borderBottomWidth: 0,
        borderBottomColor: 'transparent',
        elevation: 0
      }
    }
  }

  constructor(props) {
    super(props)
    this.state = {
      reasonModal: false,
      ratingVisible: false,
      modalType: 'rating',
      modalProps: {},
      rating: 5,
      driver_id: ''
    }
  }

  componentDidMount() {
    const { passenger } = this.props

    passenger.getBlockReasons('passengerBlock')
    this.setState({ driver_id: passenger.driver_id })
  }

  componentWillUnmount() {
    $.store.joy.getJoyList()
    $.store.joy.getPendingJoy()
  }

  showBlockReason = () => {
    this.setState({ reasonModal: true })
  }

  onHideBlockReason = () => {
    this.setState({ reasonModal: false })
  }

  onBlock = async (reason_id, text) => {
    const { strings } = this.props.app
    const { driver_id } = this.state

    try {
      const data = await $.method.session.Circle.Put('v1/block', { 
        action: 'block', 
        user_id: driver_id, 
        blockReason_id: reason_id, 
        otherReason: text 
      })
        
      if (data.isSuccess) {
        $.store.circle.getFriends()
        return $.define.func.showMessage(strings.block_success)
      }

      this.completeRating()
      $.define.func.showMessage(strings.block_failed)
    } catch (e) {
      $.define.func.showMessage(strings.timeout_try_again)
    }
  }

  confirmReason = async (reason_id, text, key) => {
    if (key == 5 && text == '' || text == ' ') {
      Alert.alert($.store.app.strings.provide_reason_driver)
    } else {
      this.onHideBlockReason()
      this.completeRating()

      await this.onBlock(reason_id, text)
    }
  }

  onHideRating = () => {
    this.setState({ ratingVisible: false, modalType: 'rating', rating: 5 })
  }

  completeRating = () => {
    const { passenger } = this.props

    passenger.reset()
    this.props.navigation.popToTop()
  }

  getPayMentText(key) {
    const { strings } = this.props.app
    let pay = ''
    switch (key) {
      case 'cash':
        pay = strings.cash
        break
    }
    return pay
  }

  async submit() {
    const isIos = Platform.OS === 'ios';
    
    !isIos && this.props.app.showLoading()
    const result = await this.props.passenger.completed()
    !isIos && this.props.app.hideLoading()

    if (result) this.showModal('rating')
  }

  driverRating = (i) => {
    let { modalProps, rating } = this.state

    rating = i + 1
    modalProps.rating = i + 1

    this.setState({ rating: rating, modalProps: modalProps })
  }

  showModal = (type) => {
    const { passenger, app } = this.props
    const { strings } = app
    let { rating = 5 } = this.state

    let modalProps = {}

    switch(type) {
      case 'rating':
        modalProps = {
          rating: rating,
          rateFunction: (index) => this.driverRating(index),
          confirmBtn: strings.rating_submit,
          label: strings.rating_label,
          caption: strings.rating_driver_caption,
          onPress: async () => {
            // Submit Rating
            const { rating: usrRating } = this.state

            await passenger.bookingRating(usrRating)

            if (usrRating >= 4) {
              const found = $.store.circle.friendData.friends.filter((friend) => {
                return friend.friend_id === passenger.driver_id
              })
              
              if(found && found.length) {
                this.onHideRating()
                this.completeRating()
              } else {
                this.showModal('addFriend')
              }
              
            } else if (usrRating == 1) {
              this.showModal('block')
            } else {
              this.onHideRating()
              this.completeRating()
            }
          }
        }
        break
      
      case 'block':
        modalProps = {
          errorBtn: strings.block_driver_submit,
          label: strings.block_driver_label,
          caption: strings.block_driver_caption,
          onPress: () => {
            // Block Driver
            this.onHideRating()
            this.showBlockReason()
          }
        }
        break
      
      case 'addFriend':
        modalProps = {
          confirmBtn: strings.friend_add_submit,
          label: strings.friend_add_label,
          caption: strings.friend_add_driver_caption,
          onPress: () => {
            // Add Friend
            $.store.circle.sendRequest($.store.passenger.driver_id)
            this.onHideRating()
            this.completeRating()
          }
        }
        break
      default:
        this.onHideRating()
        this.completeRating()
        break
    }

    this.setState({ 
      modalType: type,
      modalProps: modalProps,
      rating: rating,
      ratingVisible: true
    })
  }

  render() {
    const {
      mainCont,
      bodyCont,
      dcCol,
      fTitle,
      dcTag,
      fTag,
      fareCont,
      notiCont,
      dcIcon,
      fCaption,
      actionCont,
      dcBtn,
      fBtn
    } = styles

    const { app, passenger } = this.props
    const { strings } = app
    let { fareObj, additional_fees, vehicle_info, driver_info = {} } = passenger
    let { amount = 0, promo_discount = 0, originalAmount = 0, promo, campaign_discount = 0 } = fareObj
    let total = 0

    additional_fees = parseFloat(additional_fees)
    originalAmount = parseFloat(originalAmount)
    promo_discount = parseFloat(promo_discount)
    campaign_discount = parseFloat(campaign_discount)

    amount = !!originalAmount ? originalAmount : parseFloat(amount)

    total = amount + additional_fees - promo_discount - campaign_discount

    const { modalProps = {}, ratingVisible = false, reasonModal = false } = this.state

    return (
      <View style={mainCont}>
        <BlockDriver 
          visible={reasonModal} 
          onClose={() => {
            this.onHideBlockReason()
            this.completeRating()
          }} 
          confirm={(reason, text, key) => this.confirmReason(reason, text, key)} 
        />
        <ScrollView style={{flex: 1}}>
          <View style={bodyCont}>
            <View style={[dcCol, { marginBottom: 20 }]}>
              <Text style={fTitle}>{strings.trip_fare}</Text>
              <View style={dcTag}>
                <Text style={fTag}>{this.getPayMentText('cash')}</Text>
              </View>
            </View>
            <View style={fareCont}>
              <CarCell left={strings.total_fare} right={`RM ${parseFloat(amount).toFixed(2)}`} />
              <CarCell left={strings.tolls_fare} right={`RM ${parseFloat(additional_fees).toFixed(2)}`} />
              {!!promo_discount &&
                <CarCell
                  onPressPromo={() => {
                    $.store.passenger.showPromoSummary()
                  }}
                  left={`${strings.promo} ${!!promo.promo_code ? promo.promo_code : ''}`} right={`- RM ${promo_discount.toFixed(2)}`}
                />
              }
              {!!campaign_discount &&
                <CarCell left={`Joy Promotion`} right={`- RM ${campaign_discount.toFixed(2)}`} />
              }
              <CarCell left={strings.total_fare} right={`RM ${total.toFixed(2)}`} background={DCColor.BGColor('primary-2')} color="rgba(255, 255, 255, 0.75)" />
            </View>
            <View style={notiCont}>
              <Image style={dcIcon} source={Resources.image.attention_icon} />
              <Text style={fCaption}>{strings.fare_message}</Text>
            </View>
          </View>
        </ScrollView>
        <View style={actionCont}>
          <TouchableOpacity onPress={this.submit.bind(this)} style={dcBtn}>
            <Text style={fBtn}>{strings.confirm.toUpperCase()}</Text>
          </TouchableOpacity>
        </View>
        <PromoDetailModel />

        <DCInfoModal
          onHide={() => {
            this.onHideRating()
            this.completeRating()
          }}
          visible={ratingVisible}
          cancelBtn={true}
          {...modalProps}
        >
          <DCUserInfoCont
            avatars={driver_info && driver_info.avatars}
            fullName={driver_info && driver_info.fullName}
            manufacturer={vehicle_info && vehicle_info.manufacturer}
            model={vehicle_info && vehicle_info.model}
            registrationNo={vehicle_info && vehicle_info && vehicle_info.registrationNo}
          />
        </DCInfoModal>
      </View>
    )
  }
}

const CarCell = (props) => {
  const {
    tCol,
    tLeftCont,
    tableFont
  } = styles

  return (
    <View style={props.background ? [tCol, { backgroundColor: props.background }] : tCol}>
      <View style={tLeftCont}>
        <Text style={props.color ? [tableFont, { color: props.color, fontWeight: '400' }] : [tableFont, { fontWeight: '400' }]}>{props.left}</Text>
        {props.onPressPromo &&
          <TouchableOpacity
            style={{ marginLeft: 5, height: 30, width: 30, borderRadius: 70, backgroundColor: 'rgba(0, 0, 0, 0.05)', justifyContent: 'center', alignItems: 'center' }}
            onPress={props.onPressPromo}
          >
            <Text style={{ fontSize: TextFont.TextSize(12), color: 'rgba(0, 0, 0, 0.75)' }}>?</Text>
          </TouchableOpacity>
        }
      </View>
      {
        props.editor ? (
          <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', width: width / 2 - 40, marginHorizontal: 5, backgroundColor: '#ececec', borderRadius: 6, height: 38 }}>
            <Text style={[props.color ? [tableFont, { color: props.color, marginLeft: 10 }] : [tableFont, { marginLeft: 10 }]]}>RM</Text>
            <TextInput
              onChangeText={props.onChangeText}
              placeholder={'0.00'}
              value={props.changValue}
              underlineColorAndroid='transparent'
              holderTextColor={'#dbdbdd'}
              style={{ height: 50, paddingLeft: 8, paddingRight: 10, flex: 1, textAlign: 'right' }}
            />
          </View>
        ) : <Text style={props.color ? [tableFont, { color: props.color }] : tableFont}>{props.right}</Text>
      }
    </View>
  )
}

const styles = StyleSheet.create({
  mainCont: {
    width,
    height: '100%',
    backgroundColor: 'white',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  bodyCont: {
    width,
    padding: 20,
  },
  dcCol: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start'
  },
  fTitle: {
    fontSize: TextFont.TextSize(18),
    fontWeight: '900',
    color: DCColor.BGColor('primary-2')
  },
  dcTag: {
    paddingHorizontal: 10,
    backgroundColor: DCColor.BGColor('primary-1'),
    borderRadius: 50,
    marginLeft: 10,
  },
  fTag: {
    fontSize: TextFont.TextSize(12),
    fontWeight: '500',
    color: DCColor.BGColor('primary-2')
  },
  fareCont: {
    borderWidth: 1,
    borderRadius: 8,
    borderColor: '#ccc',
    overflow: 'hidden'
  },
  tCol: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: 15,
    alignItems: 'center',
    height: 50,
    backgroundColor: '#fff',
    borderBottomWidth: 1,
    borderColor: '#ccc'
  },
  tLeftCont: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    flex: 1
  },
  tableFont: {
    color: '#000',
    fontSize: TextFont.TextSize(12),
    fontWeight: '700',
    marginRight: 15
  },
  notiCont: {
    marginTop: 8,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.1)',
    borderRadius: 4,
    padding: 10
  },
  dcIcon: {
    width: 30,
    height: 30,
    resizeMode: 'contain',
    marginRight: 5
  },
  fCaption: {
    fontSize: TextFont.TextSize(12),
    fontWeight: '400',
    color: 'rgba(0, 0, 0, 0.65)'
  },
  actionCont: {
    width,
    paddingHorizontal: 20,
    paddingBottom: 40,
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'center'
  },
  dcBtn: {
    width: '100%',
    borderRadius: 4,
    height: 55,
    backgroundColor: DCColor.BGColor('primary-1'),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  fBtn: {
    fontSize: TextFont.TextSize(15),
    fontWeight: '700',
    color: DCColor.BGColor('primary-2')
  }
})