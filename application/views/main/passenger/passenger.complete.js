import React, { Component } from 'react'
import { Text, View, TouchableOpacity, ScrollView } from 'react-native'
import InteractionManager from 'InteractionManager'
import { NavigationActions, StackActions } from 'react-navigation'
import { inject, observer } from 'mobx-react'
import { Icons, Define, DCColor, TextFont } from 'dacsee-utils'


const MAP_DEFINE = {
  showsCompass: false,
  showsScale: false,
  tiltEnabled: false,
  rotateEnabled: false,
  showsTraffic: false,
  showsZoomControls: false /* android fix */
}

@inject('app', 'passenger', 'account', 'circle')
@observer
export default class BookingCompleteScreen extends Component {

  static navigationOptions = ({ navigation }) => {
    return {
      drawerLockMode: 'locked-closed',
      title: $.store.app.strings.pay_comment,
      headerLeft: (
        <TouchableOpacity
          activeOpacity={0.7}
          style={{ top: 1, width: 54, paddingLeft: 8, justifyContent: 'center', alignItems: 'flex-start' }}
          onPress={() => {
            navigation.dispatch(StackActions.reset({
              index: 0,
              actions: [NavigationActions.navigate({ routeName: 'Main' })]
            }))
          }}
        >
          {Icons.Generator.Material('keyboard-arrow-left', 30, DCColor.BGColor('primary-2'))}
        </TouchableOpacity>
      )
    }
  }

  constructor(props) {
    super(props)
    this.state = {
    }
    this.tick = 0
  }

  componentDidMount() {
    InteractionManager.runAfterInteractions()
  }

  componentWillUnmount() {
    this.subscription && this.subscription.remove()
    this.trackTimer && clearTimeout(this.trackTimer)
  }

  render() {
    const { from, destination, fare, payment_method } = this.props.passenger
    const { strings } = this.props.app
    return !from ? (
      <View style={{ flex: 1, backgroundColor: 'white' }}></View>
    ) : (
        <ScrollView contentContainerStyle={{ paddingVertical: 20, paddingHorizontal: 15 }} style={{ flex: 1, backgroundColor: 'white' }}>
          <View style={{ marginBottom: 12 }}>
            <Text style={{ fontSize: TextFont.TextSize(14), color: '#999', fontWeight: '400', marginBottom: 6 }}>{strings.passenger_location}</Text>
            {from && (<Text style={{ top: -1.5, fontSize: TextFont.TextSize(14), color: '#333', fontWeight: '400' }}>{`${from.name}, ${from.address}`}</Text>)}
          </View>
          <View style={{}}>
            <Text style={{ fontSize: TextFont.TextSize(14), color: '#999', fontWeight: '400', marginBottom: 6 }}>{strings.destination}</Text>
            {destination && (<Text style={{ top: -1.5, fontSize: TextFont.TextSize(14), color: '#333', fontWeight: '400' }}>{`${destination.name}, ${destination.address}`}</Text>)}
          </View>
          <View style={{ height: Define.system.ios.plus ? 1 : .8, backgroundColor: '#f2f2f2', marginVertical: 12 }} />
          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            <Text style={{ fontSize: TextFont.TextSize(14), color: '#999', fontWeight: '400', marginBottom: 12 }}>{strings.trip_fare}</Text>
            {from && (<Text style={{ top: -1.5, fontSize: TextFont.TextSize(14), color: '#333', fontWeight: '400' }}>RM{parseFloat(fare).toFixed(2)}</Text>)}
          </View>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            <Text style={{ fontSize: TextFont.TextSize(14), color: '#999', fontWeight: '400' }}>{strings.payment_method}</Text>
            {payment_method && (<Text style={{ top: -1.5, fontSize: TextFont.TextSize(14), color: '#333', fontWeight: '400' }}>{payment_method == 'Cash' ? strings.cash : payment_method}</Text>)}
          </View>
          <View style={{ height: Define.system.ios.plus ? 1 : .8, backgroundColor: '#f2f2f2', marginVertical: 12 }} />
          <TouchableOpacity onPress={
            this.props.navigation.navigate('PassengerRate')
          }>
            <Text style={{ fontSize: 18, color: 'black', fontWeight: 'bold' }}>{strings.rating}</Text>
          </TouchableOpacity>
        </ScrollView>
      )
  }
}
