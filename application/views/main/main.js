/* global navigator */

import React, { Component } from 'react'
import { Text, View, Platform, Image, TouchableOpacity, StatusBar, ScrollView, AppState, ViewPagerAndroid } from 'react-native'
import { inject, observer } from 'mobx-react'
import { autorun } from 'mobx'
import Awesome from 'react-native-vector-icons/FontAwesome'
import Material from 'react-native-vector-icons/MaterialIcons'
import { NavigationActions } from 'react-navigation'
import PassengerComponent from './passenger'
import DriverComponent from './driver'
import { MessageScreen } from '../chat'
import { MyJoyScreen } from '../joy'
import HeaderTab from '../../components/header-tab'
import { CircleFriendScreen, CircleGroupScreen } from '../circle'
import InboxNews from './passenger/components/modal.inbox.news'
import { TabContainer, TabItem } from '../../components/bottom-tab'
import { Screen, DCColor } from 'dacsee-utils'
import Resources from 'dacsee-resources'

const { width } = Screen.window

@inject('app', 'joy')
@observer
class FriendGroupMain extends Component {
  constructor(props) {
    super(props)
    this.dispose = undefined
  }

  componentDidMount() {
    this.props.joy.getPendingJoy()
    this.dispose = autorun(() => this.switchComponent(this.props.app.control.header.indexCircle))
  }

  componentWillUnmount() {
    this.dispose && this.dispose()
  }

  switchComponent(index) {
    this.scroll && this.scroll.setPageWithoutAnimation && this.scroll.setPageWithoutAnimation(index)
    this.scroll && this.scroll.scrollTo && this.scroll.scrollTo({ y: 0, x: index * width, animated: false })
  }

  render() {
    const { navigation } = this.props
    const Component = Platform.select({ ios: ScrollView, android: ViewPagerAndroid })

    return (
      <Component
        ref={e => this.scroll = e}
        initialPage={this.props.app.control.header.indexCircle}
        peekEnabled={false}
        horizontal={true}
        scrollEnabled={false}
        pinchGestureEnabled={false}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
        style={{ flex: 1 }}
      >
        <View style={{ flex: 1, width }}>
          <CircleGroupScreen navigation={navigation} />
        </View>
        <View style={{ flex: 1, width }}>
          <CircleFriendScreen navigation={navigation} />
        </View>
      </Component>
    )
  }
}

@inject('app')
@observer
class PassengerDriverMain extends Component {
  constructor(props) {
    super(props)
    this.dispose = undefined
  }

  async componentDidMount() {
    this.dispose = autorun(() => this.switchComponent(this.props.app.control.header.indexTrip))

    if (Platform.OS === 'ios') await $.store.jobs.workingChange(false)
    // $.store.jobs.syncServerWorkingStatus()
  }

  componentWillUnmount() {
    this.dispose && this.dispose()
  }

  switchComponent(index) {
    this.scroll && this.scroll.setPageWithoutAnimation && this.scroll.setPageWithoutAnimation(index)
    this.scroll && this.scroll.scrollTo && this.scroll.scrollTo({ y: 0, x: index * width, animated: false })
  }

  render() {
    const { navigation } = this.props
    const Component = Platform.select({ ios: ScrollView, android: ViewPagerAndroid })

    return (
      <Component
        ref={e => this.scroll = e}
        initialPage={this.props.app.control.header.indexTrip}
        peekEnabled={false}
        horizontal={true}
        scrollEnabled={false}
        pinchGestureEnabled={false}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
        style={{ flex: 1 }}
      >
        <View style={{ flex: 1, width }}>
          <PassengerComponent navigation={navigation} />
        </View>
        <View style={{ flex: 1, width }}>
          <DriverComponent navigation={navigation} />
        </View>
      </Component>
    )
  }
}

@inject('app', 'passenger', 'jobs')
@observer
class NavigationHeader extends Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    // const { jobs } = this.props

    // if (jobs.working) jobs.workingChange(true)
  }

  render() {
    const { app = {}, passenger = {}, jobs = {} } = this.props
    const { control, strings } = app

    if (control.tab.name === 'CommunityTab') {
      return (
        <HeaderTab onlyTitle={false} active={control.header.indexCircle} onPress={index => control.header.circleNavigate(index)}>
          <HeaderTab.Item>{strings.group}</HeaderTab.Item>
          <HeaderTab.Item>{strings.friend}</HeaderTab.Item>
        </HeaderTab>
      )
    } else if (control.tab.name === 'MainTab') {
      if (!$.store.jobs.working && !!$.store.account.isDriver) {
        if (passenger.status >= 2 && passenger.status <= 8) {
          return (
            <HeaderTab onlyTitle={false} active={control.header.indexTrip} onPress={index => control.header.tripNavigate(index)}>
              <Image source={Resources.image.logo_landscape_border} style={{ width: 100, height: 60, resizeMode: 'contain', marginRight: 10 }} />
            </HeaderTab>
          )
        } else {
          return (
            <HeaderTab onlyTitle={passenger.status >= 1} active={control.header.indexTrip} onPress={index => control.header.tripNavigate(index)}>
              <HeaderTab.Item>{strings.passenger}</HeaderTab.Item>
              <HeaderTab.Item>{strings.driver}</HeaderTab.Item>
            </HeaderTab>
          )
        }
      } else {
        return (
          <HeaderTab onlyTitle={false} active={control.header.indexTrip} onPress={index => control.header.tripNavigate(index)}>
            <Image source={Resources.image.logo_landscape_border} style={{ width: 100, height: 60, resizeMode: 'contain', marginRight: 10 }} />
          </HeaderTab>
        )
      }
    } else if (control.tab.name === 'ChatTab') {
      return Platform.select({
        ios: (
          <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', height: 44 }}>
            <Text style={{ color: DCColor.BGColor('primary-2'), fontSize: 17, fontWeight: '600' }}>{strings.chat}</Text>
          </View>
        ),
        android: (
          <View style={{ flex: 1, alignItems: 'center', flexGrow: 1 }}>
            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', height: 44 }}>
              <Text style={{ color: DCColor.BGColor('primary-2'), fontSize: 17, fontWeight: '600' }}>{strings.chat}</Text>
            </View>
          </View>
        )
      })
    } else {
      return Platform.select({
        ios: (
          <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', height: 44 }}>
            <Text style={{ color: DCColor.BGColor('primary-2'), fontSize: 17, fontWeight: '600' }}>{strings.my_joy}</Text>
          </View>
        ),
        android: (
          <View style={{ flex: 1, alignItems: 'center', flexGrow: 1 }}>
            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', height: 44 }}>
              <Text style={{ color: DCColor.BGColor('primary-2'), fontSize: 17, fontWeight: '600' }}>{strings.my_joy}</Text>
            </View>
          </View>
        )
      })
    }
  }
}

@inject('app')
@observer
class NavigatorHeaderRightButton extends Component {
  render() {
    const { app } = this.props
    return (app.control.tab.name === 'CommunityTab') ? (
      <TouchableOpacity
        activeOpacity={0.7}
        style={{ top: 1, width: 54, paddingRight: 18, justifyContent: 'center', alignItems: 'flex-end' }}
        onPress={() => {
          app.control.header.indexCircle === 0 ?
            global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'CircleGroupAdd', params: {} })) :
            global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'CircleFriendAdd', params: {} }))
        }}
      >
        <Material name={'add'} size={30} color={DCColor.BGColor('primary-2')} />
      </TouchableOpacity>
    ) : (null)
  }
}

@inject('app', 'passenger')
@observer
class NavigatorHeaderLeftButton extends Component {
  render() {
    const { app, passenger } = this.props
    const { control } = app

    if (passenger.status === 1 && control.tab.name === 'MainTab') {
      return (
        <TouchableOpacity
          activeOpacity={0.7}
          style={{ top: 1, width: 54, paddingLeft: 8, justifyContent: 'center', alignItems: 'flex-start' }}
          onPress={() => passenger.reset()}
        >
          <Material name={'keyboard-arrow-left'} size={30} color={DCColor.BGColor('primary-2')} />
        </TouchableOpacity>
      )
    } else if (passenger.status >= 2) {
      if (control.tab.name === 'MainTab') {
        return null
      }
      return (
        <TouchableOpacity
          activeOpacity={0.7}
          style={{ top: 1, width: 54, paddingLeft: 18, justifyContent: 'center', alignItems: 'flex-start' }}
          onPress={() => this.props.navigation.openDrawer()}
        >
          <Awesome name={'bars'} size={21} color={DCColor.BGColor('primary-2')} />
        </TouchableOpacity>
      )


    } else {
      return (
        <TouchableOpacity
          activeOpacity={0.7}
          style={{ top: 1, width: 54, paddingLeft: 18, justifyContent: 'center', alignItems: 'flex-start' }}
          onPress={() => this.props.navigation.openDrawer()}
        >
          <Awesome name={'bars'} size={21} color={DCColor.BGColor('primary-2')} />
        </TouchableOpacity>
      )
    }
  }
}

@inject('app', 'chat', 'joy', 'inbox')
@observer
export default class MainScreen extends Component {

  async componentWillMount() {
    await $.store.inbox.getInboxUnreadList()
  }

  async componentDidMount() {
    AppState.addEventListener('change', this._handleAppStateChange);
    this.getInboxPopupDetail()
  }

  componentWillUnmount() {
    AppState.removeEventListener('change', this._handleAppStateChange);
  }

  _handleAppStateChange = async (state) => {
    if (state === 'active') {
      await $.store.inbox.getInboxUnreadList()
    }
  }

  async getInboxPopupDetail() {
    const data = await this.props.inbox.getInboxModalDetail()
    if (data && data.length !== 0) {
      if ($.store.app.updateStatus === false) {
        this.popupMessageView()
      }
      else {
        return
      }
    }
  }

  popupMessageView() {
    if ($.store.app.runModalPopup === true) {
      this.timer = setTimeout(() => {
        if ($.store.inbox.inboxModalDetail.length > 0) {
          $.store.inbox.viewModal = true
        }
      }, 2000)
    }
    else {
      this.timer = setTimeout(() => {
        this.popupMessageView()
      }, 3000)
    }

  }

  componentWillUnmount() {
    clearTimeout(this.timer);
  }

  constructor(props) {
    super(props)
    this.state = {
      inboxModal: false,
      data: []
    }
  }

  viewInboxDetailScreen() {
    if ($.store.app.viewInboxDetail == true) {
      this.setState({
        inboxModal: false
      })
    }
  }
  onHide = () => {
    if ($.store.inbox.checkInbox === true) {
      $.store.inbox.pressNeverShow()
    }
    $.store.inbox.viewModal = false
  }

  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state
    const { headerOptions = {} } = params

    const maps = Object.assign({}, {
      drawerLockMode: 'locked-closed',
      headerTitle: (<NavigationHeader />),
      headerLeft: (
        <NavigatorHeaderLeftButton navigation={navigation} />
      ),
      headerRight: (
        <NavigatorHeaderRightButton navigation={navigation} />
      )
    }, headerOptions)

    return maps
  }

  render() {
    const { app, chat, joy } = this.props

    return (
      <View style={{ flex: 1, alignItems: 'center' }}>
        <InboxNews visible={$.store.inbox.viewModal} onClose={() => this.onHide()} />
        <StatusBar
          animated={true}
          hidden={false}
          backgroundColor={DCColor.BGColor('primary-1')}
          barStyle={'dark-content'}
        />
        <TabContainer
          onPress={name => app.control.tab.navigate(name)}
          tab={app.control.tab.name}
          totalUnread={chat.totalUnread}
          totalReward={joy.pendingJoy}
        >
          <TabItem
            iconStyle={{ tintColor: '#d2d2d2', height: 22, width: 22, top: -.5 }}
            render={<PassengerDriverMain navigation={this.props.navigation} />}
            icon={require('../../res/images/map-marker.png')}
            label={app.strings.home}
            name='MainTab'
          />
          {/* <TabItem
            iconStyle={{ tintColor: '#d2d2d2', height: 25, width: 25 }}
            render={<FriendGroupMain navigation={this.props.navigation} />}
            icon={require('../../res/images/friend-circle.png')}
            label={app.strings.group}
            name='CommunityTab'
          /> */}
          <TabItem
            iconStyle={{ tintColor: '#d2d2d2', height: 25, width: 25 }}
            render={<MessageScreen navigation={this.props.navigation} />}
            icon={require('../../res/images/im-message.png')}
            label={app.strings.chat}
            name='ChatTab'
          />
          <TabItem
            iconStyle={{ tintColor: '#d2d2d2', height: 25, width: 25 }}
            render={<MyJoyScreen navigation={this.props.navigation} />}
            icon={require('../../res/images/joy.png')}
            label={app.strings.my_joy}
            name='JoyTab'
          />
        </TabContainer>
      </View>
    )
  }
}