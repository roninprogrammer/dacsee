import React, { Component } from 'react'
import { Dimensions, StyleSheet, Text, View, Image, ListView, TextInput, TouchableOpacity, RefreshControl, SafeAreaView, InteractionManager } from 'react-native'
import { inject, observer } from 'mobx-react'
import { observable } from 'mobx'
import Material from 'react-native-vector-icons/MaterialIcons'
import { DCColor } from 'dacsee-utils'

const { width } = Dimensions.get('window')
const dataContrast = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })
@inject('app', 'circle')
@observer
export default class Friend extends Component {
  static navigationOptions = { header: null }
  @observable isSearch: Boolean = false
  @observable searchValue: String = ''
  @observable searchFriends: Array = []
  constructor(props) {
    super(props)
    this.friendData = props.circle.friendData
    this.toggerSelectAll = props.circle.toggerSelectAll
    this.confirmFriendSelect = props.circle.confirmFriendSelect
    this.friendRequest = props.circle.friendRequest
    this.getFriends = props.circle.getFriends
    this.getFriendRequests = props.circle.getFriendRequests
    const { select_friends = [] } = this.friendData
    let _friends = this.friendData.friends.map(pipe => Object.assign({}, pipe, {
      checked: typeof (select_friends) === 'string' ? false : select_friends.find(sub => sub._id === pipe._id) !== undefined
    }))
    this.friendData.check_friends = _friends
  }
  async componentDidMount() {
    await InteractionManager.runAfterInteractions()
    this.getFriends()
  }
  onChangeText(text) {
    if (text === '') {
      this.isSearch = false
    } else {
      if (!this.isSearch) this.isSearch = true
      this.searchFriend(text)
    }
    this.searchValue = text
  }
  async searchFriend(value: String) {
    const { check_friends } = this.props.circle.friendData
    try {
      const data = await $.method.session.Circle.Get(`v1/circleSearch?fullName=${value}`)
      let friends = data.friends.map(pipe => Object.assign({}, pipe, {
        checked: check_friends.find(sub => sub._id === pipe._id).checked
      }))
      this.searchFriends = friends || []
    } catch (e) {
      this.searchFriends = []
    }
  }
  onPressCheck(itemData: Object, index: Int) {
    if (this.isSearch) {
      let _clone = this.searchFriends.slice()
      _clone[index].checked = !_clone[index].checked
      this.searchFriends = _clone
      index = this.friendData.check_friends.findIndex((item) => item._id === itemData._id)
    }
    this.props.circle.onPressCheck(itemData, index)
  }
  renderFooter = () => {
    const { strings } = this.props.app
    return (
      (this.friendData.check_friends.length === 0)
        ? <View>
          <View style={{ marginTop: 85, alignItems: 'center' }}>
            <Image style={{ marginBottom: 18 }} source={require('../../../resources/images/friend-empty-state.png')} />
            <Text style={{ color: '#666', fontSize: 22, fontWeight: '600', textAlign: 'center', marginBottom: 6 }}>
              {strings.no_friend}
            </Text>
            <Text style={{ color: '#999', fontSize: 22, fontWeight: '400', textAlign: 'center' }}>
              {strings.go_friend_add}
            </Text>
          </View>
        </View>
        : null
    )
  }

  renderSectionHeader = (data) => {
    const { strings } = this.props.app
    const { selectAll } = this.friendData
    return (
      <View style={[
        {
          flexDirection: 'row',
          paddingHorizontal: 10,
          height: 40,
          alignItems: 'center',
          justifyContent: 'space-between',
          backgroundColor: 'white',
          borderBottomWidth: 1,
          borderColor: 'rgba(0, 0, 0, 0.1)'
        }, styles.shadow, this.isSearch && { backgroundColor: DCColor.BGColor('primary-1') }]}>
        <Text style={{ fontSize: 12, color: '#8c8c8c', fontWeight: '600' }}>{this.isSearch ? strings.search_result : strings.friend_my}</Text>
        {!this.isSearch && <TouchableOpacity onPress={() => this.toggerSelectAll()}
          activeOpacity={0.7}
          style={[styles.circle, { backgroundColor: selectAll ? DCColor.BGColor('green') : '#e7e7e7' }]}>
          {selectAll && <Material name='check' color='white' size={18} />}
        </TouchableOpacity>}
      </View>
    )
  }

  renderItem(data, index) {
    const { friend_info, checked } = data
    const { _id, userId, avatars, fullName, label = [], labeltype } = friend_info
    return (
      <TouchableOpacity onPress={() => { this.onPressCheck(data, parseInt(index)) }}
        activeOpacity={0.7} style={[{
          height: 70,
          paddingHorizontal: 10,
          backgroundColor: 'white',
          alignItems: 'center',
          flexDirection: 'row',
          borderBottomWidth: 1,
          borderColor: 'rgba(0, 0, 0, 0.1)'
        }, styles.shadow]}>
        <View style={{ justifyContent: 'center', marginRight: 10 }}>
          <Image style={{ width: 40, height: 40, borderRadius: 20 }} source={{ uri: $.method.utils.getHeaderPicUrl(avatars) }} />
        </View>
        <View style={{ flex: 1, marginRight: 10, justifyContent: 'center' }}>
          <View style={{ justifyContent: 'center' }}>
            <Text ellipsizeMode={'middle'} numberOfLines={1} style={{ fontSize: 14, color: '#333', fontWeight: '400' }}>{fullName}</Text>
          </View>
          <View style={{ marginTop: 10, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', overflow: 'hidden' }}>
            {
              label.map((item, index) => {
                return (
                  <View key={index} style={{ paddingHorizontal: 12, borderRadius: 10, backgroundColor: labeltype === 'hot' ? DCColor.BGColor('primary-1') : '#ccc', paddingVertical: 2 }}>
                    <Text style={{ color: '#fff' }}>{item}</Text>
                  </View>
                )
              })
            }
          </View>
        </View>
        <TouchableOpacity onPress={() => this.onPressCheck(data, parseInt(index))}
          activeOpacity={0.7} style={{ width: 23, height: 84, alignItems: 'center', justifyContent: 'center' }}>
          <View style={[styles.circle, { backgroundColor: checked ? DCColor.BGColor('green') : '#e7e7e7' }]}>
            {checked ? <Material name='check' color='white' size={18} /> : null}
          </View>
        </TouchableOpacity>
      </TouchableOpacity>
    )
  }

  render() {
    const { strings } = this.props.app
    const { loading = false, check_friends = [] } = this.friendData
    const _check = check_friends.filter(item => item.checked)
    return (
      <SafeAreaView style={{ flex: 1, flexDirection: 'column', backgroundColor: '#f2f2f2' }}>
        <View style={{ paddingHorizontal: 15, backgroundColor: 'white' }}>
          {/* <HeaderSearchBar
            name={strings.search_name_phone_email}
            search_value={this.searchValue}
            isSearch={this.isSearch}
            onChangeText={(text) => this.onChangeText(text)} /> */}
        </View>

        {/* <View style={{ padding: 5 }}>
          <SponsorInfo
            icon={Resources.image.attention_icon}
            label={strings.list_friends} 
          />
        </View> */}

        <View style={{ flex: 1 }}>
          <ListView
            removeClippedSubviews={false}
            //  contentContainerStyle={{ paddingHorizontal: 15, paddingBottom: 84 }}
            enableEmptySections
            refreshControl={<RefreshControl
              refreshing={this.props.circle.circleLoading}
              onRefresh={() => this.getFriends()}
              title={strings.pull_refresh}
              colors={['#ffffff']}
              progressBackgroundColor={DCColor.BGColor('primary-1')}
            />}
            // renderHeader={() =>
            //   <HeaderSearchBar name={strings.search_name_phone_email}
            //     onChangeText={(val) => this.search_text = val} />}
            stickySectionHeadersEnabled={false}
            dataSource={this.isSearch
              ? dataContrast.cloneWithRows(this.searchFriends.slice())
              : dataContrast.cloneWithRows(check_friends.slice())}
            renderSectionHeader={(data, section) => this.renderSectionHeader(data, section)}
            renderFooter={this.renderFooter}
            renderRow={(data, section, rowId) => this.renderItem(data, rowId)}
          />
        </View>

        {/* <View style={styles.bottomButton}>
          <TouchableOpacity disabled={_check.length === 0} activeOpacity={0.9} style={[styles.button_contain, styles.shadow, { backgroundColor: _check.length === 0 ? '#D8D8D8' : (DCColor.BGColor('primary-1')) }]}
            onPress={() => this.confirmFriendSelect()}>
            <Text style={{ fontSize: 18, fontWeight: '400', color: 'white' }}>{strings.confirm}</Text>
          </TouchableOpacity>
        </View> */}
      </SafeAreaView>
    )
  }
}

const HeaderSearchBar = (props) => {
  const { name, onChangeText = () => { }, isSearch } = props
  return (
    <View style={[styles.search_bar]}>
      <View style={{ width: 40, height: 32, alignItems: 'center', justifyContent: 'center' }}>
        <Material name='search' color='#ABABAB' size={18} />
      </View>
      <TextInput
        ref={e => this.input = e}
        underlineColorAndroid={'transparent'}
        placeholderTextColor={'#D1D1DE'}
        placeholder={name}

        // onFocus={onFocus}
        style={{ height: 42, width: width - (isSearch ? 110 : 70), paddingLeft: 0, color: '#5c5c5c' }}
        onChangeText={(text) => onChangeText(text)} />
      <View style={{ width: 40, height: 32, alignItems: 'center', justifyContent: 'center' }}
      >
        {isSearch
          ? <TouchableOpacity style={{ width: 30, height: 30, borderRadius: 18, justifyContent: 'center', alignItems: 'center', backgroundColor: DCColor.BGColor('red') }} activeOpacity={0.7}
            onPress={() => {
              this.input.clear()
              this.input.blur()
              onChangeText('')
              setTimeout(() => { this.input.setNativeProps({ text: '' }) })
            }}>
            <Material name='close' color='#fff' size={18} />
          </TouchableOpacity> : null
        }

      </View>
    </View>
  )
}
const styles = StyleSheet.create({
  bottomButton: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 70,
    width,
    backgroundColor: 'white',
    borderTopWidth: 1,
    borderColor: 'rgba(0, 0, 0, 0.1)'
  },
  confirmButton: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 60,
    borderRadius: 12,
    backgroundColor: DCColor.BGColor('primary-1')
  },
  search_bar: {
    flexDirection: 'row',
    marginVertical: 10,
    backgroundColor: '#F2F2F2',
    borderRadius: 5,
    alignItems: 'center'
  },
  circle: {
    width: 23,
    height: 23,
    borderRadius: 13,
    justifyContent: 'center',
    alignItems: 'center'

  },
  shadow: {
    elevation: 2,
    shadowOffset: { width: 0, height: 0 },
    shadowColor: 'rgba(0,0,0,0.4)',
    shadowOpacity: 0.4,
    shadowRadius: 3
  },
  button_contain: {
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 20,
    height: 50,
    width: width - 80
  },
  edit_contain: {
    backgroundColor: '#f6f5fb',
    borderRadius: 10,
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 10
  }
})
