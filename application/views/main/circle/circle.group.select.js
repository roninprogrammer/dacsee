import React, { Component } from 'react'
import { Dimensions, StyleSheet, Text, View, Image, ListView, TextInput, TouchableOpacity, RefreshControl, SafeAreaView } from 'react-native'
import { inject, observer } from 'mobx-react'
import { observable } from 'mobx'
import Material from 'react-native-vector-icons/MaterialIcons'
import { DCColor } from 'dacsee-utils'
import Resources from 'dacsee-resources'
import SponsorInfo from '../sponsorLocation/component/Sponsor.info';

const { width } = Dimensions.get('window')
const dataContrast = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })

@inject('app', 'circle')
@observer
export default class FriendsGroupRoom extends Component {
  @observable isSearch: Boolean = false
  @observable searchValue: String = ''
  @observable searchGroups: Array = []
  constructor(props) {
    super(props)
    this.groupData = props.circle.groupData
    this.confirmGroupSelect = props.circle.confirmGroupSelect
    this.getGroups = props.circle.getGroups
    this.getInvitations = props.circle.getInvitations
    this.groupRequest = props.circle.groupRequest
    const { select_group } = this.groupData
    let _groups = props.circle.getAllGroups.map(pipe => Object.assign({}, pipe, {
      checked: typeof (select_group) === 'string' ? false : select_group._id === pipe._id
    }))
    this.groupData.check_group = _groups
  }

  async componentDidMount() {
    const { select_group } = this.groupData
    await this.getGroups()
    let _groups = this.props.circle.getAllGroups.map(pipe => Object.assign({}, pipe, {
      checked: typeof (select_group) === 'string' ? false : select_group._id === pipe._id
    }))
    this.groupData.check_group = _groups
  }
  onChangeText(text) {
    if (text === '') {
      this.isSearch = false
    } else {
      if (!this.isSearch) this.isSearch = true
      this.searchGroup(text)
    }
    this.searchValue = text
  }
  async searchGroup(value: String) {
    const { check_group } = this.props.circle.groupData
    try {
      const data = await $.method.session.Circle.Get(`v1/groupSearch?name=${value}&context=booking`)
      console.log(data, check_group)
      let groups = data.map(pipe => Object.assign({}, pipe, {
        checked: check_group.find((sub, index) => {
          if (sub._id === pipe._id) return check_group[index].checked
        })
      }))
      console.log(groups)
      this.searchGroups = groups || []
    } catch (e) {
      this.searchGroups = []
    }
  }
  onPressCheck(itemData: Object) {
    if (this.isSearch) {
      let _groups = this.searchGroups.map(pipe => Object.assign({}, pipe, {
        checked: pipe._id === itemData._id && !pipe.checked
      }))
      this.searchGroups = _groups
    }
    this.props.circle.selectGroup(itemData)
  }
  _renderFooter = () => {
    const { strings } = this.props.app
    return (
      this.props.circle.getAllGroups.length === 0
        ? <View>
          <View style={{ marginTop: 108, alignItems: 'center' }}>
            <Image style={{ marginBottom: 18 }} source={require('../../../resources/images/friend-empty-state.png')} />
            <Text style={{ color: '#666', fontSize: 22, fontWeight: '600', textAlign: 'center', marginBottom: 6 }}>
              {strings.no_friend}
            </Text>
            <Text style={{ color: '#999', fontSize: 15, fontWeight: '400', textAlign: 'center' }}>
              {strings.clickto_add_friend}
            </Text>
          </View>
        </View> : null
    )
  }
  getTypeData(type) {
    let data = {}
    const { strings } = this.props.app
    switch (type) {
      case 'public':
        data.text = strings.public
        data.icon = 'lock-open'
        data.iconColor = DCColor.BGColor('primary-1')
        break
      case 'reserved':
        data.text = strings.official
        data.icon = 'lock'
        data.iconColor = '#797979'
        break
      default:
        data.text = strings.private
        data.icon = 'lock'
        data.iconColor = '#797979'
    }
    return data
  }

  renderItem = (data, section, rowId) => {
    const { _id, name, type = 'public', membersCount, avatars, checked, tagline } = data
    const { strings } = this.props
    const getTypeData = this.getTypeData(type)
    return (
      <TouchableOpacity onPress={() => this.onPressCheck(data)}
        activeOpacity={1} key={section} style={{ height: 84, paddingHorizontal: 10, backgroundColor: 'white', justifyContent: 'space-between', alignItems: 'center', flexDirection: 'row', borderBottomWidth: 1.5, borderColor: 'rgba(0, 0, 0, 0.1)' }}>
        <View style={{ justifyContent: 'center' }}>
          <Image style={{ width: 56, height: 56, borderRadius: 28 }} source={{ uri: $.method.utils.getHeaderPicUrl(avatars) }} />
        </View>
        <View style={{ flex: 1, marginLeft: 10 }}>
          <View style={{ flexDirection: 'column', alignItems: 'flex-start' }}>
            <Text style={{ fontSize: 16, color: '#000033', fontWeight: '400', marginBottom: 5 }}>{name}</Text>
            {tagline ? (
              <Text style={{ fontSize: 12, color: '#000033', fontWeight: '600', marginBottom: 5 }}>{tagline}</Text>
            ) : null}
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', paddingHorizontal: 5, paddingVertical: 0 }}>
                <Material name={getTypeData.icon} color={getTypeData.iconColor} size={15} />
                <Text style={{ fontSize: 13, color: '#000', opacity: 0.8 }}>{getTypeData.text}</Text>
                {type !== 'reserved' && <Text style={{ marginLeft: 5, color: '#000', opacity: 0.8 }}>({membersCount})</Text>}
              </View>
            </View>

          </View>
        </View>
        <View hitSlop={{ top: 27, left: 40, bottom: 27, right: 0 }}
          style={[styles.circle, { backgroundColor: checked ? DCColor.BGColor('green') : '#e7e7e7' }]}>
          {checked ? <Material name='check' color='white' size={18} /> : null}
        </View>
      </TouchableOpacity>
    )
  }
  render() {
    const { strings } = this.props.app
    const { check_group = [] } = this.groupData
    const _check = this.isSearch ? this.searchGroups.filter(item => item.checked) : check_group.filter(item => item.checked)

    return (
      <SafeAreaView style={{ flex: 1, flexDirection: 'column', backgroundColor: '#f2f2f2' }}>
        <View style={{ paddingHorizontal: 15, backgroundColor: 'white' }}>
          <HeaderSearchBar
            name={strings.search_name_phone_email}
            search_value={this.searchValue}
            isSearch={this.isSearch}
            onChangeText={(text) => this.onChangeText(text)} />
        </View>

        <View style={{ padding: 5 }}>
          <SponsorInfo
            icon={Resources.image.attention_icon}
            label={strings.list_notice}
          />
        </View>

        <View style={{ flex: 1 }}>
          <ListView
            removeClippedSubviews={false}
            enableEmptySections
            refreshControl={
              <RefreshControl
                refreshing={this.props.circle.circleLoading}
                onRefresh={() => this.getGroups()}
                title={strings.pull_refresh}
                colors={['#ffffff']}
                progressBackgroundColor={DCColor.BGColor('primary-1')}
              />
            }
            stickySectionHeadersEnabled={false}
            dataSource={this.isSearch ?
              dataContrast.cloneWithRows(this.searchGroups.slice())
              : dataContrast.cloneWithRows(check_group.slice())
            }
            renderFooter={this.renderFooter}
            renderRow={(data, section, rowId) => this.renderItem(data, rowId)}
          />
        </View>
        <View style={styles.bottomButton}>
          <TouchableOpacity disabled={_check.length === 0} activeOpacity={0.9} style={[styles.button_contain, styles.shadow, { backgroundColor: _check.length === 0 ? '#D8D8D8' : DCColor.BGColor('primary-1') }]}
            onPress={() => this.confirmGroupSelect(this.isSearch, this.searchGroups)}>
            <Text style={{ fontSize: 18, fontWeight: '400', color: 'white' }}>{strings.confirm}</Text>
          </TouchableOpacity>
        </View>
      </SafeAreaView >
    )
  }
}
const HeaderSearchBar = (props) => {
  const { name, onChangeText = () => { }, isSearch } = props
  return (
    <View style={styles.search_bar}>
      <View style={{ width: 40, height: 32, alignItems: 'center', justifyContent: 'center' }}>
        <Material name='search' color='#ABABAB' size={18} />
      </View>
      <TextInput
        ref={e => this.input = e}
        underlineColorAndroid={'transparent'}
        placeholderTextColor={'#D1D1DE'}
        placeholder={name}
        style={{ height: 42, width: width - (isSearch ? 110 : 70), paddingLeft: 0, color: '#5c5c5c' }}
        onChangeText={(text) => onChangeText(text)} />
      <View style={{ width: 40, height: 32, alignItems: 'center', justifyContent: 'center' }}
      >
        {isSearch ?
          <TouchableOpacity
            style={{ width: 30, height: 30, borderRadius: 18, justifyContent: 'center', alignItems: 'center', backgroundColor: DCColor.BGColor('red') }} activeOpacity={0.7}
            onPress={() => {
              this.input.clear()
              this.input.blur()
              onChangeText('')
              setTimeout(() => { this.input.setNativeProps({ text: '' }) })
            }}
          >
            <Material name='close' color='#fff' size={18} />
          </TouchableOpacity>
          : null
        }
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  bottomButton: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 70,
    width,
    backgroundColor: 'white',
    borderTopWidth: 1,
    borderColor: 'rgba(0, 0, 0, 0.1)'
  },
  confirmButton: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 60,
    borderRadius: 12,
    backgroundColor: '#45e2be'
  },
  search_bar: {
    flexDirection: 'row',
    marginVertical: 10,
    backgroundColor: '#F2F2F2',
    borderRadius: 5,
    alignItems: 'center'
  },
  circle: {
    width: 23,
    height: 23,
    borderRadius: 13,
    justifyContent: 'center',
    alignItems: 'center'
  },
  shadow: {
    elevation: 2,
    shadowOffset: { width: 0, height: 0 },
    shadowColor: 'rgba(0,0,0,0.4)',
    shadowOpacity: 0.4,
    shadowRadius: 3
  },
  button_contain: {
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 20,
    height: 50,
    width: width - 80
  },
  edit_contain: {
    backgroundColor: '#f6f5fb',
    borderRadius: 10,
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 10
  }
})
