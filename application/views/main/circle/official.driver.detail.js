

import React, { Component } from 'react'
import { StyleSheet, Text, View, Image, TouchableOpacity, SafeAreaView, ScrollView, Dimensions } from 'react-native'
import { inject, observer } from 'mobx-react'
import Material from 'react-native-vector-icons/MaterialIcons'
import { DCColor, TextFont } from 'dacsee-utils'

const { width } = Dimensions.get('window')
@inject('app')
@observer
class NavigatorHeaderLeftButton extends Component {
  render() {
    return (
      <TouchableOpacity
        activeOpacity={0.7}
        style={{ top: 1, width: 54, paddingLeft: 8, justifyContent: 'center', alignItems: 'flex-start' }}
        onPress={() => this.props.navigation.goBack()}
      >
        <Material name={'keyboard-arrow-left'} size={30} color={DCColor.BGColor('primary-2')} />
      </TouchableOpacity>
    )
  }
}
const DetailCard = ({ img, title, caption }) => {
  const {
    dcCard,
    imgCont,
    dcImg,
    detailCont,
    titleCont,
    titleBox,
    fTitle,
    fBody
  } = styles
  return (
    <View style={dcCard}>
      <View style={imgCont}>
        <Image style={dcImg} source={img} />
      </View>
      <View style={detailCont}>
        <View style={titleCont}>
          <View style={titleBox}>
            <Text style={fTitle}>{title.toUpperCase()}</Text>
          </View>
        </View>
        <Text style={fBody}>{caption}</Text>
      </View>
    </View>
  )
}
@inject('app', 'circle')
@observer
export default class OfficialDriverDetailScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state
    const { headerOptions = {} } = params

    const maps = Object.assign({}, {
      drawerLockMode: 'locked-open',
      headerTitle: $.store.app.strings.official_driver,
      headerLeft: (
        <NavigatorHeaderLeftButton navigation={navigation} />
      )
    }, headerOptions)

    return maps
  }

  constructor(props) {
    super(props)
    this.state = {
      groups: []
    }
    this.groupData = props.circle.groupData
  }

  componentDidMount() {
    const groups = this.groupData.groups_official

    this.setState({
      groups: groups
    })
  }

  render() {
    const {
      mainCont,
      scrollCont,
      decoCont
    } = styles
    const { groups } = this.state

    return (
      <SafeAreaView style={mainCont}>
        <View style={decoCont} />
        <ScrollView contentContainerStyle={scrollCont}>
          {groups.map((group, key) => {
            const { name, description, avatars } = group

            return (
              <DetailCard
                key={key}
                img={{ uri: $.method.utils.getHeaderPicUrl(avatars) }}
                title={name}
                caption={description}
              />
            )
          })}
        </ScrollView>
      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  mainCont: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'white',
    position: 'relative'
  },
  scrollCont: {
    padding: 20
  },
  decoCont: {
    width: width,
    height: 100,
    backgroundColor: DCColor.BGColor('primary-1'),
    position: 'absolute',
    top: 0,
    left: 0
  },
  dcCard: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#F3F3F3',
    borderRadius: 5,
    elevation: 2,
    shadowOffset: { width: 0, height: 0 },
    shadowColor: 'rgba(0,0,0,0.4)',
    shadowOpacity: 0.4,
    shadowRadius: 3,
    marginBottom: 10
  },
  imgCont: {
    width: '40%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 25
  },
  dcImg: {
    height: 100,
    width: 100,
    resizeMode: 'contain',
    zIndex: 10
  },
  detailCont: {
    flex: 1,
    padding: 15,
    backgroundColor: 'white',
    borderTopRightRadius: 5,
    borderBottomRightRadius: 5,
    minHeight: 150
  },
  titleCont: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    marginBottom: 15,
  },
  titleBox: {
    borderBottomWidth: 3,
    borderBottomColor: DCColor.BGColor('primary-1'),
    paddingBottom: 7,
  },
  fTitle: {
    fontSize: TextFont.TextSize(22),
    fontWeight: '600',
    color: 'rgba(0, 0, 0, 0.75)',
    textAlign: 'left'
  },
  fBody: {
    fontSize: TextFont.TextSize(13),
    fontWeight: '500',
    color: 'rgba(0, 0, 0, 0.55)',
    paddingBottom: 15,
  }
})