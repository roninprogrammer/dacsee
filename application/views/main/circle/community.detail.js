

import React, { Component } from 'react'
import { StyleSheet, Text, View, Image, TouchableOpacity, SafeAreaView, ScrollView, Dimensions } from 'react-native'
import { inject, observer } from 'mobx-react'
import Material from 'react-native-vector-icons/MaterialIcons'
import Swiper from 'react-native-swiper'
import { DCColor, TextFont } from 'dacsee-utils'
import Resources from 'dacsee-resources'

const { width } = Dimensions.get('window')

@inject('app')
@observer
class NavigatorHeaderLeftButton extends Component {
  render() {
    return (
      <TouchableOpacity
        activeOpacity={0.7}
        style={{ top: 1, width: 54, paddingLeft: 8, justifyContent: 'center', alignItems: 'flex-start' }}
        onPress={() => this.props.navigation.goBack()}
      >
        <Material name={'keyboard-arrow-left'} size={30} color={DCColor.BGColor('primary-2')} />
      </TouchableOpacity>
    )
  }
}
@inject('app')
@observer
export default class CommunityDetailScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      name: '',
      description: '',
      tagline: '',
      avatars: [],
      type: ''
    }
  }

  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state
    const { headerOptions = {} } = params
    
    const maps = Object.assign({}, {
      drawerLockMode: 'locked-open',
      headerTitle: $.store.app.strings.group_detail,
      headerLeft: (
        <NavigatorHeaderLeftButton navigation={navigation} />
      )
    }, headerOptions)

    return maps
  }

  componentDidMount () {
    const { navigation } = this.props;

    const { name = '', description = '', tagline = '', avatars = [], type = 'reserved' } = navigation.state.params

    this.setState({
      name,
      description,
      tagline,
      avatars,
      type
    })
  }

  render() {
    const {
      mainCont,
      imgCont,
      dcImg,
      titleCont,
      fTitle,
      fCaption,
      fBody,
      bodyCont,
      btnCont,
      dcBtn,
      btnText
    } = styles
    const { app: { strings }, onGoing } = this.props
    const { name, description, tagline, avatars = [], type } = this.state

    return (
      <SafeAreaView style={mainCont}>
        <ScrollView>
          <Swiper
            style={imgCont}
            paginationStyle={{ background: 'red' }}
            showsPagination={true}
            horizontal={true}
            loop={false}
            dotColor='rgba(255, 255, 255, 0.45)'
            activeDotColor={DCColor.BGColor('primary-1')}
          >
            {
              avatars.length ? avatars.map((img, key) => (
                <Image style={dcImg} key={key} source={{uri: img.url}}/>
              )) : (
                <Image style={dcImg} source={Resources.image.img_placeholder}/>
              )
            }
          </Swiper>
          <View style={titleCont}>
            <Text style={fTitle}>{name}</Text>
            {
              type === 'reserved' ? (
                <Text style={fCaption}>{strings.official}</Text>
              ) : null
            }
            <Text style={fBody}>{tagline}</Text>
          </View>
          <View style={bodyCont}>
            <Text style={[fTitle, { textAlign: 'left', marginBottom: 15 }]}>{strings.description}</Text>
            <Text style={[fBody, { textAlign: 'left' }]}>{description}</Text>
          </View>
        </ScrollView>
        <View style={btnCont}>
          <TouchableOpacity style={dcBtn} onPress={() => this.props.navigation.goBack()}>
            <Text style={btnText}>{strings.okay}</Text>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  mainCont: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'white',
    position: 'relative'
  },
  imgCont: {
    height: 230
  },
  dcImg: {
    width: width,
    height: 230,
    resizeMode: 'cover',
  },
  titleCont: {
    backgroundColor: 'white',
    padding: 20,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  fTitle: {
    fontSize: TextFont.TextSize(20),
    fontWeight: '700',
    textAlign: "center",
    color: 'rgba(0, 0, 0, 0.85)',
    marginBottom: 3,
  },
  fCaption: {
    fontSize: TextFont.TextSize(12),
    fontWeight: '400',
    textAlign: "center",
    color: 'rgba(0, 0, 0, 0.35)',
    marginBottom: 15,
  },
  fBody: {
    fontSize: TextFont.TextSize(15),
    fontWeight: '400',
    textAlign: "center",
    color: 'rgba(0, 0, 0, 0.75)',
    marginBottom: 20,
    lineHeight: TextFont.TextSize(18)
  },
  bodyCont: {
    padding: 25,
    backgroundColor: '#f1f1f1',
    flex: 1
  },
  btnCont: {
    padding: 10,
  },
  dcBtn: {
    backgroundColor: DCColor.BGColor('primary-1'),
    borderRadius: 4,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 15,
    elevation: 2,
    shadowOffset: { width: 0, height: 0 },
    shadowColor: 'rgba(0,0,0,0.4)',
    shadowOpacity: 0.4,
    shadowRadius: 3
  },
  btnText: {
    fontSize: TextFont.TextSize(18),
    fontWeight: '700',
    color: 'rgba(0, 0, 0, 0.75)'
  },
})