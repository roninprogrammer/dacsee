

import React, { Component } from 'react'
import { TouchableOpacity } from 'react-native'
import { inject, observer } from 'mobx-react'
import Material from 'react-native-vector-icons/MaterialIcons'
import { DCColor } from 'dacsee-utils'
import { SelectDriverScreen } from './index'
import HeaderTab from '../../../components/header-tab'

@inject('app')
@observer
class NavigationHeader extends Component {
  render() {
    const { app = {} } = this.props
    const { control, strings } = app
    return (
      <HeaderTab active={control.header.indexSelect} onPress={index => control.header.selectNavigate(index)}>
        <HeaderTab.Item>{strings.group}</HeaderTab.Item>
        <HeaderTab.Item>{strings.friend}</HeaderTab.Item>
      </HeaderTab>
    )
  }
}
@inject('app')
@observer
class NavigatorHeaderLeftButton extends Component {
  render() {
    const { app } = this.props

    return (
      <TouchableOpacity
        activeOpacity={0.7}
        style={{ top: 1, width: 54, paddingLeft: 8, justifyContent: 'center', alignItems: 'flex-start' }}
        onPress={() => this.props.navigation.goBack()}
      >
        <Material name={'keyboard-arrow-left'} size={30} color={DCColor.BGColor('primary-2')} />
      </TouchableOpacity>
    )
  }
}
@inject('app')
@observer
export default class CircleSelectScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state
    const { headerOptions = {} } = params

    const maps = Object.assign({}, {
      drawerLockMode: 'locked-open',
      headerTitle: $.store.app.strings.select_friends_or_community,
      headerLeft: (
        <NavigatorHeaderLeftButton navigation={navigation} />
      )
    }, headerOptions)

    return maps
  }

  render() {
    const { app, navigation } = this.props
    return <SelectDriverScreen navigation={navigation} />
  }
}
