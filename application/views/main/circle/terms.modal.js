import React, { Component } from 'react'
import { Text, View, TouchableOpacity, Modal, StyleSheet, SafeAreaView, Image } from 'react-native'
import { Screen, TextFont, DCColor } from 'dacsee-utils'
import Resources from 'dacsee-resources'
const { height, width } = Screen.window

export default class TermsModal extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    const {
      dcBackdrop,
      dcCard,
      dcImg,
      fTitle,
      fBody,
      actionCont,
      dcBtn,
      btnText,
      agreeBtn
    } = styles
    const { onAgree, onHide, visible, info } = this.props
    const { tnc = '', avatars = [] } = info
    const { strings } = $.store.app

    return (
      <Modal
        animationType='fade'
        transparent={true}             
        visible={visible}    
        onRequestClose={onHide}  
      >
        <SafeAreaView style={dcBackdrop}>
          <View style={dcCard} >
            <Image style={dcImg} source={$.method.utils.getPicSource(avatars, false, Resources.image.img_placeholder)}/>
            <Text style={fTitle}>{strings.terms}</Text>
            <Text style={fBody}>{tnc}</Text>
            <View style={actionCont}>
              <TouchableOpacity style={dcBtn} onPress={onHide}>
                <Text style={btnText}>{strings.cancel.toUpperCase()}</Text>
              </TouchableOpacity>
              <TouchableOpacity style={[dcBtn, agreeBtn]} onPress={onAgree}>
                <Text style={btnText}>{strings.i_agree.toUpperCase()}</Text>
              </TouchableOpacity>
            </View>
          </View>
        </SafeAreaView>
      </Modal>
    )
  }
}

const styles = StyleSheet.create({
  dcBackdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.35)',
    height: height,
    width: width,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  dcCard: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    width: width*.8,
    borderRadius: 4,
    backgroundColor: 'white',
    padding: 10,
    elevation: 2,
    shadowOffset: { width: 0, height: 0 },
    shadowColor: 'rgba(0,0,0,0.4)',
    shadowOpacity: 0.4,
    shadowRadius: 3
  },
  dcImg: {
    width: width*.35,
    height: width*.35,
    borderRadius: 5,
    resizeMode: 'cover',
    marginVertical: 20,
    backgroundColor: 'rgba(0, 0, 0, 0.1)'
  },
  fTitle: {
    fontSize: TextFont.TextSize(18),
    fontWeight: '800',
    color: 'rgba(0, 0, 0, 0.85)',
    textAlign: 'center',
    marginBottom: 15,
  },
  fBody: {
    width: '80%',
    fontSize: TextFont.TextSize(14),
    lineHeight: TextFont.TextSize(16),
    fontWeight: '400',
    color: 'rgba(0, 0, 0, 0.65)',
    marginBottom: 25
  },
  actionCont: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  dcBtn: {
    backgroundColor: DCColor.BGColor('disable'),
    borderRadius: 4,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 15,
  },
  btnText: {
    fontSize: TextFont.TextSize(15),
    fontWeight: '700',
    color: 'rgba(0, 0, 0, 0.75)'
  },
  agreeBtn: {
    flex: 2,
    backgroundColor: DCColor.BGColor('primary-1'),
    marginLeft: 5
  }
})