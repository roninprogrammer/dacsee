import React, { Component } from 'react'
import { Text, View, TouchableOpacity, Modal, TextInput, StyleSheet, SafeAreaView, KeyboardAvoidingView } from 'react-native'
import { Screen, TextFont, DCColor } from 'dacsee-utils'
const { height, width } = Screen.window

export default class EnterPromoModal extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    const {
      dcBackdrop,
      bottomCont,
      enterPromoCont,
      dcInput,
      actionCont,
      dcBtn,
      btnText,
      applyBtn
    } = styles
    const { onApply, onHide, visible } = this.props
    const { strings } = $.store.app

    return (
      <Modal
        animationType='slide'
        transparent={true}             
        visible={visible}    
        onRequestClose={onHide}  
      >
        <SafeAreaView style={dcBackdrop} onPress={onHide}>
          <KeyboardAvoidingView behavior='position' contentContainerStyle={bottomCont}>
            <View style={enterPromoCont}>
              <Text>{strings.enter_promo_code}</Text>
              <TextInput style={dcInput} placeholder="X X X X X X"/>
            </View>
            <View style={actionCont}>
              <TouchableOpacity style={dcBtn} onPress={onHide}>
                <Text style={btnText}>{strings.cancel.toUpperCase()}</Text>
              </TouchableOpacity>
              <TouchableOpacity style={[dcBtn, applyBtn]} onPress={onApply}>
                <Text style={btnText}>{strings.apply.toUpperCase()}</Text>
              </TouchableOpacity>
            </View>
          </KeyboardAvoidingView>
        </SafeAreaView>
      </Modal>
    )
  }
}

const styles = StyleSheet.create({
  dcBackdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.35)',
    height: height,
    width: width,
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  bottomCont: {
    width: width,
    backgroundColor: 'white'
  },
  enterPromoCont: {
    padding: 25,
    borderBottomColor: 'rgba(0, 0, 0, 0.1)',
    borderBottomWidth: 1,
    paddingBottom: 10,
  },
  dcInput: {
    height: 45,
    fontSize: TextFont.TextSize(22),
    color: 'rgba(0, 0, 0, 0.75)',
    textAlign: 'left',
    letterSpacing: 50,
  },
  actionCont: {
    flexDirection: 'row',
    justifyContent: 'center',
    padding: 10,
    paddingBottom: 20
  },
  dcBtn: {
    backgroundColor: DCColor.BGColor('disable'),
    borderRadius: 4,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 15,
    flex: 1
  },
  btnText: {
    fontSize: TextFont.TextSize(18),
    fontWeight: '700',
    color: 'rgba(0, 0, 0, 0.75)'
  },
  applyBtn: {
    flex: 2,
    elevation: 2,
    shadowOffset: { width: 0, height: 0 },
    shadowColor: 'rgba(0,0,0,0.4)',
    shadowOpacity: 0.4,
    shadowRadius: 3,
    marginLeft: 10,
    backgroundColor: DCColor.BGColor('primary-1'),
  }
})