import React, { Component } from 'react'
import { Dimensions, StyleSheet, Text, View, Image, FlatList, TouchableOpacity, SafeAreaView, ScrollView, ActivityIndicator, Animated, StatusBar } from 'react-native'
import { inject, observer } from 'mobx-react'
import { observable } from 'mobx'
import Material from 'react-native-vector-icons/MaterialIcons'
import { DCColor, TextFont } from 'dacsee-utils'
import Resources from 'dacsee-resources'
import EnterPromoModal from './enter.promo.modal'
import TermsModal from './terms.modal'
import CircleFriendScreen from './circle.friends.select'
import { AnswersSrv } from '../../../global/service'
import ContentLoading from '../../modal/ContentLoading'

const { width, height } = Dimensions.get('window')

const OfficialGroupCard = ({ avatars, label, currency, price, selected, onNavigate, caption, onPress }) => {
  const {
    dcGroupCont,
    dcGroupBox,
    groupAvatar,
    groupLabelCont,
    groupLabel,
    groupCaptionCont,
    groupCaption,
    groupInfoIcon,
    groupPriceTag,
    groupPrice,
    actionBox,
    groupCheckBox
  } = styles
  const { strings } =  $.store.app

  return (
    <TouchableOpacity activeOpacity={1} style={dcGroupCont} onPress={onPress}>
      <View style={dcGroupBox}>
        <Image style={groupAvatar} source={avatars} />
        <View style={groupLabelCont}>
          <Text style={groupLabel}>{label.toUpperCase()}</Text>
          <View style={groupCaptionCont}>
            <Text style={groupCaption}>{caption}</Text>
            <TouchableOpacity onPress={onNavigate}>
              <Image style={groupInfoIcon} source={Resources.image.info_icon} />
            </TouchableOpacity>
          </View>
        </View>
        <View style={actionBox}>
          <View style={selected ? [groupCheckBox, { backgroundColor: DCColor.BGColor('green') }] : groupCheckBox}>
            {selected ? <Material name='check' color='white' size={18} /> : <Material name='check' color='rgba(255, 255, 255, 0.5)' size={18} />}
          </View>
          <View style={groupPriceTag}>
            {
              price === -1 ? (
                <ActivityIndicator size="small" color="#ffffff" />
              ) : !!price ? (
                <Text style={groupPrice}>{currency.toUpperCase()} {price}</Text>
              ) : (
                <Text style={groupPrice}>{strings.free.toUpperCase()}</Text>
              )
            }
          </View>
        </View>
      </View>
    </TouchableOpacity>
  )
}

@inject('app', 'circle', 'passenger')
@observer
export default class SelectDriverScreen extends Component {
  @observable isSearch: Boolean = false
  @observable searchValue: String = ''
  @observable searchGroups: Array = []

  _isMounted = false

  select_group = {}
  select_campaign = {}

  constructor(props) {
    super(props)
    this.state = {
      selectedTab: 'communityList',
      showEnterPromoModal: false,
      showTermsModal: false,
      promoStatus: 'default',
      official_groups: [],
      official_loading: true,
      campaign_groups: [],
      featuredCampaign: [],
      campaign_loading: false,
      assign_type: 'reserved_group',
      joygroup: [],
      featuredAdvance: false
    }
    this.groupData = props.circle.groupData
    this.friendData = props.circle.friendData
    this.confirmGroupSelect = props.circle.confirmGroupSelect
    this.confirmFriendSelect = props.circle.confirmFriendSelect
    this.setSelectedGroup = props.circle.setSelectedGroup
    this.getGroups = props.circle.getGroups
    this.getFareData = props.circle.getFareData
    this.getGroupsFares = props.circle.getGroupsFares
    this.getGroupsFaresV2 = props.circle.getGroupsFaresV2
    this.getCampaigns = props.circle.getCampaigns
    this.getInvitations = props.circle.getInvitations
    this.groupRequest = props.circle.groupRequest
    this.assign_type = props.passenger.assign_type
    this._scrollView = {}
    this.loading = true
  }

  async componentDidMount() {
    this._isMounted = true
    if (this._isMounted) this.setState({ official_groups: this.groupData.groups_official, campaign_loading: true, official_loading: true })

    await this.getFareData()
    if (this._isMounted) this.setState({ official_groups: this.groupData.groups_official, official_loading: false })

    await this.getGroupsFaresV2()
    if (this._isMounted) {
      this.setState({
        joygroup: this.groupData.groupsFares, 
        campaigns: this.groupData.campaigns, 
        campaign_loading: false, 
        featuredCampaign: this.groupData.featuredCampaigns 
      })
    }

    this.loading = false

    if (this.assign_type && this.groupData.select_group) {
      if (this.assign_type === 'reserved_group') {
        const group_id = this.groupData.select_group._id

        const found = this.groupData.groups_official.some((group, index) => {
          if (group._id === group_id) {
            this.onSelectGroup(index)
            this._scrollView.scrollTo({
              x: index * (width * .68 + 10),
              y: 0,
              animated: true
            })
            return true
          }
        })

        if (!found) this.onSelectGroup(0)

      } else if (this.assign_type === 'community_group') {
        const group_id = this.groupData.select_group._id

        const found = this.groupData.campaigns.some((campaign, index) => {
          if (campaign.group_id === group_id) {
            this.onSelectCampaign(index)

            return true
          }
        })

        if (!found) this.onSelectGroup(0)

      } else if (this.assign_type === 'selected_circle') {
        selectedTab = 'friendList'
        this.setState({ selectedTab: 'friendList' })
      }
    }
  }

  componentWillUnmount() {
    this._isMounted = false
  }

  onChangeText(text) {
    if (text === '') {
      this.isSearch = false
    } else {
      if (!this.isSearch) this.isSearch = true
      this.searchGroup(text)
    }
    this.searchValue = text
  }

  async searchGroup(value: String) {
    const { check_group } = this.props.circle.groupData
    try {
      const data = await $.method.session.Circle.Get(`v1/groupSearch?name=${value}&context=booking`)

      let groups = data.map(pipe => Object.assign({}, pipe, {
        checked: check_group.find((sub, index) => {
          if (sub._id === pipe._id) return check_group[index].checked
        })
      }))

      this.searchGroups = groups || []
    } catch (e) {
      this.searchGroups = []
    }
  }

  onPressCheck(itemData: Object) {
    if (this.isSearch) {
      let _groups = this.searchGroups.map(pipe => Object.assign({}, pipe, {
        checked: pipe._id === itemData._id && !pipe.checked
      }))
      this.searchGroups = _groups
    }
    this.props.circle.selectGroup(itemData)
  }

  _renderFooter = () => {
    const { strings } = this.props.app
    return (
      this.props.circle.getAllGroups.length === 0
        ? <View>
          <View style={{ marginTop: 108, alignItems: 'center' }}>
            <Image style={{ marginBottom: 18 }} source={require('../../../resources/images/friend-empty-state.png')} />
            <Text style={{ color: '#666', fontSize: 22, fontWeight: '600', textAlign: 'center', marginBottom: 6 }}>
              {strings.no_friend}
            </Text>
            <Text style={{ color: '#999', fontSize: 15, fontWeight: '400', textAlign: 'center' }}>
              {strings.clickto_add_friend}
            </Text>
          </View>
        </View> : null
    )
  }

  getTypeData(type) {
    let data = {}
    const { strings } = this.props.app
    switch (type) {
      case 'public':
        data.text = strings.public
        data.icon = Resources.image.public_group_icon
        data.iconColor = DCColor.BGColor('primary-1')
        break
      case 'reserved':
        data.text = strings.official
        data.icon = Resources.image.official_group_icon
        data.iconColor = '#797979'
        break
      default:
        data.text = strings.private
        data.icon = Resources.image.private_group_icon
        data.iconColor = '#797979'
    }
    return data
  }

  specialDriverCard = ({ item, index }) => {
    const { group_type = 'public', group_membersCount = 0, selected = false, currency, amount = 0, campaign_info = {} } = item
    const { name: campaignName, description, tnc, avatars, allowPromo = true, highlight, groupOnline, nearByDistance, assignBookingType, featured } = campaign_info
    const distanceCal = (nearByDistance/1000).toFixed(1);

    let opacityValue = 1

    if (!!this.loading) {
      AnswersSrv.logCustom(`Joy Driver - ${campaignName}`)

      opacityValue = new Animated.Value(0)

      Animated.timing(opacityValue, {
        toValue: 1,
        duration: 800,
        delay: index * 150
      }).start();
    }

    const {
      specialDriverCont,
      imgCont,
      specialImg,
      specialDriverBox,
      sGroupLabelBox,
      sGroupLabel,
      sGroupCaption,
      sBottomCont,
      tagCont,
      tagBox,
      dcIcon,
      dcCheckBox,
      tagText,
      sPriceTag,
      sPriceText,
      infoBox,
      infoIcon,
      onlineText,
      distanceCont
    } = styles
    const { strings } = $.store.app
    const getTypeData = this.getTypeData(group_type)
    const free = !!amount ? false : true

    return (
      <Animated.View key={index} style={[specialDriverCont, { opacity: opacityValue }]} >
        <TouchableOpacity
          style={imgCont}
          onPress={() => this.props.navigation.navigate('SpecialDriverDetailScreen', campaign_info)}
        >
          <Image style={specialImg} source={$.method.utils.getPicSource(avatars, false, Resources.image.img_placeholder)} />
          <View style={infoBox}> 
            <Image source={Resources.image.info_icon} style={infoIcon} /> 
        </View>
        </TouchableOpacity>
        <TouchableOpacity
          activeOpacity={1}
          style={!!highlight ? [specialDriverBox, { borderColor: DCColor.BGColor('primary-1'), borderWidth: 3 }] : specialDriverBox}
          onPress={() => {
            this.onSelectCampaign(index)
          }}
        >
          <View style={sGroupLabelBox}>
            <Text style={sGroupLabel} numberOfLines={1} ellipsizeMode='tail'>{campaignName}</Text>
            {description ?
              <Text style={sGroupCaption} numberOfLines={1} ellipsizeMode="tail">{description}</Text>
              : null
            }
          </View>
          <View style={sBottomCont}>
            <View style={tagCont}>
            { !!groupOnline &&
              <View style={tagBox}>
                  <Text style={onlineText}>{strings.online.toUpperCase()}</Text>
              </View>
            }
            { !!nearByDistance &&
              <View style={distanceCont}>
                <Text style={tagText}>~{distanceCal}KM</Text>  
              </View>
            }
            </View>
            <View style={sPriceTag}>
              {free ?
                <Text style={sPriceText}>{strings.free.toUpperCase()}</Text>
                : <Text style={sPriceText}>{currency || 'RM'} {amount || '0'}</Text>
              }
            </View>
          </View>
          <View style={selected ? [dcCheckBox, { backgroundColor: DCColor.BGColor('green') }] : dcCheckBox}>
            {selected ? <Material name='check' color='white' size={18} /> : <Material name='check' color='rgba(255, 255, 255, .5)' size={18} />}
          </View>
        </TouchableOpacity>
      </Animated.View>
    )
  }

  specialJoyCard = ({ item, index }) => {
    const { amount = 0, campaign_info, selected = false } = item
    const { name: joyName, description, avatars } = campaign_info

    let opacityValue = 1

    if (!!this.loading) {
      opacityValue = new Animated.Value(0)

      Animated.timing(opacityValue, {
        toValue: 1,
        duration: 800,
        delay: index * 150
      }).start();
    }

    const {
      specialDriverCont,
      imgCont,
      specialImg,
      specialDriverBox,
      sGroupLabelBox,
      sGroupLabel,
      sGroupCaption,
      sBottomCont,
      tagCont,
      dcCheckBox,
      sPriceTag,
      sPriceText,
      infoBox,
      infoIcon,
    } = styles
    const { strings } = $.store.app
    const free = !!amount ? false : true

    return (
      <Animated.View key={index} style={[specialDriverCont, { opacity: opacityValue }]} >
        <TouchableOpacity
          style={imgCont}
          onPress={() => this.props.navigation.navigate('SpecialDriverDetailScreen', campaign_info)}
        >
          <Image style={specialImg} source={$.method.utils.getPicSource(avatars, false, Resources.image.img_placeholder)} />
          <View style={infoBox}> 
            <Image source={Resources.image.info_icon} style={infoIcon} /> 
        </View>
        </TouchableOpacity>
        <TouchableOpacity
          activeOpacity={1}
          style={specialDriverBox}
          onPress={() => {
            this.onSelectJoyDriver(index)
          }}
        >
          <View style={sGroupLabelBox}>
            <Text style={sGroupLabel} numberOfLines={1} ellipsizeMode='tail'>{joyName}</Text>
            {description ?
              <Text style={sGroupCaption} numberOfLines={1} ellipsizeMode="tail">{description}</Text>
              : null
            }
          </View>
          <View style={sBottomCont}>
            <View style={tagCont}>
            </View>
            <View style={sPriceTag}>
              {free ?
                <Text style={sPriceText}>{strings.free.toUpperCase()}</Text>
                : <Text style={sPriceText}>RM {amount || '0'}</Text>
              }
            </View>
          </View>
          <View style={selected ? [dcCheckBox, { backgroundColor: DCColor.BGColor('green') }] : dcCheckBox}>
            {selected ? <Material name='check' color='white' size={18} /> : <Material name='check' color='rgba(255, 255, 255, .5)' size={18} />}
          </View>
        </TouchableOpacity>
      </Animated.View>
    )
  }

  specialFeaturedCard = ({ item, index }) => {
    const { group_type = 'public', group_membersCount = 0, selected = false, currency, amount = 0, campaign_info = {} } = item
    const { name: campaignName, description, tnc, avatars, allowPromo = true, highlight, groupOnline, nearByDistance, assignBookingType, featured } = campaign_info

    let opacityValue = 1

    if (!!this.loading) {
      opacityValue = new Animated.Value(0)

      Animated.timing(opacityValue, {
        toValue: 1,
        duration: 800,
        delay: index * 150
      }).start();
    }

    const {
      specialDriverCont,
      imgCont,
      specialImg,
      specialDriverBox,
      sGroupLabelBox,
      sGroupLabel,
      sGroupCaption,
      sBottomCont,
      tagCont,
      tagBox,
      dcCheckBox,
      sPriceTag,
      sPriceText,
      infoBox,
      infoIcon,
      onlineText,
    } = styles
    const { strings } = $.store.app
    const free = !!amount ? false : true

    return (
      <Animated.View key={index} style={[specialDriverCont, { opacity: opacityValue }]} >
        <TouchableOpacity
          style={imgCont}
          onPress={() => this.props.navigation.navigate('SpecialDriverDetailScreen', campaign_info)}
        >
          <Image style={specialImg} source={$.method.utils.getPicSource(avatars, false, Resources.image.img_placeholder)} />
          <View style={infoBox}> 
            <Image source={Resources.image.info_icon} style={infoIcon} /> 
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          activeOpacity={1}
          style={!!highlight ? [specialDriverBox, { borderColor: DCColor.BGColor('primary-1'), borderWidth: 3 }] : specialDriverBox}
          onPress={() => {
            this.onSelectFeaturedJoyDriver(index)
          }}
        >
          <View style={sGroupLabelBox}>
            <Text style={sGroupLabel} numberOfLines={1} ellipsizeMode='tail'>{campaignName}</Text>
            {description ?
              <Text style={sGroupCaption} numberOfLines={1} ellipsizeMode="tail">{description}</Text>
              : null
            }
          </View>
          <View style={sBottomCont}>
            <View style={tagCont}>
            {
              !!assignBookingType && assignBookingType === 'advanced' && 
              <View style={[tagBox, { backgroundColor:  DCColor.BGColor('primary-1')}]}>
                <Text style={onlineText}>{strings.select_driver_adv_booking_tag.toUpperCase()}</Text>
            </View>
            }
            </View>
            <View style={sPriceTag}>
              {free ?
                <Text style={sPriceText}>{strings.free.toUpperCase()}</Text>
                : <Text style={sPriceText}>{currency || 'RM'} {amount || '0'}</Text>
              }
            </View>
          </View>
          <View style={selected ? [dcCheckBox, { backgroundColor: DCColor.BGColor('green') }] : dcCheckBox}>
            {selected ? <Material name='check' color='white' size={18} /> : <Material name='check' color='rgba(255, 255, 255, .5)' size={18} />}
          </View>
        </TouchableOpacity>
      </Animated.View>
    )
  }

  onSelectTab(selectedTab) {
    this.setState({ selectedTab })
  }

  onModalShow(modal) {
    this.setState({
      [modal]: true
    })
  }

  onModalHide(modal) {

    this.setState({
      [modal]: false
    })
  }

  onApplyPromo() {
    this.setState({
      promoStatus: 'success'
    }, () => this.onModalHide('showEnterPromoModal'))
  }

  onAgreeTerms() {
    this.onModalHide('showTermsModal')
    this.setSelectedGroup(select_campaign, this.state.featuredAdvance)
  }

  onRemovePromo() {
    this.setState({
      promoStatus: 'default'
    })
  }

  onSelectGroup(index) {
    const { groups_official = [], campaigns, groupsFares, featuredCampaigns } = this.groupData

    this.groupData.groups_official = groups_official.map((pipe, key) => Object.assign({}, pipe, {
      selected: index === key
    }))

    this.groupData.campaigns = campaigns.map((pipe, key) => Object.assign({}, pipe, {
      selected: false
    }))

    this.groupData.groupsFares = groupsFares.map((pipe, key) => Object.assign({}, pipe, {
      selected: false
    }))

    this.groupData.featuredCampaigns = featuredCampaigns.map((pipe, key) => Object.assign({}, pipe, {
      selected: false
    }))

    this.setState({
      official_groups: this.groupData.groups_official,
      campaigns: this.groupData.campaigns,
      assign_type: 'reserved_group',
      joygroup: this.groupData.groupsFares,
      featuredCampaign: this.groupData.featuredCampaigns
    })

    this._scrollView.scrollTo({
      x: index * (width * .68 + 10),
      y: 0,
      animated: true
    })
  }

  onSelectCampaign(index) {
    const { groups_official = [], campaigns, groupsFares, featuredCampaigns } = this.groupData

    this.groupData.groups_official = groups_official.map((pipe, key) => Object.assign({}, pipe, {
      selected: false
    }))

    this.groupData.campaigns = campaigns.map((pipe, key) => Object.assign({}, pipe, {
      selected: index === key
    }))

    this.groupData.groupsFares = groupsFares.map((pipe, key) => Object.assign({}, pipe, {
      selected: false
    }))

    this.groupData.featuredCampaigns = featuredCampaigns.map((pipe, key) => Object.assign({}, pipe, {
      selected: false
    }))

    this.setState({
      official_groups: this.groupData.groups_official,
      campaigns: this.groupData.campaigns,
      assign_type: 'community_group',
      selected_campaign: campaigns[index].campaign_info,
      joygroup: this.groupData.groupsFares,
      featuredCampaign: this.groupData.featuredCampaigns
    })
  }

  onSelectJoyDriver(index) {
    const { groups_official = [], campaigns, groupsFares, featuredCampaigns } = this.groupData

    this.groupData.groups_official = groups_official.map((pipe, key) => Object.assign({}, pipe, {
      selected: false
    }))

    this.groupData.campaigns = campaigns.map((pipe, key) => Object.assign({}, pipe, {
      selected: false
    }))

    this.groupData.groupsFares = groupsFares.map((pipe, key) => Object.assign({}, pipe, {
      selected: index === key
    }))

    this.groupData.featuredCampaigns = featuredCampaigns.map((pipe, key) => Object.assign({}, pipe, {
      selected: false
    }))

    this.setState({
      official_groups: this.groupData.groups_official,
      campaigns: this.groupData.campaigns,
      assign_type: 'community_group',
      selected_campaign: groupsFares[index].campaign_info,
      joygroup: this.groupData.groupsFares,
      featuredCampaign: this.groupData.featuredCampaigns
    })
  }

  onSelectFeaturedJoyDriver(index) {
    const { groups_official = [], campaigns, groupsFares, featuredCampaigns } = this.groupData

    this.groupData.groups_official = groups_official.map((pipe, key) => Object.assign({}, pipe, {
      selected: false
    }))

    this.groupData.campaigns = campaigns.map((pipe, key) => Object.assign({}, pipe, {
      selected: false
    }))

    this.groupData.groupsFares = groupsFares.map((pipe, key) => Object.assign({}, pipe, {
      selected: false
    }))

    this.groupData.featuredCampaigns = featuredCampaigns.map((pipe, key) => Object.assign({}, pipe, {
      selected: index === key
    }))

    this.setState({
      official_groups: this.groupData.groups_official,
      campaigns: this.groupData.campaigns,
      assign_type: 'community_group',
      selected_campaign: featuredCampaigns[index].campaign_info,
      joygroup: this.groupData.groupsFares,
      featuredCampaign: this.groupData.featuredCampaigns
    })
  }

  onConfirm() {
    const { assign_type = 'reserved_group', selectedTab, official_groups, campaigns, joygroup, featuredCampaign } = this.state

    if (selectedTab === 'communityList') {
      if (assign_type === 'reserved_group') {
        const selectedGroup = official_groups.filter((group) => group.selected)

        if (selectedGroup.length) {
          select_group = selectedGroup[0]
          this.setSelectedGroup(select_group)
        } else {
          // Throw Error
          $.method.utils.toastLong($.store.app.strings.vehicle_category_select_empty_toast)
        }

      } else if (assign_type === 'community_group') {
        const selectedCampaign = campaigns.filter((campaign) => campaign.selected)
        const selectedJoy = joygroup.filter((group) => group.selected)
        const selectedFeaturedCampaign = featuredCampaign.filter((featured) => featured.selected)

        if (selectedFeaturedCampaign.length && !!selectedFeaturedCampaign[0].campaign_info.assignBookingType && selectedFeaturedCampaign[0].campaign_info.assignBookingType === 'advanced'){
          this.setState({featuredAdvance: true})
        }

        if (selectedCampaign.length || selectedJoy.length || selectedFeaturedCampaign.length) {
          select_campaign = selectedCampaign[0] || selectedJoy[0] || selectedFeaturedCampaign[0]

          if (!!select_campaign && !!select_campaign.campaign_info) {
            if (!!select_campaign.campaign_info.promptTNC) {
              this.onModalShow('showTermsModal')
            } else {
              this.setSelectedGroup(select_campaign, this.state.featuredAdvance)
            }
          } else {
            this.setSelectedGroup(select_campaign)
          }
          
        } else {
          // Throw Error
          $.method.utils.toastLong($.store.app.strings.vehicle_category_select_empty_toast)
        }

      }
    } else if (selectedTab === 'friendList') {
      const selectedFriend = this.friendData.check_friends.filter(item => item.checked)

      if (selectedFriend.length) {
        this.confirmFriendSelect()
      } else {
        $.method.utils.toastLong($.store.app.strings.select_least_one)
      }
    }
  }


  render() {
    const {
      mainCont,
      tabCont,
      tabItem,
      fCaption,
      activeFont,
      dcTriangle,
      actionCont,
      buttonCont,
      dcBtn,
      btnText,
      bodyCont,
      titleCont,
      fTitle,
      dcHorizontalScroll,
      emptyCont,
      emptyBox,
      emptyImg,
      dataCont,
      emptyTitle,
      emptyCaption,
      dcVerticalScroll
    } = styles
    const { selectedTab, showEnterPromoModal, showTermsModal, promoStatus, official_groups = [], official_loading = false, campaigns = [], selected_campaign, campaign_loading, joygroup, featuredCampaign = [] } = this.state
    const { app: { strings }, navigation } = this.props
    const { check_group = [] } = this.groupData

    // const _check = this.isSearch ? this.searchGroups.filter(item => item.checked) : check_group.filter(item => item.checked)

    return (
      <SafeAreaView style={mainCont}>
        <StatusBar
          animated={true}
          hidden={false}
          backgroundColor={DCColor.BGColor('primary-1')}
          barStyle={'dark-content'} />
        <View style={tabCont}>
          <TouchableOpacity style={tabItem} onPress={() => this.onSelectTab('communityList')}>
            <Text style={selectedTab === 'communityList' ? [fCaption, activeFont] : fCaption}>{strings.group.toUpperCase()}</Text>
            {selectedTab === 'communityList' && (
              <View style={dcTriangle} />
            )}
          </TouchableOpacity>
          <TouchableOpacity style={tabItem} onPress={() => this.onSelectTab('friendList')}>
            <Text style={selectedTab === 'friendList' ? [fCaption, activeFont] : fCaption}>{strings.friend.toUpperCase()}</Text>
            {selectedTab === 'friendList' && (
              <View style={dcTriangle} />
            )}
          </TouchableOpacity>
        </View>
        {selectedTab === 'communityList' && (
          <View style={bodyCont}>
            <View style={titleCont}>
              <Text style={fTitle}>{strings.standard_driver_nearby}</Text>
            </View>
            <View>
              <ScrollView
                contentContainerStyle={dcHorizontalScroll}
                horizontal={true}
                alwaysBounceHorizontal={true}
                decelerationRate="fast"
                snapToInterval={width * .68 + 10}
                snapToAlignment="start"
                ref={(ref) => this._scrollView = ref}
              >
                {official_groups.map((group, key) => {
                  const { name = "", tagline = "", selected = false, avatars = [], amount = 0 } = group

                  return (
                    <OfficialGroupCard
                      key={key}
                      avatars={{ uri: $.method.utils.getHeaderPicUrl(avatars) }}
                      label={name}
                      currency="RM" // country_info.currency
                      price={official_loading ? -1 : amount} // amount
                      caption={tagline}
                      onNavigate={() => navigation.navigate('OfficialDriverDetailScreen')}
                      selected={selected} // selected
                      onPress={() => {
                        this.onSelectGroup(key)
                      }}
                    />
                  )
                })}
              </ScrollView>
            </View>
            <ScrollView contentContainerStyle={dcVerticalScroll} >
              <View style={{ flex: 1 }}>
                {!!campaigns && campaigns.length ? (
                <View style={{ flex: 1 }}>
                  <View style={titleCont}>
                    <Text style={fTitle}>{strings.joy_drivers_nearby}</Text>
                  </View>
                  <FlatList
                    removeClippedSubviews={false}
                    keyExtractor={(item, index) => item.campaign_id}
                    data={campaigns}
                    renderItem={this.specialDriverCard}
                  />
                  </View>
                  ) : !!campaign_loading ?
                  <View style={{ flex: 1 }}>
                    <View style={titleCont}>
                      <Text style={fTitle}>{strings.confirm_wait_order}</Text>
                    </View>
                    <View style={emptyCont}>
                      <View style={emptyBox}>
                        <ContentLoading avatar={'square'} title paragraph rows={3} loading={this.state.campaign_loading}/>
                      </View>
                    </View>
                  </View>
                  : (!!campaigns && !campaigns.length) && (!!joygroup && !joygroup.length) && (!!featuredCampaign && !featuredCampaign.length) ? 
                  <View style={{ flex: 1}}>
                    <View style={titleCont}>
                      <Text style={fTitle}>{strings.joy_drivers_nearby}</Text>
                    </View>
                    <View style={emptyCont}>
                      <View style={emptyBox}>
                        <Image style={emptyImg} source={Resources.image.emptystate_no_special_driver} />
                        <View style={[dataCont, {alignItems: 'flex-start', justifyContent: 'flex-start', flex: 1}]}>
                          <Text style={emptyCaption}>{strings.no_joy_drivers_nearby_caption}</Text>
                        </View>
                      </View>
                    </View>
                  </View>
                  : null
                } 

                {!!joygroup && joygroup.length ? (
                <View style ={{flex: 1}}>
                  <View style={titleCont}>
                    <Text style={fTitle}>{strings.select_driver_title_my_joy_driver}</Text>
                  </View>
                  <View style={{ flex: 1 }}>
                    <FlatList
                      removeClippedSubviews={false}
                      keyExtractor={(item, index) => item.campaign_id}
                      data={joygroup}
                      renderItem={this.specialJoyCard}
                    />
                  </View>
                </View>
                ) : null
                }

                {!!featuredCampaign && featuredCampaign.length ? (
                <View style={{flex: 1}}>
                  <View style={titleCont}>
                    <Text style={fTitle}>{strings.select_driver_title_featured_joy_drivers}</Text>
                  </View>
                  <View style={{ flex: 1 }}>
                    <FlatList
                      removeClippedSubviews={false}
                      keyExtractor={(item, index) => item.campaign_id}
                      data={featuredCampaign}
                      renderItem={this.specialFeaturedCard}
                    />
                  </View> 
                </View>
                ) : null
                }
             
              </View>
            </ScrollView>
          </View>
        )}
        {selectedTab === 'friendList' && (<CircleFriendScreen />)}
        <View style={actionCont}>
          <View style={buttonCont}>
            <TouchableOpacity style={dcBtn} onPress={() => this.onConfirm()}>
              <Text style={btnText}>{strings.confirm.toUpperCase()}</Text>
            </TouchableOpacity>
          </View>
        </View>
        <EnterPromoModal
          visible={showEnterPromoModal}
          onHide={() => this.onModalHide('showEnterPromoModal')}
          onApply={() => this.onApplyPromo()}
        />
        {!!showTermsModal ?
          <TermsModal
            visible={showTermsModal}
            info={selected_campaign}
            onHide={() => this.onModalHide('showTermsModal')}
            onAgree={() => this.onAgreeTerms()}
          />
          : null
        }
      </SafeAreaView >
    )
  }
}

const styles = StyleSheet.create({
  mainCont: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'white',
    position: 'relative'
  },
  tabCont: {
    backgroundColor: DCColor.BGColor('primary-1'),
    paddingHorizontal: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  tabItem: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    paddingVertical: 15,
    position: 'relative'
  },
  fCaption: {
    fontSize: TextFont.TextSize(13),
    color: 'rgba(0, 0, 0, 0.35)',
    fontWeight: '500',
    textAlign: "center"
  },
  activeFont: {
    color: DCColor.BGColor('primary-2'),
    fontWeight: '700'
  },
  dcTriangle: {
    position: 'absolute',
    bottom: 0,
    width: 0,
    height: 0,
    backgroundColor: 'transparent',
    borderStyle: 'solid',
    borderLeftWidth: 7,
    borderRightWidth: 7,
    borderBottomWidth: width * .03,
    borderLeftColor: 'transparent',
    borderRightColor: 'transparent',
    borderBottomColor: '#f2f2f2'
  },
  bodyCont: {
    flex: 1,
    backgroundColor: '#f2f2f2',
    padding: 5
  },
  actionCont: {
    backgroundColor: 'white',
  },
  priceCont: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.07)',
    borderBottomWidth: 1,
    borderTopWidth: 1,
    padding: 10,
    borderColor: 'rgba(0, 0, 0, 0.05)'
  },
  successCont: {
    backgroundColor: 'rgba(126, 211, 33, .2)'
  },
  errorCont: {
    backgroundColor: 'rgba(250, 0 , 0, .2)'
  },
  closeCont: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: 20,
    height: 20,
    borderRadius: 35,
    backgroundColor: 'rgba(0, 0, 0, 0.25)',
    marginRight: 5
  },
  closeIcon: {
    width: 10,
    height: 10,
    resizeMode: 'contain',
    tintColor: 'rgba(0, 0, 0, 0.75)'
  },
  promoCont: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexDirection: 'row',
  },
  promoBox: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    flexDirection: 'column',
  },
  promoText: {
    fontSize: TextFont.TextSize(11),
    color: 'rgba(0, 0, 0, 0.75)',
    fontWeight: '500',
    textAlign: 'left'
  },
  discountText: {
    fontSize: TextFont.TextSize(11),
    color: 'rgba(0, 0, 0, 0.45)',
    fontWeight: '400',
    textAlign: 'right',
    paddingRight: 5,
    textDecorationLine: 'line-through'
  },
  priceText: {
    fontSize: TextFont.TextSize(15),
    color: 'rgba(0, 0, 0, 0.95)',
    fontWeight: '700',
    textAlign: 'right',
    paddingRight: 5,
  },
  buttonCont: {
    padding: 10,
  },
  dcBtn: {
    backgroundColor: DCColor.BGColor('primary-1'),
    borderRadius: 4,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 15,
    elevation: 2,
    shadowOffset: { width: 0, height: 0 },
    shadowColor: 'rgba(0,0,0,0.4)',
    shadowOpacity: 0.4,
    shadowRadius: 3
  },
  btnText: {
    fontSize: TextFont.TextSize(18),
    fontWeight: '700',
    color: 'rgba(0, 0, 0, 0.75)'
  },
  titleCont: {
    padding: 10,
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start'
  },
  fTitle: {
    fontSize: TextFont.TextSize(18),
    fontWeight: '700',
    color: 'rgba(0, 0, 0, 0.75)',
    textAlign: 'left'
  },
  dcHorizontalScroll: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start'
  },
  dcVerticalScroll: {
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent:'flex-start'
  },
  dcGroupCont: {
    borderRadius: 6,
    minWidth: width * .68,
    margin: 5,
    elevation: 2,
    shadowOffset: { width: 0, height: 0 },
    shadowColor: 'rgba(0,0,0,0.4)',
    shadowOpacity: 0.4,
    shadowRadius: 3
  },
  dcGroupBox: {
    backgroundColor: 'white',
    borderRadius: 6,
    padding: 5,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    overflow: 'hidden',
    position: 'relative',
  },
  groupAvatar: {
    height: 45,
    width: 45,
    resizeMode: 'contain',
    zIndex: 10,
    marginHorizontal: 10
  },
  groupLabelCont: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  groupLabel: {
    fontSize: TextFont.TextSize(15),
    fontWeight: '900',
    color: 'rgba(0, 0, 0, 0.95)',
    textAlign: 'center',
  },
  groupCaptionCont: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  groupCaption: {
    fontSize: TextFont.TextSize(14),
    fontWeight: '400',
    color: 'rgba(0, 0, 0, 0.75)',
    textAlign: 'center',
    marginRight: 5,
  },
  groupInfoIcon: {
    width: TextFont.TextSize(13),
    height: TextFont.TextSize(13),
    resizeMode: 'contain',
    tintColor: 'rgba(0, 0, 0, 0.25)',
  },
  actionBox: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'flex-end'
  },
  groupCheckBox: {
    height: 25,
    width: 25,
    borderRadius: 20,
    backgroundColor: '#999999',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 10,
    marginBottom: 10
  },
  groupPriceTag: {
    borderRadius: 6,
    borderBottomLeftRadius: 15,
    backgroundColor: DCColor.BGColor('primary-1'),
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 5,
    paddingHorizontal: 15,
    minWidth: 30,
  },
  groupPrice: {
    fontSize: TextFont.TextSize(14),
    color: 'rgba(0, 0, 0, 0.95)',
    fontWeight: '800',
    textAlign: 'right',
    paddingLeft: 5,
  },
  dcRadarCont: {
    position: 'absolute',
    right: -width * .25,
  },
  dcRadar1: {
    width: width * .6,
    height: width * .6,
    borderRadius: width * .35,
    backgroundColor: 'rgba(255, 196, 17, .4)',
    opacity: .5,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  dcRadar2: {
    width: width * .45,
    height: width * .45,
    borderRadius: width * .35,
    backgroundColor: 'rgba(255, 196, 17, .4)',
    zIndex: 5
  },
  specialDriverCont: {
    position: 'relative',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: 15,
    minHeight: 100,
    width,
    overflow: 'hidden',
  },
  imgCont: {
    width: 95,
    height: 110,
    borderRadius: 5,
    marginRight: 10,
    borderColor: 'rgba(0, 0, 0, 0.05)',
    borderWidth: 1,
    position: 'absolute',
    left: 10,
    elevation: 6,
    shadowOffset: { width: 0, height: 0 },
    shadowColor: 'rgba(0,0,0,0.4)',
    shadowOpacity: 0.2,
    shadowRadius: 3,
    zIndex: 10,
    backgroundColor: 'white'
  },
  specialImg: {
    borderRadius: 5,
    width: '100%',
    height: '100%',
    resizeMode: 'cover'
  },
  specialDriverBox: {
    backgroundColor: 'white',
    flex: 1,
    borderRadius: 3,
    flexDirection: 'column',
    padding: 5,
    paddingLeft: 107,
    justifyContent: 'center',
    alignItems: 'flex-start',
    elevation: 2,
    shadowOffset: { width: 0, height: 0 },
    shadowColor: 'rgba(0,0,0,0.4)',
    shadowOpacity: 0.2,
    shadowRadius: 3,
    position: 'relative',
    minHeight: 90
  },
  sGroupLabelBox: {
    paddingTop: 5,
    paddingRight: 30,
    flex: 1
  },
  sGroupLabel: {
    fontSize: TextFont.TextSize(14),
    fontWeight: '600',
    color: 'rgba(0, 0, 0, 0.95)',
    textAlign: 'left',
  },
  sGroupCaption: {
    fontSize: TextFont.TextSize(12),
    fontWeight: '400',
    color: 'rgba(0, 0, 0, 0.5)',
    textAlign: 'left',
  },
  sBottomCont: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
  },
  tagCont: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingBottom: 5
  },
  tagBox: {
    borderRadius: 3,
    padding: 2,
    paddingHorizontal: 5,
    backgroundColor: DCColor.BGColor('green'),
    marginRight: 5,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  dcCheckBox: {
    height: 25,
    width: 25,
    borderRadius: 20,
    backgroundColor: '#999999',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 5,
    zIndex: 10,
    position: 'absolute',
    top: 5,
    right: 0
  },
  dcIcon: {
    width: 10,
    height: 10,
    marginRight: 5,
    resizeMode: 'contain'
  },
  tagText: {
    fontSize: TextFont.TextSize(11),
    fontWeight: '400',
    color: 'rgba(0, 0, 0, 0.75)',
    textAlign: 'left',
  },
  sPriceTag: {
    backgroundColor: DCColor.BGColor('primary-1'),
    borderRadius: 6,
    borderBottomLeftRadius: 15,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 5,
    paddingHorizontal: 15,
    minWidth: 50,
  },
  sPriceText: {
    fontSize: TextFont.TextSize(14),
    color: 'rgba(0, 0, 0, 0.95)',
    fontWeight: '800',
    textAlign: 'right',
    paddingLeft: 5,
  },
  infoBox: {
    position: 'absolute',
    width: 15,
    height: 15,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    marginLeft: 75,
    padding: 2
  },
  infoIcon: {
    width: '100%',
    height: '100%',
    resizeMode: 'contain',
    tintColor: 'white',
  },
  emptyCont: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingLeft: 10,
    paddingRight: 20,
    flex: 1,
    width,
    height: 100
  },
  emptyBox: {
    elevation: 2,
    shadowOffset: { width: 0, height: 0 },
    shadowColor: 'rgba(0,0,0,0.4)',
    shadowOpacity: 0.2,
    shadowRadius: 3,
    borderRadius: 6,
    backgroundColor: 'white',
    width: '100%',
    height: '95%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 10,
    padding: 10
  },
  emptyImg: {
    height: height * .13,
    width: height * .13,
    resizeMode: 'contain',
  },
  dataCont: {
    width: width * .65,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  emptyTitle: {
    fontSize: TextFont.TextSize(16),
    fontWeight: '700',
    color: 'rgba(0, 0, 0, 0.75)',
    textAlign: 'center',
    marginBottom: 2
  },
  emptyCaption: {
    fontSize: TextFont.TextSize(13),
    fontWeight: '500',
    color: 'rgba(0, 0, 0, 0.45)',
    marginTop: 2
  },
  onlineText: {
    fontSize: TextFont.TextSize(11),
    fontWeight: '400',
    color: 'black',
    textAlign: 'left',
  },
  distanceCont: {
    padding: 2,
    paddingHorizontal: 5,
    marginRight: 5,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },

})