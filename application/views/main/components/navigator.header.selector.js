import React, { Component } from 'react'
import { Text, View, TouchableOpacity, ScrollView, Platform } from 'react-native'
import { TextFont, DCColor } from 'dacsee-utils'

const DEFAULT_MENU = [
  {
    title: 'mycircle', key: 'circle'
  },
]

export default class HeaderSection extends Component {

  render() {
    const { selected_type, i18n } = this.props

    return (
      <View style={[
        { position: 'absolute', top: 0, left: 0, right: 0, height: 34 },
        Platform.select({
          ios: { backgroundColor: DCColor.BGColor('primary-1') },
          android: { backgroundColor: DCColor.BGColor('primary-1') }
        }),
        { shadowOffset: { width: 0, height: 2 }, shadowColor: '#999', shadowOpacity: .5, shadowRadius: 3 }
      ]}>
        <ScrollView showsHorizontalScrollIndicator={false} horizontal={true} style={{ height: 34 }}>
          {
            DEFAULT_MENU.map((pipe, index) => (
              <TouchableOpacity activeOpacity={1} onPress={() => this.props.dispatch(booking.passengerSetValue({ type: pipe.key }))} key={index} style={{ paddingHorizontal: 10, marginHorizontal: 10, justifyContent: 'center', alignItems: 'center' }}>
                <Text style={selected_type === pipe.key ? { color: 'white', fontSize: TextFont.TextSize(13), fontWeight: '600' } : { color: 'white', fontSize: TextFont.TextSize(13), fontWeight: '600', opacity: .7 }}>
                  {i18n[`${pipe.title}`]}
                </Text>
              </TouchableOpacity>
            ))
          }
        </ScrollView>
      </View>
    )
  }
}
