import React, { Component } from 'react'
import { Text, View, Animated } from 'react-native'
import { inject, observer } from 'mobx-react'
import Lottie from 'lottie-react-native'
import InteractionManager from 'InteractionManager'

@inject('app') @observer
class NavigatorComponent extends Component {
  constructor (props) {
    super(props)
    this.state = {}
    this.loop = new Animated.Value(0)
  }

  async componentDidMount () {
    await InteractionManager.runAfterInteractions()
    Animated.loop(Animated.timing(this.loop, { toValue: 1, fromValue: 0, duration: 5200 })).start()
  }

  static getDerivedStateFromProps (nextProps, prevState) {
    return null
  }

  render () {
    return (
      <View style={{ flex: 1, backgroundColor: 'white', justifyContent: 'center', alignItems: 'center' }}>
        <View style={{ justifyContent: 'center', alignItems: 'center', width: 200, height: 200, top: -20 }}>
          <Lottie style={{ width: 200, height: 200 }} progress={this.loop} source={require('../res/animation/location.json')} />
        </View>
        <View style={{ top: -45, justifyContent: 'center', alignItems: 'center' }}>
          <Text style={{ color: '#333', fontSize: 14, marginBottom: 10 }}>{this.props.app.strings.get_location}</Text>
        </View>
      </View>
    )
  }
}

export default NavigatorComponent
