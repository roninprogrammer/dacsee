import React, { Component } from 'react'
import { Text, View, Animated, StyleSheet, Image, TouchableOpacity, TextInput, RefreshControl, FlatList, Linking, Clipboard } from 'react-native'
import { NavigationActions } from 'react-navigation'
import { Screen, DCColor, TextFont } from 'dacsee-utils'
import Material from 'react-native-vector-icons/MaterialIcons'
import Resources from 'dacsee-resources'
import { inject, observer } from 'mobx-react'
import { observable } from 'mobx'
const { width } = Screen.window

const AnimatedFlatList = Animated.createAnimatedComponent(FlatList)
@inject('app', 'downLine', 'account')
@observer
export default class DownLineTotalScreen extends Component {
  @observable isSearch: Boolean = false
  @observable searchValue: String = ''
  static navigationOptions = ({ navigation }) => {
    const { strings } = $.store.app
    return {
      drawerLockMode: 'locked-closed',
      title: strings.downline_list,
      headerStyle: {
        backgroundColor: DCColor.BGColor('primary-1'),
        shadowColor: 'transparent',
        shadowOpacity: 0,
        borderBottomWidth: 0,
        borderBottomColor: 'transparent',
        elevation: 0
      }
    }
  }

  constructor(props) {
    super(props)
    this.friendData = props.downLine.friendData
    this.getTeamList = props.downLine.getTeamList
    this.state = {
      loading: false,
      data: {}
    }
  }

  async componentDidMount() {
    // await this.getTeamList()
    // const { teamList } = this.friendData
    // console.log(teamList, '============>')
    // this._fetchData()
  }
  fetchdUser = (isSearch) => {
    if (!this.isSearch) {
      return this.friendData.teamList
    }
  }

  async _fetchData(index = 0) {
    this.setState({
      loading: true
    })
    const { strings } = this.props
    try {
      // 获取列表
      const data = await $.method.session.User.Get('v1/downline/level/0')
      this.setState({
        loading: false,
        data: data
      })
    } catch (e) {
      // this.props.dispatch(application.showMessage(strings.unable_connect_server_pls_retry_later))
      this.setState({
        loading: false
      })
    }
  }
  goDownLineList(lve) {
    this.props.navigation.navigate('DownLineList', { level: lve })
  }
  onChangeText = (text) => {
    if (text === '') {
      this.isSearch = false
    } else {
      if (!this.isSearch) this.isSearch = true
      this.props.downLine.getSearchFriends(text)
    }
    this.searchValue = text
  }

  _renderItemView = ({ item, index }) => {
    return (
      <View style={{ backgroundColor: '#fff', marginLeft: 10, marginRight: 10, borderRadius: 6, height: 80, flexDirection: 'row', marginBottom: 10, paddingHorizontal: 15, flex: 1 }}>
        <View style={{ flex: 0.5, justifyContent: 'center', alignItems: 'center' }}>
          <Image style={{ height: 60, width: 60, borderRadius: 30, marginRight: 10 }} source={{ uri: item.avatars[0].url }} />
        </View>
        <View style={{ flex: 2, justifyContent: 'center', paddingVertical: 10 }}>
          <View style={{ flexDirection: 'row', width: width - 20 - 90, justifyContent: 'space-between', marginRight: 5 }}>
            {/* <View style={{ height: 30, justifyContent: 'center' ,flex:1}}> */}
            <Text numberOfLines={2} style={{ flex: 1 }}>{item.fullName}</Text>
            {/* </View> */}
            <View style={{ width: 100, height: 30, flexDirection: 'row', justifyContent: 'flex-end' }}>
              <TouchableOpacity activeOpacity={0.7} onPress={() => {
                const friendData = Object.assign({}, { friend_info: Object.assign({}, { _id: item._id, avatars: item.avatars, fullName: item.fullName, phoneNo: item.phoneNo, userId: item.userId }) }, { friend_id: item._id })
                global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'ChatWindow', params: { friendData, chatType: 'friend' } }))
              }}>
                <Image source={Resources.image.booking_detail_message} style={{ height: 28, width: 28 }} />
              </TouchableOpacity>
              {
                item.phoneCountryCode ?
                  <TouchableOpacity activeOpacity={0.7} onPress={() => Linking.openURL(`tel:${item.phoneCountryCode}${item.phoneNo}`)}>
                    <Image source={Resources.image.booking_detail_phone} style={{ height: 28, width: 28, marginLeft: 10 }} />
                  </TouchableOpacity> : null
              }
            </View>
          </View>

          <View style={{ height: 1, width: width - 20 - 90, backgroundColor: '#A9A9A9' }} />
          <View style={{ justifyContent: 'space-between', flexDirection: 'row', width: width - 20 - 90 }}>
            <Text style={{ fontSize: 10, color: '#A9A9A9' }}>{item.totalDownline}  {$.store.app.strings.downline_team}</Text>
            <Text style={{ fontSize: 10, color: '#A9A9A9' }}>{$.store.app.strings.downline_join_since} {item.joinedOn}</Text>
          </View>

        </View>
      </View>
    )
  }

  _emptyView = () => {
    const { strings } = this.props.app
    return (
      <View style={{ justifyContent: 'center', alignItems: 'center', height: 100, width: width }}><Text>{strings.team_message}</Text></View>
    )
  }

  _renderHeader = () => {
    return (
      <Text style={{ marginLeft: 20, color: '#A9A9A9', marginTop: 15, marginBottom: 8 }}>Team Member List</Text>
    )
  }
  render() {
    const { strings } = this.props.app
    const { teamList, searchFriends } = this.friendData
    return (
      <View style={{ flex: 1, backgroundColor: '#F2F2F2' }}>
        <View style={{ backgroundColor: DCColor.BGColor('primary-1'), paddingLeft: 20, paddingTop: 10, }}>
          <Text style={{ fontSize: TextFont.TextSize(15), color: DCColor.BGColor('primary-2') }}>{strings.total_downline}</Text>
          <Text style={{ fontSize: TextFont.TextSize(20), fontWeight: 'bold', color: DCColor.BGColor('primary-2'), marginBottom: 10 }}>{this.isSearch ? teamList.users.length : teamList.users.length === 0 ? 0 : teamList.users.length}</Text>
          <View style={{ justifyContent: 'center', alignItems: 'center', marginBottom: 10 }}>
            <View style={{
              backgroundColor: '#fff', width: width - 20, marginRight: 10,
              height: 50, borderRadius: 6, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around'
            }}>
              <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', }}>
                <Image style={{ height: 30, width: 55, marginLeft: 10 }} source={Resources.image.share} />
                <Text style={{ flex: 1, marginLeft: 10 }}>{strings.downline_invite_earn}</Text>
              </View>
              <TouchableOpacity activeOpacity={0.7} onPress={() => {
                // 新的url
                const linkTitle = 'Please register with DACSEE using my referral link below:'
                url = `${linkTitle}https://dacsee.com/#/register?ref=${this.props.account.user.userId}`
                Clipboard.setString(url)
                $.define.func.showMessage(`https://dacsee.com/#/register?ref=${this.props.account.user.userId}`)
              }}>
                <View style={{ backgroundColor: '#FFB639', height: 30, width: 105, borderRadius: 10, justifyContent: 'center', alignItems: 'center', marginRight: 10 }}>
                  <Text style={{ fontSize: TextFont.TextSize(11), fontWeight: '700' }}>{strings.conp_invite_link}</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        {/* <ScrollView refreshControl={
          <RefreshControl
            refreshing={this.state.loading}
            onRefresh={this._fetchData.bind(this)}
            title={strings.pull_refresh}
            colors={['#ffffff']}
            progressBackgroundColor={'#1c99fb'}
          />
        }>
          <View style={{ paddingLeft: 20 }}>
            <DownLineCell goDownLineList={(lve) => this.goDownLineList(lve)} cellname='1' value={data.level1} />
            <DownLineCell goDownLineList={(lve) => this.goDownLineList(lve)} cellname='2' value={data.level2} />
            <DownLineCell goDownLineList={(lve) => this.goDownLineList(lve)} cellname='3' value={data.level3} />
          </View>
        </ScrollView> */}
        <View style={{ paddingHorizontal: 15 }}>
          <HeaderSearchBar
            name={strings.downline_search}
            search_value={this.searchValue}
            isSearch={this.isSearch}
            onChangeText={(text) => this.onChangeText(text)} />
        </View>
        <FlatList
          data={this.isSearch ? searchFriends.users : teamList.users}
          refreshControl={<RefreshControl
            refreshing={false}
            onRefresh={this.fetchdUser}
            title={strings.pull_refresh}
            colors={['#ffffff']}
            progressBackgroundColor={'#1c99fb'}
          />}
          // extraData={[chatRoomStore.footerState, chatRoomStore.headerState]}
          renderItem={this._renderItemView}
          // ListFooterComponent={this.renderFooter}
          // ListHeaderComponent={this._renderHeader}
          // onRefresh={() => this.startRefresh()}
          keyExtractor={(item, index) => index.toString()}
          // getItemLayout={(data, index) => ( {length: 110, offset: 118 * index, index} )}
          // refreshing={chatRoomStore.headerState === RefreshState.Refreshing}
          // ItemSeparatorComponent={this.renderItemSeparatorView}
          // onEndReached={() => this.onEndReached()}
          onEndReachedThreshold={0.5}
          // viewabilityConfig={VIEWABILITY_CONFIG}
          ListEmptyComponent={this._emptyView}
        />
      </View>
    )
  }
}
@inject('app')
@observer
class DownLineCell extends Component {
  render() {
    const { strings } = this.props.app
    const { cellname, value } = this.props
    return (
      <TouchableOpacity onPress={() => { if (value && value !== 0) this.props.goDownLineList(cellname) }} style={{ paddingTop: 20 }}>
        <Text style={{ fontSize: TextFont.TextSize(13), fontWeight: 'bold', color: '#404040', paddingBottom: 5 }}>
          {`${strings.level} ${cellname}`}</Text>
        <Text style={{ fontSize: TextFont.TextSize(13), fontWeight: 'bold', color: '#ccc', paddingBottom: 5 }}>{strings.total_downline}
        </Text>
        <Text style={{ fontSize: TextFont.TextSize(25), fontWeight: '300', color: '#404040', paddingBottom: 5 }}>{value || 0}</Text>
      </TouchableOpacity>
    )
  }
}
const HeaderSearchBar = (props) => {
  const { name, onChangeText = () => { }, isSearch } = props
  return (
    <View style={[styles.shadow, styles.search_bar]}>
      <View style={{ width: 40, height: 32, alignItems: 'center', justifyContent: 'center' }}>
        <Material name='search' color='#D1D1DE' size={18} />
      </View>
      <TextInput
        ref={e => this.input = e}
        underlineColorAndroid={'transparent'}
        placeholderTextColor={'#D1D1DE'}
        placeholder={name}

        // onFocus={onFocus}
        style={{ height: 42, width: width - (isSearch ? 110 : 70), paddingLeft: 0, color: '#5c5c5c' }}
        onChangeText={(text) => onChangeText(text)} />
      <View style={{ width: 40, height: 32, alignItems: 'center', justifyContent: 'center' }}
      >
        {isSearch
          ? <TouchableOpacity style={{ width: 30, height: 30, borderRadius: 18, justifyContent: 'center', alignItems: 'center', backgroundColor: '#eb8d97' }} activeOpacity={0.7}
            onPress={() => {
              this.input.clear()
              this.input.blur()
              onChangeText('')
              setTimeout(() => { this.input.setNativeProps({ text: '' }) })
            }}>
            <Material name='close' color='#fff' size={18} />
          </TouchableOpacity> : null
        }

      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  search_bar: {
    flexDirection: 'row',
    marginVertical: 10,
    backgroundColor: '#FDFDFE',
    borderRadius: 10,
    alignItems: 'center'
  },
  shadow: {
    elevation: 2,
    shadowOffset: { width: 0, height: 0 },
    shadowColor: 'rgba(0,0,0,0.4)',
    shadowOpacity: 0.4,
    shadowRadius: 3
  },

})
