import React, { Component } from 'react'
import { View, ScrollView, RefreshControl } from 'react-native'
import UserList from './components/user.list'
import UserDetail from './components/user.detail'
import UserHeader from './components/user.header'
import { inject, observer } from 'mobx-react'
import { DCColor } from 'dacsee-utils'

@inject('app')
@observer
export default class DownLineDetailScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const { strings } = $.store.app
    return {
      drawerLockMode: 'locked-closed',
      title: strings.downline_detail,
      headerStyle: {
        backgroundColor: DCColor.BGColor('primary-1'),
        shadowColor: 'transparent',
        shadowOpacity: 0,
        borderBottomWidth: 0,
        borderBottomColor: 'transparent',
        elevation: 0,
      }
    }
  }

  constructor(props) {
    super(props)
    this.state = {
      loading: false,
      data: { users: [], referral: {} }
    }
  }

  async componentDidMount() {
    this._fetchData()
  }

  async _fetchData(index = 0) {
    this.setState({
      loading: true
    })
    const { state } = this.props.navigation
    const { params, strings } = state
    try {
      const data = await $.method.session.User.Get(`v1/downline/user/${params._id}`)
      this.setState({
        loading: false,
        data: data
      })
    } catch (e) {
      this.setState({
        loading: false
      })
    }
  }

  goUserDetails(_id) {
    this.props.navigation.navigate('DownLineDetail', {
      _id: _id,
      level: this.state.data.level
    })
  }

  render() {
    const { data } = this.state
    const { strings } = this.props.app

    return (
      <View style={{ flex: 1, backgroundColor: data.users && data.users.length > 0 ? '#f7f7f7' : '#fff' }}>
        <View style={{ backgroundColor: DCColor.BGColor('primary-1'), paddingHorizontal: 20 }}>
          <UserHeader data={data} />
        </View>
        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={this.state.loading}
              onRefresh={this._fetchData.bind(this)}
              title={strings.pull_refresh}
              colors={['#ffffff']}
              progressBackgroundColor={DCColor.BGColor('primary-1')}
            />
          }
        >
          <View style={{ backgroundColor: '#fff' }}>
            <UserDetail data={data} />
          </View>
          {data.users && data.users.length > 0 &&
            <View style={{ backgroundColor: '#f7f7f7', flex: 1 }}>
              <UserList users={data.users} goUserDetails={(_id) => this.goUserDetails(_id)} />
            </View>
          }
        </ScrollView>
      </View>
    )
  }
}

