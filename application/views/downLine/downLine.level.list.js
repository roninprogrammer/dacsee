import React, { Component } from 'react'
import { Text, View, StyleSheet, Image, TouchableOpacity, ScrollView, Clipboard } from 'react-native'
import { NavigationActions } from 'react-navigation'
import { Screen, TextFont, DCColor } from 'dacsee-utils'
import Material from 'react-native-vector-icons/MaterialIcons'
import Resources from 'dacsee-resources'
import { inject, observer } from 'mobx-react'
import { observable } from 'mobx'
const { width } = Screen.window

@inject('app', 'downLine', 'account')
@observer
export default class DownLineTotalLevelScreen extends Component {
  @observable isSearch: Boolean = false
  @observable searchValue: String = ''
  static navigationOptions = ({ navigation }) => {
    const { strings } = $.store.app
    return {
      drawerLockMode: 'locked-closed',
      title: strings.downline,
      headerStyle: {
        backgroundColor: DCColor.BGColor('primary-1'),
        shadowColor: 'transparent',
        shadowOpacity: 0,
        borderBottomWidth: 0,
        borderBottomColor: 'transparent',
        elevation: 0
      }
    }
  }

  constructor(props) {
    super(props)
    this.friendData = props.downLine.friendData
    this.getTeamList = props.downLine.getTeamList
    this.getLevelList = props.downLine.getLevelList
    this.getNewMemberLevel1 = props.downLine.getNewMemberLevel1
    this.getNewMemberLevel2 = props.downLine.getNewMemberLevel2
    this.getNewMemberLevel3 = props.downLine.getNewMemberLevel3
    this.state = {
      loading: false,
      data: {},
      lvl1: false
    }
  }

  async componentWillMount() {
    await this.getNewMemberLevel1()
    await this.getNewMemberLevel2()
    await this.getNewMemberLevel3()
  }

  async componentDidMount() {
    await this.getLevelList()
    await this.getTeamList()
  }

  render() {
    const { container, rowContainer, levelView, text, newView, redSpot, hugeText } = styles
    const { strings } = this.props.app
    const { teamList, searchFriends, lvlList, newMemberLvl1, newMemberLvl2, newMemberLvl3 } = this.friendData
    const { data } = this.state
    return (
      <View style={{ flex: 1, backgroundColor: '#F2F2F2' }}>
        <View style={{ backgroundColor: DCColor.BGColor('primary-1'), paddingLeft: 20, paddingTop: 10 }}>
          <Text style={{ fontSize: TextFont.TextSize(15), color: DCColor.BGColor('primary-2') }}>{strings.total_downline}</Text>
          <Text style={{ fontSize: TextFont.TextSize(20), fontWeight: 'bold', color: DCColor.BGColor('primary-2'), marginBottom: 10 }}>{this.isSearch ? searchFriends.total : teamList.total === 0 ? 0 : teamList.total}</Text>
          <View style={{ justifyContent: 'center', alignItems: 'center', marginBottom: 10 }}>
            <View style={{
              backgroundColor: '#fff', width: width - 20, marginRight: 10,
              height: 50, borderRadius: 6, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around'
            }}>
              <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', }}>
                <Image style={{ height: 30, width: 55, marginLeft: 10 }} source={Resources.image.share} />
                <Text style={{ flex: 1, marginLeft: 10 }}>{strings.downline_invite_earn}</Text>
              </View>
              <TouchableOpacity activeOpacity={0.7} onPress={() => {
                // 新的url
                const linkTitle = 'Please register with DACSEE using my referral link below:'
                url = `${linkTitle}https://dacsee.com/#/register?ref=${this.props.account.user.userId}`
                Clipboard.setString(url)
                $.define.func.showMessage(`https://dacsee.com/#/register?ref=${this.props.account.user.userId}`)
              }}>
                <View style={{ backgroundColor: DCColor.BGColor('primary-1'), height: 30, width: 105, borderRadius: 10, justifyContent: 'center', alignItems: 'center', marginRight: 10 }}>
                  <Text style={{ fontSize: TextFont.TextSize(11), fontWeight: '700' }}>{strings.conp_invite_link}</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        {lvlList.total > 0 &&
          <ScrollView style={{ paddingHorizontal: 10, paddingTop: 10 }}>
            <TouchableOpacity style={container} onPress={() => global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'DownLineTotal' }))}>
              <View style={rowContainer}>
                <View>
                  <View style={{ flexDirection: 'row' }}>
                    <View style={levelView}>
                      <Text style={text}>{strings.downline_tier_1}</Text>
                    </View>
                    {newMemberLvl1.length > 0 &&
                      <View style={newView}>
                        <Text style={text}>{newMemberLvl1.length} {strings.downline_new_member}</Text>
                        <View style={redSpot}></View>
                      </View>
                    }
                  </View>
                  <Text style={hugeText}>{lvlList.level1 ? lvlList.level1 : 0} {strings.downline_affiliates}</Text>
                </View>
                <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                  <Material name={'chevron-right'} size={40} />
                </View>
              </View>
            </TouchableOpacity>
            <View style={container}>
              <View style={rowContainer}>
                <View>
                  <View style={{ flexDirection: 'row' }}>
                    <View style={levelView}>
                      <Text style={text}>{strings.downline_tier_2}</Text>
                    </View>
                    {newMemberLvl2.length > 0 &&
                      <View style={newView}>
                        <Text style={text}>{newMemberLvl2.length} {strings.downline_new_member}</Text>
                        <View style={redSpot}></View>
                      </View>
                    }
                  </View>
                  <Text style={hugeText}>{lvlList.level2 ? lvlList.level2 : 0} {strings.downline_affiliates}</Text>
                </View>
              </View>
            </View>
            <View style={container}>
              <View style={rowContainer}>
                <View>
                  <View style={{ flexDirection: 'row' }}>
                    <View style={levelView}>
                      <Text style={text}>{strings.downline_tier_3}</Text>
                    </View>
                    {newMemberLvl3.length > 0 &&
                      <View style={newView}>
                        <Text style={text}>{newMemberLvl3.length} {strings.downline_new_member}</Text>
                        <View style={redSpot}></View>
                      </View>
                    }
                  </View>
                  <Text style={hugeText}>{lvlList.level3 ? lvlList.level3 : 0} {strings.downline_affiliates}</Text>
                </View>
              </View>
            </View>
          </ScrollView>
        }
        {lvlList.total === 0 &&
          <View style={{ justifyContent: 'center', alignItems: 'center', height: 100, width: width }}><Text>{strings.team_message}</Text></View>
        }
      </View>
    )
  }
}

const styles = StyleSheet.create({
  search_bar: {
    flexDirection: 'row',
    marginVertical: 10,
    backgroundColor: '#FDFDFE',
    borderRadius: 10,
    alignItems: 'center'
  },
  shadow: {
    elevation: 2,
    shadowOffset: { width: 0, height: 0 },
    shadowColor: 'rgba(0,0,0,0.4)',
    shadowOpacity: 0.4,
    shadowRadius: 3
  },
  container: {
    backgroundColor: '#ffffff',
    height: 80,
    width: '100%',
    marginBottom: 10,
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 10,
    paddingLeft: 30,
    paddingRight: 10,
    borderRadius: 5
  },
  rowContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%'
  },
  levelView: {
    backgroundColor: DCColor.BGColor('grey'),
    width: 50,
    height: 30,
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 10
  },
  text: {
    fontSize: TextFont.TextSize(12),
    color: DCColor.BGColor('primary-1')
  },
  newView: {
    backgroundColor: '#000000',
    height: 30,
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    paddingHorizontal: 7
  },
  redSpot: {
    backgroundColor: 'red',
    width: 12,
    height: 12,
    borderRadius: 6,
    position: 'absolute',
    top: 0,
    right: 0
  },
  hugeText: {
    fontSize: TextFont.TextSize(22),
    paddingTop: 5,
    fontWeight: '600',
    // color: '#000'
  }



})
