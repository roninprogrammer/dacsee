import React, { Component } from 'react'
import { Text, View, Image } from 'react-native'
import { inject, observer } from 'mobx-react'
import { TextFont, Avatars, Screen, DCColor } from 'dacsee-utils'

const { width } = Screen.window

@inject('app')
@observer
export default class UserDetail extends Component {
  render() {
    const { data = {} } = this.props
    const { strings } = this.props.app
    const { userId, fullName, avatars } = data

    return (
      <View style={{ backgroundColor: DCColor.BGColor('primary-1'), paddingBottom: 20 }}>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <View style={[
            { backgroundColor: '#eee', overflow: 'hidden', borderColor: '#e8e8e8', borderWidth: 1 },
            { borderRadius: 40, width: 80, height: 80, justifyContent: 'center', alignItems: 'center' }
          ]}>
            <Image style={{ width: 80, height: 80 }} source={{ uri: Avatars.getHeaderPicUrl(avatars) }} />
          </View>
          <View style={{ justifyContent: 'center', marginLeft: 15, width: width - 135 }}>
            <Text style={{ color: '#fff', fontSize: TextFont.TextSize(22) }}>{fullName}</Text>
            <Text style={{ color: '#ccc', fontSize: TextFont.TextSize(14) }}>{userId ? `${strings.userid}：${userId}` : ''}</Text>
          </View>
        </View>
      </View>
    )
  }
}

