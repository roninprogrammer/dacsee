import React, { Component } from 'react'
import { Text, View } from 'react-native'
import { TextFont } from 'dacsee-utils'
import { inject, observer } from 'mobx-react'

@inject('app')
@observer
export default class UserDetail extends Component {
  render() {
    const { data } = this.props
    const { strings } = this.props.app
    const { referral, joinedOn, phoneCountryCode, phoneNo, level } = data
    return (
      <View style={{ paddingHorizontal: 20, paddingTop: 20, backgroundColor: '#fff' }}>
        <InfoCell cellname={strings.referrer_name} value={referral.fullName ? referral.fullName : ''} />
        <InfoCell cellname={strings.join_date} value={joinedOn} />
        <InfoCell cellname={strings.phone} value={phoneCountryCode ? `${phoneCountryCode} ${phoneNo}` : ''} />
        <InfoCell cellname={strings.downline_level} value={level} />
      </View>
    )
  }
}
class InfoCell extends Component {
  render() {
    const { cellname, value } = this.props

    if (!value) {
      return null
    }
    return (
      <View style={{ flexDirection: 'row', justifyContent: 'space-between', height: 40 }}>
        <Text style={{ color: '#ccc', fontSize: TextFont.TextSize(15) }}>
          {cellname}
        </Text>
        <Text style={{ color: '#000', fontSize: TextFont.TextSize(15) }}>{value}</Text>
      </View>
    )
  }
}

