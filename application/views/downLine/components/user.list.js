import React, { Component } from 'react'
import { Text, View, Image, ListView, TouchableOpacity } from 'react-native'
import { TextFont, Avatars, Screen } from 'dacsee-utils'
import { inject, observer } from 'mobx-react'

const { width } = Screen.window
const dataContrast = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })

@inject('app')
@observer
export default class UserList extends Component {
  render() {
    const { users } = this.props
    const { strings } = this.props.app
    return (
      <View style={{ paddingHorizontal: 20, flex: 1 }}>
        <Text style={{ fontSize: TextFont.TextSize(15), fontWeight: 'bold', color: '#404040', marginVertical: 10 }}>
          {strings.downline_list}
        </Text>
        <ListView
          removeClippedSubviews={false}
          dataSource={dataContrast.cloneWithRows(users)}
          enableEmptySections={true}
          style={{ flex: 1 }}
          renderRow={(row, key, index) => {
            return (
              <TouchableOpacity key={key} activeOpacity={.7}
                onPress={this.props.goUserDetails.bind(this, row._id)}>
                {index == 0 ? <View /> : <View style={{ height: 0.8, backgroundColor: '#ccc' }} />}
                <UserCell itemData={row} />
              </TouchableOpacity>
            )
          }}
        />
      </View>
    )
  }
}
@inject('app')
@observer
class UserCell extends Component {

  render() {
    const { strings } = this.props.app
    const { itemData = {} } = this.props
    const { fullName, userId, avatars = [{ url: 'https://storage.googleapis.com/dacsee-service-user/_shared/default-profile.jpg' }], totalDownline } = itemData
    return (
      <View style={{ paddingVertical: 10, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
        <View style={{ flexDirection: 'row', alignItems: 'center', flex: 1 }}>
          <View style={[
            { backgroundColor: '#eee', marginRight: 10, overflow: 'hidden', borderColor: '#e8e8e8', borderWidth: 1 },
            { borderRadius: 25, width: 50, height: 50, justifyContent: 'center', alignItems: 'center' }
          ]}>
            <Image style={{ width: 50, height: 50 }} source={{ uri: Avatars.getHeaderPicUrl(avatars) }} />
          </View>
          <View style={{ justifyContent: 'center', width: width - 200 }} >
            <Text ellipsizeMode={'middle'} numberOfLines={1} style={{ color: '#000' }}>{fullName}</Text>
            <Text style={{ color: '#ccc' }}>{userId ? `${strings.userid}：${userId}` : ''}</Text>
          </View>
        </View>
        <Text style={{ color: '#000', textAlign: 'right' }}>{totalDownline || totalDownline === 0 ? `${totalDownline} ${strings.downline}` : ''}</Text>
      </View>
    )
  }
}
