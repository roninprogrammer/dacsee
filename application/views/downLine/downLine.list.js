import React, { Component } from 'react'
import { Text, View, ScrollView, RefreshControl } from 'react-native'
import { DCColor, TextFont } from 'dacsee-utils'
import UserList from './components/user.list'
import { inject, observer } from 'mobx-react'


@inject('app')
@observer
export default class DownLineListScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const { strings } = $.store.app
    return {
      drawerLockMode: 'locked-closed',
      title: strings.downline_list,
      headerStyle: {
        backgroundColor: DCColor.BGColor('primary-1'),
        shadowColor: 'transparent',
        shadowOpacity: 0,
        borderBottomWidth: 0,
        borderBottomColor: 'transparent',
        elevation: 0,
      }
    }
  }

  constructor(props) {
    super(props)
    this.state = {
      loading: false,
      data: { users: [] },
      total: 1
    }
  }

  async componentDidMount() {
    this._fetchData()
  }

  async _fetchData(index = 0) {
    this.setState({
      loading: true
    })
    const { state } = this.props.navigation
    const { params, strings } = state
    try {
      const data = await $.method.session.User.Get(`v1/downline/level/${params.level}`)
      this.setState({
        loading: false,
        data: data
      })
    } catch (e) {
      this.setState({
        loading: false
      })
    }
  }
  goUserDetails(_id) {
    const { strings } = this.props
    switch (String(this.state.data.level)) {
      case '1':
        this.props.navigation.navigate('DownLineDetailOne', {
          _id: _id,
          level: this.state.data.level,
        })
        break
      case '2':
        this.props.navigation.navigate('DownLineDetailTwo', {
          _id: _id,
          level: this.state.data.level,
        })
        break
      case '3':
        this.props.navigation.navigate('DownLineDetailThree', {
          _id: _id,
          level: this.state.data.level,
        })
        break
    }
  }
  render() {
    const { strings } = this.props.app
    const { data } = this.state
    const { state } = this.props.navigation
    const { params } = state
    return (
      <View style={{ flex: 1, backgroundColor: '#fff' }}>
        <View style={{ backgroundColor: DCColor.BGColor('primary-1'), paddingHorizontal: 20 }}>
          <Text style={{ fontSize: TextFont.TextSize(16), fontWeight: 'bold', color: '#fff', paddingBottom: 15 }}>{`${strings.level} ${params.level}`}</Text>
          <Text style={{ fontSize: TextFont.TextSize(15), color: '#fff', marginBottom: 5 }}>{strings.total_downline}</Text>
          <Text style={{ fontSize: TextFont.TextSize(25), fontWeight: 'bold', color: '#fff', marginBottom: 15 }}>{data.users.length}</Text>
        </View>
        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={this.state.loading}
              onRefresh={this._fetchData.bind(this)}
              title={strings.pull_refresh}
              colors={['#ffffff']}
              progressBackgroundColor={DCColor.BGColor('primary-1')}
            />
          }
        >
          <UserList users={data.users} goUserDetails={(_id) => this.goUserDetails(_id)} />
        </ScrollView>
      </View>
    )
  }
}


