import React, { Component } from 'react'
import { Text, View, Image, StyleSheet, ScrollView, Linking, TouchableOpacity, SafeAreaView } from 'react-native'
import { inject, observer } from 'mobx-react'
import ActionSheet from 'react-native-actionsheet'
import { Screen, TextFont, DCColor } from 'dacsee-utils'
import Resources from 'dacsee-resources'

const { width } = Screen.window
const styles = StyleSheet.create({
    mainCont: {
        width,
        height: '100%',
        flexDirection: 'column',
        flexWrap: 'nowrap',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        backgroundColor: 'white'
    },
    scrollCont: {
        flex: 1,
        width,
        backgroundColor: 'white'
    },
    imgCont: {
        width,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: DCColor.BGColor('primary-1'),
        padding: 15
    },
    dcLogo: {
        height: 150,
        width: 150,
        resizeMode: 'contain'
    },
    bodyCont: {
        width,
        padding: 25
    },
    fCaption: {
        fontSize: TextFont.TextSize(14),
        color: 'rgba(0, 0, 0, 0.45)',
        fontWeight: '600',
        marginBottom: 15,
        textAlign: 'left'
    },
    fTitle: {
        fontSize: TextFont.TextSize(16),
        fontWeight: '800',
        color: 'rgba(0, 0, 0, 0.75)',
    },
    dcBtn: {
        marginTop: 10,
        padding: 10,
        paddingHorizontal: 15,
        borderRadius: 7,
        backgroundColor: DCColor.BGColor('primary-1')
    },
    dcLink: {
        fontSize: TextFont.TextSize(13),
        color: DCColor.BGColor('primary-1'),
        fontWeight: '600',
        marginBottom: 15
    },
    btnText: {
        fontSize: TextFont.TextSize(15),
        fontWeight: '900',
        color: 'rgba(0, 0, 0, .75)',
        flex: 1,
        textAlign: 'center'
    },
    dcSeperator: {
        width: '100%',
        height: 1,
        backgroundColor: 'rgba(0, 0, 0, 0.2)',
        marginTop: 15,
        marginBottom: 15
    },
    dcCol: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    dcIcon: {
        width: 15,
        height: 15,
        resizeMode: 'contain',
        marginRight: 10
    }
})

@inject('app', 'joy')
@observer
export default class HelpScreen extends Component {
    static navigationOptions = ({ navigation }) => {
        const { strings } = $.store.app
        return {
            drawerLockMode: 'locked-closed',
            title: strings.help
        }
    }

    render() {
        const { strings } = this.props.app
        const navigateTo = async (index) => {
            if (index === 0) {
                await Linking.openURL(`https://www.google.com/maps/dir/?api=1&destination=DACSEE Training Center&destination_place_id=ChIJlVZdMjC1zTERqInRaHg0ajE&dir_action=navigate`)
            } else if (index === 1) {
                await Linking.openURL(`https://waze.com/ul?ll=3.029960,101.616301&navigate=yes`)
            }
        }
        const {
            mainCont,
            scrollCont,
            imgCont,
            dcLogo,
            bodyCont,
            fCaption,
            fTitle,
            dcBtn,
            btnText,
            dcSeperator,
            dcCol,
        } = styles
        
        return (
            <SafeAreaView style={mainCont}>
                <ScrollView style={scrollCont}>
                    <View style={imgCont}>
                        <Image style={dcLogo} source={Resources.image.logo_portrait_border} />
                    </View>
                    <View style={bodyCont}>
                        <Text style={fTitle}>{strings.faq_title}</Text>
                        {/* <Text style={dcLink} onPress={() => Linking.openURL('https://kb.dacsee.com/knowledge-base/faq/')}>https://kb.dacsee.com/knowledge-base/faq/</Text> */}
                        <TouchableOpacity style={dcBtn} onPress={() => Linking.openURL('https://kb.dacsee.com/knowledge-base/faq/')}>
                            <Text style={btnText}>{strings.view_faq && strings.view_faq.toUpperCase()}</Text>
                        </TouchableOpacity>
                        <View style={dcSeperator} />
                        <Text style={fTitle}>{strings.update_driver_document}</Text>
                        <TouchableOpacity style={dcBtn} onPress={() => Linking.openURL('http://bit.ly/dacsee-updatedocument')}>
                            <Text style={btnText}>{strings.update_driver_document && strings.update_driver_document.toUpperCase()}</Text>
                        </TouchableOpacity>
                        <View style={dcSeperator} />
                        <Text style={[fTitle, { marginBottom: 10 }]}>{strings.training}</Text>
                        <Text style={[fCaption, { marginBottom: 5 }]}>{strings.training_date}</Text>
                        <Text style={[fCaption, { marginBottom: 20 }]}>11:00AM - 1:00PM</Text>
                        <Text style={[fTitle, { marginBottom: 10 }]}>{strings.addr}</Text>
                        <Text style={[fCaption, { marginBottom: 0 }]}>Malaysia Office:</Text>
                        <Text style={[fCaption, { marginBottom: 0 }]}>DMD Technology Sdn. Bhd.</Text>
                        <Text style={[fCaption, { marginBottom: 0 }]}>Block D-5-3 , Setiawalk,</Text>
                        <Text style={[fCaption, { marginBottom: 0 }]}>Persiaran Wawasan, Pusat Bandar Puchong,</Text>
                        <Text style={[fCaption, { marginBottom: 5 }]}>47160 Puchong , Selangor Darul Ehsan .</Text>
                        <TouchableOpacity onPress={() => this.fromAction.show()} style={dcBtn}>
                            <Text style={btnText}>{strings.navigation && strings.navigation.toUpperCase()}</Text>
                        </TouchableOpacity>
                        <View style={dcSeperator} />
                        <Text style={[fTitle, { marginBottom: 15 }]}>{strings.chat_support_operation_hour}</Text>
                        <View style={[dcCol, { marginBottom: 10 }]}>
                            <Text style={[fTitle, { width: '30%' }]}>{strings.mon}:</Text>
                            <Text style={[fTitle, { color: DCColor.BGColor('primary-1') }]}>11am - 7pm</Text>
                        </View>
                        <View style={[dcCol, { marginBottom: 10 }]}>
                            <Text style={[fTitle, { width: '30%' }]}>{strings.tue}:</Text>
                            <Text style={[fTitle, { color: DCColor.BGColor('primary-1') }]}>11am - 7pm</Text>
                        </View>
                        <View style={[dcCol, { marginBottom: 10 }]}>
                            <Text style={[fTitle, { width: '30%' }]}>{strings.wed}:</Text>
                            <Text style={[fTitle, { color: DCColor.BGColor('primary-1') }]}>11am - 7pm</Text>
                        </View>
                        <View style={[dcCol, { marginBottom: 10 }]}>
                            <Text style={[fTitle, { width: '30%' }]}>{strings.thu}:</Text>
                            <Text style={[fTitle, { color: DCColor.BGColor('primary-1') }]}>11am - 7pm</Text>
                        </View>
                        <View style={[dcCol, { marginBottom: 10 }]}>
                            <Text style={[fTitle, { width: '30%' }]}>{strings.fri}:</Text>
                            <Text style={[fTitle, { color: DCColor.BGColor('primary-1') }]}>11am - 1pm, 2.30pm - 6pm</Text>
                        </View>
                        <View style={[dcCol, { marginBottom: 10 }]}>
                            <Text style={[fTitle, { width: '30%' }]}>{strings.sat}:</Text>
                            <Text style={[fTitle, { color: DCColor.BGColor('primary-1') }]}>{strings.offline_job}</Text>
                        </View>
                        <View style={[dcCol, { marginBottom: 5 }]}>
                            <Text style={[fTitle, { width: '30%' }]}>{strings.sun}:</Text>
                            <Text style={[fTitle, { color: DCColor.BGColor('primary-1') }]}>11am - 5pm</Text>
                        </View>
                        {/* <View style={dcSeperator} /> */}
                        {/* <Text style={fTitle}>{strings.email}</Text>
                        <Text style={dcLink} onPress={() => Linking.openURL('mailto:support@dacsee.com?subject=APP_ENQUIRY')}>support@dacsee.com</Text> */}
                        <TouchableOpacity onPress={() => Linking.openURL('https://tawk.to/chat/5bced00719b86b5920c08a54/default')} style={dcBtn}>
                            <Text style={btnText}>{strings.live_chat && strings.live_chat.toUpperCase()}</Text>
                        </TouchableOpacity>
                        <View style={dcSeperator} />
                        <Text style={[fTitle, { marginBottom: 5 }]}>{strings.error_report}</Text>
                        <Text style={[fCaption, { marginBottom: 5 }]}>{strings.error_report_desc}</Text>
                        <TouchableOpacity onPress={() => Linking.openURL('https://form.jotform.me/90623549398469')} style={dcBtn}>
                            <Text style={btnText}>{strings.error_report_button && strings.error_report_button.toUpperCase()}</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
                <ActionSheet
                    ref={o => { this.fromAction = o }}
                    title={strings.navigation}
                    message={strings.please_make_sure_installed}
                    options={[strings.google_maps, 'Waze', strings.cancel]}
                    cancelButtonIndex={2}
                    onPress={(index) => navigateTo(index)}
                />
            </SafeAreaView>
        )

    }
}

