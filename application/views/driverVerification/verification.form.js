import React, { Component } from 'react'
import { Text, View, StyleSheet, TouchableOpacity, SafeAreaView, Image } from 'react-native'
import { inject, observer } from 'mobx-react'
import { Screen, TextFont, DCColor } from 'dacsee-utils'
import VerifyPage from './verification.page'
import Resources from 'dacsee-resources'
import SkipModal from './component/modal.skip'
import NotificationBar from '../../components/notificationBar'

const { width } = Screen.window

const styles = StyleSheet.create({
    mainCont: {
        width,
        height: '100%',
        backgroundColor: '#F2F2F2',
        flexDirection: 'column',
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        flexWrap: 'nowrap'
    },
    btnCont: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width,
        padding: 15
    },
    longbutton: {
        flex: 1,
        backgroundColor: DCColor.BGColor('primary-1'),
        height: 40,
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'relative',
    },
    skipIcon: {
        alignItems: 'center',
        justifyContent: 'center',
        fontSize: TextFont.TextSize(16),
        fontWeight: '600',
        color: 'black',
        textAlign: 'center'
    },
    dcImg: {
        width: width / 2,
        height: width / 2,
        resizeMode: 'contain',
        marginBottom: 30
    },
    uploadedTitle: {
        fontSize: TextFont.TextSize(22),
        fontWeight: '800',
        color: 'rgba(0, 0 , 0, 0.75)',
        marginBottom: 10,
        width: '100%',
        textAlign: 'center'
    },
    uploadedDesc: {
        fontSize: TextFont.TextSize(15),
        fontWeight: '500',
        color: 'rgba(0, 0,0 ,0.45)',
        width: '100%',
        textAlign: 'center',
        paddingHorizontal: 20
    }
})

@inject('app', 'account', 'driver', 'driverVerification')
@observer
export default class DriverVerificationForm extends Component {
    static navigationOptions = ({ navigation }) => {
        const { strings } = $.store.app
        return {
            drawerLockMode: 'locked-closed',
            title: strings.driver_verification,
        }
    }

    constructor(props) {
        super(props)
        this.state = {
            data: {},
            pages: [],
            currentPage: 0,
            currentPageInfo: {},
            authLetterState: false
        }
    }

    componentDidMount() {
        const { form = {} } = $.store.driverVerification.dvForm
        if (form.pages && form.pages.length) {
            this.setState({
                data: $.store.driverVerification.dvForm,
                pages: form.pages,
                currentPage: 0, // $.store.account.verificationCurrentPage
                currentPageInfo: form.pages[0]
            })
        }
    }


    render() {
        const {
            btnCont,
            longbutton,
            skipIcon,
            uploadedTitle,
            dcImg,
            uploadedDesc
        } = styles
        const pages = this.props.driverVerification.dvPages
        const { strings } = this.props.app
        const { dvCurrentPageInfo, dvCurrentPage, nextLabel, dvForm } = this.props.driverVerification
        const { formType } = dvForm

        return pages && pages.length ?
            (
                <SafeAreaView style={{ flex: 1, backgroundColor: 'white' }}>
                    {dvForm && dvForm.rejectedReason ? (
                        <View style={{ width: '100%', padding: 5, backgroundColor: 'rgba(0, 0, 0, 0.05)' }}>
                            <NotificationBar
                                icon={Resources.image.attention_icon}
                                label={`${strings.reject_reason} ${dvForm.rejectedReason}`}
                            />
                        </View>
                    ) : null}
                    <VerifyPage
                        pageInfo={dvCurrentPageInfo}
                        formType={formType}
                    />
                    <View style={btnCont}>
                        {dvCurrentPage > 0 ? (
                            <TouchableOpacity style={[longbutton, { marginRight: 10, backgroundColor: DCColor.BGColor('disable') }]} onPress={() => { this.props.driverVerification.prevPage() }}>
                                <Text style={skipIcon}>{strings.prev_btn.toUpperCase()}</Text>
                            </TouchableOpacity>
                        ) : null}
                        <TouchableOpacity style={longbutton} onPress={() => { this.props.driverVerification.nextPage('driverRegistration') }}>
                            <Text style={skipIcon}>{!dvCurrentPageInfo.allowSkip ? strings.next.toUpperCase() : nextLabel}</Text>
                        </TouchableOpacity>
                    </View>
                    <SkipModal />
                </SafeAreaView>
            ) : (
                <SafeAreaView style={{ flex: 1, backgroundColor: 'white', justifyContent: 'center', alignItems: 'center' }}>
                    {dvForm && (dvForm.status === 'pendingVerification' || dvForm.status === 'completed') ? (
                        <View style={{ width, backgroundColor: 'white', justifyContent: 'center', alignItems: 'center' }}>
                            <Image source={Resources.image.verification_complete} style={dcImg} />
                            <Text style={uploadedTitle}>{strings.congratulations}</Text>
                            <Text style={uploadedDesc}>{strings.congratulations_desc}</Text>
                        </View>
                    ) : 
                     dvForm && (dvForm.status === 'pendingAPADSubmission' || dvForm.status === 'pendingAPADVerification' || dvForm.status === 'APADCompleted') ? (
                        <View style={{ width, backgroundColor: 'white', justifyContent: 'center', alignItems: 'center' }}>
                            <Image source={Resources.image.verification_complete} style={dcImg} />
                            <Text style={uploadedTitle}>{strings.congratulations}</Text>
                            <Text style={uploadedDesc}>{strings.verification_form_evpDesciption}</Text>
                        </View>
                    ) : null}
                </SafeAreaView>
            )
    }
}