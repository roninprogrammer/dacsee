import React, { Component } from 'react'
import { NavigationActions } from 'react-navigation'
import ImagePicker from 'react-native-image-picker'
import axios from 'axios'
import ActionSheet from 'react-native-actionsheet'
import { Text, View, Image, StyleSheet, DatePickerAndroid, TouchableOpacity, Platform, FlatList, TextInput, } from 'react-native'
import { inject, observer } from 'mobx-react'
import Resources from 'dacsee-resources'
import Material from 'react-native-vector-icons/MaterialIcons'
import moment from 'moment'
import { Screen, TextFont, DCColor } from 'dacsee-utils'
import CmpTimePicker from '../../components/cmpTimePicker'
import CmpSelect from '../../components/cmpSelect'

const { height, width } = Screen.window


@inject('app', 'account', 'driverVerification')
@observer
export default class VerifyPage extends Component {
    constructor(props: Object) {
        super(props)
    }
    
    render() {
        const { bigcont, pageTitle, dcForm, titleCont, pageCaption } = styles
        const { driverVerification, account } = this.props
        const { dvCurrentPageInfo } = driverVerification

        const data = dvCurrentPageInfo && dvCurrentPageInfo.fields || []

        if (this.listRef) this.listRef.scrollToOffset({ offset: 0, animated: true })

        return (
            <View style={bigcont}>
                {account.user.driverInfo.requestType === 'driverRegistration' ? 
                    <View style={titleCont}>
                        <Text style={pageTitle}>{dvCurrentPageInfo.label}</Text>
                        <Text style={pageCaption}>{dvCurrentPageInfo.caption}</Text>
                    </View>
                    : <View></View>
                }
                <FlatList
                    style={dcForm}
                    data={data.slice()}
                    extraData={driverVerification}
                    keyExtractor={(item) => item.id}
                    removeClippedSubviews={false}
                    ref={(ref) => this.listRef = ref}
                    renderItem={({ index, item }) => {
                        return (
                            <VerifyItem
                                data={item}
                                onUpdate={(value, otherValue, countryCode, phoneNo) => driverVerification.updateFields(index, value, otherValue, countryCode, phoneNo, item.id)}
                                getVehicleModal={(value) => driverVerification.getVehicleModal(value, index)}
                                vehicleModels={driverVerification.vehicleModels}
                            />
                        )
                    }}
                />
            </View>
        )
    }
}

@inject('app', 'account', 'driver', 'driverVerification')
@observer
class VerifyItem extends Component {
    constructor(props) {
        super(props)
        this.state = {
            id: '',
            value: '',
            label: '',
            type: '',
            showTimePicker: false,
            showSelect: false,
            minYear: '',
            maxYear: '',
            required: false,
            placeholder: '',
            isVisible: true,
            isRequired: false,
            extraProps: {}
        }
    }

    componentDidMount() {
        const { data } = this.props
        const { id, value = '', label = '', type, options, caption, api, otherValue, yearGap, valueCountryCode, valuePhoneNo, minYear, maxYear, required, placeholder, isVisible = true, isRequired = false,extraProps = {} } = data

        if (api && id !== 'vehicleInfo.model') {
            axios.get(api.url, {
                headers: {
                    Authorization: $.store.account.authToken
                }
            }).then((res) => {
                this.setState({
                    id,
                    value,
                    label,
                    type,
                    urlOptions: res.data,
                    required,
                    placeholder,
                    isVisible,
                    isRequired,
                    extraProps
                })
            }).catch((e) => {
                console.log(e);
            })
        } else if (maxYear && maxYear.length > 0) {
            this.setState({
                ...this.state,
                maxYear,
                isVisible,
                isRequired,
                extraProps
            })
        } else if (minYear && minYear.length > 0) {
            this.setState({
                ...this.state,
                minYear,
                isVisible,
                isRequired,
                extraProps
            })
        } else if (type === 'phone') {
            this.setState({
                id,
                valueCountryCode,
                valuePhoneNo,
                label,
                type,
                options,
                caption,
                otherValue,
                required,
                placeholder,
                isVisible,
                isRequired,
                extraProps
            })
        } else if (type === 'selectYear') {
            const currentYear = (new Date()).getFullYear();
            let startYear = currentYear - yearGap;
            let years = [];

            while (startYear < currentYear + 1) {
                years.push(startYear++)
            }

            this.setState({
                id,
                value,
                label,
                type,
                yearOptions: years,
                required,
                placeholder,
                isVisible,
                isRequired,
                extraProps
            })
        } else {
            this.setState({
                id,
                value,
                label,
                type,
                options,
                caption,
                otherValue,
                required,
                placeholder,
                isVisible,
                isRequired,
                extraProps
            })
        }
    }

    componentDidUpdate(nextProps, nextState) {
        if (typeof nextProps.data.isVisible === 'undefined') nextProps.data.isVisible = true
        if (typeof nextProps.data.isRequired === 'undefined') nextProps.data.isRequired = false

        if (nextProps.data.value !== nextState.value) {
            const { value } = nextProps.data
            this.setState({ value })
        } else if (nextProps.data.minYear !== nextState.minYear) {
            const { minYear } = nextProps.data
            this.setState({ minYear })
        } else if (nextProps.data.maxYear !== nextState.maxYear) {
            const { maxYear } = nextProps.data
            this.setState({ maxYear })
        } else if (nextProps.data.valuePhoneNo !== nextState.valuePhoneNo) {
            const { valuePhoneNo } = nextProps.data
            this.setState({ valuePhoneNo })
        } else if (nextProps.data.valueCountryCode !== nextState.valueCountryCode) {
            const { valueCountryCode } = nextProps.data
            this.setState({ valueCountryCode })
        } else if (nextProps.data.otherValue !== nextState.otherValue) {
            const { otherValue } = nextProps.data
            this.setState({ otherValue })
        } else if (nextProps.data.isVisible !== nextState.isVisible) {
            const { isVisible } = nextProps.data
            this.setState({ isVisible })
        } else if (nextProps.data.isRequired !== nextState.isRequired) {
            const { isRequired } = nextProps.data
            this.setState({ isRequired })
        }
    }

    showTimePicker() {
        if (Platform.OS === 'ios') {
            this.setState({
                showTimePicker: true,
            })
        } else if (Platform.OS === 'android') {
            this.showAndroidDatePicker()
        }
    }

    runValidate() {
        const { data } = this.props
        const { type, id, minYear, maxYear } = data
        const { value } = this.state

        if (value.length > 0 && type === 'email') {
            const reFormat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            const validateEmail = reFormat.test(String(value).toLowerCase())
            if (!validateEmail) {
                $.define.func.showMessage(this.props.app.strings.pls_enter_correct_email)
            }
        } else if (id === "personalInfo.dob" && (maxYear || minYear)) {
            if (minYear !== undefined) {
                const minDate = moment().add(minYear, 'year')
                const valueDate = moment(value)
                if (valueDate && value < minDate) {
                    $.define.func.showMessage(this.props.app.too_old_caption)
                }
            }
            if (maxYear !== undefined) {
                const maxDate = moment().add(maxYear, 'year')
                const valueDate = moment(value)
                if (value && valueDate > maxDate) {
                    $.define.func.showMessage(this.props.app.too_young_caption)
                }
            }
        } else if ((id === "personalInfo.myKAD") && value.length !== 14 && value.length !== 0) {
            $.define.func.showMessage(this.props.app.strings.mykad_error_input)
        }
    }

    onChange(id, text) {
        const { onUpdate } = this.props


        if (id === 'personalInfo.myKAD') {
            let modValue = '';
            let separator = '-';
            const pattern = /[^.]*\d+$|^.{0}$/g
            text = text.replace(/-/g, '');
            if (!pattern.test(text)) return;
            const length = text.length;
            if (length > 12) {
                modValue = text.slice(0, 14);
                modValue = text.slice(0, 6) + separator + text.slice(6, 8) + separator + text.slice(8, 12);
                this.setState({
                    value: modValue
                }, () => onUpdate(this.state.value))
            } else if (length > 8) {
                modValue = text.slice(0, 6) + separator + text.slice(6, 8) + separator + text.slice(8, length);
                this.setState({
                    value: modValue
                }, () => onUpdate(this.state.value))
            } else if (length > 6) {
                modValue = text.slice(0, 6) + separator + text.slice(6, length);
                this.setState({
                    value: modValue
                }, () => onUpdate(this.state.value))
            } else {
                this.setState({
                    value: text
                }, () => onUpdate(this.state.value))
            }
        } else if (id === 'personalInfo.postcode') {
            modValue = text.slice(0, 5);
            this.setState({
                value: modValue
            }, () => onUpdate(this.state.value))
        } else {
            this.setState({
                value: text
            }, () => onUpdate(this.state.value))
        }
    }

    showAndroidDatePicker = async () => {
        const { minYear, maxYear } = this.state;
        const pickerOptions = {
            date: new Date(),
            mode: 'spinner'
        }

        if (minYear !== undefined) pickerOptions.minDate = moment().add(minYear, 'year').toDate()
        if (maxYear !== undefined) pickerOptions.maxDate = moment().add(maxYear, 'year').toDate()

        try {
            const { action, year, month, day } = await DatePickerAndroid.open(pickerOptions);
            if (action !== DatePickerAndroid.dismissedAction) {
                this.setState({
                    value: moment().year(year).month(month).date(day).toISOString()
                }, () => {
                    this.runValidate()
                    this.props.onUpdate(this.state.value)
                })
            }
        } catch ({ code, message }) {
            console.warn('Cannot open date picker', message);
        }
    };

    async uploadImage(base64, id) {
        const data = {
            "name": id,
            'data': `data:image/jpeg;base64,${base64}`
        }
        const resp = await this.props.driver.createRequestFile(data, $.store.account.user.driverInfo.requestType)
        if (!!resp) {
            this.setState({
                ...this.state,
                uploading: false,
                value: resp[0]._id,
                imgName: resp[0].name,
                imgUrl: ''
            }, () => this.props.onUpdate(this.state.value))
        } else {
            console.log('error')
        }
    }

    async onViewImg() {
        const { imgUrl } = this.state

        if (!imgUrl) {
            const resp = await this.props.driver.getRequestFile(this.state.value, $.store.account.user.driverInfo.requestType)
            this.setState({
                imgUrl: resp
            }, () => {
                if (resp) {
                    global.$.navigator.dispatch(NavigationActions.navigate({
                        routeName: 'DriverImgViewer',
                        params: { img: resp }
                    }))
                }
            })
        } else {
            global.$.navigator.dispatch(NavigationActions.navigate({
                routeName: 'DriverImgViewer',
                params: { img: imgUrl }
            }))
        }
    }

    _getOptions = () => {
        const { strings } = this.props.app
        return {
            title: strings.album,
            storageOptions: { skipBackup: true, path: 'images' },
            quality: 0.8,
            mediaType: 'photo',
            cancelButtonTitle: strings.cancel,
            takePhotoButtonTitle: strings.take_photo,
            chooseFromLibraryButtonTitle: strings.select_from_album,
            allowsEditing: true,
            noData: false,
            maxWidth: 1000,
            maxHeight: 1000,
            permissionDenied: {
                title: strings.refuse_visit, text: strings.pls_auth,
                reTryTitle: strings.retry, okTitle: strings.okay
            }
        }
    }

    _pressActionSheet(index) {
        const { strings } = this.props.app
        if (index === 0) {
            ImagePicker.launchCamera(this._getOptions(), (response) => {
                if (response.didCancel) {
                    return
                } else if (response.error) {
                    let label = response.error.startsWith('Photo') ? strings.photo : strings.camera
                    return Alert.alert(`${strings.pls_auth_fore} ${label} ${strings.pls_auth_back} ${label}。`)
                } else {
                    this.setState({
                        media: {
                            id: `${(new Date).getTime()}`,
                            uri: response.uri,
                            feature: 'image'
                        }
                    })
                    this.uploadImage(response.data, this.state.id)
                }
            })
        }

        if (index === 1) {
            ImagePicker.launchImageLibrary(this._getOptions(), (response) => {
                if (response.didCancel) {
                    return
                } else if (response.error) {
                    let label = response.error.startsWith('Photo') ? strings.photo : strings.camera
                    return Alert.alert(`${strings.pls_auth_fore} ${label} ${strings.pls_auth_back} ${label}。`)
                } else {
                    this.setState({
                        media: {
                            id: `${(new Date).getTime()}`,
                            uri: response.uri,
                            feature: 'image'
                        }
                    })
                    this.uploadImage(response.data, this.state.id)
                }
            })
        }
    }

    render() {
        const { onUpdate, app, getVehicleModal } = this.props
        const { 
            required, 
            id, 
            value = '', 
            label = '', 
            type, 
            showTimePicker, 
            showSelect, 
            options, 
            otherValue, 
            caption, 
            urlOptions, 
            yearOptions, 
            valueCountryCode, 
            valuePhoneNo, 
            placeholder, 
            isVisible,
            isRequired,
            extraProps 
        } = this.state
        const {
            itemCont,
            title,
            fCaption,
            cfillin,
            otherCont,
            otherInput,
            inputCont,
            inputSection,
            uploadCont,
            dcIcon,
            actionCont,
            imgBtn,
            imgText,
            circle,
            checkedCircle,
            buttonContainer
        } = styles

        return !isVisible ?
            null : (
                <View style={itemCont}>
                    <View style={inputCont}>
                        <View style={inputSection}>
                            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start', width: '100%' }}>
                                <Text style={title}>{label}</Text>
                                {isRequired ?
                                    <Text style={[title, {color: 'red'}]}> *</Text>
                                    : null
                                }
                            </View>
                            <Text style={fCaption}>{caption}</Text>
                            {(type === 'file' && value) ? (
                                <View style={actionCont}>
                                    <TouchableOpacity style={[imgBtn, { marginRight: 10 }]} onPress={() => this.onViewImg()}>
                                        <Text style={imgText}>{app.strings.view_btn.toUpperCase()}</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={imgBtn} onPress={() => this.ActionSheet.show()}>
                                        <Text style={imgText}>{app.strings.edit.toUpperCase()}</Text>
                                    </TouchableOpacity>
                                </View>
                            ) : null}
                            {(() => {
                                switch (type) {
                                    case 'text': {
                                        if (id === "personalInfo.myKAD" || id === "bankInfo.accountNo") {
                                            return (
                                                <TextInput
                                                    underlineColorAndroid="transparent"
                                                    defaultValue={''}
                                                    style={cfillin}
                                                    onBlur={() => this.runValidate()}
                                                    placeholder={placeholder ? placeholder : 'Please enter here'}
                                                    returnKeyType={'done'}
                                                    keyboardType="numeric"
                                                    editable={true}
                                                    onChangeText={(text) => {
                                                        this.onChange(id, text)
                                                    }}
                                                    value={value}
                                                    {...extraProps}
                                                />
                                            );
                                        } 
                                        else {
                                            return (
                                                <TextInput
                                                    underlineColorAndroid="transparent"
                                                    defaultValue={''}
                                                    style={cfillin}
                                                    placeholder={placeholder ? placeholder : 'Please enter here'}
                                                    returnKeyType={'done'}
                                                    editable={true}
                                                    onChangeText={(text) => {
                                                        // this.setState({ value: text })
                                                        this.onChange(id, text)
                                                        onUpdate(text)
                                                    }}
                                                    value={value}
                                                    {...extraProps}
                                                />
                                            );
                                        }
                                    }
                                    case 'email':
                                        return (
                                            <TextInput
                                                underlineColorAndroid="transparent"
                                                defaultValue={''}
                                                style={cfillin}
                                                placeholder={placeholder ? placeholder : 'Please enter here'}
                                                returnKeyType={'done'}
                                                onBlur={() => this.runValidate()}
                                                editable={true}
                                                keyboardType='email-address'
                                                onChangeText={(text) => {
                                                    this.setState({ value: text })
                                                    onUpdate(text)
                                                }}
                                                value={value}
                                                {...extraProps}
                                            />
                                        );
                                    case 'select':
                                        return (
                                            <View style={otherCont}>
                                                <TouchableOpacity style={cfillin} onPress={() => this.setState({ showSelect: true })}>
                                                    <Text numberOfLines={1} ellipsizeMode='tail' style={{ color: 'rgba(0, 0, 0, 0.45)' }}>{value.length <= 0 ? app.strings.please_select : value}</Text>
                                                    <Material name={'arrow-drop-down'} size={25} />
                                                </TouchableOpacity>
                                                {value === 'Others' && (
                                                    <TextInput
                                                        underlineColorAndroid="transparent"
                                                        defaultValue={otherValue}
                                                        style={[cfillin, otherInput]}
                                                        placeholder={placeholder ? placeholder : 'Please enter here'}
                                                        returnKeyType={'done'}
                                                        editable={true}
                                                        keyboardType='default'
                                                        onChangeText={(text) => {
                                                            this.setState({ otherValue: text })
                                                            onUpdate('Others', text)
                                                        }}
                                                        value={otherValue}
                                                        {...extraProps}
                                                    />
                                                )}
                                            </View>
                                        );
                                    case 'radio':
                                        return (
                                            <View>
                                                {
                                                    options.map((item, i) => {
                                                        return (
                                                            <View key={i} style={otherCont}>
                                                                <TouchableOpacity 
                                                                    style={circle}
                                                                    onPress={() => {
                                                                        this.setState({ value: item.value })
                                                                        onUpdate(item.value)
                                                                    }}
                                                                >
                                                                    { 
                                                                        value === item.value && ( <View style={styles.checkedCircle} /> ) 
                                                                    }
                                                                </TouchableOpacity>     
                                                                <Text>{item.value}</Text>
                                                            </View>
                                                        )
                                                    })
                                                }
                                            </View>
                                        )
                                        
                                    case 'selectYear':
                                        return (
                                            <View style={otherCont}>
                                                <TouchableOpacity style={cfillin} onPress={() => this.setState({ showSelect: true })}>
                                                    <Text numberOfLines={1} ellipsizeMode='tail' style={{ color: 'rgba(0, 0, 0, 0.45)' }}>{value.length <= 0 ? app.strings.please_select : value}</Text>
                                                </TouchableOpacity>
                                            </View>
                                        );
                                    case 'phone':
                                        return (
                                            <View style={otherCont}>
                                                <TouchableOpacity style={cfillin} onPress={() => this.setState({ showSelect: true })}>
                                                    <Text numberOfLines={1} ellipsizeMode='tail' style={{ color: 'rgba(0, 0, 0, 0.45)' }}>{valueCountryCode}</Text>
                                                    <Material name={'arrow-drop-down'} size={25} />
                                                </TouchableOpacity>
                                                <TextInput
                                                    underlineColorAndroid="transparent"
                                                    defaultValue={otherValue}
                                                    style={[cfillin, otherInput]}
                                                    placeholder={placeholder ? placeholder : 'Please enter here'}
                                                    returnKeyType={'done'}
                                                    editable={true}
                                                    keyboardType='numeric'
                                                    onChangeText={(text) => {
                                                        this.setState({ valuePhoneNo: text })
                                                        // this.onChange(id, text)
                                                        onUpdate('', '', valueCountryCode, text)
                                                    }}
                                                    value={valuePhoneNo}
                                                    {...extraProps}
                                                />
                                            </View>
                                        );
                                    case 'multiLine':
                                        return (
                                            <TextInput
                                                underlineColorAndroid="transparent"
                                                defaultValue={''}
                                                multiline={true}
                                                style={[cfillin, { height: 90, paddingTop: 15, textAlignVertical: 'top' }]}
                                                placeholder={placeholder ? placeholder : 'Please enter here'}
                                                editable={true}
                                                onChangeText={(text) => {
                                                    this.setState({ value: text })
                                                    onUpdate(text)
                                                }}
                                                value={value}
                                                {...extraProps}
                                            />
                                        );
                                    case 'date':
                                        return (
                                            <TouchableOpacity style={cfillin} onPress={() => this.showTimePicker()}>
                                                <Text numberOfLines={1} ellipsizeMode='tail' style={{ color: 'rgba(0, 0, 0, 0.45)' }}>{value.length <= 0 ? app.strings.please_select : moment(value).format('DD MMM YYYY')}</Text>
                                                <Material name={'arrow-drop-down'} size={25} />
                                            </TouchableOpacity>
                                        );
                                    default:
                                        return null
                                }
                            })()}
                            <CmpTimePicker
                                visible={showTimePicker}
                                time={new Date()}
                                minYear={this.state.minYear}
                                maxYear={this.state.maxYear}
                                strings={app.strings}
                                dateChange={(type, time) => {
                                    console.log(type, time)
                                }}
                                onHide={() => this.setState({
                                    showTimePicker: false
                                })}
                                onConfirm={(value) => {
                                    this.setState({ value, showTimePicker: false })
                                    this.runValidate()
                                    onUpdate(value)
                                }}
                            />
                            {options && type === 'select' && (
                                <CmpSelect
                                    visible={showSelect}
                                    option={options.reduce((acc, curr) => [...acc, curr.value], [])}
                                    onHide={() => this.setState({ showSelect: false })}
                                    onConfirm={(value) => {
                                        this.setState({ value, showSelect: false })
                                        onUpdate(value)
                                    }}
                                />
                            )}
                            {type === 'phone' && (
                                <CmpSelect
                                    visible={showSelect}
                                    option={options.reduce((acc, curr) => [...acc, curr.value], [])}
                                    onHide={() => this.setState({ showSelect: false })}
                                    onConfirm={(value) => {
                                        this.setState({ valueCountryCode: value, showSelect: false })
                                        onUpdate('', '', value)
                                    }}
                                />
                            )}
                            {yearOptions && (
                                <CmpSelect
                                    visible={showSelect}
                                    option={yearOptions}
                                    onHide={() => this.setState({ showSelect: false })}
                                    onConfirm={(value) => {
                                        this.setState({ value, showSelect: false })
                                        onUpdate(value)
                                    }}
                                />

                            )}
                            {urlOptions && (
                                <CmpSelect
                                    visible={showSelect}
                                    option={urlOptions}
                                    onHide={() => this.setState({ showSelect: false })}
                                    onConfirm={(value) => {
                                        this.setState({ value, showSelect: false })
                                        onUpdate(value)
                                        if (id === 'vehicleInfo.manufacturer') {
                                            getVehicleModal(value)
                                        }
                                    }}
                                />
                            )}
                            {id === 'vehicleInfo.model' && this.props.driverVerification.dvVehicleModels && this.props.driverVerification.dvVehicleModels.length > 0 && (
                                <CmpSelect
                                    visible={showSelect}
                                    option={this.props.driverVerification.dvVehicleModels}
                                    onHide={() => this.setState({
                                        showSelect: false
                                    })}
                                    onConfirm={(value) => {
                                        this.setState({ value, showSelect: false })
                                        onUpdate(value)
                                    }}
                                />
                            )}
                        </View>
                        {(type === 'file' && !value) ? (
                            <TouchableOpacity style={uploadCont} onPress={() => this.ActionSheet.show()}>
                                <Image style={dcIcon} source={Resources.image.upload} />
                            </TouchableOpacity>
                        ) : null}
                        {(type === 'file' && value) ? (
                            <View style={[uploadCont, { backgroundColor: DCColor.BGColor('primary-1') }]}>
                                <Image style={dcIcon} source={Resources.image.tick_icon} />
                            </View>
                        ) : null}
                    </View>
                    <ActionSheet
                        ref={e => this.ActionSheet = e}
                        options={[app.strings.take_photo, app.strings.select_from_album, app.strings.cancel]}
                        cancelButtonIndex={2}
                        onPress={this._pressActionSheet.bind(this)}
                    />
                </View>
            )
    }
}

const styles = StyleSheet.create({
    bigcont: {
        flexDirection: 'column',
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        flexWrap: 'nowrap',
        flex: 1,
        width: '100%',
        backgroundColor: 'rgba(0, 0, 0, 0.05)',
    },
    dcForm: {
        flex: 1,
        width: '100%',
        paddingHorizontal: 20
    },
    itemCont: {
        flexDirection: 'column',
        width: '100%',
        marginBottom: 10,
        marginTop: 10,
    },
    inputCont: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    inputSection: {
        flex: 1
    },
    titleCont: {
        flexDirection: 'column',
        justifyContent: 'flex-start',
        width: '100%',
        padding: 20,
        paddingBottom: 25,
        backgroundColor: 'white'
    },
    pageTitle: {
        fontSize: 25,
        opacity: 1,
        color: '#000',
        fontWeight: 'bold',
    },
    pageCaption: {
        fontSize: TextFont.TextSize(13),
        opacity: 0.8,
        color: 'rgba(0, 0, 0, 0.65)',
    },
    title: {
        fontSize: TextFont.TextSize(16),
        opacity: 0.8,
        color: '#000',
    },
    fCaption: {
        width: '100%',
        fontSize: TextFont.TextSize(13),
        opacity: 0.8,
        color: 'rgba(0, 0, 0, 0.65)',
    },
    cfillin: {
        flex: 1,
        height: 45,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: 'rgba(0, 0, 0, 0.1)',
        borderRadius: 5,
        marginTop: 5,
        paddingHorizontal: 15
    },
    otherCont: {
        flex: 1,
        height: 45,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start'
    },
    otherInput: {
        flex: 3,
        marginLeft: 10
    },
    bottom: {
        flexDirection: 'row',
        alignItems: 'center',
        width,
        marginTop: 20
    },
    dcBtn: {
        flex: 0.45,
        height: 44,
        borderRadius: 30,
        backgroundColor: DCColor.BGColor('green'),
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    btnText: {
        padding: 5,
        fontSize: TextFont.TextSize(18),
        fontWeight: '800',
        color: 'black'
    },
    dcBackDrop: {
        width,
        height,
        position: 'absolute',
        top: 0,
        left: 0,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(0 ,0, 0, 0.75)'
    },
    uploadCont: {
        width: 45,
        height: 45,
        borderWidth: 1,
        borderColor: 'rgba(0, 0, 0, 0.1)',
        borderRadius: 35,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 15
    },
    dcIcon: {
        height: 12,
        width: 12,
        resizeMode: 'contain',
        tintColor: 'rgba(0, 0, 0 , 0.5)'
    },
    actionCont: {
        width: '100%',
        marginTop: 10,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    imgBtn: {
        height: 30,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: 'rgba(0, 0, 0 ,0.15)',
        paddingHorizontal: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    imgText: {
        fontSize: TextFont.TextSize(14),
        fontWeight: '700',
        color: 'rgba(0, 0, 0, 0.75)'
    },
    circle: {
        height: 20,
        width: 20,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: DCColor.BGColor('primary-2'),
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 10
    },
    checkedCircle: {
        width: 14,
        height: 14,
        borderRadius: 7,
        backgroundColor: DCColor.BGColor('primary-1'),
    },
    buttonContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
})