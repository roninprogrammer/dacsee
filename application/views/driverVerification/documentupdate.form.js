import React, { Component } from 'react'
import { Text, View, StyleSheet, TouchableOpacity, SafeAreaView, Image , FlatList, TextInput } from 'react-native'
import { inject, observer } from 'mobx-react'
import { Screen, TextFont, DCColor } from 'dacsee-utils'
import Resources from 'dacsee-resources'
import moment from 'moment'
import CmpTimePicker from '../../components/cmpTimePicker'
import CmpSelect from '../../components/cmpSelect'
import ActionSheet from 'react-native-actionsheet'
import Material from 'react-native-vector-icons/MaterialIcons'
import ImagePicker from 'react-native-image-picker'
import { NavigationActions } from 'react-navigation'


const { width } = Screen.window

const styles = StyleSheet.create({
    mainCont: {
        flex: 1,
        width: '100%',
        paddingHorizontal: 20
    },
    actionCont: {
        width,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 15,
        backgroundColor: DCColor.BGColor('white'),
        borderTopColor: 'rgba(0, 0, 0, 0.1)',
        borderTopWidth: 2
    },
    actionBox: {
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      width: '100%'
  },
    dcBtn: {
        backgroundColor: DCColor.BGColor('disable'),
        flex: 1,
        height: 45,
        borderRadius: 8,
        alignItems: 'center',
        justifyContent: 'center'
    },
    btnActive: {
        backgroundColor: DCColor.BGColor('primary-1'),
        borderColor: "transparent",
        borderWidth: 0
    },
    btnText: {
        fontSize: TextFont.TextSize(16),
        fontWeight: '700',
        color: 'rgba(0, 0, 0, 0.75)',
    },
    itemCont: {
      flexDirection: 'column',
      width: '100%',
      marginBottom: 10,
      marginTop: 10,
    },
    title: {
      width: '100%',
      fontSize: TextFont.TextSize(16),
      opacity: 0.8,
      color: '#000',
    },
    fCaption: {
      width: '100%',
      fontSize: TextFont.TextSize(13),
      opacity: 0.8,
      color: 'rgba(0, 0, 0, 0.65)',
    },
    cfillin: {
        flex: 1,
        height: 45,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: 'rgba(0, 0, 0, 0.1)',
        borderRadius: 5,
        marginTop: 5,
        paddingHorizontal: 15
    },
    otherCont: {
        flex: 1,
        height: 45,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start'
    },
    otherInput: {
        flex: 3,
        marginLeft: 10
    },
    inputCont: {
      width: '100%',
      flexDirection: 'row',
      justifyContent: 'flex-start',
      alignItems: 'center'
    },
    inputSection: {
        flex: 1
    },
    uploadCont: {
      width: 45,
      height: 45,
      borderWidth: 1,
      borderColor: 'rgba(0, 0, 0, 0.1)',
      borderRadius: 35,
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      marginLeft: 15
    },
    dcIcon: {
        height: 12,
        width: 12,
        resizeMode: 'contain',
        tintColor: 'rgba(0, 0, 0 , 0.5)'
    },
    actionCont2: {
        width: '100%',
        marginTop: 10,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    imgBtn: {
        height: 30,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: 'rgba(0, 0, 0 ,0.15)',
        paddingHorizontal: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    imgText: {
        fontSize: TextFont.TextSize(14),
        fontWeight: '700',
        color: 'rgba(0, 0, 0, 0.75)'
    }
})

@inject('app', 'account', 'driver', 'driverVerification')
@observer
export default class DriverUpdateDocument extends Component {
    static navigationOptions = ({ navigation }) => {
        const { strings } = $.store.app
        return {
            drawerLockMode: 'locked-closed',
            title: strings.update_driver_document,
        }
    }

    constructor(props) {
        super(props)
    }

    async onSubmit() {
        const { driverVerification, app } = this.props

        const res = await driverVerification.submitRequest()

        if (res) {
            driverVerification.clearDriverVerificationData()
            this.props.navigation.navigate('Main')
        }
    }

    render() {
        const {
          mainCont,
          actionCont,
          dcBtn,
          btnText,
          actionBox,
          btnActive,
        } = styles
        const { driverVerification } = this.props 
        const pages = this.props.driverVerification.dvPages
        const { strings } = this.props.app
        const { dvCurrentPageInfo, dvCurrentPage, nextLabel, dvForm , showAuthLetter} = this.props.driverVerification
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: 'white' }}>
                <FlatList
                    style={mainCont}
                    data={dvCurrentPageInfo.fields}
                    extraData={this.state}
                    keyExtractor={(item) => item.id}
                    renderItem={({ index, item }) => (
                        <VerifyItem
                            data={item}
                            onUpdate={(value, otherValue, countryCode, phoneNo) => driverVerification.updateFields(index, value, otherValue, countryCode, phoneNo, item.id)}
                        />
                    )}
                />
                <View style={actionCont}>
                  <View style={actionBox}>
                      <TouchableOpacity style={[dcBtn, btnActive]} onPress={() => this.onSubmit()}>
                          <Text style={btnText}>{strings.sub.toUpperCase()}</Text>
                      </TouchableOpacity>
                  </View>
                </View>
            </SafeAreaView>
            ) 
    }
}

@inject('app', 'account', 'driver', 'driverVerification')
@observer
class VerifyItem extends Component {
    constructor(props) {
        super(props)
        this.state = {
            id: '',
            value: '',
            label: '',
            type: '',
            showTimePicker: false,
            showSelect: false,
            minYear: '',
            maxYear: '',
        }
    }

    componentDidMount() {
        const { data } = this.props
        const { id, value = '', label = '', type, options, caption, api, otherValue, yearGap, minYear, maxYear } = data
        if (api && id !== 'vehicleInfo.model') {
            axios.get(api.url, {
                headers: {
                    Authorization: $.store.account.authToken
                }
            }).then((res) => {
                this.setState({
                    id,
                    value,
                    label,
                    type,
                    urlOptions: res.data,
                })
            }).catch((e) => {
                console.log(e);
            })
        } else if (maxYear && maxYear.length > 0) {
            this.setState({
                ...this.state,
                maxYear
            })
        } else if (minYear && minYear.length > 0) {
            this.setState({
                ...this.state,
                minYear
            })
        }  else if (type === 'selectYear') {
            const currentYear = (new Date()).getFullYear();
            let startYear = currentYear - yearGap;
            let years = [];

            while (startYear < currentYear + 1) {
                years.push(startYear++)
            }

            this.setState({
                id,
                value,
                label,
                type,
                yearOptions: years,
            })
        } else {
            this.setState({
                id,
                value,
                label,
                type,
                options,
                caption,
                otherValue
            })
        }
    }

    componentDidUpdate(nextProps, nextState) {
        if (nextProps.data.value !== nextState.value) {
            const { value } = nextProps.data
            this.setState({ value })
        } else if (nextProps.data.minYear !== nextState.minYear) {
            const { minYear } = nextProps.data
            this.setState({ minYear })
        } else if (nextProps.data.maxYear !== nextState.maxYear) {
            const { maxYear } = nextProps.data
            this.setState({ maxYear })
        } else if (nextProps.data.otherValue !== nextState.otherValue) {
            const { otherValue } = nextProps.data
            this.setState({ otherValue })
        }
    }

    showTimePicker() {
        if (Platform.OS === 'ios') {
            this.setState({
                showTimePicker: true,
            })
        } else if (Platform.OS === 'android') {
            this.showAndroidDatePicker()
        }
    }

    runValidate() {
        const { data } = this.props
        const { type, id, minYear, maxYear } = data
        const { value } = this.state

        if (id === "personalInfo.dob" && (maxYear || minYear)) {
            if (minYear) {
                const minDate = moment().add(minYear, 'year')
                const valueDate = moment(value)
                if (valueDate && value < minDate) {
                    $.define.func.showMessage(this.props.app.too_old_caption)
                }
            }
            if (maxYear) {
                const maxDate = moment().add(maxYear, 'year')
                const valueDate = moment(value)
                if (value && valueDate > maxDate) {
                    $.define.func.showMessage(this.props.app.too_young_caption)
                }
            }
        } 
    }

    onChange(id, text) {
        const { onUpdate } = this.props

        this.setState({ value: text }, () => onUpdate(this.state.value))
    }

    showAndroidDatePicker = async () => {
        const { minYear, maxYear } = this.state;
        const pickerOptions = {
            date: new Date(),
            mode: 'spinner'
        }
        if (minYear) pickerOptions.minDate = moment().add(minYear, 'year').toDate()
        if (maxYear) pickerOptions.maxDate = moment().add(maxYear, 'year').toDate()
        try {
            const { action, year, month, day } = await DatePickerAndroid.open(pickerOptions);
            if (action !== DatePickerAndroid.dismissedAction) {
                this.setState({
                    value: moment().year(year).month(month).date(day).toISOString()
                }, () => {
                    this.runValidate()
                    this.props.onUpdate(this.state.value)
                    this.props.onUpdate(this.state.value)
                })
            }
        } catch ({ code, message }) {
            console.warn('Cannot open date picker', message);
        }
    };

    async uploadImage(base64, id) {
        const data = {
            "name": id,
            'data': `data:image/jpeg;base64,${base64}`
        }
        const resp = await this.props.driver.createRequestFile(data)
        if (!!resp) {
            this.setState({
                ...this.state,
                uploading: false,
                value: resp[0]._id,
                imgName: resp[0].name,
                imgUrl: ''
            }, () => this.props.onUpdate(this.state.value))
        } else {
            console.log('error')
        }
    }

    async onViewImg() {
        const { imgUrl } = this.state

        if (!imgUrl) {
            const resp = await this.props.driver.getRequestFile(this.state.value)
            this.setState({
                imgUrl: resp
            }, () => {
                if (resp) {
                    global.$.navigator.dispatch(NavigationActions.navigate({
                        routeName: 'DriverImgViewer',
                        params: { img: resp }
                    }))
                }
            })
        } else {
            global.$.navigator.dispatch(NavigationActions.navigate({
                routeName: 'DriverImgViewer',
                params: { img: imgUrl }
            }))
        }
    }

    _getOptions = () => {
        const { strings } = this.props.app
        return {
            title: strings.album,
            storageOptions: { skipBackup: true, path: 'images' },
            quality: 0.8,
            mediaType: 'photo',
            cancelButtonTitle: strings.cancel,
            takePhotoButtonTitle: strings.take_photo,
            chooseFromLibraryButtonTitle: strings.select_from_album,
            allowsEditing: true,
            noData: false,
            maxWidth: 1000,
            maxHeight: 1000,
            permissionDenied: {
                title: strings.refuse_visit, text: strings.pls_auth,
                reTryTitle: strings.retry, okTitle: strings.okay
            }
        }
    }

    _pressActionSheet(index) {
        const { strings } = this.props.app
        if (index === 0) {
            ImagePicker.launchCamera(this._getOptions(), (response) => {
                if (response.didCancel) {
                    return
                } else if (response.error) {
                    let label = response.error.startsWith('Photo') ? strings.photo : strings.camera
                    return Alert.alert(`${strings.pls_auth_fore} ${label} ${strings.pls_auth_back} ${label}。`)
                } else {
                    this.setState({
                        media: {
                            id: `${(new Date).getTime()}`,
                            uri: response.uri,
                            feature: 'image'
                        }
                    })
                    this.uploadImage(response.data, this.state.id)
                }
            })
        }

        if (index === 1) {
            ImagePicker.launchImageLibrary(this._getOptions(), (response) => {
                if (response.didCancel) {
                    return
                } else if (response.error) {
                    let label = response.error.startsWith('Photo') ? strings.photo : strings.camera
                    return Alert.alert(`${strings.pls_auth_fore} ${label} ${strings.pls_auth_back} ${label}。`)
                } else {
                    this.setState({
                        media: {
                            id: `${(new Date).getTime()}`,
                            uri: response.uri,
                            feature: 'image'
                        }
                    })
                    this.uploadImage(response.data, this.state.id)
                }
            })
        }
    }

    render() {
        const { onUpdate, app } = this.props
        const { id, value = '', label = '', type, showTimePicker, showSelect, options, otherValue, caption, yearOptions} = this.state
        const {
            itemCont,
            title,
            fCaption,
            cfillin,
            otherCont,
            otherInput,
            inputCont,
            inputSection,
            uploadCont,
            dcIcon,
            actionCont2,
            imgBtn,
            imgText
        } = styles

        return (
                <View style={itemCont}>
                    <View style={inputCont}>
                        <View style={inputSection}>
                            <Text style={title}>{label}</Text>
                            <Text style={fCaption}>{caption}</Text>
                            {(type === 'file' && value) ? (
                                <View style={actionCont2}>
                                    <TouchableOpacity style={[imgBtn, { marginRight: 10 }]} onPress={() => this.onViewImg()}>
                                        <Text style={imgText}>{app.strings.view_btn.toUpperCase()}</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={imgBtn} onPress={() => this.ActionSheet.show()}>
                                        <Text style={imgText}>{app.strings.edit.toUpperCase()}</Text>
                                    </TouchableOpacity>
                                </View>
                            ) : null}
                            {(() => {
                                switch (type) {
                                    case 'select':
                                        return (
                                            <View style={otherCont}>
                                                <TouchableOpacity style={cfillin} onPress={() => this.setState({ showSelect: true })}>
                                                    <Text numberOfLines={1} ellipsizeMode='tail' style={{ color: 'rgba(0, 0, 0, 0.45)' }}>{value.length <= 0 ? app.strings.please_select : value}</Text>
                                                    <Material name={'arrow-drop-down'} size={25}/>
                                                </TouchableOpacity>
                                                {value === 'Others' && (
                                                    <TextInput
                                                        underlineColorAndroid="transparent"
                                                        defaultValue={otherValue}
                                                        style={[cfillin, otherInput]}
                                                        placeholder={'Please enter here'}
                                                        returnKeyType={'done'}
                                                        editable={true}
                                                        keyboardType='default'
                                                        onChangeText={(text) => {
                                                            this.setState({ otherValue: text })
                                                            onUpdate('Others', text)
                                                        }}
                                                        value={otherValue}
                                                    />
                                                )}
                                            </View>
                                        );
                                    case 'date':
                                        return (
                                            <TouchableOpacity style={cfillin} onPress={() => this.showTimePicker()}>
                                                <Text numberOfLines={1} ellipsizeMode='tail' style={{ color: 'rgba(0, 0, 0, 0.45)' }}>{value.length <= 0 ? app.strings.please_select : moment(value).format('DD MMM YYYY')}</Text>
                                                <Material name={'arrow-drop-down'} size={25}/>
                                            </TouchableOpacity>
                                        );
                                    default:
                                        return null
                                }
                            })()}
                            <CmpTimePicker
                                visible={showTimePicker}
                                time={new Date()}
                                minYear={this.state.minYear}
                                maxYear={this.state.maxYear}
                                strings={app.strings}
                                dateChange={(type, time) => {
                                    console.log(type, time)
                                }}
                                onHide={() => this.setState({
                                    showTimePicker: false
                                })}
                                onConfirm={(value) => {
                                    this.setState({ value, showTimePicker: false })
                                    this.runValidate()
                                    onUpdate(value)
                                }}
                            />
                            {options && type !== 'phone' && (
                                <CmpSelect
                                    visible={showSelect}
                                    option={options.reduce((acc, curr) => [...acc, curr.value], [])}
                                    onHide={() => this.setState({ showSelect: false })}
                                    onConfirm={(value) => {
                                        this.setState({ value, showSelect: false })
                                        onUpdate(value)
                                    }}
                                />
                            )}
                            {yearOptions && (
                                <CmpSelect
                                    visible={showSelect}
                                    option={yearOptions}
                                    onHide={() => this.setState({ showSelect: false })}
                                    onConfirm={(value) => {
                                        this.setState({ value, showSelect: false })
                                        onUpdate(value)
                                    }}
                                />
                            )}
                        </View>
                        {(type === 'file' && !value) ? (
                            <TouchableOpacity style={uploadCont} onPress={() => this.ActionSheet.show()}>
                                <Image style={dcIcon} source={Resources.image.upload} />
                            </TouchableOpacity>
                        ) : null}
                        {(type === 'file' && value) ? (
                            <View style={[uploadCont, { backgroundColor: DCColor.BGColor('primary-1') }]}>
                                <Image style={dcIcon} source={Resources.image.tick_icon} />
                            </View>
                        ) : null}
                    </View>
                    <ActionSheet
                        ref={e => this.ActionSheet = e}
                        options={[app.strings.take_photo, app.strings.select_from_album, app.strings.cancel]}
                        cancelButtonIndex={2}
                        onPress={this._pressActionSheet.bind(this)}
                    />
                </View>
            )
    }
}