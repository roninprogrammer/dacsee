import React, { Component } from 'react'
import { Text, View, StyleSheet, TouchableOpacity, SafeAreaView, ScrollView, Image } from 'react-native'
import { inject, observer } from 'mobx-react'
import moment from 'moment'
import { NavigationActions } from 'react-navigation'
import { Screen, TextFont, DCColor, Icons } from 'dacsee-utils'
import Resources from 'dacsee-resources'

const { width } = Screen.window

const styles = StyleSheet.create({
    mainCont: {
        width,
    },
    titleCont: {
        width,
        borderBottomWidth: 1,
        borderTopWidth: 1,
        borderColor: 'rgba(0, 0, 0, 0.1)',
        padding: 20,
    },
    fTitle: {
        textAlign: 'left',
        fontSize: TextFont.TextSize(16),
        fontWeight: '600',
        color: 'rgba(0, 0, 0, 0.75)'
    },
    dcForm: {
        width,
        padding: 20,
        backgroundColor: 'rgba(0, 0, 0, 0.05)'
    },
    dcCol: {
        width: '100%',
        marginBottom: 15
    },
    fLabel: {
        flex: 1,
        fontSize: TextFont.TextSize(14),
        fontWeight: '600',
        color: 'rgba(0, 0, 0, 0.75)',
        textAlign: 'left'
    },
    dataCont: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start'
    },
    fData: {
        fontSize: TextFont.TextSize(18),
        fontWeight: '800',
        color: 'rgba(0, 0, 0, 0.75)',
        textAlign: 'left',
    },
    imgCont: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    imgBox: {
        height: 50,
        width: 50,
        borderColor: 'rgba(0, 0, 0, 0.1)',
        borderWidth: 1,
        borderRadius: 50,
        alignItems: 'center',
        justifyContent: 'center'
    },
    actionCont: {
        width,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 15,
        backgroundColor: DCColor.BGColor('dark')
    },
    actionBox: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%'
    },
    toogleCont: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 10
    },
    toogleBtn: {
        width: 30,
        height: 30,
        borderRadius: 5,
        borderColor: DCColor.BGColor('disable'),
        borderWidth: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    dcIcon: {
        height: 10,
        width: 10,
        resizeMode: 'contain'
    },
    toogleText: {
        fontSize: TextFont.TextSize(10),
        fontWeight: '400',
        color: 'white',
        flex: 1,
        marginRight: 10
    },
    dcBtn: {
        backgroundColor: DCColor.BGColor('disable'),
        flex: 1,
        height: 45,
        borderRadius: 8,
        alignItems: 'center',
        justifyContent: 'center'
    },
    btnActive: {
        backgroundColor: DCColor.BGColor('primary-1'),
        borderColor: "transparent",
        borderWidth: 0
    },
    btnText: {
        fontSize: TextFont.TextSize(16),
        fontWeight: '700',
        color: 'rgba(0, 0, 0, 0.75)',
    }
})

const SummaryItem = ({ showAuthLetter, id, label, value, type, otherValue, valueCountryCode, valuePhoneNo, formType }) => {
    const {
        dcCol,
        fLabel,
        dataCont,
        fData,
        imgCont,
        imgBox,
    } = styles

    return (id === "vehicleInfo.authorizationLetter" && !showAuthLetter) ? null : (
        <View style={type === 'file' && value.length > 0 ? [dcCol, imgCont] : [dcCol]}>
            <Text style={fLabel}>{label}</Text>
            {type !== 'file' && type !== 'phone' && type !== 'date' ? (
                <View style={dataCont}>
                    <Text style={fData}>{otherValue ? `${value} - ` : value ? value : 'Nil'}</Text>
                    {otherValue ? 
                        <Text style={fData}>{otherValue}</Text> 
                        : null
                    }
                </View>
            ) : null}
            {type === 'phone' ? (
                <Text style={fData}>{valueCountryCode ? valueCountryCode : 'Nil'}{valuePhoneNo ? valuePhoneNo : ''}</Text>
            ) : null}
            {type === 'date' ? (
                <Text style={fData}>{moment(value).format('DD-MM-YYYY')}</Text>
            ) : null}
            {type === 'file' && value.length > 0 ? (
                <TouchableOpacity style={imgBox}
                    onPress={() => global.$.navigator.dispatch(NavigationActions.navigate({
                        routeName: 'DriverImgViewer',
                        params: { img: '', id: value, formType: formType}
                    }))}>
                    {Icons.Generator.Material('search', 18, 'black')}
                </TouchableOpacity>
            ) : null}
            {type === 'file' && value.length <= 0 ? <Text style={fData}>{$.store.app.strings.no_photo_uploaded}</Text> : null}
        </View>
    )
}

@inject('app', 'account', 'driver', 'driverVerification')
@observer
export default class DriverVerificationSummary extends Component {
    static navigationOptions = ({ navigation }) => {
        const { strings } = $.store.app
        return {
            drawerLockMode: 'locked-closed',
            title: strings.summary,
        }
    }

    constructor(props) {
        super(props)
        this.state = {
            agreedTerms: false,
        }
    }


    async onSubmit() {
        const { agreedTerms } = this.state
        const { driverVerification, app } = this.props
        if (agreedTerms) {
            const res = await driverVerification.submitRequest('driverRegistration')

            if (res) {
                driverVerification.clearDriverVerificationData()
                this.props.navigation.navigate('Main')
            }
        } else {
            $.define.func.showMessage(app.strings.agree_terms)
        }
    }

    render() {
        const {
            mainCont,
            titleCont,
            fTitle,
            dcForm,
            actionCont,
            dcBtn,
            btnText,
            toogleCont,
            actionBox,
            btnActive,
            toogleBtn,
            dcIcon,
            toogleText
        } = styles
        const { agreedTerms } = this.state
        const { strings } = this.props.app
        const { showAuthLetter, dvForm } = this.props.driverVerification
        const formType = this.props.navigation.state.params.formType

        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: 'white' }}>
                <ScrollView style={mainCont}>
                    {dvForm.form && dvForm.form.pages.map((d, idx) => (
                        <View key={idx}>
                            <View style={titleCont}>
                                <Text style={fTitle}>{d.label.toUpperCase()}</Text>
                            </View>
                            <View style={dcForm}>
                                {d.fields.map((f, idx) => (
                                    <SummaryItem
                                        id={f.id}
                                        showAuthLetter={showAuthLetter}
                                        key={idx}
                                        label={f.label}
                                        value={f.value}
                                        type={f.type}
                                        otherValue={f.otherValue}
                                        valueCountryCode={f.valueCountryCode}
                                        valuePhoneNo={f.valuePhoneNo}
                                        formType={formType}
                                    />
                                ))}
                            </View>
                        </View>
                    ))}
                </ScrollView>
                <View style={actionCont}>
                    <View style={toogleCont}>
                        <Text style={toogleText}>{dvForm && dvForm.form && dvForm.form.termsAndConditions && dvForm.form.termsAndConditions.template}</Text>
                        <TouchableOpacity style={agreedTerms ? [toogleBtn, btnActive] : toogleBtn} onPress={() => this.setState({ agreedTerms: !agreedTerms })}>
                            {agreedTerms ? (
                                <Image source={Resources.image.tick_icon} style={dcIcon} />
                            ) : null}
                        </TouchableOpacity>
                    </View>
                    <View style={actionBox}>
                        <TouchableOpacity style={[dcBtn, { marginRight: 10 }]} onPress={() => this.props.navigation.goBack()}>
                            <Text style={btnText}>{strings.back_btn.toUpperCase()}</Text>
                        </TouchableOpacity>
                        {!agreedTerms ? (
                            <TouchableOpacity style={dcBtn} onPress={() => this.onSubmit()}>
                                <Text style={btnText}>{strings.sub.toUpperCase()}</Text>
                            </TouchableOpacity>
                        ) :
                            <TouchableOpacity style={[dcBtn, btnActive]} onPress={() => this.onSubmit()}>
                                <Text style={btnText}>{strings.sub.toUpperCase()}</Text>
                            </TouchableOpacity>
                        }
                    </View>
                </View>
            </SafeAreaView>
        )
    }
}