import React, { Component } from 'react'
import { Text, View, StyleSheet, TouchableOpacity, SafeAreaView, Image , Alert } from 'react-native'
import { inject, observer } from 'mobx-react'
import { Screen, TextFont, DCColor } from 'dacsee-utils'
import Resources from 'dacsee-resources'
import NotificationBar from '../../components/notificationBar'
import VerifyPage from './verification.page';

const { width } = Screen.window

const styles = StyleSheet.create({
    actionCont: {
        width,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 15,
        backgroundColor: DCColor.BGColor('white'),
        borderTopColor: 'rgba(0, 0, 0, 0.1)',
        borderTopWidth: 2
    },
    actionBox: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%'
    },
    dcBtn: {
        backgroundColor: DCColor.BGColor('disable'),
        flex: 1,
        height: 45,
        borderRadius: 8,
        alignItems: 'center',
        justifyContent: 'center'
    },
    btnActive: {
        backgroundColor: DCColor.BGColor('primary-1'),
        borderColor: "transparent",
        borderWidth: 0
    },
    btnText: {
        fontSize: TextFont.TextSize(16),
        fontWeight: '700',
        color: 'rgba(0, 0, 0, 0.75)',
    },
    dcImg: {
        width: width / 2,
        height: width / 2,
        resizeMode: 'contain',
        marginBottom: 30
    },
    uploadedTitle: {
        fontSize: TextFont.TextSize(22),
        fontWeight: '800',
        color: 'rgba(0, 0 , 0, 0.75)',
        marginBottom: 10,
        width: '100%',
        textAlign: 'center'
    },
    uploadedDesc: {
        fontSize: TextFont.TextSize(15),
        fontWeight: '500',
        color: 'rgba(0, 0,0 ,0.45)',
        width: '100%',
        textAlign: 'center',
        paddingHorizontal: 20
    }
})

@inject('app', 'account', 'driver', 'driverVerification')
@observer
export default class PSVsubmission extends Component {
    static navigationOptions = ({ navigation }) => {
        const { strings } = $.store.app
        return {
            drawerLockMode: 'locked-closed',
            title: strings.psv_submission,
        }
    }

    constructor(props) {
        super(props)
    }


    async onSubmit() {
        const { driverVerification, app } = this.props

        let data = await driverVerification.validateVerificationField()
        if (data) {
            const resp = await driverVerification.updateRequest($.store.driverVerification.dvForm, 'psvRegistration')
            if (resp) {
                Alert.alert($.store.app.strings.reminder, $.store.app.strings.psv_confirmation_desc, [
                    { text: $.store.app.strings.cancel },
                    {
                    text: $.store.app.strings.confirm,
                    onPress: async () => {
                            const res = await driverVerification.submitRequest('psvRegistration')

                            if (res) {
                                driverVerification.clearDriverVerificationData()
                                this.props.navigation.navigate('Main') 
                            }
                        } 
                    }  
                ])
            }
        }
    }

    render() {
        const {
            actionCont,
            dcBtn,
            btnText,
            actionBox,
            btnActive,
            dcImg,
            uploadedTitle,
            uploadedDesc
        } = styles
        const pages = this.props.driverVerification.dvPages
        const { strings } = this.props.app
        const { dvCurrentPageInfo, dvCurrentPage, nextLabel, dvForm } = this.props.driverVerification
        const { formType } = dvForm
        

        return pages && pages.length ? (
            <SafeAreaView style={{ flex: 1, backgroundColor: 'white' }}>
                {dvForm && dvForm.rejectedReason ? (
                    <View style={{ width: '100%', padding: 5, backgroundColor: 'rgba(0, 0, 0, 0.05)' }}>
                        <NotificationBar
                            icon={Resources.image.attention_icon}
                            label={`${strings.reject_reason} ${dvForm.rejectedReason}`}
                        />
                    </View>
                ) : null}
                <VerifyPage
                    pageInfo={dvCurrentPageInfo}
                    formType={formType}
                />
                <View style={actionCont}>
                    <View style={actionBox}>
                        <TouchableOpacity style={[dcBtn, btnActive]} onPress={() => this.onSubmit()}>
                            <Text style={btnText}>{strings.sub.toUpperCase()}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </SafeAreaView>
        ) : (
            <SafeAreaView style={{ flex: 1, backgroundColor: 'white', justifyContent: 'center', alignItems: 'center' }}>
            {dvForm && (dvForm.status === 'pendingPsvVerification' ) ? (
                <View style={{ width, backgroundColor: 'white', justifyContent: 'center', alignItems: 'center' }}>
                    <Image source={Resources.image.verification_complete} style={dcImg} />
                    <Text style={uploadedTitle}>{strings.congratulations}</Text>
                    <Text style={uploadedDesc}>{strings.psv_congratulations_desc}</Text>
                </View>
            ) :  (dvForm.status === 'pendingAPADSubmission' || dvForm.status === 'pendingAPADVerification' || dvForm.status === 'APADCompleted' ? (
                <View style={{ width, backgroundColor: 'white', justifyContent: 'center', alignItems: 'center' }}>
                    <Image source={Resources.image.verification_complete} style={dcImg} />
                    <Text style={uploadedTitle}>{strings.congratulations}</Text>
                    <Text style={uploadedDesc}>{strings.verification_form_evpDesciption}</Text>
                </View>
            ) : null
            )}
            </SafeAreaView>
        )
    }
}
