import React, { Component } from 'react'
import { Text, View, TouchableOpacity, Modal, StyleSheet, Image } from 'react-native'
import { Screen, TextFont, DCColor } from 'dacsee-utils'
import Resources from "dacsee-resources"
import _ from 'lodash'
import { inject, observer } from 'mobx-react'

const { height, width } = Screen.window

const styles = StyleSheet.create({
  backdropStyle: {
    width,
    height,
    backgroundColor: 'rgba(57, 56, 67, 0.2)',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20
  },
  dcCard: {
    backgroundColor: 'white',
    borderRadius: 8,
    minWidth: width * 0.8
  },
  headCont: {
    padding: 30,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%'
  },
  dcImg: {
    height: 100,
    width: 100,
    resizeMode: 'contain',
    marginBottom: 20
  },
  fTitle: {
    fontSize: TextFont.TextSize(18),
    fontWeight: '600',
    color: 'rgba(0, 0, 0, 0.75)',
    textAlign: 'center',
    marginBottom: 10
  },
  fCaption: {
    fontSize: TextFont.TextSize(14),
    fontWeight: '400',
    color: 'rgba(0, 0, 0, 0.45)',
    textAlign: 'center',
  },
  bodyCont: {
    width: '100%',
    backgroundColor: 'rgba(0, 0, 0, 0.05)',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 30
  },
  dcCol: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingTop: 3,
    paddingBottom: 3,
  },
  fBody: {
    fontSize: TextFont.TextSize(14),
    fontWeight: '500',
    color: 'rgba(0, 0, 0, 0.75)',
    textAlign: 'center',
  },
  dcDot: {
    fontSize: TextFont.TextSize(14),
    fontWeight: '500',
    color: 'rgba(0, 0, 0, 0.75)',
    textAlign: 'center',
    marginRight: 5
  },
  actionCont: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    padding: 15
  },
  dcBtn: {
    borderRadius: 8,
    backgroundColor: DCColor.BGColor('primary-1'),
    flex: 1,
    height: 45,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  btnCancel: {
    backgroundColor: DCColor.BGColor('disable'),
    marginRight: 10
  },
  fButton: {
    fontSize: TextFont.TextSize(15),
    fontWeight: '500',
    color: 'rgba(0, 0, 0, 0.75)',
    textAlign: 'center',
  }
})

@inject('app', 'driverVerification')
@observer
export default class SkipModal extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }

  render() {
    const { strings } = this.props.app
    const { showSkipModal } = this.props.driverVerification
    const {
      backdropStyle,
      dcCard,
      headCont,
      dcImg,
      fTitle,
      fCaption,
      actionCont,
      dcBtn,
      btnCancel,
      fButton
    } = styles

    return (
      <Modal
        animationType='fade'
        transparent={true}
        visible={showSkipModal}
        onRequestClose={() => this.props.driverVerification.hideSkipModal()}
        onBackdropPress={() => this.props.driverVerification.hideSkipModal()}
      >
        <View style={backdropStyle}>
          <View style={dcCard}>
            <View style={headCont}>
              <Image style={dcImg} source={Resources.image.attention_icon2} />
              <Text style={fTitle}>{strings.comfirmation && strings.comfirmation.toUpperCase()}</Text>
              <Text style={fCaption}>{strings.skip_desc}</Text>
            </View>
            <View style={actionCont}>
              <TouchableOpacity style={[dcBtn, btnCancel]} onPress={() => this.props.driverVerification.hideSkipModal()}>
                <Text style={fButton}>{strings.cancel && strings.cancel.toUpperCase()}</Text>
              </TouchableOpacity>
              <TouchableOpacity style={dcBtn} onPress={() => this.props.driverVerification.clearCurrentData()}>
                <Text style={fButton}>{strings.skip_btn && strings.skip_btn.toUpperCase()}</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    )
  }
}
