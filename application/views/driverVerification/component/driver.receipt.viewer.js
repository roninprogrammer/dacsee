import React, { Component } from 'react'
import { View, Image, StatusBar } from 'react-native'
import { Screen, Define } from 'dacsee-utils'
import { inject, observer } from 'mobx-react'

const { width } = Screen.window

@inject('app', 'driver')
@observer
export default class DriverImgViewer extends Component {
  static navigationOptions = ({ navigation }) => {
    const { strings } = $.store.app
    return {
      title: strings.imgae_title,
      drawerLockMode: 'locked-closed'
    }
  }

  constructor(props) {
    super(props)
    this.state = {
    }
  }

  async componentDidMount() {
    const hasImg = this.props.navigation.state.params.img
    const id = this.props.navigation.state.params.id
    const formType = this.props.navigation.state.params.formType

    if (hasImg) {
      this.setState({ img: hasImg })
    } else {
      const imgUrl = await this.props.driver.getRequestFile(id, formType)
      this.setState({
        img: imgUrl
      })
    }
  }

  render() {
    const { img } = this.state

    return (
      <View style={{ flex: 1, backgroundColor: '#000' }}>
        <StatusBar animated={true} hidden={false} backgroundColor={'#151416'} barStyle={'dark-content'} />
        <View style={{ flex: 1, top: Define.system.ios.x ? -44 : -22, justifyContent: 'center', alignItems: 'center' }}>
          <View style={{ width, height: width }}>
            {img && img.length !== 0 && (
              <Image style={{ width: width, height: width, resizeMode: 'contain' }} source={{ uri: img }} />
            )}
          </View>
        </View>
      </View>
    )
  }
}