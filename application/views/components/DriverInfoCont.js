import React, { Component } from 'react'
import { Text, TouchableOpacity, View, Image, StyleSheet, Dimensions } from 'react-native'
import { TextFont, DCColor, Icons, Avatars } from 'dacsee-utils'
import Resources from 'dacsee-resources'

const { width } = Dimensions.get('window')

export default class DriverInfoCont extends Component {
  constructor(props) {
    super(props)
    this.state = {
      star: []
    }
  }

  componentDidMount() {
    const { rating } = this.props
    this.driverRating(rating)
  }

  driverRating = (i) => {
    switch (i) {
      case 1:
        this.setState({
          star: [true, false, false, false, false]
        })
        break
      case 2:
        this.setState({
          star: [true, true, false, false, false]
        })
        break
      case 3:
        this.setState({
          star: [true, true, true, false, false]
        })
        break
      case 4:
        this.setState({
          star: [true, true, true, true, false]
        })
        break
      default:
        this.setState({
          star: [true, true, true, true, true]
        })
        break
    }
  }

  render() {
    const {
      mainCont,
      dcAvatar,
      dataCont,
      fTitle,
      fCaption,
      ratingCont,
      ratingIcon,
      btnCont,
      dcIcon,
      chatBubble
    } = DriverInfoStyle
    const { avatar, avatarFunction, name, id, licenseNumber, callFunction, messageFunction, rating, unreadChat } = this.props
    const { star } = this.state
    const { strings } = $.store.app

    return (
      <View style={mainCont}>
        <TouchableOpacity onPress={avatarFunction} activeOpacity={avatarFunction ? .3 : 1}>
          <Image source={{ uri: Avatars.getHeaderPicUrl(avatar) }} style={dcAvatar} />
        </TouchableOpacity>
        <View style={dataCont}>
          {name ?
            <Text numberOfLines={1} ellipsizeMode='tail' style={fTitle}>{name}</Text>
            : null
          }
          {licenseNumber ?
            <Text numberOfLines={1} ellipsizeMode='tail' style={fCaption}>{licenseNumber}</Text>
            : id ? <Text numberOfLines={1} ellipsizeMode='tail' style={fCaption}>{strings.userid}: {id}</Text>
              : null
          }
          {rating ?
            <View style={ratingCont}>
              {!!star && star.map((rate, index) => (
                <View key={index} style={ratingIcon}>
                  {Icons.Generator.Awesome('star', 12, rate ? DCColor.BGColor('primary-1') : DCColor.BGColor('disable'))}
                </View>
              ))}
            </View>
            : null
          }
        </View>
        {callFunction ?
          <TouchableOpacity style={btnCont} onPress={callFunction}>
            <Image source={Resources.image.phone} style={dcIcon} />
          </TouchableOpacity>
          : null
        }
        {messageFunction ?
          <TouchableOpacity style={btnCont} onPress={messageFunction}>
            {unreadChat ?
              <View style={chatBubble}>
                <Text style={[fCaption, { color: 'white' }]}>{unreadChat >= 10 ? '10+' : unreadChat}</Text>
              </View>
              : null
            }
            <Image source={Resources.image.chat} style={dcIcon} />
          </TouchableOpacity>
          : null
        }
      </View>
    )
  }
}

const DriverInfoStyle = StyleSheet.create({
  mainCont: {
    width,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: DCColor.BGColor('primary-2'),
    padding: 15,
  },
  dcAvatar: {
    width: 45,
    height: 45,
    borderRadius: 45 / 2,
    overflow: 'hidden',
    resizeMode: 'cover',
    marginRight: 10,
    backgroundColor: DCColor.BGColor('disable')
  },
  dataCont: {
    flex: 1,
  },
  fTitle: {
    fontSize: TextFont.TextSize(18),
    fontWeight: '700',
    color: 'white',
    textAlign: 'left'
  },
  fCaption: {
    fontSize: TextFont.TextSize(12),
    fontWeight: '700',
    color: 'rgba(255, 255, 255, 0.5)',
    textAlign: 'left',
    marginBottom: 3
  },
  ratingCont: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start'
  },
  ratingIcon: {
    marginRight: 3
  },
  btnCont: {
    position: 'relative',
    width: 55,
    height: 55,
    borderRadius: 55 / 2,
    borderColor: 'rgba(0, 0, 0, 0.5)',
    borderWidth: 5,
    backgroundColor: DCColor.BGColor('primary-1'),
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 10,
  },
  dcIcon: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
    tintColor: DCColor.BGColor('primary-2')
  },
  chatBubble: {
    width: 25,
    height: 25,
    borderRadius: 25 / 2,
    backgroundColor: DCColor.BGColor('red'),
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    top: -8,
    right: -8,
    zIndex: 9999
  }
})
