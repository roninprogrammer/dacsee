import React, { Component } from 'react'
import { Text, View, StyleSheet, Image, Dimensions } from 'react-native'
import { TextFont, DCColor, Avatars, Icons } from 'dacsee-utils'
import { inject, observer } from 'mobx-react'

const { width } = Dimensions.get('window')

@inject('app')
@observer
export default class DCUserInfoCont extends Component {
  componentDidMount() {
    const { rating } = this.props
    this.driverRating(rating)
  }

  driverRating = (i) => {
    this.rate_index = parseInt(i)
    switch (i) {
      case 1:
        this.star = [true, false, false, false, false]
        break
      case 2:
        this.star = [true, true, false, false, false]
        break
      case 3:
        this.star = [true, true, true, false, false]
        break
      case 4:
        this.star = [true, true, true, true, false]
        break
      default:
        this.star = [true, true, true, true, true]
        break
    }
  }

  render() {
    const {
      avatars,
      rating,
      fullName,
      manufacturer,
      model,
      registrationNo,
      userId
    } = this.props
    const {
      mainCont,
      imageCont,
      imagePic,
      ratingCont,
      ratingStar,
      fTitle,
      vehicleCont,
      fCaption
    } = styles

    return (
      <View style={mainCont}>
        <View style={imageCont}>
          {avatars ?
            <Image style={imagePic} source={{ uri: Avatars.getHeaderPicUrl(avatars) }} />
            : null
          }
        </View>
        { rating ? 
        <View style={ratingCont}>
          {this.star && this.star.map((rate, index) => (
          <View key={index} style={ratingStar}>
            {Icons.Generator.Awesome('star', 20, rate ? 'white': DCColor.BGColor('disable'))}
          </View>
          ))}
        </View>
         : null 
        }
        {fullName ?
          <Text style={fTitle}>{fullName}</Text>
          : null
        }
        <View style={vehicleCont}>
          {manufacturer ?
            <Text style={[fCaption, { marginRight: 5 }]}>{manufacturer}</Text>
            : null
          }
          {model ?
            <Text style={[fCaption, { marginRight: 10 }]}>{model}</Text>
            : null
          }
          {registrationNo ?
            <Text style={fCaption}>{registrationNo}</Text>
            : null
          }
          {userId ?
            <Text style={fCaption}>{registrationNo}</Text>
            : null
          }
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  mainCont: {
    width: '100%',
    padding: 20,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  imageCont: {
    width: width * .5,
    height: width * .5,
    borderRadius: (width * .5) / 2,
    backgroundColor: 'rgba(0,0,0,0.1)',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
  },
  imagePic: {
    width: (width * .5) - 40,
    height: (width * .5) - 40,
    borderRadius: ((width * .5) - 40) / 2,
    resizeMode: 'cover'
  },
  ratingCont: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginBottom: 10
  },
  ratingStar: {
    marginRight: 15
  },
  fTitle: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    fontSize: TextFont.TextSize(18),
    fontWeight: '700',
    color: DCColor.BGColor('primary-2'),
    marginBottom: 10
  },
  vehicleCont: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  fCaption: {
    fontSize: TextFont.TextSize(12),
    fontWeight: '400',
    color: DCColor.BGColor('primary-2')
  }
})