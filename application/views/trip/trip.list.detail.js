import React, { Component } from 'react'
import { Text, View, Image, ScrollView, TouchableOpacity, Alert, StyleSheet } from 'react-native'
import moment from 'moment'
import { inject, observer } from 'mobx-react'
import { Screen, TextFont, DCColor } from 'dacsee-utils'
import Resources from 'dacsee-resources'

const { width } = Screen.window

const FareItem = (props) => {
  const { title, amount, deduct = false } = props
  return (
    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 5 }}>
      <Text style={{ fontSize: TextFont.TextSize(15), color: '#555' }}> {title} </Text>
      <Text style={{ fontSize: TextFont.TextSize(15), fontWeight: 'bold', color: '#333' }}>
        {deduct && (' - ')}
        {`RM ${parseFloat(amount).toFixed(2)}`}
      </Text>
    </View>
  )
}
@inject('app')
@observer
export default class TripListDetailScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const { strings } = $.store.app
    return {
      drawerLockMode: 'locked-closed',
      title: strings.trip_detail,
      headerStyle: {
        backgroundColor: DCColor.BGColor('primary-1'),
        shadowColor: 'transparent',
        shadowOpacity: 0,
        borderBottomWidth: 0,
        borderBottomColor: 'transparent',
        elevation: 0
      }
    }
  }

  constructor(props) {
    super(props)
  }

  async sendReceipt(id) {
    // $.store.app.showLoading()
    const result = await $.store.app.sendReceipt(id)
    // $.store.app.hideLoading()
    if (result === 'success') {
      Alert.alert(
        $.store.app.strings.receipt_sent,
        $.store.app.strings.receipt_sent_message,
        [
          { text: $.store.app.strings.okay, onPress: () => console.log('OK Pressed') },
        ],
        { cancelable: false }
      );
    }
    else if (result === 'email') {
      Alert.alert(
        $.store.app.strings.receipt_send_fail,
        $.store.app.strings.receipt_send_fail_message,
        [
          { text: $.store.app.strings.okay, onPress: () => console.log('OK Pressed') },
        ],
        { cancelable: false }
      )
    }
    else {
      Alert.alert(
        $.store.app.strings.receipt_send_fail,
        $.store.app.strings.receipt_send_system_fail,
        [
          { text: $.store.app.strings.okay, onPress: () => console.log('OK Pressed') },
        ],
        { cancelable: false }
      )
    }
  }

  render() {
    const { strings } = this.props.app
    const { tripDetail } = this.props.navigation.state.params
    const { _id, destination, from, assign_type, payment_method, group_info, fare, booking_at, status, additional_fees = 0, driver_info, code, fareObj } = tripDetail
    const {
      dcCard,
      dcBtn,
      fButton
    } = styles

    let { amount = 0, promo_discount = 0, originalAmount = 0, campaign_discount = 0 } = fareObj
    let total = 0

    originalAmount = parseFloat(originalAmount)
    promo_discount = parseFloat(promo_discount)
    campaign_discount = parseFloat(campaign_discount)

    amount = !!originalAmount ? originalAmount : parseFloat(amount)

    total = amount + parseFloat(additional_fees) - promo_discount - campaign_discount

    return (
      <View style={{ flex: 1, backgroundColor: DCColor.BGColor('primary-1'), alignItems: 'center' }}>
        <View style={dcCard}>
          <ScrollView style={{ flex: 1 }}>
            <View style={{ paddingHorizontal: 20, paddingBottom: 10 }}>
              <TripCell title={strings.booking_id} value={code} />
              <TripCell title={strings.booking_status} value={status} />
              <View style={{ marginTop: 6 }}>
                <Text style={{ fontSize: TextFont.TextSize(13), color: '#999' }}>{strings.issue_by_drive}</Text>
                <View style={{ flexDirection: 'row', marginTop: 6 }}>
                  <Text style={{ fontSize: TextFont.TextSize(16), color: '#333', fontWeight: 'bold' }}>{`${driver_info.fullName} - `}</Text>
                  <Text style={{ fontSize: TextFont.TextSize(15), color: '#555' }}>
                    {driver_info.userId}
                  </Text>
                </View>
              </View>
            </View>
            <Image source={Resources.image.logo_portrait} style={{ height: 100, width: 100, position: 'absolute', right: 10, top: 10, resizeMode: 'contain' }} />
            <View style={{ height: 1, backgroundColor: '#e5e5e5' }} />
            <View style={{ paddingTop: 5, paddingBottom: 10, paddingHorizontal: 20 }}>
              {assign_type !== 'selected_circle' &&
                <TripCell title={strings.group_name} value={group_info.name} />}
              <TripCell title={strings.from} value={from.name} description={from.address} />
              <TripCell title={strings.destination} value={destination.name} description={destination.address} />
              <TripCell title={strings.book_time} value={moment(booking_at).format('YYYY-MM-D HH:mm:ss')} />
              <TripCell title={strings.payment_method} value={payment_method} />
            </View>

            <View style={{ paddingHorizontal: 20, paddingBottom: 5, marginBottom: 5, backgroundColor: '#F3F3F3' }}>
              <Text style={{ marginTop: 5, fontSize: TextFont.TextSize(15), fontWeight: 'bold', color: '#333' }}>
                {strings.fare}
              </Text>

              <View style={{ marginTop: 5, height: 1, backgroundColor: '#e3e3e3' }} />

              <FareItem title={strings.base_fare} amount={amount} deduct={false} />
              <FareItem title={strings.tolls_fare} amount={additional_fees} deduct={false} />

              {
                !!promo_discount && (
                  <FareItem title={strings.promo} amount={promo_discount} deduct={true} />
                )
              }

              {
                !!campaign_discount && (
                  <FareItem title={`Joy Promotion`} amount={campaign_discount} deduct={true} />
                )
              }
            </View>

            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 20, marginBottom: 20 }}>
              <Text style={{ fontSize: TextFont.TextSize(15), color: '#555' }}>
                {strings.fare_total}
              </Text>
              <Text style={{ fontSize: TextFont.TextSize(15), fontWeight: 'bold', color: '#333' }}>
                {`RM ${total.toFixed(2)}`}
              </Text>
            </View>
          </ScrollView>
          {status === 'Completed' ?
            <TouchableOpacity onPress={() => this.sendReceipt(_id)} style={dcBtn}>
              <Text style={fButton}>{$.store.app.strings.get_receipt.toUpperCase()}</Text>
            </TouchableOpacity>
            :
            null
          }
        </View>
      </View>
    )
  }
}
const TripCell = (props) => {
  const { title, value, description = '' } = props
  return (
    <View style={{ marginTop: 5 }}>
      <Text style={{ fontSize: TextFont.TextSize(13), color: '#999' }}>{title}</Text>
      <Text style={{ marginTop: 5, fontSize: TextFont.TextSize(15), color: '#555' }}>{value}</Text>

      {
        !!description && (
          <Text style={{ marginTop: 5, fontSize: TextFont.TextSize(13), color: '#555' }}>{description}</Text>
        )
      }
    </View>
  )
}

const styles = StyleSheet.create({
  dcCard: {
    margin: 10,
    width: width - 30,
    flex: 1,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    backgroundColor: 'white',
    shadowOffset: { width: 0, height: 10 },
    shadowColor: '#000',
    shadowOpacity: 0.2,
    elevation: 0.8
  },
  dcBtn: {
    height: 80,
    backgroundColor: DCColor.BGColor('primary-1'),
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  fButton: {
    fontSize: TextFont.TextSize(15),
    fontWeight: '700',
    color: DCColor.BGColor('primary-2'),
  }
})