import React, { Component } from 'react'
import { Text, View, Image, FlatList, StyleSheet, RefreshControl, SafeAreaView } from 'react-native'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import moment from 'moment'
import { inject, observer } from 'mobx-react'
import { Screen, Icons, DCColor, TextFont } from 'dacsee-utils'
import Resources from 'dacsee-resources'
import TripListItem from './compoment/trip.list.item'
const { height } = Screen.window

@inject('app', 'trip')
@observer
export default class TripListScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const { strings } = $.store.app
    return {
      drawerLockMode: 'locked-closed',
      title: strings.mytrip
    }
  }

  constructor(props) {
    super(props)
  }

  componentDidMount() {
    this.props.trip.getTripList()
  }

  render() {
    const { app = {}, trip = {}, navigation } = this.props
    const { strings } = app
    const { tripList, loading, getTripList } = trip
    const { emptyListStyle, imagejoblistemptyStyle , textjoblistStyle} = styles 
    const refreshControl = (
      <RefreshControl
        refreshing={loading}
        onRefresh={() => getTripList()}
        title={app.strings.pull_refresh}
        colors={['#ffffff']}
        progressBackgroundColor={DCColor.BGColor('primary-1')}
      />
    )
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <View style={{ flex: 1 }}>
          <FlatList
            refreshControl={refreshControl}
            data={tripList}
            enableEmptySections
            ListEmptyComponent={() =>
              <View style={emptyListStyle}>
                <Image source={Resources.image.joblist_empty} style={imagejoblistemptyStyle} />
                <Text style={textjoblistStyle} >
                  {this.props.app.strings.no_job}
                </Text>
              </View>}
            keyExtractor={(item, index) => item._id}
            renderItem={(row) => (
              <TripListItem
                itemIndex={row.index}
                itemData={row.item}
                onPress={() => {
                  if (row.item.status === 'Confirmed' || row.item.status === 'Pending_Acceptance') {
                    this.props.trip.activeTrip = row.item
                    navigation.navigate('TripAdvanceDetail', { bookingId: row.item._id })
                  } else {
                    navigation.navigate('TripListDetail', { tripDetail: row.item })
                  }
                }}
              />
            )}
          />
        </View>
      </SafeAreaView>
    )
  }
}

class ListItem extends Component {
  render() {
    const { itemData, index } = this.props
    const { from, destination, booking_at, payment_method, fare, additional_fees = 0 } = itemData
    const isEven = index % 2 == 0
    return (
      <View style={{ padding: 4 }}>
        <View style={styles.card}>
          <View style={{ justifyContent: 'center' }}>
            <View style={[styles.line, { backgroundColor: isEven ? '#45e2be' : DCColor.BGColor('primary-1') }]} />
          </View>
          <View style={{ alignItems: 'center', flex: 1 }}>
            <FontAwesome name={'share-square-o'} size={18} color={'#24d7ab'} style={{ alignSelf: 'flex-end' }} />
            <View style={styles.cardItem}>
              <View style={styles.cardLeft}>
                <View style={{ flexDirection: 'row', alignItems: 'center', width: 130 }}>
                  <Image style={{ width: 26, height: 26, marginLeft: -10, marginRight: 4 }} source={Resources.image.from_addr} />
                  <Text numberOfLines={1} style={{ fontSize: TextFont.TextSize(14), fontFamily: 'Helvetica', fontWeight: 'bold', color: '#67666c' }}>{from.name}</Text>
                </View>
                <View style={{ flex: 1, marginTop: 6 }}>
                  <View style={[styles.circle, { backgroundColor: '#baccd3' }]} />
                  <View style={[styles.circle, { backgroundColor: '#cddadf' }]} />
                  <View style={[styles.circle, { backgroundColor: '#dbe5e8' }]} />
                  <View style={[styles.circle, { backgroundColor: '#e9eff1' }]} />
                </View>
                <View style={{ width: 130, flexDirection: 'row', alignItems: 'center' }}>
                  <Image style={{ width: 18, height: 23, marginLeft: -5, marginRight: 4 }} source={Resources.image.to_addr} />
                  <Text numberOfLines={1} style={{ fontSize: TextFont.TextSize(13), fontFamily: 'Helvetica', fontWeight: '500', color: '#B2B1B6' }}> {destination.name} </Text>
                </View>
              </View>
              <View style={styles.cardRight}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  {Icons.Generator.Material('access-time', TextFont.TextSize(14), '#000000')}
                  <Text style={{ fontSize: TextFont.TextSize(14), color: '#5C5B63', marginLeft: 4 }}>{moment(booking_at).format('YY-MM-DD HH:mm')}</Text>
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center', marginVertical: 20 }}>
                  {Icons.Generator.Material('payment', TextFont.TextSize(14), '#000000')}
                  <Text style={{ fontSize: TextFont.TextSize(14), color: '#5C5B63', marginLeft: 4 }}>{payment_method}</Text>
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <Text style={{ fontSize: TextFont.TextSize(14), fontWeight: 'bold', color: '#6A696F' }}>
                    {`RM ${(parseFloat(fare) + parseFloat(additional_fees)).toFixed(2)}`}
                  </Text>
                </View>
              </View>
            </View>
          </View>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  line: {
    width: 3,
    height: 40
  },
  card: {
    flex: 1,
    marginBottom: 20,
    borderRadius: 6,
    backgroundColor: 'white',
    flexDirection: 'row',
    alignItems: 'center',
    shadowOffset: { width: 0, height: 0 },
    shadowColor: '#a2a3a8',
    shadowOpacity: 0.5,
    shadowRadius: 3,
    elevation: 3,
    paddingRight: 16,
    paddingBottom: 16,
    paddingTop: 8
  },
  circle: {
    width: 6,
    height: 6,
    borderRadius: 3,
    marginBottom: 6
  },
  cardItem: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,
    marginTop: 4
  },
  cardLeft: {
    borderColor: '#ededed',
    borderRightWidth: 2,
    paddingRight: 18,
    paddingLeft: 10
  },
  cardRight: {
    paddingLeft: 12
  }, 
  emptyListStyle : {
    height: height - 200, 
    justifyContent: 'center', 
    alignItems: 'center'
  }, 
  imagejoblistemptyStyle : {
    width: 100, 
    height: 100
  },
  textjoblistStyle : {
     marginTop: 20, 
     color: '#777', 
     fontSize: TextFont.TextSize(18), 
     fontWeight: '400' 
  }
})
