import React, { Component } from 'react'
import { View, Image, Linking } from 'react-native'
import moment from 'moment'
import ActionSheet from 'react-native-actionsheet'
import { Screen, Avatars, DCColor } from 'dacsee-utils'
import Resources from 'dacsee-resources'
import { inject, observer } from 'mobx-react'
import { MapView } from '../../components/map'
import { NavigationActions } from 'react-navigation'
import { observable, autorun } from 'mobx'
import ImageModal from '../main/passenger/components/modal.image'
import { DCBookingDetailCont } from '../main/passenger/components'
import {
  DCBookingDetailModal,
  DCInfoModal
} from '../modal'
import HeaderTab from '../../components/header-tab'

const { height, width } = Screen.window

const navigateTo = async (index, destination) => {
  if (index === 0) {
    await Linking.openURL(`https://www.google.com/maps/dir/?api=1&destination=${destination.name}&destination_place_id=${destination.placeId}&dir_action=navigate`)
  } else if (index === 1) {
    await Linking.openURL(`https://waze.com/ul?ll=${destination.coords.lat},${destination.coords.lng}&navigate=yes`)
  }
}

@inject('app', 'trip', 'chat')
@observer
export default class JobsListDetailScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const { control } = $.store.app
    return {
      drawerLockMode: 'locked-closed',
      headerTitle: (
        <HeaderTab onlyTitle={false} active={control.header.indexTrip} onPress={index => control.header.tripNavigate(index)}>
          <Image source={Resources.image.logo_landscape_border} style={{ width: 100, height: 60, resizeMode: 'contain', marginRight: 10 }} />
        </HeaderTab>
      ),
      headerStyle: {
        backgroundColor: DCColor.BGColor('primary-1'),
        shadowColor: 'transparent',
        shadowOpacity: 0,
        borderBottomWidth: 0,
        borderBottomColor: 'transparent',
        elevation: 0
      }
    }
  }
  @observable imageModalVisible: Boolean = false
  @observable activeImage: String = ''
  constructor(props) {
    super(props)
    this.state = {
      route: {
        routeBounds: {
          northEast: { longitude: 0, latitude: 0 },
          southWest: { longitude: 0, latitude: 0 }
        },
        routeCenterPoint: { longitude: 0, latitude: 0 },
        routeLength: 0,
        routeNaviPoint: [],
        routeTime: 0,
        routeTollCost: 0
      },
      showbookingDetailModal: false,
      driverPhotoModal: false,
      target: {},
      to: {}
    }
  }

  async componentDidMount() {
    const { trip, app } = this.props
    const { activeTrip = {} } = trip
    const detail = activeTrip
    const { status = 'Pending_Acceptance', from, destination, distance } = detail
    const target = status === 'Pending_Acceptance' ? from.coords : app.coords
    const to = status === 'Pending_Acceptance' ? destination.coords : from.coords
    const points = [trip.destination.coords, trip.actualFrom.coords]

    const region = this.regionContainingPoints(points)

    this.setState({
      target,
      to,
      latitude: region.latitude,
      latitudeDelta: region.latitudeDelta,
      longitude: region.longitude,
      longitudeDelta: region.longitudeDelta
    })
  }

  moveCurrentLocation(coords, zoom) {
    if (this.map && this.map.handle) {
      this.map.handle.moveTo(coords, zoom)
      setTimeout(() => {
      }, 400)
    }
  }

  regionContainingPoints(points) {
    var minX, maxX, minY, maxY;

    // init first point
    ((point) => {
      minX = point.latitude ? point.latitude : point.lat;
      maxX = point.latitude ? point.latitude : point.lat;
      minY = point.longitude ? point.longitude : point.lng;
      maxY = point.longitude ? point.longitude : point.lng;
    })(points[0]);

    // calculate rect
    points.map((point) => {
      minX = Math.min(minX, point.latitude ? point.latitude : point.lat);
      maxX = Math.max(maxX, point.latitude ? point.latitude : point.lat);
      minY = Math.min(minY, point.longitude ? point.longitude : point.lng);
      maxY = Math.max(maxY, point.longitude ? point.longitude : point.lng);
    });

    var midX = (minX + maxX) / 2;
    var midY = (minY + maxY) / 2;
    var midPoint = [midX, midY];

    var deltaX = (maxX - minX);
    var deltaY = (maxY - minY);

    return {
      latitude: midX,
      longitude: midY,
      latitudeDelta: deltaX,
      longitudeDelta: deltaY,
    };
  }

  componentWillUnmount() {
    this.imageModalVisible = false
    this.onHide('showbookingDetailModal')
  }

  onMapReady() {
    this.setState({
      mapReady: true
    })
  }

  onMapLayout() {
    const { target, to } = this.state
    this.setState({
      mapReady: true
    })

    if ((typeof this.props.isFocused === 'function' ? !this.props.isFocused() : !this.props.isFocused)) return null
    this.setState({ mapReady: true }, () => {
      const whenActiveMapEvent = () => {
        dispose && dispose()
        const FromCoords = {
          latitude: target.lat ? target.lat : target.latitude,
          longitude: target.lng ? target.lng : target.longitude
        }
        const toCoords = {
          latitude: to.lat ? to.lat : to.latitude,
          longitude: to.lng ? to.lng : to.longitude
        }

        this.moveCurrentLocation($.method.utils.getRegion([FromCoords, toCoords]), $.method.utils.mathDistanceZoom(distance))
      }
      const dispose = autorun(() => whenActiveMapEvent(destination.coords))
    })
  }

  onShow = (modal) => {
    this.setState({
      [modal]: true
    })
  }

  onHide = (modal) => {
    this.setState({
      [modal]: false
    })
  }

  cancelTrip = () => {
    const { bookingId } = this.props.navigation.state.params

    this.setState({
      showbookingDetailModal: false
    }, () => {
      setTimeout(() => this.props.trip.cancelTrip(bookingId), 100)
    })
  }

  render() {
    const { app, trip, chat } = this.props
    const { activeTrip = {} } = trip
    const { totalUnread } = chat
    const { strings } = app
    const { mapReady = false, showbookingDetailModal, driverPhotoModal, latitudeDelta, latitude, longitude, longitudeDelta } = this.state
    const { destination = { address: '' }, status, fare, from = { address: '' }, payment_method, booking_at, vehicle_info = '', group_info = { name: '' }, assign_type,
      driver_info, additional_fees = 0, code, notes } = activeTrip
    const friendData = Object.assign({}, { friend_info: Object.assign({}, { _id: activeTrip.driver_id }, driver_info) }, { friend_id: activeTrip.driver_id })
    const extraMapProps = {}
    const driverFound = status === 'Confirmed'

    let sameday = moment(booking_at).isSame(moment(), 'day')

    return (
      <View style={{ flex: 1 }}>

        <MapView
          initialRegion={mapReady ? {
            ...app.coords,
            latitude,
            latitudeDelta: latitudeDelta / .7,
            longitudeDelta: longitudeDelta,
            longitude
          } : null}
          onRegionChangeComplete={() => {
            this.extraMapProps = {
              from: mapReady && trip.actualFrom.hasOwnProperty('coords') ? trip.actualFrom : null,
              destination: mapReady && trip.destination.hasOwnProperty('coords') ? trip.destination : null,
              routes: mapReady && trip.bookingPath.routes
            }
          }}
          from={mapReady && trip.actualFrom.hasOwnProperty('coords') ? trip.actualFrom : null}
          destination={mapReady && trip.destination.hasOwnProperty('coords') ? trip.destination : null}
          routes={mapReady && trip.bookingPath.routes}
          showNavigator={false}
          region_type={app.region}
          style={{ flex: 1 }}
          ref={e => { this.map = e }}
          onMapReady={() => this.onMapReady()}
          {...extraMapProps}
        />
        <DCBookingDetailCont
          expandFunction={() => this.onShow('showbookingDetailModal')}
          bookingStatus={status ? status : null}
          avatar={driverFound ? driver_info && driver_info.avatars : null}
          avatarFunction={driverFound ? () => this.onShow('driverPhotoModal') : null}
          name={driverFound ? driver_info && driver_info.fullName : null}
          messageFunction={driverFound ? () => global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'ChatWindow', params: { friendData, chatType: 'friend' } })) : null}
          unreadChat={totalUnread}
          callFunction={driverFound ? () => driver_info && Linking.openURL(`tel:${driver_info.phoneCountryCode}${driver_info.phoneNo}`) : null}
          userId={driverFound ? driver_info ? driver_info.userId : null : null}
          rating={driverFound ? 4 : null}
          advTime={booking_at ? moment(booking_at).format('hh:mm A') : null}
          advDate={booking_at ? sameday ? strings.today : moment(booking_at).format('ddd, DD MMM') : null}
          bookingId={code ? code : null}
          paymentType={payment_method == 'Cash' ? strings.cash : payment_method}
          carManufacture={vehicle_info && vehicle_info.manufacturer}
          carModel={vehicle_info && vehicle_info.model}
          carColor={vehicle_info && vehicle_info.color}
          carPlateNumber={vehicle_info && vehicle_info && vehicle_info.registrationNo}
          carFrontImg={vehicle_info && vehicle_info.frontPhoto && vehicle_info.frontPhoto.urlSmall}
          carFrontFunction={() => {
            this.imageModalVisible = true
            this.activeImage = vehicle_info && vehicle_info.frontPhoto && vehicle_info.frontPhoto.url
          }}
          carBackImg={vehicle_info && vehicle_info.backPhoto && vehicle_info.backPhoto.urlSmall}
          carBackFunction={() => {
            this.imageModalVisible = true
            this.activeImage = vehicle_info && vehicle_info.backPhoto && vehicle_info.backPhoto.url
          }}
          price={(fare && additional_fees !== null && additional_fees !== undefined) ? parseFloat(fare + additional_fees) : null}
        />

        <DCBookingDetailModal
          onHide={() => this.onHide('showbookingDetailModal')}
          visible={showbookingDetailModal}
          bookingStatus={status ? status : null}
          avatar={driverFound ? driver_info && driver_info.avatars : null}
          userId={driverFound ? driver_info ? driver_info.userId : null : null}
          licenseNumber={driverFound ? driver_info && driver_info.evpInfo ? driver_info.evpInfo.evpNo : null : null}
          rating={driverFound ? 4 : null}
          name={driverFound ? driver_info && driver_info.fullName : null}
          messageFunction={driverFound ? () => {
            this.onHide('showbookingDetailModal')
            global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'ChatWindow', params: { friendData, chatType: 'friend' } }))
          } : null}
          unreadChat={totalUnread}
          callFunction={driverFound ? () => {
            this.onHide('showbookingDetailModal')
            driver_info && Linking.openURL(`tel:${driver_info.phoneCountryCode}${driver_info.phoneNo}`)
          } : null}
          bookingId={code ? code : null}
          pickupAddress={`${from.name}, ${from.address}`}
          dropoffAddress={`${destination.name}, ${destination.address}`}
          campaignName={assign_type !== 'selected_circle' ? group_info.name : strings.circle_of_friend}
          driverNote={notes ? notes : strings.note_to_driver}
          paymentType={payment_method == 'Cash' ? strings.cash : payment_method}
          price={(fare && additional_fees !== null && additional_fees !== undefined) ? parseFloat(fare + additional_fees) : null}
          rejectFunction={() => {
            this.cancelTrip()
          }}
          rejectBtnLabel={strings.cancel_trip}
        />

        <DCInfoModal
          onHide={() => this.onHide('driverPhotoModal')}
          visible={driverPhotoModal}
          label={driver_info ? driver_info.fullName : null}
          caption={driver_info ? driver_info.userId : null}
          licenseNumber={driver_info && driver_info.evpInfo ? driver_info.evpInfo.evpNo : null}
          rating={4}
          confirmBtn={app.strings.okay}
          onPress={() => this.onHide('driverPhotoModal')}
        >
          <Image
            resizeMode="cover"
            style={{ position: 'absolute', top: 0, left: 0, bottom: 0, right: 0 }}
            source={driver_info ? { uri: Avatars.getHeaderPicUrl(driver_info.avatars) } : null}
          />
        </DCInfoModal>

        <ActionSheet
          ref={o => { this.fromAction = o }}
          title={strings.navigation}
          message={strings.please_make_sure_installed}
          options={[strings.google_maps, 'Waze', strings.cancel]}
          cancelButtonIndex={2}
          onPress={(index) => navigateTo(index, from)}
        />
        <ImageModal
          close={() => this.imageModalVisible = false}
          visible={this.imageModalVisible}
          uri={this.activeImage}
        />
        <ActionSheet
          ref={o => { this.destinationAction = o }}
          title={strings.navigation}
          message={strings.please_make_sure_installed}
          options={[strings.google_maps, 'Waze', strings.cancel]}
          cancelButtonIndex={2}
          onPress={(index) => navigateTo(index, destination)}
        />
      </View>
    )
  }
}