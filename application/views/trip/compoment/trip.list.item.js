import React, { Component } from 'react'
import { Text, View, Image, TouchableOpacity, StyleSheet } from 'react-native'
import moment from 'moment'
import { inject, observer } from 'mobx-react'
import Resources from 'dacsee-resources'
import { Screen, Icons, DCColor, TextFont } from 'dacsee-utils'
const { height, width } = Screen.window

@inject('app', 'jobs')
@observer
export default class OfflineListItem extends Component {
  constructor(props) {
    super(props)
    this.state = {
      distance: 0
    }
  }
  // distance
  _getStatus(str) {
    let json = {}
    let { strings } = this.props.app
    switch (str) {
      case 'Pending_Acceptance':
        json.text = strings.Pending_Acceptance
        json.color = DCColor.BGColor('green')
        return json
      case 'On_The_Way':
        json.text = strings.On_The_Way
        json.color = DCColor.BGColor('green')
        return json
      case 'Arrived':
        json.text = strings.Arrived
        json.color = DCColor.BGColor('green')
        return json
      case 'No_Show':
        json.text = strings.No_Show
        json.color = '#ccc'
        return json
      case 'Confirmed':
        json.text = strings.ongoing
        json.color = DCColor.BGColor('primary-1')
        return json
      case 'On_Board':
        json.text = strings.On_Board
        json.color = DCColor.BGColor('green')
        return json
      case 'Completed_by_Passenger':
      case 'Completed':
        json.text = strings.Completed
        json.color = DCColor.BGColor('green')
        return json
      case 'Finalize_Total_Fare':
        json.text = strings.confirm_fare
        json.color = DCColor.BGColor('green')
        return json
      case 'Cancelled_by_Passenger':
        json.text = strings.Cancelled_by_Passenger
        json.color = '#ccc'
        return json
      case 'Cancelled_by_System':
        json.text = strings.Cancelled_by_System
        json.color = '#ccc'
        return json
      case 'Cancelled_by_Driver':
        json.text = strings.Cancelled_by_Driver
        json.color = '#ccc'
        return json
      case 'Rejected_by_Driver':
        json.text = strings.Rejected_by_Driver
        json.color = '#red'
        return json
      case 'No_Taker':
        json.text = strings.No_Taker
        json.color = '#ccc'
        return json
    }
  }
  render() {
    const { strings } = this.props.app
    const { itemData, itemIndex, onPress = () => { } } = this.props
    const { from, destination, distance, booking_at, payment_method, fare, status, additional_fees = 0 } = itemData
    const optionObject = this._getStatus(status)
    return (
      <TouchableOpacity activeOpacity={0.7} onPress={onPress}>
        <View style={[styles.container, styles.shadow, { marginTop: itemIndex === 0 ? 15 : 0 }]}>
          <View style={{ borderBottomLeftRadius: 6, width: 15, borderTopLeftRadius: 6, backgroundColor: optionObject.color }} />
          <View style={styles.rightContent}>
            <View style={[styles.text_cell, { justifyContent: 'space-between', paddingTop: 15 }]}>
              <View style={{ flexDirection: 'row' }}>
                <Text style={styles.orderDate}>{moment(booking_at).format('DD MMMM YYYY, h:mm a')}</Text>
                <Text style={[styles.order_status, { color: optionObject.color }]}>{optionObject.text}</Text>
              </View>
            </View>
            <View style={styles.text_cell}>
              <View style={[styles.dot, { backgroundColor: DCColor.BGColor('primary-1') }]} />
              <Text style={styles.adress}>{from.name}</Text>
            </View>
            <View style={styles.text_cell}>
              <View style={[styles.dot, { backgroundColor: DCColor.BGColor('green') }]} />
              <Text style={styles.adress}>{destination.name}</Text>
            </View>
            <View style={[styles.text_cell, { justifyContent: 'space-between' }]}>
              <View style={{ flexDirection: 'row' }}>
                <Image source={Resources.image.distance} resizeMode='contain' style={{ height: 18, width: 20 }} />
                {/* <Text style={styles.order_status}>{this.state.distance === 0 ? '<100m' : (this.state.distance + 'km')}</Text> */}
                <Text style={{ fontSize: TextFont.TextSize(14), fontWeight: 'bold' }}>{`~${parseFloat(distance / 1000).toFixed(1)}km`}</Text>
                <View style={{ marginLeft: 15, justifyContent: 'center' }} >
                  {Icons.Generator.Material('payment', TextFont.TextSize(14), '#000000')}
                </View>
                <Text style={{ fontSize: TextFont.TextSize(14), color: '#5C5B63', marginLeft: 4 }}>{payment_method === 'Cash' ? strings.cash : payment_method}</Text>
              </View>

              <Text style={styles.fare}>{`RM ${(parseFloat(fare) + parseFloat(additional_fees)).toFixed(2)}`}</Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    width: width - 20,
    marginLeft: 10,
    marginBottom: 15,
    backgroundColor: '#fff',
    flex: 1,
    flexDirection: 'row',
    borderRadius: 6
  },
  rightContent: {
    flex: 1,
    paddingBottom: 15,
    paddingHorizontal: 15
  },
  info_cell: {
    flexDirection: 'row', justifyContent: 'space-between', flex: 1
  },
  text_cell: {
    flexDirection: 'row', flex: 1, alignItems: 'center', paddingTop: 10
  },
  orderDate: {
    fontSize: TextFont.TextSize(14), fontWeight: 'bold', color: '#aaaaaa'
  },
  shadow: {
    elevation: 2,
    shadowOffset: { width: 0, height: 0 },
    shadowColor: 'rgba(0,0,0,0.4)',
    shadowOpacity: 0.4,
    shadowRadius: 3
  },
  order_status: {
    fontSize: TextFont.TextSize(14), fontWeight: 'bold', marginLeft: 15
  },
  fare: {
    fontSize: TextFont.TextSize(16), fontWeight: 'bold', color: '#000'
  },
  dot: {
    height: 10, width: 10, borderRadius: 5
  },
  adress: {
    fontSize: TextFont.TextSize(16), fontWeight: 'bold', color: '#404040', marginLeft: 15
  },
  content: {
    height: 40, width: width - 40, position: 'absolute', top: 5, left: 0, backgroundColor: '#ccc', flexDirection: 'row', borderRadius: 20, justifyContent: 'space-between', alignItems: 'center'
  },
  circle: {
    height: 50, width: height / 13, borderRadius: 25, justifyContent: 'center', alignItems: 'center'
  }
})
