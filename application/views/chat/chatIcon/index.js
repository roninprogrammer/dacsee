import SvgIcon from './SVGIcon'

import * as iconPath from './path'

export { SvgIcon, iconPath }