/**
 * @flow
 */

import React, {Component} from 'react'
import {
  StyleSheet,
  View,
  Animated,
  Dimensions,
  Text,
  ViewPropTypes as RNViewPropTypes,
  ActivityIndicator,
  Platform
} from 'react-native'
import Lottie from 'lottie-react-native'
import Resources from '../../resources'
import RNFS from 'react-native-fs'
import {AudioRecorder, AudioUtils} from 'react-native-audio'
import PropTypes from 'prop-types'
const delay = (ms) => new Promise(resolve => setTimeout(() => resolve(), ms))
const ViewPropTypes = RNViewPropTypes || View.propTypes

const { height } = Dimensions.get('window')

export default class VoiceModal extends Component {
  constructor (props) {
    super(props)
    this.timer = null
    this.state = {
      isShow: false,
      opacityValue: new Animated.Value(this.props.opacity),
      progress: new Animated.Value(0),
      currentTime: 0.0,
      recording: false,
      paused: false,
      stoppedRecording: false,
      finished: false,
      audioPath: '',
      error: false,
      hasPermission: undefined,
      waiting: true,
      volume: 0,
      errorText: props.strings.talk_short
    }
  }

  componentDidMount () {
    this.initPath()
    AudioRecorder.onProgress = (data) => {
      console.log(data)
      if (data.currentTime === 0) {
        this.setState((prevState) => ({ currentTime: Math.floor(prevState.currentTime + 0.25) }))
      } else {
        this.setState({ currentTime: Math.floor(data.currentTime) })
      }
      this.setState({ waiting: false, volume: Math.floor(data.currentMetering) })
    }
    AudioRecorder.onFinished = (data) => this._finishRecording(data.status === 'OK', data.audioFileURL)
    // System.Platform.Android && this._checkPermission()
    // System.Platform.iOS && this._requestAuthorization()
  }

  // async _requestAuthorization () {
  //   try {
  //     const res = await AudioRecorder.requestAuthorization()
  //     this.setState({ hasPermission: res })
  //   } catch (e) {
  //     console.log(e)
  //   }
  // }

  prepareRecordingPath (audioPath) {
    AudioRecorder.prepareRecordingAtPath(audioPath, {
      SampleRate: 22050,
      Channels: 1,
      AudioQuality: 'High',
      AudioEncoding: 'aac',
      OutputFormat: 'aac_adts',
      AudioEncodingBitRate: 32000,
      MeteringEnabled: true,
      IncludeBase64: false
    })
  }

  show () {
    const { microphonePermission } = this.props
    if (!microphonePermission) {
      // DeviceEventEmitter.emit('permissionAlert')
      return
    }
    this.setState({
      isShow: true
    })

    Animated.timing(
      this.state.opacityValue,
      {
        toValue: this.props.opacity,
        duration: this.props.fadeInDuration
      }
    ).start()
    this._record()
    this._setProgress()
  }

  async close () {
    const { microphonePermission } = this.props
    let delayTime = 0
    if (!microphonePermission) {
      return this.delayClose()
    }
    const { currentTime } = this.state
    if (currentTime < 1) {
      this.setState({ error: true })
      delayTime = 1000
    }
    await this._stop()
    let base64 = ''
    if (currentTime >= 1) {
      // let keep = true
      while (true) {
        // try {
        base64 = await RNFS.readFile(this.state.audioPath, 'base64')
        if (!base64 || base64.length === 0) {
          await delay(50)
        } else {
          break
        }
      }
    }
    this.timer && clearTimeout(this.timer)
    this.timer = setTimeout(() => this.delayClose(), delayTime)
    await delay(50)
    currentTime >= 1 && this.props.sendVoice && this.props.sendVoice('voice', this.state.audioPath, this.state.currentTime, base64)
    this.initPath()
  }

  async checkDir () {
    if (!await RNFS.exists(`${AudioUtils.DocumentDirectoryPath}/voice/`)) {
      RNFS.mkdir(`${AudioUtils.DocumentDirectoryPath}/voice/`)
    }
  }

  async initPath () {
    await this.checkDir()
    const nowPath = `${AudioUtils.DocumentDirectoryPath}/voice/voice${Date.now()}.aac`
    this.setState({ audioPath: nowPath, currentTime: 0 })
    this.prepareRecordingPath(nowPath)
  }

  delayClose () {
    Animated.timing(
      this.state.opacityValue,
      {
        toValue: 0.0,
        duration: this.props.fadeOutDuration
      }
    ).start(() => {
      this.setState({
        isShow: false,
        error: false,
        waiting: true
      })
      this._reload()
    })
  }

  async _pause () {
    if (!this.state.recording) {
      console.warn('Can\'t pause, not recording!')
      return
    }

    try {
      // const filePath = await AudioRecorder.pauseRecording() // NEVER USED
      this.setState({paused: true})
    } catch (error) {
      console.log(error)
    }
  }

  async _resume () {
    if (!this.state.paused) {
      console.warn('Can\'t resume, not paused!')
      return
    }

    try {
      await AudioRecorder.resumeRecording()
      this.setState({paused: false})
    } catch (error) {
      console.log(error)
    }
  }

  async _stop () {
    if (!this.state.recording) { return undefined }
    this.setState({ stoppedRecording: true, recording: false, paused: false })
    try {
      const filePath = await AudioRecorder.stopRecording()
      if (Platform.OS === 'android') {
        this._finishRecording(true, filePath)
      }
      return filePath
    } catch (error) {
      console.log(error)
    }
  }

  // async _checkPermission () {
  //   try {
  //     const rationale = {
  //       'title': 'Microphone Permission',
  //       'message': 'DACSEE needs access to your microphone so you can record audio.'
  //     }
  //     const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.RECORD_AUDIO, rationale)
  //     this.setState({ hasPermission: granted === PermissionsAndroid.RESULTS.GRANTED })
  //   } catch (e) {
  //     console.log(e)
  //   }
  // }

  async _record () {
    if (this.state.recording) {
      return
    }

    // if (this.state.stoppedRecording) {
    //   this.initPath()
    // }

    this.setState({ recording: true, paused: false })

    try {
      await AudioRecorder.startRecording() // NEVER USED
    } catch (error) {
      console.log(error)
    }
  }

  _finishRecording (didSucceed, filePath) {
    this.setState({ finished: didSucceed })
  }

  _setProgress () {
    Animated.loop(Animated.timing(this.state.progress, {
      toValue: 1,
      duration: 3000
    })).start()
  }

  _reload () {
    Animated.timing(this.state.progress, {
      toValue: 0,
      duration: 3000
    }).start()
  }

  componentWillUnmount () {}

  _renderContent () {
    // /*currentTime,
    const { error, waiting } = this.state
    const {errorIcon} = this.props
    const { errorText } = this.state
    return (
      error ? (
        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
          { errorIcon }
          <Text style={{ color: '#fff', marginTop: 10, textAlign: 'center' }}>{errorText}</Text>
        </View>
      ) : (
        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
          {
            waiting ? (
              <ActivityIndicator color={'#fff'} size='large' />
            ) : (
              <Lottie source={Resources.animation.speech_start} progress={this.state.progress} style={{ width: 150, height: 150 }} />
            )
          }
        </View>
      )
    )
  }

  render () {
    let pos = (height - 64 - 44 - 200) / 2
    const view = this.state.isShow ? (
      <View style={[styles.container, { top: pos }]} pointerEvents='none'>
        <Animated.View style={[styles.content, { opacity: this.state.opacityValue }, this.props.style]}>
          { this._renderContent()}
        </Animated.View>
      </View>
    ) : null
    return view
  }
}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    left: 0,
    right: 0,
    elevation: 999,
    alignItems: 'center',
    zIndex: 10000
  },
  content: {
    backgroundColor: 'rgba(0,0,0,0.6)',
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
    width: 150,
    height: 150
    // opacity:0.6
  },
  text: {
    color: 'white'
  }
})

VoiceModal.propTypes = {
  style: ViewPropTypes.style,
  textStyle: Text.propTypes.style,
  positionValue: PropTypes.number,
  fadeInDuration: PropTypes.number,
  fadeOutDuration: PropTypes.number,
  opacity: PropTypes.number,
  sendVoice: PropTypes.func
}

VoiceModal.defaultProps = {
  textStyle: styles.text,
  positionValue: 120,
  fadeInDuration: 500,
  fadeOutDuration: 200,
  opacity: 1,
  sendVoice: () => {}
}
