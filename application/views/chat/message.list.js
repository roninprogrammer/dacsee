import React, {Component} from 'react'
import {View, Text, StyleSheet,
  TouchableOpacity,Dimensions, Image,SafeAreaView, FlatList} from 'react-native'
import {inject, observer} from 'mobx-react'
import {NavigationActions} from 'react-navigation'
const {height, width} = Dimensions.get('window')
import ActionSheet from 'react-native-actionsheet'
import { observable, computed, action, autorun } from 'mobx'


@inject('app','chat')
@observer
class MessageList extends Component {
  @observable select_key = ''

  handlePress = ( rowData ) => {
    global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'ChatWindow',params:{ friendData:rowData } }))
  }


  handleLongPress = (list) =>{
    this.ActionSheet.show()
    this.select_key = list.key
  }

  _del = (index)=>{
    if (index === 0) {
      this.select_key.length>0 && this.props.chat.removeChatMsg(this.select_key)
    }
  }

  render () {
    const { app={}, chat={} } = this.props
    const { language, strings } = app
    const { msgList } = chat
    const currentList = msgList.length > 0 ? msgList.sort((a,b)=> b.time - a.time) : []

    return (
      <SafeAreaView style={styles.container}>
        <View style={{flex:1}}>
          <FlatList
            ref = {(e)=> this.swipe = e}
            data = { currentList }
            ListEmptyComponent={()=>
              <View style={{flex:1,justifyContent:'center',alignItems:'center', marginTop:180}}>
                <Text>{strings.no_msg}</Text>
              </View>}
            keyExtractor={(item, index) => item.key}
            renderItem={({item}) =>
              <ItemList language={language} list={item} itemPress={this.handlePress} itemLongPress={this.handleLongPress} strings={app.strings}/>
            }
          />
          <ActionSheet
            ref={e => this.ActionSheet = e}
            options={[strings.del, strings.cancel]}
            cancelButtonIndex={2}
            onPress={(index)=>this._del(index)}
          />

        </View>
      </SafeAreaView>
    )
  }
}


class ItemList extends Component{

  _rendercontent=(data)=>{
    const { strings } = this.props
    let message = data
    switch (message.type){
    case 'image':
      return '[图片]'
    case 'voice' :
      return `[${strings.voice}]`
    default :
      return message.content
    }
  }

  render(){
    const { list, itemPress=()=>{}, language, itemLongPress=()=>{} } = this.props
    return(
      <TouchableOpacity onPress={()=>itemPress(list)} onLongPress={()=>itemLongPress(list) }activeOpacity={1} style={{paddingLeft:12,backgroundColor:'#fff'}}>
        <View style={{ flexDirection:'row',alignItems:'center',borderBottomWidth:0.8,borderColor: '#DDD',paddingVertical:8}}>
          <Image
            style={styles.card__avatar}
            source={[{width: 50, height: 50, uri: $.method.utils.getHeaderPicUrl(list.friend_info.avatars)}]} />
          {
            list.isRead?
              null:
              <View style={[styles.circle,
                list.unReadNumber>99 ?
                  {paddingHorizontal:7,left:22} :
                  list.unReadNumber>10 ?
                    {height:24,width:24,borderRadius:12, left:36} :
                    {width:20}]}>
                <Text style={{color:'#fff',backgroundColor:'transparent',fontSize:12}}>{list.unReadNumber>99?'99+':list.unReadNumber}</Text>
              </View>
          }
          <View style={styles.card__text}>
            <Text style={styles.card__name} numberOfLines={1} ellipsizeMode={'middle'} >
              {
                list.friend_info.fullName
              }
            </Text>
            <Text style={styles.card__summary} numberOfLines={1}>{this._rendercontent(list.firstMsg)}</Text>
          </View>
          <View style={{position:'absolute',right:10,top:12,justifyContent:'center',alignItems:'center'}}>
            <Text style={{fontSize:12,color:'rgb(135,135,135)'}}>{$.method.utils.getCurrentTime(new Date(list.time).getTime(),true,language)}</Text>
          </View>
        </View>
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  card: {
    flex: 1,
    height: 60,
    flexDirection: 'row',
    alignItems: 'center',
  },

  card__avatar: {
    width: 50,
    height: 50,
    borderRadius:5
  },

  card__text: {
    justifyContent:'center',
    paddingLeft:12
  },

  card__name: {
    width:width - 150,
    fontSize: 16,
    color: '#333',
  },

  card__summary: {
    fontSize:12,
    marginTop:5,
    color: '#888',
    width:width-100

  },

  first_msg:{
    color: '#888'
  },

  circle:{
    height:20,
    borderRadius:10,
    backgroundColor:'#e4393c',
    position:'absolute',
    left:38,
    top:3,
    justifyContent:'center',
    alignItems:'center',
    zIndex:999
  }
})

export default MessageList
