import React, { Component } from 'react'
import {
  Text,
  View,
  Image,
  TouchableOpacity,
  Keyboard,
  StyleSheet,
  Platform,
  LayoutAnimation,
  TouchableWithoutFeedback,
  TouchableHighlight,
  ActivityIndicator,
  DeviceEventEmitter,
  Alert,
  ListView,
  TextInput,
  PermissionsAndroid,
  BackHandler,
  SafeAreaView
} from 'react-native'
import { Screen, Icons, Define, System, DCColor, TextFont } from 'dacsee-utils'
import Voice from './voice.modal'
import RNFS from 'react-native-fs'
import { SvgIcon, iconPath } from './chatIcon'
import Sound from 'react-native-sound'
import { inject, observer } from 'mobx-react'
import { AudioRecorder, AudioUtils } from 'react-native-audio'
const { height, width } = Screen.window

let playTime = null
let voiceSound = null

@inject('app', 'chat', 'account')
@observer
class ChatWindow extends Component {
  static navigationOptions = ({ navigation }) => {
    const { params } = navigation.state
    const { chatType = 'friend' } = params
    const title = chatType === 'friend' ? params.friendData.friend_info.fullName : params.groupData.group_info.name
    const avatars = chatType === 'friend' ? params.friendData.friend_info.avatars : params.groupData.group_info.avatars
    return {
      drawerLockMode: 'locked-closed',
      title: title,
      headerRight: (
        <TouchableOpacity
          activeOpacity={0.7}
          style={{ paddingRight: 15, justifyContent: 'center', alignItems: 'flex-end' }}
          onPress={navigation.state.params && navigation.state.params.rightPress}
        >
          <Image style={{ width: 36, height: 36, borderRadius: 18, borderWidth: 0.6, borderColor: DCColor.BGColor('primary-2') }} source={{ uri: $.method.utils.getHeaderPicUrl(avatars) }} />
        </TouchableOpacity>
      )
    }
  }

  constructor(props) {
    super(props)
    const { chat } = props
    const { setChatId, isOpen } = chat
    const { params } = props.navigation.state
    const { chatType = 'friend' } = params
    const id = chatType === 'friend' ? params.friendData.friend_info._id : params.groupData.group_info._id
    this.chatType = chatType
    this.currentMaxRowId = 0
    this._id = id // group_id or friend_id
    this.time = null
    this._userHasBeenInputed = false
    this.scrollEvent = {}
    isOpen(true, { type: 'FRIEND', id })
    setChatId(id)
    this.ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => {
        return r1.key !== r2.key
      }
    })
    this.state = {
      messageContent: '',
      listVisibleHeight: 0,
      keyboardShow: false,
      keyboardHeight: 0,
      visibleHeight: height,
      showVoice: false,
      xHeight: 20,
      saveChangeSize: 0,
      inputChangeSize: 0,
      voiceLength: 0,
      voiceEnd: false,
      microphonePermission: false
    }
  }

  componentDidMount() {
    const { navigation } = this.props
    const { goBack } = navigation;
    const { params } = navigation.state
    System.Platform.iOS && this._willShow()
    System.Platform.iOS && this._willHide()

    this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
      goBack();
      return true
    })
    this.alert = DeviceEventEmitter.addListener('permissionAlert', () => Alert.alert('无法访问', '请前往 设置 - Dacsee 中开启麦克风权限.', [
      { text: '确定', onPress: () => console.log('Cancel Pressed') }
    ]))
    navigation.setParams({ rightPress: () => navigation.navigate('FriendsDetail', { friendData: params.friendData, type: 'FRIEND' }) })
    System.Platform.Android && this._checkPermission()
    System.Platform.iOS && this._requestAuthorization()
  }

  componentWillUnmount() {
    this.backHandler.remove()
    this.chatItem && this.chatItem._voiceStop()
    this.props.chat.isOpen(false)
    this.props.chat.setChatId('')
    let { chat } = this.props
    let { removeMsg } = chat
    const targetKey = `${this.chatType}_${this._id}`
    System.Platform.iOS && this._willRemove()
    // this.keyboardDidShowListener.remove()
    this.time && clearTimeout(this.time)
    voiceSound && clearInterval(voiceSound)
    removeMsg(targetKey)
  }

  _willShow() {
    this.keyboardWillShowListener = Keyboard.addListener('keyboardWillShow', (e) => {
      const visibleHeight = height - e.endCoordinates.height
      LayoutAnimation.configureNext(LayoutAnimation.create(
        e.duration,
        LayoutAnimation.Types[e.easing],
        LayoutAnimation.Properties.scaleXY
      ))
      this.setState({
        visibleHeight,
        xHeight: 0,
        keyboardHeight: e.endCoordinates.height,
        keyboardShow: true
      })
      // this.chatList.scrollToEnd({animated:true})
    })
  }

  _willHide() {
    this.keyboardWillHideListener = Keyboard.addListener('keyboardWillHide', (e) => {
      LayoutAnimation.configureNext(LayoutAnimation.create(
        e.duration,
        LayoutAnimation.Types[e.easing],
        LayoutAnimation.Properties.scaleXY
      ))
      this.setState({ keyboardHeight: 0, keyboardShow: false, visibleHeight: height, xHeight: 20 })
    })
  }

  _willRemove() {
    this.keyboardWillShowListener.remove()
    this.keyboardWillHideListener.remove()
  }

  _sendMessage(type, messageContent, voiceLen = 0, voiceData = '') {
    this._userHasBeenInputed = true
    if (type === 'text' && messageContent.trim().length === 0) {
      return undefined
    }
    const { account, chat } = this.props
    const targetKey = `${this.chatType}_${this._id}`
    let content = {}
    content.type = type
    content.content = messageContent
    if (voiceLen >= 1) content.len = voiceLen
    if (voiceData.length > 0) content.data = voiceData
    let msgSend = {}
    msgSend.from_id = account.user._id
    msgSend.to_id = this._id
    msgSend.type = this.chatType
    msgSend.content = content
    chat.SendMsg(msgSend, targetKey)
    this.InputBar.input && this.InputBar.input.clear()
    this.setState({ messageContent: '' })
    this.time && clearTimeout(this.time)
    this.time = setTimeout(() => { this.chatList && this.chatList.scrollToEnd({ animated: true }) }, 200)
  }

  _reSendMessage(content) {
    this.chatItem && this.chatItem._voiceStop()
    const targetKey = `${this.chatType}_${this._id}`
    this.props.chat.SendMsg(content, targetKey)
  }

  async _checkPermission() {
    try {
      const rationale = {
        'title': 'Microphone Permission',
        'message': 'DACSEE needs access to your microphone so you can record audio.'
      }
      const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.RECORD_AUDIO, rationale)
      this.setState({ microphonePermission: granted === PermissionsAndroid.RESULTS.GRANTED })
    } catch (e) {
      console.log(e)
    }
  }

  async _requestAuthorization() {
    try {
      const res = await AudioRecorder.requestAuthorization()
      this.setState({ microphonePermission: res })
    } catch (e) {
      console.log(e)
    }
  }

  _changeMethod() {
    const { microphonePermission } = this.state
    if (!this.state.showVoice && microphonePermission) {
      this.setState({ showVoice: true })
      this.setState({ saveChangeSize: this.state.inputChangeSize })
      this.time && clearTimeout(this.time)
      this.time = setTimeout(() => this.InputBar.input && this.InputBar.input.focus(), 300)
      return
    }
    this.setState({ showVoice: !this.state.showVoice })
    this.setState({ saveChangeSize: this.state.inputChangeSize })
    this.time && clearTimeout(this.time)
    this.time = setTimeout(() => this.InputBar.input && this.InputBar.input.focus(), 300)
  }

  _changeText(e) {
    this.setState({ messageContent: e })
  }

  _jump(url) {
    const { navigation } = this.props
    let safeUrl = this._safeUrl(url)
    navigation.navigate('SettingWetView', {
      title: '',
      source: { uri: safeUrl }
    })
  }

  _safeUrl(url) {
    if (url.indexOf('https') === 0) return url
    if (url.indexOf('http') === 0) return url.replace('http://', 'https://')
    if (url.indexOf('http') === -1) return 'https://' + url
  }

  _onContentSizeChange(e) {
    const changeHeight = e.nativeEvent.contentSize.height
    if (changeHeight === 34) return
    this.setState({ inputChangeSize: changeHeight <= 70 ? changeHeight : 70 })
    this.chatList && this.chatList.scrollToEnd({ animated: true })
  }

  _onVoiceStart() {
    this.chatItem && this.chatItem._voiceStop()
    this.setState({ voiceEnd: true })
    this.voice.show()
  }

  _onVoiceEnd() {
    this.voice.close()
    this.setState({ voiceEnd: false })
  }

  _PressAvatar = () => {
    const { params } = this.props.navigation.state
    const { friendData } = params
    this.props.navigation.navigate('FriendsDetail', { friendData, type: 'FRIEND' })
  }

  _scrollToBottom(even) {
    let scrollProperties = this.chatList.scrollProperties

    // 如果组件没有挂载完全，则不进行内容偏移
    if (!scrollProperties.visibleLength) { return }

    setTimeout(() => {
      this.chatList && this.chatList.scrollToEnd({
        animated: this._userHasBeenInputed
      })
    }, this._userHasBeenInputed ? 0 : 130)
  }

  render() {
    const { messageContent, xHeight, visibleHeight, voiceEnd, inputChangeSize, hasPermission } = this.state
    const { account = {}, chat = {}, app, navigation } = this.props
    const { user } = account
    const { strings } = app
    const { chatList = {}, addFilePath } = chat
    const targetKey = `${this.chatType}_${this._id}`
    const { params } = navigation.state
    const { chatType = 'friend' } = params
    const to_info = chatType === 'friend' ? params.friendData.friend_info : params.groupData.group_info
    const currentList = chatList[targetKey] !== undefined ? chatList[targetKey].slice() : []

    return (
      <SafeAreaView style={Platform.OS === 'android' ? { flex: 1, backgroundColor: 'white' } : { backgroundColor: 'white', height: visibleHeight - (Define.system.ios.x ? 88 : 64) }}>
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
          <ListView
            removeClippedSubviews={false}
            ref={e => this.chatList = e}
            dataSource={this.ds.cloneWithRows(currentList)}
            enableEmptySections
            onLayout={
              (event) => {
                this._scrollToBottom()
              }
            }
            onContentSizeChange={
              (event) => {
                this._scrollToBottom()
              }
            }

            renderRow={(rowData, sectionID, rowId) => {
              this.currentMaxRowId += rowId
              return (<ChatItem
                ref={e => this.chatItem = e}
                content={rowData}
                to_info={to_info}
                user={user}
                addFilePath={addFilePath}
                targetKey={targetKey}
                jump={(url) => this._jump(url)}
                reSend={(content) => this._reSendMessage(content)}
              />
              )
            }
            }
          />
        </View>
        <InputBar
          ref={e => this.InputBar = e}
          placeholder={`${strings.input_msg}...`}
          onMethodChange={this._changeMethod.bind(this)}
          showVoice={this.state.showVoice}
          onSubmitEditing={(type, content) => this._sendMessage(type, content)}
          messageContent={messageContent}
          textChange={this._changeText.bind(this)}
          onContentSizeChange={this._onContentSizeChange.bind(this)}
          xHeight={xHeight}
          voiceStart={this._onVoiceStart.bind(this)}
          voiceEnd={this._onVoiceEnd.bind(this)}
          isVoiceEnd={voiceEnd}
          inputChangeSize={inputChangeSize}
          hasPermission={hasPermission}
          pressInText={`${strings.hold_talk}`}
          pressOutText={`${strings.loosen_send}`}
        />
        {this.state.showVoice ? <Voice
          microphonePermission={this.state.microphonePermission}
          ref={(e) => this.voice = e}
          sendVoice={(type, content, voiceLen, data) => this._sendMessage(type, content, voiceLen, data)}
          strings={strings}
          errorIcon={<SvgIcon size={60} fill={['#fff']} path={iconPath.warning} />
          }
        /> : null}
      </SafeAreaView>
    )
  }
}

class InputBar extends Component {
  render() {
    const {
      messageContent,
      onSubmitEditing = () => { },
      textChange = () => { }, onMethodChange = () => { }, onContentSizeChange = () => { }, voiceStart = () => { }, voiceEnd = () => { },
      showVoice,
      xHeight,
      isVoiceEnd,
      inputChangeSize, placeholder, pressInText, pressOutText
    } = this.props
    return (
      <View style={[styles.commentBar, { paddingBottom: 0 }]} >
        <View style={{ flexDirection: 'row', alignItems: 'center', marginVertical: 5 }}>
          <View style={{ width: (width - 36) * 0.1, height: 34, justifyContent: 'center', alignItems: 'center' }} activeOpacity={0.7}>
            <TouchableOpacity onPress={onMethodChange} style={{ width: 34, height: 34, justifyContent: 'center', alignItems: 'center', borderColor: '#ddd', borderWidth: 0.8, borderRadius: 17 }}>
              <SvgIcon size={20} fill={['#bbb']} path={showVoice ? iconPath.keyboard : iconPath.leftVoice} />
            </TouchableOpacity>
          </View>
          <View style={{ backgroundColor: '#fff', borderRadius: 18, marginHorizontal: 8, justifyContent: 'center', width: 0.9 * (width - 36), borderColor: '#ddd', borderWidth: showVoice ? 0.6 : 0 }}
          >
            {showVoice
              ? <TouchableHighlight onPressIn={voiceStart} onPressOut={voiceEnd} activeOpacity={0.7} underlayColor={'#eee'}
                style={{ borderRadius: 18 }}>
                <View style={[{ justifyContent: 'center', alignItems: 'center', flexDirection: 'row', paddingVertical: 6 }]}>
                  {Icons.Generator.Ion('ios-mic-outline', 20, '#bbb')}
                  <Text style={{ fontSize: 16, color: '#4d4d4d', marginLeft: 10 }}>
                    {isVoiceEnd ? `${pressOutText}` : `${pressInText}`}
                  </Text>
                </View>
              </TouchableHighlight>
              : <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                <TextInput
                  ref={e => this.input = e}
                  multiline
                  blurOnSubmit={false}
                  placeholder={placeholder}
                  placeholderTextColor={'#5f5d70'}
                  onContentSizeChange={onContentSizeChange}
                  underlineColorAndroid='transparent'
                  onChangeText={textChange}
                  value={messageContent}
                  style={[styles.commentBar__input, { height: Math.max(35, inputChangeSize), paddingHorizontal: 16, paddingTop: System.Platform.iOS ? 8 : 0 }]}
                />
                <View style={{ width: (width - 36) * 0.1, alignItems: 'center', justifyContent: 'center' }}>
                  <TouchableOpacity style={styles.sendButton} onPress={onSubmitEditing.bind(this, 'text', messageContent)} activeOpacity={0.7}>
                    <View style={{ paddingRight: 2 }} >
                      {Icons.Generator.Awesome('send', 15, '#fff')}
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
            }
          </View>
        </View>
      </View>
    )
  }
}

const PATTERNS = {
  url: /(https?:\/\/|www\.)[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&\/\/=]*)/i,
  phone: /[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,7}/
}

class ChatItem extends Component {
  constructor(props) {
    super(props)
    this.state = {
      progress: 2,
      leftIcons: [iconPath.leftVoiceOne, iconPath.leftVoiceTwo, iconPath.leftVoiceThree],
      rightIcons: [iconPath.rightVoiceOne, iconPath.rightVoiceTwo, iconPath.rightVoiceThree],
      loading: false
    }
  }

  componentDidMount() {
    this.subscription = DeviceEventEmitter.addListener('INIT_PROGRESS', () => this.setState({ progress: 2 }))
  }

  componentWillUnmount() {
    this.subscription && this.subscription.remove()
  }

  _renderContent = () => {
    const { content, user, jump = () => { }, addFilePath = () => { }, targetKey } = this.props
    const isSelf = user._id === content.from_id
    const { loading } = this.state
    const msgContent = content.content
    let reg = PATTERNS.url
    let resArr,
      splitArr,
      text,
      url = null
    if (msgContent.type === 'text') {
      resArr = msgContent.content.match(reg)
      url = resArr && resArr[0]
      splitArr = resArr && msgContent.content.split(url)
      text = !splitArr ? <View style={[styles.txtArea, { backgroundColor: isSelf ? DCColor.BGColor('primary-1') : '#f4f4f4' }, { shadowOffset: { width: 5, height: 5 }, }]}>
        <Text selectable style={{ fontSize: TextFont.TextSize(13), fontWeight: '600', lineHeight: 20, color: isSelf ? DCColor.BGColor('primary-2') : '#5f5d70' }}>{msgContent.content}</Text>
      </View> : <View style={[styles.txtArea, { backgroundColor: isSelf ? DCColor.BGColor('primary-1') : '#f4f4f4' }, { shadowOffset: { width: 5, height: 5 }, }]}>
          <Text selectable style={{ fontSize: TextFont.TextSize(13), fontWeight: '600', lineHeight: 20, color: isSelf ? '#fff' : '#5f5d70' }}>{splitArr[0]}<Text style={{ color: 'green' }} onPress={jump.bind(this, url)}>{url}</Text>{splitArr[1]}</Text>
        </View>
    }

    switch (msgContent.type) {
      case 'text':
        return (text)
      case 'voice':
        return (
          <View style={{ flexDirection: isSelf ? 'row-reverse' : 'row' }}>
            <TouchableOpacity style={[styles.voiceArea, loading ? { backgroundColor: isSelf ? '#217f6a' : '#dfdfdf' } : { backgroundColor: isSelf ? DCColor.BGColor('primary-1') : '#f4f4f4' }, { shadowOffset: { width: 5, height: 5 }, }]} onPress={() => { this._playVoice(msgContent, isSelf, targetKey, addFilePath) }} activeOpacity={0.7}>
              <View style={[{ width: 40 + (msgContent.len > 1 ? msgContent.len * 2 : 0), alignItems: isSelf ? 'flex-end' : 'flex-start' }, isSelf ? { alignItems: 'flex-end', right: 10 } : { alignItems: 'flex-start', left: 10 }, { maxWidth: width - 160 }]}>
                <SvgIcon size={20} fill={[isSelf ? '#fff' : '#aaa']} path={isSelf ? this.state.rightIcons[this.state.progress] : this.state.leftIcons[this.state.progress]} />
              </View>
            </TouchableOpacity>
            <View style={{ justifyContent: 'flex-end' }}>
              <Text style={[{ color: '#aaa', marginBottom: 4 }, isSelf ? { marginRight: 4 } : { marginLeft: 4 }]}>
                {`${msgContent.len}"`}
              </Text>
            </View>
          </View>)
    }
  }

  async checkDir() {
    if (!await RNFS.exists(`${AudioUtils.DocumentDirectoryPath}/voice/`)) {
      RNFS.mkdir(`${AudioUtils.DocumentDirectoryPath}/voice/`)
    }
  }
  async _playVoice(msgContent, isSelf, targetKey, callBack) {
    console.log(msgContent, isSelf)
    if (this.state.loading) return
    // 将Data写入临时文件
    // 变更路径
    await this.checkDir()
    let path = `${RNFS.DocumentDirectoryPath}/voice/voice${msgContent.id}.aac`
    // write the file
    if (isSelf) {
      path = msgContent.content
    } else {
      if (msgContent.filePath) {
        path = msgContent.filePath
      } else {
        await RNFS.writeFile(path, msgContent.data, 'base64')
      }
    }
    if (voiceSound) {
      if (path === voiceSound._filename) {
        this._voiceStop()
        return
      } else {
        this._voiceStop()
      }
    }
    this.setState({ loading: true })
    voiceSound = new Sound(path, '', (error) => {
      if (error) {
        console.log('failed to load the sound', error)
        this.setState({ loading: false })
      } else {
        this._play(msgContent.len)
        this.setState({ loading: false })
        voiceSound.play((success) => {
          if (success) {
            playTime && clearInterval(playTime)
            this.setState({ progress: 2 })
            console.log('successfully finished playing')
            voiceSound = null
            if (msgContent.filePath === undefined && !isSelf) callBack(path, msgContent.id, targetKey)
          } else {
            console.log('playback failed due to audio decoding errors')
          }
        })
      }
    })
  }

  _play() {
    playTime && clearInterval(playTime)
    let index = 0
    const { progress } = this.state
    if (progress === 2) index = 2
    playTime = setInterval(() => {
      if (index === 2) {
        index = -1
      }
      index += 1
      this.setState({ progress: index })
    }, 400)
  }

  _voiceStop() {
    playTime && clearInterval(playTime)
    DeviceEventEmitter.emit('INIT_PROGRESS')
    this.setState({ progress: 2 })
    voiceSound && voiceSound.stop()
    voiceSound = null
  }

  render() {
    const { user = {}, content, reSend = () => { }, to_info = {} } = this.props
    const { avatars } = user
    const isSelf = user._id === content.from_id
    const { loading } = this.state
    return (
      <TouchableWithoutFeedback>
        <View>
          {
            content.renderTime ? <View style={{ justifyContent: 'center', alignItems: 'center', paddingTop: 10 }}>
              <View style={{ backgroundColor: 'rgba(204,204,204,.5)', paddingVertical: 3, paddingHorizontal: 4, borderRadius: 3 }}>
                <Text style={{ color: '#fff', fontSize: 10 }}>{$.method.utils.getCurrentTime(new Date(content.time).getTime())}</Text>
              </View>
            </View>
              : null
          }
          <View style={[styles.chat, isSelf ? styles.right : styles.left]} ref={(e) => this.content = e} >
            <TouchableOpacity activeOpacity={0.7}>
              <Image
                source={[{ uri: $.method.utils.getHeaderPicUrl(isSelf ? avatars : to_info.avatars), width: 30, height: 30 }]}
                style={styles.avatar} />
            </TouchableOpacity>
            <View style={[isSelf ? styles.right : styles.left]}>
              <View style={[styles.triangle, isSelf ? styles.right_triangle : styles.left_triangle, loading ? { borderColor: isSelf ? '#217f6a' : '#dfdfdf' } : { borderColor: isSelf ? DCColor.BGColor('primary-1') : '#f4f4f4' }]} />
              {this._renderContent()}
              <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                {!isSelf ? null : content.sendStatus === undefined ? null : content.sendStatus === 1 ? <ActivityIndicator style={{ marginRight: 10 }} /> : content.sendStatus === 3 ? <TouchableOpacity
                  activeOpacity={0.7} onPress={reSend.bind(this, content)}>
                  <SvgIcon
                    style={{ marginRight: 10 }} size={20} fill={[DCColor.BGColor('red')]} path={iconPath.warning_fill} />
                </TouchableOpacity>
                  : null}
              </View>
            </View>
          </View>
        </View>
      </TouchableWithoutFeedback>
    )
  }
}

const styles = StyleSheet.create({
  commentBar: {
    width: width,
    backgroundColor: '#fff',
    alignItems: 'center',
    borderTopWidth: 1,
    borderColor: 'rgba(0, 0, 0, 0.1)'
  },
  commentBar__input: {
    borderRadius: 18,
    height: 40,
    width: 0.8 * (width - 50),
    padding: 0,
    paddingHorizontal: 20,
    backgroundColor: '#f5f5f8'
  },
  circle: {
    width: 34,
    height: 34,
    borderRadius: 17,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#ddd',
    borderWidth: 0.8
  },
  chat: {
    paddingHorizontal: 18,
    paddingVertical: 14
  },
  right: {
    flexDirection: 'row-reverse'
  },
  left: {
    flexDirection: 'row'
  },
  txtArea: {
    borderRadius: 12,
    paddingHorizontal: 15,
    paddingVertical: 10,
    backgroundColor: '#f4f4f4',
    justifyContent: 'center',
    maxWidth: width - 130,
    flexWrap: 'wrap',
    minHeight: 20
  },
  voiceArea: {
    borderRadius: 12,
    maxWidth: width - 160,
    justifyContent: 'center',
    minHeight: 30
  },
  avatar: {
    marginHorizontal: 8,
    borderRadius: 24,
    width: 48,
    height: 48
  },
  triangle: {
    width: 0,
    height: 0,
    zIndex: 999,
    borderWidth: 8,
    borderTopColor: 'transparent',
    borderBottomColor: 'transparent',
    marginTop: 16
  },
  left_triangle: {
    borderLeftWidth: 0
  },
  right_triangle: {
    borderRightWidth: 0
  },
  sendButton: {
    width: 36,
    height: 36,
    borderRadius: 18,
    backgroundColor: DCColor.BGColor('primary-1'),
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#fff',
    borderWidth: 2
  }
})

export default ChatWindow
