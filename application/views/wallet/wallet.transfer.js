import React, { Component } from 'react'
import { Text, View, ScrollView } from 'react-native'
import Input from '../../components/input'
import { DCColor, TextFont } from 'dacsee-utils'
import { Button } from '../../components'
import { inject, observer } from 'mobx-react'



@inject('app', 'wallet')
@observer
export default class WalletTransferScreen extends Component {

  static navigationOptions = ({ navigation }) => {
    const { strings } = $.store.app
    return {
      drawerLockMode: 'locked-closed',
      title: strings.transfer
    }
  }

  constructor(props) {
    super(props)
    const { selected_wallet } = this.props.navigation.state.params
    this.state = {
      countryCode: selected_wallet.countryCode,
      phoneCountryCode: '+60',
      searchType: 0,
      searchTitle: 'phone',
      searchContent: '',
      amount: 0,
      remark: '',
    }
  }

  _fetchData() {
    const { countryCode, searchType, searchContent, phoneCountryCode, amount, remark } = this.state
    const { searchUser } = this.props.wallet
    let body
    if (searchType == 0) {
      body = {
        country: countryCode,
        phoneCountryCode: phoneCountryCode,
        phoneNo: searchContent
      }
    } else if (searchType == 1) {
      body = {
        country: countryCode,
        email: searchContent
      }
    } else {
      body = {
        country: countryCode,
        userId: searchContent
      }
    }
    searchUser(body, amount, remark)
  }

  onSubmit = () => {
    const { amount, searchContent } = this.state
    let reg = /(^(([0-9]+\.[0-9]{1,2})|([0-9]*[1-9][0-9]*\.[0-9]{1,2})|([0-9]*[1-9][0-9]*))$)/
    if (amount <= 0 | !reg.test(amount)) {
      return null // To do add in error message 
    } else if (searchContent == '') {
      return null // To do add in error message 
    } else {
      this._fetchData()
    }
  }

  render() {
    const { strings } = this.props.app
    const { searching } = this.props.wallet
    return (
      <ScrollView
        style={{ flex: 1, backgroundColor: 'white' }}
        horizontal={false}
      // keyboardDismissMode={ 'on-drag' }
      >
        <View style={{ padding: 20 }}>
          <View style={{ borderBottomWidth: 1, borderBottomColor: '#a5a5a5' }}>
            <Text style={{ fontSize: TextFont.TextSize(12), opacity: 0.5, marginBottom: 5 }}>
              {strings.sending_amount}
            </Text>
            <View style={{ flexDirection: 'row', alignItems: 'center', height: 40 }}>
              {/* <Text style={{ fontSize: 14 }}>RM</Text> */}
              <Input style={{ flex: 1, fontSize: TextFont.TextSize(14) }} placeholder={'0.00'} keyboardType={'numeric'} onChangeText={(value) => this.setState({ amount: value })} />
            </View>
          </View>

          <View style={{ paddingTop: 20, borderBottomWidth: 1, borderBottomColor: '#a5a5a5' }}>
            <Text style={{ fontSize: TextFont.TextSize(12), opacity: 0.5, marginBottom: 5 }}>
              {strings.recipient_account_type}
            </Text>
            <CheckBox titles={['phone', 'email', 'userid']} style={{ flex: 1, height: 44 }}
              strings={strings}
              onPress={(index, title) => {
                this.setState({
                  searchType: index,
                  searchTitle: title,
                  accountVal: this.state.searchType == index ? this.state.accountVal : ''
                })
              }} />
          </View>

          <View style={{ paddingTop: 20, borderBottomWidth: 1, borderBottomColor: '#a5a5a5' }}>
            <Text style={{ fontSize: TextFont.TextSize(12), opacity: 0.5, marginBottom: 5 }}>
              {strings.recipient_account}
            </Text>
            <View style={{ flex: 1, flexDirection: 'row' }}>
              {
                this.state.searchType == 0 ?
                  (
                    <Button style={{ marginRight: 10, height: 40, width: 60, justifyContent: 'center' }}
                      onPress={() => this.props.navigation.navigate('PublicPickerCountry', {
                        onPress: (code, name) => this.setState({ phoneCountryCode: code })
                      })} >
                      <Text style={{}}>{this.state.phoneCountryCode}</Text>
                    </Button>
                  ) : null
              }
              <Input
                style={{ flex: 1, fontSize: TextFont.TextSize(14), height: 40, justifyContent: 'center' }}
                placeholder={strings[this.state.searchTitle]}
                returnKeyType={'done'}
                onChangeText={(value) => this.setState({ searchContent: value, accountVal: value })}
              />
            </View>
          </View>

          <View style={{ paddingTop: 20, borderBottomWidth: 1, borderBottomColor: '#a5a5a5' }}>
            <Text style={{ fontSize: TextFont.TextSize(12), opacity: 0.5, marginBottom: 5 }}>
              {strings.remarks}
            </Text>
            <Input
              style={{ paddingVertical: 10, fontSize: TextFont.TextSize(14), height: 70, textAlignVertical: 'top' }}
              multiline={true}
              placeholder={strings.pls_enter_remarks}
              returnKeyType={'done'}
              onChangeText={(value) => this.setState({ remark: value })}
            />
          </View>

          <View style={{ paddingTop: 30, flex: 1, flexDirection: 'row', justifyContent: 'center' }}>
            <Button disabled={searching} onPress={() => this.onSubmit()} style={[{ width: 240, height: 44, borderRadius: 4 }, searching ? { backgroundColor: '#a5a5a5' } : { backgroundColor: DCColor.BGColor('primary-1') }]}>
              {
                searching ?
                  <Text style={{ fontSize: TextFont.TextSize(15), color: DCColor.BGColor('disable') }}>
                    {strings.searching}
                  </Text> :
                  <Text style={{ fontSize: TextFont.TextSize(15), fontWeight: 'bold', color: DCColor.BGColor('primary-2') }}>
                    {strings.next.toUpperCase()}
                  </Text>
              }
            </Button>
          </View>
        </View>
      </ScrollView>
    )
  }
}

class CheckBox extends Component {
  constructor(props) {
    super(props)
    this.state = {
      titles: this.props.titles,
      selectIndex: 0
    }
  }

  render() {
    const { titles, selectIndex } = this.state
    const { strings } = this.props
    return (
      <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
        {
          titles.map((item, index) => {
            return (
              <Button key={index} activeOpacity={1} onPress={() => {
                this.setState({
                  selectIndex: index
                })
                this.props.onPress(index, item)
              }} style={{ height: 40, justifyContent: 'center' }}>
                <Text style={selectIndex == index ? { fontSize: TextFont.TextSize(14), color: DCColor.BGColor('primary-1') } : { fontSize: TextFont.TextSize(14), color: '#a5a5a5', opacity: 0.7 }}>
                  {strings[item]}
                </Text>
              </Button>
            )
          })
        }
      </View>
    )
  }
}
