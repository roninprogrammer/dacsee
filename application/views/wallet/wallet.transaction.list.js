import React, { Component } from 'react'
import { Text, View, ListView, RefreshControl, Image } from 'react-native'
import InteractionManager from 'InteractionManager'
import moment from 'moment'
import { Screen, System, DCColor, TextFont } from 'dacsee-utils'
import Resources from 'dacsee-resources'
import { inject, observer } from 'mobx-react'

const { width } = Screen.window
const dataContrast = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })

@inject('app', 'wallet')
@observer
export default class WalletTransactionListScreen extends Component {

  static navigationOptions = ({ navigation }) => {
    const { strings } = $.store.app
    return {
      drawerLockMode: 'locked-closed',
      title: strings.wallet
    }
  }

  constructor(props) {
    super(props)
  }

  componentDidMount() {
    InteractionManager.runAfterInteractions()
    this._getList()
  }

  _getList = () => {
    const { type } = this.props.walletInfo
    this.props.wallet.getWalletDetail(type)
  }

  render() {
    const { strings } = this.props.app
    const { walletDetailList, transactionListLoading } = this.props.wallet
    return (
      <View style={{ backgroundColor: '#f8f8f8', flex: 1, width: width }}>
        {/* DETAIL */}
        <ListView
          removeClippedSubviews={false}
          refreshControl={
            <RefreshControl
              refreshing={transactionListLoading}
              onRefresh={this._getList}
              title={strings.pull_refresh}
              colors={['#ffffff']}
              progressBackgroundColor={DCColor.BGColor('primary-1')}
            />
          }
          dataSource={dataContrast.cloneWithRows(walletDetailList.slice())}
          enableEmptySections={true}
          renderRow={(row) => <DetailItem data={row} />}
          renderSectionHeader={() => (
            <View style={{ height: 2 }}></View>
          )}
          renderSeparator={() => <View style={{ height: 2, backgroundColor: '#f2f2f2' }} />}
        />
      </View>
    )
  }
}

class DetailItem extends Component {
  render() {
    const { data } = this.props
    const { type, amount, yourRef } = data
    const timestamp = moment(new Date(data.timestamp).getTime()).format('YYYY-MM-DD HH:mm:ss')
    return (
      <View style={{ paddingHorizontal: 26, paddingVertical: 14, flex: 1, backgroundColor: 'white', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between' }}>
        <View style={{ width: 40 }}>
          <Image source={amount > 0 ? Resources.image.transfer_in : Resources.image.transfer_out} />
        </View>
        <View style={{ flex: 1, justifyContent: 'space-between' }}>
          <Text ellipsizeMode={'middle'} numberOfLines={1} style={{ fontSize: TextFont.TextSize(15), color: '#333', fontWeight: System.Platform.Android ? '400' : '600', marginBottom: 4 }}>{type}</Text>
          {
            yourRef == null ? (
              <Text style={{ fontSize: TextFont.TextSize(12), color: '#666' }}>{timestamp}</Text>
            ) : (
                <View>
                  <View style={{ flexDirection: 'row', marginBottom: 4 }}>
                    <Text style={{ fontSize: TextFont.TextSize(12), color: '#666', fontWeight: '400', top: .5 }}>{timestamp}</Text>
                  </View>
                  <Text style={{ fontSize: TextFont.TextSize(12), color: '#666' }}>{yourRef.toUpperCase()}</Text>
                </View>
              )
          }
        </View>
        <View style={{ width: 80, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }}>
          <Text style={{ fontSize: TextFont.TextSize(18), textAlign: 'right', color: '#555', fontWeight: System.Platform.Android ? '200' : '400', fontFamily: 'Cochin' }}>{amount.toFixed(2)}</Text>
        </View>
      </View>
    )
  }
}
