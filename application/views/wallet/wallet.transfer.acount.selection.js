import React, { Component } from 'react'
import { Text, View, Image, TouchableOpacity, ListView } from 'react-native'
import { TextFont, Avatars } from 'dacsee-utils'
import { inject, observer } from 'mobx-react'


const dataContrast = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })

@inject('app', 'wallet')
@observer
export default class WalletTransferSelectionScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const { strings } = $.store.app
    return {
      drawerLockMode: 'locked-closed',
      title: strings.transfer_account_select
    }
  }

  constructor(props) {
    super(props)
  }

  render() {
    const { transferInfo } = this.props.navigation.state.params
    const { strings } = this.props.app
    return (
      <View style={{ flex: 1, backgroundColor: 'white' }}>
        <View style={{ marginTop: 20, alignItems: 'center' }}>
          <Text style={{ fontSize: TextFont.TextSize(13), opacity: 0.5 }}>
            {strings.multi_account_tip}
          </Text>
          <Text style={{ marginTop: 10, fontSize: TextFont.TextSize(13), opacity: 0.5 }}>
            {strings.choose_transfer_account}
          </Text>
        </View>

        <ListView
          removeClippedSubviews={false}
          dataSource={dataContrast.cloneWithRows(transferInfo.userList)}
          enableEmptySections={true}
          renderRow={(row) => <TouchableOpacity onPress={() => {
            let temp = transferInfo
            temp.userList = [row]
            this.props.navigation.navigate('WalletTransferSummary', { transferInfo: temp })
          }
          } activeOpacity={.7}><AccountItem data={row} /></TouchableOpacity>}
          renderSeparator={() => <View style={{ height: 2, backgroundColor: '#f2f2f2' }} />}
          style={{ flex: 1, marginTop: 30 }}
        />
      </View>
    )
  }
}

class AccountItem extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    const { data } = this.props
    const { avatars } = data
    return (
      <View style={{ flexDirection: 'row' }}>
        <Image source={{ uri: Avatars.getHeaderPicUrl(avatars) }} style={{ margin: 15, width: 66, height: 66, borderRadius: 33 }} />
        <View style={{ justifyContent: 'center' }}>
          <Text style={{ fontSize: TextFont.TextSize(11), opacity: 0.6 }}>{data.userId}</Text>
          <Text style={{ fontSize: TextFont.TextSize(17) }}>{data.fullName}</Text>
        </View>
      </View>
    )
  }
}
