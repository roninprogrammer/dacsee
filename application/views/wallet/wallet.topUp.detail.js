import React, { Component } from 'react'
import { Text, View, StyleSheet, TouchableOpacity, Linking } from 'react-native'
import { inject, observer } from 'mobx-react'
import { TextFont, DCColor } from 'dacsee-utils'

@inject('app', 'account')
@observer
export default class WalletTopUpDetail extends Component {

  static navigationOptions = ({ navigation }) => {
    return {
      drawerLockMode: 'locked-closed',
      title: $.store.app.strings.cash_deposit,
    }
  }

  constructor(props) {
    super(props)
    const { app } = props
    this.bankOptions = [
      { type: app.strings.cash_deposit_bank_name, value: 'Public Bank Berhad' },
      { type: app.strings.cash_deposit_account_number, value: '3209651525' },
      { type: app.strings.cash_deposit_account_holder, value: 'DMD Technology SDN BHD' },
      { type: app.strings.cash_deposit_recipient_reference, value: props.account.user.userId },
      { type: app.strings.cash_deposit_other_detail_pay, value: props.account.user.userId },
    ]

    const three = app.strings.cash_deposit_tips_three.replace('USER-ID', props.account.user.userId)
    this.tips = [
      app.strings.cash_deposit_tips_one,
      app.strings.cash_deposit_tips_two,
      three,
      app.strings.cash_deposit_tips_four
    ]
  }

  async componentDidMount() {

  }
  componentWillUnmount() { }


  render() {
    const { navigation } = this.props
    const { strings } = $.store.app

    return (
      <View style={styles.container}>
        <View style={{ marginVertical: 18, paddingHorizontal: 18 }}>
          {
            this.bankOptions.map((item, index) =>
              <View style={{ justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center', marginBottom: 16 }} key={index}>
                <Text style={{ fontSize: TextFont.TextSize(11), color: '#666' }}>{item.type}</Text>
                <Text style={{ fontSize: TextFont.TextSize(11), color: '#333', fontWeight: 'bold' }}>{item.value}</Text>
              </View>
            )
          }
        </View>
        <View style={{ backgroundColor: '#979797', height: 2 }} />
        <View style={{ marginVertical: 18, paddingHorizontal: 18 }}>
          {
            this.tips.map((item, index) =>
              <View key={index} style={{ alignItems: 'center', flexDirection: 'row', marginBottom: 16 }}>
                <View style={{ width: 3, height: 3, borderRadius: 1.5, backgroundColor: '#333', marginRight: 10 }} />
                <Text style={{ fontSize: TextFont.TextSize(11), color: '#333' }}>{item}</Text>
              </View>
            )
          }
        </View>
        <View style={{ alignItems: 'center', justifyContent: 'center', paddingHorizontal: 15, position: 'absolute', bottom: 25, width: '100%' }}>
          <TouchableOpacity onPress={() => Linking.openURL('https://form.jotform.me/90482168535462')} style={styles.dcBtn}>
            <Text style={styles.btnText}>{strings.cash_deposit_submit_slip_button}</Text>
          </TouchableOpacity>
        </View>
      </View>

    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  dcBtn: {
    padding: 10,
    paddingHorizontal: 15,
    borderRadius: 5,
    height: 45,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: DCColor.BGColor('primary-1')
  },
  btnText: {
    fontSize: TextFont.TextSize(15),
    fontWeight: '900',
    color: 'rgba(0, 0, 0, .75)',
    flex: 1,
    textAlign: 'center'
  },
})

