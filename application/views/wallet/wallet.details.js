import React, { Component } from 'react'
import { Text, View, Image, Picker, SafeAreaView, Platform, StyleSheet, Alert, TouchableOpacity, FlatList, RefreshControl } from 'react-native'
import { Screen, TextFont, DCColor } from 'dacsee-utils'
import { Button } from '../../components'
import WalletTransactionListScreen from './wallet.transaction.list'
import IncomeList from '../income/income.list'
import { inject, observer } from 'mobx-react'
import CmpWalletHeader from './compoment/WalletHeader'
import resources from '../../resources/index'

const { width } = Screen.window

const ListItem = ({ label, caption, value, onPress }) => {
  const { listItem, dataCont, fDesc, fPlus, fMinus, processingBar, fCaption } = styles
  return (
    <TouchableOpacity activeOpacity={.6} style={listItem} onPress={onPress}>
      <View style={dataCont}>
        {label &&
          <Text style={fCaption}>{label}</Text>
        }
        {caption &&
          <Text style={[fCaption, fDesc]}>{caption}</Text>
        }
      </View>
      {value &&
        <Text style={[fCaption, fPlus]}>{value}</Text>
      }
      <View style={processingBar} />
    </TouchableOpacity>
  )
}

@inject('app', 'wallet', 'bank')
@observer
export default class WalletDetailScreen extends Component {

  static navigationOptions = ({ navigation }) => {
    const { walletSelected: { name } } = $.store.wallet
    return {
      drawerLockMode: 'locked-closed',
      title: name
    }
  }

  constructor(props) {
    super(props)
    this.state = {
      bankInfo: {
        _id: '',
        user_id: '',
        bankcountry: '',
        bankname: '',
        accountNo: '',
        accountHolderName: '',
        accountHolderIdNo: '',
        accountHolderRelationship: ''
      },
      selectedTab: 'available',
      walletType: '',
      sampleData: [
        {
          id: 'a123456',
          label: 'Platform Fees Reversal',
          time: '08/09/2019, 3:19PM',
          value: 'RM5.50'
        },
        {
          id: 'b123456',
          label: 'Platform Fees',
          time: '08/09/2019, 3:19PM',
          value: 'RM5.50'
        }
      ]
    }
  }

  componentWillUnmount() {
    this.props.wallet.activityState = true
  }

  async componentDidMount() {
    const { type } = this.props.wallet.walletSelected

    if (type) {
      this.setState({ walletType: type.slice(0, 2) })
    }
    await this.props.bank.getBankList()
  }

  onSelect = (selectedTab) => {
    this.setState({ selectedTab })
  }

  render() {
    const { strings } = this.props.app
    const { walletSelected } = this.props.wallet
    const { name, availableAmount, countryCurrency } = walletSelected
    const { mainCont, tabCont, tabItem, activeIndicator, fBody, filterCont, dcSelect, dropdownIcon, fCaption, listCont, dcCol, dcBtn, fBtn } = styles
    const { selectedTab, walletType, sampleData } = this.state

    let countryName = name.split(' ')[0]
    let walletName = name.split(' ')[1]
    return (
      <SafeAreaView style={mainCont}>
        <CmpWalletHeader
          currency={countryCurrency}
          availableAmount={availableAmount}
        />
        <View style={tabCont}>
          <TouchableOpacity style={tabItem} onPress={() => this.onSelect('available')}>
            <Text style={selectedTab === 'available' ? fBody : [fBody, { opacity: .45, fontWeight: '400' }]}>Available</Text>
            {selectedTab === 'available' &&
              <View style={activeIndicator} />
            }
          </TouchableOpacity>
          {walletType && walletType.toUpperCase() !== 'CR' ?
            <TouchableOpacity style={tabItem} onPress={() => this.onSelect('floating')}>
              <Text style={selectedTab === 'floating' ? fBody : [fBody, { opacity: .45, fontWeight: '400' }]}>Floating</Text>
              {selectedTab === 'floating' &&
                <View style={activeIndicator} />
              }
            </TouchableOpacity>
            : <TouchableOpacity style={tabItem} onPress={() => this.onSelect('requested')}>
              <Text style={selectedTab === 'requested' ? fBody : [fBody, { opacity: .45, fontWeight: '400' }]}>Requested</Text>
              {selectedTab === 'requested' &&
                <View style={activeIndicator} />
              }
            </TouchableOpacity>
          }
        </View>
        <View style={filterCont}>
          <View style={dcSelect}>
            <Picker style={[fCaption, { backgroundColor: 'transparent' }]}>
              <Picker.Item label="Item 1" value="a" />
              <Picker.Item label="Item 2" value="b" />
            </Picker>
            <Image source={resources.image.drop_down_icon} style={dropdownIcon} />
          </View>
          <TouchableOpacity style={[dcSelect, { marginLeft: 5 }]}>
            <Picker style={[fCaption, { backgroundColor: 'transparent' }]}>
              <Picker.Item label="Item 1" value="a" />
              <Picker.Item label="Item 2" value="b" />
            </Picker>
            <Image source={resources.image.drop_down_icon} style={dropdownIcon} />
          </TouchableOpacity>
        </View>
        <FlatList
          style={listCont}
          data={sampleData}
          keyExtractor={(item, index) => item._id}
          renderItem={({ item, index }) =>
            <ListItem
              onPress={() => this.props.navigation.navigate('WalletTransactionDetail')}
              key={index}
              label={item.label}
              caption={item.time}
              value={item.value}
            />
          }
          refreshControl={
            <RefreshControl
              refreshing={false}
              onRefresh={() => { }}
              title={this.props.app.strings.pull_refresh}
              colors={['#ffffff']}
              progressBackgroundColor={DCColor.BGColor('primary-1')}
            />
          }
          enableEmptySections
          ListEmptyComponent={() =>
            <ListItem
              label="You dont have any transaction at the moment"
            />
          }
        />
        <View style={dcCol}>
          <TouchableOpacity style={dcBtn} >
            <Text style={fBtn}>{strings.top_up.toUpperCase()}</Text>
          </TouchableOpacity>
        </View>

        {/* <View style={{ width: width, height: 150, justifyContent: 'center', alignItems: 'center', backgroundColor: '#fff' }}>
            <Image style={{ width: 66, height: 66, borderRadius: 33 }}
              source={{ uri: countryFlag }} />
            <Text style={{ fontSize: TextFont.TextSize(13), paddingTop: 10 }}>{countryName}</Text>
            <Text style={{ fontSize: TextFont.TextSize(13), color: '#a5a5a5' }}>{walletName}</Text>
            <Text style={{ fontSize: TextFont.TextSize(13), color: '#a5a5a5' }}>({countryCurrency})</Text>
          </View> */}
        {/* <ScrollTabView onNavigate={(type) => {
          switch (type) {
            case 'transfer':
              if (availableAmount == 0) {
                $.define.func.showMessage(strings.lack_balance)
              }
              else {
                this.props.navigation.navigate('WalletTransfer', { selected_wallet: walletSelected })
              }
              break
            case 'withdraw':
              if (availableAmount == 0) {
                $.define.func.showMessage(strings.lack_balance)
              } else {
                if (this.props.bank.bankList.length === 0) {
                  Alert.alert((strings.bank_verification), (strings.add_bank_tip),
                    [
                      { text: (strings.okay), onPress: () => console.log('pressed ok') },
                      { text: (strings.addbank), onPress: () => this.props.navigation.navigate('AddBankAccount', { countryCode: walletSelected.countryCode, countryName: walletSelected.countryName }) },
                    ])
                } else {
                  this.props.navigation.navigate('WalletWithdrawScreen')
                }
              }
              break
            case 'topUp':
              this.props.navigation.navigate('WalletTopUp')
              break
          }
        }}
          walletInfo={walletSelected} strings={strings}
        /> */}
      </SafeAreaView>
    )
  }
}

// class ScrollTabView extends Component {
//   constructor(props) {
//     super(props)
//     this.state = {
//       currentPage: 0
//     }
//   }

//   _goToPage(pageNum, scrollAnimation = true) {
//     if (this._scrollView && this._scrollView.scrollTo) {
//       this._scrollView.scrollTo({ x: pageNum * width, scrollAnimation })
//       this.setState({
//         currentPage: pageNum,
//       })
//     }
//   }
//   _renderTabView() {
//     let array = ['overView', 'transaction']
//     if (this.props.walletInfo.type == 'DS-US') array.push('floating_comm')
//     return (
//       <View style={{ width: width, height: 50, flexDirection: 'row', backgroundColor: 'white' }} >
//         {
//           array.map((item, index) => {
//             return (
//               <Button key={index}
//                 style={[{ flex: 1, justifyContent: 'center', alignItems: 'center' }, index == this.state.currentPage ? { borderBottomWidth: 3, borderColor: DCColor.BGColor('primary-1') } : {}]}
//                 onPress={() => {
//                   this.setState({
//                     currentPage: index
//                   })
//                   this._goToPage(index)
//                 }}>
//                 <Text style={[{ fontSize: TextFont.TextSize(15) }, index == this.state.currentPage ? { color: DCColor.BGColor('primary-1') } : { color: '#a5a5a5', opacity: 0.7 }]}>
//                   {this.props.strings[item]}
//                 </Text>
//               </Button>
//             )
//           })
//         }
//       </View>
//     )
//   }

//   _renderScrollContent() {
//     let array = [<OverView key={1} onNavigate={this.props.onNavigate} walletInfo={this.props.walletInfo} strings={this.props.strings} />,
//     <WalletTransactionListScreen key={2} walletInfo={this.props.walletInfo} />]
//     if (this.props.walletInfo.type == 'DS-US') array.push(<IncomeList key={3} walletInfo={this.props.walletInfo} />)
//     return (
//       <ScrollView
//         ref={(ref) => {
//           this._scrollView = ref
//         }}
//         style={{ flex: 1 }}
//         scrollEnabled={false}
//         pagingEnabled={true}
//         horizontal={true}
//         showsHorizontalScrollIndicator={false}

//       >
//         {
//           array.map((item, index) => {
//             return (
//               item
//             )
//           })
//         }
//       </ScrollView>
//     )
//   }

//   render() {
//     return (
//       <View style={{ flex: 1 }}>
//         <View style={{ height: 1, backgroundColor: '#D8D8D8', opacity: 0.5 }}></View>
//         {this._renderTabView()}
//         {this._renderScrollContent()}
//       </View>
//     )
//   }
// }

// OverView
// class OverView extends Component {


//   successNotify = data => {
//     if (Platform.OS === 'ios') {
//       const {
//         transactionId,
//         referenceNo,
//         amount,
//         remark,
//         authorizationCode
//       } = data

//       alert('Message', `Payment authcode is ${authorizationCode}`, {
//         cancelable: true
//       })
//     } else {

//     }
//   };

//   cancelNotify = data => {
//     const { transactionId, referenceNo, amount, remark, error } = data

//     if (Platform.OS === 'ios') {
//       if (__DEV__) {
//         console.log('MessageIos', `${error}`, { cancelable: false })
//       }

//     } else {
//       if (__DEV__) {
//         console.log('MessageAndroid', `${error}`)
//       }
//     }
//   };

//   failedNotify = data => {
//     const { transactionId, referenceNo, amount, remark, error } = data

//     if (Platform.OS === 'ios') {
//       if (__DEV__) {
//         console.log('MessageIos', `${error}`, { cancelable: false })
//       }
//     } else {
//       if (__DEV__) {
//         console.log('MessageAndroid', `${error}`, `${referenceNo}`)
//       }
//     }
//   }


//   pay = () => {
//     try {
//       const data = {}
//       data.paymentId = '6' // refer to ipay88 docs
//       data.merchantKey = 'apple88KEY'
//       data.merchantCode = 'M09999'
//       data.referenceNo = 'BPYF0000011'
//       data.amount = '1.00'
//       data.currency = 'MYR'
//       data.productDescription = 'Payment'
//       data.userName = 'test'
//       data.userEmail = 'test@gmail.com'
//       data.userContact = '0123456789'
//       data.remark = 'me'
//       data.utfLang = 'UTF-8'
//       data.country = 'MY'
//       data.backendUrl = 'http://www.baidu.com'
//       const errs = Pay(data)
//       if (Object.keys(errs).length > 0) {
//         console.log(errs)
//       }
//     } catch (e) {
//       console.log(e)
//     }
//   }

//   _renderItem(obj) {
//     return (
//       <View key={obj.name} style={{ marginHorizontal: 20, height: 44, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
//         <Text style={{ fontSize: TextFont.TextSize(13), color: '#a5a5a5', }}>
//           {this.props.strings[obj.name]}
//           {/*<FormattedMessage id={obj.name} value={TextFont.TextSize(18)} />*/}
//         </Text>
//         <Text style={{ fontSize: TextFont.TextSize(18) }}>{obj.value.toFixed(2)}</Text>
//       </View>
//     )
//   }

//   render() {
//     const { availableAmount, allowWithdrawal, floatingDriverAmount, floatingPassengerAmount, allowDeposit, allowTransfer } = this.props.walletInfo
//     const { dcButton, cYellow } = styles
//     return (
//       <View style={{ flex: 1, width: width, marginTop: 20 }}>
//         {
//           this._renderItem({ name: 'available_balance', value: availableAmount })
//         }
//         {/*  {
//           this._renderItem({name: '可用余额（乘客）', value: floatingPassengerAmount})
//         }
//         {
//           this._renderItem({name: '可用余额（司机）', value: floatingDriverAmount})
//         } */}
//         <View style={{ paddingTop: 30, flex: 1, alignItems: 'center' }}>
//           {allowDeposit && allowTransfer ?
//             <Button onPress={() => { this.props.onNavigate('transfer') }} style={dcButton}>
//               {/*<Button onPress={()=>{this.pay()}} style={{ width: 240, height: 44, backgroundColor: '#4cb1f7', borderRadius: 4 }}>*/}
//               {/*<IPay88*/}
//               {/*successNotify={this.successNotify}*/}
//               {/*failedNotify={this.failedNotify}*/}
//               {/*cancelNotify={this.cancelNotify}*/}
//               {/*/>*/}
//               <Text style={{ fontSize: TextFont.TextSize(15), fontWeight: 'bold', color: DCColor.BGColor('primary-2') }}>
//                 {this.props.strings.transfer.toUpperCase()}
//               </Text>
//             </Button> : null
//           }

//           {
//             !!(allowDeposit && allowTransfer) ?
//               <Button onPress={() => { this.props.onNavigate('topUp') }} style={dcButton}>
//                 <Text style={{ fontSize: TextFont.TextSize(15), fontWeight: 'bold', color: DCColor.BGColor('primary-2') }}>{this.props.strings.top_up.toUpperCase()}</Text>
//               </Button> : null
//           }
//           {
//             (!!allowWithdrawal) ?
//               <Button onPress={() => { this.props.onNavigate('withdraw') }} style={[dcButton, cYellow]}>
//                 <Text style={{ fontSize: TextFont.TextSize(15), fontWeight: 'bold', color: DCColor.BGColor('primary-2') }}>{this.props.strings.withdraw.toUpperCase()}</Text>
//               </Button> : null

//           }



//         </View>
//       </View>
//     )
//   }
// }

const styles = StyleSheet.create({
  dcButton: {
    width: 240,
    height: 44,
    backgroundColor: DCColor.BGColor('primary-1'),
    borderRadius: 4,
    marginBottom: 18
  },
  cYellow: {
    backgroundColor: DCColor.BGColor('primary-1')
  },
  mainCont: {
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    width,
    height: '100%',
    backgroundColor: 'white'
  },
  tabCont: {
    width,
    backgroundColor: DCColor.BGColor('primary-2'),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  tabItem: {
    position: 'relative',
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    paddingVertical: 15
  },
  activeIndicator: {
    width: '100%',
    height: 5,
    backgroundColor: DCColor.BGColor('primary-1'),
    position: 'absolute',
    bottom: 0
  },
  fBody: {
    fontSize: TextFont.TextSize(15),
    fontWeight: '600',
    color: 'white',
    textAlign: 'center'
  },
  filterCont: {
    width,
    padding: 5,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: DCColor.BGColor('disable')
  },
  dcSelect: {
    flex: 1,
    borderRadius: 4,
    backgroundColor: 'rgba(0, 0, 0, 0.05)',
    height: 45,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: 10,
    paddingHorizontal: 15
  },
  dropdownIcon: {
    width: 8,
    height: 8,
    resizeMode: 'contain'
  },
  fCaption: {
    fontSize: TextFont.TextSize(12),
    fontWeight: "600",
    color: 'black',
    flex: 1,
    textAlign: 'left'
  },
  listCont: {
    flex: 1,
    width
  },
  listItem: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: 15,
    paddingHorizontal: 20,
    position: 'relative',
    borderBottomWidth: 1,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  dataCont: {
    flex: 1
  },
  fDesc: {
    opacity: .4,
    fontWeight: '400'
  },
  fPlus: {
    color: DCColor.BGColor('green'),
    flex: 0
  },
  fMinus: {
    color: DCColor.BGColor('red'),
    flex: 0
  },
  processingBar: {
    height: 1000,
    width: 5,
    backgroundColor: DCColor.BGColor('primary-1'),
    position: 'absolute',
    left: 0,
    top: 0
  },
  dcCol: {
    width,
    padding: 15,
    backgroundColor: 'white',
    borderTopWidth: 1,
    borderColor: 'rgba(0, 0, 0, 0.1)'
  },
  dcBtn: {
    borderRadius: 4,
    backgroundColor: DCColor.BGColor('primary-1'),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height: 55,
    width: '100%'
  },
  fBtn: {
    fontSize: TextFont.TextSize(15),
    fontWeight: '900',
    color: DCColor.BGColor('primary-2')
  }
})