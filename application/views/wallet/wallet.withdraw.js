import React, { Component } from 'react'
import { Text, View, ScrollView, StyleSheet, Platform, Alert, AlertIOS } from 'react-native'
import { inject, observer } from 'mobx-react'
import { TextFont, DCColor } from 'dacsee-utils'
import Input from '../../components/input'
import { Button } from '../../components'
import PinUpdateModal from './compoment/pin.update.modal'

const styles = StyleSheet.create({
  formCont: {
    flex: 1,
    padding: 20
  },
  itemCont: {
    width: '100%',
    marginBottom: 10
  },
  fLabel: {
    fontSize: TextFont.TextSize(12),
    color: 'rgba(0, 0, 0, 0.5)',
    marginBottom: 5
  },
  amountCont: {
    width: '100%',
    height: 40,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    borderBottomWidth: 1,
    borderColor: 'rgba(0, 0, 0, 0.75)'
  },
  fData: {
    fontSize: TextFont.TextSize(14)
  },
  actionCont: {
    flex: 1,
    paddingTop: 30,
    flexDirection: 'row',
    justifyContent: 'center'
  },
  dcBtn: {
    width: 240,
    height: 44,
    borderRadius: 4,
    backgroundColor: DCColor.BGColor('primary-1'),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  btnText: {
    fontSize: TextFont.TextSize(15),
    fontWeight: 'bold',
    color: DCColor.BGColor('primary-2')
  },
  availableAmountCont: {
    width: '100%',
    marginBottom: 15,
    marginTop: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start'
  },
  fAmount: {
    color: 'black',
    marginLeft: 15
  }
})


@inject('app', 'wallet', 'bank', 'account')
@observer
export default class WalletWithdrawScreen extends Component {

  static navigationOptions = ({ navigation }) => {
    const { strings } = $.store.app
    return {
      drawerLockMode: 'locked-closed',
      title: strings.withdraw
    }
  }

  constructor(props) {
    super(props)
    this.state = {
      bank_id: this.props.bank.bankList[this.props.bank.bankList.length - 1]._id,
      walletType: this.props.wallet.walletSelected.type,
      amount: '',
      yourRef: '',
      showPinModal: false,
      newPin: '',
      confirmPin: ''
    }
  }

  async componentDidMount() {
    await this.props.account.getProfile()
    await this.props.bank.getBankList()
  }

  onPinHide = () => {
    this.setState({
      showPinModal: false
    })
  }

  onPinSubmit = async () => {
    if (this.state.newPin.length < 6) {
      $.define.func.showMessage($.store.app.strings.pin_error_1)
    } else if (this.state.newPin !== this.state.confirmPin) {
      $.define.func.showMessage($.store.app.strings.pin_error_2)
    } else {
      const isSuccess = await this.props.bank.setNewPin(this.state.newPin)
      if (isSuccess) {
        this.setState({ showPinModal: false }, () => {
          setTimeout(() => {
            this.props.navigation.navigate('WalletVerification', { data: this.state })
          }, 500);
        })
      }

    }
  }

  onPinChange = (type, text) => {
    if (type === 'newPin') {
      this.setState({
        newPin: text
      })
    } else {
      this.setState({
        confirmPin: text
      })
    }
  }

  onEnterAmount(value) {
    const pattern = /^(?![0])[^.]*\d+$|^.{0}$/g
    if (pattern.test(value)) {
      this.setState({ amount: value })
    }
  }

  onContinue = () => {
    const { amount } = this.state
    const { bankList = [] } = this.props.bank
    const icRegistered = bankList[bankList.length - 1].accountHolderIdNo
    const { strings } = $.store.app
    const bankDetail = bankList.length > 0
      ? bankList[bankList.length - 1]
      : null

    if (!icRegistered) {
      if (Platform.OS === 'ios') {
        AlertIOS.alert(
          (strings.withdraw_error_5), (strings.withdraw_error_5_desc),
          [
            {
              text: strings.cancel,
              style: 'cancel'
            },
            {
              text: strings.set_up_now,
              onPress: () => this.props.navigation.navigate('AddBankAccount', {
                title: strings.bank_detail,
                countryCode: bankDetail.countryCode,
                mode: 'edit',
                bankDetail
              }),
            }
          ]
        )
      } else {
        Alert.alert(
          (strings.withdraw_error_5), (strings.withdraw_error_5_desc),
          [
            { text: (strings.cancel) },
            {
              text: (strings.set_up_now),
              onPress: () => this.props.navigation.navigate('AddBankAccount', {
                title: strings.bank_detail,
                countryCode: bankDetail.countryCode,
                mode: 'edit',
                bankDetail
              })
            },
          ]
        )
      }
    } else if (amount === 0) {
      $.define.func.showMessage(strings.withdraw_error_1)
    } else if (Number.isNaN(Number(amount)) || !Number.isInteger(Number(amount))) {
      $.define.func.showMessage(strings.withdraw_error_4)
    } else if (amount < 5) {
      $.define.func.showMessage(strings.withdraw_error_3)
    } else if (amount > this.props.wallet.walletSelected.availableAmount) {
      $.define.func.showMessage(strings.withdraw_error_2)
    } else if (!$.store.account.user.hasPin) {
      this.setState({
        showPinModal: true
      })
    } else {
      this.props.navigation.navigate('WalletVerification', { data: this.state })
    }
  }

  render() {
    const { strings } = this.props.app
    const { formCont, itemCont, fLabel, amountCont, fData, actionCont, dcBtn, btnText, fAmount, availableAmountCont } = styles
    const { availableAmount, countryCurrency } = this.props.wallet.walletSelected
    const { bankList } = this.props.bank
    const { showPinModal, amount } = this.state

    return (
      <View style={{ flex: 1 }}>

        <PinUpdateModal
          visible={showPinModal}
          onHide={() => this.onPinHide()}
          onChange={(type, text) => this.onPinChange(type, text)}
          onSubmit={() => this.onPinSubmit()}
        />
        <ScrollView
          style={formCont}
          horizontal={false}
        // keyboardDismissMode={ 'on-drag' }
        >
          <View style={{ flex: 1 }}>
            <View style={itemCont}>
              <Text style={fLabel}>{strings.bank_name}</Text>
              <View style={amountCont}>
                {/* <Picker 
                    itemStyle={[fData, { textAlign: 'left' }]} 
                    style={{ flex: 1 }} keyboardType={'numeric'} 
                    onChangeText={(value) => this.setState({ amount: value })}>
                  <Picker.Item label='maybank' value="maybank" />
                </Picker> */}
                <Text style={fData}>{bankList[bankList.length - 1].name}</Text>
              </View>
            </View>

            <View style={availableAmountCont}>
              <Text style={fLabel}>{strings.available_amount}</Text>
              <Text style={[fLabel, fAmount]}>{countryCurrency} {availableAmount.toFixed(2)}</Text>
            </View>

            <View style={itemCont}>
              <Text style={fLabel}>{strings.withdrawal_amount}</Text>
              <View style={amountCont}>
                <Text style={[fData, { marginRight: 10 }]}>RM</Text>
                <Input
                  style={[fData, { flex: 1 }]} placeholder={'0'}
                  keyboardType={'numeric'}
                  onChangeText={(text) => this.onEnterAmount(text)}
                  value={amount}
                />
              </View>
            </View>

            <View style={itemCont}>
              <Text style={fLabel}>{strings.remarks}</Text>
              <View style={[amountCont, { height: 80 }]}>
                <Input
                  multiline={true}
                  numberOfLines={4}
                  style={[fData, { flex: 1, height: 80 }]}
                  placeholder={strings.pls_enter_remarks}
                  onChangeText={(value) => this.setState({ yourRef: value })} />
              </View>
            </View>

          </View>
          <View style={actionCont}>
            <Button
              onPress={() => this.onContinue()}
              style={dcBtn}
            >
              <Text style={btnText}>
                {strings.continue_.toUpperCase()}
              </Text>
            </Button>
          </View>
        </ScrollView>
      </View>
    )
  }
}