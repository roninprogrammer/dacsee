import React, { Component } from 'react'
import { Text, View, Image, ScrollView } from 'react-native'
import { DCColor, TextFont, Avatars } from 'dacsee-utils'
import { Button } from '../../components'
import { inject, observer } from 'mobx-react'

@inject('app', 'wallet')
@observer
export default class WalletTransferSummaryScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const { strings } = $.store.app
    return {
      drawerLockMode: 'locked-closed',
      title: strings.transfer_confirm
    }
  }

  constructor(props) {
    super(props)
    this.state = {
      transferInfo: this.props.navigation.state.params.transferInfo,
    }
  }

  _submitTransfer() {
    const { transfer, walletSelected } = this.props.wallet
    const { amount, remark, userList } = this.state.transferInfo
    const user = userList[0]
    const body = {
      from: {
        walletType: walletSelected.type
      },
      to: {
        walletType: walletSelected.type,
        user_id: user._id
      },
      amount: amount,
      remarks: remark
    }
    transfer(body)
  }

  render() {
    const { strings } = this.props.app
    const { walletSelected, transferring } = this.props.wallet
    const { amount, userList, remark } = this.state.transferInfo
    const user = userList[0]
    const { avatars = [] } = user
    return (
      <ScrollView style={{ flex: 1, backgroundColor: 'white' }} horizontal={false} >
        <View style={{ padding: 20 }}>
          <View style={{ borderBottomWidth: 1, borderBottomColor: '#a5a5a5' }}>
            <Text style={{ fontSize: TextFont.TextSize(12), opacity: 0.5 }}>
              {strings.transfer_wallet}
            </Text>
            <View style={{ flexDirection: 'row', alignItems: 'center', height: 40 }}>
              <Text style={{ fontSize: TextFont.TextSize(14) }}>{walletSelected.name}</Text>
            </View>
          </View>

          <View style={{ paddingTop: 20, borderBottomWidth: 1, borderBottomColor: '#a5a5a5' }}>
            <Text style={{ fontSize: TextFont.TextSize(12), opacity: 0.5 }}>
              {strings.sending_amount}
            </Text>
            <View style={{ height: 40, justifyContent: 'center' }}>
              <Text >{amount}</Text>
            </View>
          </View>

          <View style={{ paddingTop: 20, borderBottomWidth: 1, borderBottomColor: '#a5a5a5' }}>
            <Text style={{ fontSize: TextFont.TextSize(12), opacity: 0.5 }}>
              {strings.recipient_account}
            </Text>
            <View style={{ flexDirection: 'row' }}>
              <Image source={{ uri: Avatars.getHeaderPicUrl(avatars) }} style={{ marginVertical: 15, marginRight: 15, width: 66, height: 66, borderRadius: 33 }} />
              <View style={{ justifyContent: 'center' }}>
                <Text style={{ fontSize: TextFont.TextSize(11), opacity: 0.6 }}>{user.userId}</Text>
                <Text style={{ fontSize: TextFont.TextSize(17) }}>{user.fullName}</Text>
              </View>
            </View>
          </View>

          <View style={{ paddingTop: 20, borderBottomWidth: 1, borderBottomColor: '#a5a5a5' }}>
            <Text style={{ fontSize: TextFont.TextSize(12), opacity: 0.5 }}>
              {strings.remarks}
            </Text>
            <Text style={{ marginVertical: 10, fontSize: TextFont.TextSize(14) }}>{remark}</Text>
          </View>

          <View style={{ paddingTop: 30, flex: 1, flexDirection: 'row', justifyContent: 'center' }}>
            <Button disabled={transferring} onPress={() => this._submitTransfer()} style={[{ width: 240, height: 44, borderRadius: 4 }, transferring ? { backgroundColor: '#a5a5a5' } : { backgroundColor: DCColor.BGColor('primary-1') }]}>
              {
                transferring ?
                  <Text style={{ fontSize: TextFont.TextSize(15), color: DCColor.BGColor('disable') }}>
                    {strings.transfering}
                  </Text> :
                  <Text style={{ fontSize: TextFont.TextSize(15), fontWeight: 'bold', color: DCColor.BGColor('primary-2') }}>
                    {strings.confirm_transfer.toUpperCase()}
                  </Text>

              }
            </Button>
          </View>
        </View>
      </ScrollView>
    )
  }
}
