
import React, { Component } from 'react'
import { Text, View, Image, TouchableOpacity, ListView, RefreshControl, SafeAreaView, StyleSheet } from 'react-native'
import InteractionManager from 'InteractionManager'
import imgs from '../../resources'
import { DCColor, Screen, TextFont } from 'dacsee-utils'
import { inject, observer } from 'mobx-react'
import Resources from 'dacsee-resources'

const { width } = Screen.window
const styles = StyleSheet.create({
  notiCont: {
    width,
    padding: 5,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ededed'
  },
  mainCont: {
    flexDirection: 'column',
    alignContent: 'flex-start',
    justifyContent: 'flex-start',
    flex: 1
  },
  listCont: {
    width,
    flex: 1,
    padding: 10,
    paddingTop: 0,
    flexDirection: 'column',
    alignContent: 'flex-start',
    justifyContent: 'flex-start',
  },
  listItem: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    borderBottomWidth: 1,
    borderColor: 'rgba(0, 0, 0, 0.1)',
    padding: 15
  },
  walletImg: {
    width: 58,
    height: 58,
    borderRadius: 29,
    marginRight: 16,
    resizeMode: 'cover'
  },
  walletDetailCont: {
    flex: 1,
    marginRight: 10,
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'center'
  },
  fTitle: {
    fontSize: TextFont.TextSize(13),
    fontWeight: '600',
    color: 'rgba(0, 0, 0, 0.75)',
  },
  fCaption: {
    fontSize: TextFont.TextSize(11),
    fontWeight: '400',
    color: 'rgba(0, 0, 0, 0.45)',
  },
  fDisplay: {
    fontSize: TextFont.TextSize(27),
    fontWeight: '600',
    color: 'rgba(0, 0, 0, 0.75)',
  },
  balanceCont: {
    flexDirection: 'column',
    alignItems: 'flex-end',
    justifyContent: 'center'
  },
  nextIcon: {
    width: 6,
    height: 6,
    resizeMode: 'contain',
    marginLeft: 10
  }
})

const dataContrast = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })

@inject('app', 'wallet')
@observer
export default class WalletBalanceScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const { strings } = $.store.app
    return {
      drawerLockMode: 'locked-closed',
      title: strings.wallet_lis,
    }
  }

  constructor(props) {
    super(props)
  }

  componentDidMount() {
    InteractionManager.runAfterInteractions()
    this.props.wallet.getWalletList()
  }

  _goDetail = (row, index) => {
    const { wallet, navigation } = this.props
    const idx = parseInt(index)
    
    wallet.updateWalletIndex(idx)
    navigation.navigate('WalletDetail')
  }

  render() {
    const { notiCont, mainCont, listCont } = styles
    const { strings } = this.props.app
    const { walletList, getWalletList, walletListLoading } = this.props.wallet
    return (
      <SafeAreaView style={{ backgroundColor: 'white', flex: 1 }}>
        <View style={mainCont}>
          <View style={notiCont}>
            {/* <NotificationBar
              icon={Resources.image.attention_icon}
              label={strings.put_message}
            /> */}
          </View>
          <View style={listCont}>
            <ListView
              removeClippedSubviews={false}
              style={{ flex: 1 }}
              refreshControl={
                <RefreshControl
                  refreshing={walletListLoading}
                  onRefresh={getWalletList}
                  title={strings.pull_refresh}
                  colors={['#ffffff']}
                  progressBackgroundColor={DCColor.BGColor('primary-1')}
                />
              }
              dataSource={dataContrast.cloneWithRows(walletList.slice())}
              enableEmptySections={true}
              renderRow={(row, sectionid, rowid) =>
                <ListItem data={row} strings={strings} index={rowid} itemPress={this._goDetail} />
              }
            />
          </View>
        </View>
      </SafeAreaView>
    )
  }
}

class ListItem extends Component {
  constructor(props) {
    super(props)
    this.state = {
      open: false,
    }
  }


  render() {
    const { listItem, walletImg, walletDetailCont, fTitle, fCaption, fDisplay, balanceCont, nextCont, nextIcon } = styles
    const { data, strings, index, itemPress } = this.props
    const { name, countryName, countryFlag, availableAmount } = this.props.data
    return (
      <TouchableOpacity style={listItem} onPress={() => itemPress(data, index)} activeOpacity={1}>
        <Image style={walletImg} source={{ uri: countryFlag }} />
        <View style={walletDetailCont}>
          <Text style={fTitle}>{countryName}</Text>
          <Text style={fCaption}>{name}</Text>
        </View>
        <View style={balanceCont}>
          <Text style={fCaption}>{strings.available_balance}</Text>
          <Text style={fDisplay}>{availableAmount.toFixed(2)}</Text>
        </View>
        <Image style={nextIcon} source={Resources.image.right_icon} />
      </TouchableOpacity>
    )
  }
}

class FloatingAmounView extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    const { floatingPassengerAmount, floatingDriverAmount } = this.props.amount
    return (
      <View style={{ backgroundColor: '#fff' }}>
        <Image resizeMode={'cover'}
          style={{ width, height: width / 1.8 }}
          source={imgs.image.wallet_bg} />
        <View style={{ position: 'absolute', right: 30, left: 30, top: 70 }}>
          {
            floatingPassengerAmount === undefined ?
              null :
              <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginBottom: 12 }}>
                <Text style={{ fontSize: TextFont.TextSize(13), color: '#000033' }}>{this.props.strings.float_balance_passenger}</Text>
                <Text style={{ fontSize: TextFont.TextSize(13) }}>{floatingPassengerAmount.toFixed(2)}</Text>
              </View>
          }
          {
            floatingDriverAmount === undefined ?
              null :
              <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                <Text style={{ fontSize: TextFont.TextSize(13), color: '#000033' }}>{this.props.strings.float_balance_driver}</Text>
                <Text style={{ fontSize: TextFont.TextSize(13) }}>{floatingDriverAmount.toFixed(2)}</Text>
              </View>
          }
        </View>
      </View>
    )
  }
}
