import React, { Component } from 'react'
import { KeyboardAvoidingView, Text, View, TextInput, StyleSheet, ScrollView, TouchableOpacity } from 'react-native'
import { TextFont, Screen, DCColor } from 'dacsee-utils'
import { inject, observer } from 'mobx-react'

const { width } = Screen.window

const styles = StyleSheet.create({
    formCont: {
        width,
        flex: 1,
    },
    mainCont: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'flex-start',
    },
    fLabel: {
        fontSize: TextFont.TextSize(12),
        color: 'rgba(0, 0, 0, 0.5)',
        marginBottom: 5,
        flex: 1,
    },
    errorCont: {
        fontSize: TextFont.TextSize(12),
        color: 'red',
        marginTop: 10,
        flex: 1,
    },
    fData: {
        color: 'black',
        textAlign: 'right'
    },
    itemCont: {
        flex: 1,
        padding: 20
    },
    codeCont: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    dcInput: {
        flex: 1,
        borderBottomWidth: 1,
        borderColor: 'rgba(0, 0, 0, 0.75)',
        padding: 15,
        marginRight: 10
    },
    codeInput: {
        color: 'rgba(0, 0, 0, 0.75)',
        fontWeight: '600',
        flexDirection: 'row',
        textAlign: 'center',
        fontSize: 16,
        height: 44,
        flex: 1,
        borderBottomWidth: 1,
        borderColor: 'rgba(0, 0, 0, 0.75)',
        marginRight: 10
    },
    actionCont: {
        flex: 1,
        paddingTop: 30,
        flexDirection: 'row',
        justifyContent: 'center',
        alignSelf: 'center'
    },
    dcBtn: {
        width: 240,
        height: 44,
        borderRadius: 4,
        backgroundColor: DCColor.BGColor('primary-1'),
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    disableBtn: {
        width: 240,
        height: 44,
        borderRadius: 4,
        backgroundColor: DCColor.BGColor('disable'),
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    disableBtnText: {
        fontSize: TextFont.TextSize(15),
        color: 'grey'
    },
    btnText: {
        fontSize: TextFont.TextSize(15),
        fontWeight: 'bold',
        color: DCColor.BGColor('primary-2')
    },
    dcCol: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        marginBottom: 10,
    },
    ConCont: {
        width: '100%',
        borderTopWidth: 1,
        borderColor: 'rgba(0, 0, 0, 0.1)',
        padding: 20
    },
    detailCont: {
        width: '100%',
        padding: 20
    },
    dcForm: {
        flexDirection: 'column',
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        width,
    }
})

const codeInputProps = {
    maxLength: 1,
    returnKeyType: 'next',
    placeholderTextColor: '#eee',
    keyboardType: 'numeric',
    clearTextOnFocus: false,
    autoCapitalize: 'none',
    selectTextOnFocus: false,
    autoCorrect: false,
    underlineColorAndroid: 'transparent',
    borderBottomWidth: 1,
    borderColor: 'rgba(0, 0, 0, 0.75)',
    secureTextEntry: true
}
@inject('app', 'bank')
@observer
export default class WalletVerification extends Component {
    static navigationOptions = ({ navigation }) => {
        const { strings } = $.store.app
        return {
            drawerLockMode: 'locked-closed',
            title: strings.withdraw
        }
    }

    constructor(props) {
        super(props)
        this.state = {
            v1: '',
            v2: '',
            v3: '',
            v4: '',
            v5: '',
            v6: '',
        }
        this.codeInput = {}
    }

    componentDidMount = async () => {
        await this.props.bank.getBankDetail(this.props.navigation.state.params.data.bank_id)
    }

    onSubmit = async () => {
        const { v1, v2, v3, v4, v5, v6 } = this.state
        if (v1.length > 0 && v2.length > 0 && v3.length > 0 && v4.length > 0 && v5.length > 0 && v6.length > 0) {
            const { bank_id, walletType, amount, yourRef } = this.props.navigation.state.params.data
            const form = {
                pin: `${v1}${v2}${v3}${v4}${v5}${v6}`,
                bank_id,
                walletType,
                amount,
                yourRef,
            }
            await this.props.bank.createWithdrawal(form)
        }
        else {
            $.define.func.showMessage(this.props.app.strings.pin_error_3)
        }
    }

    render() {
        const { strings } = this.props.app
        const { name, accountNo, accountHolderIdNo, accountHolderName } = this.props.bank.bankDetail
        const { amount } = this.props.navigation.state.params.data
        const {
            formCont,
            mainCont,
            fLabel,
            errorCont,
            itemCont,
            codeCont,
            codeInput,
            actionCont,
            dcBtn,
            disableBtn,
            btnText,
            dcCol,
            ConCont,
            detailCont,
            fData,
            disableBtnText,
            dcForm
        } = styles

        return (
            <View style={{ flex: 1 }}>
                <ScrollView
                    style={formCont}
                    horizontal={false}
                // keyboardDismissMode={ 'on-drag' }
                >
                    <KeyboardAvoidingView behavior="position" contentContainerStyle={dcForm}>
                        <View style={mainCont} >
                            <View style={detailCont}>
                                <View style={dcCol}>
                                    <Text style={fLabel}>{strings.bank_name}</Text>
                                    <Text style={[fLabel, fData]}>{name ? name : '-'}</Text>
                                </View>
                                <View style={dcCol}>
                                    <Text style={fLabel}>{strings.withdrawal_account_no}</Text>
                                    <Text style={[fLabel, fData]}>{accountNo ? accountNo : '-'}</Text>
                                </View>
                                <View style={dcCol}>
                                    <Text style={fLabel}>{strings.withdrawal_account_name}</Text>
                                    <Text style={[fLabel, fData]}>{accountHolderName ? accountHolderName : '-'}</Text>
                                </View>
                                <View style={dcCol}>
                                    <Text style={fLabel}>{strings.withdrawal_account_id}</Text>
                                    <Text style={[fLabel, fData]}>{accountHolderIdNo ? accountHolderIdNo : '-'}</Text>
                                </View>
                                <View style={[dcCol, { marginBottom: 0 }]}>
                                    <Text style={fLabel}>{strings.withdrawal_amount}</Text>
                                    <Text style={[fLabel, fData]}>{amount}</Text>
                                </View>
                            </View>
                            <View style={ConCont}>
                                <View style={dcCol}>
                                    <Text style={fLabel}>{strings.withdrawal_tip1}</Text>
                                </View>
                                <View style={dcCol}>
                                    <Text style={fLabel}>{strings.withdrawal_tip2}</Text>
                                </View>
                            </View>
                            <View style={itemCont}>
                                <Text style={fLabel}>{strings.pin_code}</Text>
                                <View style={codeCont}>
                                    <TextInput ref={e => this.codeInput.v1 = e} style={codeInput} {...codeInputProps}
                                        onChangeText={text => {
                                            this.setState({ v1: text })
                                            if (text.length === 1) return this.codeInput.v2.focus()
                                        }}
                                    />
                                    <TextInput ref={e => this.codeInput.v2 = e} style={codeInput} {...codeInputProps}
                                        onChangeText={text => {
                                            this.setState({ v2: text })
                                            if (text.length === 0) return this.codeInput.v1.focus()
                                            this.codeInput.v3.focus()
                                        }}
                                    />
                                    <TextInput ref={e => this.codeInput.v3 = e} style={codeInput} {...codeInputProps}
                                        onChangeText={text => {
                                            this.setState({ v3: text })
                                            if (text.length === 0) return this.codeInput.v2.focus()
                                            this.codeInput.v4.focus()
                                        }}
                                    />
                                    <TextInput ref={e => this.codeInput.v4 = e} style={codeInput} {...codeInputProps}
                                        onChangeText={text => {
                                            this.setState({ v4: text })
                                            if (text.length === 0) return this.codeInput.v3.focus()
                                            this.codeInput.v5.focus()
                                        }}
                                    />
                                    <TextInput ref={e => this.codeInput.v5 = e} style={codeInput} {...codeInputProps}
                                        onChangeText={text => {
                                            this.setState({ v5: text })
                                            if (text.length === 0) return this.codeInput.v4.focus()
                                            this.codeInput.v6.focus()
                                        }}
                                    />
                                    <TextInput ref={e => this.codeInput.v6 = e} style={codeInput} {...codeInputProps}
                                        onChangeText={text => {
                                            this.setState({ v6: text })
                                            if (text.length === 0) return this.codeInput.v5.focus()
                                        }}
                                    />
                                </View>
                                {this.props.bank.errormessage !== '' &&
                                    <Text style={errorCont}>{this.props.bank.errormessage}</Text>
                                }
                                {this.props.bank.errormessage == '' &&
                                    <Text style={errorCont}></Text>
                                }
                            </View>
                        </View>

                        <View style={actionCont}>
                            {this.props.bank.errorcode !== 'USER_PIN_INACTIVE' &&
                                <TouchableOpacity
                                    onPress={() => this.onSubmit()}
                                    style={dcBtn}
                                >
                                    <Text style={btnText}>
                                        {strings.confirm.toUpperCase()}
                                    </Text>
                                </TouchableOpacity>
                            }
                            {this.props.bank.errorcode === 'USER_PIN_INACTIVE' &&
                                <View style={disableBtn}>
                                    <Text style={disableBtnText}>
                                        {strings.confirm.toUpperCase()}
                                    </Text>
                                </View>
                            }
                        </View>
                    </KeyboardAvoidingView>
                </ScrollView>
            </View>
        )
    }
}
