import React, { PureComponent } from 'react'
import { Text, View, Image, ScrollView, SafeAreaView, StyleSheet, TouchableOpacity } from 'react-native'
import { Screen, TextFont, DCColor } from 'dacsee-utils'
import moment from 'moment'

const { width } = Screen.window

export default class CmpWalletHeader extends PureComponent {

  constructor(props) {
    super(props)
    this.state = {
      expired: false
    }
  }

  componentDidMount() {
    const { expiryDate } = this.props

    expiryDate && !moment().isBefore(expiryDate) && this.setState({ expired: true })
  }

  render() {
    const { currency, availableAmount = 0, floatingAmount = 0, expiryDate, exchangeRate, estValue } = this.props
    const { mainCont, disabledWallet, balanceCont, fBody, fDisplay, dcCol, dcCard, fCaption, exchangeRateFont } = styles
    const { expired } = this.state
    return (
      <View style={expiryDate && expired ? [mainCont, disabledWallet] : mainCont}>
        <View style={exchangeRate && estValue ? balanceCont : [balanceCont, {marginBottom: 15}]}>
          <Text style={[fBody, { paddingBottom: 8, marginRight: 5 }]}>{currency}</Text>
          <Text style={fDisplay}>{(Number(availableAmount) + Number(floatingAmount)).toFixed(exchangeRate ? 3 : 2)}</Text>
        </View>
        {exchangeRate && estValue &&
          <Text style={exchangeRateFont}>{`~${currency} ${Number(estValue).toFixed(2)}(${exchangeRate})`}</Text>
        }
        <View style={dcCol}>
          {expiryDate &&
            <View style={dcCard}>
              <Text style={fCaption}>Expiry Date</Text>
              {expired ?
                <Text style={[fBody, { color: DCColor.BGColor('red') }]}>Expired</Text>
                : <Text style={fBody}>{moment(expiryDate).format('DD / MM / YYYY')}</Text>
              }
            </View>
          }
          {!expiryDate &&
            <View style={dcCard}>
              <Text style={fCaption}>Available Balance</Text>
              <Text style={fBody}>{currency} {Number(availableAmount).toFixed(exchangeRate ? 3 : 2)}</Text>
            </View>
          }
          {!expiryDate &&
            <View style={[dcCard, { marginLeft: 5 }]}>
              <Text style={fCaption}>Floating Balance</Text>
              <Text style={fBody}>{currency} {Number(floatingAmount).toFixed(exchangeRate ? 3 : 2)}</Text>
            </View>
          }
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  mainCont: {
    width,
    backgroundColor: DCColor.BGColor('primary-1'),
    padding: 15
  },
  disabledWallet: {
    backgroundColor: DCColor.BGColor('disable')
  },
  balanceCont: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  fBody: {
    fontSize: TextFont.TextSize(15),
    fontWeight: '700',
    color: 'black',
  },
  fDisplay: {
    fontSize: TextFont.TextSize(35),
    fontWeight: '700',
    color: 'black',
  },
  dcCol: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  dcCard: {
    flex: 1,
    maxWidth: '60%',
    borderRadius: 4,
    backgroundColor: 'white',
    padding: 10,
    paddingHorizontal: 20,
    shadowColor: 'rgba(0, 0, 0, 0.15)',
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowRadius: 3,
    shadowOpacity: 1,
    elevation: 8,
  },
  fCaption: {
    fontWeight: '400',
    fontSize: TextFont.TextSize(12),
    color: 'rgba(0, 0, 0, 0.4)',
    textAlign: 'left'
  },
  exchangeRateFont: {
    fontWeight: '400',
    fontSize: TextFont.TextSize(10),
    color: 'rgba(0, 0, 0, 0.4)',
    textAlign: 'center',
    marginBottom: 15,
  }
})