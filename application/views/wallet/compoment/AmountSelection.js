import React, { PureComponent } from 'react'
import { Text, View, Image, ScrollView, SafeAreaView, StyleSheet, TouchableOpacity } from 'react-native'
import { Screen, TextFont, DCColor } from 'dacsee-utils'

export default class CmpAmountSelection extends PureComponent {

  constructor(props) {
    super(props)
    this.state = {
      selected: 0
    }
  }

  onSelect = (selected) => {
    this.setState({
      selected
    })
  }

  render() {
    const { options, onPress } = this.props
    const { mainCont, itemCont, activeBtn, btnFont } = styles
    const { selected } = this.state
    return (
      <View style={mainCont}>
        {options && options.map((o, i) =>
          <TouchableOpacity key={i} style={i === selected ? [itemCont, activeBtn] : itemCont} onPress={() => {
            onPress && onPress(o.value)
            this.onSelect(i)
          }}>
            <Text style={btnFont}>{o.label}</Text>
          </TouchableOpacity>
        )}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  mainCont: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  itemCont: {
    borderRadius: 100,
    backgroundColor: 'rgba(0, 0, 0, 0.1)',
    paddingVertical: 5,
    paddingHorizontal: 13,
  },
  activeBtn: {
    backgroundColor: DCColor.BGColor('primary-1')
  },
  btnFont: {
    fontSize: TextFont.TextSize(15),
    fontWeight: '900',
    color: DCColor.BGColor('primary-2'),
    textAlign: 'center'
  }
})