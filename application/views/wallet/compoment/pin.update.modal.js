import React, { Component } from 'react'
import { Text, View, TouchableOpacity, Image, Modal, TextInput, StyleSheet, KeyboardAvoidingView } from 'react-native'
import { inject, observer } from 'mobx-react'
import { Screen, TextFont, DCColor } from 'dacsee-utils'
import Resources from 'dacsee-resources'

const { height, width } = Screen.window
const styles = StyleSheet.create({
  dcBackDrop: {
    width,
    height,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.75)'
  },
  keyboardAvoidCont: {
    width: '100%',
    height: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: '5%'
  },
  dcCard: {
    width: '100%',
    borderRadius: 8,
    backgroundColor: 'white',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  dcImg: {
    height: 130,
    width: 130,
    resizeMode: 'contain',
    marginBottom: 30,
    marginTop: 20
  },
  fTitle: {
    textAlign: 'center',
    fontSize: TextFont.TextSize(22),
    lineHeight: TextFont.TextSize(22),
    fontWeight: '800',
    color: 'rgba(0, 0, 0, 0.75)',
    marginBottom: 10
  },
  fCaption: {
    textAlign: 'center',
    fontSize: TextFont.TextSize(16),
    lineHeight: TextFont.TextSize(16),
    fontWeight: '500',
    color: 'rgba(0, 0, 0, 0.5)',
    marginBottom: 20,
    paddingHorizontal: 20,
  },
  pinCont: {
    width: '100%',
    backgroundColor: 'rgba(0, 0, 0, 0.1)',
    padding: 20
  },
  dcInput: {
    marginBottom: 15,
    borderRadius: 22.5,
    height: 45,
    width: '100%',
    backgroundColor: 'rgba(0, 0, 0 ,0.1)',
    fontWeight: '700',
    paddingHorizontal: 15,
    textAlign: 'center'
  },
  actionCont: {
    width: '100%',
    height: 80,
    flexDirection: 'row',
    alignContent: 'center',
    justifyContent: 'center',
    padding: 15
  },
  dcBtn: {
    flex: 1,
    borderRadius: 8,
    flex: 3,
    backgroundColor: DCColor.BGColor('primary-1'),
    padding: 10,
    paddingLeft: 10,
    paddingRight: 10,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  cancelBtn: {
    flex: 2,
    backgroundColor: DCColor.BGColor('disable'),
    marginRight: 10,
  },
  btnFont: {
    fontSize: TextFont.TextSize(15),
    fontWeight: 'bold',
    color: 'rgba(0, 0, 0, 0.9)',
    textAlign: 'center'
  },
  cancelFont: {
    color: 'rgba(0, 0, 0, 0.65)'
  }
})

@inject('app')
@observer
export default class PinUpdateModal extends Component {

  constructor(props) {
    super(props)
    this.state = {
    }
  }

  render() {
    const {
      dcBackDrop,
      keyboardAvoidCont,
      dcCard,
      dcImg,
      fTitle,
      fCaption,
      pinCont,
      dcInput,
      actionCont,
      dcBtn,
      cancelBtn,
      btnFont,
      cancelFont
    } = styles
    const { app, onHide, onSubmit, visible, onChange } = this.props
    return (
      <Modal
        animationType='fade'
        transparent={true}
        visible={visible}
        onRequestClose={() => this.props.onClose()}
      >
        <View style={dcBackDrop}>
          <KeyboardAvoidingView behavior="position" contentContainerStyle={keyboardAvoidCont}>
            <View style={dcCard}>
              <Image style={dcImg} source={Resources.image.attention_icon} />
              <Text style={fTitle}>{app.strings.pin_title}</Text>
              <Text style={fCaption}>{app.strings.pin_desc}</Text>
              <View style={pinCont}>
                <TextInput
                  keyboardType='numeric'
                  returnKeyType={'done'}
                  secureTextEntry={true}
                  style={dcInput}
                  placeholder={app.strings.new_pin}
                  onChangeText={(text) => onChange('newPin', text)}
                  onSubmitEditing={() => this.codeInput && this.codeInput.focus()}
                  underlineColorAndroid='transparent'
                  holderTextColor={'#dbdbdd'}
                />
                <TextInput
                  keyboardType='numeric'
                  returnKeyType={'done'}
                  secureTextEntry={true}
                  ref={e => this.codeInput = e}
                  style={[dcInput, { marginBottom: 0 }]} placeholder={app.strings.confirm_pin}
                  onChangeText={(text) => onChange('confirmPin', text)}
                  onSubmitEditing={() => onSubmit()}
                  underlineColorAndroid='transparent'
                  holderTextColor={'#dbdbdd'}
                />
              </View>
              <View style={actionCont}>
                <TouchableOpacity style={[dcBtn, cancelBtn]} onPress={() => onHide()}>
                  <Text style={[btnFont, cancelFont]}>{app.strings.cancel.toUpperCase()}</Text>
                </TouchableOpacity>
                <TouchableOpacity style={dcBtn} onPress={() => onSubmit()}>
                  <Text style={btnFont}>{app.strings.confirm.toUpperCase()}</Text>
                </TouchableOpacity>
              </View>
            </View>
          </KeyboardAvoidingView>
        </View>
      </Modal>
    )
  }

}