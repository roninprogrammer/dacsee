import React, { Component } from 'react'
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native'
import { TextFont, DCColor } from 'dacsee-utils'
import { inject, observer } from 'mobx-react'

@inject('app')
@observer
export default class WalletTopUp extends Component {

  static navigationOptions = ({ navigation }) => {
    return {
      drawerLockMode: 'locked-closed',
      title: $.store.app.strings.top_up_method,
    }
  }

  async componentDidMount() {

  }
  componentWillUnmount() { }


  render() {
    const { navigation, app } = this.props

    return (
      <View style={styles.container}>
        <View style={{ justifyContent: 'center', alignItems: 'center', marginVertical: 18 }}>
          <Text style={{ fontSize: TextFont.TextSize(11), color: '#333' }}>{app.strings.how_deposit}</Text>
        </View>
        <TouchableOpacity
          onPress={() => navigation.navigate('WalletTopUpDetail')}
          activeOpacity={.7}
          style={{
            marginHorizontal: 22,
            backgroundColor: DCColor.BGColor('primary-1'),
            justifyContent: 'center',
            alignItems: 'center',
            paddingVertical: 10,
            borderRadius: 4,
            marginTop: 16,
            height: 45
          }}>
          <Text style={{ fontSize: TextFont.TextSize(15), fontWeight: 'bold', color: 'rgba(0, 0, 0, .75)' }}>{app.strings.cash_deposit}</Text>
        </TouchableOpacity>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  }
})

