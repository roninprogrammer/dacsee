
import WalletBalanceScreen from './wallet.balance'
import WalletTransactionListScreen from './wallet.transaction.list'
import WalletDetailScreen from './wallet.details'
import WalletTransferScreen from './wallet.transfer'
// import BankListScreen from '../setting/bank/bank.list'
import WalletTransferSelectionScreen from './wallet.transfer.acount.selection'
import WalletTransferSummaryScreen from './wallet.transfer.summary'
import WalletTopUpScreen from './wallet.topUp'
import WalletTopUpDetailScreen from './wallet.topUp.detail'
import WalletWithdrawScreen from './wallet.withdraw'
import WalletVerification from './wallet.verification'

export {
  WalletBalanceScreen,
  WalletTransactionListScreen,
  WalletDetailScreen,
  WalletTransferScreen,
  WalletTransferSelectionScreen,
  WalletTransferSummaryScreen,
  // BankListScreen,
  WalletTopUpScreen,
  WalletTopUpDetailScreen,
  WalletWithdrawScreen,
  WalletVerification
}