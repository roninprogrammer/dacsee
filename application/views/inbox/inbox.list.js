import React, { Component } from 'react'
import { StyleSheet ,Text, View, TouchableOpacity, Alert, RefreshControl, SafeAreaView, FlatList, AppState } from 'react-native'
import moment from 'moment'
import { inject, observer } from 'mobx-react'
import Material from 'react-native-vector-icons/MaterialIcons'
import MaterialIcon from 'react-native-vector-icons/FontAwesome'
import { Screen, DCColor, TextFont } from 'dacsee-utils'
import { NavigationActions } from 'react-navigation'

const { height, width } = Screen.window

@inject('app', 'inbox')
@observer
export default class InboxListingScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const { strings } = $.store.app
    return {
      title: strings.inbox_news
    }
  }
  constructor(props) {
    super(props);
    this.state = {
      loading: false
    }
  }

  async componentWillMount() {
    await this.props.inbox.getInboxList()
  }

  componentDidMount() {
    AppState.addEventListener('change', this._handleAppStateChange);
  }

  componentWillUnmount() {
    AppState.removeEventListener('change', this._handleAppStateChange);
  }

  _handleAppStateChange = async (state) => {
    if (state === 'active') {
      await this.props.inbox.getInboxList()
      await this.props.inbox.getInboxUnreadList()
    }
  }

  time(date) {
    const time = moment(date).toISOString();
    const time2 = moment(time).fromNow();
    return time2
  }

  deleteMessage(id) {
    Alert.alert(
      $.store.app.strings.delete_message,
      $.store.app.strings.confirm_delete_message,
      [
        { text: $.store.app.strings.cancel, onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        { text: $.store.app.strings.confirm, onPress: () => { $.store.inbox.deleteInboxMessage(id) } },
      ],
      { cancelable: false }
    )
  }

  async onReload() {
    await this.props.inbox.getInboxList()
  }

  render() {
    const {
      safeareaStyle,
      container, 
      indexStyle,
      inboxcontainerStyle,
      inboxdetailStyle,
      inboxtitleStyle,
      inboxstartonStyle,
      inboxdescStyle, 
      inboxtextdescStyle,
      listemptycomponentStyle,
      textemptycomponentStyle
    } = styles
    
    const refreshControl = (
      <RefreshControl
        refreshing={$.store.inbox.loading}
        onRefresh={() => this.onReload()}
        title={this.props.app.strings.pull_refresh}
        colors={['#ffffff']}
        progressBackgroundColor={DCColor.BGColor('primary-1')}
      />
    )
    return (
      <SafeAreaView style={safeareaStyle}>
        <View style={container}>
          <FlatList
            refreshControl={refreshControl}
            data={$.store.inbox.inboxList}
            keyExtractor={(item, index) => item._id}
            renderItem={({ item, index }) => (
              <View index={index} style={indexStyle}>
                <View style={inboxcontainerStyle}>
                  {item.state == 'new' ? <MaterialIcon name={'circle'} size={10} color={'red'} style={{ marginBottom: 20 }} /> : null}
                  <TouchableOpacity onPress={() => { this.deleteMessage(item._id) }}>
                    <Material name={'delete'} size={22} color={'#00000040'} />
                  </TouchableOpacity>
                </View>
                <View style={{ width: '100%' }}>
                  <TouchableOpacity onPress={() => { global.$.navigator.dispatch(NavigationActions.navigate({ routeName: 'InboxDetailScreen', params: { id: item._id, index: index, from: 'inbox' } })) }}>
                    <View style={inboxdetailStyle}>
                      <Text style={inboxtitleStyle} numberOfLines={1}>{item.title}</Text>
                      <Text style={inboxstartonStyle}>{this.time(item.startOn)}</Text>
                      <View style={inboxdescStyle}>
                        <Text style={inboxtextdescStyle} numberOfLines={1}>{item.description}</Text>
                      </View>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
            )}
            enableEmptySections
            ListEmptyComponent={() =>
              <View style={listemptycomponentStyle} >
                <Text style={textemptycomponentStyle} >You dont have any inbox message at the moment!</Text>
              </View>}
          />
        </View>
      </SafeAreaView>
    )
  }

}

const styles = StyleSheet.create({
safeareaStyle: {
  flex: 1
},
container: {
  flex: 1, 
  height: height - 90, 
  width, 
  paddingHorizontal: 20,
  paddingTop: 5, 
  position: 'absolute'
},
indexStyle: {
  flexDirection: 'row', 
  width: '100%', 
  borderRadius: 5, 
  backgroundColor: '#ffffff', 
  marginTop: 5
},
inboxcontainerStyle: {
  borderBottomLeftRadius: 5,
  width: 40, 
  borderTopLeftRadius: 5, 
  alignItems: 'center',
  justifyContent: 'center' 
}, 
inboxdetailStyle : {
  flex: 1, 
  paddingBottom: 15,
  paddingTop: 15,
  paddingHorizontal: 10
}, 
inboxtitleStyle : {
   fontSize: TextFont.TextSize(18), 
   fontWeight: 'bold', 
   color: 'black',
   width: '83%' 
},
inboxstartonStyle : {
   fontSize: TextFont.TextSize(15), 
   color: DCColor.BGColor('primary-1') 
},
inboxdescStyle : {
  paddingTop: 10, 
  width: '83%'
},
inboxtextdescStyle : {
  fontSize: TextFont.TextSize(12)
},
listemptycomponentStyle : {
   height: height - 200, 
   justifyContent: 'center', 
   alignItems: 'center' 
}, 
textemptycomponentStyle : {
   marginTop: 20, 
   color: '#777', 
   fontSize: TextFont.TextSize(16), 
   fontWeight: '400' 
}
})
