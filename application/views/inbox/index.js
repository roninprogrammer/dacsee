import InboxListingScreen from './inbox.list'
import InboxDetailScreen from './inbox.detail'

export {
    InboxListingScreen,
    InboxDetailScreen
}