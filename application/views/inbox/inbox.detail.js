import React, { Component } from 'react'
import { Text, View, TouchableOpacity, ScrollView, Alert, SafeAreaView, Linking, Image, StyleSheet } from 'react-native'
import { inject, observer } from 'mobx-react'
import Material from 'react-native-vector-icons/MaterialIcons'
import Barcode from 'react-native-barcode-builder'
import { Screen, DCColor, TextFont } from 'dacsee-utils'
import { toastShort } from '../../global/method/ToastUtil'

const { height, width } = Screen.window

@inject('app', 'inbox')
@observer
export default class InboxDetailScreen extends Component {

  static navigationOptions = ({ navigation }) => {
    const { strings } = $.store.app
    return {
      title: strings.inbox_news
    }
  }
  constructor(props) {
    super(props);
    this.state = {
      index: '',
      from: '',
      data: '',
      toast: true,
    }
  }

  async componentDidMount() {
    const { index = 0, id, from } = this.props.navigation.state.params
    const data = await this.props.inbox.getInboxDetail(id)
    this.setState({ index: index, from: from })
    if (data) {
      this.messageRead(id)
    }
  }

  messageRead = (id) => {
    this.props.inbox.submitMessageRead(id)
  }

  getNextMessageData(index) {
    if (this.state.from == 'popup') {
      index = index + 1
      const id = $.store.inbox.inboxModalDetail[index]._id
      const data = this.props.inbox.getInboxDetail(id)
      this.setState({ index: index })
      if (data) {
        this.messageRead(id)
      }
    }
    else if (this.state.from == 'inbox') {
      index = index + 1
      const id = $.store.inbox.inboxList[index]._id
      const data = this.props.inbox.getInboxDetail(id)
      this.setState({ index: index })
      if (data) {
        this.messageRead(id)
      }
    }
  }

  getPerviousMessageData(index) {
    if (this.state.from == 'popup') {
      index = index - 1
      const id = $.store.inbox.inboxModalDetail[index]._id
      const data = this.props.inbox.getInboxDetail(id)
      this.setState({ index: index })
      if (data) {
        this.messageRead(id)
      }
    }
    else if (this.state.from == 'inbox') {
      index = index - 1
      const id = $.store.inbox.inboxList[index]._id
      const data = this.props.inbox.getInboxDetail(id)
      this.setState({ index: index })
      if (data) {
        this.messageRead(id)
      }
    }
  }

  nextPage(index) {
    if (this.state.from == 'popup') {
      const inboxAmount = $.store.inbox.inboxModalDetail.length
      if (index == 0 && inboxAmount > 1) {
        this.getNextMessageData(index)
      }
      else if (index > 0) {
        if (index !== (inboxAmount - 1)) {
          this.getNextMessageData(index)
        }
        else {
          if (this.state.toast == true) {
            toastShort($.store.app.strings.no_more_message)
            this.setState({ toast: false })
          }
          return
        }
      }
    }
    else if (this.state.from == 'inbox') {
      const inboxAmount = $.store.inbox.inboxList.length
      if (index == 0 && inboxAmount > 1) {
        this.getNextMessageData(index)
      }
      else if (index > 0) {
        if (index !== (inboxAmount - 1)) {
          this.getNextMessageData(index)
        }
        else {
          if (this.state.toast == true) {
            toastShort($.store.app.strings.no_more_message)
            this.setState({ toast: false })
          }
          return
        }
      }
    }

  }

  perviousPage(index) {
    this.setState({ toast: true })
    if (this.state.from === 'popup') {
      const inboxAmount = $.store.inbox.inboxModalDetail.length
      if (index == (inboxAmount - 1) && inboxAmount > 1) {
        this.getPerviousMessageData(index)
      }
      else if (index < (inboxAmount - 1)) {
        if (index !== 0) {
          this.getPerviousMessageData(index)
        }
        else if (index == 0) {
          return
        }
      }
    }
    else if (this.state.from === 'inbox') {
      const inboxAmount = $.store.inbox.inboxList.length
      if (index == (inboxAmount - 1) && inboxAmount > 1) {
        this.getPerviousMessageData(index)
      }
      else if (index < (inboxAmount - 1)) {
        if (index !== 0) {
          this.getPerviousMessageData(index)
        }
        else if (index == 0) {
          return
        }
      }
    }

  }

  deleteMessage(id, index) {
    Alert.alert(
      $.store.app.strings.delete_message,
      $.store.app.strings.confirm_delete_message,
      [
        { text: $.store.app.strings.cancel, onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        { text: $.store.app.strings.confirm, onPress: () => { this.deleteMessageProcess(id, index) } },
      ],
      { cancelable: false }
    )
  }

  deleteMessageProcess(id, index) {
    const { goBack } = this.props.navigation;
    $.store.inbox.deleteInboxMessage(id)
    if (this.state.from === 'popup') {
      if (index > 0) {
        const inboxAmount = $.store.inbox.inboxModalDetail.length
        if (index < (inboxAmount - 1)) {
          index = index + 1
          const messageID = this.props.inbox.inboxModalDetail[index]._id
          const data = this.props.inbox.getInboxDetail(messageID)
          this.setState({ index: index - 1 })
          if (data) {
            this.messageRead(messageID)
          }
        }
        else {
          index = index - 1
          const messageID = this.props.inbox.inboxModalDetail[index]._id
          const data = this.props.inbox.getInboxDetail(messageID)
          this.setState({ index: index })
          if (data) {
            this.messageRead(messageID)
          }
        }
      }
      else if (index == 0) {
        const inboxAmount = $.store.inbox.inboxModalDetail.length
        if (inboxAmount > 1) {
          index = index + 1
          const messageID = this.props.inbox.inboxModalDetail[index]._id
          const data = this.props.inbox.getInboxDetail(messageID)
          this.setState({ index: index - 1 })
          if (data) {
            this.messageRead(messageID)
          }
        }
        else {
          goBack();
        }
      }
    }
    else if (this.state.from === 'inbox') {
      if (index > 0) {
        const inboxAmount = $.store.inbox.inboxList.length
        if (index < (inboxAmount - 1)) {
          index = index + 1
          const messageID = this.props.inbox.inboxList[index]._id
          const data = this.props.inbox.getInboxDetail(messageID)
          this.setState({ index: index - 1 })
          if (data) {
            this.messageRead(messageID)
          }
        }
        else {
          index = index - 1
          const messageID = this.props.inbox.inboxList[index]._id
          const data = this.props.inbox.getInboxDetail(messageID)
          this.setState({ index: index })
          if (data) {
            this.messageRead(messageID)
          }
        }
      }
      else if (index == 0) {
        const inboxAmount = $.store.inbox.inboxList.length
        if (inboxAmount > 1) {
          index = index + 1
          const messageID = this.props.inbox.inboxList[index]._id
          const data = this.props.inbox.getInboxDetail(messageID)
          this.setState({ index: index - 1 })
          if (data) {
            this.messageRead(messageID)
          }
        }
        else {
          $.store.inbox.getInboxList()
          goBack();
        }
      }
    }
    else if (this.state.from === 'notification') {
      const { goBack } = this.props.navigation;
      $.store.inbox.deleteInboxMessage(id)
      goBack();
    }
  }

  async getFriendProfile(data) {
    const { strings } = $.store.app
    try {
      const res = await $.method.session.Circle.Get(`v1/circleStatus?user_id=${data._id}`)
      const isFriend = res.isFriend
      if (isFriend) {
        const friendData = Object.assign({}, { friend_info: data }, { friend_id: data._id }, { _id: res.connection_id })
        this.props.navigation.navigate('FriendsDetail', { friendData, type: 'FRIEND' })
      } else {
        this.props.navigation.navigate('FriendsRequest', { referrer: data.userId, id: data._id, avatars: data.avatars, fullName: data.fullName, userId: data.userId })
      }
    } catch (e) {
      $.define.func.showMessage(strings.unable_connect_server_pls_retry_later)
      this.props.navigation.goBack()
    }
  }

  async getButtonInfo(type, data) {
    if (type === 'internalLink') {
      if (data && data !== null) {
        if (data.target === 'joy_reward') {
          const result = await $.store.joy.getJoyDetail(data.userReward_id)
          if (result && result._id) {
            this.props.navigation.navigate('JoyDetailScreen', { id: data.userReward_id })
          }
          else {
            Alert.alert(
              $.store.app.strings.alert_joyTitle,
              $.store.app.strings.alert_nojoy,
              [
                { text: 'OK', onPress: () => console.log("ok") },
              ],
              { cancelable: false }
            )
          }
        }
        else if (data.target === 'profile') {
          this.props.navigation.navigate('SettingAccount');
        }
        else if (data.target === 'friendSuggestion') {
          const { searchFriendAccount } = $.store.circle
          const result = await searchFriendAccount('id', data.friend_id)
          if (result.length > 0) {
            this.getFriendProfile(result[0]);
          }
        }
      }
    }
    else if (type === 'externalLink') {
      if (data && data !== null) {
        Linking.openURL(data.url).catch(err => console.log('An error occurred', err));
      }
    }
  }

  barcodeFlat = (format) => {
    if (format === 'EAN13') {
      return true
    }
    else {
      return false
    }

  }

  barcodeLength = (format) => {
    if (format === 'EAN13') {
      return 2
    }
    else {
      return 1.2
    }
  }


  render() {
    const {
      mainCont,
      decoBG,
      scrollCont,
      dcCard
    } = styles
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <View style={mainCont}>
          <View style={decoBG} />
          <View style={scrollCont}>
            <View style={dcCard}>
              <View style={{ height: '90%', width: '100%' }}>
                <View style={{ flex: 1 }}>
                  <View style={{ width: '100%', height: height - (height * 0.725), backgroundColor: DCColor.BGColor('primary-1'), justifyContent: 'center', borderTopLeftRadius: 5, borderTopRightRadius: 5, overflow: 'hidden' }}>
                    <Image source={$.store.inbox.inboxDetail.img} style={$.store.inbox.inboxDetail.hasImg ? { width: '100%', height: height - (height * 0.725), justifyContent: 'center', alignSelf: 'center', borderTopLeftRadius: 5, borderTopRightRadius: 5, resizeMode: 'cover' } : { width: 120, height: 120, justifyContent: 'center', alignSelf: 'center', borderTopLeftRadius: 5, borderTopRightRadius: 5, resizeMode: 'contain' }} />
                  </View>
                  <ScrollView >
                    <View style={{ width: '100%', alignItems: 'center', paddingTop: 10, paddingBottom: 20, paddingHorizontal: 20 }}>
                      <Text style={{ textAlign: 'center', fontSize: TextFont.TextSize(18), fontWeight: 'bold', color: 'black' }}>{$.store.inbox.inboxDetail.title}</Text>
                      {$.store.inbox.inboxDetail.barcode == null ? null
                        :
                        <View style={{ alignSelf: 'center', paddingVertical: 15 }}>
                          <Barcode width={this.barcodeLength($.store.inbox.inboxDetail.barcode.format)} value={$.store.inbox.inboxDetail.barcode.value} format={$.store.inbox.inboxDetail.barcode.format} flat={this.barcodeFlat($.store.inbox.inboxDetail.barcode.format)} />
                          <Text style={{ textAlign: 'center', fontSize: TextFont.TextSize(16) }}>{$.store.inbox.inboxDetail.barcode.value}</Text>
                        </View>
                      }
                      <Text style={{ fontSize: TextFont.TextSize(14), marginTop: 10 }}>{$.store.inbox.inboxDetail.description}</Text>
                    </View>
                    {$.store.inbox.inboxDetail.buttons == null ? null
                      :
                      <View>
                        {$.store.inbox.inboxDetail.buttons.map((i, idx) => (
                          <View key={idx} style={{ alignSelf: 'center', paddingBottom: 10, width: '100%', justifyContent: 'center', alignItems: 'center' }}>
                            <TouchableOpacity onPress={() => this.getButtonInfo(i.actionType, i.actionData)} style={{ width: width - 160, alignItems: 'center', justifyContent: 'center', backgroundColor: DCColor.BGColor('primary-1'), padding: 10, borderRadius: 5 }}>
                              <Text style={{ fontSize: TextFont.TextSize(15), fontWeight: 'bold', color: DCColor.BGColor('primary-2') }}>{i.name}</Text>
                            </TouchableOpacity>
                          </View>
                        ))}
                      </View>
                    }
                  </ScrollView>
                </View>
              </View>
              {this.state.from !== 'notification' ?
                <View style={{ flexDirection: 'row', position: 'absolute', bottom: 20, justifyContent: 'space-between', alignItems: 'center', width: '98%' }}>
                  <TouchableOpacity onPress={() => this.perviousPage(this.state.index)}>
                    <Material name={'chevron-left'} size={35} />
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => { this.deleteMessage($.store.inbox.inboxDetail._id, this.state.index) }}>
                    <Material name={'delete'} size={30} />
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => this.nextPage(this.state.index)}>
                    <Material name={'chevron-right'} size={35} />
                  </TouchableOpacity>
                </View>
                :
                <View style={{ position: 'absolute', bottom: 20, justifyContent: 'center', alignSelf: 'center', alignItems: 'center', width: '100%' }}>
                  <TouchableOpacity onPress={() => { this.deleteMessage($.store.inbox.inboxDetail._id, this.state.index) }}>
                    <Material name={'delete'} size={30} />
                  </TouchableOpacity>
                </View>
              }
            </View>
          </View>
        </View>
      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  mainCont: {
    position: 'relative',
    height,
    width,
  },
  decoBG: {
    height: height * .3,
    width,
    backgroundColor: DCColor.BGColor('primary-1'),
    position: 'absolute',
    top: 0
  },
  scrollCont: {
    width: '100%',
    height: '100%',
    padding: 10,
    paddingVertical: 5,
    paddingBottom: '15%',
  },
  dcCard: {
    flex: 1,
    borderRadius: 10,
    backgroundColor: '#ffffff',
    marginTop: 5,
    alignItems: 'center',
    shadowOffset: { width: 0, height: 1 },
    shadowColor: '#000',
    shadowOpacity: 0.3,
    elevation: 0.3
  },
})