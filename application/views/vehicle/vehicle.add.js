import React, { Component } from 'react'
import { StyleSheet, Text, View, Image, TextInput, ScrollView, TouchableOpacity, Alert, ActivityIndicator, Keyboard, SafeAreaView } from 'react-native'
import { System, Screen, Icons, TextFont, DCColor } from 'dacsee-utils'
import { inject, observer } from 'mobx-react'
import ActionSheet from 'react-native-actionsheet'
import ImagePicker from 'react-native-image-picker'
import SelectManufacturer from '../../components/selectManufacturer'
import SelectCarModel from '../../components/selectCarModel'
import SelectVehicles from '../modal/modal.selectvehicle'

const { width, height } = Screen.window

const styles = StyleSheet.create({
  manuCont: {
    padding: 10,
    marginHorizontal: 5,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  detailsCont: {
    marginHorizontal: 5,
    paddingHorizontal: 10,
    paddingBottom: 2,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  subcont: {
    textAlign: 'center',
    height: 40,
    width: 40
  },
  imgcont: {
    height: 23,
    width: 23
  },
  editCont: {
    textAlign: 'center',
    width: 40,
    fontSize: TextFont.TextSize(12),
    fontWeight: 'bold'
  },
  typeCont: {
    fontSize: TextFont.TextSize(15),
    padding: 5,
    textAlign: 'center',
    width: 100
  },
  smallCont: {
    textAlign: 'center',
    height: 40,
    width: 90
  },
  maintitle: {
    fontSize: TextFont.TextSize(15),
    color: 'black',
    marginLeft: 0,
    fontWeight: '500',
  },
  background_title: {
    backgroundColor: '#ccc',
    borderRadius: 20,
    width: 100,
    alignItems: 'center',
    marginTop: 5
  },
  background_circle: {
    backgroundColor: '#ccc',
    borderRadius: 27,
    marginRight: 5,
    height: 27,
    width: 27,
    alignItems: 'center',
    justifyContent: 'center'
  },
  background_edit: {
    backgroundColor: DCColor.BGColor('primary-1'),
    borderRadius: 27,
    width: 50,
    height: 27,
    alignItems: 'center',
    justifyContent: 'center'
  },
  jobCont: {
    padding: 15,
    flexDirection: 'column',
    justifyContent: 'space-between',
    backgroundColor: 'rgba(0,0,0,0.05)',
    paddingBottom: 5
  },
  carCont: {
    flexDirection: 'row',
    marginRight: 10
  },
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  photo: {
    flex: 1,
    backgroundColor: '#eee',
    borderColor: '#ccc',
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomWidth: 0.8
  },
  photoButton: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 8,
    backgroundColor: '#ccc',
    borderRadius: 18,
    paddingLeft: 10,
    marginHorizontal: 10,
  },
  title: {
    fontSize: TextFont.TextSize(14),
    color: 'black',
    opacity: 0.8,
    marginLeft: 0
  },
  carselection: {
    marginRight: 0,
    fontSize: TextFont.TextSize(14),
  },
  selection: {
    flexDirection: 'row',
    marginRight: 0,
  },
  button: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#5FD700',
    height: 50,
    margin: 5,
    borderRadius: 5
  },
  longRow: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnText: {
    textAlign: 'center',
    justifyContent: 'center',
    padding: 5,
    fontSize: TextFont.TextSize(18),
    color: 'black',
    fontWeight: '500'
  },
  touchButton: {
    width: width / 2 - 20,
    height: 70,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 50,
    backgroundColor: '#cfcfcf',
    borderStyle: 'solid',
    borderWidth: 4,
    borderColor: '#ffffff',
    shadowColor: 'rgba(0, 0, 0, 0.21)',
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowRadius: 3,
    shadowOpacity: 1
  }

})

@inject('app', 'vehicles')
@observer
export default class VehicleAdd extends Component {

  static navigationOptions = ({ navigation }) => {
    const { strings } = $.store.app
    const { vehiclesData } = $.store.vehicles
    let title = vehiclesData.length > 0 ? strings.myvehicle : strings.car_add_vehicle

    return {
      drawerLockMode: 'locked-closed',
      title: title,
    }
  }
  constructor(props) {
    super(props)
    const { user = {}, vehicles = {} } = props
    const { vehiclesData } = vehicles
    const currentData = vehiclesData.slice()[0] || {}

    this.state = {
      carNumber: currentData.registrationNo ? currentData.registrationNo : '',
      manufacturer: currentData.manufacturer ? currentData.manufacturer : '',
      carModel: currentData.model ? currentData.model : '',
      manufactureYear: currentData.manufactureYear ? currentData.manufactureYear : '',
      color: currentData.color ? currentData.color : '',
      availableCategories: currentData.availableCategories ? currentData.availableCategories : '',
      category_ids: currentData.category_ids ? currentData.category_ids : '',
      front_photo: currentData.frontPhoto && currentData.frontPhoto.urlSmall
        ? currentData.frontPhoto.urlSmall
        : '',
      back_photo: currentData.backPhoto && currentData.backPhoto.urlSmall
        ? currentData.backPhoto.urlSmall
        : '',
      maxManufacturerYear: (new Date()).getFullYear() + 1,
      update_id: currentData._id ? currentData._id : '',
      type: currentData._id ? 'update' : 'add',
      front_photo_data: '',
      back_photo_data: '',
      carPhotos: [],
      manufacturerData: [],
      isManufacturerFocus: true,
      carModelData: [],
      isCarModelFocus: true,
      buttonName: 'front',
      keyboardHeight: 0,
      iosBottomViewHeight: 85,
      marginBottom: 85,
      showManufacturer: false,
      showCarModel: false,
      showSelectVehicles: false,
      identicalElements: []
    }
  }

  componentDidMount() {
    if (System.Platform.iOS) {
      this.keyboardWillShowSub = Keyboard.addListener('keyboardWillShow', this.keyboardWillShow)
      this.keyboardWillHideSub = Keyboard.addListener('keyboardWillHide', this.keyboardWillHide)
    } else {
      this.keyboardWillShowSub = Keyboard.addListener('keyboardDidShow', this.keyboardDidShow)
      this.keyboardWillHideSub = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide)
    }
    this.fetchData()
  }

  componentWillUnmount() {
    if (System.Platform.iOS) {
      this.keyboardWillShowSub.remove()
      this.keyboardWillHideSub.remove()
    }
    const { vehicles } = this.props
    vehicles.loading = false

  }

  keyboardWillShow = (event) => {
    this.setState({ keyboardHeight: event.endCoordinates.height, iosBottomViewHeight: 0 })
  };

  keyboardWillHide = (event) => {
    this.setState({ keyboardHeight: 0, iosBottomViewHeight: 85 })
  };

  keyboardDidShow = () => {
    this.setState({ marginBottom: 0 })
  }

  keyboardDidHide = (event) => {
    this.setState({ marginBottom: 85 })
  }

  async fetchData() {
    const { vehicles } = this.props
    const resp = await vehicles.getVehiclesData()
    if (resp.length > 0) {
      const currentData = resp[0]
      this.setState({
        carNumber: currentData.registrationNo,
        manufacturer: currentData.manufacturer,
        carModel: currentData.model,
        manufactureYear: currentData.manufactureYear,
        color: currentData.color,
        front_photo: currentData.frontPhoto && currentData.frontPhoto.urlSmall
          ? currentData.frontPhoto.urlSmall
          : '',
        back_photo: currentData.backPhoto && currentData.backPhoto.urlSmall
          ? currentData.backPhoto.urlSmall
          : '',
        update_id: currentData._id,
        availableCategories: currentData.availableCategories,
        category_ids: currentData.category_ids
      })
    }
    if (resp[0] && !!resp[0].category_ids) {
      this.comparing()
    }
  }

  onSubmit = () => {
    const { addOrUpdateVehicle } = this.props.vehicles
    const { carNumber, manufacturer, carModel,
      manufactureYear, color, front_photo_data, back_photo_data, back_photo, front_photo, type, update_id, } = this.state
    const data = {
      carNumber, manufacturer, carModel,
      manufactureYear, color, front_photo_data, back_photo_data, back_photo, front_photo, type, update_id
    }
    addOrUpdateVehicle(data)
  }

  cancelPress = () => {
    this.props.navigation.pop()
  }

  _addMethod = (type) => {
    this.setState({ buttonName: type })
    this.ActionSheet.show()
  }

  selectvehicles = () => {
    this.setState({ showSelectVehicles: true })
  }

  onHide = async () => {
    this.setState({ showSelectVehicles: false })
    await this.fetchData()
  }

  _getOptions = () => {
    const { strings } = this.props.app
    return {
      title: strings.album,
      storageOptions: { skipBackup: true, path: 'images' },
      quality: 0.8,
      mediaType: 'photo',
      cancelButtonTitle: strings.cancel,
      takePhotoButtonTitle: strings.take_photo,
      chooseFromLibraryButtonTitle: strings.select_from_album,
      allowsEditing: true,
      noData: false,
      maxWidth: 1000,
      maxHeight: 1000,
      permissionDenied: {
        title: strings.refuse_visit, text: strings.pls_auth,
        reTryTitle: strings.retry, okTitle: strings.okay
      }
    }
  }

  _pressActionSheet(index) {
    const { strings } = this.props.app
    const { buttonName } = this.state
    const uri = buttonName === 'front' ? 'front_photo' : 'back_photo'
    const data = buttonName === 'front' ? 'front_photo_data' : 'back_photo_data'
    if (index === 0) {
      ImagePicker.launchCamera(this._getOptions(), (response) => {
        if (response.didCancel) {
          return
        } else if (response.error) {
          let label = response.error.startsWith('Photo') ? strings.photo : strings.camera
          return Alert.alert(`${strings.pls_auth_fore} ${label} ${strings.pls_auth_back} ${label}。`)
        } else {
          this.setState({
            [uri]: response.uri,
            [data]: response.data
          })
        }
      })
    }
    if (index === 1) {
      ImagePicker.launchImageLibrary(this._getOptions(), (response) => {
        if (response.didCancel) {
          return
        } else if (response.error) {
          let label = response.error.startsWith('Photo') ? strings.photo : strings.camera
          return Alert.alert(`${strings.pls_auth_fore} ${label} ${strings.pls_auth_back} ${label}。`)
        } else {
          this.setState({
            [uri]: response.uri,
            [data]: response.data
          })
        }
      })
    }
  }

  comparing = () => {
    let identicalElements = [];
    const { category_ids, availableCategories } = this.state

    category_ids.forEach((element1) => {
      availableCategories.forEach((element2) => {
        if (element1 === element2['_id'])
          identicalElements.push(element2);
      });
    });
    $.store.vehicles.carStorege = identicalElements;
    return identicalElements

  }

  render() {
    const { strings } = this.props.app
    const { manufacturerData, fetchCarModel, carModelData, vehiclesData, loading } = this.props.vehicles
    const { carNumber, manufacturer, carModel, manufactureYear, color, front_photo, back_photo, marginBottom, maxManufacturerYear, showManufacturer, showCarModel } = this.state
    const androidStyle = System.Platform.Android ? { left: 20, position: 'absolute', top: height - 85 - 70 } : null
    const { container, manuCont, title, selection, carselection, photoButton, maintitle, background_circle, jobCont, carCont, background_edit, background_title, editCont, typeCont, imgcont, detailsCont, button, btnText } = styles

    return (
      <SafeAreaView style={container}>
        <ScrollView style={[{ backgroundColor: 'white', flex: 1, }, System.Platform.Android && { marginBottom }]}>
          {vehiclesData.length > 0 && ((!!vehiclesData[0].availableCategories && vehiclesData[0].availableCategories.length > 0) && (!!vehiclesData[0].category_ids && vehiclesData[0].category_ids.length > 0)) && <SelectVehicles visible={this.state.showSelectVehicles} onClose={() => this.onHide()} />}
          <View style={{ flexDirection: 'row', height: 150 }}>
            <View style={[styles.photo, { borderRightWidth: 0.6 }]}>
              {front_photo ?
                <TouchableOpacity activeOpacity={.7} onPress={() => this._addMethod('front')}>
                  <Image style={{ width: width / 2, height: 150 }} source={{ uri: front_photo }} resizeMode={'cover'} />
                </TouchableOpacity>
                :
                <TouchableOpacity activeOpacity={.7} style={photoButton} onPress={() => this._addMethod('front')}>
                  {Icons.Generator.Awesome('image', 20, '#111')}
                  <Text style={{ opacity: 0.7, marginHorizontal: 10, textAlign: 'center' }} >{strings.add_front_photo}</Text>
                </TouchableOpacity>
              }
            </View>
            <View style={[styles.photo, { borderLeftWidth: 0.6 }]}>
              {back_photo ?
                <TouchableOpacity activeOpacity={.7} onPress={() => this._addMethod('back')}>
                  <Image style={{ width: width / 2, height: 150 }} source={{ uri: back_photo }} resizeMode={'cover'} />
                </TouchableOpacity>
                :
                <TouchableOpacity activeOpacity={.7} style={photoButton} onPress={() => this._addMethod('back')}>
                  {Icons.Generator.Awesome('image', 20, '#111')}
                  <Text style={{ opacity: 0.7, marginHorizontal: 10, textAlign: 'center' }} >{strings.add_back_photo}</Text>
                </TouchableOpacity>
              }
            </View>
          </View>

          {vehiclesData.length > 0 && (!!vehiclesData[0].registrationNo && !!vehiclesData[0].manufacturer && !!vehiclesData[0].model) &&
            <View style={manuCont}>
              <Text style={maintitle}>{manufacturer} {carModel}</Text>
              <View style={background_title}>
                <Text style={typeCont}>{carNumber}</Text>
              </View>
            </View>
          }
          {vehiclesData.length > 0 && ((!!vehiclesData[0].availableCategories && vehiclesData[0].availableCategories.length > 0) && (!!vehiclesData[0].category_ids && vehiclesData[0].category_ids.length > 0)) &&
            <View style={jobCont}>
              <Text style={[maintitle, { paddingBottom: 5 }]}>{strings.receive_job_from}</Text>
              <ScrollView
                style={carCont}
                horizontal={true}
              >
                {
                  $.store.vehicles.carStorege.map((i, idx) => (
                    <View key={idx} style={background_circle}>
                      <Image style={imgcont} source={{ uri: i.avatars[i.avatars.length - 1].url }} />
                    </View>
                  ))
                }
                <TouchableOpacity style={background_edit} onPress={() => this.selectvehicles()}>
                  <Text style={editCont}>{strings.edit_vehicle}</Text>
                </TouchableOpacity>
              </ScrollView>
            </View>
          }
          <View style={[detailsCont, { marginTop: 10 }]}>
            <Text style={title}>{strings.car_registration_number}</Text>
            <TextInput
              style={{ textAlign: 'right', fontSize: TextFont.TextSize(14), color: '#000', paddingVertical: -25, width: 120, fontWeight: '500', paddingRight: 0 }}
              multiline={false}
              allowFontScaling={true}
              editable={true}
              placeholder={strings.input_prompt}
              value={carNumber ? carNumber.toString() : ''}
              onChangeText={(text) =>
                this.setState({ carNumber: text })
              }
            />
          </View>
          <View style={[detailsCont, { marginBottom: 5, marginTop: 2 }]}>
            <Text style={title}>{strings.car_manufacturer}</Text>
            <TouchableOpacity style={selection} onPress={() => this.setState({ showManufacturer: true })}>
              <Text style={[carselection, manufacturer === '' ? { color: 'rgba(0,0,0,0.08)', fontWeight: '500' } : { color: 'black', fontWeight: '500' }]}>{manufacturer === '' ? strings.input_prompt : manufacturer}</Text>
              <SelectManufacturer
                visible={showManufacturer}
                onHide={() => this.setState({ showManufacturer: false })}
                manufacturerChange={(text) => {
                  this.setState({ showManufacturer: false })
                  if (text) {
                    this.setState({ manufacturer: text, carModel: '' }, () => this.props.vehicles.fetchCarModel(this.state.manufacturer))
                  }
                }}
              />
            </TouchableOpacity>
          </View>
          <View style={detailsCont}>
            <Text style={title}>{strings.car_model}</Text>
            <TouchableOpacity style={selection} onPress={() => { this.props.vehicles.fetchCarModel(this.state.manufacturer), this.setState({ showCarModel: true }) }}>
              <Text style={[carselection, carModel === '' ? { color: 'rgba(0,0,0,0.08)', fontWeight: '500' } : { color: 'black', fontWeight: '500' }]}>{carModel === '' ? strings.input_prompt : carModel}</Text>
              <SelectCarModel
                visible={showCarModel}
                onHide={() => this.setState({ showCarModel: false })}
                carModelChange={(text) => {
                  this.setState({ showCarModel: false })
                  if (text) {
                    this.setState({ carModel: text })
                  }
                }}
              />
            </TouchableOpacity>
          </View>
          <View style={detailsCont}>
            <Text style={title}>{strings.car_manufacture_year}</Text>
            <TextInput
              style={{ textAlign: 'right', fontSize: TextFont.TextSize(14), color: 'black', width: 120, paddingVertical: -15, paddingRight: 0, fontWeight: '500' }}
              multiline={false}
              editable={true}
              placeholder={strings.input_prompt}
              value={manufactureYear ? `${manufactureYear}` : ''}
              onChangeText={(text) => {
                if (isNaN(Number(text))) return $.define.func.showMessage('Please enter a valid year')
                if (parseInt(text) > maxManufacturerYear) return $.define.func.showMessage('Please enter a valid manufacture year')
                this.setState({ manufactureYear: parseInt(text) })
              }}
              keyboardType='numeric'
              returnKeyType='done'
            />
          </View>
          <View style={[detailsCont, { marginTop: -2 }]}>
            <Text style={title}>{strings.car_color}</Text>
            <TextInput
              style={{ textAlign: 'right', fontSize: TextFont.TextSize(14), color: 'black', width: 120, paddingVertical: -25, paddingRight: 0, fontWeight: '500' }}
              editable={true}
              value={color ? color.toString() : ''}
              placeholder={strings.input_prompt}
              onChangeText={(text) => this.setState({ color: text })}
            />
          </View>
        </ScrollView>
        {/* </View> */}
        {
          this.state.type === 'update' ?
            <View style={[{ flexDirection: 'row', height: this.state.iosBottomViewHeight, backgroundColor: 'white', justifyContent: 'center' }, androidStyle]}>
              <TouchButton title={strings.car_update.toUpperCase()} status={loading} cancel={false} style={{ width: width - 40 }} backgroundColor={DCColor.BGColor('primary-1')} onPress={this.onSubmit} />
            </View>
            :
            <View style={[{ flexDirection: 'row', height: this.state.iosBottomViewHeight, backgroundColor: 'white', justifyContent: 'center' }, androidStyle]}>
              <TouchButton title={strings.sub.toUpperCase()} status={loading} cancel={false} style={{ width: width - 40 }} backgroundColor={DCColor.BGColor('primary-1')} onPress={this.onSubmit} />
            </View>
        }
        <ActionSheet
          ref={e => this.ActionSheet = e}
          options={[strings.take_photo, strings.select_from_album, strings.cancel]}
          cancelButtonIndex={2}
          onPress={this._pressActionSheet.bind(this)}
        />
      </SafeAreaView>
    )
  }
}

function TouchButton(props) {
  const { style, title, backgroundColor, onPress, status = false, cancel = false } = props
  return (
    <TouchableOpacity style={[styles.button, { backgroundColor }, style]} onPress={cancel ? onPress : status ? () => { } : onPress}>
      {cancel ?
        <Text style={{ fontSize: TextFont.TextSize(15), color: DCColor.BGColor('primary-2'), fontWeight: 'bold' }}>{title}</Text> :
        status ?
          <ActivityIndicator color={'#fff'} />
          :
          <Text style={{ fontSize: TextFont.TextSize(15), color: DCColor.BGColor('primary-2'), fontWeight: 'bold' }}>{title}</Text>
      }

    </TouchableOpacity>
  )
}

