import React, { Component } from 'react'
import { Text, View, TouchableOpacity, Modal, PixelRatio } from 'react-native'
import { Screen, Define, TextFont, DCColor } from 'dacsee-utils'
import Wheel from './Wheel'
import _ from 'lodash'
import { inject, observer } from 'mobx-react'

const { height, width } = Screen.window
const pixelSize = (function () {
  let pixelRatio = PixelRatio.get()
  if (pixelRatio >= 3) return 0.333
  else if (pixelRatio >= 2) return 0.5
  else return 1
})()

@inject('app')
@observer
export default class CmpSelect extends Component {
  constructor(props) {
    super(props)
    this.state = {
      item: '',
      modalVisible: false,
    }
  }

  componentDidMount() {
    this.setState({
      item: this.props.option[0]
    })
  }

  onChange(index) {
    this.setState({
      item: this.props.option[index]
    })
  }

  render() {
    let modalHeight = Define.system.ios.x ? 266 + 22 : 266
    let weelHeight = modalHeight - 70
    const { strings } = this.props.app
    const { visible, onHide, onConfirm, option = [] } = this.props
    return (
      <Modal
        animationType='fade'           //渐变
        transparent={true}             // 不透明
        visible={visible}    // 根据isModal决定是否显示
        onRequestClose={onHide}  // android必须实现 安卓返回键调用
      >
        <View style={{ width: width, height: height, backgroundColor: 'rgba(57, 56, 67, 0.2)' }}>
          <View style={{ width: width, height: height - modalHeight }} />
          <View style={{ height: modalHeight, backgroundColor: '#fff', paddingBottom: 10 }}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', borderBottomWidth: pixelSize, borderBottomColor: '#ccc', alignItems: 'center', width: width, height: 50 }}>
              <TouchableOpacity style={{ height: 50, paddingHorizontal: 20, alignItems: 'center', justifyContent: 'center' }} onPress={onHide} >
                <Text style={{ color: DCColor.BGColor('primary-2'), fontSize: TextFont.TextSize(15) }}>{strings.cancel}</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{ height: 50, paddingHorizontal: 20, alignItems: 'center', justifyContent: 'center' }} onPress={() => onConfirm(this.state.item)}>
                <Text style={{ color: DCColor.BGColor('primary-1'), fontSize: TextFont.TextSize(15) }}>{strings.confirm}</Text>
              </TouchableOpacity>
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', width: width, height: weelHeight }}>
              <Wheel
                style={{ height: weelHeight, width: width }}
                itemStyle={{ textAlign: 'center' }}
                index={option.indexOf(this.state.item)}
                items={option}
                onChange={(index) => this.onChange(index)}
              />
            </View>
          </View>
        </View>
      </Modal>
    )
  }
}
