import React, { Component } from 'react'
import { Image } from 'react-native'
import { toJS } from 'mobx'
import { DCColor } from 'dacsee-utils'

import MapView, { Marker, Polyline } from 'react-native-maps'

const ANIMATE_DURATION = 1500

const CAR_MARKER = {
  budget: require('./images/car_budget.png'),
  premium: require('./images/car_premium.png'),
  rookie: require('./images/car_rookie.png'),
  taxi: require('./images/car_taxi.png')
}



export class GoogleMapViewComponent extends Component {

  constructor(props) {
    super(props)
    this.state = { render: false }
  }

  async componentDidMount() {
    const support = await $.bridge.utils.google_service.support()
    this.setState({ render: support })
  }

  moveTo({ latitude, longitude, latitudeDelta, longitudeDelta }) {
    const _lat = isNaN(parseFloat(latitude)) ? 0 : parseFloat(latitude)
    const _lng = isNaN(parseFloat(longitude)) ? 0 : parseFloat(longitude)
    const _latitudeDelta = isNaN(parseFloat(latitudeDelta)) ? .005 : parseFloat(latitudeDelta)
    const _longitudeDelta = isNaN(parseFloat(longitudeDelta)) ? .005 : parseFloat(longitudeDelta)

    const region = {
      latitude: _lat,
      longitude: _lng,
      latitudeDelta: _latitudeDelta,
      longitudeDelta: _longitudeDelta
    }

    if (this.instance) {
      this.instance.animateToRegion(region, ANIMATE_DURATION)
    }
  }

  render() {
    const { render } = this.state
    const extraProps = { ...this.props }
    const STORAGE_URL = $.config.staging ? 'https://storage.googleapis.com/dacsee-dev-service-circle/' : 'https://storage.googleapis.com/dacsee-service-circle/'

    let zIndex = 1
    extraProps.children = []

    if (extraProps.markers && extraProps.markers.length > 0) {
      const markers = extraProps.markers.filter((marker) => {
        return marker.coords && (marker.coords.lat || marker.coords.latitude) && (marker.coords.lng || marker.coords.longitude)
      })

      const markers_element = markers.map((pipe, index) => {
        let coords = {
          latitude: pipe.coords.lat ? parseFloat(pipe.coords.lat) : parseFloat(pipe.coords.latitude),
          longitude: pipe.coords.lng ? parseFloat(pipe.coords.lng) : parseFloat(pipe.coords.longitude)
        }

        if (pipe.marker) {
          let url = pipe.marker.urlSmall ? pipe.marker.urlSmall : STORAGE_URL + pipe.marker

          return (
            <Marker key={`markers_${index}`} coordinate={coords} style={{ zIndex: zIndex++ }}>
              <Image source={{ uri: url }} defaultSource={CAR_MARKER[pipe.type]} style={{ height: 35, width: 35, resizeMode: 'contain' }} />
            </Marker>
          )
        } else {
          return (
            <Marker key={`markers_${index}`} coordinate={coords} style={{ zIndex: zIndex++ }}>
              <Image source={CAR_MARKER[pipe.type]} style={{ height: 35, width: 35, resizeMode: 'contain' }} />
            </Marker>
          )
        }
      })

      extraProps.children = markers_element
    }

    if (extraProps.initialRegion) {
      extraProps.initialRegion = extraProps.initialRegion.latitudeDelta ? { ...extraProps.initialRegion } : { ...extraProps.initialRegion, latitudeDelta: .014, longitudeDelta: .014 }
    }

    if (!!extraProps.from && !!extraProps.destination && extraProps.from.coords && extraProps.destination.coords) {
      const fromCoords = {
        latitude: extraProps.from.coords.lat ? parseFloat(extraProps.from.coords.lat) : parseFloat(extraProps.from.coords.latitude),
        longitude: extraProps.from.coords.lng ? parseFloat(extraProps.from.coords.lng) : parseFloat(extraProps.from.coords.longitude),
      }
      const destinationCoords = {
        latitude: extraProps.destination.coords.lat ? parseFloat(extraProps.destination.coords.lat) : parseFloat(extraProps.destination.coords.latitude),
        longitude: extraProps.destination.coords.lng ? parseFloat(extraProps.destination.coords.lng) : parseFloat(extraProps.destination.coords.longitude),
      }

      const from_element = (
        <Marker key={'from'} coordinate={fromCoords} style={{ zIndex: zIndex++ }}>
          <Image source={require('./images/from.png')} style={{ height: 35, width: 35, resizeMode: 'contain' }} />
        </Marker>
      )

      const destination_element = (
        <Marker key={'destination'} coordinate={destinationCoords} style={{ zIndex: zIndex++ }}>
          <Image source={require('./images/destination.png')} style={{ height: 35, width: 35, resizeMode: 'contain' }} />
        </Marker>
      )

      extraProps.children.push(from_element)
      extraProps.children.push(destination_element)
    }

    if (extraProps.routes && extraProps.routes.every(pipe => (typeof (pipe.latitude) === 'number' && typeof (pipe.longitude) === 'number'))) {
      const router_element = (
        <Polyline
          key={'router'}
          strokeWidth={3}
          strokeColor={DCColor.BGColor('primary-2')}
          // lineCap={'square'} 
          // lineJoin={'bevel'}
          coordinates={toJS(extraProps.routes)}
        />
      )
      extraProps.children.push(router_element)
    }

    delete extraProps.routes
    delete extraProps.from
    delete extraProps.destination

    return (
      <MapView
        provider={'google'}
        ref={e => this.instance = e}
        showsScale={false}
        showsCompass={false}
        rotateEnabled={false}
        minZoomLevel={2}
        customMapStyle={STANDARD_MODE}
        loadingEnabled={true}
        // maxZoomLevel={20}
        {...extraProps}
      />
    )
  }
}

export class GoogleMarkerComponent extends Component {
  render() {
    return (<Marker {...this.props} />)
  }
}

export class GooglePolylineComponent extends Component {
  render() {
    return (<Polyline {...this.props} />)
  }
}

const STANDARD_MODE = [
  {
    "featureType": "administrative.land_parcel",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "administrative.neighborhood",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "landscape.man_made",
    "elementType": "geometry.fill",
    "stylers": [
      {
        "color": "#f3f3f2"
      }
    ]
  },
  {
    "featureType": "landscape.man_made",
    "elementType": "geometry.stroke",
    "stylers": [
      {
        "color": "#f3f3f2"
      }
    ]
  },
  {
    "featureType": "poi.business",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "labels",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "road.arterial",
    "elementType": "geometry.fill",
    "stylers": [
      {
        "color": "#e7e8e9"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry.fill",
    "stylers": [
      {
        "color": "#efe6ae"
      }
    ]
  },
  {
    "featureType": "road.local",
    "elementType": "geometry.fill",
    "stylers": [
      {
        "color": "#e7e8e9"
      }
    ]
  },
  {
    "featureType": "road.local",
    "elementType": "geometry.stroke",
    "stylers": [
      {
        "color": "#e7e8e9"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry.fill",
    "stylers": [
      {
        "color": "#52b4e9"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  }
]
