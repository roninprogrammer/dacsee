import React, {Component} from 'react'

// import { AMapViewComponent, AMarkerComponent, APolylineComponent } from './map.a'
import { GoogleMapViewComponent, GoogleMarkerComponent, GooglePolylineComponent } from './map.google'

export class MapView extends Component {
  render() {
    const {region_type} = this.props
 //   const Component = region_type === 'CN' ? AMapViewComponent : GoogleMapViewComponent
    const Component = GoogleMapViewComponent
    const extraProps = {...this.props}

    return (<Component {...extraProps} ref={e => this.handle = e} />)
  }
}