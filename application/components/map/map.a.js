import React, {Component} from 'react'
import {View, Text} from 'react-native'

import { MapView, Marker, Polyline } from 'react-native-amap3d'

const ANIMATE_DURATION = 500

export class AMapViewComponent extends Component {

  moveTo(lat, lng, zoom) {
    const _lat = parseFloat(lat || 0)
    const _lng = parseFloat(lng || 0)
    const _zoom = parseFloat(zoom || 10)

    if (this.ref.handle) {
      this.ref.handle.animateTo({ zoomLevel: _zoom, coordinate: { latitude: _lat, longitude: _lng } }, ANIMATE_DURATION)
    }
  }

  render() {
    console.log(this.props)
    return (
      <MapView 
        minZoomLevel={4}
        showsScale={false}
        showsCompass={false}
        rotateEnabled={false}
        refs={'handle'} 
        {...this.props} 
      />
    )
  }
}

export class AMarkerComponent extends Component {
  render() {
    return (<Marker {...this.props} />)
  }
}

export class APolylineComponent extends Component {
  render() {
    return (<Polyline {...this.props} />)
  }
}