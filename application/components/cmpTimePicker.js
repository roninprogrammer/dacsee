import React, { Component } from 'react'
import { Text, View, TouchableOpacity, Modal, DatePickerIOS } from 'react-native'
import { Screen, DCColor, Define, TextFont } from 'dacsee-utils'
import _ from 'lodash'
import moment from 'moment'
const { height, width } = Screen.window
const pixelSize = 0.6
export default class CmpTimePicker extends Component {
  constructor(props) {
    super(props)
    this.state = {
      chosenDate: new Date(),
    }
    this.setDate = this.setDate.bind(this)
  }

  setDate(newDate) {
    this.setState({
      chosenDate: newDate
    })
  }

  render() {
    const { chosenDate } = this.state
    let modalHeight = Define.system.ios.x ? 266 + 22 : 266
    const otherOptions = {}
    const { minYear, maxYear } = this.props

    if (maxYear) otherOptions.maximumDate = moment().add(maxYear, 'year').toDate()
    if (minYear) otherOptions.minimumDate = moment().add(minYear, 'year').toDate()

    return (
      <Modal
        animationType='fade'           //渐变
        transparent={true}             // 不透明
        visible={this.props.visible}    // 根据isModal决定是否显示
        onRequestClose={this.props.onHide}  // android必须实现 安卓返回键调用
      >
        <View style={{ width: width, height: height, backgroundColor: 'rgba(57, 56, 67, 0.2)' }}>
          <View style={{ width: width, height: height - modalHeight }}></View>
          <View style={{ height: modalHeight, backgroundColor: '#fff', paddingBottom: 10 }}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', borderBottomWidth: pixelSize, borderBottomColor: '#ccc', alignItems: 'center', width: width, height: 50 }}>
              <TouchableOpacity style={{ height: 50, paddingHorizontal: 20, alignItems: 'center', justifyContent: 'center' }} onPress={this.props.onHide} >
                <Text style={{ color: DCColor.BGColor('primary-2'), fontSize: TextFont.TextSize(15) }}>{this.props.strings.cancel}</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{ height: 50, paddingHorizontal: 20, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.props.onConfirm(this.state.chosenDate)}>
                <Text style={{ color: DCColor.BGColor('primary-1'), fontSize: TextFont.TextSize(15) }}>{this.props.strings.confirm}</Text>
              </TouchableOpacity>
            </View>
            <View style={{ flex: 1, justifyContent: 'center' }}>
              <DatePickerIOS
                mode='date'
                date={chosenDate}
                onDateChange={this.setDate}
                {...otherOptions}
              />
            </View>
          </View>
        </View>
      </Modal>
    )
  }
}