import React, { Component } from 'react'
import { Text, View, TouchableOpacity, Modal, PixelRatio } from 'react-native'
import { Screen, Define, TextFont, DCColor } from 'dacsee-utils'
import Wheel from './Wheel'
import _ from 'lodash'
const { height, width } = Screen.window
const pixelSize = (function () {
  let pixelRatio = PixelRatio.get()
  if (pixelRatio >= 3) return 0.333
  else if (pixelRatio >= 2) return 0.5
  else return 1
})()
export default class SelectPay extends Component {
  constructor(props) {
    super(props)
    this.pays = [this.props.strings.cash] //,this.props.strings.wallet,this.props.strings.credit_card
    this.payskey = ['cash'] //,'wallet','credit_card'
    this.state = {
      pay: this.payskey[0, 1]
    }
  }
  componentDidMount() {

  }
  onPayChange(index) {
    this.setState({
      pay: this.payskey[index]
    })
  }

  render() {
    let modalHeight = Define.system.ios.x ? 266 + 22 : 266
    let weelHeight = modalHeight - 70
    return (
      <Modal
        animationType='fade'           //渐变
        transparent={true}             // 不透明
        visible={this.props.visible}    // 根据isModal决定是否显示
        onRequestClose={() => this.props.payChange()}  // android必须实现 安卓返回键调用
      >
        <View style={{ width: width, height: height, backgroundColor: 'rgba(57, 56, 67, 0.2)' }}>
          <View style={{ width: width, height: height - modalHeight }}></View>
          <View style={{ height: modalHeight, backgroundColor: '#fff', paddingBottom: 10 }}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', borderBottomWidth: pixelSize, borderBottomColor: '#ccc', alignItems: 'center', width: width, height: 50 }}>
              <TouchableOpacity style={{ height: 50, paddingHorizontal: 20, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.props.payChange()} >
                <Text style={{ color: DCColor.BGColor('primary-2'), fontSize: TextFont.TextSize(15) }}>{this.props.strings.cancel}</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{ height: 50, paddingHorizontal: 20, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.props.payChange(this.state.pay)} >
                <Text style={{ color: DCColor.BGColor('primary-1'), fontSize: TextFont.TextSize(15) }}>{this.props.strings.confirm}</Text>
              </TouchableOpacity>
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', width: width, height: weelHeight }}>
              <Wheel
                style={{ height: weelHeight, width: width }}
                itemStyle={{ textAlign: 'center' }}
                index={this.payskey.indexOf(this.state.pay)}
                items={this.pays}
                onChange={index => this.onPayChange(index)}
              />
            </View>
          </View>
        </View>
      </Modal>
    )
  }
}
