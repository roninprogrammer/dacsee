import React, { Component, cloneElement } from 'react'
import { Text, View, Platform, TouchableOpacity } from 'react-native'
import { DCColor } from 'dacsee-utils';

export default class HeaderSwitcher extends Component {

  static Item = class SwitcherItem extends Component {

    constructor(props) {
      super(props)
    }

    render() {
      return (
        <TouchableOpacity hitSlop={{ left: 5, right: 5, top: 5, bottom: 5 }} onPress={this.props.onPress} activeOpacity={this.props.active ? 1 : .15} style={{ paddingHorizontal: 12 }}>
          <Text style={[{ color: 'rgba(0, 0, 0 ,0.35)', fontSize: 17, fontWeight: '600' }, this.props.active ? { color: DCColor.BGColor('primary-2') } : {}]}>{this.props.children}</Text>
        </TouchableOpacity>
      )
    }
  }

  render() {
    const { children, onlyTitle } = this.props
    const childs = Array.isArray(children) ? children : [children]

    let elements = childs.map((element, index) => {
      const active = this.props.active === parseInt(index)

      const extraProps = { active }
      extraProps.onPress = () => { this.props.onPress && this.props.onPress(index) }
      extraProps.key = `${index}_${element.type.name}`
      return cloneElement(element, extraProps)
    })

    if (onlyTitle) {
      elements = elements.filter(element => element.props.active)
    }

    return Platform.select({
      ios: (
        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', height: 44 }}>
          {elements}
        </View>
      ),
      android: (
        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', height: 44 }}>
          {elements}
        </View>
      )
    })
  }
}