import React, { Component } from 'react'
import { Text, View, TouchableOpacity, Modal, PixelRatio } from 'react-native'
import { Screen, DCColor, Define, TextFont } from 'dacsee-utils'
import Wheel from './Wheel'
import _ from 'lodash'
import { inject, observer } from 'mobx-react'
import { toJS } from 'mobx'

const { height, width } = Screen.window
const pixelSize = (function () {
  let pixelRatio = PixelRatio.get()
  if (pixelRatio >= 3) return 0.333
  else if (pixelRatio >= 2) return 0.5
  else return 1
})()

@inject('app', 'vehicles')
@observer
export default class SelectManufacturer extends Component {
  constructor(props) {
    super(props)
    this.manufacturerData = []
    this.state = {
      manufacturerName: '',
      modalVisible: false,
      selectedIndex: -1
    }
  }

  async componentDidMount() {
    await this.props.vehicles.getVehiclesData()
    this.manufacturerData = toJS($.store.vehicles.manufacturerData)
  }

  onManufacturerChange(index) {
    this.setState({
      selectedIndex: index
    })
    this.setState({ manufacturerName: this.manufacturerData[index] })
  }

  setModalVisbile(visible) {
    this.setState({ modalVisible: visible })
  }


  render() {
    let modalHeight = Define.system.ios.x ? 266 + 22 : 266
    let weelHeight = modalHeight - 70
    const { strings } = this.props.app
    const { selectedIndex = -1 } = this.state;

    return (
      <Modal
        animationType='fade'           //渐变
        transparent={true}             // 不透明
        visible={this.props.visible}    // 根据isModal决定是否显示
        onRequestClose={() => this.props.manufacturerChange()}  // android必须实现 安卓返回键调用
      >
        <View style={{ width: width, height: height, backgroundColor: 'rgba(57, 56, 67, 0.2)' }}>
          <View style={{ width: width, height: height - modalHeight }}></View>
          <View style={{ height: modalHeight, backgroundColor: '#fff', paddingBottom: 10 }}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', borderBottomWidth: pixelSize, borderBottomColor: '#ccc', alignItems: 'center', width: width, height: 50 }}>
              <TouchableOpacity style={{ height: 50, paddingHorizontal: 20, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.props.manufacturerChange()} >
                <Text style={{ color: DCColor.BGColor('primary-2'), fontSize: TextFont.TextSize(15) }}>{strings.cancel}</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{ height: 50, paddingHorizontal: 20, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.props.manufacturerChange(this.state.manufacturerName)}>
                <Text style={{ color: DCColor.BGColor('primary-1'), fontSize: TextFont.TextSize(15) }}>{strings.confirm}</Text>
              </TouchableOpacity>
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', width: width, height: weelHeight }}>
              <Wheel
                style={{ height: weelHeight, width: width }}
                itemStyle={{ textAlign: 'center' }}
                index={this.manufacturerData.indexOf(this.state.manufacturerName)}
                items={this.manufacturerData}
                onChange={(index) => this.onManufacturerChange(index)}
              />
            </View>
          </View>
        </View>
      </Modal>
    )
  }
}
