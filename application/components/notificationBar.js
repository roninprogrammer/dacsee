import React from 'react'
import { Text, View, Image, StyleSheet } from 'react-native'
import { TextFont } from 'dacsee-utils'
import Resource from 'dacsee-resources'

const styles = StyleSheet.create({
    mainCont: {
        borderRadius: 8,
        padding: 20,
        flexDirection: 'row',
        width: '100%',
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
    },
    dcIcon: {
        height: 45,
        width: 45,
        resizeMode: 'contain'
    },
    dcText: {
        flex: 1,
        fontWeight: '400',
        justifyContent: 'center',
        alignItems: 'center',
        fontSize: TextFont.TextSize(14),
        lineHeight: TextFont.TextSize(16),
        color: 'rgba(0, 0, 0, 0.5)',
        marginLeft: 15,
        fontWeight: '400',
    },
})

const NotificationBar = ({ icon, label, onPress }) => {
    const {
        mainCont,
        dcIcon,
        dcText
    } = styles

    return (
        <View style={mainCont}>
            <Image style={dcIcon} source={icon} />
            <Text style={dcText}>{label}</Text>
            {onPress &&
                <Image style={{ width: 8, height: 8, resizeMode: 'contain' }} source={Resource.image.right_icon} />
            }
        </View>
    )
}

export default NotificationBar