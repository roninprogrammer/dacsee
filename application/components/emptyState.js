import React from 'react'
import { Text, View, Image, StyleSheet } from 'react-native'
import { TextFont, Screen } from 'dacsee-utils'
import Resource from '../resources'

const { width } = Screen.window

const styles = StyleSheet.create({
    mainCont: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        width
    },
    imgCont: {
        width: 90,
        height: 90,
        borderRadius: 60,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(0, 0, 0, 0.1)',
        marginBottom: 35
    },
    dcImg: {
        height: 80,
        width: 80,
        resizeMode: 'contain',
        position: 'absolute'
    },
    fTitle: {
        fontWeight: '900',
        justifyContent: 'center',
        alignItems: 'center',
        fontSize: TextFont.TextSize(18),
        color: 'rgba(0, 0, 0, 0.75)',
        marginBottom: 5
    },
    dcText: {
        fontWeight: '400',
        justifyContent: 'center',
        alignItems: 'center',
        fontSize: TextFont.TextSize(14),
        lineHeight: TextFont.TextSize(16),
        color: 'rgba(0, 0, 0, 0.5)',
    },
    stepCont: {
        width: '80%'
    }
})

const DcEmptyState = ({ icon, title, label, step1, step2, step3, step4, labelStyle }) => {
    const {
        mainCont,
        imgCont,
        dcImg,
        dcText,
        fTitle,
        stepCont
    } = styles

    return (
        <View style={mainCont}>
            <View style={imgCont}>
                <Image style={dcImg} source={icon ? icon : Resource.image.attention_icon} />
            </View>
            {
                title ? <Text style={fTitle}>{title}</Text> : null
            }
            {
                label ? <Text style={!labelStyle ? [dcText, { marginBottom: 25 }] : [dcText, {marginBottom: 25}, labelStyle]}>{label}</Text> : null
            }
            {
                step1 ? (
                    <View style={stepCont}>
                        { step1 ? <Text style={[dcText, { textAlign: 'left', marginBottom: 5 }]}>{step1}</Text> : null }
                        { step2 ? <Text style={[dcText, { textAlign: 'left', marginBottom: 5 }]}>{step2}</Text> : null }
                        { step3 ? <Text style={[dcText, { textAlign: 'left', marginBottom: 5 }]}>{step3}</Text> : null }
                        { step4 ? <Text style={[dcText, { textAlign: 'left' }]}>{step4}</Text> : null }
                    </View>
                ) : null
            }
            
        </View>
    )
}

export default DcEmptyState