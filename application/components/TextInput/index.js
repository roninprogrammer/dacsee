import React, {Component} from 'react'
import {Platform, TextInput} from 'react-native'

// class TextInputSolution extends Component {
//   shouldComponentUpdate(nextProps) {
//     return Platform.OS !== 'ios' || this.props.value === nextProps.value
//   }

//   clear() {
//     this.input && this.input.clear()
//   }
  
//   render() {
//     const extraProps = this.props
//     delete extraProps.ref

//     return <TextInput ref={e => this.input = e} {...extraProps} />
//   }
// }

class TextInputSolution extends Component {

  static defaultProps = {
    onFocus: () => { }
  }

  constructor(props) {
    super(props)

    this.state = {
      value: this.props.value,
      refresh: false,
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (this.state.value !== nextState.value) {
      return false
    }
    return true
  }

  componentDidUpdate(prevProps) {
    if (prevProps.value !== this.props.value && this.props.value === '') {
      this.setState({ value: '', refresh: true }, () => this.setState({ refresh: false }))
    }
  }

  onFocus = (e) => {
    this.input.focus()
    this.props.onFocus()
  }
  
  render() {
    if (this.state.refresh) {
      return null
    }
  
    return (
      <TextInput
        {...this.props}
        ref={(ref) => { this.input = ref }}
        value={this.state.value}
      />
    )
  }
}

export default TextInputSolution