import React, { Component, Children, cloneElement, createElement } from 'react'
import { TouchableOpacity, View, Image, Text, SafeAreaView, ScrollView, StyleSheet, ViewPagerAndroid, StatusBar } from 'react-native'
import { Platform, Dimensions } from 'react-native'
import { observable, autorun } from 'mobx'
import { inject, observer } from 'mobx-react'
import Awesome from 'react-native-vector-icons/FontAwesome'
import { Header } from 'react-navigation'

const { width, height } = Dimensions.get('window')
const iPhoneX = height === 812
const STATUS_DEFINE = {
  ERROR: -10,
  NO_TAKER: -1,
  PICK_ADDRESS: 0,
  PASSENGER_CONFIRM: 1,
  WAIT_RESPONSE: 2,
  WAIT_DRIVER_CONFRIM: 3,
  DRIVER_CONFRIM: 4,
  ON_THE_WAY: 5,
  DRIVER_ARRIVED: 6,
  WAIT_PASSENGER: 7,
  ON_BOARD: 8,
  ON_RATING: 9,
  CANCEL_BY_DRIVER: 10,
  FINALIZE_TOTAL_FARE: 11,
  COMPLETED_BY_PASSENGER: 12,
  SUCCESS: 13,
  CONFIRMED: 14
}

@observer
export default class TabContainer extends Component {
  TabItems = []

  constructor(props) {
    super(props)
    this.dispose = undefined
  }

  componentDidMount() {
    this.dispose = autorun(() => this.switchComponent(this.props.tab))
  }

  componentWillUnmount() {
    this.dispose && this.dispose()
  }

  switchComponent(name) {
    const index = TabItems.indexOf(name)
    this.scroll && this.scroll.setPageWithoutAnimation && this.scroll.setPageWithoutAnimation(index)
    this.scroll && this.scroll.scrollTo && this.scroll.scrollTo({ y: 0, x: index * width, animated: false })
  }

  render() {
    const showFooter = $.store.passenger.status < STATUS_DEFINE.PASSENGER_CONFIRM
    const childs = Array.isArray(this.props.children) ? this.props.children : [this.props.children]
    const child = childs.map((element, index) => {
      const active = this.props.tab === element.props.name

      const extraProps = { active, index }
      extraProps.name = element.props.name
      extraProps.onPress = () => { this.props.onPress && this.props.onPress(element.props.name) }
      extraProps.key = `${index}_${element.type.name}`
      extraProps.totalUnread = this.props.totalUnread
      extraProps.totalReward = this.props.totalReward
      return cloneElement(element, extraProps)
    })

    TabItems = child.map(e => e.props.name)

    const component = child.map(({ props, type }, index) => {
      const extraProps = {}
      extraProps.key = `${index}_${type.name}`
      extraProps.style = { flex: 1, width }
      return createElement(View, extraProps, cloneElement(props.render))
    })

    const WrapComponent = View
    const ContainerComponent = Platform.select({ ios: ScrollView, android: ViewPagerAndroid })
    const ContainerBar = View


    return (
      <WrapComponent style={styles.wrapStyle}>
        <ContainerComponent
          ref={e => this.scroll = e}
          peekEnabled={false}
          horizontal
          scrollEnabled={false}
          pinchGestureEnabled={false}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
          style={styles.componentStyle}
        >
          {component}
        </ContainerComponent>
        {showFooter ?
          <ContainerBar style={styles.barStyle}>
            {child}
          </ContainerBar>
          : null
        }
      </WrapComponent>
    )
  }
}

const styles = StyleSheet.create({
  wrapStyle: {
    width,
    height: '100%',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'flex-start'
  },
  componentStyle: {
    flex: 1,
    width,
    height: '100%',
  },
  barStyle: {
    width,
    height: iPhoneX ? 22 + 56 : 56,
    paddingBottom: iPhoneX ? 22 : 0,
    backgroundColor: '#fdfdfe',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    shadowOffset: { width: 0, height: -1 },
    shadowColor: '#000',
    shadowOpacity: 0.05,
    elevation: 0.5
  }
})