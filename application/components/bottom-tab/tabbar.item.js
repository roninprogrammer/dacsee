import React, { Component } from 'react'
import { TouchableOpacity, View, Image, StyleSheet, Text } from 'react-native'
import { DCColor, TextFont } from 'dacsee-utils'

class TabBarItem extends Component {
  
  render() {
    const { wrap, circle, fLabel, dcActive } = styles
    const { active, onPress = () => { }, iconStyle, icon, index, name, totalUnread, totalReward, label } = this.props
    return (
      <TouchableOpacity hitSlop={{ left: 5, right: 5, top: 5, bottom: 5 }} onPress={onPress} activeOpacity={1} style={wrap}>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <Image style={[iconStyle, active ? { tintColor: DCColor.BGColor('primary-1') } : {}]} source={icon} />
          {
            (name === 'ChatTab' && totalUnread > 0) && <View style={[circle,
            totalUnread > 99 ?
              { paddingHorizontal: 7 } :
              totalUnread > 10 ?
                { height: 24, width: 24, borderRadius: 12 } :
                { width: 20 }]}>
              <Text style={{ color: '#fff', backgroundColor: 'transparent', fontSize: 11 }}>{totalUnread > 99 ? '99+' : totalUnread}</Text>
            </View>
          }
          {
            (name === 'JoyTab' && totalReward > 0) &&
            <View style={[
              styles.circle,
              totalReward > 99 ?
                { paddingHorizontal: 7 } :
                totalReward > 10 ?
                  { height: 24, width: 24, borderRadius: 12 } :
                  { width: 20 }]}>
              <Text style={{ color: '#fff', backgroundColor: 'transparent', fontSize: 11 }}>{totalReward > 99 ? '99+' : totalReward}</Text>
            </View>
          }
        </View>
        <Text style={active ? [fLabel, dcActive] : fLabel}>{label}</Text>
      </TouchableOpacity>
    )
  }

}

export default TabBarItem

const styles = StyleSheet.create({
  wrap: { 
    width: 95, 
    height: 56, 
    justifyContent: 'center', 
    alignItems: 'center' 
  },
  circle: {
    height: 20,
    borderRadius: 10,
    position: 'absolute',
    backgroundColor: DCColor.BGColor('red'),
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 999,
    top: -4,
    left: 20
  },
  fLabel: {
    fontSize: TextFont.TextSize(9),
    color: 'rgba(0, 0, 0, 0.3)'
  },
  dcActive: {
    color: DCColor.BGColor('primary-1')
  }
})