import React from 'react';
import { StyleSheet, Text, Image, View, TouchableOpacity, TextInput } from 'react-native';
import Resources from 'dacsee-resources';
import { Screen } from 'dacsee-utils'
const { width } = Screen.window

const RadioButton = ({ data = [], checked, text, onChange, handleSelect = () => { } }) => {
    const checkedImageSrc = Resources.image.radio_button_checked
    const uncheckedImageSrc = Resources.image.radio_button_unchecked
    return (
        <View>
            {data.map((data, key) => (
                <View key={key} style={{ marginRight: 20, marginLeft: 20 }}>
                    {data.name === 'other reason driver' || data.name === 'other reason passenger' ?
                        <TouchableOpacity style={styles.btn} onPress={() => handleSelect(key)}>
                            <Image
                                source={checked == key ? checkedImageSrc : uncheckedImageSrc}
                                style={{ height: 20, width: 20, marginRight: 5 }}
                            />
                            {(() => {
                                switch ($.store.app.language) {
                                    case 'EN_US':
                                        return <Text style={{ fontSize: 14, fontWeight: 'bold' }}>{data.text.EN_US}</Text>
                                    case 'ZH_CN':
                                        return <Text style={{ fontSize: 14, fontWeight: 'bold' }}>{data.text.ZH_CN}</Text>
                                    case 'MAS':
                                        return <Text style={{ fontSize: 14, fontWeight: 'bold' }}>{data.text.MAS}</Text>
                                    default:
                                        return null
                                }
                            })()}
                            <TextInput
                                underlineColorAndroid={'transparent'}
                                editable={checked == 5 ? true : false}
                                style={{ width: width - 200, borderBottomColor: 'black', borderBottomWidth: 0.5 }}
                                onChangeText={(text) => onChange(text)}
                                value={text}
                            />
                        </TouchableOpacity>
                        :
                        <TouchableOpacity style={styles.btn} onPress={() => handleSelect(key)}>
                            <Image
                                source={checked == key ? checkedImageSrc : uncheckedImageSrc}
                                style={{ height: 20, width: 20, marginRight: 5 }}
                            />
                            {(() => {
                                switch ($.store.app.language) {
                                    case 'EN_US':
                                        return <Text style={{ fontSize: 14, fontWeight: 'bold' }}>{data.text.EN_US}</Text>
                                    case 'ZH_CN':
                                        return <Text style={{ fontSize: 14, fontWeight: 'bold' }}>{data.text.ZH_CN}</Text>
                                    case 'MAS':
                                        return <Text style={{ fontSize: 14, fontWeight: 'bold' }}>{data.text.MAS}</Text>
                                    default:
                                        return null
                                }
                            })()}
                        </TouchableOpacity>
                    }
                </View>
            )
            )}
        </View>
    );
}
// }
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
    },
    btn: {
        flexDirection: 'row',
        marginBottom: 10
    }
})

export default RadioButton