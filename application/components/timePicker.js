import React, { Component } from 'react'
import { Text, View, TouchableOpacity, Modal } from 'react-native'
import { Screen, Define, TextFont, DCColor } from 'dacsee-utils'
import Wheel from './Wheel'
import _ from 'lodash'
import moment from 'moment'
const { height, width } = Screen.window
const pixelSize = 0.6
export default class TimePicker extends Component {
  constructor(props) {
    super(props)
    this.days = []
    // this.hours = ['0点', '1点', '2点', '3点','4点', '5点', '6点', '7点', '8点', '9点', '10点', '11点', '12点','13点','14点','15点','16点','17点','18点','19点','20点','21点','22点','23点']
    // this.minutes = ['0分','10分','20分','30分','40分','50分']
    this.hours = ['00', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23']
    this.minutes = ['00', '10', '20', '30', '40', '50']

    this.state = {
      hours: this.hours,
      minutes: this.minutes,
      day: this.getDateStr(0),
      hour: this.getDafultHours()[0],
      minute: this.getDafultMinutes()[0],
    }
  }
  async getAllDays(n) {
    let data = []
    for (let i = 0; i < n; i++) {
      let day = await this.getDateStr(i)
      data.push(day)
    }
    return data
  }
  async componentDidMount() {
    this.days = await this.getAllDays(3)
    this.setState({
      hours: await this.getDafultHours(),
      minutes: await this.getDafultMinutes(),
    })
  }


  //获取今天前后n天的日期
  getDateStr(n) {
    let date = new Date()
    date.setDate(date.getDate() + n)//获取n天后的日期
    return moment(date).format('MMM DD ddd')
  }
  getYear(day) {
    let index = _.findIndex(this.days, function (chr) {
      return chr == day
    })
    let date = new Date()
    date.setDate(date.getDate() + index)
    return moment(date).format('YYYY-MM-DD')
  }
  getDafultHours() {
    let HM = this.getNowHM()
    let nowHour = HM.hour
    // let nowMinute = HM.minute
    return this.getDropedDate(this.hours, nowHour)
  }
  getDafultMinutes() {
    let HM = this.getNowHM()
    // let nowHour = HM.hour
    let nowMinute = HM.minute
    return this.getDropedDate(this.minutes, nowMinute)
  }
  getNowHM() {
    let json = {}
    let timestamp = new Date().valueOf()
    let date = new Date(timestamp + 120 * 60 * 1000)
    let h = date.getHours()
    let m = date.getMinutes()
    let y = date.getFullYear()
    if (m % 10 > 0) {
      m = (parseInt(m / 10) + 1) * 10
      if (m === 60) {
        h += 1
        if (h === 24) y += 1
        m = 0
      }
    } else { m = parseInt(m / 10) * 10 }
    json.year = y
    json.hour = h
    json.minute = m
    return json
  }
  getDropedDate(data, pa) {
    let index = _.findIndex(data, function (chr) {
      return chr == pa
    })
    return _.drop(data, index)
  }
  onDayChange(index) {
    let day = this.days[index]
    let HM = this.getNowHM()
    let nowHour = HM.hour
    let nowMinute = HM.minute
    if (this.state.day == this.days[0] && day != this.days[0]) {
      this.setState({ hours: this.hours })
      if (this.state.hour == nowHour) {
        this.setState({ minutes: this.minutes })
      }
    }
    if (day == this.days[0] && this.state.day != day) {

      let hours = this.getDropedDate(this.hours, nowHour)
      this.setState({ hours: hours, hour: hours[0] })
      if (this.state.hour == nowHour) {
        let minutes = this.getDropedDate(this.minutes, nowMinute)
        this.setState({ minutes: minutes, minute: minutes[0] })
      }
    }
    this.setState({ day: day })
  }
  onHourChange(index) {
    let hour = this.state.hours[index]
    let HM = this.getNowHM()
    let nowHour = HM.hour
    let nowMinute = HM.minute
    if (this.state.day == this.days[0] && hour == nowHour) {
      let minutes = this.getDropedDate(this.minutes, nowMinute)
      this.setState({ minutes: minutes, hour: hour, minute: minutes[0] })
    }
    if (this.state.day == this.days[0] && this.state.hour == nowHour && hour != nowHour) {
      this.setState({ hour: hour, minutes: this.minutes })
    }
    this.setState({ hour: hour })
  }
  onMinuteChange(index) {
    this.setState({ minute: this.state.minutes[index] })
  }
  async UNSAFE_componentWillReceiveProps(props) {
    if (props.visible && props.visible !== this.props.visible) {
      let HM = this.getNowHM()
      let newtime = moment(new Date(`${HM.year} ${HM.hour}:${HM.minute}`)).valueOf()
      let oldtime = moment(new Date(`${this.getYear(this.state.day)} ${this.state.hour}:${this.state.minute}`)).valueOf()
      let inittime = moment(new Date(`${this.getYear(this.days[0])} ${this.state.hours[0]}:${this.state.minutes[0]}`)).valueOf()
      if (props.time === 'now' || (oldtime < newtime && inittime < newtime)) {
        await this.dateInit()
      } else if ((inittime < newtime) && this.state.day === this.days[0]) {
        this.setState({
          hours: await this.getDafultHours(),
          minutes: await this.getDafultMinutes()
        })
      }
    }
  }
  //
  async dateInit(status) {
    this.setState({
      hours: await this.getDafultHours(),
      minutes: await this.getDafultMinutes(),
      day: await this.getDateStr(0),
      hour: await this.getDafultHours()[0],
      minute: await this.getDafultMinutes()[0]
    })
  }
  render() {
    let modalHeight = Define.system.ios.x ? 266 + 22 : 266
    let weelHeight = modalHeight - 70
    return (
      <Modal
        animationType='fade'           //渐变
        transparent={true}             // 不透明
        visible={this.props.visible}    // 根据isModal决定是否显示
        onRequestClose={() => this.props.dateChange('now', 'now')}  // android必须实现 安卓返回键调用
      >
        <View style={{ width: width, height: height, backgroundColor: 'rgba(57, 56, 67, 0.2)' }}>
          <View style={{ width: width, height: height - modalHeight }}></View>
          <View style={{ height: modalHeight, backgroundColor: '#fff', paddingBottom: 10 }}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', borderBottomWidth: pixelSize, borderBottomColor: '#ccc', alignItems: 'center', width: width, height: 50 }}>
              <TouchableOpacity style={{ height: 50, paddingHorizontal: 20, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.props.dateChange('now', 'now')} >
                <Text style={{ color: DCColor.BGColor('primary-2'), fontSize: TextFont.TextSize(15) }}>{this.props.time === 'now' ? this.props.strings.cancel : this.props.strings.clear}</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{ height: 50, paddingHorizontal: 20, alignItems: 'center', justifyContent: 'center' }}
                onPress={() => this.props.dateChange('advance', moment(`${this.getYear(this.state.day)} ${this.state.hour}:${this.state.minute}`).toISOString())} >
                <Text style={{ color: DCColor.BGColor('primary-1'), fontSize: TextFont.TextSize(15) }}>{this.props.strings.confirm}</Text>
              </TouchableOpacity>
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', width: width, height: weelHeight }}>
              <Wheel
                style={{ height: weelHeight, width: width / 2 }}
                index={this.days.indexOf(this.state.day)}
                itemStyle={{ textAlign: 'center' }}
                items={this.days}
                onChange={index => this.onDayChange(index)}
              />
              <Wheel
                style={{ height: weelHeight, width: width / 4 }}
                itemStyle={{ textAlign: 'center' }}
                index={this.state.hours.indexOf(this.state.hour)}
                type={'h'}
                items={this.state.hours}
                onChange={index => this.onHourChange(index)}
              />
              <Wheel
                style={{ height: weelHeight, width: width / 4 }}
                itemStyle={{ textAlign: 'center' }}
                type={'m'}
                index={this.state.minutes.indexOf(this.state.minute)}
                items={this.state.minutes}
                onChange={index => this.onMinuteChange(index)}
              />
            </View>
          </View>
        </View>
      </Modal>
    )
  }
}
