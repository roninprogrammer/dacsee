# DacseeMobile

## <strike>ReactNative 0.52.0</strike> React Native 0.59.4
## iOS && Android Platform

### Setup

1) git clone 
2) yarn (npm install)


### Build & Install

Android:
cd (ProjectDir) & npm run build-android
or
Android Studio

iOS:
XCode
iOS Ver 8.0+

### How to run this version

To jetify / convert node_modules dependencies to AndroidX

One of your library dependencies converts to AndroidX, and you need to use the new version. So now you need to convert your app to AndroidX, but many of your react-native libraries ship native Java code and have not updated.


- npm install --save-dev jetifier
- npx jetify
- npx react-native run-android (your app should correctly compile and work)
- Call npx jetify run in the postinstall target of your package.json (Any time your dependencies update you have to jetify again)


### ChangeLog (dev/vicknesh/androidx) - 4/09/2019
+ [ADD] convergence between ios and androidx 
    - new Splash screen in IOS
    - IOS job card fixed with background color 
    - Fare summary page and Header removal in Android 

### Changelog (dev/vicknesh/androidx) - 13/08/2019
+ <strike>[ADD] adding back SmsListener</strike>

### ChangeLog (dev/vicknesh/androidx) - 30/07/2019
+ [ADD] adding back SmsListener


### ChangeLog (dev/vicknesh/androidx) - 22/07/2019
+ [FIX] SecurityException : Permission Denial : requires android.permission.FOREGROUND_SERVICE  
  (Apps wanting to use foreground services must request the FOREGROUND_SERVICE permission first. This is normal permission, so the system automatically grants it to the requesting app. Starting ap foreground services without the permission throws a Security Exception)

+ [FIX] Splash Screen Fix out of proportion 

### ChangeLog (dev/vicknesh/androidx) - 19/07/2019
+ <strike> Change react-native From 0.54 to 0.60 </strike> Remaining react-native version till component refectoring done
+ [ADD] included mathjs packages


### ChangeLog (dev/vicknesh/androidx) - 16/07/2019
+ Change react-native From 0.54 to 0.60
+ Android X supported
+ Splash Screen bug fixing and changed the content of the splash screen 
+ include jsc-android for resolve the missing libjsc.so 


### Migration on AndroidX 
The NPM that i have worked on migrate to AndroidX 
+ :lottie-react-native -> merged to community
+ :rn-splash-screen -> merged to community
+ :react-native-push-notification -> merged to community 
+ :react-native-fs - merge to community  
+ :react-native-audio - merge to community
+ :react-native-location-switch - using my master repo 
+ :react-native-camera (due to crossborder issue)
